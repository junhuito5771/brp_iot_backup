# Daikin Monorepo

#### est. 3 Jun, 2019

### A monorepo containing 2 mobile apps - Daikin & Acson

## Prerequisite

1. NPM - 6.4.1
2. Node - V10.15.3
3. Yarn - 1.9.2
4. Pod - 1.8.4

## Optional

1. NVM - Node version manager
2. YVM - Yarn version manager
3. RVM - Ruby version manager
4. SDKMAN - Software development kit manager

## Project Setup

- Copy the .env.example and rename it to .env.dev, configure the env variables
- In the root directory
- `npx lerna bootstrap`
- `yarn pod` to automatically run all pod install in all necessary folders
- Daikin
  - `yarn`
  - `cd packages/daikin-mobile`
  - `yarn start`
  - IOS: Open the project folder in `packages/daikin-mobile/ios` and click play button
  - Android: Open the project folder in `packages/daikin-mobile/android`and click play button
- Acson
  - `cd packages/acson-mobile`
  - `yarn start`
  - IOS: Open the project folder in `packages/acson-mobile/ios` and click play button
  - Android: Open the project folder in `packages/acson-mobile/android`and click play button

## Development note

- Husky
  1. This project is set up with husky to run lint staged on the changes when performing a commit. If your changes violate the eslint rules, then it will NOT commit your changes and report the error on the terminal together with the rules violated in each file.
  2. To skip husky, you can use `git commit -m <your message> --no-verify`

## Things to note

- When adding a new package to any module
  1. Always add package manually in package.json instead of `yarn add package-name`
  2. Run `npx lerna bootstrap`, NOT `yarn install`
- When adding a new shared module,
  1. create a new folder under `packages`
  2. `cd new-folder && yarn init`
  3. Name package in the format `@module/new-folder`
  4. Go back to root and run `npx lerna bootstrap`
- When using a shared module,
  1. In app `package.json`, add `@module/new-folder: <<version e.g.1.0.0>>` in dependencies
  2. Go back to root and run `npx lerna bootstrap`

  ## IMPORTANT

- Branch naming convention
  1. feature: `feature/`
  2. fix: `fix/`
  3. release: `release/`
  4. others: `misc/` (lerna config, etc)

  * If it is a single app related issue, add in /<company> 
    * Example: `fix/acson/locate_dealer_trim`

- Commit message naming rules:
  1. fix "xxx"
  2. add "yyy"

- Use CodeCommit console for PR, comments, approvals and merging

- Specify tags manually before the PR title
  * Example: (rebase required) xxxxx

- Use this template for PR description:

  ## Issue
  [](url)
  ## Changes
  -
  ## Test
  -

PR Approver
- **from Upstack:** Upstack (approve and merge)
- **from DAMA:** Upstack (approve and merge)
- **from EMI:** DAMA (review) + Upstack (approve and merge)