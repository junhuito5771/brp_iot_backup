# Redux IOT Module

## Publish step

1. Make sure you have ${IOT_TOKEN} set in your environment, for etc, `export IOT_TOKEN=`
2. Cd to this `redux-iot-module`
3. Increment the version number in `package.json`
4. Run `yarn publish`
