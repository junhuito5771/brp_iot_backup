import { call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

const MULTIPLE_PLANS_RETRIEVAL = '2';
const SINGLE_PLANS_RETRIEVAL = '1';

const getPlanRetrievalValue = value => {
  const isArray = Array.isArray(value);
  if (isArray) {
    return MULTIPLE_PLANS_RETRIEVAL;
  } return SINGLE_PLANS_RETRIEVAL;
};

export default function* fetchPlans(action) {
  const allUnits = action.unitResult;

  const apiEnpoint = {
    requestData: {
      type: getPlanRetrievalValue(action.plansList),
      username: action.email,
      plan: action.plansList,
    },
  };

  const responseData = yield call(
    ApiService.post,
    'getplandetails',
    apiEnpoint
  );

  // set planID as key
  const planDetails = {};
  responseData.forEach(element => {
    planDetails[element.planID] = element.features[0];
  });

  // insert features in allUnits
  const unitResult = {};
  const unitKeys = Object.keys(allUnits);

  unitKeys.forEach(thingName => {
    const curUnit = allUnits[thingName];
    const { ...restState } = curUnit;

    unitResult[thingName] = {
      ...restState,
      planFeatures: planDetails[restState.planID]
        ? planDetails[restState.planID]
        : [],
    };
  });

  return unitResult;
}
