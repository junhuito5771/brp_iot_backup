/* eslint-disable no-param-reassign */
import { call, select, put } from 'redux-saga/effects';
import { ApiService, StringHelper } from '@module/utility';
import { setEditStatus, setIsLoading } from '../actions/editUnits';
import { setUnitName } from '../actions/units';

export function* submitEditUnitName(action) {
  try {
    yield put(setIsLoading(true));

    const {
      editUnitNameParams: { unitName, thingName },
    } = action;

    const {
      units: { allUnits },
      user: {
        profile: { email },
      },
      preferredUnits: { units: preferUnits },
    } = yield select(state => state);

    const editHomepagePayload = [];

    Object.keys(allUnits).forEach(curThingName => {
      const curUnit = allUnits[curThingName];
      const preferredDeviceValue =
        preferUnits[curThingName] && preferUnits[curThingName].isAlexaPreferred
          ? 1
          : 0;
      const curUnitName =
        curThingName === thingName ? unitName : curUnit.ACName;
      editHomepagePayload.push({
        thingName: curThingName,
        ACName: StringHelper.trim(curUnitName),
        ACGroup: StringHelper.trim(curUnit.ACGroup),
        Logo: curUnit.Logo,
        groupIndex: curUnit.groupIndex,
        unitIndex: curUnit.unitIndex,
        qx: curUnit.qx,
        isPreferredDevice: preferredDeviceValue,
        needUpdate: 1,
      });
    });

    const response = yield call(ApiService.post, 'sethomepageinfo', {
      requestData: {
        email,
        data: editHomepagePayload,
      },
    });

    if (response && response.message === 'success') {
      yield put(setUnitName({ thingName, unitName }));
      yield put(setEditStatus({ messageType: 'success', message: 'success' }));
    } else {
      yield put(setEditStatus({ messageType: 'error', message: 'error' }));
    }
  } catch (error) {
    yield put(setEditStatus({ messageType: 'error', message: error.message }));
  } finally {
    yield put(setIsLoading(false));
  }
}

export default function* submitEditForm() {
  try {
    yield put(setIsLoading(true));
    const {
      editUnits: { itemIds, items },
      units: { allUnits },
      user: {
        profile: { email },
      },
      preferredUnits: { units: preferUnits },
    } = yield select(state => state);

    const editHomepagePayload = [];

    const validItemIds = itemIds.filter(({ childs }) => childs.length > 0);

    let validGroupIndex = 0;
    validItemIds.forEach(({ id, childs }) => {
      const editedGroupInfo = items[id];

      if (id === '-1') {
        validGroupIndex = -1;
      }
      childs.forEach((itemKey, validUnitIndex) => {
        const unitInfo = allUnits[itemKey];
        const editedUnitInfo = items[itemKey];
        const preferredDeviceValue =
          preferUnits[itemKey] && preferUnits[itemKey].isAlexaPreferred ? 1 : 0;

        const needUpdate =
          editedUnitInfo.name !== unitInfo.ACName ||
          editedGroupInfo.name !== unitInfo.ACGroup ||
          Number(editedUnitInfo.parentKey) !== unitInfo.groupIndex;

        editHomepagePayload.push({
          thingName: unitInfo.ThingName,
          ACName: StringHelper.trim(editedUnitInfo.name),
          ACGroup: StringHelper.trim(editedGroupInfo.name),
          Logo: editedUnitInfo.logo,
          groupIndex: validGroupIndex,
          unitIndex: validUnitIndex,
          qx: unitInfo.qx,
          isPreferredDevice: preferredDeviceValue,
          needUpdate: needUpdate ? 1 : 0,
        });

        validUnitIndex += 1;
      });

      validGroupIndex += 1;
    });

    const response = yield call(ApiService.post, 'sethomepageinfo', {
      requestData: {
        email,
        data: editHomepagePayload,
      },
    });

    if (response && response.message === 'success') {
      yield put(setEditStatus({ messageType: 'success', message: 'success' }));
    } else {
      yield put(setEditStatus({ messageType: 'error', message: 'error' }));
    }
  } catch (error) {
    yield put(setEditStatus({ messageType: 'error', message: error.message }));
  } finally {
    yield put(setIsLoading(false));
  }
}
