import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setEditTimerLoading,
  setEditTimerStatus,
  clearEditTimerStatus,
  setEditTimerParam,
  setEditTimerInitParam,
} from '../actions/editTimer';

const FETCH_WEEKLY_TIMER = 1;
const MAX_NR_OF_TIMER = 42;

function getNrOfTimerFromDay(response) {
  if (!response || !response[0] || response.message) return -1;

  return Object.values(response[0]).length;
}

function isEmptyTimer(response) {
  return response && response.message === 'No data';
}

function* processSuccessResponse(thingName, day) {
  yield put(setEditTimerParam({ key: 'thingName', value: thingName }));
  yield put(setEditTimerInitParam({ key: 'thingName', value: thingName }));
  // You also need to set the default value, cause each thingName has the different default value
  const { allUnits } = yield select(state => state.units);
  const { days } = yield select(state => state.editTimer.config);
  const unit = allUnits[thingName];

  // Get availables modes and select the first one as default values
  const firstAvailableModes = Object.keys(unit.modes).filter(
    modeKey => !unit.modes[modeKey]
  )[0];
  yield put(setEditTimerParam({ key: 'mode', value: firstAvailableModes }));
  yield put(setEditTimerInitParam({ key: 'mode', value: firstAvailableModes }));

  // Toggle the current day
  const newDaysValue = [...days];
  newDaysValue[day] = !days[day];
  yield put(setEditTimerParam({ key: 'days', value: newDaysValue }));
  yield put(setEditTimerInitParam({ key: 'days', value: newDaysValue }));

  yield put(
    setEditTimerStatus({
      messageType: 'canCreateSuccess',
      message: 'success',
    })
  );
}

export default function* checkCanCreateTimer(action) {
  try {
    yield put(clearEditTimerStatus());
    yield put(setEditTimerLoading(true));

    const { thingName, day } = action;

    const {
      user: {
        profile: { email },
      },
    } = yield select(state => state);

    const response = yield call(ApiService.post, 'timerfunction', {
      requestData: {
        type: FETCH_WEEKLY_TIMER,
        username: email,
        thingName,
      },
    });

    if (
      isEmptyTimer(response) ||
      getNrOfTimerFromDay(response) <= MAX_NR_OF_TIMER
    ) {
      yield call(processSuccessResponse, thingName, day);
    } else {
      throw new Error(
        `This unit has reached the maximun of ${MAX_NR_OF_TIMER} timer`
      );
    }
  } catch (error) {
    yield put(
      setEditTimerStatus({
        messageType: 'canCreateError',
        message: error.message,
      })
    );
  } finally {
    yield put(setEditTimerLoading(false));
  }
}
