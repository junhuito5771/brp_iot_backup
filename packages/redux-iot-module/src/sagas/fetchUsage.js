import { all, call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import moment from 'moment';

import {
  setUsageData,
  setLoading,
  setUsageStatus,
  clearUsageStatus,
  setCompareUsageData,
  setToggleButton,
} from '../actions/usage';

const TYPE_TEMP = '1';
const TYPE_ENERGY = '2';

const PERIOD_DAILY = 'daily';
const PERIOD_WEEKLY = 'weekly';
const PERIOD_MONTHLY = 'monthly';
const PERIOD_YEARLY = 'yearly';

const STA_TEMP = 'temp';

const STANDARD_DATE_FORMAT = 'YYYY-MM-DD';

function getPassDate(date, period) {
  switch (period) {
    case PERIOD_DAILY:
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT);
    case PERIOD_WEEKLY:
      return moment(date, STANDARD_DATE_FORMAT)
        .startOf('isoWeek')
        .format(STANDARD_DATE_FORMAT);
    case PERIOD_MONTHLY:
      return moment(date, STANDARD_DATE_FORMAT)
        .startOf('months')
        .format(STANDARD_DATE_FORMAT);
    case PERIOD_YEARLY:
      return moment(date, STANDARD_DATE_FORMAT)
        .startOf('years')
        .format(STANDARD_DATE_FORMAT);
    default:
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT);
  }
}

function getDate(date, period) {
  switch (period) {
    case PERIOD_DAILY:
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT);
    case PERIOD_WEEKLY:
      return moment(date, STANDARD_DATE_FORMAT)
        .endOf('isoWeek')
        .format(STANDARD_DATE_FORMAT);
    case PERIOD_MONTHLY:
      return moment(date, STANDARD_DATE_FORMAT)
        .endOf('months')
        .format(STANDARD_DATE_FORMAT);
    case PERIOD_YEARLY:
      return moment(date, STANDARD_DATE_FORMAT)
        .endOf('years')
        .format(STANDARD_DATE_FORMAT);
    default:
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT);
  }
}

function getPeriodMapper(periodType) {
  switch (periodType) {
    case PERIOD_DAILY: {
      return period => moment(period, 'HH:mm').format('ha');
    }
    case PERIOD_WEEKLY: {
      return period => moment(period, 'YYYY-MM-DD').format('ddd');
    }
    case PERIOD_MONTHLY: {
      return period => moment(period, 'YYYY-MM-DD').format('D');
    }
    case PERIOD_YEARLY: {
      return period => {
        const periodNr = Number(period);

        return moment()
          .month(periodNr - 1)
          .format('MMM');
      };
    }
    default: {
      return v => v;
    }
  }
}

function getRequestBodyByPeriodAndType(params) {
  // 1. Get the period type
  // 2. Get the graph type
  const { periodType, staType, thingName, email, targetDate } = params;
  const date = getDate(targetDate, periodType);

  // 3. Prepare the request body
  const requestBody = {
    requestData: {
      type: staType,
      email,
      day: periodType,
      thingName,
      passDate: getPassDate(targetDate, periodType), // start date
      date, // end date
      lastData: moment(targetDate).isBefore(date) ? 'N' : 'Y',
      ...(periodType === PERIOD_WEEKLY
        ? { week: moment(targetDate).isoWeek() }
        : {}),
    },
  };
  return requestBody;
}

function* fetchGraphData({ unit, email, periodType, targetDate, isInverter }) {
  const { thingName } = unit;
  const apiEndpoint =
    periodType === PERIOD_DAILY || periodType === PERIOD_WEEKLY
      ? 'getacgraphdata'
      : 'getacgraphdatamonyr';

  const apiEndpointCpRT =
    periodType === PERIOD_DAILY || periodType === PERIOD_WEEKLY
      ? 'getacgraphninv'
      : 'getacgraphmonyrlyninv';
  const requestApiArray = [
    call(
      ApiService.post,
      apiEndpoint,
      getRequestBodyByPeriodAndType({
        email,
        thingName,
        periodType,
        staType: TYPE_TEMP,
        targetDate,
      })
    ),
    call(
      ApiService.post,
      apiEndpoint,
      getRequestBodyByPeriodAndType({
        email,
        thingName,
        periodType,
        staType: TYPE_ENERGY,
        targetDate,
      })
    ),
  ];

  const nonInverterRequestApi = call(
    ApiService.post,
    apiEndpointCpRT,
    getRequestBodyByPeriodAndType({
      email,
      thingName,
      periodType,
      staType: TYPE_TEMP,
      targetDate,
    })
  );

  if (isInverter) {
    requestApiArray.push(nonInverterRequestApi);
  }

  const [tempData, energyData, getCrtData] = yield all(requestApiArray);
  const crtData = getCrtData || [];
  const keyValueArray = (key, val, ar) =>
    !!ar.filter(item => item[key] === val).length;
  const objIndexKeyValue = (key, val, ar) => ar.findIndex(e => e[key] === val);

  const energyTempData = [energyData, tempData, crtData];
  const finalData = [];

  energyTempData.forEach(mergedArray => {
    mergedArray.forEach(curArray => {
      if (!keyValueArray('updatedOn', curArray.updatedOn, finalData)) {
        finalData.push({
          updatedOn: '',
          kWh: '0.0000',
          displaykWh: parseFloat(curArray.kWh || '0.0000', 10) * 10,
          Set_OnOff: 0,
          Set_Temp: '0',
          Stat_IDRoomTemp: '0',
          isPopulate: -1,
          cpRT: '0.00',
          ...curArray,
        });
      } else {
        const refInd = objIndexKeyValue(
          'updatedOn',
          curArray.updatedOn,
          finalData
        );
        finalData[refInd] = { ...finalData[refInd], ...curArray };
      }
    });
  });

  finalData.sort(
    (a, b) => parseInt(a.updatedOn, 10) - parseInt(b.updatedOn, 10)
  );

  return finalData;
}

function mapTempResponse(response, periodMapper, tnbRate) {
  const electricRateValue = Number(response.kWh) * tnbRate;
  return {
    updatedOn: periodMapper(response.updatedOn),
    setTemp: Number(response.Set_Temp),
    indoorTemp: Number(response.Stat_IDRoomTemp),
    SetOnOff: Number(response.Set_OnOff),
    kWh: +Number(response.kWh).toFixed(1),
    displaykWh: +Number(response.displaykWh).toFixed(1),
    electricRate: +Number(electricRateValue).toFixed(2),
    isPopulate: Number(response.isPopulate),
    cpRT: Number(response.cpRT),
  };
}

function getMinMaxPeriod(periodType, date) {
  switch (periodType) {
    case PERIOD_DAILY:
      return {
        start: 0,
        end: 23,
      };
    case PERIOD_WEEKLY:
      return {
        start: 1,
        end: 7,
      };
    case PERIOD_MONTHLY:
      return {
        start: 1,
        end: Number(moment(date).endOf('months').format('DD')),
      };
    case PERIOD_YEARLY:
      return {
        start: 1,
        end: 12,
      };
    default: {
      return {};
    }
  }
}

function getPeriodKey(periodType, value) {
  switch (periodType) {
    case PERIOD_DAILY:
      return moment().hour(value).format('ha');
    case PERIOD_WEEKLY:
      return moment().isoWeekday(value).format('ddd');
    case PERIOD_MONTHLY:
      return value.toString();
    case PERIOD_YEARLY:
      return moment(value.toString(), 'MM').format('MMM');
    default: {
      return value.toString();
    }
  }
}

function initPeriodMap(periodType, targetDate) {
  let periodMap = {};
  const { start, end } = getMinMaxPeriod(periodType, moment(targetDate));

  for (let i = start; i <= end; i += 1) {
    const periodKey = getPeriodKey(periodType, i);
    periodMap = {
      ...periodMap,
      [periodKey]: {
        updatedOn: periodKey,
        ...{
          setTemp: 0,
          indoorTemp: 0,
          SetOnOff: 0,
          kWh: 0,
          displaykWh: 0,
          cpRT: 0,
          electricRate: 0,
          isPopulate: -1,
        },
      },
    };
  }

  periodMap = {
    ...periodMap,
    xtra: {
      updatedOn: ' ',
      ...{
        setTemp: 0,
        indoorTemp: 0,
        SetOnOff: 0,
        kWh: 0,
        displaykWh: 0,
        cpRT: 0,
        electricRate: 0,
        isPopulate: -1,
      },
    },
  };
  return periodMap;
}

function processAPIData({
  data,
  displayValueKey,
  displayAverageKwhKey,
  displayCpRTKey,
  periodType,
  responseMapper,
  targetDate,
  electricRate,
}) {
  let displayValue = 0;
  let availableValue = 0;
  let displayAverageKwh = 0;
  let displayCpRT = 0;

  const tnbRate = electricRate === undefined ? 0.218 : electricRate;

  // Init the data based on the period Type
  const periodMap = initPeriodMap(periodType, targetDate);

  data.forEach(record => {
    if (Number(record[displayValueKey]) > 0) {
      const value = Number(record[displayValueKey]);
      displayValue += value;
      availableValue += 1;
    }

    if (Number(record[displayAverageKwhKey]) > 0) {
      const valuekWh = Number(record[displayAverageKwhKey]);
      displayAverageKwh += valuekWh;
    }
    if (Number(record[displayCpRTKey]) > 0) {
      const valuecpRT = Number(record[displayCpRTKey]);
      displayCpRT += valuecpRT;
    }

    const processed = responseMapper(
      record,
      getPeriodMapper(periodType),
      tnbRate
    );

    const { updatedOn } = processed;

    if (periodMap[updatedOn]) {
      periodMap[updatedOn] = {
        ...processed,
      };
    }
  });

  const totalTnbRate = displayAverageKwh * tnbRate;

  return {
    displayValue:
      displayValue > 0
        ? parseInt(Math.round(displayValue / availableValue), 10).toString()
        : '-',
    displayAverageKwh: displayAverageKwh.toFixed(1),
    displayCpRT: displayCpRT.toFixed(2),
    displayTnbRate: totalTnbRate.toFixed(2),
    data: Object.values(periodMap),
  };
}

export default function* fetchUsageData(action) {
  try {
    yield put(clearUsageStatus());
    yield put(setLoading(true));

    const {
      periodType,
      thingName,
      isCompare = false,
      targetDate = moment().format(STANDARD_DATE_FORMAT),
    } = action.fetchUsageParams;
    const allUnit = yield select(state => state.units.allUnits);
    const currentUnit = allUnit[thingName];
    const {isInverter} = allUnit[thingName];
    const { email } = yield select(state => state.user.profile);
    const electricRate = yield select(state => state.usage.curElectricRate);
    const usagePeriod = yield select(state => state.usage.usageByPeriod);
    const comparedDate =
      usagePeriod[thingName] !== undefined &&
      usagePeriod[thingName][periodType] !== undefined
        ? usagePeriod[thingName][periodType].compareDate
        : null;

    const getDataArray = [
      call(fetchGraphData, {
        unit: currentUnit,
        email,
        periodType,
        targetDate,
        isInverter,
      }),
      null,
    ];

    if (isCompare || comparedDate) {
      getDataArray[1] = call(fetchGraphData, {
        unit: currentUnit,
        email,
        periodType,
        targetDate: isCompare ? targetDate : comparedDate,
        isInverter,
      });
    }

    const [usageResponse, comparedUsageResponse] = yield all(getDataArray);

    const getProcessData = [
      call(processAPIData, {
        data: usageResponse,
        responseMapper: mapTempResponse,
        displayValueKey: 'Stat_IDRoomTemp',
        displayAverageKwhKey: 'kWh',
        displayCpRTKey: 'cpRT',
        isAverage: STA_TEMP,
        periodType,
        targetDate,
        electricRate,
      }),
      null,
    ];

    if (isCompare || comparedDate) {
      getProcessData[1] = call(processAPIData, {
        data: comparedUsageResponse,
        responseMapper: mapTempResponse,
        displayValueKey: 'Stat_IDRoomTemp',
        displayAverageKwhKey: 'kWh',
        displayCpRTKey: 'cpRT',
        isAverage: STA_TEMP,
        periodType,
        targetDate: isCompare ? targetDate : comparedDate,
        electricRate,
      });
    }
    const [usageData, usageComparedData] = yield all(getProcessData);

    if (comparedDate) {
      yield put(
        setCompareUsageData({
          periodType,
          thingName,
          data: usageComparedData.data,
          targetDate: comparedDate,
        })
      );
    }
    if (isCompare) {
      yield put(
        setCompareUsageData({
          periodType,
          thingName,
          data: usageData.data,
          targetDate,
        })
      );
    } else {
      yield put(
        setUsageData({
          periodType,
          thingName,
          data: usageData,
        })
      );
      yield put(
        setToggleButton({
          periodType,
          thingName,
        })
      );
    }

    yield put(
      setUsageStatus({
        messageType: 'success',
        message: 'success',
      })
    );
  } catch (error) {
    yield put(
      setUsageStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setLoading(false));
  }
}
