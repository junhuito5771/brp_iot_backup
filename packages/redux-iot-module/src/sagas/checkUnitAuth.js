import { eventChannel } from 'redux-saga';
import {
  select,
  call,
  fork,
  cancel,
  take,
  join,
  put,
} from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import PubSubHelper from '../utils/pubSubHelper';

import { CANCEL_CHECK_UNIT_AUTH, setRemoteStatus } from '../actions/remote';
import { getAllUnits } from '../actions/units';

function* fetchPairedUnits(user) {
  const responseData = yield call(ApiService.post, 'getpaireddevices', {
    requestData: {
      type: 1,
      value: user,
    },
  });

  return responseData && responseData.device ? responseData.device : [];
}

function* unbindTopicWatcher(unbindTopicChannel) {
  while (true) {
    const unbindTopicResponse = yield take(unbindTopicChannel);
    const message = unbindTopicResponse.value;

    if (message && message.unbindedGuest) {
      return message.unbindedGuest;
    }
  }
}

function* validateRemoteTask(action) {
  let unbindTopicWatchTask;
  let unbindTopicChannel;
  let needFetchUnits = false;
  let userEmail;
  try {
    const thingName = yield select(state => state.remote.thingName);
    userEmail = yield select(state => state.user.profile.email);
    const unitList = yield call(fetchPairedUnits, userEmail);
    if (
      unitList &&
      unitList.findIndex(
        curUnit =>
          curUnit.thingName &&
          curUnit.thingName.slice(0, curUnit.thingName.lastIndexOf('=')) ===
            thingName
      ) === -1
    ) {
      needFetchUnits = true;
      action.unitAuthParams.onUnbind();
      throw new Error('Unauthorized access');
    }
    // Fork a task to subscribe and watch the topic
    const topic = `Daikin/IoT/event/${thingName}/unbindDevice/get/accepted/`;
    unbindTopicChannel = eventChannel(emit => {
      PubSubHelper.subscribeWithEmit({ topic, emit });
      return () => PubSubHelper.unsubscribe({ topic });
    });
    unbindTopicWatchTask = yield fork(
      unbindTopicWatcher,
      unbindTopicChannel,
      userEmail,
      topic
    );
    const unbindedGuestList = yield join(unbindTopicWatchTask);
    if (unbindTopicWatchTask) yield cancel(unbindTopicWatchTask);

    if (
      unbindedGuestList &&
      unbindedGuestList.findIndex(
        unbindedEmail => userEmail === unbindedEmail
      ) > -1
    ) {
      needFetchUnits = true;
      throw new Error('Unauthorized access');
    }
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    if (needFetchUnits) yield put(getAllUnits({ email: userEmail }));
    if (unbindTopicChannel) unbindTopicChannel.close();
    if (unbindTopicWatchTask) yield cancel(unbindTopicWatchTask);
  }
}

export default function* validateRemoteFlow(action) {
  let authRemoteTask;
  try {
    authRemoteTask = yield fork(validateRemoteTask, action);

    yield take(CANCEL_CHECK_UNIT_AUTH);
    if (authRemoteTask) yield cancel(authRemoteTask);
  } finally {
    if (authRemoteTask) yield cancel(authRemoteTask);
  }
}
