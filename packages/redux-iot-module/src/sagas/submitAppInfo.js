import { call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

export default function* submitAppInfo(action) {
  try {
    const requestData = {
      username: action.appInfoParams.username,
      IMEI: action.appInfoParams.imei,
      Token: action.appInfoParams.token,
    };

    yield call(ApiService.post, 'insertappinfo', {
      requestData,
    });
  } catch (error) {
    // Empty catch block since we need to keep it going regardless got error or not
  }
}
