import { call, select, put } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  SET_CONTROL_STATUS,
  setUnitUser,
  SET_USER_CONTROL_LOADING,
  clearUserControlStatus,
} from '../actions/unitUserControl';

export default function* setUserControl(action) {
  const { thingName } = yield select(state => state.remote);
  const { unitUser } = yield select(state => state.unitUserControl);
  const { email } = yield select(state => state.user.profile);
  const isShareUnit = action.controlType.shareUnit;
  const isOverwriteSchedule = action.controlType.overwriteSchedule;
  const isExpiryTime = action.controlType.expiryTime;
  const shareUnit =
    isShareUnit !== undefined ? isShareUnit : unitUser.shareUnit;
  const overwriteSchedule =
    isOverwriteSchedule !== undefined
      ? isOverwriteSchedule
      : unitUser.overwriteSchedule;
  const expiryTime =
    isExpiryTime !== undefined ? isExpiryTime : unitUser.expiryTime;

  const setLoading = () => {
    if (isShareUnit !== undefined && isOverwriteSchedule === undefined) {
      return 'shareUnit';
    } if (isShareUnit === undefined && isOverwriteSchedule !== undefined) {
      return 'overwriteSchedule';
    }
    return 'usageLimit';
  };

  yield put(clearUserControlStatus());

  yield put({
    type: SET_USER_CONTROL_LOADING,
    payload: {
      controlType: setLoading(),
      isLoading: true,
    },
  });

  const data = {
    username: unitUser.email,
    hostUser: email,
    type: 2,
    thingName,
    config: [
      {
        shareUnit,
        overwriteSchedule,
        qx: parseInt(unitUser.qx, 10),
        expiryTime,
      },
    ],
  };

  yield put(
    setUnitUser({
      ...unitUser,
      shareUnit,
      overwriteSchedule,
      expiryTime,
    })
  );

  try {
    const response = yield call(ApiService.post, 'usercontrolfunction', {
      requestData: data,
    });

    if (response.message === 'success') {
      yield put({
        type: SET_CONTROL_STATUS,
        payload: {
          messageType: 'success',
          message: response.message,
        },
      });
    } else {
      yield put(setUnitUser(unitUser));

      yield put({
        type: SET_CONTROL_STATUS,
        payload: {
          messageType: 'error',
          message: response.message,
        },
      });
    }
  } catch (error) {
    yield put({
      type: SET_CONTROL_STATUS,
      payload: {
        messageType: 'error',
        message: error.message,
      },
    });
  }

  yield put({
    type: SET_USER_CONTROL_LOADING,
    payload: {
      controlType: setLoading(),
      isLoading: false,
    },
  });

  yield put(clearUserControlStatus());
}
