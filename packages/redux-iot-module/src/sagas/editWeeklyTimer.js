import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import produce from 'immer';

import ShadowValueHelper from '../utils/Remote/shadowValueHelper';
import { RemoteHelper } from '../utils/Remote';

import { setUnitTimers } from '../actions/timer';
import { setEditTimerLoading, setEditTimerStatus } from '../actions/editTimer';
import { timerInfoMapper, generateTimeBarInstance } from './fetchTimer';

const { getShadowObjByFanLvl, getShadowValByMode } = ShadowValueHelper;

const CREATE_WEEKLY_TIMER = 2;
const UPDATE_DELETE_WEEKLY_TIMER = 3;

function* convertRequestBodyToTimerInfo(isNew, thingName, timerArray) {
  const timers = [];

  const {
    timer: { timerList: exTimers },
  } = yield select(state => state);

  if (isNew) {
    (exTimers[thingName] || []).forEach(timer => timers.push({ ...timer }));
  }

  timerArray.forEach(userTimers => {
    Object.keys(userTimers).forEach(timerUser => {
      userTimers[timerUser].forEach(timerInfo => {
        timers.push(
          timerInfoMapper({
            timerInfo,
            timerUser,
          })
        );
      });
    });
  });

  // Sorting the timer data based on day, hour, minute
  let timerListAsc = timers.sort(
    (curTimer, nextTimer) =>
      curTimer.day - nextTimer.day ||
      curTimer.hour - nextTimer.hour ||
      curTimer.minute - nextTimer.minute
  );

  timerListAsc = timerListAsc.map((cur, index) => ({
    ...cur,
    timerIndex: index,
  }));

  return timerListAsc;
}

// Convert the request body to timer info and
// generate timer pair instance, update both to state
export function* saveTimerRequestToState(isNew, thingName, timerArray) {
  const timerListByThingName = yield call(
    convertRequestBodyToTimerInfo,
    isNew,
    thingName,
    timerArray
  );

  const timerByThingName = {
    [thingName]: timerListByThingName,
  };

  const timerPairsByThingName = generateTimeBarInstance(timerByThingName);

  yield put(
    setUnitTimers({
      thingName,
      timers: timerByThingName[thingName],
      timerPairs: timerPairsByThingName[thingName],
    })
  );
}

export function mapTimerInfoToResponse(timerInfo) {
  const onTimerConfig = {
    Set_Mode: getShadowValByMode(timerInfo.mode), // Need helper to convert to number,
    ...(timerInfo.silent === 0 && {
      Set_Fan: getShadowObjByFanLvl(
        RemoteHelper.HANDSET_DAMA,
        timerInfo.fanLevel.toLowerCase()
      ).fan,
    }),
    ...(timerInfo.silent === 1 && { Set_Silent: Number(timerInfo.silent) }),
    ...((timerInfo.mode === 'auto' ||
      timerInfo.mode === 'cool' ||
      timerInfo.mode === 'heat') && { Set_Temp: timerInfo.temp }),
  };
  const curTimerConfig = {
    Flag: timerInfo.isActive === undefined ? 1 : Number(timerInfo.isActive),
    Day: timerInfo.day,
    Hour: timerInfo.hour,
    Minute: timerInfo.minute,
    Set_OnOff: timerInfo.switchValue,
    ...(timerInfo.switchValue === 1 && onTimerConfig),
  };

  return curTimerConfig;
}

function* getCreateTimerBody() {
  const { timerList, days } = yield select(state => state.editTimer);
  const timerByUsers = {};

  timerList.forEach(timerInfo => {
    const { user } = timerInfo;

    if (!timerByUsers[user]) {
      timerByUsers[user] = [];
    }

    days.forEach((isDayActive, index) => {
      if (isDayActive) {
        const curTimerConfig = mapTimerInfoToResponse({
          ...timerInfo,
          day: index,
        });

        timerByUsers[user].push(curTimerConfig);
      }
    });
  });

  return timerByUsers;
}

function* getUpdateDeleteTimerBody() {
  const timers = yield select(state => state.timer.timerList);
  const { timerList, thingName, achieveTimerList, days } = yield select(
    state => state.editTimer
  );

  const dayNr = days.findIndex(isActiveDay => isActiveDay);
  const unitTimer = timers[thingName];
  const updatedUnitTimer = produce(unitTimer, draftState => {
    timerList.forEach(timerInfo => {
      draftState[timerInfo.timerIndex] = {
        ...timerInfo,
        day: dayNr,
      };
    });
  });

  const timerByUsers = {};

  updatedUnitTimer.forEach(timerInfo => {
    // Push the timer that is not the current selected days
    const isRemained =
      achieveTimerList.findIndex(
        achieveIndex => timerInfo.timerIndex === achieveIndex
      ) === -1;

    if (!timerByUsers[timerInfo.user]) {
      timerByUsers[timerInfo.user] = [];
    }

    if (isRemained) {
      const curTimerConfig = mapTimerInfoToResponse(timerInfo);

      timerByUsers[timerInfo.user].push(curTimerConfig);
    }
  });

  return timerByUsers;
}

export default function* editWeeklyTimer() {
  try {
    yield put(setEditTimerLoading(true));
    const {
      user: {
        profile: { email },
      },
      editTimer: { isNew, thingName },
      units: { allUnits },
    } = yield select(state => state);

    const timerInArray = isNew
      ? yield call(getCreateTimerBody)
      : yield call(getUpdateDeleteTimerBody);

    const unit = allUnits[thingName];

    // If the edited units is supported overwrite schedule
    // only return timers belong to the current user
    const timerInArrayData =
      !isNew && unit.overwriteSchedule === 0
        ? { [email]: timerInArray[email] }
        : timerInArray;

    const response = yield call(ApiService.post, 'timerfunction', {
      requestData: {
        type: isNew ? CREATE_WEEKLY_TIMER : UPDATE_DELETE_WEEKLY_TIMER,
        username: email,
        thingName,
        timerData: [timerInArrayData],
      },
    });

    if (response && response.message === 'success') {
      yield call(saveTimerRequestToState, isNew, thingName, [timerInArray]);

      yield put(
        setEditTimerStatus({
          messageType: 'success',
          message: 'success',
        })
      );
    }
  } catch (error) {
    yield put(
      setEditTimerStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setEditTimerLoading(false));
  }
}
