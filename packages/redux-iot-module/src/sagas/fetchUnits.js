import { call, put, select, fork, takeLatest } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setAllUnits,
  setGroups,
  setGroupUnits,
  setUnitsLoading,
  setUnitStatus,
  clearUnitsStatus,
  setNotifiedError,
  GET_ALL_UNITS,
} from '../actions/units';
import { initPreferredUnits } from './alexa/preferredUnits';
import {
  setAlexaTokenStatus,
  clearAlexaTokenStatus,
} from '../actions/preferredUnits';

import fetchTimer from './fetchTimer';
import fetchPlans from './fetchPlans';

import { getLatestFirmwareVersion } from './updateFirmware';
import shadowStateMapper from '../utils/shadowStateMapper';

export function* fetchUnits(action) {
  const responseData = yield call(ApiService.post, 'gethomepageinfo', {
    requestData: {
      type: 1,
      value: action.email,
    },
  });

  let fetchUnitsResult;

  if (responseData) {
    fetchUnitsResult = {
      isAlexaLinked: responseData.isAlexaLinked === 1,
    };

    if (responseData.data && responseData.data.length > 0) {
      fetchUnitsResult = {
        ...fetchUnitsResult,
        units: {},
        groupUnits: {},
        groups: {},
      };

      const data = responseData.data.sort(
        (a, b) => a.groupIndex - b.groupIndex || a.unitIndex - b.unitIndex
      );

      let groupIndex = 0;

      data.forEach(cur => {
        const curThingName = cur.ThingName;
        const curGroupIndex = cur.groupIndex;

        // units
        fetchUnitsResult.units = {
          ...fetchUnitsResult.units,
          [curThingName]: cur,
        };

        if (!fetchUnitsResult.groupUnits[curGroupIndex]) {
          fetchUnitsResult.groups = {
            ...fetchUnitsResult.groups,
            [groupIndex]: {
              groupIndex: curGroupIndex,
              groupName: cur.ACGroup,
            },
          };
          groupIndex += 1;
        }

        fetchUnitsResult.groupUnits = {
          ...fetchUnitsResult.groupUnits,
          [curGroupIndex]: [
            ...(fetchUnitsResult.groupUnits[curGroupIndex] || []),
            curThingName,
          ],
        };
      });
    }
  }

  return fetchUnitsResult;
}

function* processUnitShadowState(units, email) {
  const unitResult = {};
  let plansList = [];
  const unitKeys = Object.keys(units);

  const curFirmwareVer = yield call(getLatestFirmwareVersion, email);

  unitKeys.forEach(thingName => {
    const curUnit = units[thingName];
    const { shadowState, ...restState } = curUnit;

    // get all plans id each unit
    plansList.push(restState.planID);

    const needUpgrade =
      curFirmwareVer &&
      shadowState.version &&
      curFirmwareVer !== shadowState.version
        ? `Latest version ${curFirmwareVer} is available to update.`
        : '';

    // Process shadow state;
    unitResult[thingName] = {
      ...restState,
      ...shadowStateMapper(shadowState, {}),
      needUpgrade,
    };
  });

  // remove duplicate planID
  plansList = [...new Set(plansList)];

  // fetch all plans
  const allUnitsData = yield call(fetchPlans, { email, plansList, unitResult });

  return allUnitsData;
}

function* fetchUnitsTask(action) {
  try {
    let units = {};
    let groups = {};
    let groupUnits = {};
    yield put(clearUnitsStatus());
    yield put(clearAlexaTokenStatus());
    yield put(setUnitsLoading(true));

    const fetchUnitsResult = yield call(fetchUnits, action.fetchUnitsParams);

    if (fetchUnitsResult && fetchUnitsResult.isAlexaLinked) {
      yield put(setAlexaTokenStatus({ alexaLinkStatus: 'activateSuccess' }));
    }

    if (fetchUnitsResult && fetchUnitsResult.units) {

      units = yield call(
        processUnitShadowState,
        fetchUnitsResult.units,
        action.fetchUnitsParams.email
      );
      groups = fetchUnitsResult.groups;
      groupUnits = fetchUnitsResult.groupUnits;

      // populate the group switch
      Object.keys(groups).forEach(groupKey => {
        const { groupIndex } = groups[groupKey];
        const unitsInGroup = groupUnits[groupIndex];

        // The group is consider connected if at least one of the unit is connected
        const isConnected = unitsInGroup.some(
          thingName => units[thingName].status === 'connected'
        );
        // The switch value of the group will ignore the units that is diconnected
        const switchValue =
          isConnected &&
          unitsInGroup.every(
            thingName =>
              units[thingName].status === 'disconnected' ||
              units[thingName].switch === 1
          );

        groups[groupKey].switch = switchValue;
        groups[groupKey].isConnected = isConnected;
      });

      const { hasNotifiedError } = yield select(s => s.units);
      const res = {};
      Object.values(units).forEach(obj => {
        if (obj.errorCode > 0) {
          res[obj.thingName] = false;
        }
      });

      if (
        JSON.stringify(Object.keys(res)) !==
        JSON.stringify(Object.keys(hasNotifiedError))
      ) {
        yield put(setNotifiedError(res));
      }
    }
    yield put(
      setGroups({
        groups,
        lastGroupIndex: Math.max(
          ...Object.values(groups).map(group => group.groupIndex),
          0
        ),
      })
    );
    yield put(setGroupUnits(groupUnits));

    console.log("redux-iot-module/src/sagas/fetchUnits.js");
    console.log(units);

    yield put(setAllUnits(units, new Date().getTime()));

    yield fork(initPreferredUnits, units);
  } catch (error) {
    yield put(
      setUnitStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setUnitsLoading(false));
  }
}

export function* fetchUnitsAndTimersTask(action) {
  yield fork(fetchUnitsTask, action);
  yield fork(fetchTimer, action);
}

export default function* rootSaga() {
  yield takeLatest(GET_ALL_UNITS, fetchUnitsAndTimersTask);
}
