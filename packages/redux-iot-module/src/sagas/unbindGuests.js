import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setUnbindGuestLoading,
  setUnbindDeviceStatus,
  clearUnbindDeviceStatus,
} from '../actions/unbindDevice';

import fetchUnitUsers from './fetchUnitUsers';

const UNBIND_GUESTS = 2;

function* getGuestUserList(checkList, isRemoveAllGuest) {
  const unitUserList = yield select(state => state.unitUsers.unitUserList);

  const guestUsersList = unitUserList[1] && unitUserList[1].data;

  const guestList = [];

  guestUsersList.forEach((guestUser, index) => {
    if (isRemoveAllGuest || checkList[index]) {
      guestList.push(guestUser.email);
    }
  });

  return guestList;
}
export default function* removeGuests(action) {
  try {
    yield put(clearUnbindDeviceStatus());
    yield put(setUnbindGuestLoading(true));

    const email = yield select(state => state.user.profile.email);
    const {thingName} = action.removeGuestsParams;
    const isRemoveAllGuest =
      action.removeGuestsParams.isRemoveAllGuest || false;

    const guestList = yield call(
      getGuestUserList,
      action.removeGuestsParams.checklist,
      isRemoveAllGuest
    );

    if (guestList && guestList.length > 0) {
      const responseData = yield call(ApiService.post, 'unbinddevice', {
        requestData: {
          type: UNBIND_GUESTS,
          email,
          thingName,
          guest: guestList,
        },
      });
      if (responseData && responseData.code) {
        throw new Error(' Error occured while unbind guest user');
      } else {
        // Refresh the unit users
        yield call(fetchUnitUsers);
        yield put(
          setUnbindDeviceStatus({
            messageType: 'guestSuccess',
            message: 'Successfully unbind the guest.',
          })
        );
      }
    } else {
      throw new Error('Please select at least 1 guest user to unbind.');
    }
  } catch (error) {
    yield put(
      setUnbindDeviceStatus({
        messageType: 'guestError',
        message: error.message,
      })
    );
  } finally {
    yield put(setUnbindGuestLoading(false));
  }
}
