import { eventChannel } from 'redux-saga';
import { call, put, take, fork, join, race, delay } from 'redux-saga/effects';
import BLEService from '../utils/BLE/BLEService';

import {
  setScanSSIDLoading,
  setScanSSIDStatus,
  setNearbySSID,
  clearScanSSIDStatus,
  CANCEL_SCAN_SSID,
} from '../actions/scanSSID';

function convertToJSON(text) {
  try {
    return JSON.parse(text);
  } catch (error) {
    return null;
  }
}

function* listenBLEChannel(channel) {
  try {
    let response = '';

    while (true) {
      const data = yield take(channel);

      if (data.startsWith('<{')) {
        response = '';
      }

      response = response.concat(data);

      if (response.length > 0 && response.endsWith('}>')) {
        // Process the response by replacing the whitespace, remove < and >
        response = response.replace(/\s(?=(?:"[^"]*"|[^"])*$)|<|>/g, '');

        const responseInObj = convertToJSON(response);

        if (responseInObj && responseInObj.result) {
          return responseInObj.result;
        }
        response = '';
      }
    }
  } catch (error) {
    yield put(
      setScanSSIDStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  }

  return null;
}

function* scanSSID() {
  let channel;
  let task;
  yield call(BLEService.scan);
  const units = yield call(BLEService.getDiscoveredUnits);

  // 1. Only filter to get the units with name: Daikin
  const selectedUnits = units.filter(unit => unit.name === 'Daikin');
  const sortedUnits = selectedUnits.sort(unit => unit.rssi);
  const notifyID = '2bb1';

  // 2. Find the highest RSSI
  for (let i = 0; i < sortedUnits.length; i += 1) {
    const curUnit = sortedUnits[i];
    const connectionID = curUnit.id;

    yield call(BLEService.connectAndRetrieveService, curUnit);

    // Create process ble response channel
    channel = eventChannel(emit => {
      BLEService.addNotificationListerner(emit);

      return () => BLEService.stopNotification({ id: connectionID }, notifyID);
    });

    task = yield fork(listenBLEChannel, channel);

    yield call(BLEService.sendMessage, {
      id: connectionID,
      message: '<{"action":"getWifiScanResult","version":-1}>',
    });

    BLEService.startNotification({ id: connectionID }, notifyID);

    const { ssidList, cancel } = yield race({
      ssidList: join(task),
      timeout: delay(3000),
      cancel: take(CANCEL_SCAN_SSID),
    });

    // Disconnect the current connected connection
    BLEService.disconnect({ id: connectionID });

    if (cancel) {
      return;
    }
    if (ssidList) {
      const sortedSSIDList = ssidList.sort(ssidUnit => ssidUnit.ssid);
      yield put(setNearbySSID(sortedSSIDList));

      return;
    }
  }
}

export default function* scanSSIDTask() {
  try {
    yield put(clearScanSSIDStatus());
    yield put(setScanSSIDLoading(true));

    const { timeout } = yield race({
      scanSSIDTask: call(scanSSID),
      timeout: delay(30000),
    });

    if (timeout) {
      throw new Error('Timeout while retrying to get the nearby SSID');
    }
  } catch (error) {
    yield put(
      setScanSSIDStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setScanSSIDLoading(false));
  }
}
