import { put, select, call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import Configuration from '../../configuration';
import { setAlexaToken } from '../../actions/preferredUnits';

export default function* refreshAlexaToken() {
  const {
    preferredUnits: {
      alexaToken: { refreshToken },
    },
  } = yield select(state => state);

  const {
    alexaClientId,
    alexaClientSecret,
    alexaTokenUrl,
  } = Configuration.getAlexaConfig();

  const requestBody = {
    grant_type: 'refresh_token',
    refresh_token: refreshToken,
    client_id: alexaClientId,
    client_secret: alexaClientSecret,
  };

  const response = yield call(
    ApiService.postWithoutAuthCustom,
    `https://${alexaTokenUrl}`,
    requestBody
  );

  if (response && response.data) {
    const {
      access_token: accessToken,
      refresh_token: refreshToken,
      expires_in: expiresIn,
      token_type: tokenType,
    } = response.data;
    yield put(
      setAlexaToken({ accessToken, refreshToken, expiresIn, tokenType })
    );
  }
}
