import { put, select, call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setPrefferedUnits,
  setLoading,
  setPreferredStatus,
} from '../../actions/preferredUnits';

export function* initPreferredUnits(units) {
  let prefferedUnits = {};
  Object.keys(units).forEach(thingName => {
    prefferedUnits = {
      ...prefferedUnits,
      [thingName]: {
        thingName,
        unitName: units[thingName].ACName,
        isAlexaPreferred: units[thingName].isPreferredDevice === 1,
        logo: units[thingName].Logo,
      },
    };
  });

  yield put(setPrefferedUnits(prefferedUnits));
}

export default function* submitPrefferedUnits() {
  try {
    yield put(setLoading(true));

    const {
      preferredUnits: { units },
      user: {
        profile: { email },
      },
    } = yield select(state => state);

    const selectedUnitList = [];

    Object.keys(units).forEach(curThingName => {
      if (units[curThingName] && units[curThingName].isAlexaPreferred) {
        selectedUnitList.push(curThingName);
      }
    });

    const responseData = yield call(ApiService.post, 'linkwithalexa', {
      requestData: {
        value: email,
        thingName: selectedUnitList,
      },
    });

    if (responseData && responseData.message === 'success') {
      yield put(
        setPreferredStatus({
          messageType: 'success',
          message:
            "You've successfully set your units as preferred units in Alexa",
        })
      );
    } else {
      yield put(
        setPreferredStatus({
          messageType: 'error',
          message: 'Fail to set your units as preferred units in Alexa',
        })
      );
    }
  } catch (error) {
    yield put(
      setPreferredStatus({ messageType: 'error', message: error.message })
    );
  } finally {
    yield put(setLoading(false));
  }
}
