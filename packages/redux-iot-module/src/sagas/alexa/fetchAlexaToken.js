import { put, call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setLoading,
  setAlexaToken,
  setAlexaTokenStatus,
} from '../../actions/preferredUnits';

export default function* fetchAlexaToken(action) {
  try {
    yield put(setLoading(true));
    yield put(setAlexaTokenStatus({ alexaLinkStatus: '' }));

    const {
      fetchTokenParams: { code, clientId, clientSecret, tokenUrl, redirectUrl },
    } = action;

    const requestBody = {
      code,
      grant_type: 'authorization_code',
      client_id: clientId,
      client_secret: clientSecret,
      redirect_uri: `https://${redirectUrl}`,
    };

    const response = yield call(
      ApiService.postWithoutAuthCustom,
      `https://${tokenUrl}`,
      requestBody
    );

    if (response && response.data && response.data.access_token) {
      const {
        access_token: accessToken,
        refresh_token: refreshToken,
        expires_in: expiresIn,
        token_type: tokenType,
      } = response.data;
      yield put(
        setAlexaToken({ accessToken, refreshToken, expiresIn, tokenType })
      );

      yield put(setAlexaTokenStatus({ alexaLinkStatus: 'tokenSuccess' }));
    }
  } catch (error) {
    yield put(setAlexaTokenStatus({ alexaLinkStatus: 'tokenError' }));
  } finally {
    yield put(setLoading(false));
  }
}
