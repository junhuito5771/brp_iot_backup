import { put, call, select } from 'redux-saga/effects';
import { marketHelper } from '@module/redux-marketing';
import moment from 'moment';
import { API_TIME_FORMAT } from '@module/utility';

import { setLoading, setAlexaCommandUrl } from '../../actions/preferredUnits';

export default function* fetchCommandUrl(action) {
  try {
    yield put(setLoading(true));

    const { isDaikin = true } = action;

    const promoCatType = isDaikin
      ? marketHelper.cmsTypeEnum.PromotionCategory
      : marketHelper.cmsTypeEnum.PromotionCategory_Acson;

    const promoLinkType = isDaikin
      ? marketHelper.cmsTypeEnum.PromotionLink
      : marketHelper.cmsTypeEnum.PromotionLink_Acson;

    yield call(marketHelper.checkLastModified, promoLinkType);

    const {
      content: { lastModified },
      preferredUnits: {
        alexaCommand: { lastUpdateDT },
      },
    } = yield select(state => state);

    const dbModified = lastModified[promoLinkType];

    const isExpired =
      !lastUpdateDT ||
      moment(dbModified, API_TIME_FORMAT).isAfter(
        moment(lastUpdateDT, API_TIME_FORMAT)
      );

    if (isExpired) {
      const promoCatRecords = yield call(
        marketHelper.getLatestRecord,
        promoCatType
      );

      const promoCatKey = isDaikin ? 'AlexaDaikin' : 'AlexaAcson';

      const { Oid: alexaCommandId } = promoCatRecords.find(
        ({ Name }) => Name === promoCatKey
      );

      const promoLinkRecord = yield call(
        marketHelper.getLatestRecord,
        promoLinkType
      );

      const promoLinkKey = isDaikin
        ? 'CMS_PromotionCategory'
        : 'CMS_AcsonPromotionCategory';

      const targertRecord = promoLinkRecord.find(
        ({ [promoLinkKey]: { Oid } }) => Oid === alexaCommandId
      );

      yield put(
        setAlexaCommandUrl({
          title: targertRecord.Title,
          url: targertRecord.Hyperlink,
          lastUpdateDT: dbModified,
          needRefresh: true,
        })
      );
    }
  } catch (error) {
    // Handle error
  } finally {
    yield put(setLoading(false));
  }
}
