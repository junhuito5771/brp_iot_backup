import { put, select, call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import Configuration from '../../configuration';
import { setLoading, setAlexaTokenStatus } from '../../actions/preferredUnits';

import refreshToken from './refreshToken';

export default function* fetchActivateStatus() {
  try {
    yield put(setLoading(true));
    yield put(setAlexaTokenStatus({ alexaLinkStatus: '' }));

    yield call(refreshToken);

    const { alexaSkillEnaUrl } = Configuration.getAlexaConfig();

    const authToken = yield select(
      state => state.preferredUnits.alexaToken.accessToken
    );

    const response = yield call(
      ApiService.getWithAuth,
      `https://${alexaSkillEnaUrl}`,
      { Authorization: `Bearer ${authToken}` }
    );

    if (
      response &&
      response.status === 'ENABLED' &&
      response.accountLink.status === 'LINKED'
    ) {
      yield put(setAlexaTokenStatus({ alexaLinkStatus: 'activateSuccess' }));
    }
  } catch (error) {
    yield put(setAlexaTokenStatus({ alexaLinkStatus: 'activateError' }));
  } finally {
    yield put(setLoading(false));
  }
}
