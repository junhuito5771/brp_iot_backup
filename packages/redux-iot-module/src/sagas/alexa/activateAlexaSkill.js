import { put, select, call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import { setLoading, setAlexaTokenStatus } from '../../actions/preferredUnits';

export default function* activateAlexaSkill(action) {
  try {
    yield put(setLoading(true));

    const {
      activateParams: { code, skillEnaUrl, redirectUrl, skillStage },
    } = action;

    const requestBody = {
      stage: skillStage,
      accountLinkRequest: {
        redirectUri: `https://${redirectUrl}`,
        authCode: code,
        type: 'AUTH_CODE',
      },
    };

    const authToken = yield select(
      state => state.preferredUnits.alexaToken.accessToken
    );

    const response = yield call(
      ApiService.postWithoutAuthCustom,
      `https://${skillEnaUrl}`,
      requestBody,
      `Bearer ${authToken}`
    );

    if (response && response.data) {
      yield put(setAlexaTokenStatus({ alexaLinkStatus: 'activateSuccess' }));
    }
  } catch (error) {
    yield put(setAlexaTokenStatus({ alexaLinkStatus: 'activateError' }));
  } finally {
    yield put(setLoading(false));
  }
}
