import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setDeviceConfigLoading,
  clearDeviceConfigStatus,
  setDeviceConfigStatus,
} from '../actions/deviceConfig';

import { getAllUnits } from '../actions/units';
import getGroupIndex from '../utils/getGroupIndex';
import getUnitIndex from '../utils/getUnitIndex';

export default function* bindDevice(action) {
  try {
    yield put(clearDeviceConfigStatus());
    yield put(setDeviceConfigLoading({ isLoading: true }));

    const username = yield select(state => state.user.profile.email);
    const {thingName} = action.deviceConfigParams;
    const groupName = action.deviceConfigParams.deviceGroup;

    const { groupIndex, groupKey } = yield call(getGroupIndex, groupName);
    const unitIndex = yield call(getUnitIndex, groupKey, groupIndex, thingName);

    const responseData = yield call(ApiService.post, 'sendbindmasterinfo', {
      requestData: {
        username,
        thingName,
        qx: '15',
        acName: action.deviceConfigParams.deviceName,
        acGroup: groupName,
        logo: action.deviceConfigParams.logo,
        key: action.deviceConfigParams.randomKey,
        groupIndex,
        unitIndex,
      },
    });

    if (responseData && responseData.code) {
      yield put(
        setDeviceConfigStatus({
          messageType: 'error',
          message: responseData.message,
        })
      );
    } else {
      const email = yield select(state => state.user.profile.email);

      yield put(getAllUnits({ email }));

      yield put(
        setDeviceConfigStatus({
          messageType: 'success',
          message: 'success',
        })
      );
    }
  } catch (error) {
    yield put(
      setDeviceConfigStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setDeviceConfigLoading({ isLoading: false }));
  }
}
