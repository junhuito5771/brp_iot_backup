import { takeLeading, takeLatest } from 'redux-saga/effects';
import { fetchUnitsAndTimersTask } from './fetchUnits';
import fetchUnitUsers from './fetchUnitUsers';

import { FETCH_UNIT_USERS_LIST } from '../actions/unitUsers';
// import { SUBMIT_APP_INFO } from '../actions/user';
import { GET_ALL_UNITS } from '../actions/units';
import {
  CHECK_IOT_STATUS,
  CREATE_UNIT_IN_IOT,
  SUBMIT_BIND_NO_QR,
} from '../actions/pairDevice';
import { SUBMIT_DEVICE_CONFIG_PARAMS } from '../actions/deviceConfig';
import { SWITCH_AC, SWITCH_GROUP_AC } from '../actions/homepageIotShadow';
import {
  SET_FAN_SPEED,
  SET_MODE,
  MOUNT_UNIT,
  SET_SLEEP,
  SET_SENSE,
  SET_TURBO,
  SET_SILENT,
  SET_SWING,
  SET_SWING_LR,
  SET_SWING_3D,
  SET_ECOPLUS,
  SET_SMART_DRIFT,
  SET_AC_SWITCH,
  SET_AC_TEMP,
  SET_BREEZE,
  SET_STREAMER,
  SET_LED,
  SET_CK_SWING,
  REFRESH_UNIT,
  FETCH_ERROR_HISTORY,
  // CHECK_UNIT_AUTH,
} from '../actions/remote';
import {
  SUBMIT_EDIT_PARAMS,
  SUBMIT_EDIT_UNIT_NAME,
} from '../actions/editUnits';
import { PROCESS_BLE_UNITS } from '../actions/bleMode';

import { SUBMIT_BIND_GUEST_DEVICE } from '../actions/bindGuestDevice';
import {
  SUBMIT_UNBIND_DEVICE,
  SUBMIT_REMOVE_GUESTS,
} from '../actions/unbindDevice';
import { WLAN_DISCOVER_UNITS, WLAN_PAIR } from '../actions/wlanMode';
import { SET_USER_CONTROL } from '../actions/unitUserControl';
import { BLECONTROL_CONNECT } from '../actions/bleControl';
import { SUBMIT_FIRMWARE_UPGRADE } from '../actions/updateFirmware';
import { FETCH_TIMER, SUBMIT_TOGGLE_TIMER } from '../actions/timer';
import {
  CHECK_CAN_CREATE_TIMER,
  SUBMIT_TIMER_PARAMS,
  SUBMIT_QUICK_TIMER,
} from '../actions/editTimer';
import { FETCH_USAGE_DATA } from '../actions/usage';
import { FETCH_ALL_PLANS } from '../actions/plans';
import { CHECK_EXPIRY_TIME } from '../actions/controlUsageLimit';
import { SCAN_SSID } from '../actions/scanSSID';
import {
  SUBMIT_ALEXA_PREFERRED,
  FETCH_ALEXA_TOKEN,
  ACTIVATE_ALEXA_SKILL,
  REFRESH_ALEXA_TOKEN,
  FETCH_ALEXA_ACTIVATE_STATUS,
  FETCH_ALEXA_COMMAND_URL,
} from '../actions/preferredUnits';

import checkIoTStatus, { createUnitInIoT } from './checkIoTStatus';
import bindDevice from './bindDevice';
import { switchAc, switchGroupAc } from './homepageIotShadow';
import {
  remoteFlow,
  refreshUnit,
  remoteActionInterface,
  fetchErrorHistory,
} from './remote';
import submitEditForm, { submitEditUnitName } from './editUnits';
import processBleUnits from './processBleUnits';

import bindGuestDevice from './bindGuestDevice';
import unbindDevice from './unbindDevice';
import discoverWLANUnits from './discoverWLANUnits';
import setUserControl from './unitUserControl';
import unbindGuests from './unbindGuests';
import { connectBleUnit } from './remote/bleControl';
import { pairWLANUnit } from './remote/wlanControl';
import submitFirmwareUpgrade from './updateFirmware';
import fetchTimer from './fetchTimer';
import checkCreateTimer from './checkCreateTimer';
import editWeeklyTimer from './editWeeklyTimer';
import toggleTimer from './toggleTimer';
import fetchUsageData from './fetchUsage';
// import checkUnitAuth from './checkUnitAuth';
import scanSSID from './scanSSID';
import submitQuickTimer from './submitQuickTimer';
// import submitAppInfo from './submitAppInfo';
import submitBindNoQR from './bindNoQR';
import fetchPlans from './fetchPlans';
import controlUsageLimit from './controlUsageLimit';

import submitAlexaPreferred from './alexa/preferredUnits';
import fetchAlexaToken from './alexa/fetchAlexaToken';
import activateAlexaSkill from './alexa/activateAlexaSkill';
import refreshAlexaToken from './alexa/refreshToken';
import fetchAlexaActivateStatus from './alexa/fetchActivateStatus';
import fetchAlexaCommandUrl from './alexa/fetchCommandUrl';

export default function* rootSaga() {
  yield takeLeading(FETCH_UNIT_USERS_LIST, fetchUnitUsers);

  yield takeLeading(GET_ALL_UNITS, fetchUnitsAndTimersTask);
  yield takeLeading(FETCH_ERROR_HISTORY, fetchErrorHistory);
  yield takeLeading(SUBMIT_EDIT_PARAMS, submitEditForm);
  yield takeLeading(SUBMIT_EDIT_UNIT_NAME, submitEditUnitName);
  yield takeLeading(SET_USER_CONTROL, setUserControl);
  yield takeLeading(FETCH_USAGE_DATA, fetchUsageData);
  yield takeLeading(FETCH_ALL_PLANS, fetchPlans);
  yield takeLeading(CHECK_EXPIRY_TIME, controlUsageLimit);
  // // IoTStatus
  yield takeLatest(CHECK_IOT_STATUS, checkIoTStatus);
  yield takeLeading(CREATE_UNIT_IN_IOT, createUnitInIoT);
  yield takeLeading(SUBMIT_DEVICE_CONFIG_PARAMS, bindDevice);
  yield takeLeading(SWITCH_AC, switchAc);
  yield takeLeading(SWITCH_GROUP_AC, switchGroupAc);
  yield takeLatest(SET_AC_TEMP, remoteActionInterface);
  yield takeLeading(
    [
      SET_FAN_SPEED,
      SET_SLEEP,
      SET_TURBO,
      SET_SWING,
      SET_SWING_LR,
      SET_SWING_3D,
      SET_ECOPLUS,
      SET_MODE,
      SET_SILENT,
      SET_AC_SWITCH,
      SET_SENSE,
      SET_BREEZE,
      SET_SMART_DRIFT,
      SET_STREAMER,
      SET_LED,
      SET_CK_SWING,
    ],
    remoteActionInterface
  );
  yield takeLatest(MOUNT_UNIT, remoteFlow);
  // yield takeLeading(CHECK_UNIT_AUTH, checkUnitAuth);
  yield takeLeading(REFRESH_UNIT, refreshUnit);
  yield takeLeading(PROCESS_BLE_UNITS, processBleUnits);
  yield takeLeading(SUBMIT_BIND_GUEST_DEVICE, bindGuestDevice);
  yield takeLeading(SUBMIT_REMOVE_GUESTS, unbindGuests);
  yield takeLeading(SUBMIT_UNBIND_DEVICE, unbindDevice);

  yield takeLeading(WLAN_DISCOVER_UNITS, discoverWLANUnits);
  yield takeLeading(BLECONTROL_CONNECT, connectBleUnit);
  yield takeLeading(WLAN_PAIR, pairWLANUnit);
  yield takeLeading(SUBMIT_FIRMWARE_UPGRADE, submitFirmwareUpgrade);
  yield takeLeading(FETCH_TIMER, fetchTimer);
  yield takeLeading(CHECK_CAN_CREATE_TIMER, checkCreateTimer);
  yield takeLeading(SUBMIT_TIMER_PARAMS, editWeeklyTimer);
  // yield takeLeading(SET_EDIT_TIMER_CONFIG, initEditTimer);
  yield takeLeading(SUBMIT_TOGGLE_TIMER, toggleTimer);
  yield takeLeading(SCAN_SSID, scanSSID);
  yield takeLeading(SUBMIT_QUICK_TIMER, submitQuickTimer);
  // // yield takeLeading(SUBMIT_APP_INFO, submitAppInfo);
  yield takeLeading(SUBMIT_BIND_NO_QR, submitBindNoQR);
  yield takeLeading(SUBMIT_ALEXA_PREFERRED, submitAlexaPreferred);
  yield takeLeading(FETCH_ALEXA_TOKEN, fetchAlexaToken);
  yield takeLeading(ACTIVATE_ALEXA_SKILL, activateAlexaSkill);
  yield takeLeading(REFRESH_ALEXA_TOKEN, refreshAlexaToken);
  yield takeLeading(FETCH_ALEXA_ACTIVATE_STATUS, fetchAlexaActivateStatus);
  yield takeLeading(FETCH_ALEXA_COMMAND_URL, fetchAlexaCommandUrl);
}
