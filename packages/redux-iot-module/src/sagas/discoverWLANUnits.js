import { NetworkHelper, StringHelper } from '@module/utility';
import { call, put, select, retry } from 'redux-saga/effects';

import UDPService from '../utils/UDPService';

import {
  setLoading,
  setStatus,
  setUnits,
  clearStatus,
} from '../actions/wlanMode';

const LAN_DEV_DETECT = 'LAN_DEV_DETECT';
const WLAN_DISCOVER_MESSAGE = `{"${LAN_DEV_DETECT}":"${LAN_DEV_DETECT}"}`;
const DEFAULT_MAX_INDEX = 9999;
const UNAUTHORIZED_UNIT_KEY = 'Unauthorized units';
const DEFAULT_LOGO = '1.png';

export function* getWLANUnits(isBroadcast, preWLANUnits) {
  const targetAddress = yield call(NetworkHelper.getBroadcastAddress);
  const udpClient = UDPService.createUDPSocket({
    type: 'udp4',
    reuseAddr: true,
  });

  const response = yield call(UDPService.sendMessage, {
    isBroadcast,
    udpClient,
    message: WLAN_DISCOVER_MESSAGE,
    targetAddress,
    noResponseMessage:
      'We are unable to detect any available units. Please ensure that the units you would like to control are connected to the same wireless network.',
  });

  // Process the response and also remove the duplicate records
  const processedResponse = response.reduce(
    (acc, item) => {
      const { data, source } = item;
      const slicedData = data.slice(0, -1).replace(/'/g, '"');
      const dataObj = JSON.parse(slicedData);
      const unitData = dataObj[LAN_DEV_DETECT];

      const { thingName } = unitData;

      if (!acc[thingName] || acc[thingName].source !== source) {
        // eslint-disable-next-line no-param-reassign
        acc[thingName] = { data: unitData, source };
      }

      return acc;
    },
    { ...preWLANUnits }
  );

  return processedResponse;
}

function getWLANUnitFromStore(unitKeys, allUnits) {
  const wlanUnits = {};

  unitKeys.forEach(thingName => {
    if (!wlanUnits[thingName] && allUnits[thingName].IP) {
      wlanUnits[thingName] = {
        data: { thingName },
        source: { address: allUnits[thingName].IP },
      };
    }
  });

  return wlanUnits;
}

export default function* discoverWLANUnits(action) {
  try {
    yield put(clearStatus());
    yield put(setLoading(true));

    const isConnectedToWifi = yield call(NetworkHelper.isConnectedToWifi);

    if (!isConnectedToWifi) {
      throw new Error('Please connect to wifi network to use WLAN mode.');
    }

    const allUnits = yield select(state => state.units.allUnits);
    const unitKeys = Object.keys(allUnits);

    let processedResponse = yield call(
      getWLANUnitFromStore,
      unitKeys,
      allUnits
    );

    // If not all units has the ip in all units,
    // then need to perform udp scanning
    if (unitKeys.length !== processedResponse.length) {
      processedResponse = yield retry(
        5,
        3000,
        getWLANUnits,
        action.discoverWLANParams.setBroadcast,
        processedResponse
      );
    }

    // convert the processed response to array and sort
    const processedResponseInArray = Object.values(processedResponse);

    processedResponseInArray.sort(({ data: curUnit }, { data: nextUnit }) => {
      const {
        groupIndex: curGroupIndex = DEFAULT_MAX_INDEX,
        unitIndex: curUnitIndex = DEFAULT_MAX_INDEX,
      } = allUnits[curUnit.thingName] || {};
      const {
        groupIndex: nextGroupIndex = DEFAULT_MAX_INDEX,
        unitIndex: nextUnitIndex = DEFAULT_MAX_INDEX,
      } = allUnits[nextUnit.thingName] || {};

      return curGroupIndex - nextGroupIndex || curUnitIndex - nextUnitIndex;
    });

    // Group the units by the group and set to the store
    if (processedResponseInArray && processedResponseInArray.length > -1) {
      const allGroups = yield select(state => state.units.groupInfo.groups);
      const allGroupsInArray = Object.values(allGroups);
      const units = {};

      processedResponseInArray.forEach(({ data, source }) => {
        const { thingName } = data;

        const unitInStore = allUnits[thingName];
        const groupInStore = unitInStore
          ? allGroupsInArray.find(
              group => group.groupIndex === unitInStore.groupIndex
            )
          : undefined;

        const groupKey = groupInStore
          ? groupInStore.groupName
          : UNAUTHORIZED_UNIT_KEY;
        const groupInUnits = units[groupKey];

        const unitProps = {
          name: unitInStore
            ? unitInStore.ACName
            : StringHelper.removePrefix(thingName),
          logo: unitInStore ? unitInStore.Logo : DEFAULT_LOGO,
          disallowed: !unitInStore,
          thingName,
          host: source.address,
        };

        const dataInArray = groupInUnits
          ? [...groupInUnits.data, unitProps]
          : [unitProps];

        units[groupKey] = {
          title: groupKey,
          data: dataInArray,
        };
      });

      const unitsInArray = Object.values(units);

      if (unitsInArray && unitsInArray.length > -1) {
        yield put(setUnits(unitsInArray));
        yield put(
          setStatus({
            messageType: 'success',
            message: 'success',
          })
        );
      } else {
        yield put(
          setStatus({
            messageType: 'success',
            message: 'success',
          })
        );
      }
    }
  } catch (error) {
    yield put(
      setStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setLoading(false));
  }
}
