import { call, select, put } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import { ShadowValueHelper } from '../utils/Remote';

import {
  addQuickTimer,
  removeQuickTimer,
  setQuickTimerLoading,
  clearFetchTimerStatus,
  setQuickTimerError,
} from '../actions/timer';
import {
  mapTimerInfoToResponse,
  saveTimerRequestToState,
} from './editWeeklyTimer';
import fetchAllTimer from './fetchTimer';

const CREATE_WEEKLY_TIMER = 2;
const UPDATE_DELETE_WEEKLY_TIMER = 3;

function* createQuickTimerBody(timerInfo) {
  const {
    units: { allUnits },
    user: {
      profile: { email },
    },
    remote: { thingName },
  } = yield select(state => state);

  const { handsetType, mode, modeConfig } = allUnits[thingName] || {};
  const { silent, fan, fanExtend, acTemp } = modeConfig;

  const quickTimerConfig = {
    isActive: true,
    day: timerInfo.day,
    hour: timerInfo.hour,
    minute: timerInfo.minute,
    switchValue: timerInfo.switchValue,
    mode,
    silent,
    temp: acTemp,
    fanLevel: ShadowValueHelper.getFanLvlByShadowVal(
      handsetType,
      fan,
      fanExtend,
      modeConfig,
      mode
    ),
  };

  return {
    [email]: [{ ...mapTimerInfoToResponse(quickTimerConfig), countDown: 1 }],
  };
}

function* removeQuickTimerBody({ thingName }) {
  const {
    timer: { timerList },
  } = yield select(state => state);

  const timersByUser = {};

  (timerList[thingName] || []).forEach(timerInfo => {
    if (!timersByUser[timerInfo.user]) {
      timersByUser[timerInfo.user] = [];
    }

    if (!timerInfo.isQuickTimer) {
      timersByUser[timerInfo.user].push(mapTimerInfoToResponse(timerInfo));
    }
  });

  return timersByUser;
}

export default function* submitQuickTimer(action) {
  const { isCreate, timerInfo } = action.quickTimerParams;
  try {
    yield put(clearFetchTimerStatus());
    yield put(setQuickTimerLoading(true));
    yield call(fetchAllTimer);
    const {
      user: {
        profile: { email },
      },
      timer: { timerList },
      remote: { thingName },
    } = yield select(state => state);

    if (isCreate) {
      const hasDuplicatedTimer =
        (timerList[thingName] || []).findIndex(
          curTimerInfo =>
            curTimerInfo.day === timerInfo.day &&
            curTimerInfo.hour === timerInfo.hour &&
            curTimerInfo.minute === timerInfo.minute
        ) > -1;

      if (hasDuplicatedTimer) {
        throw new Error('Timer already exists');
      }
    }

    const timerInArray = isCreate
      ? yield call(createQuickTimerBody, timerInfo)
      : yield call(removeQuickTimerBody, timerInfo);

    const response = yield call(ApiService.post, 'timerfunction', {
      requestData: {
        type: isCreate ? CREATE_WEEKLY_TIMER : UPDATE_DELETE_WEEKLY_TIMER,
        username: email,
        thingName,
        timerData: [timerInArray],
      },
    });

    if (response && response.message) {
      if (response.message === 'success') {
        if (isCreate) {
          const quickTimerConfig = {
            [thingName]: {
              ...timerInfo,
            },
          };
          yield put(addQuickTimer(quickTimerConfig));
        } else {
          yield put(removeQuickTimer(thingName));
        }

        yield call(saveTimerRequestToState, isCreate, thingName, [
          timerInArray,
        ]);
      } else {
        throw new Error(response.message);
      }
    }
  } catch (error) {
    let message = isCreate
      ? 'Error while creating quick timer'
      : 'Error while removing quick timer';
    // Prompt user quick timer is already created
    // when duplicate timer detected
    if (
      error.message === 'Quick timer already existed for this unit' ||
      error.message === 'Timer already exists'
    ) {
      message = 'Quick timer is already created';
    }

    yield put(setQuickTimerError(message));
  } finally {
    yield put(setQuickTimerLoading(false));
  }
}
