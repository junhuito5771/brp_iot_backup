import { call, put, select, delay, spawn } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import {
  setIoTStatus,
  setIoTStatusLoading,
  resetRetry,
} from '../actions/pairDevice';
import { setReconnect } from '../actions/remote';

export function* createUnitInIoT(action) {
  const {thingName} = action.checkIoTParams;
  const key = action.checkIoTParams.randomKey;
  const {factoryKey} = action.checkIoTParams;
  const currentUser = yield select(state => state.user.profile);

  yield put(
    setIoTStatusLoading({
      loadingStatus: 1,
      loadingText: 'Creating unit in server....',
    })
  );

  const createThingResponse = yield call(ApiService.post, 'creatething', {
    requestData: {
      username: currentUser.email,
      thingName,
      key,
      factoryKey: factoryKey || '-',
    },
  });

  if (createThingResponse && createThingResponse.code) {
    throw new Error('Failed to create unit in IOTCore');
  }
}

function* callSendMessage(sendMessage) {
  const notifyIDs = ['2bb1', '2bb2'];
  for (let i = 0; i < 2; i += 1) {
    try {
      const response = yield call(sendMessage, notifyIDs[i]);

      return response;
    } catch (error) {
      yield put(
        setIoTStatusLoading({
          loadingStatus: 6.5,
          loadingText: 'Try to resend the configuration...',
        })
      );
      yield delay(1000);
    }
  }

  throw new Error('Connect to unit timeout');
}

function* handleReconnectTimeout() {
  yield delay(20000);

  yield put(setReconnect(false));
}

export default function* submitCheckUnitStatus(action) {
  const { isReconnect, ssid } = action.checkIoTParams;
  try {
    yield put(
      setIoTStatusLoading({
        loadingStatus: 1,
        loadingText: 'Start connecting...',
      })
    );
    const response = yield call(
      callSendMessage,
      action.checkIoTParams.sendMessage
    );
    yield put(
      setIoTStatusLoading({
        loadingStatus: 7,
        loadingText: 'Checking wifi configuration status...',
      })
    );

    yield put(
      setIoTStatusLoading({
        loadingStatus: 8,
        loadingText: 'Checking unit connectivity...',
      })
    );

    if (isReconnect) {
      yield put(setReconnect(isReconnect));
      yield spawn(handleReconnectTimeout);
    }

    if (response && response.indexOf('result:"ok"') > -1) {
      yield call(createUnitInIoT, action);
      yield put(
        setIoTStatus({
          ssid,
          messageType: 'success',
          message: 'success',
        })
      );
    } else {
      yield put(
        setIoTStatus({
          ssid,
          messageType: 'error',
          message:
            'Unable to connect due to incorrect SSID and Password or unstable network. Please try again in a few minutes.',
        })
      );
    }
  } catch (error) {
    yield put(
      setIoTStatus({
        ssid,
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(resetRetry());
    yield put(
      setIoTStatusLoading({
        loadingStatus: 0,
        loadingText: '',
      })
    );
  }
}
