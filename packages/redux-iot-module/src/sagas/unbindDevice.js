import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import UserType from '../utils/userType';
import {
  setUnbindDeviceLoading,
  setUnbindDeviceStatus,
  clearUnbindDeviceStatus,
} from '../actions/unbindDevice';
import { getAllUnits } from '../actions/units';
import { cancelCheckUnitAuth } from '../actions/remote';

function getGuestEmailList(guestUsers) {
  return guestUsers.reduce((acc, user) => {
    acc.push(user.email);
    return acc;
  }, []);
}

function getResponseContent({ qx, unitUserList, thingName, email }) {
  const isHostUser = UserType.isHostUser(qx);
  const isGuestUser = UserType.isGuestUser(qx);

  if (!isHostUser && !isGuestUser) throw new Error('Invalid user type');

  let response = {};
  if (isHostUser) {
    const hasGuestUser =
      unitUserList[1] &&
      unitUserList[1].data &&
      unitUserList[1].data.length > 0;
    const currentType = hasGuestUser ? 3 : 1;
    response = {
      type: currentType,
      email,
      thingName,
    };

    if (hasGuestUser) {
      response.guest = getGuestEmailList(unitUserList[1].data);
    }
  } else if (isGuestUser) {
    response = {
      type: 1,
      email,
      thingName,
    };
  }

  return response;
}

export default function* unbindDevice(action) {
  try {
    // Cancel the checking of unit auth to avoid the error message
    yield put(cancelCheckUnitAuth());
    yield put(clearUnbindDeviceStatus());
    yield put(setUnbindDeviceLoading(true));

    const email = yield select(state => state.user.profile.email);
    const allUnits = yield select(state => state.units.allUnits);
    const unitUserList = yield select(state => state.unitUsers.unitUserList);
    const thingName = Array.isArray(action)
      ? action[0].thingName
      : action.unbindDeviceParams.thingName;

    if (allUnits[thingName]) {
      const { qx } = allUnits[thingName];
      const responseContent = getResponseContent({
        qx,
        unitUserList,
        thingName,
        email,
      });

      if (!responseContent || !responseContent.type) {
        throw new Error('Invalid response content');
      }

      const responseData = yield call(ApiService.post, 'unbinddevice', {
        requestData: responseContent,
      });

      if (responseData && responseData.code) {
        yield put(
          setUnbindDeviceStatus({
            messageType: 'error',
            message: 'Error unbinding unit. Please try again.',
          })
        );
      } else {
        yield put(getAllUnits({ email }));

        yield put(
          setUnbindDeviceStatus({
            messageType: 'success',
            message: 'Successfully unbind the unit.',
          })
        );
      }
    }
  } catch (error) {
    yield put(
      setUnbindDeviceStatus({
        messageType: 'error',
        message: error.message || 'Error unbinding unit. Please try again.',
      })
    );
  }
  yield put(clearUnbindDeviceStatus());
  yield put(setUnbindDeviceLoading(false));
}
