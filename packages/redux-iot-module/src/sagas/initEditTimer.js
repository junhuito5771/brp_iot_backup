import { put, select } from 'redux-saga/effects';

import {
  setEditTimerParam,
  setEditTimerStatus,
  clearEditTimerStatus,
  setEditTimerInitParam,
} from '../actions/editTimer';

export default function* initEditWeeklyTimer(action) {
  try {
    yield put(clearEditTimerStatus());
    const {
      timer: { timerList },
    } = yield select(state => state);

    const { timerIndex, thingName } = action;
    const {
      day,
      hour,
      minute,
      mode,
      fanOptions,
      switchValue,
      temp,
    } = timerList[thingName][timerIndex];

    const currentTime = new Date();
    currentTime.setHours(hour);
    currentTime.setMinutes(minute);

    const curDays = Array(7).fill(false);
    curDays[day] = true;

    // set initial value
    yield put(setEditTimerInitParam({ key: 'thingName', value: thingName }));
    yield put(setEditTimerInitParam({ key: 'timerIndex', value: timerIndex }));
    yield put(setEditTimerInitParam({ key: 'time', value: currentTime }));
    yield put(setEditTimerInitParam({ key: 'days', value: curDays }));
    yield put(setEditTimerInitParam({ key: 'mode', value: mode }));
    yield put(
      setEditTimerInitParam({ key: 'switchValue', value: Number(switchValue) })
    );
    yield put(
      setEditTimerInitParam({ key: 'fanOptions', value: { ...fanOptions } })
    );
    yield put(setEditTimerInitParam({ key: 'temp', value: temp }));

    // Set the thingName and timerIndex
    yield put(setEditTimerParam({ key: 'thingName', value: thingName }));
    yield put(setEditTimerParam({ key: 'timerIndex', value: timerIndex }));

    // Setting the timer config
    yield put(setEditTimerParam({ key: 'time', value: currentTime }));
    yield put(setEditTimerParam({ key: 'days', value: curDays }));
    yield put(setEditTimerParam({ key: 'mode', value: mode }));
    yield put(
      setEditTimerParam({ key: 'switchValue', value: Number(switchValue) })
    );
    yield put(
      setEditTimerParam({ key: 'fanOptions', value: { ...fanOptions } })
    );
    yield put(setEditTimerParam({ key: 'temp', value: temp }));

    yield put(
      setEditTimerStatus({
        messageType: 'canCreateSuccess',
        message: 'success',
      })
    );
  } catch (error) {
    yield put(
      setEditTimerStatus({
        messageType: 'canCreateError',
        message: error.message,
      })
    );
  }
}
