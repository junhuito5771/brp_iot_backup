import { eventChannel } from 'redux-saga';
import {
  call,
  fork,
  delay,
  take,
  race,
  join,
  select,
  put,
} from 'redux-saga/effects';
import BLEService from '../utils/BLE/BLEService';

import {
  clearIoTStatus,
  setIoTStatusLoading,
  setIoTStatus,
  CANCEL_BIND_NO_QR,
} from '../actions/pairDevice';
import { getAllUnits } from '../actions/units';

import getUnitIndex from '../utils/getUnitIndex';

function convertToJSON(text) {
  try {
    return JSON.parse(text);
  } catch (error) {
    return null;
  }
}

function* listenBLEChannel(channel) {
  let response = '';

  while (true) {
    const data = yield take(channel);

    if (data.startsWith('<{')) {
      response = '';
    }

    response = response.concat(data);

    if (response.length > 0 && response.endsWith('}>')) {
      // Process the response by replacing the whitespace, remove < and >
      response = response.replace(/\s(?=(?:"[^"]*"|[^"])*$)|<|>/g, '');

      const responseInObj = convertToJSON(response);

      if (responseInObj && responseInObj.message) {
        return {
          payload: responseInObj.message,
        };
      }

      if (responseInObj && responseInObj.action) {
        return {
          payload: responseInObj.action,
        };
      }
      response = '';
    }
  }
}

function* submitBindUnitNoQR({
  ssid,
  password,
  username,
  acName,
  hasAllGroup,
}) {
  let channel;
  let task;

  yield put(
    setIoTStatusLoading({
      loadingStatus: 1,
      loadingText: 'Start scanning...',
    })
  );
  yield call(BLEService.scan);

  const units = yield call(BLEService.getDiscoveredUnits);

  // 1. Only filter to get the units with name: Daikin
  const selectedUnits = units.filter(unit => unit.name === 'Daikin');
  const sortedUnits = selectedUnits.sort(unit => unit.rssi);
  const notifyID = '2bb1';

  if (!sortedUnits.length) {
    throw new Error('No units found');
  }

  // 2. Find the highest RSSI
  for (let i = 0; i < sortedUnits.length; i += 1) {
    const curUnit = sortedUnits[i];
    const connectionID = curUnit.id;
    const thingName = yield call(BLEService.getUnitThingName, curUnit);
    const unitIndex = yield call(getUnitIndex, hasAllGroup, -1, thingName);

    yield put(
      setIoTStatusLoading({ loadingStatus: 1, loadingText: 'Unit found' })
    );

    yield call(BLEService.connectAndRetrieveService, curUnit);

    // To enable BleManagerDidUpdateValueForCharacteristic listener
    yield call(BLEService.startNotification, { id: connectionID }, notifyID);

    // Prevent listener starts before startNotification
    yield delay(1000);

    // Create process ble response channel
    channel = eventChannel(emit => {
      BLEService.addNotificationListerner(emit);

      return () => BLEService.stopNotification({ id: connectionID }, notifyID);
    });

    task = yield fork(listenBLEChannel, channel);

    const payload = {
      action: 'reset',
      ssid,
      password,
      username,
      acName,
      unitIndex,
    };

    yield put(
      setIoTStatusLoading({
        loadingStatus: 2,
        loadingText: 'Sending message...',
      })
    );

    yield call(BLEService.sendMessage, {
      id: connectionID,
      message: `<${JSON.stringify(payload)}>`,
    });

    yield put(
      setIoTStatusLoading({
        loadingStatus: 3,
        loadingText: 'Waiting for unit to respond...',
      })
    );

    const { result } = yield race({
      result: join(task),
    });

    if (result) {
      yield put(
        setIoTStatusLoading({
          loadingStatus: 4,
          loadingText: 'Unit responded....',
        })
      );

      return result.payload;
    }
  }
}

export default function* submitBindUnitNoQRTask(action) {
  try {
    yield put(clearIoTStatus());
    yield put(
      setIoTStatusLoading({
        loadingStatus: 1,
        loadingText: 'Preparing...',
      })
    );
    const { ssid, password, unitName: acName } = action.submitBindParams;

    // 1. Get username
    const {
      user: {
        profile: { email },
      },
      units: {
        groupInfo: { groups },
      },
    } = yield select(state => state);
    // 2. Get unitIndex by using groupIndex

    const groupObj = Object.keys(groups).find(
      groupKey => groups[groupKey].groupIndex === -1
    );
    const hasAllGroup = groupObj !== undefined;

    const payload = {
      ssid,
      password,
      acName,
      username: email,
      hasAllGroup,
    };
    const { tasks, timeout, cancelTask } = yield race({
      tasks: call(submitBindUnitNoQR, payload),
      timeout: delay(60000),
      cancelTask: take(CANCEL_BIND_NO_QR),
    });

    if (!cancelTask) {
      if (timeout) {
        throw new Error('Timeout while waiting response from unit');
      }
      // Success
      // Waiting for 5 seconds
      yield put(
        setIoTStatusLoading({
          loadingStatus: 3,
          loadingText: 'Waiting for unit to submit request...',
        })
      );

      if (tasks === 'reset') {
        yield put(setIoTStatus({ messageType: 'success', message: 'success' }));
        yield put(getAllUnits({ email }));
      } else {
        throw new Error(tasks || 'Unexpected Error Occured.');
      }
    }
  } catch (error) {
    yield put(setIoTStatus({ messageType: 'error', message: error.message }));
  } finally {
    yield put(
      setIoTStatusLoading({
        loadingStatus: -1,
        loadingText: '',
      })
    );
  }
}
