import {
  call,
  cancelled,
  cancel,
  select,
  put,
  take,
  delay,
  race,
  fork,
} from 'redux-saga/effects';
import { eventChannel, END } from 'redux-saga';
import {
  StringConverter,
  NetworkHelper,
  VersionCompare,
  ApiService,
} from '@module/utility';
import TCPHelper from 'react-native-tcp';
import UDPService from 'react-native-udp';
import FileSystem from 'react-native-fs';

import { setShadowState } from './remote/iotControl';
import {
  getShadowState,
  sendMessage as TCPSendMessage,
} from './remote/wlanControl';
import {
  setUploadCompleted,
  SET_UPLOAD_COMPLETED,
  setLoading,
  clearUpdateStatus,
  setUpdateStatus,
  CANCEL_FIRMWARE_UPGRADE,
  UNIT_DISCONNECT,
  unitDisconnect,
} from '../actions/updateFirmware';
import { setFirmwareVersion } from '../actions/settings';
import { setUnit } from '../actions/units';
import { getUnit } from '../selector';
import shadowStateMapper from '../utils/shadowStateMapper';
import { refreshUnit } from './remote';

const CHUNK_SIZE = 1024;
const OTA_PORT = 37126;
const FIND_WLAN_UNIT_TIMEOUT = 10000;
const SERVER_LISTENER_CONNECTION = 'connection';
const LAN_DEV_DETECT = 'LAN_DEV_DETECT';
const WLAN_DISCOVER_MESSAGE = `{"${LAN_DEV_DETECT}":"${LAN_DEV_DETECT}"}`;

const DEFAULT_LOCAL_ADDRESS = '0.0.0.0';
const DEFAULT_TARGET_ADDRESS = '0.0.0.0';
const DEFAULT_TIMEOUT = 5000;
const DEFAULT_LOCAL_PORT = 61245;
const DEFAULT_TARGET_PORT = 61244;
const DEFAULT_NO_RESPONSE_MESSAGE = 'No response received';

const FIRMWARE_FILE_PATH = `${FileSystem.TemporaryDirectoryPath}/firmware.ota`;

const sendMessage = ({
  localAddress = DEFAULT_LOCAL_ADDRESS,
  localport = DEFAULT_LOCAL_PORT,
  targetAddress = DEFAULT_TARGET_ADDRESS,
  targetport = DEFAULT_TARGET_PORT,
  message,
  isBroadcast = false,
  timeout = DEFAULT_TIMEOUT,
  noResponseMessage = DEFAULT_NO_RESPONSE_MESSAGE,
  udpClient,
}) => {
  const promiseResponse = new Promise((resolve, reject) => {
    const response = [];
    let timeoutListener;

    const handleTimeout = () => {
      udpClient.close();
      if (response.length > 0) resolve(response);
      else reject(new Error(noResponseMessage));
    };

    udpClient.on(
      'listening',
      () => {
        timeoutListener = setTimeout(handleTimeout, timeout);

        const data = StringConverter.StringToByte(message);
        if (data) {
          udpClient.send(data, 0, data.length, targetport, targetAddress);
        }
      },
      err => {
        if (err) reject(err);
      }
    );

    udpClient.on('error', err => reject(err));

    udpClient.on('message', (data, rinfo) => {
      // Clear the timeout when receive data
      if (timeoutListener) {
        clearTimeout(timeoutListener);
        timeoutListener = null;
      }
      const dataInString = StringConverter.ByteToString(data);
      if (dataInString) response.push({ data: dataInString, source: rinfo });

      timeoutListener = setTimeout(handleTimeout, timeout);
    });
  });

  udpClient.bind(
    {
      address: localAddress,
      port: localport,
      exclusive: true,
    },
    () => {
      if (isBroadcast) {
        udpClient.setBroadcast(isBroadcast);
      }
    }
  );

  return promiseResponse;
};

function splitFile(data, startIndex, length) {
  const endIndex = Math.min(startIndex + length, data.length);
  return Uint8Array.from(data.slice(startIndex, endIndex));
}

function* getFirmwareFile(email, thingName) {
  const firmwareFile = yield call(
    ApiService.post,
    'getfirmwareupdate',
    {
      requestData: {
        type: 2,
        email,
        thingName,
      },
    },
    ApiService.contentType.Text
  );

  if (!firmwareFile && firmwareFile.length < 1) {
    throw new Error('Error downloading the latest firmware file');
  }

  return firmwareFile;
}

function convertIntToBytes(value) {
  /* eslint-disable no-bitwise */
  const result = new Uint8Array(4);
  result[0] = value & 0xff;
  result[1] = (value >> 8) & 0xff;
  result[2] = (value >> 16) & 0xff;
  result[3] = (value >> 24) & 0xff;
  /* eslint-enable no-bitwise */

  return result;
}

function socketWritePromisfy(socket, content) {
  return new Promise((resolve, reject) => {
    socket.write(content, err => {
      if (err) reject(err);

      resolve(true);
    });
  });
}

function listenServerPromise(server, localAddress) {
  return new Promise((resolve, reject) => {
    server.listen(OTA_PORT, localAddress, () => {
      resolve(true);
    });

    const handleServerError = err => {
      reject(err);
    };

    server.on('error', err => {
      handleServerError(err);
    });
  });
}

function* createFirmwareUploadChannel(server, firmwareFile) {
  const localAddress = yield call(NetworkHelper.getLocalIPAddress);
  const fileSizeInBytes = convertIntToBytes(firmwareFile.length);

  const eventChannelInstance = eventChannel(emit => {
    const handleConnect = async socket => {
      emit(
        setLoading({
          isLoading: true,
          loadingText: 'Sending the firmware file to the unit...',
        })
      );

      // 1. Send the file length
      await socketWritePromisfy(socket, Buffer.from(fileSizeInBytes.buffer));

      // 2. Send the checksum
      const checksum = splitFile(firmwareFile, 16, 4);
      await socketWritePromisfy(socket, Buffer.from(checksum.buffer));

      // 3. Send the zero byte
      await socketWritePromisfy(
        socket,
        Buffer.from(convertIntToBytes(0).buffer)
      );

      socket.pipe(socket);

      for (let i = 0; i < firmwareFile.length; i += CHUNK_SIZE) {
        const splittedData = splitFile(firmwareFile, i, CHUNK_SIZE);
        const completedPointer = i + CHUNK_SIZE;
        const curPercentage = Math.floor(
          (completedPointer / firmwareFile.length) * 100
        );
        const curFileContent = Buffer.from(splittedData.buffer);

        /* eslint-disable no-await-in-loop */
        await socketWritePromisfy(socket, curFileContent);
        /* eslint-enable no-await-in-loop */

        emit(
          setLoading({
            isLoading: true,
            loadingText: `Uploading the firmware file to the unit...(${curPercentage}%)`,
          })
        );

        if (curPercentage === 100) {
          emit(setUploadCompleted());
          socket.destroy();
          emit(END);
        }
      }
    };
    server.on(SERVER_LISTENER_CONNECTION, handleConnect);

    return () => {
      server.removeListener(SERVER_LISTENER_CONNECTION, handleConnect);
    };
  });

  yield call(listenServerPromise, server, localAddress);

  return eventChannelInstance;
}

function* pingUnit(address) {
  try {
    const { pingResponse, pingTimeout } = yield race({
      pingResponse: call(TCPSendMessage, address, '<{"ack": 1}>'),
      pingTimeout: delay(1000),
    });

    if (!pingResponse || pingTimeout) {
      throw new Error('No response when trying to ping unit');
    }
  } catch (error) {
    return false;
  }

  return true;
}

function* watchUploadFirmwareTask(channel, ipAddress) {
  try {
    while (true) {
      const { uploadFirmwareAction, uploadFirmwareActionTimeout } = yield race({
        uploadFirmwareAction: take(channel),
        uploadFirmwareActionTimeout: delay(5000),
      });

      if (uploadFirmwareActionTimeout) {
        // If timeout, check whether this unit is pingable or not
        const canPing = yield call(pingUnit, ipAddress);
        if (!canPing) yield put(unitDisconnect());
      } else if (uploadFirmwareAction.type) {
        yield put(uploadFirmwareAction);
      }
    }
  } finally {
    if (yield cancelled()) {
      channel.close();
    }
  }
}

export function* getLatestFirmwareVersion(email) {
  const response = yield call(ApiService.post, 'getfirmwareupdate', {
    requestData: {
      type: 1,
      email,
    },
  });

  return response && response.latestFirmwareVersion
    ? response.latestFirmwareVersion
    : undefined;
}

function* getUnitShadowState({ thingName, key, host }) {
  for (let i = 0; i < 5; i += 1) {
    try {
      const shadowState = yield call(getShadowState, {
        thingName,
        key,
        id: host,
      });

      return shadowState;
    } catch (error) {
      yield delay(5000);
    }
  }

  try {
    // If not able to get the wlan shadow state after 5 times, try to get it from rest api
    const { email } = yield select(state => state.user.profile);

    const unitShadowData = yield call(ApiService.post, 'publishdevicestate', {
      requestData: {
        type: 1,
        username: email,
        thingName,
        key,
      },
    });

    return unitShadowData;
  } catch (error) {
    throw new Error(
      'Error while trying to verify firmware version. Please refresh the unit screen manually to verify and retry if update is still available.'
    );
  }
}

function* setStartOTA(thingName, key) {
  const localAddress = yield call(NetworkHelper.getLocalIPAddress);
  yield call(setShadowState, {
    thingName,
    key,
    shadowState: {
      ota_flag: 1,
      port: OTA_PORT.toString(),
      ip: localAddress,
    },
  });
}

function checkIsUpToDate(curVersion, latestVersion) {
  const curVersionNr = curVersion ? curVersion.slice(1) : undefined;
  const latestVersionNr = latestVersion ? latestVersion.slice(1) : undefined;

  if (!curVersionNr || !latestVersionNr) {
    throw new Error('Error while checking for updates...');
  }

  return (
    curVersion &&
    latestVersion &&
    (curVersion.charCodeAt(0) === latestVersion.charCodeAt(0) &&
      VersionCompare(latestVersionNr, curVersionNr)) === 0
  );
}

function* getWLANUnits(isBroadcast, timeout, udpClient) {
  const targetAddress = yield call(NetworkHelper.getBroadcastAddress);

  const response = yield call(sendMessage, {
    isBroadcast,
    message: WLAN_DISCOVER_MESSAGE,
    targetAddress,
    timeout,
    noResponseMessage:
      "We're unable to detect any available units. Please ensure the units you would like to control are connect to the same WLAN.",
    udpClient,
  });

  // Process the response and also remove the duplicate records
  const processedResponse = response.reduce((acc, item) => {
    const { data, source } = item;
    const slicedData = data.slice(0, -1).replace(/'/g, '"');
    const dataObj = JSON.parse(slicedData);
    const unitData = dataObj[LAN_DEV_DETECT];

    const { thingName } = unitData;

    /* eslint-disable no-param-reassign */
    if (!acc[thingName]) acc[thingName] = { data: unitData, source };
    /* eslint-enable no-param-reassign */

    return acc;
  }, {});

  return processedResponse;
}

function* findWLANUnit(targetThingName, isBroadcast, timeout, udpClient) {
  const wlanUnits = yield call(getWLANUnits, isBroadcast, timeout, udpClient);

  return Object.values(wlanUnits).find(
    ({ data }) => data.thingName === targetThingName
  );
}

function* upgradeFirmwareTask(action) {
  let sendFirmwareFileChannel;
  let server;
  let udpClient;
  let uploadFirmwareTask;
  try {
    yield put(clearUpdateStatus());
    yield put(
      setLoading({
        isLoading: true,
        loadingText: 'Refresh unit before performing firmware upgrade...',
      })
    );
    const { thingName } = action.firmwareUpgradeParams;
    // Refresh the unit shadow before perform ota update
    yield call(refreshUnit);

    const curStore = yield select(state => state);
    const curRemoteUnit = yield call(getUnit, curStore, thingName);
    const { email } = curStore.user.profile;
    const { firmwareVersion: localFirmwareVersion } = curStore.settings || {};
    const targetAddress = yield call(NetworkHelper.getBroadcastAddress);

    // To tackle the situation where user kill the app previously when undergo ota update
    if (curRemoteUnit.otaRunning === 1) {
      throw new Error(
        'Please restart your unit before perform firmware upgrade'
      );
    }

    if (!curRemoteUnit.status || curRemoteUnit.status === 'disconnected') {
      throw new Error(
        'Unit is disconnected to the network, unable to perform the firmware update'
      );
    }

    yield put(
      setLoading({
        isLoading: true,
        loadingText:
          'Checking that the unit is connected to the same network...',
      })
    );

    if (targetAddress === null) {
      throw new Error('Unit is not connected to the same network');
    }

    // From firmware version >= 1.0.3, ip address can be obtained from /getip
    const ipAddrResponse = yield call(ApiService.post, 'getip', {
      requestData: {
        email,
        thingName,
      },
    });

    let targetUnitIPAddr =
      ipAddrResponse && ipAddrResponse.ip ? ipAddrResponse.ip : '';

    let wlanUnit;

    // If the unit is connect to the same network, it can be located through WLAN
    // Only scan the nearby unit if not able to get the ip address through api
    if (!targetUnitIPAddr) {
      for (let i = 1; i <= 3; i += 1) {
        udpClient = UDPService.createSocket({ type: 'udp4', reuseAddr: true });

        wlanUnit = yield call(
          findWLANUnit,
          thingName,
          action.firmwareUpgradeParams.ios,
          FIND_WLAN_UNIT_TIMEOUT,
          udpClient
        );

        // Exit the loop if wlanUnit can be found
        if (wlanUnit) {
          targetUnitIPAddr = wlanUnit.source.address;
          break;
        }

        // Else retry until it reached max of 3
        yield put(
          setLoading({
            isLoading: true,
            loadingText: `Checking that the unit is connected to the same network... (${i}/3)`,
          })
        );

        yield delay(500);
      }
    }

    if (!targetUnitIPAddr) {
      throw new Error('Unit is not connected to the same network');
    }

    yield put(
      setLoading({ isLoading: true, loadingText: 'Checking for updates...' })
    );

    const curFirmwareVersion = curRemoteUnit.firmwareVersion;

    // Retrieve the latest firmware version through api
    const latestFirmwareVersion = yield call(getLatestFirmwareVersion, email);

    if (checkIsUpToDate(latestFirmwareVersion, curFirmwareVersion)) {
      throw new Error('This is already the latest version');
    }

    const isLocalFirmwareExist = yield call(
      FileSystem.exists,
      FIRMWARE_FILE_PATH
    );
    // Download the firmware file if it local firmware file is not the latest
    // or the local firmware file is not exists
    const needDownloadFromServer =
      latestFirmwareVersion !== localFirmwareVersion || !isLocalFirmwareExist;

    yield put(
      setLoading({
        isLoading: true,
        loadingText: needDownloadFromServer
          ? 'Downloading the latest firmware file from server...'
          : 'Retrieving the latest firmware file from local storage...',
      })
    );
    const firmwareFile = needDownloadFromServer
      ? yield call(getFirmwareFile, email, thingName)
      : yield call(FileSystem.readFile, FIRMWARE_FILE_PATH, 'base64');

    if (!firmwareFile) {
      throw new Error(
        'Error while getting the firmware file from server/local storage.\nPlease try again later'
      );
    }

    // Only stored the firmware file if the current local firmware file is not the latest
    if (needDownloadFromServer) {
      yield call(
        FileSystem.writeFile,
        FIRMWARE_FILE_PATH,
        firmwareFile,
        'base64'
      );

      yield put(setFirmwareVersion(latestFirmwareVersion));
    }

    const firmwareFileBuffer = Buffer.from(firmwareFile, 'base64');

    yield put(
      setLoading({
        isLoading: true,
        loadingText: 'Initiate firmware update...',
      })
    );

    yield delay(5000);
    server = TCPHelper.createServer();
    yield delay(3000);
    sendFirmwareFileChannel = yield call(
      createFirmwareUploadChannel,
      server,
      firmwareFileBuffer
    );

    uploadFirmwareTask = yield fork(
      watchUploadFirmwareTask,
      sendFirmwareFileChannel,
      targetUnitIPAddr
    );
    yield call(setStartOTA, thingName, curRemoteUnit.key);

    yield take(SET_UPLOAD_COMPLETED);
    if (uploadFirmwareTask) yield cancel(uploadFirmwareTask);

    yield put(
      setLoading({
        isLoading: true,
        loadingText: 'Waiting for the unit to be restarted...',
      })
    );

    yield delay(30000);

    yield put(
      setLoading({
        isLoading: true,
        loadingText: 'Verifying the firmware version....',
      })
    );

    const unitShadowState = yield call(getUnitShadowState, {
      thingName: curRemoteUnit.thingName,
      key: curRemoteUnit.key,
      host: targetUnitIPAddr,
    });

    // Verify the firmware version
    const isUpgradeSuccess =
      unitShadowState && unitShadowState.version === latestFirmwareVersion;

    if (isUpgradeSuccess) {
      // Update the firmware version
      const allUnits = yield select(
        stateInStore => stateInStore.units.allUnits
      );
      const prevShadowData = allUnits[curRemoteUnit.thingName];
      const curUnit = shadowStateMapper(unitShadowState, prevShadowData);
      yield put(setUnit({ ...curUnit, needUpgrade: '' }));
      yield put(
        setUpdateStatus({
          messageType: 'success',
          message: 'Firmware update completed',
        })
      );
    }
  } catch (error) {
    yield put(
      setUpdateStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    if (server) server.close();
    if (udpClient) udpClient.close();
    if (uploadFirmwareTask) yield cancel(uploadFirmwareTask);

    yield put(
      setLoading({
        isLoading: false,
        loadingText: '',
      })
    );
  }
}

export default function* updateFirmware(action) {
  const { isDisconnect } = yield race({
    task: call(upgradeFirmwareTask, action),
    isDisconnect: take(UNIT_DISCONNECT),
    cancel: take(CANCEL_FIRMWARE_UPGRADE),
  });

  if (isDisconnect) {
    yield put(
      setUpdateStatus({
        messageType: 'error',
        message:
          'Fail to perform firmware upgrade as unit is disconnected from network.',
      })
    );
  }
}
