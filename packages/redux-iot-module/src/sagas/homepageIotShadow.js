import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import produce from 'immer';

import { unitLoading, setToastMsg } from '../actions/homepageIotShadow';
import {
  setAllUnits,
  setGroups,
  setUnitStatus,
  setUnitsLoading,
} from '../actions/units';

import { getUnitShadow } from './remote';

// Get the group with updated switch value by thingName
export function* getGroupByUnit({ thingName, groupIndex, newUnits }) {
  const {
    units: {
      allUnits,
      groupUnits,
      groupInfo: { groups },
    },
  } = yield select(state => state);

  const curAllUnits = newUnits || allUnits;

  const unit = curAllUnits[thingName];
  const groupUnitIndex = unit.groupIndex;

  const groupSwitchValue = groupUnits[groupUnitIndex].every(
    curThingName =>
      curAllUnits[curThingName].status === 'disconnected' ||
      curAllUnits[curThingName].switch === 1
  );

  const groupInfoIndex = Object.values(groups).findIndex(
    ({ groupIndex: curGroupIndex }) => curGroupIndex === Number(groupIndex)
  );

  // Update the group switch as well
  const newGroups = produce(groups, draftState => {
    draftState[groupInfoIndex].switch = groupSwitchValue;
  });

  return newGroups;
}

export function* switchAc(action) {
  try {
    const unitLoadingIndicatorInit = { [action.thingName]: true };

    yield put(unitLoading(unitLoadingIndicatorInit));
    const unitShadow = yield call(getUnitShadow, {
      thingName: action.thingName,
    });

    const date = new Date();

    const switchType = action.switchType === 1 ? 'on' : 'off';

    if (unitShadow.otaRunning === 1) {
      throw new Error(
        `Unable to switch ${switchType} this unit as firmware upgrade is still running.
      \nPlease wait until firmware upgrade is completed`
      );
    }

    const response = yield call(ApiService.post, 'setdevicestatus', {
      requestData: {
        type: 1,
        thingName: action.thingName,
        field: 'Set_OnOff',
        value: action.switchType,
      },
    });

    if (response.Set_OnOff === action.switchType) {
      const units = yield select(state => state.units.allUnits);

      console.log("redux-iot-module/src/sagas/homepageIotShadow.js");
      console.log(units);

      const curThingName = response.thingName;

      const newUnits = produce(units, draftState => {
        draftState[curThingName].switch = response.Set_OnOff;
      });

      const newGroups = yield call(getGroupByUnit, {
        thingName: response.thingName,
        groupIndex: action.groupIndex,
        newUnits,
      });

      console.log("newUnits = ");
      console.log(newUnits);

      yield put(setGroups({ groups: newGroups, lastGroupIndex: 0 }));
      yield put(setAllUnits(newUnits, date.getTime()));
    }
  } catch (error) {
    yield put(setToastMsg(error.message));
  } finally {
    const unitLoadingIndicatorEnd = { [action.thingName]: false };
    yield put(unitLoading(unitLoadingIndicatorEnd));
  }
}

function* retrieveMutipleUnits(units) {
  const {
    user: {
      profile: { email },
    },
    units: { allUnits },
  } = yield select(state => state);

  const thingArr = units.map(({ thingName }) => {
    const curUnit = allUnits[thingName];

    return { thingName, key: curUnit.key };
  });

  const unitsReponse = yield call(ApiService.post, 'publishdevicestate', {
    requestData: {
      type: 2,
      username: email,
      thingArr,
    },
  });

  return unitsReponse;
}

export function* switchGroupAc(action) {
  try {
    yield put(setUnitsLoading(true));

    const {
      user: {
        profile: { email },
      },
      units: {
        allUnits,
        groupUnits,
        groupInfo: { groups },
      },
    } = yield select(state => state);

    // Only process the connected units
    const connectedUnits = groupUnits[action.groupIndex]
      .map(thingName => allUnits[thingName])
      .filter(unit => unit.status === 'connected');

    const unitsFrmServer = yield call(retrieveMutipleUnits, connectedUnits);
    const targetedUnits = unitsFrmServer.filter(
      ({ data }) => data.ota_flag === 0
    );

    const switchType = action.switchType ? 'on' : 'off';
    if (!targetedUnits || targetedUnits.length === 0) {
      throw new Error(
        `Unable to switch ${switchType} units as firmware upgrade is still running.
        \nPlease wait until firmware upgrade is completed`
      );
    } else if (connectedUnits.length > targetedUnits.length) {
      yield put(
        setToastMsg(
          `Unable to switch ${switchType} some units as firmware upgrade is still running.
          \nPlease wait until firmware upgrade is completed`
        )
      );
    }

    const date = new Date();

    const thingArr = targetedUnits.map(({ thingName }) => {
      const { key } = allUnits[thingName];
      return {
        thingName,
        key,
        payload: {
          state: { desired: { Set_OnOff: Number(action.switchType) } },
        },
      };
    });

	console.log("CALLING API publishdevicestate"); // ADDED BY JUN
    const response = yield call(ApiService.post, 'publishdevicestate', {
      requestData: {
        type: 4,
        username: email,
        thingArr,
      },
    });
	console.log("AFTER CALLING API"); // ADDED BY JUN

    // Only update the units if there is response returned
    if (response && response.message === 'success') {
      const newUnits = produce(allUnits, draftState => {
        thingArr.forEach(({ thingName }) => {
          draftState[thingName].switch = Number(action.switchType);
        });
      });

      const groupSwitchValue = groupUnits[action.groupIndex].every(
        thingName =>
          newUnits[thingName].status === 'disconnected' ||
          newUnits[thingName].switch === 1
      );

      const groupInfoIndex = Object.values(groups).findIndex(
        ({ groupIndex }) => groupIndex === action.groupIndex
      );

      const newGroups = produce(groups, draftState => {
        draftState[groupInfoIndex].switch = groupSwitchValue;
      });

      // Update the units
      yield put(setGroups({ groups: newGroups, lastGroupIndex: 0 }));
      yield put(setAllUnits(newUnits, date.getTime()));
      yield put(
        setUnitStatus({
          messageType: 'success',
          message: 'success',
        })
      );
    }
  } catch (error) {
    yield put(setToastMsg(error.message));
  } finally {
    yield put(setUnitsLoading(false));
  }
}
