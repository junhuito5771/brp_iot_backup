import { eventChannel } from 'redux-saga';
import {
  call,
  put,
  fork,
  take,
  cancel,
  select,
  race,
  delay,
  join,
} from 'redux-saga/effects';
import { ConnectionType } from '@module/utility';

import {
  BLECONTROL_DISCONNECT,
  setBLEControlStatus,
  clearBLEControlStatus,
  disconnectBLE,
  setPairLoading,
} from '../../actions/bleControl';
import { setUnit } from '../../actions/units';
import { setRemoteUnit, setRemoteStatus } from '../../actions/remote';

import BLEService from '../../utils/BLE/BLEService';
// eslint-disable-next-line import/no-named-as-default-member
import ShadowStateKeyHelper from '../../utils/Remote/shadowStateKeyHelper';
import shadowStateMapper from '../../utils/shadowStateMapper';

export function setShadowState({ id, shadowState, thingName, key }) {
  const stateInStrng = JSON.stringify(shadowState);
  const message = `<{"thingName":"${thingName}","action":"setStatus","version":-1,"state":${stateInStrng},"key":"${key}"}>`;

  BLEService.sendMessage({
    id,
    message,
  });
}

function convertToJSON(text) {
  try {
    return JSON.parse(text);
  } catch (error) {
    return null;
  }
}

function getShadowState({ id, thingName, key }) {
  const message = `<{"thingName":"${thingName}","action":"getStatus","version":-1,"key":"${key}"}>`;

  BLEService.sendMessage({
    id,
    message,
  });
}

function* retreiveUnitState(channel) {
  try {
    let response = '';

    while (true) {
      const data = yield take(channel);
      if (data.indexOf('<{') > -1) {
        response = '';
      }

      response = response.concat(data);

      if (
        response.length > 0 &&
        response.indexOf('<{') === 0 &&
        response.indexOf('}>') === response.length - 2
      ) {
        // Process the response by replacing the whitespace, remove < and >
        response = response.replace(/\s|<|>/g, '');
        const responseInObj = convertToJSON(response);

        if (responseInObj) {
          const allUnits = yield select(state => state.units.allUnits);
          const currentUnit = allUnits[responseInObj.thingName];
          // Update the shadow to shadow
          return shadowStateMapper(responseInObj, currentUnit);
        }
        response = '';
      }
    }
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  }

  return null;
}

function* retrieveTargetUnit({ thingName, nrRetry = 3 }) {
  let peripheral = null;
  for (let i = 1; i <= nrRetry; i += 1) {
    try {
      peripheral = yield call(BLEService.getTargetUnit, thingName);

      if (peripheral) break;
      else {
        yield put(
          setPairLoading(
            `Unable to retrieve ID from the cache list, performing re-scan...(${i}/${nrRetry})`
          )
        );
        const { scanTimeout } = yield race({
          scan: call(BLEService.scan),
          scanTimeout: delay(20000),
        });

        if (scanTimeout) {
          throw new Error(
            'Bluetooth scanning process timeout. Please try again.'
          );
        }
      }
    } catch (error) {
      yield delay(1000);
    }
  }

  return peripheral;
}

export function* connectBleUnit(action) {
  let processBLEResponseTask;
  let channel;
  let id = '';
  let notifyID;
  const { thingName } = action.connectParams;
  try {
    yield put(clearBLEControlStatus());
    yield put(setPairLoading('Initiating unit pairing process...'));

    const peripheral = yield call(retrieveTargetUnit, { thingName });

    if (!peripheral) {
      throw new Error(
        'This unit is no longer valid.\n\nPlease refresh the list to display the latest units.'
      );
    }

    id = peripheral.id;

    yield put(setPairLoading('Connecting to unit'));

    const { service, timeout } = yield race({
      service: call(BLEService.connectAndRetrieveService, peripheral),
      timeout: delay(10000),
    });

    if (timeout) {
      throw new Error(
        'Connection to unit timeout. Please refresh the list and try again.'
      );
    } else if (!service) {
      throw new Error(
        'This unit is no longer valid.\n\nPlease refresh the list to display the latest units.'
      );
    }

    yield put(
      setRemoteUnit({
        thingName,
        connectionID: peripheral.id,
        connectionType: ConnectionType.BLE_MDOE,
      })
    );

    yield put(setPairLoading('Retrieving latest state of unit...'));

    const allUnits = yield select(state => state.units.allUnits);
    const { firmwareVersion } = allUnits[thingName];
    const { key } = allUnits[thingName];
    notifyID = ShadowStateKeyHelper.checkIsV1(firmwareVersion)
      ? '2bb2'
      : '2bb1';

    channel = eventChannel(emit => {
      BLEService.addNotificationListerner(emit);

      return () => BLEService.stopNotification(peripheral, notifyID);
    });

    processBLEResponseTask = yield fork(retreiveUnitState, channel);

    yield call(getShadowState, { id, thingName, key });
    BLEService.startNotification(peripheral, notifyID);

    const { shadowData, processBleTimeout } = yield race({
      shadowData: join(processBLEResponseTask),
      processBleTimeout: delay(30000),
    });

    if (shadowData) {
      yield put(setUnit(shadowData));

      yield put(
        setBLEControlStatus({
          messageType: 'success',
          message: 'success',
        })
      );
    } else if (processBleTimeout) {
      throw new Error(
        'This unit might have been removed from your account.\n\nPlease log in to refresh the unit.'
      );
    }
  } catch (error) {
    yield put(
      setBLEControlStatus({
        messageType: 'error',
        message: typeof error === 'string' ? error : error.message,
      })
    );

    if (id) {
      BLEService.disconnect({ id });
    }
  } finally {
    if (channel) {
      channel.close();
    }
    if (processBLEResponseTask) {
      yield cancel(processBLEResponseTask);
    }
    if (id) {
      BLEService.stopNotification({ id }, notifyID);
    }
    yield put(setPairLoading(''));
  }
}

function* listenUpdateBLEChannel(channel) {
  try {
    let response = '';

    while (true) {
      const data = yield take(channel);
      if (data.indexOf('<{') > -1) {
        response = '';
      }

      response = response.concat(data);

      if (
        response.length > 0 &&
        response.indexOf('<{') === 0 &&
        response.indexOf('}>') === response.length - 2
      ) {
        // Process the response by replacing the whitespace, remove < and >
        response = response.replace(/\s|<|>/g, '');
        const responseInObj = convertToJSON(response);

        if (responseInObj) {
          const allUnits = yield select(state => state.units.allUnits);
          const currentUnit = allUnits[responseInObj.thingName];
          // Update the shadow to shadow
          const unitData = shadowStateMapper(responseInObj, currentUnit);

          yield put(setUnit(unitData));
        }
        response = '';
      }
    }
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );

    yield put(disconnectBLE());
  }
}

function* start() {
  let channel;
  let notifyID;
  let processBleTask;
  try {
    const {
      remote: { thingName, connectionID },
      units: { allUnits },
    } = yield select(state => state);

    const curUnit = allUnits[thingName];
    const { firmwareVersion } = curUnit;
    notifyID = ShadowStateKeyHelper.checkIsV1(firmwareVersion)
      ? '2bb2'
      : '2bb1';

    // Create process ble response channel
    channel = eventChannel(emit => {
      BLEService.addNotificationListerner(emit);

      return () => BLEService.stopNotification({ id: connectionID }, notifyID);
    });

    processBleTask = yield fork(listenUpdateBLEChannel, channel);

    BLEService.startNotification({ id: connectionID }, notifyID);

    yield take(BLECONTROL_DISCONNECT);
    yield cancel(processBleTask);
    yield call(BLEService.disconnect, { id: connectionID });
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    if (channel) channel.close();
    if (processBleTask) yield cancel(processBleTask);
  }
}

function* end() {
  yield put(disconnectBLE());
}

export default {
  setShadowState,
  getShadowState,
  start,
  end,
};
