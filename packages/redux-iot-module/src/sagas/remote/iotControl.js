import { eventChannel } from 'redux-saga';
import {
  put,
  select,
  call,
  take,
  fork,
  cancel,
  delay,
} from 'redux-saga/effects';
import { ApiService, syncAmplifyTime } from '@module/utility';

import { setUnit, setGroups } from '../../actions/units';
import {
  setReconnect,
  setRemoteStatus,
  setConnectionStatus,
} from '../../actions/remote';
import PubSubHelper from '../../utils/pubSubHelper';
import shadowStateMapper from '../../utils/shadowStateMapper';
import ConnectionStatus from '../../utils/connectionStatus';
import Configuration from '../../configuration';
import { autoAttachIoTPolicy } from './attachIoTPolicy';

import { refreshUnit } from './refreshUnit';
import { getGroupByUnit } from '../homepageIotShadow';
import { checkExpiryTime } from '../../actions/controlUsageLimit';

const MAX_RETRY = Number.MAX_SAFE_INTEGER;

function* getRemoteUnit() {
  const allUnits = yield select(state => state.units.allUnits);
  const thingName = yield select(state => state.remote.thingName);

  return allUnits[thingName];
}

export function* setShadowState({ thingName, shadowState, key }) {
  const msg = {
    state: {
      desired: shadowState,
    },
  };

  const { email } = yield select(state => state.user.profile);

  const responseData = yield call(ApiService.post, 'publishdevicestate', {
    requestData: {
      type: 3,
      username: email,
      thingName,
      key,
      payload: msg,
    },
  });

  return responseData && responseData.message === 'success';
}

function* listenProcessIoTMQTT(thingName) {
  let channel;
  let refreshTask;
  let curRetryNr = 0;

  // loop until connection has been established
  while (curRetryNr < MAX_RETRY) {
    try {
      // Refresh the unit when we first naviga†e to remote screen
      refreshTask = yield fork(refreshUnit, false);
      channel = eventChannel(emit => {
        const topic = `$aws/things/${thingName}/shadow/update/documents`;

        PubSubHelper.subscribeWithEmit({ topic, emit });
        return () => PubSubHelper.unsubscribe({ topic });
      });

      while (true) {
        // Track unit usage limit expiring status
        yield put(checkExpiryTime({ thingName }));

        const data = yield take(channel);

        // Throw error if channel emit error
        if (data.type === 'error') throw new Error(data.message);

        // To ensure we are getting the latest shadow state
        const curUnit = yield call(getRemoteUnit);
        const curVersion = curUnit.version;

        const shadowData = data.value;

        if (
          shadowData &&
          shadowData.current &&
          shadowData.current.state &&
          shadowData.current.state.reported
        ) {
          const reportedShadowData = shadowData.current.state.reported;
          const reportedVersion = reportedShadowData
            ? Number(shadowData.current.version)
            : -1;
          const isCurrentUnit =
            reportedShadowData &&
            reportedShadowData.thingName === curUnit.thingName;

          if (
            reportedShadowData &&
            isCurrentUnit &&
            curVersion < reportedVersion
          ) {
            const reportedUnit = shadowStateMapper(
              reportedShadowData,
              curUnit,
              reportedVersion
            );
            if (reportedUnit.status === 'connected') {
              yield put(setReconnect(false));
            }

            console.log("redux-iot-module/src/sagas/remote/iotControl.js");
            console.log(reportedUnit);
            yield put(setUnit(reportedUnit));

            // Only update group if current switch value not equal to previous switch value
            if (curUnit.switch !== reportedUnit.switch) {
              const newGroups = yield call(getGroupByUnit, {
                thingName: reportedUnit.thingName,
                groupIndex: curUnit.groupIndex,
              });

              yield put(setGroups({ groups: newGroups, lastGroupIndex: 0 }));
            }
          }
        }

        yield put(setConnectionStatus(ConnectionStatus.CONNECTED));
      }
    } catch (error) {
      curRetryNr += 1;
      yield put(setConnectionStatus(ConnectionStatus.RECONNECTING));
      // Backoff algorithm delay (1s 2s 3s 4s 5s).
      const delayMs = curRetryNr >= 5 ? 5000 : 1000 * curRetryNr;
      yield delay(delayMs);
    } finally {
      if (channel) channel.close();
      if (refreshTask) cancel(refreshTask);
    }
  }
}

export function* start({ thingName }) {
  try {
    yield call(syncAmplifyTime);
    yield call(autoAttachIoTPolicy);
    // Init pubsub
    yield call(PubSubHelper.init, Configuration.getIoTConfig());
    // Validate whether this unit is belong to the user

    yield call(listenProcessIoTMQTT, thingName);
  } catch (error) {
    yield put(setRemoteStatus({ type: 'error', error: error.message }));
  }
}

export function* end(thingName) {
  yield call(PubSubHelper.unsubscribe, {
    topic: `$aws/things/${thingName}/shadow/update/accepted`,
  });
}

export default {
  setShadowState,
  start,
  end,
};
