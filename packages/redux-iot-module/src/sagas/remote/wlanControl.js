import { eventChannel } from 'redux-saga';
import {
  call,
  put,
  select,
  race,
  delay,
  retry,
  fork,
  cancel,
  take,
} from 'redux-saga/effects';
import TCPHelper from 'react-native-tcp';
import { ConnectionType, StringConverter } from '@module/utility';

import { getUnit } from '../../selector/index';
import {
  setPairLoading,
  setStatus,
  WLAN_DISCONNECT,
  wlanDisconnect,
} from '../../actions/wlanMode';
import { setRemoteUnit, setRemoteStatus } from '../../actions/remote';
import { setUnit } from '../../actions/units';
import shadowStateMapper from '../../utils/shadowStateMapper';
import CryptoHelper from '../../utils/CryptoHelper';

const DEFAULT_TCP_PORT = 7002;
const TCP_LISTENER_CONNECT = 'connect';
const TCP_LISTENER_DATA = 'data';
const TCP_LISTENER_ERROR = 'error';
const TCP_LISTENER_END = 'end';
const DEFAULT_CONNECTION_TIMEOUT = 10000;

function clearClientAllListeners(client) {
  client.removeAllListeners(TCP_LISTENER_CONNECT);
  client.removeAllListeners(TCP_LISTENER_ERROR);
  client.removeAllListeners(TCP_LISTENER_DATA);
  client.removeAllListeners(TCP_LISTENER_END);
}

function createClient(host, timeout) {
  let connectionTimeout = timeout;
  if (!connectionTimeout) {
    connectionTimeout = DEFAULT_CONNECTION_TIMEOUT;
  }
  const client = TCPHelper.createConnection({
    port: DEFAULT_TCP_PORT,
    host,
  });

  return new Promise((resolve, reject) => {
    try {
      const connectTimeout = setTimeout(() => {
        client.destroy();
        reject(new Error('Please try again.'));
      }, connectionTimeout);

      client.on(TCP_LISTENER_CONNECT, () => {
        clearTimeout(connectTimeout);
        clearClientAllListeners(client);
        resolve(client);
      });

      client.on(TCP_LISTENER_ERROR, error => reject(error));
    } catch (error) {
      reject(error);
    }
  });
}

export function* sendMessage(host, message) {
  const client = yield call(createClient, host);
  return yield new Promise((resolve, reject) => {
    try {
      client.once(TCP_LISTENER_DATA, data => {
        client.destroy();
        resolve(StringConverter.ByteToString(data));
      });

      client.once(TCP_LISTENER_ERROR, err => {
        clearClientAllListeners(client);
        reject(err);
      });

      client.once(TCP_LISTENER_END, () => {
        clearClientAllListeners(client);
      });
      client.write(message);
    } catch (error) {
      reject(error);
    }
  });
}

function* setShadowState({ thingName, shadowState, key, id }) {
  const stateInString = JSON.stringify(shadowState);
  const payload = `{"action":"setStatus","version":-1,"state":${stateInString},"thingName":"${thingName}"}`;

  const encryptedText = CryptoHelper.encryption({
    type: CryptoHelper.TYPE_AES_128_CBC,
    text: payload,
    key,
    iv: key,
  });

  let response = yield call(
    sendMessage,
    id,
    `<{"payload":"${encryptedText}"}>`
  );
  response = response.replace(/\s|<|>/g, '');

  const responseInObj = JSON.parse(response);
  const { payload: responsePayload } = responseInObj;
  // Need to take the payload from response
  const decryptedPayload = CryptoHelper.decryption({
    type: CryptoHelper.TYPE_AES_128_CBC,
    text: responsePayload,
    key,
    iv: key,
  });
  const shadowResponseState = decryptedPayload.replace(/\s/g, '');
  const shadowStateInObj = JSON.parse(shadowResponseState);

  const allUnits = yield select(stateInStore => stateInStore.units.allUnits);
  const prevShadowData = allUnits[thingName];
  yield put(setUnit(shadowStateMapper(shadowStateInObj, prevShadowData)));
}

export function* getShadowState({ thingName, key, id }) {
  const payload = `{"action":"getStatus","version":-1,"thingName":"${thingName}"}`;

  const encryptedPayload = CryptoHelper.encryption({
    type: CryptoHelper.TYPE_AES_128_CBC,
    text: payload,
    key,
    iv: key,
  });

  const sendMessageResult = yield race({
    response: call(sendMessage, id, `<{"payload":"${encryptedPayload}"}>`),
    timeout: delay(5000),
  });

  let { response } = sendMessageResult;
  const { timeout } = sendMessageResult;

  if (timeout) {
    throw new Error(
      'Target unit might have been removed from your account or disconnected from your current network. Please log in to your account to verify.'
    );
  }

  response = response.replace(/\s|<|>/g, '');
  let responseInObj;
  try {
    responseInObj = JSON.parse(response);
  } catch (error) {
    throw new Error('Incorrect response format received');
  }

  const { payload: responsePayload } = responseInObj;
  // Need to take the payload from response
  const decryptedPayload = CryptoHelper.decryption({
    type: CryptoHelper.TYPE_AES_128_CBC,
    text: responsePayload,
    key,
    iv: key,
  });
  const shadowResponseState = decryptedPayload.replace(/\s/g, '');

  let shadowStateInObj;

  try {
    shadowStateInObj = JSON.parse(shadowResponseState);
  } catch (error) {
    throw new Error(
      "Target unit's key might have expired. Please login to get the latest unit's key."
    );
  }

  return shadowStateInObj;
}

function processEncryptedPayload(data, key) {
  const payload = data.replace(/\s|<|>/g, '');
  let payloadInObj;
  try {
    payloadInObj = JSON.parse(payload);
  } catch (error) {
    payloadInObj = undefined;
  }

  let shadowStateInObj;

  if (payloadInObj) {
    const { payload: responsePayload } = payloadInObj;
    // Need to take the payload from response
    const decryptedPayload = CryptoHelper.decryption({
      type: CryptoHelper.TYPE_AES_128_CBC,
      text: responsePayload,
      key,
      iv: key,
    });
    const shadowResponseState = decryptedPayload.replace(/\s/g, '');

    try {
      shadowStateInObj = JSON.parse(shadowResponseState);
    } catch (error) {
      shadowStateInObj = undefined;
    }
  }

  return shadowStateInObj;
}

function* processShadowStateTask(channel, { key, thingName }) {
  while (true) {
    try {
      const { type, data, error } = yield take(channel);

      if (type === TCP_LISTENER_DATA) {
        const shadowStateInObj = yield call(processEncryptedPayload, data, key);

        const allUnits = yield select(state => state.units.allUnits);
        const prevShadowData = allUnits[thingName];
        yield put(setUnit(shadowStateMapper(shadowStateInObj, prevShadowData)));
      } else if (type === TCP_LISTENER_ERROR) {
        yield put(
          setRemoteStatus({
            messageType: 'error',
            message: error.message,
          })
        );
      }
    } catch (error) {
      // Empty catch block since we need to keep it going regardless got error or not
    }
  }
}

function* sendGetStatusCommand({ client, thingName, key }) {
  while (true) {
    const payload = `{"action":"getStatus","version":-1,"thingName":"${thingName}"}`;

    const encryptedPayload = CryptoHelper.encryption({
      type: CryptoHelper.TYPE_AES_128_CBC,
      text: payload,
      key,
      iv: key,
    });

    const message = `<{"payload":"${encryptedPayload}"}>`;

    client.write(message);

    yield delay(5000);
  }
}

function* updateShadowState(unit) {
  const client = yield call(createClient, unit.id);

  const channel = eventChannel(emit => {
    client.on(TCP_LISTENER_DATA, data => {
      emit({
        type: TCP_LISTENER_DATA,
        data: StringConverter.ByteToString(data),
      });
    });

    client.on(TCP_LISTENER_END, () => {
      emit({ type: TCP_LISTENER_END });
    });

    client.on(TCP_LISTENER_ERROR, error =>
      emit({ type: TCP_LISTENER_ERROR, error })
    );

    return () => {
      client.removeAllListeners(TCP_LISTENER_DATA);
      client.removeAllListeners(TCP_LISTENER_END);
      client.removeAllListeners(TCP_LISTENER_ERROR);
    };
  });

  yield fork(processShadowStateTask, channel, unit);

  yield fork(sendGetStatusCommand, { ...unit, client });
}

export function* pairWLANUnit(action) {
  try {
    yield put(setPairLoading(true));
    const { host, thingName } = action.unitProps;
    const response = yield call(sendMessage, host, '<{"ack": 1}>');

    if (!response) {
      throw new Error(
        'This unit is no longer connected to the same network, please refresh the list.'
      );
    }

    const unit = getUnit(yield select(state => state), thingName);

    if (!unit || !unit.key) {
      throw new Error('Unable to find the unit in local storage...');
    }

    yield retry(3, 3000, getShadowState, {
      thingName,
      key: unit.key,
      id: host,
    });

    // If has response then set pair success
    // And also setting the remote thingname and mode
    yield put(
      setRemoteUnit({
        thingName,
        connectionType: ConnectionType.WLAN_MODE,
        connectionID: host,
      })
    );
    yield put(
      setStatus({
        messageType: 'pairSuccess',
        message: 'success',
      })
    );
  } catch (error) {
    yield put(
      setStatus({
        messageType: 'pairError',
        message: error.message,
      })
    );
  } finally {
    yield put(setPairLoading(false));
  }
}

function* intervalUpdateShadowState(unit) {
  try {
    while (true) {
      yield call(updateShadowState, unit);

      yield delay(1000);
    }
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  }
}

function* start(unit) {
  let updateTask;
  try {
    updateTask = yield fork(intervalUpdateShadowState, unit);

    yield take(WLAN_DISCONNECT);
    if (updateTask) cancel(updateTask);
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    if (updateTask) cancel(updateTask);
  }
}

function* end() {
  yield put(wlanDisconnect());
}

export default {
  setShadowState,
  getShadowState,
  start,
  end,
};
