import { put, select, call } from 'redux-saga/effects';
import { ApiService, ConnectionType } from '@module/utility';

import {
  setRemoteStatus,
  setLoading,
  restartStartTask,
  setConnectionStatus,
} from '../../actions/remote';
import { setUnit } from '../../actions/units';
import fetchTimer from '../fetchTimer';
import BLEControl from './bleControl';
import WLANControl from './wlanControl';

import shadowStateMapper from '../../utils/shadowStateMapper';
import ConnectionStatusEnum from '../../utils/connectionStatus';

export function* getUnitShadow({ thingName }) {
  const curStore = yield select(state => state);
  const {
    user: {
      profile: { email },
    },
    units: { allUnits },
  } = curStore;
  const remoteUnitShadowData = allUnits[thingName];

  const unitShadowData = yield call(ApiService.post, 'publishdevicestate', {
    requestData: {
      type: 1,
      username: email,
      thingName,
      key: remoteUnitShadowData.key,
    },
  });

  if (
    remoteUnitShadowData &&
    unitShadowData &&
    remoteUnitShadowData.thingName === unitShadowData.thingName &&
    remoteUnitShadowData.version <= unitShadowData.shadowStateVersion
  ) {
    const reportedUnit = shadowStateMapper(
      unitShadowData,
      remoteUnitShadowData,
      unitShadowData.shadowStateVersion
    );

    return reportedUnit;
  }

  return remoteUnitShadowData;
}

function* refreshUnitIoT({ thingName, connectionStatus, restartProcess }) {
  yield call(fetchTimer);

  if (
    restartProcess &&
    (connectionStatus === ConnectionStatusEnum.DISCONNECTED ||
      connectionStatus === ConnectionStatusEnum.RECONNECTING)
  ) {
    yield put(setConnectionStatus(ConnectionStatusEnum.CONNECTING));
    yield put(restartStartTask());
  } else {
    // Refresh unit call
    const unitData = yield call(getUnitShadow, {
      thingName,
    });

    console.log("redux-iot-module/src/sagas/remote/refreshUnit.js");
    console.log("unitData");

    yield put(setUnit(unitData));
    yield put(setConnectionStatus(ConnectionStatusEnum.CONNECTED));
  }
}

function getRefreshInterfaceByMode(mode) {
  switch (mode) {
    case ConnectionType.BLE_MDOE:
      return BLEControl.getShadowState;
    case ConnectionType.WLAN_MODE:
      // eslint-disable-next-line import/no-named-as-default-member
      return WLANControl.getShadowState;
    case ConnectionType.IOT_MODE:
      return refreshUnitIoT;
    default:
      return undefined;
  }
}

export function* refreshUnit(restartProcess = true) {
  try {
    yield put(setLoading(true));
    const {
      remote: { connectionType, thingName, connectionID, connectionStatus },
      units: { allUnits },
    } = yield select(state => state);

    const refreshInterface = yield call(
      getRefreshInterfaceByMode,
      connectionType
    );

    const { key } = allUnits[thingName];

    yield call(refreshInterface, {
      thingName,
      id: connectionID,
      key,
      connectionStatus,
      restartProcess,
    });
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: 'Error while refreshing the unit',
      })
    );
  } finally {
    yield put(setLoading(false));
  }
}
