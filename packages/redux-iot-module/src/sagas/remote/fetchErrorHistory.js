import { put, select, call } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setRemoteStatus,
  setLoading,
  setErrorHistory,
} from '../../actions/remote';

export default function* fetchErrorHistory({ value }) {
  yield put(setLoading(true));
  const { email } = yield select(state => state.user.profile);
  try {
    const res = yield call(ApiService.post, 'geterrorcode', {
      requestData: {
        username: email,
        thingName: value,
      },
    });

    yield put(
      setErrorHistory(
        res.sort((a, b) => new Date(b.updatedOn) - new Date(a.updatedOn))
      )
    );
  } catch (error) {
    setRemoteStatus({
      messageType: 'error',
      message: error.message,
    });
  } finally {
    yield put(setLoading(false));
  }
}
