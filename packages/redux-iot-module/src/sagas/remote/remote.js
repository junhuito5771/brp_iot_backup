import {
  put,
  select,
  call,
  fork,
  take,
  cancel,
  join,
  race,
  delay,
} from 'redux-saga/effects';
import { ConnectionType } from '@module/utility';

import {
  HandsetRuleHelper,
  ShadowStateKeyHelper,
  ShadowValueHelper,
  RemoteHelper,
} from '../../utils/Remote';

import {
  setRemoteStatus,
  clearRemoteStatus,
  SET_AC_TEMP,
  SET_AC_SWITCH,
  SET_FAN_SPEED,
  SET_TURBO,
  SET_SILENT,
  SET_MODE,
  SET_ECOPLUS,
  SET_SLEEP,
  SET_SWING,
  SET_SWING_LR,
  SET_SWING_3D,
  SET_BREEZE,
  SET_SMART_DRIFT,
  SET_STREAMER,
  SET_SENSE,
  SET_LED,
  SET_CK_SWING,
  UNMOUNT_UNIT,
  setExpectedTemp,
  clearDesireTemp,
  RESTART_START_TASK,
} from '../../actions/remote';
import {
  setUnitPrevConfig,
  removeUnitPrevConfigProp,
  getAllUnits,
} from '../../actions/units';
import BleControl from './bleControl';
import IoTControl from './iotControl';
import WlanControl from './wlanControl';

function* getRemoteUnit() {
  const allUnits = yield select(state => state.units.allUnits);
  const thingName = yield select(state => state.remote.thingName);

  return allUnits[thingName];
}

function* getRemoteInterfaceFromMode() {
  const currentMode = yield select(state => state.remote.connectionType);
  switch (currentMode) {
    case ConnectionType.BLE_MDOE:
      return BleControl;
    case ConnectionType.IOT_MODE:
      return IoTControl;
    case ConnectionType.WLAN_MODE:
      return WlanControl;
    default:
      return undefined;
  }
}

function getFanSpeedPayload(unit, fanLvl) {
  const { handsetType, firmwareVersion, enableSmartTurbo } = unit;

  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );
  const isSilent = fanLvl === RemoteHelper.FANSPEED_SILENT;
  const silentValue = isSilent ? 1 : 0;

  const { fan, fanExtend } = ShadowValueHelper.getShadowObjByFanLvl(
    handsetType,
    fanLvl
  );

  const turnOffBreeze = isSilent || fanLvl === RemoteHelper.FANSPEED_AUTO;

  return {
    ...(!isSilent && {
      [shadowKeys.SET_FAN]: fan,
      [shadowKeys.SET_FAN_EXTEND]: fanExtend,
    }),
    ...(!enableSmartTurbo &&
      ShadowValueHelper.getShadowObjByTurbo(
        RemoteHelper.TURBO_OFF,
        firmwareVersion
      )),
    [shadowKeys.SET_SILENT]: silentValue,
    ...(turnOffBreeze && { [shadowKeys.SET_BREEZE]: 0 }),
  };
}

function getTempOnModeChange(targetMode, unit) {
  const { prevConfig, firmwareVersion } = unit;
  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );

  const tempShadowKey = shadowKeys.SET_TEMP;
  const tempStateKey = ShadowStateKeyHelper.getModeConfigStateKey(
    firmwareVersion,
    tempShadowKey
  );

  return prevConfig &&
    prevConfig[tempStateKey] &&
    prevConfig[tempStateKey][targetMode]
    ? prevConfig[tempStateKey][targetMode]
    : undefined;
}

function getFanSpeedOnModeChange(targetMode, unit) {
  const { handsetType, modeConfig, prevConfig } = unit;
  const { fan, fanExtend, silent } = modeConfig;
  const fanSpdLv = ShadowValueHelper.getFanLvlByShadowVal(
    handsetType,
    fan,
    fanExtend
  );
  const isSilent = silent === 1;
  let canPassOver = HandsetRuleHelper.isFanSpeedOptAllowed(
    targetMode,
    fanSpdLv,
    handsetType
  );

  if (isSilent) {
    canPassOver = HandsetRuleHelper.isFeatureAllowed(
      targetMode,
      RemoteHelper.FEAT_SILENT,
      handsetType
    );
  }

  if (targetMode === RemoteHelper.MODE_DRY) {
    return undefined;
  }
  if (
    (prevConfig && prevConfig.fan && prevConfig.fan[targetMode]) ||
    (prevConfig.silent && prevConfig.silent[targetMode])
  ) {
    return prevConfig.silent && prevConfig.silent[targetMode] === 1
      ? getFanSpeedPayload(unit, RemoteHelper.FANSPEED_SILENT)
      : getFanSpeedPayload(
          unit,
          ShadowValueHelper.getFanLvlByShadowVal(
            handsetType,
            prevConfig.fan[targetMode],
            fanExtend
          )
        );
  }
  if (!canPassOver) {
    return getFanSpeedPayload(
      unit,
      ShadowValueHelper.getDefaultFanLvByHandset(handsetType, targetMode)
    );
  }

  return isSilent
    ? getFanSpeedPayload(unit, RemoteHelper.FANSPEED_SILENT)
    : getFanSpeedPayload(
        unit,
        ShadowValueHelper.getFanLvlByShadowVal(handsetType, fan, fanExtend)
      );
}

function getFanOptPreConfig(unit, targetMode) {
  const { handsetType, firmwareVersion, mode } = unit;
  const { fan, fanExtend, silent } = unit.modeConfig;
  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );
  const fanSpdLv = ShadowValueHelper.getFanLvlByShadowVal(
    handsetType,
    fan,
    fanExtend
  );

  const needSavePre =
    (silent === 1 &&
      !HandsetRuleHelper.isFeatureAllowed(
        targetMode,
        RemoteHelper.FEAT_SILENT,
        handsetType
      )) ||
    !HandsetRuleHelper.isFanSpeedOptAllowed(targetMode, fanSpdLv, handsetType);

  let shadowData;

  if (needSavePre) {
    // Save the prevFanLv
    const fanOptShadowKey =
      silent === 1 ? shadowKeys.SET_SILENT : shadowKeys.SET_FAN;
    const fanOptStateKey = ShadowStateKeyHelper.getModeConfigStateKey(
      firmwareVersion,
      fanOptShadowKey
    );
    const value = silent === 1 ? silent : fan;
    shadowData = {
      [fanOptStateKey]: { [mode]: value },
    };
  }

  return shadowData;
}

function getTempPreConfig(unit) {
  const { firmwareVersion, modeConfig, handsetType, mode } = unit;
  if (
    !HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_TEMP,
      handsetType
    )
  ) {
    return undefined;
  }

  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );

  const tempShadowKey = shadowKeys.SET_TEMP;
  const tempStateKey = ShadowStateKeyHelper.getModeConfigStateKey(
    firmwareVersion,
    tempShadowKey
  );
  const tempValue = modeConfig[tempStateKey];

  return {
    [tempStateKey]: {
      ...(unit.prevConfig && unit.prevConfig[tempStateKey]),
      [unit.mode]: tempValue,
    },
  };
}

function* savePreValueIfNeeded(unit, targetModeNr) {
  const targetMode = ShadowValueHelper.getModeByShadowVal(targetModeNr);
  const { thingName } = unit;

  const fanSpdPrevConfig = getFanOptPreConfig(unit, targetMode);
  const switchValuePrevConfig = getTempPreConfig(unit);

  const shadowData = {
    ...fanSpdPrevConfig,
    ...switchValuePrevConfig,
  };

  yield put(setUnitPrevConfig({ thingName, shadowData }));
}

function* clearFanOptPreConfig(unit) {
  const { thingName } = unit;
  const keys = ['silent', 'fan'];
  yield put(removeUnitPrevConfigProp({ thingName, keys }));
}

function getUnsupportedFeatOffPayload(targetMode, unit) {
  const { firmwareVersion } = unit;
  const {
    handsetType,
    enableTurbo,
    enableSmartTurbo,
    enableSleep,
    enableSmartSleep,
    enableEcoplus,
    enableSmartEcomax,
  } = unit;
  const turboAllowed =
    (enableTurbo &&
      !enableSmartTurbo &&
      HandsetRuleHelper.isFeatureAllowed(
        targetMode,
        RemoteHelper.FEAT_TURBO,
        handsetType
      )) ||
    (enableSmartTurbo &&
      HandsetRuleHelper.isFeatureAllowed(
        targetMode,
        RemoteHelper.FEAT_SMART_TURBO,
        handsetType
      ));

  const sleepAllowed =
    (enableSleep &&
      !enableSmartSleep &&
      HandsetRuleHelper.isFeatureAllowed(
        targetMode,
        RemoteHelper.FEAT_SLEEP,
        handsetType
      )) ||
    (enableSmartSleep &&
      HandsetRuleHelper.isFeatureAllowed(
        targetMode,
        RemoteHelper.FEAT_SMART_SLEEP,
        handsetType
      ));

  const ecoAllowed =
    (enableEcoplus &&
      HandsetRuleHelper.isFeatureAllowed(
        targetMode,
        RemoteHelper.FEAT_ECOPLUS,
        handsetType
      )) ||
    (enableSmartEcomax &&
      HandsetRuleHelper.isFeatureAllowed(
        targetMode,
        RemoteHelper.FEAT_SMART_ECOMAX,
        handsetType
      ));

  const unsupportedPayload = {
    ...(!turboAllowed &&
      ShadowValueHelper.getShadowObjByTurbo(
        RemoteHelper.TURBO_OFF,
        firmwareVersion
      )),
    ...(!sleepAllowed &&
      ShadowValueHelper.getShadowObjBySleep({
        sleepKey: RemoteHelper.SLEEP_OFF,
        ...unit,
      })),
    ...(!ecoAllowed &&
      ShadowValueHelper.getShadowObjByEcoplus({
        ecoKey: RemoteHelper.ECOPLUS_OFF,
        ...unit,
      })),
  };

  return unsupportedPayload;
}

function* getModePayload(modeValue) {
  const unit = yield call(getRemoteUnit);
  const targetMode = ShadowValueHelper.getModeByShadowVal(modeValue);
  const { firmwareVersion } = unit;
  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );
  const acTemp = getTempOnModeChange(targetMode, unit);

  return {
    [shadowKeys.SET_MODE]: modeValue,
    // Fan state
    ...getFanSpeedOnModeChange(targetMode, unit),
    ...(acTemp && { [shadowKeys.SET_TEMP]: acTemp }),
    ...getUnsupportedFeatOffPayload(targetMode, unit),
  };
}

function getSilentTurboPayload({
  actionType,
  controlValue,
  firmwareVersion,
  modeConfig,
  enableSmartTurbo,
  enableEcoplus,
  enableBreeze,
  enableSense,
}) {
  // 1. Check action is silent or turbo
  const silentValue = actionType === SET_SILENT ? controlValue : 0;
  const turboValue =
    actionType === SET_TURBO ? controlValue : RemoteHelper.TURBO_OFF;

  const shadowKeyMap = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );

  const { sense, ecoPlus, breeze } = modeConfig;

  const { turbo = 0, smartTurbo = 0 } = ShadowValueHelper.getShadowValByTurbo(
    turboValue
  );

  const silentInterlock = {
    ...(enableSense &&
      sense === 1 && {
        [shadowKeyMap.SET_SENSE]: 0,
      }),
    ...(enableBreeze &&
      breeze === 1 && {
        [shadowKeyMap.SET_BREEZE]: 0,
      }),
  };

  const silentState = {
    [shadowKeyMap.SET_SILENT]: parseInt(silentValue, 10),
    ...(parseInt(silentValue, 10) === 1 && silentInterlock),
  };

  const turboState = {
    [shadowKeyMap.SET_TURBO]: parseInt(turbo, 10),
    ...(enableSmartTurbo && {
      [shadowKeyMap.SET_SMART_TURBO]: parseInt(smartTurbo, 10),
    }),
    ...(enableEcoplus &&
      ecoPlus === 1 && {
        [shadowKeyMap.SET_ECOPLUS]: 0,
      }),
  };

  return {
    ...silentState,
    ...turboState,
  };
}

function getSensePayload(senseValue, unit) {
  const {
    modeConfig,
    firmwareVersion,
    enableCKSwing,
    enableSmartSleep,
    enableEcoplus,
    enableSmartTurbo,
    enableSmartDrift,
    enableSmartEcomax,
  } = unit;
  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );

  const {
    ckSwing,
    silent,
    sleep,
    ecoplus,
    smartSleep,
    smartTurbo,
    smartDrift,
    smartEcoMax,
  } = modeConfig;

  const interlockState =
    modeConfig && senseValue === 1
      ? {
          ...(enableCKSwing && ckSwing > 0 && { [shadowKeys.SET_CK_SWING]: 0 }),
          ...(silent === 1 && { [shadowKeys.SET_SILENT]: 0 }),
          ...(sleep === 1 && { [shadowKeys.SET_SLEEP]: 0 }),
          ...(enableEcoplus &&
            ecoplus === 1 && { [shadowKeys.SET_ECOPLUS]: 0 }),
          ...(enableSmartSleep &&
            smartSleep === 1 && { [shadowKeys.SET_SMART_SLEEP]: 0 }),
          ...(enableSmartTurbo &&
            smartTurbo === 1 && { [shadowKeys.SET_SMART_TURBO]: 0 }),
          ...(enableSmartDrift &&
            smartDrift === 1 && { [shadowKeys.SET_SMART_DRIFT]: 0 }),
          ...(enableSmartEcomax &&
            smartEcoMax === 1 && { [shadowKeys.SET_SMART_ECOMAX]: 0 }),
        }
      : {};

  const shadowState = {
    [shadowKeys.SET_SENSE]: senseValue,
    ...interlockState,
  };

  return shadowState;
}

function getBreezePayload(breezeValue, unit) {
  const {
    modeConfig,
    firmwareVersion,
    enableCKSwing,
    enableSmartSleep,
    enableSmartDrift,
    enableLRSwing,
    enableLRStep,
    enableUDStep,
  } = unit;
  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );

  const {
    ckSwing,
    silent,
    smartSleep,
    smartDrift,
    leftRightSwing,
    upDownSwing,
    swing,
  } = modeConfig;

  const interlockState =
    modeConfig && breezeValue === 1
      ? {
          ...(enableCKSwing && ckSwing > 0 && { [shadowKeys.SET_CK_SWING]: 0 }),
          ...(silent === 1 && { [shadowKeys.SET_SILENT]: 0 }),
          ...(enableSmartSleep &&
            smartSleep === 1 && { [shadowKeys.SET_SMART_SLEEP]: 0 }),
          ...(enableSmartDrift &&
            smartDrift === 1 && { [shadowKeys.SET_SMART_DRIFT]: 0 }),
          ...(enableLRSwing &&
            enableLRStep &&
            leftRightSwing === 15 && { [shadowKeys.SET_LRLVR]: 0 }),
          ...(enableUDStep &&
            upDownSwing === 1 && { [shadowKeys.SET_UDLVR]: 0 }),
          ...(swing === 1 && { [shadowKeys.SET_SWING]: 0 }),
        }
      : {};

  const shadowState = {
    [shadowKeys.SET_BREEZE]: breezeValue,
    ...interlockState,
  };

  return shadowState;
}

function getSmartDriftPayload(smartDriftValue, unit) {
  const {
    modeConfig,
    firmwareVersion,
    enableCKSwing,
    enableSmartSleep,
    enableLRSwing,
    enableLRStep,
    enableUDStep,
    enableSense,
    enableBreeze,
  } = unit;
  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );

  const {
    ckSwing,
    smartSleep,
    leftRightSwing,
    upDownSwing,
    swing,
    sense,
    breeze,
  } = modeConfig;

  const interlockState =
    modeConfig && smartDriftValue === 1
      ? {
          ...(enableCKSwing && ckSwing > 0 && { [shadowKeys.SET_CK_SWING]: 0 }),
          ...(enableSmartSleep &&
            smartSleep === 1 && { [shadowKeys.SET_SMART_SLEEP]: 0 }),
          ...(enableLRSwing &&
            enableLRStep &&
            leftRightSwing === 15 && { [shadowKeys.SET_LRLVR]: 0 }),
          ...(enableUDStep &&
            upDownSwing === 1 && { [shadowKeys.SET_UDLVR]: 0 }),
          ...(swing === 1 && { [shadowKeys.SET_SWING]: 0 }),
          ...(enableSense && sense === 1 && { [shadowKeys.SET_SENSE]: 0 }),
          ...(enableBreeze && breeze === 1 && { [shadowKeys.SET_BREEZE]: 0 }),
        }
      : {};

  const shadowState = {
    [shadowKeys.SET_SMART_DRIFT]: smartDriftValue,
    ...interlockState,
  };

  return shadowState;
}

function* getPayload(action) {
  const { connectionID, thingName } = yield select(state => state.remote);
  const allUnits = yield select(state => state.units.allUnits);
  const curUnit = allUnits[thingName];
  const { key } = curUnit;
  const { handsetType, firmwareVersion, mode, modeConfig } = curUnit;
  const shadowKeys = ShadowStateKeyHelper.getModeConfigShadowKeyMap(
    firmwareVersion
  );
  const { controlValue } = action;

  const payload = {
    type: action.type,
    id: connectionID,
    thingName,
    key,
  };

  // IF qx == "10", then run BRP remote control
  // DO BRP REMOTE SETTING
  console.log("ACTION TYPE === ", action.type);
  console.log("THING NAME === ", thingName);
  console.log("KEY? === ", key);
  console.log("controlValue? === ", controlValue);
  // console.log("shadowKeys? === ", shadowKeys);
  console.log("curUnit? === ", curUnit);
  console.log("QX HERE????");
  console.log("QX? === ", curUnit.qx);
  console.log("PAYLOAD BEFORE SWITCH CASE === ", payload);

  if(curUnit.qx == "20"){ // IF IS BRP MODULE
    console.log("RUN BRP THING");

    // switch (action.type){
    //   case SET_AC_TEMP: console.log("Setting temperature");
    //   default: return payload;
    // }

  }else{
    switch (action.type) {
      case SET_AC_SWITCH:
        return {
          ...payload,
          shadowState: {
            [shadowKeys.SET_SWITCH]: action.switchType,
          },
        };
      case SET_AC_TEMP: {
        yield put(setExpectedTemp(controlValue));
        return {
          ...payload,
          shadowState: {
            [shadowKeys.SET_TEMP]: action.controlValue,
          },
        };
      }
      case SET_FAN_SPEED: {
        // Clear the prev config
        yield call(clearFanOptPreConfig, curUnit);
        return {
          ...payload,
          shadowState: getFanSpeedPayload(curUnit, controlValue),
        };
      }
      case SET_MODE: {
        yield put(clearDesireTemp());
        yield call(savePreValueIfNeeded, curUnit, action.controlValue);
        const shadowState = yield call(getModePayload, action.controlValue);
  
        return {
          ...payload,
          shadowState,
        };
      }
      case SET_TURBO:
      case SET_SILENT: {
        return {
          ...payload,
          shadowState: getSilentTurboPayload({
            actionType: action.type,
            controlValue,
            firmwareVersion,
            ...curUnit,
          }),
        };
      }
      case SET_ECOPLUS: {
        const isManual = action.canManual;
        const tempMinMax = HandsetRuleHelper.getTempMinMax(mode, handsetType);
        let manualTemp;
  
        if (tempMinMax && tempMinMax.max) {
          manualTemp =
            modeConfig.acTemp < 24
              ? 24
              : Math.min(modeConfig.acTemp + 1, tempMinMax.max);
        }
  
        return {
          ...payload,
          shadowState: ShadowValueHelper.getShadowObjByEcoplus({
            ecoKey: action.controlValue,
            firmwareVersion,
            isManual,
            manualTemp,
            ...curUnit,
          }),
        };
      }
      case SET_SLEEP:
        return {
          ...payload,
          shadowState: ShadowValueHelper.getShadowObjBySleep({
            sleepKey: action.controlValue,
            firmwareVersion,
            ...curUnit,
          }),
        };
      case SET_SWING: {
        const swingValKey = action.controlValue;
        const shadowState = ShadowValueHelper.getShadowObjBySwing({
          swingValKey,
          ...curUnit,
          firmwareVersion,
        });
  
        return {
          ...payload,
          shadowState,
        };
      }
      case SET_SWING_LR: {
        const shadowState = ShadowValueHelper.getShadowObjBySwingLR({
          lrSwingKey: action.controlValue,
          ...curUnit,
          firmwareVersion,
        });
        return {
          ...payload,
          shadowState,
        };
      }
      case SET_SWING_3D: {
        const is3D = action.controlValue === 1;
  
        const {
          modeConfig: { breeze, smartSleep, smartDrift, smartTurbo },
        } = curUnit;
  
        const interlockState = {
          ...(breeze === 1 && { [shadowKeys.SET_BREEZE]: 0 }),
          ...(smartSleep === 1 && { [shadowKeys.SET_SMART_SLEEP]: 0 }),
          ...(smartDrift === 1 && { [shadowKeys.SET_SMART_DRIFT]: 0 }),
          ...(smartTurbo === 1 && { [shadowKeys.SET_SMART_TURBO]: 0 }),
        };
        const shadowState = {
          [shadowKeys.SET_SWING]: is3D ? 1 : 0,
          [shadowKeys.SET_UDLVR]: is3D ? 15 : 0,
          [shadowKeys.SET_LRLVR]: is3D ? 15 : 0,
          ...(is3D && interlockState),
        };
  
        return {
          ...payload,
          shadowState,
        };
      }
      case SET_BREEZE: {
        return {
          ...payload,
          shadowState: getBreezePayload(action.controlValue, curUnit),
        };
      }
      case SET_SMART_DRIFT: {
        return {
          ...payload,
          shadowState: getSmartDriftPayload(action.controlValue, curUnit),
        };
      }
      case SET_STREAMER: {
        const { enableCKSwing } = curUnit;
        const { ckSwing } = modeConfig;
        const shadowState = {
          [shadowKeys.SET_STREAMER]: action.controlValue,
          ...(action.controlValue === 1 &&
            enableCKSwing &&
            ckSwing > 0 && { [shadowKeys.SET_CK_SWING]: 0 }),
        };
  
        return {
          ...payload,
          shadowState,
        };
      }
      case SET_SENSE: {
        return {
          ...payload,
          shadowState: getSensePayload(action.controlValue, curUnit),
        };
      }
      case SET_LED: {
        const shadowState = ShadowValueHelper.getShadowObjByLED({
          ledKey: action.controlValue,
          firmwareVersion,
          modeConfig: curUnit.modeConfig,
        });
  
        return {
          ...payload,
          shadowState,
        };
      }
      case SET_CK_SWING: {
        const shadowState = ShadowValueHelper.getShadowObjByCKSwing({
          ckSwingKey: action.controlValue,
          firmwareVersion,
          modeConfig: curUnit.modeConfig,
        });
        return {
          ...payload,
          shadowState,
        };
      }
      default:
        return payload;
    }
  }

  /* // TEMPORARY REMOVE FOR TESTING PURPOSE
  switch (action.type) {
    case SET_AC_SWITCH:
      return {
        ...payload,
        shadowState: {
          [shadowKeys.SET_SWITCH]: action.switchType,
        },
      };
    case SET_AC_TEMP: {
      yield put(setExpectedTemp(controlValue));
      return {
        ...payload,
        shadowState: {
          [shadowKeys.SET_TEMP]: action.controlValue,
        },
      };
    }
    case SET_FAN_SPEED: {
      // Clear the prev config
      yield call(clearFanOptPreConfig, curUnit);
      return {
        ...payload,
        shadowState: getFanSpeedPayload(curUnit, controlValue),
      };
    }
    case SET_MODE: {
      yield put(clearDesireTemp());
      yield call(savePreValueIfNeeded, curUnit, action.controlValue);
      const shadowState = yield call(getModePayload, action.controlValue);

      return {
        ...payload,
        shadowState,
      };
    }
    case SET_TURBO:
    case SET_SILENT: {
      return {
        ...payload,
        shadowState: getSilentTurboPayload({
          actionType: action.type,
          controlValue,
          firmwareVersion,
          ...curUnit,
        }),
      };
    }
    case SET_ECOPLUS: {
      const isManual = action.canManual;
      const tempMinMax = HandsetRuleHelper.getTempMinMax(mode, handsetType);
      let manualTemp;

      if (tempMinMax && tempMinMax.max) {
        manualTemp =
          modeConfig.acTemp < 24
            ? 24
            : Math.min(modeConfig.acTemp + 1, tempMinMax.max);
      }

      return {
        ...payload,
        shadowState: ShadowValueHelper.getShadowObjByEcoplus({
          ecoKey: action.controlValue,
          firmwareVersion,
          isManual,
          manualTemp,
          ...curUnit,
        }),
      };
    }
    case SET_SLEEP:
      return {
        ...payload,
        shadowState: ShadowValueHelper.getShadowObjBySleep({
          sleepKey: action.controlValue,
          firmwareVersion,
          ...curUnit,
        }),
      };
    case SET_SWING: {
      const swingValKey = action.controlValue;
      const shadowState = ShadowValueHelper.getShadowObjBySwing({
        swingValKey,
        ...curUnit,
        firmwareVersion,
      });

      return {
        ...payload,
        shadowState,
      };
    }
    case SET_SWING_LR: {
      const shadowState = ShadowValueHelper.getShadowObjBySwingLR({
        lrSwingKey: action.controlValue,
        ...curUnit,
        firmwareVersion,
      });
      return {
        ...payload,
        shadowState,
      };
    }
    case SET_SWING_3D: {
      const is3D = action.controlValue === 1;

      const {
        modeConfig: { breeze, smartSleep, smartDrift, smartTurbo },
      } = curUnit;

      const interlockState = {
        ...(breeze === 1 && { [shadowKeys.SET_BREEZE]: 0 }),
        ...(smartSleep === 1 && { [shadowKeys.SET_SMART_SLEEP]: 0 }),
        ...(smartDrift === 1 && { [shadowKeys.SET_SMART_DRIFT]: 0 }),
        ...(smartTurbo === 1 && { [shadowKeys.SET_SMART_TURBO]: 0 }),
      };
      const shadowState = {
        [shadowKeys.SET_SWING]: is3D ? 1 : 0,
        [shadowKeys.SET_UDLVR]: is3D ? 15 : 0,
        [shadowKeys.SET_LRLVR]: is3D ? 15 : 0,
        ...(is3D && interlockState),
      };

      return {
        ...payload,
        shadowState,
      };
    }
    case SET_BREEZE: {
      return {
        ...payload,
        shadowState: getBreezePayload(action.controlValue, curUnit),
      };
    }
    case SET_SMART_DRIFT: {
      return {
        ...payload,
        shadowState: getSmartDriftPayload(action.controlValue, curUnit),
      };
    }
    case SET_STREAMER: {
      const { enableCKSwing } = curUnit;
      const { ckSwing } = modeConfig;
      const shadowState = {
        [shadowKeys.SET_STREAMER]: action.controlValue,
        ...(action.controlValue === 1 &&
          enableCKSwing &&
          ckSwing > 0 && { [shadowKeys.SET_CK_SWING]: 0 }),
      };

      return {
        ...payload,
        shadowState,
      };
    }
    case SET_SENSE: {
      return {
        ...payload,
        shadowState: getSensePayload(action.controlValue, curUnit),
      };
    }
    case SET_LED: {
      const shadowState = ShadowValueHelper.getShadowObjByLED({
        ledKey: action.controlValue,
        firmwareVersion,
        modeConfig: curUnit.modeConfig,
      });

      return {
        ...payload,
        shadowState,
      };
    }
    case SET_CK_SWING: {
      const shadowState = ShadowValueHelper.getShadowObjByCKSwing({
        ckSwingKey: action.controlValue,
        firmwareVersion,
        modeConfig: curUnit.modeConfig,
      });
      return {
        ...payload,
        shadowState,
      };
    }
    default:
      return payload;
  }
  */
}

export function* remoteActionInterface(action) {
  try {
    yield put(clearRemoteStatus());
    // All the remote control action will go through here, it will get the correct payload
    // and correct interface (iotControl, wlanControl, bleControl) and send the shadow
    const remoteInterface = yield call(getRemoteInterfaceFromMode);

    const payload = yield call(getPayload, action);

    console.log("THE PAYLOAD WILL BE =====>>");
    console.log("THE PAYLOAD WILL BE =====>>");
    console.log("THE PAYLOAD WILL BE =====>>");
    console.log("THE PAYLOAD WILL BE =====>>");
    console.log(payload);

    yield call(remoteInterface.setShadowState, payload);
  } catch (error) {
    if (error.message === 'Unauthorized access') {
      const email = yield select(state => state.user.profile.email);
      yield put(getAllUnits({ email }));
    }
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  }
}

function* executeStartTask(remoteInterface, unitProps) {
  let startTask;
  try {
    while (true) {
      startTask = yield fork(remoteInterface.start, unitProps);

      yield take(RESTART_START_TASK);
      if (startTask) yield cancel(startTask);
    }
  } finally {
    if (startTask) yield cancel(startTask);
  }
}

export function* remoteFlow() {
  let startTask;
  let endTask;
  try {
    const remoteInterface = yield call(getRemoteInterfaceFromMode);
    const unit = yield call(getRemoteUnit);
    const id = yield select(state => state.remote.connectionID);

    if (unit) {
      const unitProps = {
        thingName: unit.thingName,
        id,
        key: unit.key,
      };

      startTask = yield fork(executeStartTask, remoteInterface, unitProps);

      yield take(UNMOUNT_UNIT);

      // Some of the mode doesn't really need the end flow
      // For example: WLAN mode doesn't have the end flow
      if (remoteInterface.end) {
        endTask = yield fork(remoteInterface.end, unitProps);
      }

      // Wait until the end the start task or timeout trigged
      if (startTask) {
        yield race({
          task: join(startTask),
          timeout: delay(5000),
        });
      }
    }
  } catch (error) {
    yield put(
      setRemoteStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    // Cancel the task if needed
    if (startTask) yield cancel(startTask);
    if (endTask) yield cancel(endTask);
  }
}
