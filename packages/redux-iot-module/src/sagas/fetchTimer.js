import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import { ShadowValueHelper, RemoteHelper } from '../utils/Remote';

import {
  setFetchTimerLoading,
  setFetchTimerStatus,
  setTimer,
  setTimerPairs,
  setQuickTimer,
  clearFetchTimerStatus,
} from '../actions/timer';

const FETCH_ALL_TIMER = 4;

/*
const EXAMPLE_DATA = [
  {
    Daikin_d813997f4992: [
      {
        'richard@upstackstudio.com': [
          {
            Flag: 1,
            Day: 2,
            Hour: 16,
            Minute: 56,
            Set_OnOff: 1,
            Set_Mode: 10,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 16,
            Minute: 57,
            Set_OnOff: 1,
            Set_Mode: 2,
            Set_Fan: 8,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 16,
            Minute: 58,
            Set_OnOff: 0,
          },
        ],
      },
    ],
  },
  {
    Daikin_d813997f4916: [
      {
        'april@milkcreative.my': [
          {
            Flag: 1,
            Day: 5,
            Hour: 3,
            Minute: 39,
            Set_OnOff: 0,
          },
        ],
        'richard@upstackstudio.com': [
          {
            Flag: 1,
            Day: 6,
            Hour: 3,
            Minute: 39,
            Set_OnOff: 1,
            Set_Mode: 10,
          },
        ],
        'isabel@upstackstudio.com': [
          {
            Flag: 1,
            Day: 2,
            Hour: 16,
            Minute: 56,
            Set_OnOff: 1,
            Set_Mode: 10,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 16,
            Minute: 57,
            Set_OnOff: 1,
            Set_Mode: 2,
            Set_Fan: 8,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 16,
            Minute: 58,
            Set_OnOff: 0,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 20,
            Minute: 58,
            Set_OnOff: 0,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 19,
            Minute: 58,
            Set_OnOff: 0,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 18,
            Minute: 58,
            Set_OnOff: 0,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 16,
            Minute: 59,
            Set_OnOff: 0,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 22,
            Minute: 59,
            Set_OnOff: 0,
          },
          {
            Flag: 1,
            Day: 2,
            Hour: 9,
            Minute: 59,
            Set_OnOff: 0,
          },
        ],
      },
    ],
  },
]; */

export function timerInfoMapper({ timerInfo, timerUser }) {
  const timerObj = {
    day: timerInfo.Day,
    hour: timerInfo.Hour,
    minute: timerInfo.Minute,
    isActive: timerInfo.Flag === 1,
    mode: ShadowValueHelper.getModeByShadowVal(timerInfo.Set_Mode), // Mapper needed
    switchValue: timerInfo.Set_OnOff || 0,
    temp: timerInfo.Set_Temp,
    silent: timerInfo.Set_Silent || 0,
    fanLevel: ShadowValueHelper.getFanLvlByShadowVal(
      RemoteHelper.HANDSET_DAMA,
      timerInfo.Set_Fan
    ),
    user: timerUser,
    isQuickTimer: timerInfo.countDown === 1,
  };

  return timerObj;
}

// Convert the response to the state Obj
export function responseMapper(response) {
  if (!response || response.length < 0 || response.message) return undefined;

  const timer = {};
  const quickTimer = {};

  response.forEach(timerData => {
    const thingName = Object.keys(timerData)[0];
    const userTimer = timerData[thingName];
    const userList = Object.keys(userTimer);
    // Combine all timerdata
    const concatTimerList = [];

    userList.forEach(curUserKey => {
      const timersByUser = userTimer[curUserKey] || {};
      const userKeys = Object.keys(timersByUser);

      userKeys.forEach(timerUser => {
        const curTimerList = timersByUser[timerUser];

        curTimerList.forEach(curTimerInfo => {
          if (curTimerInfo) {
            concatTimerList.push(
              timerInfoMapper({
                timerInfo: curTimerInfo,
                timerUser,
              })
            );

            if (curTimerInfo.countDown === 1) {
              quickTimer[thingName] = {
                day: curTimerInfo.Day,
                hour: curTimerInfo.Hour,
                minute: curTimerInfo.Minute,
                switchValue: curTimerInfo.Set_OnOff || 0,
              };
            }
          }
        });
      });
    });

    // Sorting the timer data based on day, hour, minute
    let timerListAsc = concatTimerList.sort(
      (curTimer, nextTimer) =>
        curTimer.day - nextTimer.day ||
        curTimer.hour - nextTimer.hour ||
        curTimer.minute - nextTimer.minute
    );

    timerListAsc = timerListAsc.map((cur, index) => ({
      ...cur,
      timerIndex: index,
    }));

    timer[thingName] = timerListAsc;
  });
  return { timer, quickTimer };
}

function getNextDay(day) {
  return (day + 1) % 7;
}

function modulus(value, modulusValue) {
  return ((value % modulusValue) + modulusValue) % modulusValue;
}

export function generateTimeBarInstance(timers) {
  const unitThingNameList = Object.keys(timers);
  const unitPairList = {};

  unitThingNameList.forEach(thingName => {
    const unitTimers = timers[thingName].filter(
      timerInfo => timerInfo.isActive
    );
    const pairList = [];

    for (let pointer = 0; pointer < unitTimers.length; pointer += 1) {
      const curTimer = unitTimers[pointer];
      const nextPointer = (pointer + 1) % unitTimers.length;
      const nextTimer = unitTimers[nextPointer];

      if (curTimer.switchValue === 1) {
        const nextTime = nextTimer.minute + nextTimer.hour * 100;
        const curTime = curTimer.minute + curTimer.hour * 100;
        const dayDifference =
          modulus(nextTimer.day - curTimer.day, 7) ||
          (nextTime - curTime < 0 ? 7 : 0) ||
          (unitTimers.length === 1 ? 7 : 0);

        for (
          let curDay = curTimer.day, curDayDifference = dayDifference;
          curDayDifference >= 0;
          curDay = getNextDay(curDay), curDayDifference -= 1
        ) {
          let startDay = curTimer.day;
          let startHour = curTimer.hour;
          let startMinute = curTimer.minute;
          let endDay = nextTimer.day;
          let endHour = nextTimer.hour;
          let endMinute = nextTimer.minute;

          if (dayDifference > 0) {
            if (curDayDifference === dayDifference) {
              endDay = curTimer.day;
              endHour = 25;
              endMinute = 0;
            } else if (curDayDifference === 0) {
              startDay = nextTimer.day;
              startHour = -1;
              startMinute = 0;
            } else {
              startDay = curDay;
              startHour = -1;
              startMinute = 0;
              endDay = curDay;
              endHour = 25;
              endMinute = 0;
            }
          }
          pairList.push({
            start: {
              day: startDay,
              hour: startHour,
              minute: startMinute,
            },
            end: {
              day: endDay,
              hour: endHour,
              minute: endMinute,
            },
            mode: curTimer.mode,
            timerIndex: curTimer.timerIndex,
          });
        }
      }
    }

    if (Object.keys(pairList).length > 0) {
      unitPairList[thingName] = pairList;
    }
  });

  /*
  const displayKeys = Object.keys(unitPairList);

  displayKeys.forEach((thingNameKey) => {
    const curPairs = unitPairList[thingNameKey];

    curPairs.forEach((curPair) => {
      const { start, end } = curPair;
    });
  }); */

  return unitPairList;
}

export default function* fetchTimer(action) {
  try {
    yield put(clearFetchTimerStatus());
    yield put(setFetchTimerLoading(true));
    const {
      user: {
        profile: { email },
      },
    } = yield select(state => state);

    const response = yield call(ApiService.post, 'timerfunction', {
      requestData: {
        type: FETCH_ALL_TIMER,
        username: action ? action.fetchUnitsParams.email : email,
      },
    });

    if (!response) throw new Error('Unexpected error while sending the api');

    const { timer: timerData, quickTimer } = responseMapper(response);

    if (!timerData) throw new Error('Error while processing the response');

    yield put(setTimer(timerData));
    yield put(setQuickTimer(quickTimer));

    const timerPairs = yield call(generateTimeBarInstance, timerData);
    yield put(setTimerPairs(timerPairs));

    yield put(
      setFetchTimerStatus({
        messageType: 'success',
        message: 'success',
      })
    );
  } catch (error) {
    yield put(
      setFetchTimerStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setFetchTimerLoading(false));
  }
}
