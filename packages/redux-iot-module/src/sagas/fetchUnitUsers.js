import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import moment from 'moment';

import UserType from '../utils/userType';
import {
  setUnitUsers,
  setUnitUsersLoading,
  setUnitUsersStatus,
  clearUnitUsersStatus,
  setControlLimit,
  setGuestNumber,
} from '../actions/unitUsers';

// import checkExpiryTime from './controlUsageLimit';

export function checkExpiryTime(expiryTime) {
  if (
    expiryTime &&
    expiryTime !== '-' &&
    moment().isSameOrAfter(
      moment(expiryTime, 'YYYY-MM-DD HH:mm:s').format('YYYY-MM-DD HH:mm')
    )
  ) {
    return false;
  }
  return true;
}

export default function* fetchUnitUsers() {
  try {
    yield put(clearUnitUsersStatus());
    yield put(setUnitUsersLoading(true));

    // Get the current selected thingName
    const { thingName } = yield select(state => state.remote);

    const allUnit = yield select(state => state.units.allUnits);
    const currentUnit = allUnit[thingName];
    const currentUser = yield select(state => state.user.profile);
    // If can find the thingName
    if (currentUnit) {
      let canControlLimit = false;

      // set control limit enablement
      canControlLimit = currentUnit.planFeatures.Ena_ControlLimit === '1';

      // You need to check whether is host or guest user based on qx
      if (UserType.isHostUser(currentUnit.qx)) {
        // Then you need to fetch all the guest users

        const responseData = yield call(ApiService.post, 'getpaireddevices', {
          requestData: {
            type: 2,
            value: thingName,
          },
        });

        if (responseData.message === 'success') {
          // Process the response data when it is success
          const guestList = responseData.user.reduce((acc, user) => {
            if (UserType.isGuestUser(user.qx)) {
              if (checkExpiryTime(user.expiryTime)) {
                acc.push({
                  email: user.userName,
                  name: user.fullName,
                  qx: user.qx,
                  shareUnit: user.shareUnit,
                  overwriteSchedule: user.overwriteSchedule,
                  expiryTime: user.expiryTime,
                });
              }
            }

            return acc;
          }, []);

          const unitUsersList = [];
          unitUsersList.push({
            title: 'Host User',
            data: [{ email: currentUser.email, name: currentUser.name }],
          });

          if (guestList.length > 0) {
            unitUsersList.push({
              title: 'Guest User',
              data: guestList,
            });
          }

          yield put(setGuestNumber(guestList.length));

          yield put(
            setUnitUsers({
              unitUsersList,
            })
          );

          yield put(
            setUnitUsersStatus({ messageType: 'success', message: 'success' })
          );

          // yield call(checkExpiryTime, { guestList });
        } else {
          yield put(
            setUnitUsersStatus({
              messageType: 'error',
              message: responseData.message,
            })
          );
        }
      } else if (UserType.isGuestUser(currentUnit.qx)) {
        const responseData = yield call(
          ApiService.post,
          'usercontrolfunction',
          {
            requestData: {
              type: 1,
              username: currentUser.email,
              thingName,
              config: [],
            },
          }
        );

        const unitUsersList = [
          {
            title: 'Guest User',
            data: [
              {
                email: currentUser.email,
                name: currentUser.name,
                expiryTime: responseData[0].expiryTime,
                // Store more information in the future
              },
            ],
          },
        ];

        yield put(setUnitUsers({ unitUsersList }));
      }

      yield put(setControlLimit(canControlLimit));
    }
  } catch (error) {
    yield put(
      setUnitUsersStatus({ messageType: 'error', message: error.message })
    );
  } finally {
    yield put(setUnitUsersLoading(false));
  }
}
