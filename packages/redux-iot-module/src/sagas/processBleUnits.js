import { put, select, call, race, delay } from 'redux-saga/effects';
import { StringHelper } from '@module/utility';
import {
  setBleUnits,
  setLoading,
  setBleStatus,
  clearBleUnits,
  clearBleStatus,
} from '../actions/bleMode';

import BLEService from '../utils/BLE/BLEService';

const UNAUTHORIZED_UNIT_KEY = 'Unauthorized units';
const DEFAULT_LOGO = 'ble.png';
const DEFAULT_MAX_INDEX = 9999;

function* processBleUnits(bleUnits) {
  const allUnits = yield select(state => state.units.allUnits);
  const groups = yield select(state => state.units.groupInfo.groups);
  const groupsInArray = Object.values(groups);

  if (!bleUnits || bleUnits.length < 1) {
    throw new Error('No units found');
  }

  bleUnits.sort((curUnit, nextUnit) => {
    const { groupIndex: curGroupIndex = DEFAULT_MAX_INDEX } =
      allUnits[curUnit.thingName] || {};
    const { groupIndex: nextGroupIndex = DEFAULT_MAX_INDEX } =
      allUnits[nextUnit.thingName] || {};

    return curGroupIndex - nextGroupIndex;
  });

  const units = {};

  bleUnits.forEach(unit => {
    const { thingName, id } = unit;
    const unitInStore = allUnits[thingName];

    const groupInStore = unitInStore
      ? groupsInArray.find(group => group.groupIndex === unitInStore.groupIndex)
      : undefined;

    const groupKey = groupInStore
      ? groupInStore.groupName
      : UNAUTHORIZED_UNIT_KEY;

    const groupInUnits = units[groupKey];

    const unitProps = {
      name: unitInStore
        ? unitInStore.ACName
        : StringHelper.removePrefix(unit.thingName),
      logo: unitInStore ? unitInStore.Logo : DEFAULT_LOGO,
      disallowed: !unitInStore,
      thingName: unitInStore ? unitInStore.thingName : unit.thingName,
      id,
    };

    const data = groupInUnits ? [...groupInUnits.data, unitProps] : [unitProps];

    units[groupKey] = {
      title: groupKey,
      data,
    };
  });

  const unitsInArray = Object.values(units);

  return unitsInArray;
}

export default function* scanUnits(action) {
  try {
    yield put(clearBleUnits());
    yield put(clearBleStatus());
    yield put(setLoading(true));
    const { scanTimeout } = yield race({
      scan: call(BLEService.scan),
      scanTimeout: delay(20000),
    });

    if (scanTimeout) {
      throw new Error('Scan timeout');
    }

    const peripherals = yield call(BLEService.getDiscoveredUnits) || [];

    const filteredPeripherals = peripherals.reduce((acc, peripheral) => {
      if (peripheral.name === 'Daikin') {
        const unitThingName = action.unitHandler(peripheral);
        // Strictly avoid duplication
        if (!acc[unitThingName]) {
          // eslint-disable-next-line no-param-reassign
          acc[unitThingName] = {
            thingName: unitThingName,
            id: peripheral.id,
          };
        }
      }

      return acc;
    }, {});

    if (!filteredPeripherals || filteredPeripherals.length < 1) {
      throw new Error('No unit found');
    }

    const unitsInArray = yield call(
      processBleUnits,
      Object.values(filteredPeripherals)
    );
    yield put(setBleUnits(unitsInArray));
    yield put(setBleStatus({ messageType: 'success', message: 'success' }));
  } catch (error) {
    yield put(setBleStatus({ messageType: 'error', message: error.message }));
  } finally {
    yield put(setLoading(false));
  }
}
