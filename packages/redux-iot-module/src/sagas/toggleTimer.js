import { call, put, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';

import {
  setFetchTimerStatus,
  clearFetchTimerStatus,
  setToggleTimerLoading,
  removeToggleTimerLoading,
  setTimerToggle,
  setTimerPairs,
} from '../actions/timer';
import { generateTimeBarInstance } from './fetchTimer';
import { mapTimerInfoToResponse } from './editWeeklyTimer';

export default function* toggleTimer(action) {
  const { timerIndex, thingName } = action;

  try {
    yield put(clearFetchTimerStatus());
    yield put(setToggleTimerLoading({ thingName, timerIndex }));
    const {
      timer: { timerList },
      user: {
        profile: { email },
      },
    } = yield select(state => state);

    const unitTimers = timerList[thingName];
    let timerObj = {};

    unitTimers.forEach(timerInfo => {
      const flagValue = Number(timerInfo.isActive);
      const toggleFlagValue =
        timerInfo.timerIndex === timerIndex ? 1 - flagValue : flagValue;
      const curTimerInfo = mapTimerInfoToResponse({
        ...timerInfo,
        isActive: Boolean(toggleFlagValue),
      });

      const previousTimerObj = timerObj[timerInfo.user] || [];

      timerObj = {
        ...timerObj,
        [timerInfo.user]: [...previousTimerObj, curTimerInfo],
      };
    });

    const timerInArray = [timerObj];

    const response = yield call(ApiService.post, 'timerfunction', {
      requestData: {
        type: 3,
        username: email,
        thingName,
        timerData: timerInArray,
      },
    });

    if (response && response.message === 'success') {
      // Changing the store
      yield put(
        setTimerToggle({
          thingName,
          timerIndex,
        })
      );
      const timerData = yield select(state => state.timer.timerList);
      const timerPairs = yield call(generateTimeBarInstance, timerData);

      yield put(setTimerPairs(timerPairs));
    }
  } catch (error) {
    yield put(
      setFetchTimerStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(
      removeToggleTimerLoading({
        thingName,
        timerIndex,
      })
    );
  }
}
