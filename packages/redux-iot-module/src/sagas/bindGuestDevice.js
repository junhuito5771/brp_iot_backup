import { call, put, select, putResolve } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import { getAllUnits } from '../actions/units';
import {
  setBindGuestLoading,
  clearBindGuestStatus,
  setBindGuestStatus,
} from '../actions/bindGuestDevice';
import getGroupIndex from '../utils/getGroupIndex';
import getUnitIndex from '../utils/getUnitIndex';

const mapQRCodeObjToAPI = (QRCodeObj, version) => {
  switch (version) {
    case 3:
      return QRCodeObj;
    default:
      return {
        thingName: QRCodeObj.ThingName,
        acGroup: QRCodeObj.ACGroup,
        acName: QRCodeObj.ACName,
        logo: QRCodeObj.Logo,
        qx: QRCodeObj.QX,
        key: QRCodeObj.Key,
      };
  }
};

export default function* bindDevice(action) {
  try {
    yield put(clearBindGuestStatus());
    yield put(setBindGuestLoading(true));

    const { thingName, acName, acGroup, logo, key, qx } = mapQRCodeObjToAPI(
      action.bindGuestDeviceParams,
      action.version
    );

    const username = yield select(state => state.user.profile.email);

    const { groupIndex, groupKey } = yield call(getGroupIndex, acGroup);
    const unitIndex = yield call(getUnitIndex, groupKey, groupIndex, thingName);

    const responseData = yield call(ApiService.post, 'binddeviceforguest', {
      requestData: {
        username,
        thingName,
        thingType: 'AC',
        qx,
        acName,
        acGroup,
        logo,
        key,
        groupIndex,
        unitIndex,
      },
    });

    if (responseData && responseData.code) {
      yield put(
        setBindGuestStatus({
          messageType: 'error',
          message: responseData.message,
        })
      );
    } else {
      const email = yield select(state => state.user.profile.email);

      yield putResolve(getAllUnits({ email }));

      yield putResolve(
        setBindGuestStatus({
          messageType: 'success',
          message: 'success',
        })
      );
    }
  } catch (error) {
    yield put(
      setBindGuestStatus({
        messageType: 'error',
        message: error.message,
      })
    );
  } finally {
    yield put(setBindGuestLoading(false));
  }
}
