import { call, select } from 'redux-saga/effects';
import { ApiService } from '@module/utility';
import moment from 'moment';

import unbindDevice from './unbindDevice';
import fetchUnitUsers from './fetchUnitUsers';

const UNBIND_GUESTS = 2;

function getExpiredDate(data) {
  return data.reduce((acc, user) => {
    acc.push({ thingName: user.thingName });
    return acc;
  }, []);
}
export default function* checkExpiryTime(action) {
  const experyList = [];
  const allUnit = yield select(state => state.units.allUnits);
  const unitKeys = Object.keys(allUnit);
  const isHostChecking = action && action.guestList;
  const isRemoteChecking =
    action.checkExpiryTimeParams && action.checkExpiryTimeParams.thingName;

  if (isHostChecking) {
    const { guestList } = action;
    const { thingName } = yield select(state => state.remote);
    const hostEmail = yield select(state => state.user.profile.email);

    guestList.forEach(k => {
      if (
        k.expiryTime &&
        k.expiryTime !== '-' &&
        moment().isSameOrAfter(
          moment(k.expiryTime, 'YYYY-MM-DD HH:mm:s').format('YYYY-MM-DD HH:mm')
        )
      )
        experyList.push(k.email);
    });

    if (experyList.length > 0) {
      const apiEnpoint = {
        requestData: {
          type: UNBIND_GUESTS,
          email: hostEmail,
          thingName,
          guest: experyList,
        },
      };
      const responseData = yield call(
        ApiService.post,
        'unbinddevice',
        apiEnpoint
      );
      if (responseData && responseData.code) {
        throw new Error(' Error occured while unbind guest user');
      } else {
        // Refresh the unit users
        yield call(fetchUnitUsers);
      }
    }
  } else if (isRemoteChecking) {
    const curUnit = action.checkExpiryTimeParams.thingName;
    const getExpiryTime = allUnit[curUnit] && allUnit[curUnit].expiryTime;

    if (
      getExpiryTime &&
      getExpiryTime !== '-' &&
      moment().isSameOrAfter(
        moment(getExpiryTime, 'YYYY-MM-DD HH:mm:s').format('YYYY-MM-DD HH:mm')
      )
    ) {
      experyList.push({ thingName: curUnit, expiryTime: getExpiryTime });
    }

    const expiredData = getExpiredDate(experyList);

    if (expiredData.length > 0) {
      yield call(unbindDevice, expiredData);
    }
  } else {
    unitKeys.forEach(thingName => {
      const curUnit = allUnit[thingName];
      if (
        curUnit.expiryTime &&
        curUnit.expiryTime !== '-' &&
        moment().isSameOrAfter(
          moment(curUnit.expiryTime, 'YYYY-MM-DD HH:mm:s').format(
            'YYYY-MM-DD HH:mm'
          )
        )
      )
        experyList.push({
          thingName,
          expiryTime: curUnit.expiryTime,
        });
    });

    const expiredData = getExpiredDate(experyList);

    if (expiredData.length > 0) {
      yield call(unbindDevice, expiredData);
    }
  }
}
