export const getTimerList = state => state.timer.timerList;
export const getTimerPairs = state => state.timer.timerPairs;
export const getGroups = state => state.units.groupInfo.groups;
export const getGroupUnits = state => state.units.groupUnits;
export const getUnits = state => state.units.allUnits;

export const getDayIndex = (_, props) =>
  props.route
    ? props.route.params.dayIndex
    : props.navigation.getParam('dayIndex');

export const remoteThingName = state => state.remote.thingName;
export const remoteConnectionType = state => state.remote.connectionType;
export const getCurrentUser = state => state.user.profile.email;
