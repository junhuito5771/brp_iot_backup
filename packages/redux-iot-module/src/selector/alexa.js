import { createSelector } from 'reselect';
import { getGroups, getGroupUnits, getUnits } from './common';

function isValidUnit(unit) {
  return unit && unit.firmwareVersion && unit.ACName && unit.thingName;
}

const getAlexaPreferredUnitsUnits = state => state.preferredUnits.units;

const getAlexaPrerredUnitList = createSelector(
  [getUnits, getGroups, getGroupUnits, getAlexaPreferredUnitsUnits],
  (allUnits, groups, groupUnits, alexaUnits) => {
    const preferredUnits = [];

    Object.values(groups).forEach(groupInfo => {
      const groupInUnits = [];
      const groupKey = groupInfo.groupIndex;
      const targetGroupUnit = groupUnits[groupKey] || [];

      targetGroupUnit.forEach(thingName => {
        const targetUnit = allUnits[thingName];
        const alexaUnit = alexaUnits[thingName];
        // Only push the unit to show if can be found in allUnits
        if (isValidUnit(targetUnit) && alexaUnit) {
          groupInUnits.push(alexaUnit);
        }
      });

      if (groupInUnits.length > 0) {
        preferredUnits.push({
          title: groupInfo.groupName,
          data: groupInUnits,
        });
      }
    });

    return preferredUnits;
  }
);

export default getAlexaPrerredUnitList;
