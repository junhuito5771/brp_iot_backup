import { createSelector } from 'reselect';

const getUnits = state => state.units.allUnits;
const getGroups = state => state.units.groupInfo.groups;
const getGroupUnits = state => state.units.groupUnits;

const getEditGroupIds = state => state.editUnits.itemIds;
const getEditItemData = state => state.editUnits.items;
const getSectionId = (_, props) => props.sectionId;

const getGroupSelectionList = createSelector(
  [getEditGroupIds, getEditItemData, getSectionId],
  (groupIds, groupData, sectionId) => {
    const groupSelection = [];
    // Temporary fix for the acson crash
    (groupIds || []).forEach(({ id }) => {
      if (id !== sectionId && groupData[id] && groupData[id].name) {
        groupSelection.push({ label: groupData[id].name, value: id });
      }
    });

    return groupSelection;
  }
);

export { getGroupSelectionList };

export default createSelector(
  [getUnits, getGroups, getGroupUnits],
  (units, groups, groupUnits) => {
    const unitIds = [];
    const unitsObj = {};
    let index = 0;
    Object.values(groups).forEach(groupInfo => {
      const groupKey = groupInfo.groupIndex;
      const targetGroupUnit = groupUnits[groupKey] || [];

      unitIds.push(index);
      const parentKey = index;
      unitsObj[index] = {
        type: 'group',
        name: groupInfo.groupName,
        key: groupKey,
        index,
        totalChild: targetGroupUnit.length,
      };

      targetGroupUnit.forEach(thingName => {
        const targetUnit = units[thingName];
        // Only push the unit to show if can be found in allUnits
        if (targetUnit) {
          index += 1;
          unitIds.push(index);
          unitsObj[index] = {
            type: 'unit',
            name: targetUnit.ACName,
            group: targetUnit.ACGroup,
            key: thingName,
            parentKey,
            index,
          };
        }
      });

      index += 1;
    });

    return { unitIds, unitsObj, groupUnits: Object.values(unitsObj) };
  }
);
