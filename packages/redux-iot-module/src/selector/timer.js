import { createSelector } from 'reselect';
import { ConnectionType, QXInfoConstant } from '@module/utility';
import produce from 'immer';
import {
  getTimerList,
  remoteThingName,
  remoteConnectionType,
  getTimerPairs,
  getGroups,
  getGroupUnits,
  getUnits,
  getDayIndex,
  getCurrentUser,
} from './common';

export const getUpcomingTimer = createSelector(
  [getTimerList, remoteThingName, remoteConnectionType],
  (timers, thingName, connectionType) => {
    // Do not show upcoming timer if current it's not IOTMode
    if (connectionType !== ConnectionType.IOT_MODE) return undefined;
    const curDT = new Date();
    const curHour = curDT.getHours();
    const curMinute = curDT.getMinutes();
    const curDay = curDT.getDay();

    // Sort the timer array to start with the current day index instead of Sunday
    // Thus it will get the most recent upcoming timer
    const sortedTimer =
      timers[thingName] &&
      produce(timers[thingName], draft => {
        const upcomingDays = draft.filter(
          d =>
            d.day < curDay ||
            (d.day === curDay && d.hour <= curHour && d.minute < curMinute)
        );
        draft.splice(0, upcomingDays.length);
        draft.push(...upcomingDays);
      });

    return (sortedTimer || []).find(
      timerInfo => timerInfo && timerInfo.isActive
    );
  }
);

export default () =>
  createSelector(
    [
      getTimerList,
      getTimerPairs,
      getGroups,
      getGroupUnits,
      getUnits,
      getDayIndex,
      getCurrentUser,
    ],
    (timers, timerPairs, groups, groupUnits, units, dayIndex, curUser) => {
      const groupInArray = Object.values(groups);
      const timerList = [];

      groupInArray.forEach(groupData => {
        const groupUnitKey = groupData.groupIndex;
        const { groupName } = groupData;

        const thingNameList = groupUnits[groupUnitKey] || [];

        const unitInArray = [];

        thingNameList.forEach(thingName => {
          const curUnit = units[thingName];
          if (curUnit) {
            const unitName = curUnit.ACName;
            const { modes } = curUnit;
            const timerLimit =
              curUnit.planFeatures && curUnit.planFeatures.Lim_WeekTimer
                ? parseInt(curUnit.planFeatures.Lim_WeekTimer, 10)
                : 0;

            const curTimers = timers[thingName] || [];
            const timerInArray = [];

            const canCreate = curTimers.length < timerLimit;

            curTimers.forEach(curTimerData => {
              if (curTimerData.day === dayIndex) {
                const canEdit =
                  curUnit.overwriteSchedule === 1 ||
                  curTimerData.user === curUser ||
                  curUnit.qx === QXInfoConstant.HOST;
                timerInArray.push({
                  ...curTimerData,
                  canEdit,
                });
              }
            });

            const timerPairInArray = (timerPairs[thingName] || []).filter(
              curTimerPair => curTimerPair.start.day === dayIndex
            );

            unitInArray.push({
              unitName,
              thingName,
              modes,
              timerLimit,
              timers: timerInArray,
              pairs: timerPairInArray,
              canCreate,
            });
          }
        });

        timerList.push({
          groupName,
          count: thingNameList.length,
          units: unitInArray,
        });
      });

      return timerList;
    }
  );
