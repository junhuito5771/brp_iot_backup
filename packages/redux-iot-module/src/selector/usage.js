import { createSelector } from 'reselect';
import { getUnits } from './common';

export default createSelector([getUnits], units => {
  const usageList = [
    { title: 'Inverter', data: [] },
    { title: 'Non-inverter', data: [] },
  ];

  Object.values(units).forEach(unit => {
    const id = unit.isInverter === 0 ? 0 : 1;
    usageList[id].data.push({
      thingName: unit.thingName,
      unitName: unit.ACName,
      usageValue: unit.powerConsumption || 0, // Watt
      enableEnergyConsumption: unit.enableEnergyConsumption,
    });
  });

  return usageList;
});
