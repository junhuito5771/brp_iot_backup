export const getRemoteUnit = state => {
  const {allUnits} = state.units;
  const {thingName} = state.remote;

  return allUnits[thingName] || {};
};

export const getUnit = (state, thingName) => {
  const {allUnits} = state.units;

  return allUnits[thingName] || {};
};

export { default as getHomepageList } from './homepage';
export { default as getTimerList, getUpcomingTimer } from './timer';
export { default as getUnitUsageList } from './usage';
export { default as getEditUnitList, getGroupSelectionList } from './editUnit';
export { default as getAlexaPreferredUnits } from './alexa';
