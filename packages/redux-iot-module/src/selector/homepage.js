import { createSelector } from 'reselect';
import { checkExpiryTime } from '../sagas/fetchUnitUsers';
import { getGroups, getGroupUnits, getUnits } from './common';

function isValidUnit(unit) {
  return (
    unit &&
    unit.firmwareVersion &&
    unit.ACName &&
    unit.thingName &&
    checkExpiryTime(unit.expiryTime)
  );
}

const getHomepageList = createSelector(
  [getUnits, getGroups, getGroupUnits],
  (allUnits, groups, groupUnits) => {
    const homepageList = [];

    Object.values(groups).forEach(groupInfo => {
      const groupInUnits = [];
      const groupKey = groupInfo.groupIndex;
      const targetGroupUnit = groupUnits[groupKey] || [];

      console.log("homepage.js file");
      var i = 0;
      targetGroupUnit.forEach(thingName => {
        const targetUnit = allUnits[thingName];
        console.log(++i, " = ", targetUnit);
        // Only push the unit to show if can be found in allUnits
        if (isValidUnit(targetUnit)) {
          groupInUnits.push(targetUnit);
        }
      });

      if (groupInUnits.length > 0) {
        homepageList.push({
          group: groupInfo,
          units: groupInUnits,
        });
      }
    });

    return homepageList;
  }
);

export default getHomepageList;
