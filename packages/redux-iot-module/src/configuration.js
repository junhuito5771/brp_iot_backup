class Configuration {
  constructor() {
    if (!Configuration.instance) {
      this.apiEndpoint = '';
      Configuration.instance = this;
    }
  }

  init = config => {
    // Loop through the config and assign the key and value to the configuration
    Object.keys(config).forEach(key => {
      this[key] = config[key];
    });

    // Do not allow this configuration to be edited once it has been init
    Object.freeze(Configuration.instance);
  };

  getIoTConfig = () => ({
    aws_pubsub_region: this.region,
    aws_pubsub_endpoint: `wss://${this.iotEndpoint}/mqtt`,
  });

  getAlexaConfig = () => ({
    alexaClientId: this.alexaClientId,
    alexaClientSecret: this.alexaClientSecret,
    alexaSkillEnaUrl: this.alexaSkillEnaUrl,
    alexaTokenUrl: this.alexaTokenUrl,
  });
}

const config = new Configuration();

export default config;
