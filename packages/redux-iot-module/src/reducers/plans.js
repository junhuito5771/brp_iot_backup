import { SET_ALL_PLANS } from '../actions/plans';

const initialState = {
  featurePlans: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ALL_PLANS:
      return {
        ...state,
        featurePlans: action.value,
      };
    default:
      return state;
  }
};

export default reducer;
