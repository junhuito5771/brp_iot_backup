import produce from 'immer';
import {
  SET_IS_LOADING,
  SET_EDIT_STATUS,
  CLEAR_EDIT_STATUS,
  SET_EDIT_STATE,
  INIT_EDIT_UNIT,
  REMOVE_EDIT_GROUP,
  SET_ITEM_NAME,
  ADD_EDIT_ITEM,
  SET_ITEM_ORDER,
  SET_ITEM_CHILD_ORDER,
  SET_ITEM_DIFF_GROUP,
  SET_ITEM_ICON,
} from '../actions/editUnits';

const initialState = {
  isLoading: false,
  error: '',
  success: '',
  editState: {},
  itemIds: [],
  items: {},
  canSave: false,
  isEdit: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_EDIT_STATUS:
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_EDIT_STATUS:
      return initialState;
    case SET_EDIT_STATE:
      return {
        ...state,
        editState: action.state,
      };
    case INIT_EDIT_UNIT: {
      const { homepageList } = action.initParams;
      const itemIds = [];
      const items = {};

      console.log("editUnits.js REDUCER FILE");
      var i = 0;
      homepageList.forEach(item => {
        console.log(++i," = ",item);
        const { group, units: unitInGroup } = item;

        const childs = [];

        unitInGroup.forEach(unitInfo => {
          childs.push(unitInfo.thingName);
          items[unitInfo.thingName] = {
            type: 'unit',
            name: unitInfo.ACName,
            logo: unitInfo.Logo,
            parentKey: group.groupIndex.toString(),
          };
        });

        itemIds.push({ id: group.groupIndex.toString(), childs });

        items[group.groupIndex] = {
          type: 'group',
          name: group.groupName,
          units: childs,
        };
      });

      return {
        ...state,
        itemIds,
        items,
        canSave: false,
        isEdit: true,
      };
    }
    case SET_ITEM_NAME: {
      const { id, name } = action.editParams;

      return {
        ...state,
        items: {
          ...state.items,
          [id]: {
            ...state.items[id],
            name,
          },
        },
        canSave: true,
      };
    }
    case ADD_EDIT_ITEM: {
      const { name } = action;
      const newGroupIndex =
        Number(state.itemIds[state.itemIds.length - 1].id) + 1;

      return {
        ...state,
        itemIds: [
          ...state.itemIds,
          { id: newGroupIndex.toString(), childs: [] },
        ],
        items: {
          ...state.items,
          [newGroupIndex]: {
            type: 'group',
            name: name || `Group ${newGroupIndex}`,
            units: [],
          },
        },
        canSave: true,
      };
    }
    case REMOVE_EDIT_GROUP: {
      const { index } = action.editParams;

      const newItemIds = produce(state.itemIds, draftState => {
        const hasUngroup = draftState[0].id === '-1';

        if (!hasUngroup) {
          draftState.unshift({ id: '-1', childs: [] });
        }

        const deleteIndex = draftState.findIndex(({ id }) => id === index);

        draftState[deleteIndex].childs.forEach(unit => {
          draftState[0].childs.push(unit);
        });

        draftState.splice(deleteIndex, 1);
      });

      const missingUngroup = state.items['-1'] === undefined;

      return {
        ...state,
        itemIds: newItemIds,
        canSave: true,
        ...(missingUngroup && {
          items: { ...state.items, '-1': { name: 'All Units', type: 'group' } },
        }),
      };
    }
    case SET_ITEM_ORDER: {
      return {
        ...state,
        itemIds: action.orders,
        canSave: true,
      };
    }
    case SET_ITEM_CHILD_ORDER: {
      const { orders, key } = action;
      const { itemIds } = state;

      const targetIndex = itemIds.findIndex(({ id }) => id === key);

      const newItemIds = produce(state.itemIds, draftState => {
        draftState[targetIndex].childs = orders;
      });

      return {
        ...state,
        itemIds: newItemIds,
        canSave: true,
      };
    }
    case SET_ITEM_DIFF_GROUP: {
      const { fromGroup, unit, targetGroup } = action.assignParams;

      const newItemIds = produce(state.itemIds, draftState => {
        const fromGroupIndex = draftState.findIndex(
          ({ id }) => id === fromGroup
        );

        const removedUnitIndex = draftState[fromGroupIndex].childs.findIndex(
          curUnit => curUnit === unit
        );

        draftState[fromGroupIndex].childs.splice(removedUnitIndex, 1);

        if (draftState[fromGroupIndex].childs.length === 0) {
          draftState.splice(fromGroupIndex, 1);
        }

        const targetGroupIndex = draftState.findIndex(
          ({ id }) => id === targetGroup
        );

        draftState[targetGroupIndex].childs.push(unit);
      });

      return {
        ...state,
        itemIds: newItemIds,
        canSave: true,
      };
    }
    case SET_ITEM_ICON: {
      const { key, logo } = action;

      const newItems = produce(state.items, draftState => {
        draftState[key].logo = logo;
      });

      return {
        ...state,
        items: newItems,
        canSave: true,
      };
    }
    default:
      return state;
  }
};

export default reducer;
