import produce from 'immer';

import {
  SET_LOADING,
  SET_USAGE_DATA,
  SET_SELECTED_PERIOD,
  CLEAR_USAGE_STATUS,
  SET_USAGE_STATUS,
  SET_USAGE_COMPARE_DATA,
  CLEAR_COMPARE,
  SET_TOGGLE_BUTTON,
  SET_SELECTED_DATE,
  SET_SELECTED_WEEK,
  SET_SELECTED_MONTH,
  SET_SELECTED_YEAR,
  SET_ELECTRIC_RATE,
} from '../actions/usage';

const initialState = {
  isLoading: false,
  error: '',
  success: '',
  selectedFromDate: null,
  selectedFromWeek: null,
  selectedFromMonth: null,
  selectedFromYear: null,
  curElectricRate: 0.218,
  usageByPeriod: {},
  usageToggle: {},
  usageSetting: {
    thingName: '',
    periodType: 'weekly',
    toggleCompare: false,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SELECTED_DATE:
      return { ...state, selectedFromDate: action.selectedFromDate };
    case SET_SELECTED_WEEK:
      return { ...state, selectedFromWeek: action.selectedFromWeek };
    case SET_SELECTED_MONTH:
      return { ...state, selectedFromMonth: action.selectedFromMonth };
    case SET_SELECTED_YEAR:
      return { ...state, selectedFromYear: action.selectedFromYear };
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_USAGE_DATA: {
      const { periodType, thingName, data } = action.usageParams;
      const usageByPeriodState = state.usageByPeriod;
      const draftResult = produce(usageByPeriodState, draftState => {
        if (!draftState[thingName]) {
          draftState[thingName] = {};
        }

        if (!draftState[thingName][periodType]) {
          draftState[thingName][periodType] = {};
        }

        draftState[thingName][periodType] = {
          ...draftState[thingName][periodType],
          ...data,
        };
      });
      return {
        ...state,
        usageByPeriod: draftResult,
        usageSetting: {
          ...state.usageSetting,
          thingName,
        },
      };
    }
    case SET_USAGE_COMPARE_DATA: {
      const { periodType, thingName, data, targetDate } = action.usageParams;
      const draftResult = produce(state, draftState => {
        if (!draftState.usageByPeriod[thingName]) {
          draftState.usageByPeriod[thingName] = {};
        }

        if (!draftState.usageByPeriod[thingName][periodType]) {
          draftState.usageByPeriod[thingName][periodType] = {};
        }

        draftState.usageByPeriod[thingName][periodType].compareData = data;
        draftState.usageByPeriod[thingName][
          periodType
        ].compareDate = targetDate;
      });

      return { ...state, ...draftResult };
    }
    case SET_SELECTED_PERIOD: {
      const { periodType } = action;
      const draftResult = produce(state, draftState => {
        draftState.usageSetting.periodType = periodType;
      });
      return { ...state, ...draftResult };
    }
    case SET_TOGGLE_BUTTON: {
      const { periodType, thingName, type, isEnergySup } = action.toggleParams;
      const usageToggleInit = {
        setTemp: true,
        indoorTemp: true,
        energyConsumption: true,
        onOffUnit: true,
        cpRT: true,
        electricRate: true,
        isCompare: false,
      };

      const draftResult = produce(state, draftState => {
        if (!state.usageToggle[thingName]) {
          draftState.usageToggle[thingName] = {};
          draftState.usageToggle[thingName][periodType] = usageToggleInit;
        } else if (!state.usageToggle[thingName][periodType]) {
          draftState.usageToggle[thingName][periodType] = usageToggleInit;
        } else {
          const {isCompare} = state.usageToggle[thingName][periodType];
          if (type === 'setTemp') {
            draftState.usageToggle[thingName][periodType] = {
              setTemp: !state.usageToggle[thingName][periodType].setTemp,
              indoorTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].indoorTemp,
              energyConsumption: isCompare
                ? false
                : state.usageToggle[thingName][periodType].energyConsumption,
              onOffUnit: isCompare
                ? false
                : state.usageToggle[thingName][periodType].onOffUnit,
              cpRT: isCompare
                ? false
                : state.usageToggle[thingName][periodType].cpRT,
              electricRate: isCompare
                ? false
                : state.usageToggle[thingName][periodType].electricRate,
              isCompare,
            };
          }
          if (type === 'indoorTemp') {
            draftState.usageToggle[thingName][periodType] = {
              indoorTemp: !state.usageToggle[thingName][periodType].indoorTemp,
              setTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].setTemp,
              energyConsumption: isCompare
                ? false
                : state.usageToggle[thingName][periodType].energyConsumption,
              onOffUnit: isCompare
                ? false
                : state.usageToggle[thingName][periodType].onOffUnit,
              cpRT: isCompare
                ? false
                : state.usageToggle[thingName][periodType].cpRT,
              electricRate: isCompare
                ? false
                : state.usageToggle[thingName][periodType].electricRate,
              isCompare,
            };
          }

          if (type === 'energyConsumption') {
            draftState.usageToggle[thingName][periodType] = {
              energyConsumption: !state.usageToggle[thingName][periodType]
                .energyConsumption,
              setTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].setTemp,
              indoorTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].indoorTemp,
              onOffUnit: isCompare
                ? false
                : state.usageToggle[thingName][periodType].onOffUnit,
              cpRT: isCompare
                ? false
                : state.usageToggle[thingName][periodType].cpRT,
              electricRate: isCompare
                ? false
                : state.usageToggle[thingName][periodType].electricRate,
              isCompare,
            };
          }
          if (type === 'onOffUnit') {
            draftState.usageToggle[thingName][periodType] = {
              onOffUnit: !state.usageToggle[thingName][periodType].onOffUnit,
              setTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].setTemp,
              indoorTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].indoorTemp,
              energyConsumption: isCompare
                ? false
                : state.usageToggle[thingName][periodType].energyConsumption,
              cpRT: isCompare
                ? false
                : state.usageToggle[thingName][periodType].cpRT,
              electricRate: isCompare
                ? false
                : state.usageToggle[thingName][periodType].electricRate,
              isCompare,
            };
          }
          if (type === 'cpRT') {
            draftState.usageToggle[thingName][periodType] = {
              cpRT: !state.usageToggle[thingName][periodType].cpRT,
              setTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].setTemp,
              indoorTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].indoorTemp,
              energyConsumption: isCompare
                ? false
                : state.usageToggle[thingName][periodType].energyConsumption,
              onOffUnit: isCompare
                ? false
                : state.usageToggle[thingName][periodType].onOffUnit,
              electricRate: isCompare
                ? false
                : state.usageToggle[thingName][periodType].electricRate,
              isCompare,
            };
          }
          if (type === 'electricRate') {
            draftState.usageToggle[thingName][periodType] = {
              electricRate: !state.usageToggle[thingName][periodType]
                .electricRate,
              setTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].setTemp,
              indoorTemp: isCompare
                ? false
                : state.usageToggle[thingName][periodType].indoorTemp,
              energyConsumption: isCompare
                ? false
                : state.usageToggle[thingName][periodType].energyConsumption,
              onOffUnit: isCompare
                ? false
                : state.usageToggle[thingName][periodType].onOffUnit,
              cpRT: isCompare
                ? false
                : state.usageToggle[thingName][periodType].cpRT,
              isCompare,
            };
          }
          if (type === 'setCompare') {
            draftState.usageToggle[thingName][periodType] = {
              onOffUnit: false,
              setTemp: false,
              indoorTemp: !isEnergySup,
              energyConsumption: !!isEnergySup,
              cpRT: false,
              electricRate: false,
              isCompare: true,
            };
          }
        }
      });
      return {
        ...state,
        ...draftResult,
      };
    }
    case CLEAR_COMPARE: {
      const { periodType, thingName } = action.value;
      const draftResult = produce(state, draftState => {
        const {compareData} = draftState.usageByPeriod[thingName][periodType];
        const {compareDate} = draftState.usageByPeriod[thingName][periodType];

        const usageToggleInit = {
          setTemp: true,
          indoorTemp: true,
          energyConsumption: true,
          onOffUnit: true,
          cpRT: true,
          electricRate: true,
          isCompare: false,
        };

        if (compareData) {
          delete draftState.usageByPeriod[thingName][periodType].compareData;
          draftState.usageToggle[thingName][periodType] = usageToggleInit;
        }
        if (compareDate) {
          delete draftState.usageByPeriod[thingName][periodType].compareDate;
        }
        if (periodType === 'daily') {
          draftState.selectedFromDate = null;
        }
        if (periodType === 'weekly') {
          draftState.selectedFromWeek = null;
        }
        if (periodType === 'monthly') {
          draftState.selectedFromMonth = null;
        }
        if (periodType === 'yearly') {
          draftState.selectedFromYear = null;
        }
      });

      return { ...state, ...draftResult };
    }
    case SET_USAGE_STATUS:
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_USAGE_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    case SET_ELECTRIC_RATE: {
      return {
        ...state,
        curElectricRate: action.value,
      };
    }

    default:
      return state;
  }
};

export default reducer;
