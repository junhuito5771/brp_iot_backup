import {
  SET_AGREEMENT_VIEWED,
  SET_AGREEMENT_ACCEPTED,
} from '../actions/userAgreement';

const initialState = {
  agreementViewed: false,
  agreementAccepted: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_AGREEMENT_VIEWED:
      return {
        ...state,
        agreementViewed: action.agreementViewed,
      };
    case SET_AGREEMENT_ACCEPTED:
      return {
        ...state,
        agreementAccepted: action.agreementAccepted,
      };
    default:
      return state;
  }
};

export default reducer;
