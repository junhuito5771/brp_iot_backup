import {
  SET_BLE_UNITS,
  SET_BLE_LOADING,
  CLEAR_BLE_UNITS,
  SET_BLE_STATUS,
  CLEAR_BLE_STATUS,
} from '../actions/bleMode';

const initialState = {
  units: [],
  error: '',
  success: '',
  isLoading: false,
  lastUpdated: new Date().getTime(),
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_BLE_UNITS:
      return {
        ...state,
        units: { ...action.bleUnits },
        lastUpdated: new Date().getTime(),
      };
    case SET_BLE_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case CLEAR_BLE_UNITS:
      return {
        ...state,
        units: {},
        lastUpdated: new Date().getTime(),
      };
    case SET_BLE_STATUS:
      return {
        ...state,
        [action.messageType]: action.message,
      };
    case CLEAR_BLE_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    default:
      return state;
  }
};

export default reducer;
