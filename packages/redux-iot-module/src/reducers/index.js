import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import EncryptedStorage from 'react-native-encrypted-storage';

import units from './units';
import pairDevice from './pairDevice';
import deviceConfig from './deviceConfig';
import homepageIotShadow from './homepageIotShadow';
import remote from './remote';
import editUnits from './editUnits';
import bleMode from './bleMode';
import settings from './settings';
import unitUsers from './unitUsers';
import bindGuestDevice from './bindGuestDevice';
import unbindDevice from './unbindDevice';
import wlanMode from './wlanMode';
import unitUserControl from './unitUserControl';
import bleControl from './bleControl';
import updateFirmware from './updateFirmware';
import timer from './timer';
import editTimer from './editTimer';
import usage from './usage';
import scanSSID from './scanSSID';
import tutorialFlag from './tutorialFlag';
import preferredUnits from './preferredUnits';

const unitsPersistConfig = {
  key: 'units',
  storage: AsyncStorage,
  whitelist: ['hasNotifiedError'],
};

const pairDeviceConfig = {
  key: 'pairDevice',
  storage: AsyncStorage,
  whitelist: ['lastSavedSSID'],
};

const tutorialFlagPersistConfig = {
  key: 'tutorialFlag',
  storage: AsyncStorage,
};

const preferredUnitsConfig = {
  key: 'preferredUnits',
  storage: EncryptedStorage,
  whitelist: ['alexaToken'],
};

const usageConfig = {
  key: 'usage',
  storage: AsyncStorage,
  whitelist: ['curElectricRate'],
};

const rootReducer = {
  units: persistReducer(unitsPersistConfig, units),
  pairDevice: persistReducer(pairDeviceConfig, pairDevice),
  tutorialFlag: persistReducer(tutorialFlagPersistConfig, tutorialFlag),
  deviceConfig,
  homepageIotShadow,
  remote,
  editUnits,
  bleMode,
  settings,
  unitUsers,
  bindGuestDevice,
  unbindDevice,
  wlanMode,
  unitUserControl,
  bleControl,
  updateFirmware,
  timer,
  editTimer,
  usage: persistReducer(usageConfig, usage),
  scanSSID,
  preferredUnits: persistReducer(preferredUnitsConfig, preferredUnits),
};

export default rootReducer;
