import {
  CLEAR_STATUS,
  SET_LOADING,
  TOGGLE_UNIT,
  SET_PREFFERED_UNITS,
  SET_PREFERRED_STATUS,
  SET_ALEXA_TOKEN,
  SET_ALEXA_TOKEN_STATUS,
  CLEAR_ALEXA_TOKEN_STATUS,
  SET_ALEXA_COMMAND_URL,
  SUBMIT_LOGOUT_PARAMS,
  RESET_ALEXA_COMMAND_REFRESH,
} from '../actions/preferredUnits';

const initialState = {
  units: {},
  isLoading: false,
  success: '',
  error: '',
  canSubmit: false,
  alexaToken: {
    accessToken: '',
    expiresIn: 0,
    refreshToken: '',
    tokenType: '',
  },
  alexaCommand: {
    url: '',
    title: '',
    lastUpdateDT: '',
    needRefresh: false,
  },
  alexaLinkStatus: '', // tokenSucess or tokenError or activePending or activeSucess or activeError
  alexaLinkErrTitle: '',
  alexaLinkErrMsg: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case CLEAR_STATUS:
      return {
        ...state,
        success: '',
        error: '',
        canSubmit: false,
      };
    case TOGGLE_UNIT:
      return {
        ...state,
        units: {
          ...state.units,
          [action.thingName]: {
            ...state.units[action.thingName],
            isAlexaPreferred: !state.units[action.thingName].isAlexaPreferred,
          },
        },
        canSubmit: true,
      };
    case SET_PREFFERED_UNITS:
      return {
        ...state,
        units: action.prefferedUnits,
        canSubmit: false,
      };
    case SET_PREFERRED_STATUS:
      return {
        ...state,
        [action.statusParams.messageType]: action.statusParams.message,
      };
    case SET_ALEXA_TOKEN: {
      return {
        ...state,
        alexaToken: action.tokenParams,
      };
    }
    case SET_ALEXA_TOKEN_STATUS: {
      return {
        ...state,
        ...action.statusParams,
      };
    }
    case CLEAR_ALEXA_TOKEN_STATUS: {
      return {
        ...state,
        alexaLinkStatus: '',
        alexaLinkErrTitle: '',
        alexaLinkErrMsg: '',
      };
    }
    case SET_ALEXA_COMMAND_URL: {
      return {
        ...state,
        alexaCommand: action.commandParams,
      };
    }
    case SUBMIT_LOGOUT_PARAMS: {
      return initialState;
    }
    case RESET_ALEXA_COMMAND_REFRESH: {
      return {
        ...state,
        alexaCommand: {
          ...state.alexaCommand,
          needRefresh: false,
        },
      };
    }
    default:
      return state;
  }
};

export default reducer;
