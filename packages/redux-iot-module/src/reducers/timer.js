import produce from 'immer';
import {
  SET_FETCH_TIMER_LOADING,
  SET_FETCH_TIMER_STATUS,
  SET_TIMER,
  CLEAR_TIMER,
  SET_TOGGLE_TIMER,
  SET_TOGGLE_TIMER_LOADING,
  REMOVE_TOGGLE_TIMER_LOADING,
  SET_TIMER_PAIRS,
  SET_UNIT_TIMERS,
  SET_QUICK_TIMER,
  ADD_QUICK_TIMER,
  REMOVE_QUICK_TIMER,
  SET_QUICK_TIMER_LOADING,
  SET_QUICK_TIMER_ERROR,
  CLEAR_QUICK_TIMER_ERROR,
  CLEAR_FETCH_TIMER_STATUS,
} from '../actions/timer';

/*
Proposed 1:
// Easy and simple
Structure for timer
timer: {
  [thingName]: Sorted[{
    day: number, (key)
    hour: number, (key)
    minute: number, (key)
    isActive: boolean,
    mode: string,
    switchValue: boolean,
    tempe: number,
    fanOptions: {
      silent: number,
      fan: string, enum('Auto', 'Level1', 'Level2', 'Level3')
    }
  }]
}

Screen: [
  {
    thingName: [index1, index2, index3]
  },
  {
    thingName: [index1, index2, index3]
  }
]

Proposed 2: (Not good as it won't be refered by other data)
timer: {
  [thingName]: {
    allTimerIds: sorted[day_hour_minute_1, day_hour_minute_2, day_hour_minute_3]
    byId: {
      [day_hour_minute]: {
        day: number, (key)
        hour: number, (key)
        minute: number, (key)
        isActive: boolean,
        mode: string,
        switchValue: boolean,
        tempe: number,
        fanOptions: {
          silent: number,
          fan: string, enum('Auto', 'Level1', 'Level2', 'Level3')
        }
      }
    }
  }
}
*/

const initialState = {
  isLoading: false,
  success: '',
  error: '',
  timerList: {},
  toggleLoadingList: {},
  timerPairs: {},
  quickTimerList: {},
  isQuickTimerLoading: false,
  quickTimerError: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FETCH_TIMER_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_FETCH_TIMER_STATUS:
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    case SET_TIMER:
      return {
        ...state,
        timerList: action.timerData,
      };
    case SET_TOGGLE_TIMER_LOADING: {
      const { thingName, timerIndex } = action.toggleLoadingParams;

      const toggleLoadingListInState = state.toggleLoadingList;
      const newToggleLoadingList = produce(
        toggleLoadingListInState,
        draftState => {
          if (!draftState[thingName]) draftState[thingName] = {};
          draftState[thingName][timerIndex] = true;
        }
      );

      return {
        ...state,
        toggleLoadingList: newToggleLoadingList,
      };
    }
    case REMOVE_TOGGLE_TIMER_LOADING: {
      const { thingName, timerIndex } = action.toggleLoadingParams;
      let newToggleLoadingList = {};

      const toggleLoadingListInState = state.toggleLoadingList;
      if (
        toggleLoadingListInState &&
        Object.keys(toggleLoadingListInState) > 0
      ) {
        newToggleLoadingList = produce(toggleLoadingListInState, draftState => {
          delete draftState[thingName][timerIndex];

          if (Object.keys(draftState[thingName]).length === 0) {
            delete draftState[thingName];
          }
        });
      }

      return {
        ...state,
        toggleLoadingList: newToggleLoadingList,
      };
    }
    case SET_TOGGLE_TIMER: {
      const { thingName, timerIndex } = action.toggleParams;
      const timersInState = state.timerList;

      const newTimerInState = produce(timersInState, draftState => {
        draftState[thingName][timerIndex].isActive = !draftState[thingName][
          timerIndex
        ].isActive;
      });

      return {
        ...state,
        timerList: newTimerInState,
      };
    }
    case SET_TIMER_PAIRS:
      return {
        ...state,
        timerPairs: action.timerPairs,
      };
    case SET_UNIT_TIMERS: {
      const { timers, timerPairs, thingName } = action.unitTimersParams;
      return {
        ...state,
        timerList: {
          ...state.timerList,
          [thingName]: timers,
        },
        timerPairs: {
          ...state.timerPairs,
          [thingName]: timerPairs,
        },
      };
    }
    case SET_QUICK_TIMER_LOADING: {
      return {
        ...state,
        isQuickTimerLoading: action.isLoading,
      };
    }
    case SET_QUICK_TIMER:
      return {
        ...state,
        quickTimerList: action.quickTimerList,
      };
    case ADD_QUICK_TIMER:
      return {
        ...state,
        quickTimerList: { ...state.quickTimerList, ...action.quickTimer },
      };
    case REMOVE_QUICK_TIMER: {
      const { thingName } = action;
      const quickTimerList = produce(state.quickTimerList, draftState => {
        delete draftState[thingName];
      });
      return {
        ...state,
        quickTimerList,
      };
    }
    case SET_QUICK_TIMER_ERROR: {
      return {
        ...state,
        quickTimerError: action.errMsg,
      };
    }
    case CLEAR_QUICK_TIMER_ERROR: {
      return {
        ...state,
        quickTimerError: '',
      };
    }
    case CLEAR_TIMER:
      return initialState;
    case CLEAR_FETCH_TIMER_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    default:
      return state;
  }
};

export default reducer;
