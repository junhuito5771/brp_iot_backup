import { ConnectionType } from '@module/utility';
import {
  SET_REMOTE_UNIT,
  SET_REMOTE_STATUS,
  CLEAR_REMOTE_STATUS,
  SET_LAST_UPDATE_TIME,
  SET_REMOTE_LOADING,
  SET_ERROR_HISTORY,
  CLEAR_ERROR_HISTORY,
  SET_RECONNECT,
  SET_EXPECTED_TEMP,
  CLEAR_DESIRE_TEMP,
  SET_CONNECTION_STATUS,
} from '../actions/remote';
import ConnectionStatusEnum from '../utils/connectionStatus';

const initialState = {
  isLoading: false,
  isReconnect: false,
  error: '',
  success: '',
  thingName: '',
  connectionType: ConnectionType.IOT_MODE, // Either IOT, BLE, WLAN
  connectionID: '', // Either BLE ID or IP Address
  connectionStatus: ConnectionStatusEnum.CONNECTING, // CONNECTED, DISCONNECTED, CONNECTING
  lastUpdated: new Date().getTime(),
  errorHistory: [],
  desired: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_ERROR_HISTORY:
      return { ...state, errorHistory: action.value };
    case SET_REMOTE_UNIT:
      return {
        ...state,
        thingName: action.thingName,
        connectionType: action.connectionType
          ? action.connectionType
          : ConnectionType.IOT_MODE,
        connectionID: action.connectionID,
        isReconnect: false,
      };

    case SET_REMOTE_STATUS:
      return {
        ...state,
        [action.status.messageType]: action.status.message,
      };
    case SET_RECONNECT:
      return {
        ...state,
        isReconnect: action.isReconnect,
      };
    case CLEAR_REMOTE_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    case SET_EXPECTED_TEMP:
      return {
        ...state,
        desired: { temp: action.value },
      };
    case CLEAR_DESIRE_TEMP:
      return {
        ...state,
        desired: {},
      };
    case SET_REMOTE_LOADING:
      return {
        ...state,
        isLoading: action.value,
      };
    case CLEAR_ERROR_HISTORY:
      return { ...state, errorHistory: [] };
    case SET_LAST_UPDATE_TIME:
      return { ...state, lastUpdated: action.value };
    case SET_CONNECTION_STATUS:
      return { ...state, connectionStatus: action.status };
    default:
      return state;
  }
};

export default reducer;
