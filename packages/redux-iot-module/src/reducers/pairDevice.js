import {
  SET_WIFI_CONFIG_STATUS,
  SET_IOT_STATUS_LOADING,
  SET_IOT_STATUS,
  INCREMENT_RETRY,
  RESET_RETRY,
  CLEAR_IOT_STATUS,
} from '../actions/pairDevice';

const initialState = {
  loadingStatus: 0,
  loadingText: '',
  error: '',
  success: '',
  eventType: '',
  retryRequestNr: 0,
  lastSavedSSID: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_WIFI_CONFIG_STATUS:
      return {
        ...state,
        eventType: action.eventType || '',
        [action.messageType]: action.message,
      };
    case SET_IOT_STATUS:
      return {
        ...state,
        eventType: action.iotStatus.eventType || '',
        [action.iotStatus.messageType]: action.iotStatus.message,
        lastSavedSSID: action.iotStatus.ssid,
      };
    case SET_IOT_STATUS_LOADING:
      return {
        ...state,
        loadingStatus: action.iotStatusLoading.loadingStatus,
        loadingText: action.iotStatusLoading.loadingText,
      };
    case INCREMENT_RETRY:
      return {
        ...state,
        retryRequestNr: state.retryRequestNr + 1,
      };
    case RESET_RETRY:
      return {
        ...state,
        retryRequestNr: 0,
      };
    case CLEAR_IOT_STATUS:
      return {
        ...state,
        success: '',
        error: '',
        loadingStatus: 0,
        loadingText: '',
      };
    default:
      return state;
  }
};

export default reducer;
