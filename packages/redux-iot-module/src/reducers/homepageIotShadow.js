import {
  SET_MQTT_STATUS,
  IS_LOADING,
  ADD_WAITING_UNIT,
  REMOVE_WAITING_UNIT,
  MQTT_CLEAR_STATUS,
  UNIT_LOADING,
  SET_TOAST_MESSAGE,
} from '../actions/homepageIotShadow';

const initialState = {
  status: '',
  isLoading: true,
  waitingList: {},
  unitLoadingList: {},
  toastMsg: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_MQTT_STATUS:
      return {
        ...state,
        status: action.status,
      };
    case MQTT_CLEAR_STATUS:
      return initialState;
    case UNIT_LOADING:
      return {
        ...state,
        unitLoadingList: {
          ...state.unitLoadingList,
          ...action.unitLoadingList,
        },
      };
    case IS_LOADING:
      return {
        ...state,
        isLoading: action.loading,
      };
    case ADD_WAITING_UNIT:
      return {
        ...state,
        waitingList: {
          ...state.waitingList,
          [action.unit]: action.unit,
        },
      };
    case REMOVE_WAITING_UNIT: {
      const newWaitingList = { ...state.waitingList };
      delete newWaitingList[action.unit];
      return {
        ...state,
        waitingList: newWaitingList,
      };
    }
    case SET_TOAST_MESSAGE: {
      return {
        ...state,
        toastMsg: action.msg,
      };
    }
    default:
      return state;
  }
};

export default reducer;
