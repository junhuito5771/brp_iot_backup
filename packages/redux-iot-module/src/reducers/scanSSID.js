import {
  SET_SCAN_SSID_LOADING,
  SET_NEARBY_SSID,
  SET_SCAN_SSID_STATUS,
  CLEAR_SCAN_SSID_STATUS,
} from '../actions/scanSSID';

const initialState = {
  isLoading: false,
  ssidList: [],
  success: '',
  error: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SCAN_SSID_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
        success: '',
        error: '',
      };
    case SET_NEARBY_SSID:
      return {
        ...state,
        ssidList: action.ssidList,
      };
    case SET_SCAN_SSID_STATUS:
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_SCAN_SSID_STATUS:
      return {
        ...state,
        error: '',
        success: '',
      };
    default:
      return state;
  }
};

export default reducer;
