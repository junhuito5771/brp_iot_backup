import {
  SET_DEVICE_CONFIG_LOADING,
  CLEAR_DEVICE_CONFIG_STATUS,
  SET_DEVICE_CONFIG_STATUS,
  SET_SELECTED_GROUP,
} from '../actions/deviceConfig';

const initialState = {
  selectedGroup: null,
  isLoading: false,
  error: '',
  success: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DEVICE_CONFIG_LOADING:
      return {
        ...state,
        isLoading: action.deviceConfigLoading.isLoading,
      };
    case CLEAR_DEVICE_CONFIG_STATUS:
      return initialState;
    case SET_DEVICE_CONFIG_STATUS:
      return {
        ...state,
        [action.deviceConfigStatus.messageType]:
          action.deviceConfigStatus.message,
      };
    case SET_SELECTED_GROUP:
      return {
        ...state,
        selectedGroup: action.selectedGroup,
      };
    default:
      return state;
  }
};

export default reducer;
