import {
  SET_VALIDITY_PERIOD,
  SET_SAGA_ERROR,
  SET_FIRMWARE_VERSION,
  SET_REMOTE_INSTRUCT,
} from '../actions/settings';

const initialState = {
  qrValidityPeriod: { label: '30 minutes', value: 1800000 },
  sagaError: '',
  firmwareVersion: '',
  showRemoteInstruct: true,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_VALIDITY_PERIOD:
      return {
        ...state,
        qrValidityPeriod: action.qrValidityPeriod,
      };
    case SET_SAGA_ERROR:
      return {
        ...state,
        sagaError: action.error,
      };
    case SET_FIRMWARE_VERSION:
      return {
        ...state,
        firmwareVersion: action.firmwareVersion,
      };
    case SET_REMOTE_INSTRUCT:
      return {
        ...state,
        showRemoteInstruct: action.value,
      };
    default:
      return state;
  }
};

export default reducer;
