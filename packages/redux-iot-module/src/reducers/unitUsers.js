import {
  SET_UNIT_USERS_LOADING,
  SET_UNIT_USERS_LIST,
  CLEAR_UNIT_USERS_STATUS,
  SET_UNIT_USERS_STATUS,
  CLEAR_UNIT_USERS,
  SET_CONTROL_LIMIT,
  SET_GUEST_NUMBER,
} from '../actions/unitUsers';

const initialState = {
  isLoading: false,
  error: '',
  success: '',
  totalShared: 0,
  canControlLimit: false,
  unitUserList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_UNIT_USERS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_UNIT_USERS_LIST:
      return {
        ...state,
        unitUserList: [...action.unitUsersParams.unitUsersList],
      };
    case SET_UNIT_USERS_STATUS:
      return {
        ...state,
        [action.unitUsersParams.messageType]: action.unitUsersParams.message,
      };
    case CLEAR_UNIT_USERS_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    case SET_CONTROL_LIMIT:
      return {
        ...state,
        canControlLimit: action.value,
      };
    case SET_GUEST_NUMBER:
      return {
        ...state,
        totalShared: action.value,
      };
    case CLEAR_UNIT_USERS:
      return initialState;
    default:
      return state;
  }
};

export default reducer;
