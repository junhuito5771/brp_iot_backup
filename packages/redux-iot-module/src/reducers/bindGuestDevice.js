import {
  SET_BIND_GUEST_LOADING,
  SET_BIND_GUEST_STATUS,
  CLEAR_BIND_GUEST_STATUS,
} from '../actions/bindGuestDevice';

const initialState = {
  isLoading: false,
  error: '',
  success: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_BIND_GUEST_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case CLEAR_BIND_GUEST_STATUS:
      return initialState;
    case SET_BIND_GUEST_STATUS:
      return {
        ...state,
        [action.bindGuestDeviceStatus.messageType]:
          action.bindGuestDeviceStatus.message,
      };
    default:
      return state;
  }
};

export default reducer;
