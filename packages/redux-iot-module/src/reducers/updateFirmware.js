import {
  SET_FIRMWARE_LOADING,
  SET_UPDATE_STATUS,
  CLEAR_UPDATE_STATUS,
  SET_UPLOAD_COMPLETED,
} from '../actions/updateFirmware';

const initialState = {
  isLoading: false,
  loadingText: '',
  error: '',
  success: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_FIRMWARE_LOADING:
      return {
        ...state,
        isLoading: action.loadingParams.isLoading,
        loadingText: action.loadingParams.loadingText,
      };
    case SET_UPLOAD_COMPLETED:
      return {
        ...state,
        isLoading: false,
        loadingText: '',
      };
    case SET_UPDATE_STATUS:
      return {
        ...state,
        [action.status.messageType]: action.status.message,
      };
    case CLEAR_UPDATE_STATUS:
      return initialState;
    default:
      return state;
  }
};

export default reducer;
