import {
  WLAN_SET_STATUS,
  WLAN_CLEAR_STATUS,
  WLAN_SET_LOADING,
  WLAN_SET_UNITS,
  WLAN_SET_PAIR_LOADING,
} from '../actions/wlanMode';

const initialState = {
  units: [],
  success: '',
  error: '',
  isLoading: false,
  pairSuccess: '',
  pairError: '',
  isPairLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case WLAN_SET_UNITS:
      return {
        ...state,
        units: action.units,
      };
    case WLAN_SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case WLAN_SET_STATUS:
      return {
        ...state,
        success: '',
        error: '',
        [action.status.messageType]: action.status.message,
      };
    case WLAN_CLEAR_STATUS:
      return {
        ...state,
        success: '',
        error: '',
        pairSuccess: '',
        pairError: '',
      };
    case WLAN_SET_PAIR_LOADING:
      return {
        ...state,
        isPairLoading: action.isLoading,
      };
    default:
      return state;
  }
};

export default reducer;
