import {
  BLECONTROL_SET_CONNECTED_UNIT,
  BLECONTROL_CLEAR_STATUS,
  BLECONTROL_SET_STATUS,
  BLECONTROL_SET_LOADING,
  BLECONTROL_SET_RESPONSE,
  BLECONTROL_SET_PAIR_LOADING,
} from '../actions/bleControl';

const initialState = {
  unit: {},
  error: '',
  success: '',
  isLoading: false,
  response: '',
  pairLoading: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case BLECONTROL_SET_CONNECTED_UNIT:
      return {
        ...state,
        unit: action.peripheralProps,
      };
    case BLECONTROL_SET_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case BLECONTROL_CLEAR_STATUS:
      return initialState;
    case BLECONTROL_SET_STATUS:
      return {
        ...state,
        [action.bleControlStatus.messageType]: action.bleControlStatus.message,
      };
    case BLECONTROL_SET_RESPONSE:
      return {
        ...state,
        response: action.response,
      };
    case BLECONTROL_SET_PAIR_LOADING:
      return {
        ...state,
        pairLoading: action.pairLoading,
      };
    default:
      return state;
  }
};

export default reducer;
