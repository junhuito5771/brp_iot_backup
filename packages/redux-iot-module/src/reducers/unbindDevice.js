import {
  SET_UNBIND_DEVICE_LOADING,
  SET_UNBIND_DEVICE_STATUS,
  CLEAR_UNBIND_DEVICE_STATUS,
  SET_UNBIND_GUEST_LOADING,
} from '../actions/unbindDevice';

const initialState = {
  isLoading: false,
  error: '',
  success: '',
  guestSuccess: '',
  guestError: '',
  isGuestLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_UNBIND_DEVICE_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_UNBIND_GUEST_LOADING:
      return {
        ...state,
        isGuestLoading: action.isLoading,
      };
    case CLEAR_UNBIND_DEVICE_STATUS:
      return initialState;
    case SET_UNBIND_DEVICE_STATUS:
      return {
        ...state,
        [action.unbindDeviceStatus.messageType]:
          action.unbindDeviceStatus.message,
      };
    default:
      return state;
  }
};

export default reducer;
