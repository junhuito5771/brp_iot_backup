import produce from 'immer';
import {
  SET_UNITS_STATUS,
  SET_GROUPS,
  SET_GROUP_UNITS,
  SET_ALL_UNITS,
  CLEAR_UNITS_STATUS,
  SET_UNITS_LOADING,
  SET_UNIT,
  SET_NOTIFIED_ERROR,
  UPDATE_NOTIFIED_ERROR,
  SET_FIRMWARE,
  SET_UNIT_PREV_CONFIG,
  REMOVE_PREV_CONFIG_PROPS,
  SET_UNIT_NAME,
} from '../actions/units';

const initialState = {
  hasNotifiedError: {},
  allUnits: {},
  groupInfo: {
    groups: {},
    lastGroupIndex: 0,
  },
  groupUnits: {},
  unitCount: 0,
  error: '',
  success: '',
  lastUpdated: new Date().getTime(),
  isLoading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_NOTIFIED_ERROR:
      return {
        ...state,
        hasNotifiedError: {
          ...state.hasNotifiedError,
          [action.thingName]: action.value,
        },
      };
    case SET_NOTIFIED_ERROR:
      return { ...state, hasNotifiedError: action.value };
    case SET_UNITS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_ALL_UNITS:
      console.log("redux-iot-module/src/reducers/units.js");
      console.log("SET_ALL_UNITS!");
      return {
        ...state,
        allUnits: { ...action.allUnits },
        unitCount: Object.keys(action.allUnits).length,
        lastUpdated: action.lastUpdated,
      };
    case SET_GROUPS:
      return {
        ...state,
        groupInfo: action.groupInfo,
      };
    case SET_GROUP_UNITS:
      return {
        ...state,
        groupUnits: action.groupUnits,
      };
    case SET_UNITS_STATUS:
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_UNITS_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    case SET_UNIT:
      return {
        ...state,
        allUnits: {
          ...state.allUnits,
          [action.unitProps.thingName]: {
            ...state.allUnits[action.unitProps.thingName],
            ...action.unitProps,
          },
        },
      };
    case SET_FIRMWARE:
      return {
        ...state,
        firmwareInfo: {
          version: action.firmwareInfo.version,
          content: action.firmwareInfo.content,
        },
      };
    case SET_UNIT_PREV_CONFIG: {
      const { thingName, shadowData } = action.prevConfig;

      const allUnits = produce(state.allUnits, draftState => {
        const draftUnit = draftState[thingName];

        draftUnit.prevConfig = {
          ...draftUnit.prevConfig,
          ...shadowData,
        };
      });

      return {
        ...state,
        allUnits,
      };
    }
    case REMOVE_PREV_CONFIG_PROPS: {
      const { thingName, keys } = action.prevConfig;

      const allUnits = produce(state.allUnits, draftState => {
        const draftUnit = draftState[thingName];
        // Only go through if prevConfig is defined
        if (
          draftUnit.prevConfig &&
          Object.keys(draftUnit.prevConfig).length > 0
        ) {
          keys.forEach(key => {
            delete draftUnit.prevConfig[key];
          });
        }
        // Delete prev config if it has empty property
        if (
          draftUnit.prevConfig &&
          Object.keys(draftUnit.prevConfig).length < 1
        ) {
          delete draftUnit.prevConfig;
        }
      });

      return {
        ...state,
        allUnits,
      };
    }
    case SET_UNIT_NAME: {
      const { unitName, thingName } = action.editParams;

      const allUnits = produce(state.allUnits, draftState => {
        const draftUnit = draftState[thingName];

        draftUnit.ACName = unitName;
      });

      return {
        ...state,
        allUnits,
      };
    }
    default:
      return state;
  }
};

export default reducer;
