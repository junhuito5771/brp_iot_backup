import {
  SET_IS_ADD_NEW_GROUP_DISPLAYED,
  SET_IS_DELETE_GROUP_DISPLAYED,
  SET_IS_EDIT_NAME_DISPLAYED,
  SET_IS_GRAPH_CLEAR_RESULT_DISPLAYED,
  SET_IS_GRAPH_COMPARE_DATE_DISPLAYED,
  SET_IS_GRAPH_ELECTRICITY_BILL_RATE_DISPLAYED,
  SET_IS_GRAPH_NAVIGATE_DISPLAYED,
  SET_IS_GRAPH_NAVIGATE_PERIOD_DISPLAYED,
  SET_IS_GRAPH_SCROLL_LINE_INDICATOR_DISPLAYED,
  SET_IS_GRAPH_TOGGLE_INDICATOR_DISPLAYED,
  SET_IS_REARRANGE_GROUP_DISPLAYED,
  SET_IS_REARRANGE_UNIT_DISPLAYED,
  SET_IS_REFRESH_DISPLAYED,
} from '../actions/tutorialFlag';

const initialState = {
  // Home
  isRefreshDisplayed: false,
  isAddNewGroupDisplayed: false,
  isRearrangeGroupDisplayed: false,
  isRearrangeUnitDisplayed: false,
  isEditNameDisplayed: false,
  isDeleteGroupDisplayed: false,
  // Energy Consumption
  isGraphNavigateDisplayed: false,
  // Graph Info
  isGraphCompareDateDisplayed: false,
  isGraphClearResultDisplayed: false,
  isGraphToggleIndicatorDisplayed: false,
  isGraphScrollLineIndicatorDisplayed: false,
  isGraphElectricityBillRateDisplayed: false,
  isGraphNavigatePeriodDisplayed: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_IS_REFRESH_DISPLAYED:
      return { ...state, isRefreshDisplayed: action.isRefreshDisplayed };
    case SET_IS_ADD_NEW_GROUP_DISPLAYED:
      return {
        ...state,
        isAddNewGroupDisplayed: action.isAddNewGroupDisplayed,
      };
    case SET_IS_REARRANGE_GROUP_DISPLAYED:
      return {
        ...state,
        isRearrangeGroupDisplayed: action.isRearrangeGroupDisplayed,
      };
    case SET_IS_REARRANGE_UNIT_DISPLAYED:
      return {
        ...state,
        isRearrangeUnitDisplayed: action.isRearrangeUnitDisplayed,
      };
    case SET_IS_EDIT_NAME_DISPLAYED:
      return { ...state, isEditNameDisplayed: action.isEditNameDisplayed };
    case SET_IS_DELETE_GROUP_DISPLAYED:
      return {
        ...state,
        isDeleteGroupDisplayed: action.isDeleteGroupDisplayed,
      };
    case SET_IS_GRAPH_NAVIGATE_DISPLAYED:
      return {
        ...state,
        isGraphNavigateDisplayed: action.isGraphNavigateDisplayed,
      };
    case SET_IS_GRAPH_COMPARE_DATE_DISPLAYED:
      return {
        ...state,
        isGraphCompareDateDisplayed: action.isGraphCompareDateDisplayed,
      };
    case SET_IS_GRAPH_CLEAR_RESULT_DISPLAYED:
      return {
        ...state,
        isGraphClearResultDisplayed: action.isGraphClearResultDisplayed,
      };
    case SET_IS_GRAPH_TOGGLE_INDICATOR_DISPLAYED:
      return {
        ...state,
        isGraphToggleIndicatorDisplayed: action.isGraphToggleIndicatorDisplayed,
      };
    case SET_IS_GRAPH_SCROLL_LINE_INDICATOR_DISPLAYED:
      return {
        ...state,
        isGraphScrollLineIndicatorDisplayed:
          action.isGraphScrollLineIndicatorDisplayed,
      };
    case SET_IS_GRAPH_ELECTRICITY_BILL_RATE_DISPLAYED:
      return {
        ...state,
        isGraphElectricityBillRateDisplayed:
          action.isGraphElectricityBillRateDisplayed,
      };
    case SET_IS_GRAPH_NAVIGATE_PERIOD_DISPLAYED:
      return {
        ...state,
        isGraphNavigatePeriodDisplayed: action.isGraphNavigatePeriodDisplayed,
      };
    default:
      return { ...state };
  }
};

export default reducer;
