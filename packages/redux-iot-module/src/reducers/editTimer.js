import produce from 'immer';
import {
  SET_EDIT_TIMER_LOADING,
  SET_EDIT_TIMER_PARAM,
  SET_EDIT_TIMER_STATUS,
  CLEAR_EDIT_TIMER_STATUS,
  CLEAR_EDIT_TIMER_CONFIG,
  SET_EDIT_TIMER_INIT_CONFIG,
  SET_EDIT_TIMER_CONFIG,
  SET_EDIT_TIMER_DAYS,
  REMOVE_EDIT_TIMER_ITEM,
  SET_EDIT_TIMER_NEW_CONFIG,
  ADD_EDIT_TIMER_NEW_TIMER,
  SET_ACHIEVE_TIMER,
} from '../actions/editTimer';

const initialState = {
  isLoading: false,
  error: '',
  success: '',
  canSubmit: false,
  isNew: false,
  thingName: '',
  days: [],
  initialTimerList: {},
  timerList: [],
  achieveTimerList: [],
};

const mapTimerInfo = timerInfo => ({
  timerIndex: timerInfo.timerIndex,
  hour: timerInfo.hour,
  minute: timerInfo.minute,
  switchValue: timerInfo.switchValue,
  mode: timerInfo.mode,
  temp: timerInfo.temp || 18,
  fanLevel: timerInfo.fanLevel || 'auto',
  silent: timerInfo.silent,
  isActive: timerInfo.isActive,
  user: timerInfo.user,
});

const mapDefaultEditTimer = (modes, timerIndex, user) => {
  const availableModes = Object.keys(modes).filter(
    modeKey => modes[modeKey] === 0
  );

  const defaultMode = availableModes[0] || 'cool';
  const curDT = new Date();
  const newTimer = {
    timerIndex,
    hour: curDT.getHours(),
    minute: curDT.getMinutes(),
    switchValue: 1,
    mode: defaultMode,
    silent: 0,
    fanLevel: 'auto',
    isActive: true,
    user,
  };

  if (defaultMode !== 'fan' && defaultMode !== 'dry') {
    newTimer.temp = 18;
  }

  return newTimer;
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_EDIT_TIMER_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_EDIT_TIMER_CONFIG: {
      const { timerList, thingName, dayIndex } = action.configParams;
      const days = Array(7).fill(false);
      days[dayIndex] = true;

      const editTimerList = [];

      timerList.forEach(timerInfo => {
        if (timerInfo.canEdit) {
          editTimerList.push(mapTimerInfo(timerInfo));
        }
      });

      return {
        ...state,
        thingName,
        isNew: false,
        days,
        timerList: editTimerList,
        canSubmit: false,
        achieveTimerList: [],
      };
    }
    case SET_EDIT_TIMER_STATUS:
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    case SET_EDIT_TIMER_INIT_CONFIG: {
      const { key, value } = action.configParams;

      return {
        ...state,
        initConfig: {
          ...state.initConfig,
          [key]: value,
        },
      };
    }
    case SET_EDIT_TIMER_NEW_CONFIG: {
      const {
        unit: { modes },
        thingName,
        dayIndex,
        user,
      } = action.configParams;
      const { timerList } = state;
      const newDays = Array(7).fill(false);
      newDays[dayIndex] = true;
      const timerIndex = timerList.length;

      const newTimerList = [mapDefaultEditTimer(modes, timerIndex, user)];

      return {
        ...state,
        isNew: true,
        thingName,
        timerList: newTimerList,
        days: newDays,
        achieveTimerList: [],
        initialTimerList: {},
        canSubmit: true,
      };
    }
    case ADD_EDIT_TIMER_NEW_TIMER: {
      const { modes, user } = action.configParams;

      const newTimerList = [...state.timerList];
      newTimerList.push(mapDefaultEditTimer(modes, newTimerList.length, user));

      return {
        ...state,
        timerList: newTimerList,
        canSubmit: newTimerList.length > 0,
      };
    }
    case SET_EDIT_TIMER_DAYS: {
      const { dayIndex } = action;
      const newDayValue = [...state.days];
      newDayValue[dayIndex] = !newDayValue[dayIndex];

      const canSubmit = newDayValue.findIndex(day => day) > -1;

      return {
        ...state,
        days: newDayValue,
        canSubmit,
      };
    }
    case SET_EDIT_TIMER_PARAM: {
      const { key, value, timerIndex } = action.configParams;
      const { timerList, initialTimerList, days } = state;
      let hasChanges = false;

      const curTimer = timerList[timerIndex];

      const newTimerList = produce(timerList, draftState => {
        const draftTimer = draftState[timerIndex];
        draftTimer[key] = value;

        if (key === 'mode') {
          if (curTimer.mode === 'cool') {
            draftTimer.cool = {
              silent: draftTimer.silent,
              fanLevel: draftTimer.fanLevel,
            };
          }
          if (value === 'dry') {
            draftTimer.fanLevel = 'auto';
          } else if (
            value === 'fan' &&
            (draftTimer.silent === 1 || draftTimer.fanLevel === 'auto')
          ) {
            draftTimer.silent = 0;
            draftTimer.fanLevel = 'level1';
          } else if (value === 'cool' && draftTimer.cool) {
            draftTimer.silent = draftTimer.cool.silent;
            draftTimer.fanLevel = draftTimer.cool.fanLevel;
          }
        }
      });

      const newInitialTimerList = produce(initialTimerList, draftState => {
        const initialTimer = draftState[timerIndex];

        if (!initialTimer) {
          hasChanges = true;
          draftState[timerIndex] = {
            ...timerList[timerIndex],
          };
        } else {
          hasChanges =
            Object.keys(initialTimerList).length > 1 ||
            Object.keys(initialTimer).some(
              timerKey =>
                newTimerList[timerIndex][timerKey] !== initialTimer[timerKey]
            );
        }
      });

      const canSubmit = hasChanges && days.findIndex(day => day) > -1;

      return {
        ...state,
        timerList: newTimerList,
        initialTimerList: newInitialTimerList,
        canSubmit,
      };
    }
    case SET_ACHIEVE_TIMER: {
      const { timerIndex } = action;
      const newAchieveTimerList = [timerIndex];

      return {
        ...state,
        achieveTimerList: newAchieveTimerList,
      };
    }
    case REMOVE_EDIT_TIMER_ITEM: {
      const { timerIndex } = action;
      const { achieveTimerList, isNew } = state;

      const newAchieveTimerList = [...achieveTimerList];
      newAchieveTimerList.push(timerIndex);

      const newTimerList = produce(state.timerList, draftState => {
        const deleteIndex = draftState.findIndex(
          curTimerInfo => curTimerInfo.timerIndex === timerIndex
        );

        draftState.splice(deleteIndex, 1);
      });
      return {
        ...state,
        timerList: newTimerList,
        // Only set the achieve timer list if not new
        ...(!isNew && { achieveTimerList: newAchieveTimerList }),
        // Zero timer in edit mode means delete all timer
        canSubmit: !isNew || newTimerList.length > 0,
      };
    }
    case CLEAR_EDIT_TIMER_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    case CLEAR_EDIT_TIMER_CONFIG:
      return initialState;
    default:
      return state;
  }
};

export default reducer;
