import {
  SET_UNIT_USER,
  SET_CONTROL_STATUS,
  SET_USER_CONTROL_LOADING,
  CLEAR_USER_CONTROL_STATUS,
} from '../actions/unitUserControl';

const initialState = {
  unitUser: {
    shareUnit: 0,
    overwriteSchedule: 0,
  },
  shareUnitLoading: false,
  overwriteScheduleLoading: false,
  usageLimitLoading: false,
  error: '',
  success: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_UNIT_USER:
      return {
        ...state,
        unitUser: action.user,
      };
    case SET_USER_CONTROL_LOADING:
      return {
        ...state,
        [`${action.payload.controlType}Loading`]: action.payload.isLoading,
      };
    case SET_CONTROL_STATUS:
      return {
        ...state,
        [action.payload.messageType]: action.payload.message,
      };
    case CLEAR_USER_CONTROL_STATUS:
      return {
        ...state,
        success: '',
        error: '',
      };
    default:
      return state;
  }
};

export default reducer;
