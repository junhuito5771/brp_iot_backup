export const CHECK_CAN_CREATE_TIMER = 'CHECK_CAN_CREATE_TIMER';
export const SUBMIT_TIMER_PARAMS = 'SUBMIT_TIMER_PARAMS';
export const SET_EDIT_TIMER_LOADING = 'SET_EDIT_TIMER_LOADING';
export const SET_EDIT_TIMER_PARAM = 'SET_EDIT_TIMER_PARAM';
export const SET_EDIT_TIMER_STATUS = 'SET_EDIT_TIMER_STATUS';
export const CLEAR_EDIT_TIMER_STATUS = 'CLEAR_EDIT_TIMER_STATUS';
export const SET_EDIT_TIMER_CONFIG = 'SET_EDIT_TIMER_CONFIG';
export const CLEAR_EDIT_TIMER_CONFIG = 'CLEAR_EDIT_TIMER_CONFIG';
export const SET_EDIT_TIMER_INIT_CONFIG = 'SET_EDIT_TIMER_INIT_CONFIG';
export const SET_EDIT_TIMER_DAYS = 'SET_EDIT_TIMER_DAYS';
export const REMOVE_EDIT_TIMER_ITEM = 'REMOVE_EDIT_TIMER_ITEM';
export const SET_EDIT_TIMER_NEW_CONFIG = 'SET_EDIT_TIMER_NEW_CONFIG';
export const ADD_EDIT_TIMER_NEW_TIMER = 'ADD_EDIT_TIMER_NEW_TIMER';
export const SET_ACHIEVE_TIMER = 'SET_ACHIEVE_TIMER';
export const SUBMIT_QUICK_TIMER = 'SUBMIT_QUICK_TIMER';

export const setEditTimerConfig = configParams => ({
  type: SET_EDIT_TIMER_CONFIG,
  configParams,
});

export const submitTimerParams = isDelete => ({
  type: SUBMIT_TIMER_PARAMS,
  isDelete,
});

export const checkCanCreateTimer = (thingName, day) => ({
  type: CHECK_CAN_CREATE_TIMER,
  thingName,
  day,
});

export const setEditTimerLoading = isLoading => ({
  type: SET_EDIT_TIMER_LOADING,
  isLoading,
});

export const setEditTimerDays = dayIndex => ({
  type: SET_EDIT_TIMER_DAYS,
  dayIndex,
});

export const setEditTimerParam = configParams => ({
  type: SET_EDIT_TIMER_PARAM,
  configParams,
});

export const setEditTimerStatus = payload => ({
  type: SET_EDIT_TIMER_STATUS,
  payload,
});

export const clearEditTimerStatus = () => ({
  type: CLEAR_EDIT_TIMER_STATUS,
});

export const clearEditTimerConfig = () => ({
  type: CLEAR_EDIT_TIMER_CONFIG,
});

export const setEditTimerInitParam = configParams => ({
  type: SET_EDIT_TIMER_INIT_CONFIG,
  configParams,
});

export const removeEditTimerItem = timerIndex => ({
  type: REMOVE_EDIT_TIMER_ITEM,
  timerIndex,
});

export const setEditTimerNewConfig = configParams => ({
  type: SET_EDIT_TIMER_NEW_CONFIG,
  configParams,
});

export const addEditTimerNewTimer = configParams => ({
  type: ADD_EDIT_TIMER_NEW_TIMER,
  configParams,
});

export const setAchieveTimer = timerIndex => ({
  type: SET_ACHIEVE_TIMER,
  timerIndex,
});

export const submitQuickTimer = quickTimerParams => ({
  type: SUBMIT_QUICK_TIMER,
  quickTimerParams,
});
