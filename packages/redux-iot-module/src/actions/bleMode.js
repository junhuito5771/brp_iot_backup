export const SET_BLE_UNITS = 'SET_BLE_UNITS';
export const SET_BLE_LOADING = 'SET_BLE_LOADING';
export const CLEAR_BLE_UNITS = 'CLEAR_BLE_UNITS';
export const PROCESS_BLE_UNITS = 'PROCESS_BLE_UNITS';
export const SET_BLE_STATUS = 'SET_BLE_STATUS';
export const CLEAR_BLE_STATUS = 'CLEAR_BLE_STATUS';

export const setBleUnits = bleUnits => ({
  type: SET_BLE_UNITS,
  bleUnits,
});

export const setLoading = isLoading => ({
  type: SET_BLE_LOADING,
  isLoading,
});

export const clearBleUnits = () => ({
  type: CLEAR_BLE_UNITS,
});

export const processBleUnits = unitHandler => ({
  type: PROCESS_BLE_UNITS,
  unitHandler,
});

export const setBleStatus = ({ messageType, message }) => ({
  type: SET_BLE_STATUS,
  messageType,
  message,
});

export const clearBleStatus = () => ({
  type: CLEAR_BLE_STATUS,
});
