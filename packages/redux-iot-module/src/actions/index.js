// import * as userAgreement from './userAgreement';
import * as units from './units';
import * as pairDevice from './pairDevice';
import * as deviceConfig from './deviceConfig';
import * as homepageIotShadow from './homepageIotShadow';
import * as remote from './remote';
import * as editUnits from './editUnits';
import * as bleMode from './bleMode';
// Maybe need to move to common redux
import * as settings from './settings';
import * as tutorialFlag from './tutorialFlag';
import * as unitUsers from './unitUsers';
import * as bindGuestDevice from './bindGuestDevice';
import * as unbindDevice from './unbindDevice';
import * as wlanMode from './wlanMode';
import * as unitUserControl from './unitUserControl';
import * as bleControl from './bleControl';
import * as updateFirmware from './updateFirmware';
import * as timer from './timer';
import * as editTimer from './editTimer';
import * as usage from './usage';
import * as scanSSID from './scanSSID';
import * as preferredUnits from './preferredUnits';
import * as plans from './plans';
import * as controlUsageLimit from './controlUsageLimit';

export {
  remote,
  units,
  editUnits,
  updateFirmware,
  controlUsageLimit,
  bindGuestDevice,
  pairDevice,
  deviceConfig,
  homepageIotShadow,
  unitUsers,
  unbindDevice,
  wlanMode,
  unitUserControl,
  timer,
  editTimer,
  usage,
  scanSSID,
  preferredUnits,
  plans,
  settings,
  tutorialFlag,
  bleMode,
  bleControl,
};
