export const SET_UNITS_LOADING = 'SET_UNITS_LOADING';
export const GET_ALL_UNITS = 'GET_ALL_UNITS';
export const SET_ALL_UNITS = 'SET_ALL_UNITS';
export const SET_GROUPS = 'SET_GROUPS';
export const SET_GROUP_UNITS = 'SET_GROUP_UNITS';
export const SET_UNITS_STATUS = 'SET_UNITS_STATUS';
export const CLEAR_UNITS_STATUS = 'CLEAR_UNITS_STATUS';
export const SET_UNIT = 'SET_UNIT';
export const SET_NOTIFIED_ERROR = 'SET_NOTIFIED_ERROR';
export const UPDATE_NOTIFIED_ERROR = 'UPDATE_NOTIFIED_ERROR';
export const SET_FIRMWARE = 'SET_FIRMWARE';
export const SET_UNIT_PREV_CONFIG = 'SET_UNIT_PREV_CONFIG';
export const REMOVE_PREV_CONFIG_PROPS = 'REMOVE_PREV_CONFIG_PROPS';
export const SET_UNIT_NAME = 'SET_UNIT_NAME';

export const updateNotifiedError = (thingName, value) => ({
  type: UPDATE_NOTIFIED_ERROR,
  thingName,
  value,
});

export const setNotifiedError = value => ({
  type: SET_NOTIFIED_ERROR,
  value,
});

export const setUnitsLoading = isLoading => ({
  type: SET_UNITS_LOADING,
  isLoading,
});

export const getAllUnits = fetchUnitsParams => ({
  type: GET_ALL_UNITS,
  fetchUnitsParams,
});

export const setAllUnits = (allUnits, lastUpdated) => ({
  type: SET_ALL_UNITS,
  allUnits,
  lastUpdated,
});

export const setGroups = groupInfo => ({
  type: SET_GROUPS,
  groupInfo,
});

export const setGroupUnits = groupUnits => ({
  type: SET_GROUP_UNITS,
  groupUnits,
});

export const clearUnitsStatus = () => ({
  type: CLEAR_UNITS_STATUS,
});

export const setUnit = unitProps => ({
  type: SET_UNIT,
  unitProps,
});

export const setUnitStatus = payload => ({
  type: SET_UNITS_STATUS,
  payload,
});

export const setFirmware = firmwareInfo => ({
  type: SET_FIRMWARE,
  firmwareInfo,
});

export const setUnitPrevConfig = prevConfig => ({
  type: SET_UNIT_PREV_CONFIG,
  prevConfig,
});

export const removeUnitPrevConfigProp = prevConfig => ({
  type: REMOVE_PREV_CONFIG_PROPS,
  prevConfig,
});

export const setUnitName = editParams => ({
  type: SET_UNIT_NAME,
  editParams,
});
