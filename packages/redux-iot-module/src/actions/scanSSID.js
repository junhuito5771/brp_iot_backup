export const SET_SCAN_SSID_LOADING = 'SET_SCAN_SSID_LOADING';
export const SET_NEARBY_SSID = 'SET_NEARBY_SSID';
export const SCAN_SSID = 'SCAN_SSID';
export const SET_SCAN_SSID_STATUS = 'SET_SCAN_SSID_STATUS';
export const CLEAR_SCAN_SSID_STATUS = 'CLEAR_SCAN_SSID_STATUS';
export const CANCEL_SCAN_SSID = 'CANCEL_SCAN_SSID';

export const setScanSSIDLoading = isLoading => ({
  type: SET_SCAN_SSID_LOADING,
  isLoading,
});

export const setNearbySSID = ssidList => ({
  type: SET_NEARBY_SSID,
  ssidList,
});

export const scanSSID = () => ({
  type: SCAN_SSID,
});

export const setScanSSIDStatus = payload => ({
  type: SET_SCAN_SSID_STATUS,
  payload,
});

export const clearScanSSIDStatus = () => ({
  type: CLEAR_SCAN_SSID_STATUS,
});

export const cancelScanSSID = () => ({
  type: CANCEL_SCAN_SSID,
});
