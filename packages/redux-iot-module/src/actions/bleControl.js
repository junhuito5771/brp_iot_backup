export const BLECONTROL_SET_SWITCH = 'BLECONTROL_SET_SWITCH';
export const BLECONTROL_SET_TEMP = 'BLECONTROL_SET_TEMP';
export const BLECONTROL_CONNECT = 'BLECONTROL_CONNECT';
export const BLECONTROL_GET_STATUS = 'BLECONTROL_GET_STATUS';
export const BLECONTROL_DISCONNECT = 'BLECONTROL_DISCONNECT';
export const BLECONTROL_SET_CONNECTED_UNIT = 'BLECONTROL_SET_CONNECTED_UNIT';
export const BLECONTROL_CLEAR_STATUS = 'BLECONTROL_CLEAR_STATUS';
export const BLECONTROL_SET_STATUS = 'BLECONTROL_SET_STATUS';
export const BLECONTROL_SET_LOADING = 'BLECONTROL_SET_LOADING';
export const BLECONTROL_SET_RESPONSE = 'BLECONTROL_SET_RESPONSE';
export const BLECONTROL_SET_PAIR_LOADING = 'BLECONTROL_SET_PAIR_LOADING';

export const setSwitch = switchValue => ({
  type: BLECONTROL_SET_SWITCH,
  switchValue,
});

export const setTemp = tempValue => ({
  type: BLECONTROL_SET_TEMP,
  tempValue,
});

export const connectBLEUnit = connectParams => ({
  type: BLECONTROL_CONNECT,
  connectParams,
});

export const getStatus = bleParams => ({
  type: BLECONTROL_GET_STATUS,
  bleParams,
});

export const setConnectedUnit = peripheralProps => ({
  type: BLECONTROL_SET_CONNECTED_UNIT,
  peripheralProps,
});

export const setPairLoading = pairLoading => ({
  type: BLECONTROL_SET_PAIR_LOADING,
  pairLoading,
});

export const setBLEControlStatus = bleControlStatus => ({
  type: BLECONTROL_SET_STATUS,
  bleControlStatus,
});

export const clearBLEControlStatus = () => ({
  type: BLECONTROL_CLEAR_STATUS,
});

export const setLoading = isLoading => ({
  type: BLECONTROL_SET_LOADING,
  isLoading,
});

export const setResponse = response => ({
  type: BLECONTROL_CLEAR_STATUS,
  response,
});

export const disconnectBLE = () => ({
  type: BLECONTROL_DISCONNECT,
});
