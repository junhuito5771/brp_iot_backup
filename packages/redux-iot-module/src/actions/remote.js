export const SET_REMOTE_UNIT = 'SET_REMOTE_UNIT';
export const SET_FAN_SPEED = 'SET_FAN_SPEED';
export const SET_SLEEP = 'SET_SLEEP';
export const SET_SILENT = 'SET_SILENT';
export const SET_TURBO = 'SET_TURBO';
export const SET_SWING = 'SET_SWING';
export const SET_SWING_LR = 'SET_SWING_LR';
export const SET_SWING_3D = 'SET_SWING_3D';
export const SET_ECOPLUS = 'SET_ECOPLUS';
export const SET_BREEZE = 'SET_BREEZE';
export const SET_STREAMER = 'SET_STREAMER';
export const SET_SMART_DRIFT = 'SET_SMART_DRIFT';
export const SET_LED = 'SET_LED';
export const SET_CK_SWING = 'SET_CK_SWING';
export const SET_REMOTE_STATUS = 'SET_REMOTE_STATUS';
export const CLEAR_REMOTE_STATUS = 'CLEAR_REMOTES_STATUS';
export const SET_MODE = 'SET_MODE';
export const MOUNT_UNIT = 'MOUNT_UNIT';
export const UNMOUNT_UNIT = 'UNMOUNT_UNIT';
export const SET_AC_SWITCH = 'SET_AC_SWITCH';
export const SET_REMOTE_LOADING = 'SET_REMOTE_LOADING';
export const SET_AC_TEMP = 'SET_AC_TEMP';
export const SET_LAST_UPDATE_TIME = 'SET_LAST_UPDATE_TIME';
export const REFRESH_UNIT = 'REFRESH_UNIT';
export const SET_SENSE = 'SET_SENSE';
export const SET_ERROR_HISTORY = 'SET_ERROR_HISTORY';
export const FETCH_ERROR_HISTORY = 'FETCH_ERROR_HISTORY';
export const CLEAR_ERROR_HISTORY = 'CLEAR_ERROR_HISTORY';
export const SET_RECONNECT = 'SET_RECONNECT';
export const CHECK_UNIT_AUTH = 'CHECK_UNIT_AUTH';
export const CANCEL_CHECK_UNIT_AUTH = 'CANCEL_CHECK_UNIT_AUTH';
export const SET_EXPECTED_TEMP = 'SET_EXPECTED_TEMPT';
export const CLEAR_DESIRE_TEMP = 'CLEAR_DESIRE_TEMP';
export const SET_CONNECTION_STATUS = 'SET_CONNECTION_STATUS';
export const RESTART_START_TASK = 'RESTART_START_TASK';
export const SET_IOT_POLICY = 'SET_IOT_POLICY';

export const clearErrorHistory = () => ({
  type: CLEAR_ERROR_HISTORY,
});

export const setRemoteUnit = ({ thingName, connectionType, connectionID }) => ({
  type: SET_REMOTE_UNIT,
  thingName,
  connectionType,
  connectionID,
});

export const setErrorHistory = value => ({
  type: SET_ERROR_HISTORY,
  value,
});

export const fetchErrorHistory = value => ({
  type: FETCH_ERROR_HISTORY,
  value,
});
export const setExpectedTemp = value => ({
  type: SET_EXPECTED_TEMP,
  value,
});
export const clearDesireTemp = () => ({
  type: CLEAR_DESIRE_TEMP,
});
export const setFanSpeed = controlValue => ({
  type: SET_FAN_SPEED,
  controlValue,
});

export const setSilent = controlValue => ({
  type: SET_SILENT,
  controlValue,
});

export const setSleep = controlValue => ({
  type: SET_SLEEP,
  controlValue,
});

export const setTurbo = controlValue => ({
  type: SET_TURBO,
  controlValue,
});

export const setSense = controlValue => ({
  type: SET_SENSE,
  controlValue,
});

export const setSwing = controlValue => ({
  type: SET_SWING,
  controlValue,
});

export const setSwingLR = controlValue => ({
  type: SET_SWING_LR,
  controlValue,
});

export const setSwing3D = controlValue => ({
  type: SET_SWING_3D,
  controlValue,
});

export const setAcTemp = controlValue => ({
  type: SET_AC_TEMP,
  controlValue,
});

export const setEcoplus = (controlValue, canManual = false) => ({
  type: SET_ECOPLUS,
  controlValue,
  canManual,
});

export const setMode = controlValue => ({
  type: SET_MODE,
  controlValue,
});

export const setBreeze = controlValue => ({
  type: SET_BREEZE,
  controlValue,
});

export const setSmartDrift = controlValue => ({
  type: SET_SMART_DRIFT,
  controlValue,
});

export const setStreamer = controlValue => ({
  type: SET_STREAMER,
  controlValue,
});

export const setLED = controlValue => ({
  type: SET_LED,
  controlValue,
});

export const setCKSwing = controlValue => ({
  type: SET_CK_SWING,
  controlValue,
});

export const setAcSwitch = (thingName, switchType) => ({
  type: SET_AC_SWITCH,
  thingName,
  switchType,
});

export const setRemoteStatus = status => ({
  type: SET_REMOTE_STATUS,
  status,
});

export const clearRemoteStatus = () => ({
  type: CLEAR_REMOTE_STATUS,
});

export const onMountUnit = remoteParams => ({
  type: MOUNT_UNIT,
  remoteParams,
});

export const onUnmountUnit = remoteParams => ({
  type: UNMOUNT_UNIT,
  remoteParams,
});

export const setLoading = value => ({
  type: SET_REMOTE_LOADING,
  value,
});

export const setLastUpdateTime = value => ({
  type: SET_LAST_UPDATE_TIME,
  value,
});

export const refreshUnit = value => ({
  type: REFRESH_UNIT,
  value,
});

export const setReconnect = isReconnect => ({
  type: SET_RECONNECT,
  isReconnect,
});

export const checkUnitAuth = unitAuthParams => ({
  type: CHECK_UNIT_AUTH,
  unitAuthParams,
});

export const cancelCheckUnitAuth = () => ({
  type: CANCEL_CHECK_UNIT_AUTH,
});

export const setConnectionStatus = status => ({
  type: SET_CONNECTION_STATUS,
  status,
});

export const restartStartTask = () => ({
  type: RESTART_START_TASK,
});

export const setIoTPolicy = status => ({
  type: SET_IOT_POLICY,
  status,
});
