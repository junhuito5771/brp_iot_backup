export const SET_USER_CONTROL = 'SET_USER_CONTROL';
export const SET_UNIT_USER = 'SET_UNIT_USER';
export const SET_CONTROL_STATUS = 'SET_CONTROL_STATUS';
export const SET_USER_CONTROL_LOADING = 'SET_USER_CONTROL_LOADING';
export const CLEAR_USER_CONTROL_STATUS = 'CLEAR_USER_CONTROL_STATUS';

export const setUserControl = controlType => ({
  type: SET_USER_CONTROL,
  controlType,
});

export const setUnitUser = user => ({
  type: SET_UNIT_USER,
  user,
});

export const clearUserControlStatus = () => ({
  type: CLEAR_USER_CONTROL_STATUS,
});
