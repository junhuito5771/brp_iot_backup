export const SUBMIT_EDIT_PARAMS = 'SUBMIT_EDIT_PARAMS';
export const SET_IS_LOADING = 'SET_IS_LOADING';
export const SET_EDIT_STATUS = 'SET_EDIT_STATUS';
export const CLEAR_EDIT_STATUS = 'CLEAR_EDIT_STATUS';
export const CLEAR_GROUP_ORDER = 'CLEAR_GROUP_ORDER';
export const SET_EDIT_STATE = 'SET_EDIT_STATE';
export const INIT_EDIT_UNIT = 'INIT_EDIT_UNIT';
export const SET_ITEM_NAME = 'SET_ITEM_NAME';
export const SUBMIT_EDIT_UNIT_NAME = 'SUBMIT_EDIT_UNIT_NAME';
export const ADD_EDIT_ITEM = 'ADD_EDIT_ITEM';
export const REMOVE_EDIT_GROUP = 'REMOVE_EDIT_GROUP';
export const SET_ITEM_ORDER = 'SET_ITEM_ORDER';
export const SET_ITEM_CHILD_ORDER = 'SET_ITEM_CHILD_ORDER';
export const SET_ITEM_DIFF_GROUP = 'SET_ITEM_DIFF_GROUP';
export const SET_ITEM_ICON = 'SET_ITEM_ICON';

export const submitEditParams = () => ({
  type: SUBMIT_EDIT_PARAMS,
});

export const setEditStatus = payload => ({
  type: SET_EDIT_STATUS,
  payload,
});

export const setIsLoading = isLoading => ({
  type: SET_IS_LOADING,
  isLoading,
});

export const clearEditStatus = () => ({
  type: CLEAR_EDIT_STATUS,
});

export const setEditState = state => ({
  type: SET_EDIT_STATE,
  state,
});

export const initEditUnit = initParams => ({
  type: INIT_EDIT_UNIT,
  initParams,
});

export const submitEditUnitName = editUnitNameParams => ({
  type: SUBMIT_EDIT_UNIT_NAME,
  editUnitNameParams,
});

export const setItemName = editParams => ({
  type: SET_ITEM_NAME,
  editParams,
});

export const addEditItem = name => ({
  type: ADD_EDIT_ITEM,
  name,
});

export const removeGroup = editParams => ({
  type: REMOVE_EDIT_GROUP,
  editParams,
});

export const setItemOrder = orders => ({
  type: SET_ITEM_ORDER,
  orders,
});

export const setItemChildOrder = (key, orders) => ({
  type: SET_ITEM_CHILD_ORDER,
  key,
  orders,
});

export const setItemToDiffGroup = assignParams => ({
  type: SET_ITEM_DIFF_GROUP,
  // Contains FROMGROUP, UNITKEY, TARGETGROUP
  assignParams,
});

export const setItemIcon = (key, logo) => ({
  type: SET_ITEM_ICON,
  key,
  logo,
});
