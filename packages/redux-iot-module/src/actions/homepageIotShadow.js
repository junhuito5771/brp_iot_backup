export const MQTT_INIT = 'MQTT_INIT';
export const MQTT_CONNECT = 'MQTT_CONNECT';
export const SET_MQTT_STATUS = 'SET_MQTT_STATUS';
export const HOMEPAGE_SUBSCRIBE = 'HOMEPAGE_SUBSCRIBE';
export const HOMEPAGE_PUBLISH = 'HOMEPAGE_PUBLISH';
export const HOMEPAGE_UNSUBSCRIBE = 'HOMEPAGE_UNSUBSCRIBE';
export const HOMEPAGE_UNSUBSCRIBE_ALL = 'HOMEPAGE_UNSUBSCRIBE_ALL';
export const GET_HOMEPAGE_IOT_STATUS = 'GET_HOMEPAGE_IOT_STATUS';
export const SWITCH_AC = 'SWITCH_AC';
export const SWITCH_GROUP_AC = 'SWITCH_GROUP_AC';
export const IS_LOADING = 'IS_LOADING';
export const ADD_WAITING_UNIT = 'ADD_WAITING_UNIT';
export const REMOVE_WAITING_UNIT = 'REMOVE_WAITING_UNIT';
export const MQTT_CONNECT_SUCCESS = 'MQTT_CONNECT_SUCCESS';
export const MQTT_INIT_CONNECT = 'MQTT_INIT_CONNECT';
export const MQTT_CLEAR_STATUS = 'MQTT_CLEAR_STATUS';
export const FETCH_UNIT_PUBLISH_SHADOW = 'FETCH_UNIT_PUBLISH_SHADOW';
export const UNIT_LOADING = 'UNIT_LOADING';
export const SET_TOAST_MESSAGE = 'SET_TOAST_MESSAGE';

export const mqttInit = config => ({
  type: MQTT_INIT,
  config,
});

export const mqttConnect = store => ({
  type: MQTT_CONNECT,
  store,
});

export const mqttInitConnect = mqttConfig => ({
  type: MQTT_INIT_CONNECT,
  mqttConfig,
});

export const setMqttStatus = status => ({
  type: SET_MQTT_STATUS,
  status,
});

export const clearMqttStatus = () => ({
  type: MQTT_CLEAR_STATUS,
});

export const homepageSubscribe = () => ({
  type: HOMEPAGE_SUBSCRIBE,
});

export const homepagePublish = (topic, msg) => ({
  type: HOMEPAGE_PUBLISH,
  topic,
  msg,
});

export const homepageUnsubscribe = topic => ({
  type: HOMEPAGE_UNSUBSCRIBE,
  topic,
});

export const homepageUnsubscribeAll = () => ({
  type: HOMEPAGE_UNSUBSCRIBE_ALL,
});

export const getHomepageIotStatus = () => ({
  type: GET_HOMEPAGE_IOT_STATUS,
});

export const switchAc = (groupIndex, thingName, switchType) => ({
  type: SWITCH_AC,
  groupIndex,
  thingName,
  switchType,
});

export const switchGroupAC = (groupIndex, switchType) => ({
  type: SWITCH_GROUP_AC,
  groupIndex,
  switchType,
});

export const isLoading = loading => ({
  type: IS_LOADING,
  loading,
});

export const unitLoading = unitLoadingList => ({
  type: UNIT_LOADING,
  unitLoadingList,
});
export const addWaitingUnit = unit => ({
  type: ADD_WAITING_UNIT,
  unit,
});

export const removeWaitingUnit = unit => ({
  type: REMOVE_WAITING_UNIT,
  unit,
});

export const fetchUnitPublishShadow = () => ({
  type: FETCH_UNIT_PUBLISH_SHADOW,
});

export const setToastMsg = msg => ({
  type: SET_TOAST_MESSAGE,
  msg,
});
