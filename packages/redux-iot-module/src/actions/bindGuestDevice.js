export const SET_BIND_GUEST_LOADING = 'SET_BIND_GUEST_LOADING';
export const SUBMIT_BIND_GUEST_DEVICE = 'SUBMIT_BIND_GUEST_DEVICE';
export const SET_BIND_GUEST_STATUS = 'SET_BIND_GUEST_STATUS';
export const CLEAR_BIND_GUEST_STATUS = 'CLEAR_BIND_GUEST_STATUS';

export const setBindGuestLoading = isLoading => ({
  type: SET_BIND_GUEST_LOADING,
  isLoading,
});

export const submitBindGuestDevice = (bindGuestDeviceParams, version) => ({
  type: SUBMIT_BIND_GUEST_DEVICE,
  bindGuestDeviceParams,
  version,
});

export const setBindGuestStatus = bindGuestDeviceStatus => ({
  type: SET_BIND_GUEST_STATUS,
  bindGuestDeviceStatus,
});

export const clearBindGuestStatus = () => ({
  type: CLEAR_BIND_GUEST_STATUS,
});
