export const SET_UNIT_USERS_LOADING = 'SET_UNIT_USERS_LOADING';
export const FETCH_UNIT_USERS_LIST = 'FETCH_UNIT_USERS_LIST';
export const SET_UNIT_USERS_LIST = 'SET_UNIT_USERS_LIST';
export const CLEAR_UNIT_USERS_STATUS = 'CLEAR_UNIT_USERS_STATUS';
export const SET_UNIT_USERS_STATUS = 'SET_UNIT_USERS_STATUS';
export const SET_UNIT_USERS_ID_LIST = 'SET_UNIT_USERS_ID_LIST';
export const CLEAR_UNIT_USERS = 'CLEAR_UNIT_USERS';
export const SET_CONTROL_LIMIT = 'SET_CONTROL_LIMIT';
export const SET_GUEST_NUMBER = 'SET_GUEST_NUMBER';

export const setUnitUsersLoading = isLoading => ({
  type: SET_UNIT_USERS_LOADING,
  isLoading,
});

export const fetchUnitUsers = unitUsersParams => ({
  type: FETCH_UNIT_USERS_LIST,
  unitUsersParams,
});

export const setUnitUsers = unitUsersParams => ({
  type: SET_UNIT_USERS_LIST,
  unitUsersParams,
});

export const setUnitUsersIDs = unitUsersIds => ({
  type: SET_UNIT_USERS_ID_LIST,
  unitUsersIds,
});

export const setUnitUsersStatus = unitUsersParams => ({
  type: SET_UNIT_USERS_STATUS,
  unitUsersParams,
});

export const clearUnitUsersStatus = () => ({
  type: CLEAR_UNIT_USERS_STATUS,
});

export const clearUnitUsers = () => ({
  type: CLEAR_UNIT_USERS,
});

export const setControlLimit = value => ({
  type: SET_CONTROL_LIMIT,
  value,
});

export const setGuestNumber = value => ({
  type: SET_GUEST_NUMBER,
  value,
});
