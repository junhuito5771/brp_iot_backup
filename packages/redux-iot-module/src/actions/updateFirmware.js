export const SUBMIT_FIRMWARE_UPGRADE = 'SUBMIT_FIRMWARE_UPGRADE';
export const SET_FIRMWARE_LOADING = 'SET_FIRMWARE_LOADING';
export const SET_UPDATE_STATUS = 'SET_UPDATE_STATUS';
export const CLEAR_UPDATE_STATUS = 'CLEAR_UPDATE_STATUS';
export const SET_UPLOAD_COMPLETED = 'SET_UPLOAD_COMPLETED';
export const CANCEL_FIRMWARE_UPGRADE = 'CANCEL_FIRMWARE_UPGRADE';
export const UNIT_DISCONNECT = 'UNIT_DISCONNECT';

export const submitFirmwareUpgrade = firmwareUpgradeParams => ({
  type: SUBMIT_FIRMWARE_UPGRADE,
  firmwareUpgradeParams,
});

export const setUpdateStatus = status => ({
  type: SET_UPDATE_STATUS,
  status,
});

export const setLoading = ({ isLoading, loadingText }) => ({
  type: SET_FIRMWARE_LOADING,
  loadingParams: {
    isLoading,
    loadingText,
  },
});

export const clearUpdateStatus = () => ({
  type: CLEAR_UPDATE_STATUS,
});

export const setUploadCompleted = () => ({
  type: SET_UPLOAD_COMPLETED,
});

export const cancelFirmwareUpgrade = () => ({
  type: CANCEL_FIRMWARE_UPGRADE,
});

export const unitDisconnect = () => ({
  type: UNIT_DISCONNECT,
});
