export const FETCH_TIMER = 'FETCH_TIMER';
export const SET_FETCH_TIMER_STATUS = 'SET_FETCH_TIMER_STATUS';
export const CLEAR_FETCH_TIMER_STATUS = 'CLEAR_FETCH_TIMER_STATUS';
export const SET_FETCH_TIMER_LOADING = 'SET_FETCH_TIMER_LOADING';
export const SET_TIMER = 'SET_TIMER';
export const CLEAR_TIMER = 'CLEAR_TIMER';
export const SET_TOGGLE_TIMER_LOADING = 'SET_TOGGLE_TIMER_LOADING';
export const REMOVE_TOGGLE_TIMER_LOADING = 'REMOVE_TOGGLE_TIMER_LOADING';
export const SET_TOGGLE_TIMER = 'SET_TOGGLE_TIMER';
export const SUBMIT_TOGGLE_TIMER = 'SUBMIT_TOGGLE_TIMER';
export const SET_TIMER_PAIRS = 'SET_TIMER_PAIRS';
export const SET_UNIT_TIMERS = 'SET_UNIT_TIMERS';
export const SET_QUICK_TIMER = 'SET_QUICK_TIMER';
export const REMOVE_QUICK_TIMER = 'REMOVE_QUICK_TIMER';
export const SET_QUICK_TIMER_LOADING = 'SET_QUICK_TIMER_LOADING';
export const SET_QUICK_TIMER_ERROR = 'SET_QUICK_TIMER_ERROR';
export const CLEAR_QUICK_TIMER_ERROR = 'CLEAR_QUICK_TIMER_ERROR';
export const ADD_QUICK_TIMER = 'ADD_QUICK_TIMER';

export const fetchTimer = () => ({
  type: FETCH_TIMER,
});

export const setFetchTimerLoading = isLoading => ({
  type: SET_FETCH_TIMER_LOADING,
  isLoading,
});

export const setFetchTimerStatus = payload => ({
  type: SET_FETCH_TIMER_STATUS,
  payload,
});

export const clearFetchTimerStatus = () => ({
  type: CLEAR_FETCH_TIMER_STATUS,
});

export const setTimer = timerData => ({
  type: SET_TIMER,
  timerData,
});

export const removeWeeklyTimer = () => ({
  type: CLEAR_TIMER,
});

export const setToggleTimerLoading = toggleLoadingParams => ({
  type: SET_TOGGLE_TIMER_LOADING,
  toggleLoadingParams,
});

export const removeToggleTimerLoading = toggleLoadingParams => ({
  type: REMOVE_TOGGLE_TIMER_LOADING,
  toggleLoadingParams,
});

export const setTimerToggle = toggleParams => ({
  type: SET_TOGGLE_TIMER,
  toggleParams,
});

export const submitToggleTimer = (thingName, timerIndex) => ({
  type: SUBMIT_TOGGLE_TIMER,
  thingName,
  timerIndex,
});

export const setTimerPairs = timerPairs => ({
  type: SET_TIMER_PAIRS,
  timerPairs,
});

export const setUnitTimers = unitTimersParams => ({
  type: SET_UNIT_TIMERS,
  unitTimersParams,
});

export const setQuickTimerLoading = isLoading => ({
  type: SET_QUICK_TIMER_LOADING,
  isLoading,
});

export const setQuickTimer = quickTimerList => ({
  type: SET_QUICK_TIMER,
  quickTimerList,
});

export const addQuickTimer = quickTimer => ({
  type: ADD_QUICK_TIMER,
  quickTimer,
});

export const removeQuickTimer = thingName => ({
  type: REMOVE_QUICK_TIMER,
  thingName,
});

export const setQuickTimerError = errMsg => ({
  type: SET_QUICK_TIMER_ERROR,
  errMsg,
});

export const clearQuickTimerError = () => ({
  type: CLEAR_QUICK_TIMER_ERROR,
});
