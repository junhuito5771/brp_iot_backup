export const SUBMIT_UNBIND_DEVICE = 'SUBMIT_UNBIND_DEVICE';
export const SET_UNBIND_DEVICE_LOADING = 'SET_UNBIND_DEVICE_LOADING';
export const SET_UNBIND_DEVICE_STATUS = 'SET_UNBIND_DEVICE_STATUS';
export const CLEAR_UNBIND_DEVICE_STATUS = 'CLEAR_UNBIND_DEVICE_STATUS';
export const SUBMIT_REMOVE_GUESTS = 'SUBMIT_REMOVE_GUESTS';
export const SET_UNBIND_GUEST_LOADING = 'SET_UNBIND_GUEST_LOADING';

export const submitUnbindDevice = unbindDeviceParams => ({
  type: SUBMIT_UNBIND_DEVICE,
  unbindDeviceParams,
});

export const setUnbindDeviceLoading = isLoading => ({
  type: SET_UNBIND_DEVICE_LOADING,
  isLoading,
});

export const setUnbindDeviceStatus = unbindDeviceStatus => ({
  type: SET_UNBIND_DEVICE_STATUS,
  unbindDeviceStatus,
});

export const clearUnbindDeviceStatus = () => ({
  type: CLEAR_UNBIND_DEVICE_STATUS,
});

export const submitRemoveGuests = removeGuestsParams => ({
  type: SUBMIT_REMOVE_GUESTS,
  removeGuestsParams,
});

export const setUnbindGuestLoading = isLoading => ({
  type: SET_UNBIND_GUEST_LOADING,
  isLoading,
});
