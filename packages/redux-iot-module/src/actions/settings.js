export const SET_VALIDITY_PERIOD = 'SET_VALIDITY_PERIOD';
export const SET_SAGA_ERROR = 'SET_SAGA_ERROR';
export const RESTART_APP = 'RESTART_APP';
export const SET_FIRMWARE_VERSION = 'SET_FIRMWARE_VERSION';
export const SET_REMOTE_INSTRUCT = 'SET_REMOTE_INSTRUCT';

export const setValidityPeriod = qrValidityPeriod => ({
  type: SET_VALIDITY_PERIOD,
  qrValidityPeriod,
});

export const setSagaError = error => ({
  type: SET_SAGA_ERROR,
  error,
});

export const setFirmwareVersion = firmwareVersion => ({
  type: SET_FIRMWARE_VERSION,
  firmwareVersion,
});

export const setRemoteInstruct = value => ({
  type: SET_REMOTE_INSTRUCT,
  value,
});
