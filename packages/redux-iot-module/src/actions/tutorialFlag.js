// Home Tutorial
export const SET_IS_REFRESH_DISPLAYED = 'SET_IS_REFRESH_DISPLAYED';
export const SET_IS_ADD_NEW_GROUP_DISPLAYED = 'SET_IS_ADD_NEW_GROUP_DISPLAYED';
export const SET_IS_REARRANGE_GROUP_DISPLAYED =
  'SET_IS_REARRANGE_GROUP_DISPLAYED';
export const SET_IS_REARRANGE_UNIT_DISPLAYED =
  'SET_IS_REARRANGE_UNIT_DISPLAYED';
export const SET_IS_EDIT_NAME_DISPLAYED = 'SET_IS_EDIT_NAME_DISPLAYED';
export const SET_IS_DELETE_GROUP_DISPLAYED = 'SET_IS_DELETE_GROUP_DISPLAYED';
// Energy Consumption Tutorial
export const SET_IS_GRAPH_NAVIGATE_DISPLAYED =
  'SET_IS_GRAPH_NAVIGATE_DISPLAYED';
// Graph Tutorial
export const SET_IS_GRAPH_COMPARE_DATE_DISPLAYED =
  'SET_IS_GRAPH_COMPARE_DATE_DISPLAYED';
export const SET_IS_GRAPH_CLEAR_RESULT_DISPLAYED =
  'SET_IS_GRAPH_CLEAR_RESULT_DISPLAYED';
export const SET_IS_GRAPH_TOGGLE_INDICATOR_DISPLAYED =
  'SET_IS_GRAPH_TOGGLE_INDICATOR_DISPLAYED';
export const SET_IS_GRAPH_SCROLL_LINE_INDICATOR_DISPLAYED =
  'SET_IS_GRAPH_SCROLL_LINE_INDICATOR_DISPLAYED';
export const SET_IS_GRAPH_ELECTRICITY_BILL_RATE_DISPLAYED =
  'SET_IS_GRAPH_ELECTRICITY_BILL_RATE_DISPLAYED';
export const SET_IS_GRAPH_NAVIGATE_PERIOD_DISPLAYED =
  'SET_IS_GRAPH_NAVIGATE_PERIOD_DISPLAYED';

export const setIsRefreshDisplayed = isRefreshDisplayed => ({
  type: SET_IS_REFRESH_DISPLAYED,
  isRefreshDisplayed,
});

export const setIsAddNewGroupDisplayed = isAddNewGroupDisplayed => ({
  type: SET_IS_ADD_NEW_GROUP_DISPLAYED,
  isAddNewGroupDisplayed,
});

export const setIsRearrangeGroupDisplayed = isRearrangeGroupDisplayed => ({
  type: SET_IS_REARRANGE_GROUP_DISPLAYED,
  isRearrangeGroupDisplayed,
});

export const setIsRearrangeUnitDisplayed = isRearrangeUnitDisplayed => ({
  type: SET_IS_REARRANGE_UNIT_DISPLAYED,
  isRearrangeUnitDisplayed,
});

export const setIsEditNameDisplayed = isEditNameDisplayed => ({
  type: SET_IS_EDIT_NAME_DISPLAYED,
  isEditNameDisplayed,
});

export const setIsDeleteGroupDisplayed = isDeleteGroupDisplayed => ({
  type: SET_IS_DELETE_GROUP_DISPLAYED,
  isDeleteGroupDisplayed,
});

export const setIsGraphNavigateDisplayed = isGraphNavigateDisplayed => ({
  type: SET_IS_GRAPH_NAVIGATE_DISPLAYED,
  isGraphNavigateDisplayed,
});

export const setIsGraphCompareDateDisplayed = isGraphCompareDateDisplayed => ({
  type: SET_IS_GRAPH_COMPARE_DATE_DISPLAYED,
  isGraphCompareDateDisplayed,
});

export const setIsGraphClearResultDisplayed = isGraphClearResultDisplayed => ({
  type: SET_IS_GRAPH_CLEAR_RESULT_DISPLAYED,
  isGraphClearResultDisplayed,
});

export const setIsGraphToggleIndicatorDisplayed = isGraphToggleIndicatorDisplayed => ({
  type: SET_IS_GRAPH_TOGGLE_INDICATOR_DISPLAYED,
  isGraphToggleIndicatorDisplayed,
});

export const setIsGraphScrollLineIndicatorDisplayed = isGraphScrollLineIndicatorDisplayed => ({
  type: SET_IS_GRAPH_SCROLL_LINE_INDICATOR_DISPLAYED,
  isGraphScrollLineIndicatorDisplayed,
});

export const setIsGraphElectricityBillRateDisplayed = isGraphElectricityBillRateDisplayed => ({
  type: SET_IS_GRAPH_ELECTRICITY_BILL_RATE_DISPLAYED,
  isGraphElectricityBillRateDisplayed,
});

export const setIsGraphNavigatePeriodDisplayed = isGraphNavigatePeriodDisplayed => ({
  type: SET_IS_GRAPH_NAVIGATE_PERIOD_DISPLAYED,
  isGraphNavigatePeriodDisplayed,
});
