export const WLAN_SET_LOADING = 'WLAN_SET_LOADING';
export const WLAN_SET_UNITS = 'WLAN_SET_UNITS';
export const WLAN_DISCOVER_UNITS = 'WLAN_DISCOVER_UNITS';
export const WLAN_SET_STATUS = 'WLAN_SET_STATUS';
export const WLAN_CLEAR_STATUS = 'WLAN_CLEAR_STATUS';
export const WLAN_PAIR = 'WLAN_PAIR';
export const WLAN_SET_PAIR_STATUS = 'WLAN_SET_PAIR_STATUS';
export const WLAN_CLEAR_PAIR_STATUS = 'WLAN_CLEAR_PAIR_STATUS';
export const WLAN_SET_PAIR_LOADING = 'WLAN_SET_PAIR_LOADING';
export const WLAN_DISCONNECT = 'WLAN_DISCONNECT';

export const setLoading = isLoading => ({
  type: WLAN_SET_LOADING,
  isLoading,
});

export const setUnits = units => ({
  type: WLAN_SET_UNITS,
  units,
});

export const discoverUnits = discoverWLANParams => ({
  type: WLAN_DISCOVER_UNITS,
  discoverWLANParams,
});

export const setStatus = status => ({
  type: WLAN_SET_STATUS,
  status,
});

export const clearStatus = () => ({
  type: WLAN_CLEAR_STATUS,
});

export const pairUnit = unitProps => ({
  type: WLAN_PAIR,
  unitProps,
});

export const setPairLoading = isLoading => ({
  type: WLAN_SET_PAIR_LOADING,
  isLoading,
});

export const wlanDisconnect = () => ({
  type: WLAN_DISCONNECT,
});
