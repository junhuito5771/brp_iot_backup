export const SET_LOADING = 'SET_LOADING';
export const FETCH_USAGE_DATA = 'FETCH_USAGE_DATA';
export const SET_USAGE_DATA = 'SET_USAGE_DATA';
export const SET_SELECTED_PERIOD = 'SET_SELECTED_PERIOD';
export const SET_TOGGLE_COMPARE = 'SET_TOGGLE_COMPARE';
export const CLEAR_USAGE_STATUS = 'CLEAR_USAGE_STATUS';
export const SET_USAGE_STATUS = 'SET_USAGE_STATUS';
export const SET_USAGE_COMPARE_DATA = 'SET_USAGE_COMPARE_DATA';
export const CLEAR_COMPARE = 'CLEAR_COMPARE';
export const SET_TOGGLE_BUTTON = 'SET_TOGGLE_BUTTON';
export const SET_SELECTED_DATE = 'SET_SELECTED_DATE';
export const SET_SELECTED_WEEK = 'SET_SELECTED_WEEK';
export const SET_SELECTED_MONTH = 'SET_SELECTED_MONTH';
export const SET_SELECTED_YEAR = 'SET_SELECTED_YEAR';
export const SET_ELECTRIC_RATE = 'SET_ELECTRIC_RATE';

export const setSelectedDate = selectedFromDate => ({
  type: SET_SELECTED_DATE,
  selectedFromDate,
});
export const setSelectedWeek = selectedFromWeek => ({
  type: SET_SELECTED_WEEK,
  selectedFromWeek,
});
export const setSelectedMonth = selectedFromMonth => ({
  type: SET_SELECTED_MONTH,
  selectedFromMonth,
});
export const setSelectedYear = selectedFromYear => ({
  type: SET_SELECTED_YEAR,
  selectedFromYear,
});

export const setLoading = isLoading => ({
  type: SET_LOADING,
  isLoading,
});

export const fetchUsageData = fetchUsageParams => ({
  type: FETCH_USAGE_DATA,
  fetchUsageParams,
});

export const setSelectedPeriod = periodType => ({
  type: SET_SELECTED_PERIOD,
  periodType,
});

export const setUsageData = usageParams => ({
  type: SET_USAGE_DATA,
  usageParams,
});

export const setToggleButton = toggleParams => ({
  type: SET_TOGGLE_BUTTON,
  toggleParams,
});

export const setCompareUsageData = usageParams => ({
  type: SET_USAGE_COMPARE_DATA,
  usageParams,
});

export const setUsageStatus = payload => ({
  type: SET_USAGE_STATUS,
  payload,
});

export const clearUsageStatus = () => ({
  type: CLEAR_USAGE_STATUS,
});

export const clearCompareData = value => ({
  type: CLEAR_COMPARE,
  value,
});

export const setElectricRate = value => ({
  type: SET_ELECTRIC_RATE,
  value,
});
