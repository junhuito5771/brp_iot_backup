export const SET_DEVICE_CONFIG_LOADING = 'SET_DEVICE_CONFIG_LOADING';
export const SUBMIT_DEVICE_CONFIG_PARAMS = 'SUBMIT_DEVICE_CONFIG_PARAMS';
export const CLEAR_DEVICE_CONFIG_STATUS = 'CLEAR_DEVICE_CONFIG_STATUS';
export const SET_DEVICE_CONFIG_STATUS = 'SET_DEVICE_CONFIG_STATUS';
export const SET_SELECTED_GROUP = 'SET_SELECTED_GROUP';

export const submitDeviceConfigParams = deviceConfigParams => ({
  type: SUBMIT_DEVICE_CONFIG_PARAMS,
  deviceConfigParams,
});

export const setDeviceConfigLoading = deviceConfigLoading => ({
  type: SET_DEVICE_CONFIG_LOADING,
  deviceConfigLoading,
});

export const clearDeviceConfigStatus = () => ({
  type: CLEAR_DEVICE_CONFIG_STATUS,
});

export const setDeviceConfigStatus = deviceConfigStatus => ({
  type: SET_DEVICE_CONFIG_STATUS,
  deviceConfigStatus,
});

export const setSelectedGroup = selectedGroup => ({
  type: SET_SELECTED_GROUP,
  selectedGroup,
});
