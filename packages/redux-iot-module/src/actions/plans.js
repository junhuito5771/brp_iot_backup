export const FETCH_ALL_PLANS = 'FETCH_ALL_PLANS';

export const fetchAllPlans = planIdParam => ({
  type: FETCH_ALL_PLANS,
  planIdParam,
});
