export const SET_AGREEMENT_VIEWED = 'SET_AGREEMENT_VIEWED';
export const SET_AGREEMENT_ACCEPTED = 'SET_AGREEMENT_ACCEPTED';

export const setAgreementViewed = agreementViewed => ({
  type: SET_AGREEMENT_VIEWED,
  agreementViewed,
});

export const setAgreementAccepted = agreementAccepted => ({
  type: SET_AGREEMENT_ACCEPTED,
  agreementAccepted,
});
