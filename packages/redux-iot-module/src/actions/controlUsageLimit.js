export const CHECK_EXPIRY_TIME = 'CHECK_EXPIRY_TIME';

export const checkExpiryTime = checkExpiryTimeParams => ({
  type: CHECK_EXPIRY_TIME,
  checkExpiryTimeParams,
});
