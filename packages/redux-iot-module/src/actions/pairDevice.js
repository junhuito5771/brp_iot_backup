export const CHECK_IOT_STATUS = 'CHECK_IOT_STATUS';
export const SET_IOT_STATUS_LOADING = 'SET_IOT_STATUS_LOADING';
export const SET_IOT_STATUS = 'SET_IOT_STATUS';
export const INCREMENT_RETRY = 'INCREMENT_RETRY';
export const RESET_RETRY = 'RESET_RETRY';
export const CLEAR_IOT_STATUS = 'CLEAR_IOT_STATUS';
export const CREATE_UNIT_IN_IOT = 'CREATE_UNIT_IN_IOT';
export const SUBMIT_BIND_NO_QR = 'SUBMIT_PAIR_NO_QR';
export const CANCEL_BIND_NO_QR = 'CANCEL_BIND_NO_QR';

export const checkIOTStatus = checkIoTParams => ({
  type: CHECK_IOT_STATUS,
  checkIoTParams,
});

export const clearIoTStatus = clearIoTStatusParams => ({
  type: CLEAR_IOT_STATUS,
  clearIoTStatusParams,
});

export const setIoTStatusLoading = iotStatusLoading => ({
  type: SET_IOT_STATUS_LOADING,
  iotStatusLoading,
});

export const setIoTStatus = iotStatus => ({
  type: SET_IOT_STATUS,
  iotStatus,
});

export const incrementRetry = () => ({
  type: INCREMENT_RETRY,
});

export const resetRetry = () => ({
  type: RESET_RETRY,
});

export const createUnitInIoT = thingName => ({
  type: CREATE_UNIT_IN_IOT,
  thingName,
});

export const submitBindNoQR = submitBindParams => ({
  type: SUBMIT_BIND_NO_QR,
  submitBindParams,
});

export const cancelBindNoQR = () => ({
  type: CANCEL_BIND_NO_QR,
});
