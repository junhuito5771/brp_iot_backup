export const SET_LOADING = 'SET_LOADING';
export const TOGGLE_UNIT = 'TOGGLE_UNIT';
export const SET_PREFFERED_UNITS = 'SET_TOGGLE_UNITS';
export const CLEAR_STATUS = 'CLEAR_STATUS';
export const SUBMIT_ALEXA_PREFERRED = 'SUBMIT_ALEXA_PREFERRED';
export const SET_PREFERRED_STATUS = 'SET_PREFERRED_STATUS';
export const FETCH_ALEXA_TOKEN = 'FETCH_ALEXA_TOKEN';
export const SET_ALEXA_TOKEN = 'SET_ALEXA_TOKEN';
export const SET_ALEXA_TOKEN_STATUS = 'SET_ALEXA_TOKEN_STATUS';
export const CLEAR_ALEXA_TOKEN_STATUS = 'CLEAR_ALEXA_TOKEN_STATUS';
export const ACTIVATE_ALEXA_SKILL = 'ACTIVATE_ALEXA_SKILL';
export const REFRESH_ALEXA_TOKEN = 'REFRESH_ALEXA_TOKEN';
export const FETCH_ALEXA_ACTIVATE_STATUS = 'FETCH_ALEXA_ACTIVATE_STATUS';
export const SET_ALEXA_COMMAND_URL = 'SET_ALEXA_COMMAND_URL';
export const FETCH_ALEXA_COMMAND_URL = 'FETCH_ALEXA_COMMAND_URL';
export const SUBMIT_LOGOUT_PARAMS = 'SUBMIT_LOGOUT_PARAMS';
export const RESET_ALEXA_COMMAND_REFRESH = 'RESET_ALEXA_COMMAND_REFRESH';

export const setLoading = isLoading => ({
  type: SET_LOADING,
  isLoading,
});

export const setToggleUnit = thingName => ({
  type: TOGGLE_UNIT,
  thingName,
});

export const clearStatus = () => ({
  type: CLEAR_STATUS,
});

export const setPrefferedUnits = prefferedUnits => ({
  type: SET_PREFFERED_UNITS,
  prefferedUnits,
});

export const submitAlexaPreferred = () => ({
  type: SUBMIT_ALEXA_PREFERRED,
});

export const setPreferredStatus = statusParams => ({
  type: SET_PREFERRED_STATUS,
  statusParams,
});

export const fetchAlexaToken = fetchTokenParams => ({
  type: FETCH_ALEXA_TOKEN,
  fetchTokenParams,
});

export const setAlexaToken = tokenParams => ({
  type: SET_ALEXA_TOKEN,
  tokenParams,
});

export const setAlexaTokenStatus = statusParams => ({
  type: SET_ALEXA_TOKEN_STATUS,
  statusParams,
});

export const clearAlexaTokenStatus = () => ({
  type: CLEAR_ALEXA_TOKEN_STATUS,
});

export const activateAlexaSkill = activateParams => ({
  type: ACTIVATE_ALEXA_SKILL,
  activateParams,
});

export const refreshAlexaToken = () => ({
  type: REFRESH_ALEXA_TOKEN,
});

export const fetchAlexaActivateStatus = activateParams => ({
  type: FETCH_ALEXA_ACTIVATE_STATUS,
  activateParams,
});

export const fetchAlexaCommandUrl = isDaikin => ({
  type: FETCH_ALEXA_COMMAND_URL,
  isDaikin,
});

export const setAlexaCommandUrl = commandParams => ({
  type: SET_ALEXA_COMMAND_URL,
  commandParams,
});

export const resetAlexaCommandRefresh = () => ({
  type: RESET_ALEXA_COMMAND_REFRESH,
});
