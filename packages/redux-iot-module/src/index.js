import { Provider, connect } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
// import '../shim';

import reducers from './reducers';
import * as selector from './selector';
import sagas from './sagas';
import {
  RemoteHelper,
  ShadowValueHelper,
  HandsetRuleHelper,
} from './utils/Remote';
import CryptoHelper from './utils/CryptoHelper';
import QRCodeHelper from './utils/QRCodeHelper';
import BLEService from './utils/BLE';

import configuration from './configuration';

export * from './actions';

export { Provider, connect, PersistGate, reducers, sagas };

// Export module configuration
export { configuration };

// // Export selector
export { selector };

export { RemoteHelper, ShadowValueHelper, HandsetRuleHelper };

export { CryptoHelper, BLEService, QRCodeHelper };
