import UDPService from 'react-native-udp';
import { StringConverter } from '@module/utility';

const DEFAULT_LOCAL_ADDRESS = '0.0.0.0';
const DEFAULT_TARGET_ADDRESS = '0.0.0.0';
const DEFAULT_TIMEOUT = 10000;
const DEFAULT_LOCAL_PORT = 61245;
const DEFAULT_TARGET_PORT = 61244;
const DEFAULT_NO_RESPONSE_MESSAGE = 'No response received';

const sendMessage = ({
  localAddress = DEFAULT_LOCAL_ADDRESS,
  localport = DEFAULT_LOCAL_PORT,
  targetAddress = DEFAULT_TARGET_ADDRESS,
  targetport = DEFAULT_TARGET_PORT,
  message,
  isBroadcast = false,
  timeout = DEFAULT_TIMEOUT,
  noResponseMessage = DEFAULT_NO_RESPONSE_MESSAGE,
  udpClient,
}) => {
  const promiseResponse = new Promise((resolve, reject) => {
    const response = [];
    let timeoutListener;

    const handleTimeout = () => {
      udpClient.close();
      if (response.length > 0) resolve(response);
      else reject(new Error(noResponseMessage));
    };

    udpClient.on(
      'listening',
      () => {
        timeoutListener = setTimeout(handleTimeout, timeout);

        const data = StringConverter.StringToByte(message);
        if (data) {
          udpClient.send(data, 0, data.length, targetport, targetAddress);
        }
      },
      err => {
        if (err) reject(err);
      }
    );

    udpClient.on('error', err => reject(err));

    udpClient.on('message', (data, rinfo) => {
      // Clear the timeout when receive data
      if (timeoutListener) {
        clearTimeout(timeoutListener);
        timeoutListener = null;
      }
      const dataInString = StringConverter.ByteToString(data);
      if (dataInString) response.push({ data: dataInString, source: rinfo });

      timeoutListener = setTimeout(handleTimeout, timeout);
    });
  });

  udpClient.bind(
    {
      address: localAddress,
      port: localport,
      exclusive: true,
    },
    () => {
      if (isBroadcast) {
        udpClient.setBroadcast(isBroadcast);
      }
    }
  );

  return promiseResponse;
};

const createUDPSocket = ({ type = 'udp4', reuseAddr = true }) =>
  UDPService.createSocket({ type, reuseAddr });

export default {
  createUDPSocket,
  sendMessage,
};
