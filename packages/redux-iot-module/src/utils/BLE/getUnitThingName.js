/* eslint import/no-extraneous-dependencies: 0 */
/* eslint import/extensions: 0 */
/* eslint import/no-unresolved: 0 */
import { Platform } from 'react-native';
import { StringConverter } from '@module/utility';
import decryption from './decryptionTable';

export default peripheral => {
  const byteLength = Platform.select({
    ios: peripheral.advertising.manufacturerData.bytes.length,
    android: 27,
  });
  const startingByte = Platform.select({
    ios: 2,
    android: 15,
  });

  const unitID = peripheral.advertising.manufacturerData.bytes.slice(
    startingByte,
    byteLength
  );
  const decryptedUnitID = `Daikin_${decryption(
    StringConverter.ByteToString(unitID)
  )
    .join('')
    .toLowerCase()}`;

  return decryptedUnitID;
};
