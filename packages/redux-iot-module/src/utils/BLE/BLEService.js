/* eslint import/no-extraneous-dependencies: 0 */
/* eslint import/extensions: 0 */
/* eslint import/no-unresolved: 0 */
import BleManager from 'react-native-ble-manager';
import { NativeModules, NativeEventEmitter } from 'react-native';
import { StringConverter } from '@module/utility';
import getUnitThingName from './getUnitThingName';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const DEFAULT_SERVICE_ID = '18aa';
const DEFAULT_CHAR_WRITE_ID = '2bb2';
const DEFAULT_SCAN_TIMEOUT = 10;
const DEFAULT_MAX_BYTE = 1024;

const BLE_CHAR_UPDATE = 'BleManagerDidUpdateValueForCharacteristic';
const BLE_SCAN_STOP = 'BleManagerStopScan';

const start = options => {
  BleManager.start(options);
};

const scan = ({
  maxScanTimeOut = DEFAULT_SCAN_TIMEOUT,
  canDuplicate = false,
} = {}) => {
  const promise = new Promise((resolve, reject) => {
    bleManagerEmitter.addListener(BLE_SCAN_STOP, () => {
      bleManagerEmitter.removeAllListeners(BLE_SCAN_STOP);
      resolve(true);
    });

    BleManager.scan([], maxScanTimeOut, canDuplicate).catch(error =>
      reject(new Error(error))
    );
  });

  return promise;
};

const getDiscoveredUnits = () => BleManager.getDiscoveredPeripherals();

const getTargetUnit = async thingName => {
  const discoveredUnits = await BleManager.getDiscoveredPeripherals();

  if (!discoveredUnits || discoveredUnits.length < -1) return undefined;

  const targetedPeripheral = discoveredUnits.find(
    peripheral =>
      peripheral.name === 'Daikin' &&
      peripheral.advertising &&
      peripheral.advertising.manufacturerData &&
      peripheral.advertising.manufacturerData.bytes &&
      getUnitThingName(peripheral) === thingName
  );

  return targetedPeripheral;
};

const connectAndRetrieveService = async targetedPeripheral => {
  await BleManager.connect(targetedPeripheral.id);

  const service = await BleManager.retrieveServices(targetedPeripheral.id);

  return service;
};

const startNotification = (peripheral, notifyID = DEFAULT_CHAR_WRITE_ID) => {
  BleManager.startNotification(
    peripheral.id,
    DEFAULT_SERVICE_ID,
    notifyID
  ).catch(() => {});

  return true;
};

const stopNotification = (peripheral, notifyID = DEFAULT_CHAR_WRITE_ID) => {
  BleManager.stopNotification(
    peripheral.id,
    DEFAULT_SERVICE_ID,
    notifyID
  ).catch(() => {});

  bleManagerEmitter.removeAllListeners(BLE_CHAR_UPDATE);

  return true;
};

const addNotificationListerner = emit => {
  bleManagerEmitter.addListener(BLE_CHAR_UPDATE, data => {
    const dataInString = StringConverter.ByteToString(data.value);

    emit(dataInString);
  });
};

const disconnect = peripheral => {
  BleManager.disconnect(peripheral.id).catch(() => {});
};

const sendMessage = async ({
  id,
  serviceID = DEFAULT_SERVICE_ID,
  charID = DEFAULT_CHAR_WRITE_ID,
  message,
  maxByte = DEFAULT_MAX_BYTE,
}) => {
  await BleManager.write(
    id,
    serviceID,
    charID,
    StringConverter.StringToByte(message),
    maxByte
  );
};

export default {
  start,
  connectAndRetrieveService,
  sendMessage,
  startNotification,
  stopNotification,
  addNotificationListerner,
  disconnect,
  getDiscoveredUnits,
  getTargetUnit,
  scan,
  getUnitThingName,
};
