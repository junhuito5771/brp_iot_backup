import React, { useEffect, useState } from 'react';
import { NativeModules, NativeEventEmitter, Platform } from 'react-native';
import BleManager from 'react-native-ble-manager';
import { stringToBytes, bytesToString } from 'convert-string';
import hoistNonReactStatics from 'hoist-non-react-statics';
import { LocationHelper } from '@module/utility';
import {
  check as PermissionCheck,
  request as PermissionRequest,
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions';
import * as bleMode from '../../actions/bleMode';
import decryption from './decryptionTable';

const BleManagerModule = NativeModules.BleManager;
const bleManagerEmitter = new NativeEventEmitter(BleManagerModule);

const BLE_DISCOVER = 'BleManagerDiscoverPeripheral';
const BLE_SCAN_STOP = 'BleManagerStopScan';
const BLE_UPDATE_STATE = 'BleManagerDidUpdateState';
const BLE_UPDATE_VALUE_CHARACTERTISTIC =
  'BleManagerDidUpdateValueForCharacteristic';
const BLE_DISCONNECT = 'BleManagerDisconnectPeripheral';

const BLE_STATE_ON = 'on';
const BLE_STATE_NOT_AUTH = 'unauthorized';

const getUnitThingName = peripheral => {
  const byteLength = Platform.select({
    ios: peripheral.advertising.manufacturerData.bytes.length,
    android: 27,
  });
  const startingByte = Platform.select({
    ios: 2,
    android: 15,
  });

  const unitID = peripheral.advertising.manufacturerData.bytes.slice(
    startingByte,
    byteLength
  );
  const decryptedUnitID = `Daikin_${decryption(bytesToString(unitID))
    .join('')
    .toLowerCase()}`;

  return decryptedUnitID;
};

export const getBluetoothState = async () =>
  new Promise((resolve, reject) => {
    try {
      const checkStateListerner = bleManagerEmitter.addListener(
        BLE_UPDATE_STATE,
        ({ state }) => {
          checkStateListerner.remove();
          resolve(state);
        }
      );
    } catch (error) {
      reject(error);
    }

    BleManager.checkState();
  });

export const checkIsBluetoothOn = async () => {
  const isBluetoothOn = (await getBluetoothState()) === BLE_STATE_ON;

  return isBluetoothOn;
};

export const turnOnBluetoothIfNeededAndroid = async () => {
  const bluetoothEnabler = () =>
    new Promise(resolve =>
      BleManager.enableBluetooth()
        .then(() => resolve(true))
        .catch(() => resolve(false))
    );

  const status = (await checkIsBluetoothOn()) || (await bluetoothEnabler());

  return status;
};

export const discoverAndProcessUnits = async dispatch => {
  dispatch(bleMode.processBleUnits(getUnitThingName));
};

const getErrorMessageFromCode = code => {
  switch (code) {
    case 'BluetoothOff':
      return 'Bluetooth is turned off';
    case 'BluetoothNotAuthorized':
      return 'Bluetooth permission is not granted';
    case 'LocationNotAuthorized':
      return 'Location permission is not granted';
    case 'LocationOff':
      return 'Location is turned off';
    default:
      return 'Unknown error';
  }
};

export const checkBluetoothAndLocation = async () => {
  const checkPermission = Platform.select({
    ios: async () => {
      let errorMsg = '';
      const isBluetoothNotAuth =
        (await getBluetoothState()) === BLE_STATE_NOT_AUTH;

      if (isBluetoothNotAuth) {
        errorMsg = getErrorMessageFromCode('BluetoothNotAuthorized');

        errorMsg +=
          '\n\nPlease grant the permission in the Settings > Privacy > Bluetooth > GO DAIKIN';

        return {
          isAllowed: false,
          errorMsg,
        };
      }

      const isBluetoothOn = await checkIsBluetoothOn;

      if (!isBluetoothOn) {
        errorMsg = getErrorMessageFromCode('BluetoothOff');
      }

      return {
        isAllowed: isBluetoothOn,
        errorMsg,
      };
    },
    android: async () => {
      let errorMsg = '';
      try {
        const isBluetoothOn = await turnOnBluetoothIfNeededAndroid();

        if (!isBluetoothOn) {
          errorMsg = getErrorMessageFromCode('BluetoothOff');
          return { isAllowed: false, errorMsg };
        }

        const hasLocationPermission =
          (await PermissionCheck(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION)) ===
          RESULTS.GRANTED;

        const isLocationGranted =
          hasLocationPermission ||
          (await PermissionRequest(
            PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
          )) === RESULTS.GRANTED;

        if (!isLocationGranted) {
          errorMsg = getErrorMessageFromCode('LocationNotAuthorized');
          return { isAllowed: false, errorMsg };
        }

        const isLocationOn = await LocationHelper.enableLocationIfNeeded();

        if (!isLocationOn) {
          errorMsg = getErrorMessageFromCode('LocationOff');
          return { isAllowed: false, errorMsg };
        }

        return { isAllowed: true, errorMsg };
      } catch (error) {
        return {
          isAllowed: false,
          errorMsg:
            'Unexpected error occured when requesting for bluetooth and location permission.',
        };
      }
    },
  });

  const { isAllowed, errorMsg } = await checkPermission();

  return { isAllowed, errorMsg };
};

const withBleManager = ({
  maxScanTimeOut,
  canDuplicate,
  updateProgress,
}) => WrappedComponent => {
  const Component = props => {
    const [units, setUnits] = useState([]);

    const scan = async () => {
      BleManager.scan([], maxScanTimeOut, canDuplicate);
    };

    const discoverUnit = async () => {
      bleManagerEmitter.removeAllListeners(BLE_SCAN_STOP);
      const PromiseResult = new Promise(resolve => {
        bleManagerEmitter.addListener(BLE_SCAN_STOP, () => {
          BleManager.getDiscoveredPeripherals().then(peripheralsArray => {
            const filteredPeripherals = peripheralsArray.reduce(
              (acc, peripheral) => {
                if (peripheral.name === 'Daikin') {
                  const unitThingName = getUnitThingName(peripheral);
                  // Strictly avoid duplication
                  if (!acc[unitThingName]) {
                    // eslint-disable-next-line no-param-reassign
                    acc[unitThingName] = {
                      thingName: unitThingName,
                      id: peripheral.id,
                    };
                  }
                }

                return acc;
              },
              {}
            );
            bleManagerEmitter.removeAllListeners(BLE_SCAN_STOP);
            // stopScanListern.remove();
            resolve(Object.values(filteredPeripherals));
          });
        });
      });

      setUnits([]);
      await scan();

      return PromiseResult;
    };

    const onMount = async () => {
      await BleManager.start({ showAlert: false });
    };

    useEffect(() => {
      onMount();
    }, []);

    const sendMessage = ({
      deviceName,
      serviceID,
      charID,
      message,
      notifyID,
    }) => {
      let isAnyDeviceFound = false;
      return new Promise((resolve, reject) => {
        try {
          let responses = [];
          let processedResponse = '';
          const stopScanListener = bleManagerEmitter.addListener(
            BLE_SCAN_STOP,
            () => {
              if (!isAnyDeviceFound) {
                stopScanListener.remove();
                reject(new Error('No unit found'));
              }
            }
          );
          const disconnectListener = bleManagerEmitter.addListener(
            BLE_DISCONNECT,
            () => {
              disconnectListener.remove();
              reject(
                new Error(
                  'Connection to unit timeout. Please make sure that you are connected to 2.4Ghz Wi-Fi, enabled Bluetooth and location services.'
                )
              );
            }
          );
          bleManagerEmitter.addListener(BLE_DISCOVER, async peripheral => {
            const peripheralID = peripheral.id;
            if (peripheral.name === 'Daikin') {
              const decryptedUnitThingName = getUnitThingName(peripheral);

              if (decryptedUnitThingName === deviceName) {
                isAnyDeviceFound = true;

                await BleManager.stopScan();

                await BleManager.connect(peripheralID);
                updateProgress(
                  {
                    status: 4,
                    text: 'Device found',
                  },
                  props
                );
                const service = await BleManager.retrieveServices(peripheralID);
                updateProgress(
                  {
                    status: 4.5,
                    text: 'Transmitting message via Bluetooth to Wifi module',
                  },
                  props
                );
                if (service) {
                  bleManagerEmitter.addListener(
                    BLE_UPDATE_VALUE_CHARACTERTISTIC,
                    data => {
                      const dataInString = bytesToString(data.value);
                      if (
                        responses.length > 0 &&
                        dataInString.indexOf('<{') > -1
                      ) {
                        responses = [dataInString];
                        return;
                      }
                      responses.push(dataInString);

                      if (dataInString.indexOf('}>', 0) > -1) {
                        const responseInString = responses.reduce(
                          (acc, res) => acc.concat(res),
                          ''
                        );

                        if (responseInString.indexOf('bleconfig', 0) === -1) {
                          responses = [];
                          return;
                        }
                        processedResponse = responseInString.replace(
                          /\s|<|>/g,
                          ''
                        );
                        BleManager.disconnect(peripheralID).catch(() => {});
                        bleManagerEmitter.removeAllListeners(
                          BLE_UPDATE_VALUE_CHARACTERTISTIC
                        );
                        bleManagerEmitter.removeAllListeners(BLE_DISCOVER);
                        bleManagerEmitter.removeAllListeners(BLE_DISCONNECT);
                        resolve(processedResponse);
                      }
                    }
                  );

                  await BleManager.write(
                    peripheralID,
                    serviceID,
                    charID,
                    stringToBytes(message),
                    20
                  );

                  BleManager.startNotification(
                    peripheralID,
                    serviceID,
                    notifyID
                  ).catch(() => {
                    if (Platform.OS === 'ios') {
                      reject(new Error('Notify error'));
                    }
                  });

                  setTimeout(() => {
                    BleManager.disconnect().catch(() => {});
                  }, 20000);

                  updateProgress(
                    {
                      status: 6,
                      text: 'Wifi module attempting to connect to network',
                    },
                    props
                  );
                } else {
                  reject(new Error('No service found'));
                }
              }
            }
          });

          scan();
        } catch (error) {
          reject(error);
        }
      });
    };

    const bleManagerProps = {
      sendMessage,
      scan,
      discoverUnit,
      units,
    };

    return <WrappedComponent {...props} bleManager={bleManagerProps} />;
  };

  return hoistNonReactStatics(Component, WrappedComponent);
};
export { withBleManager };
