const decryptionTable = {
  A: 0,
  s: 1,
  v: 2,
  b: 3,
  '=': 4,
  '#': 5,
  f: 6,
  G: 7,
  H: 8,
  p: 9,
  n: 'A',
  m: 'B',
  L: 'C',
  o: 'D',
  k: 'E',
  Q: 'F',
};

export default value => [...value].map(char => decryptionTable[char]);
