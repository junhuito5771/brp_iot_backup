import { StringConverter } from '@module/utility';
import CryptoHelper from './CryptoHelper';

function processHostQRVer2(data) {
  const processedThingName = /t=(.*)/g.exec(data[0]);
  const processedThingType = /y=(.*)/g.exec(data[1]);
  const processedFactoryKey = /f=(.*)/g.exec(data[2]);

  const isValid =
    processedThingName &&
    processedThingName.length === 2 &&
    processedThingType &&
    processedThingType.length === 2 &&
    processedFactoryKey &&
    processedFactoryKey.length === 2;

  if (!isValid) {
    return {
      error: 'Invalid QR Code',
    };
  }

  return {
    version: 2,
    type: 'host',
    data: {
      thingName: processedThingName[1],
      thingType: processedThingType[1],
      factoryKey: processedFactoryKey[1],
    },
  };
}

function getErrorIfQRCodeExpired(expiredDT) {
  const currentTimestamp = Date.now();

  if (currentTimestamp > expiredDT) {
    return { error: 'This QR Code has expired' };
  }

  return {};
}

function processQRVer3(data, key, iv) {
  try {
    // If data length more than 49, then use isAutoPadding is true
    const isAutoPadding = data.length > 49;

    // 1. Remove the <~ and also ~>
    const noPrefixData = data.substring(2).slice(0, -2);

    // 2. Decode the ASCII85 to ASCII
    const decodedData = StringConverter.ascii85ToAscii(noPrefixData);

    // 3. Cut the decoded text into two parts
    const encryptedData = decodedData.slice(0, -8);
    const randomKey = decodedData.slice(-8);

    // 4. Insert the random key into the iv
    const finalIV = CryptoHelper.insertRandomKeyToIV(iv, randomKey);

    // 5. Run the decryption
    const decryptedText = CryptoHelper.decryption({
      text: encryptedData,
      key,
      iv: finalIV,
      type: CryptoHelper.TYPE_AES_256_CBC,
      inputType: CryptoHelper.ENCODING_HEX,
      outputType: CryptoHelper.ENCODING_ASCII,
      isAutoPadding,
    });

    const decryptedTextArray = decryptedText.split(',');

    // Return invalid QRCode if it is not host and guest
    // 4 fields means host QRCode
    // 8 fields means guest QRCode
    const isHostQR = decryptedTextArray.length === 4;
    const isGuestQR = decryptedTextArray.length === 8;
    if (!decryptedTextArray || !(isHostQR || isGuestQR)) {
      return {
        error: 'Invalid QR Code',
      };
    }

    if (isGuestQR) {
      const [
        thingNameID,
        acGroup,
        acName,
        logo,
        qx,
        randomkey,
        expiredDT,
        unitCode,
      ] = decryptedTextArray;
      // Check if the QRCode is expired
      const expiredErrObj = getErrorIfQRCodeExpired(expiredDT);
      if (expiredErrObj && expiredErrObj.error) {
        return expiredErrObj;
      }

      return {
        version: 3,
        type: 'guest',
        data: {
          thingName: `Daikin_${thingNameID}`,
          acGroup,
          acName,
          logo,
          qx,
          key: randomkey,
          unitCode,
        },
      };
    }

    const [thingNameID, factoryKey] = decryptedTextArray;

    return {
      version: 3,
      type: 'host',
      data: {
        thingName: `Daikin_${thingNameID}`,
        factoryKey,
      },
    };
  } catch (error) {
    return {
      error: 'Error while trying to process QRCode',
    };
  }
}

function processGuestQR(data) {
  const qrCodeObj = JSON.parse(data);
  const isValid = Object.keys(qrCodeObj).length === 8;

  if (!isValid) {
    return { error: 'Invalid Guest QR Code' };
  }

  // Check if the QRCode is expired
  const expiredErrObj = getErrorIfQRCodeExpired(qrCodeObj.expiredDT);
  if (expiredErrObj && expiredErrObj.error) {
    return expiredErrObj;
  }

  return {
    version: 1,
    type: 'guest',
    data: qrCodeObj,
  };
}

const QRHandler = (data, key, iv) => {
  const result = {};

  if (!data) return result;
  if (data.length < 3) {
    return { error: 'Invalid QRCode' };
  }

  const isGuest = data && data.length > 0 && data[0] === '{';

  if (isGuest) {
    return processGuestQR(data);
  }

  const encryptionMethod = data.slice(0, 2);
  const encryptedPayload = encryptionMethod === '01' ? data.slice(2) : data;
  if (encryptedPayload.startsWith('<~') && encryptedPayload.endsWith('~>')) {
    // Process encrypted Host and Guest QRCode
    return processQRVer3(encryptedPayload, key, iv);
  }

  const items = data.split(',');

  // QRCode Ver 1 no longer supported from 1.0.23 onward
  return processHostQRVer2(items);
};

const getEncryptedQRPayload = ({
  thingName,
  acGroup,
  acName,
  logo,
  qxInfo,
  randomKey,
  expiredDuration,
  secretKey,
  iv,
  unitCode,
}) => {
  const expiredDT = Date.now() + expiredDuration;
  const payload = `${thingName},${acGroup},${acName},${logo},${qxInfo},${randomKey},${expiredDT.toString()},${unitCode}`;
  const randomBytes = CryptoHelper.generateRandomBytes(
    4,
    CryptoHelper.ENCODING_HEX
  );
  const randomIV = CryptoHelper.insertRandomKeyToIV(iv, randomBytes);

  const encrypPayload = CryptoHelper.encryption({
    text: payload,
    key: secretKey,
    iv: randomIV,
    type: CryptoHelper.TYPE_AES_256_CBC,
    inputType: CryptoHelper.ENCODING_ASCII,
    outputType: CryptoHelper.ENCODING_HEX,
  });

  const finalEncryPayload = encrypPayload + randomBytes;
  const encodedPayload = StringConverter.asciiToAscii85(finalEncryPayload);

  return `01${encodedPayload}`;
};

export default {
  getEncryptedQRPayload,
  QRHandler,
};
