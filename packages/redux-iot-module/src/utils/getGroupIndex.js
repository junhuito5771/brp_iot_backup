import { select } from 'redux-saga/effects';

export default function* getGroupUnitIndex(groupName) {
  const units = yield select(state => state.units);
  const allGroups = units.groupInfo.groups;

  const groupInArray = Object.keys(allGroups);

  let groupIndex = 0;
  const groupKey = groupInArray.find(
    key => allGroups[key].groupName === groupName
  );

  if (groupKey) {
    // Group exists
    groupIndex = allGroups[groupKey].groupIndex;
  } else {
    // Group not exists
    groupIndex =
      groupInArray.reduce((acc, key) => {
        const currentGroupIndex = Number.parseInt(
          allGroups[key].groupIndex,
          10
        );
        return acc > currentGroupIndex ? acc : currentGroupIndex;
      }, -1) + 1;
  }

  return { groupIndex, groupKey };
}
