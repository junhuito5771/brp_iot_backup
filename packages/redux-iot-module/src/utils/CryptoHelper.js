/* eslint-disable no-buffer-constructor */

// import cryto from 'crypto';

const TYPE_AES_128_CBC = 'aes-128-cbc';
const TYPE_AES_256_CBC = 'aes-256-cbc';
const ENCODING_UTF8 = 'utf8';
const ENCODING_HEX = 'hex';
const ENCODING_ASCII = 'ascii';

function decryption({
  text,
  key,
  iv,
  type,
  keyType = ENCODING_HEX,
  ivType = ENCODING_HEX,
  inputType = ENCODING_HEX,
  outputType = ENCODING_UTF8,
  isAutoPadding = true,
}) {
  // const secretKey = new Buffer(key, keyType);
  // const secretIV = new Buffer(iv, ivType);
  // const cipher = cryto.createDecipheriv(type, secretKey, secretIV);
  // cipher.setAutoPadding(isAutoPadding);

  // const decryptedText =
  //   cipher.update(text, inputType, outputType) + cipher.final(outputType);

  // return decryptedText;

  return "";
}

function encryption({
  type,
  text,
  key,
  iv,
  keyType = ENCODING_HEX,
  ivType = ENCODING_HEX,
  inputType = ENCODING_UTF8,
  outputType = ENCODING_HEX,
  isAutoPadding = true,
}) {
  // const secretKey = new Buffer(key, keyType);
  // const secretIV = new Buffer(iv, ivType);
  // const cipher = cryto.createCipheriv(type, secretKey, secretIV);
  // cipher.setAutoPadding(isAutoPadding);

  // const encryptedText =
  //   cipher.update(text, inputType, outputType) + cipher.final(outputType);

  // return encryptedText;
  return "";
}

function insertRandomKeyToIV(iv, randomKey) {
  if (!iv) {
    throw new Error('IV cannot be undefined');
  }

  if (!randomKey) {
    throw new Error('Random key cannot be undefined');
  }

  if (iv.length !== 24) {
    throw new Error('Length of IV must be 24');
  }

  if (randomKey.length !== 8) {
    throw new Error('Length of random key must be 8');
  }

  return (
    randomKey.substring(0, 2) +
    iv.substring(0, 8) +
    randomKey.substring(2, 4) +
    iv.substring(8, 16) +
    randomKey.substring(4, 6) +
    iv.substring(16, 24) +
    randomKey.substring(6, 8)
  );
}

function generateRandomBytes(noOfBytes, outputType) {
  // return cryto.randomBytes(noOfBytes).toString(outputType);
  return "";
}

export default {
  generateRandomBytes,
  insertRandomKeyToIV,
  encryption,
  decryption,
  TYPE_AES_128_CBC,
  TYPE_AES_256_CBC,
  ENCODING_UTF8,
  ENCODING_HEX,
  ENCODING_ASCII,
};

/* eslint-enable no-buffer-constructor */
