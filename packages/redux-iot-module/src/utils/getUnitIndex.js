import { select } from 'redux-saga/effects';

export default function* getUnitIndex(isGroupExist, groupIndex, thingName) {
  const units = yield select(state => state.units);
  const {allUnits} = units;
  const {groupUnits} = units;

  let unitIndex = 0;

  if (isGroupExist) {
    const isSameGroup = groupUnits[groupIndex].find(item => item === thingName);
    const unit = allUnits[thingName];

    if (isSameGroup && unit) {
      // reuse
      unitIndex = unit.unitIndex;
    } else {
      // Need to compute the highest unit index based within the same group
      unitIndex =
        groupUnits[groupIndex].reduce((acc, key) => {
          const curUnitIndex = Number.parseInt(allUnits[key].unitIndex, 10);
          return acc > curUnitIndex ? acc : curUnitIndex;
        }, -1) + 1;
    }
  }

  return unitIndex;
}
