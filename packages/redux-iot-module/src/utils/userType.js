function isHostUser(qx) {
  return qx === '15';
}

function isGuestUser(qx) {
  return qx === '0' || qx === '1';
}

export default {
  isHostUser,
  isGuestUser,
};
