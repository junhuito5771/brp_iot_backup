// eslint-disable-next-line import/no-unresolved
import PubSub from '@aws-amplify/pubsub';
import { v4 as uuid } from 'uuid';
// eslint-disable-next-line import/no-unresolved
import { AWSIoTProvider } from '@aws-amplify/pubsub/lib/Providers';

let subscribeHandlerMap = {};
let pubSubInstance;

async function init(config) {
  if (!pubSubInstance) {
    pubSubInstance = await PubSub.addPluggable(new AWSIoTProvider(config));
  }

  return pubSubInstance;
}

async function subscribeWithEmit({ topic, emit }) {
  if (!subscribeHandlerMap[topic]) {
    subscribeHandlerMap[topic] = await PubSub.subscribe(topic, {
      clientId: uuid(),
    }).subscribe({
      next: data => {
        try {
          const topicKey = Object.getOwnPropertySymbols(data.value)[0];
          if (data.value && data.value[topicKey] === topic) {
            emit(data);
          }
        } catch (error) {
          emit({ type: 'error', message: error.message });
        }
      },
      error: error => {
        emit({ type: 'error', message: error });
      },
    });
  }
}

function publish({ topic, msg }) {
  PubSub.publish(topic, {
    msg,
  });
}

function unsubscribe({ topic }) {
  if (subscribeHandlerMap[topic]) {
    subscribeHandlerMap[topic].unsubscribe();

    delete subscribeHandlerMap[topic];
  }
}

function unsubscribeAll() {
  Object.keys(subscribeHandlerMap).forEach(topic => {
    subscribeHandlerMap[topic].unsubscribe();
  });

  subscribeHandlerMap = {};
}

export default {
  init,
  subscribeWithEmit,
  publish,
  unsubscribe,
  unsubscribeAll,
};
