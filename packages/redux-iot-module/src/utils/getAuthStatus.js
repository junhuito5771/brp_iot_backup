export default function getAuthStatus({ data }) {
  // Failure if response has the code field
  if (data && data.code) {
    return {
      messageType: 'error',
      message: data.message,
      errorCode: data.code,
    };
  }

  return {
    messageType: 'success',
    message: 'success',
  };
}
