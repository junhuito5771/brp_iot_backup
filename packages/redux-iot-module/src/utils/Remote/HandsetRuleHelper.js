import { FEAT_FAN_SPEED } from './constants/feature';
import * as FanSpeedEnum from './constants/fanSpeed';

const damaHandsetRules = {
  cool: {
    fanSpeed: 60, // Binary 110101 => [auto, lv1, lv2, lv3, lv4, lv5]
    temperature: {
      min: 16,
      max: 30,
    },
    power: 1,
    quiet: 1,
    swing: 1,
    sleep: 1,
    ecoPlus: 1,
    smartDrift: 1,
    breeze: 1,
    smartSleep: 1,
    sense: 1,
    smartEcoMax: 1,
    smartPowerful: 1,
  },
  heat: {
    fanSpeed: 60,
    temperature: {
      min: 16,
      max: 30,
    },
    power: 1,
    quiet: 1,
    swing: 1,
    sleep: 1,
    ecoPlus: 1,
    sense: 1,
  },
  fan: {
    fanSpeed: 28,
    swing: 1,
    breeze: 1,
  },
  dry: {
    fanSpeed: 32,
    temperature: {
      min: 16,
      max: 30,
    },
    swing: 1,
  },
  auto: {
    fanSpeed: 60,
    temperature: {
      min: 16,
      max: 30,
    },
    swing: 1,
    sleep: 1,
  },
};

const dilHandsetRules = {
  cool: {
    fanSpeed: 63,
    temperature: {
      min: 18,
      max: 32,
    },
    power: 1,
    quiet: 1,
    swing: 1,
    sleep: 1,
    ecoPlus: 1,
  },
  heat: {
    fanSpeed: 63,
    temperature: {
      min: 16,
      max: 30,
    },
    power: 1,
    quiet: 1,
    swing: 1,
    sleep: 1,
    ecoPlus: 1,
  },
  fan: {
    fanSpeed: 63,
    power: 1,
    quiet: 1,
    swing: 1,
  },
  dry: {
    fanSpeed: 32,
    power: 1,
    swing: 1,
    ecoPlus: 1,
  },
  auto: {
    fanSpeed: 63,
    temperature: {
      min: 18,
      max: 30,
    },
    power: 1,
    swing: 1,
    ecoPlus: 1,
    quiet: 1,
  },
};

function getFanSpeedBinPos(fanSpeedValue) {
  const fanSpeedBinPosMap = {
    [FanSpeedEnum.FANSPEED_AUTO]: 0,
    [FanSpeedEnum.FANSPEED_LV1]: 1,
    [FanSpeedEnum.FANSPEED_LV2]: 2,
    [FanSpeedEnum.FANSPEED_LV3]: 3,
    [FanSpeedEnum.FANSPEED_LV4]: 4,
    [FanSpeedEnum.FANSPEED_LV5]: 5,
  };

  return fanSpeedBinPosMap[fanSpeedValue];
}

function getHandsetRule(mode, handsetType) {
  const handsetRuleMap = handsetType === 0 ? damaHandsetRules : dilHandsetRules;

  return handsetRuleMap[mode] || {};
}

export function getTempMinMax(mode, handsetType) {
  const handsetRuleMap = getHandsetRule(mode, handsetType);

  return handsetRuleMap.temperature;
}

export function isFanSpeedOptAllowed(mode, fanSpeedOptValue, handsetType) {
  const fanSpeedDec = getHandsetRule(mode, handsetType)[FEAT_FAN_SPEED];
  const fanSpeedBin = (fanSpeedDec >>> 0).toString(2).padStart(6, '0'); // eslint-disable-line no-bitwise
  const fanSpeedBinPos = getFanSpeedBinPos(fanSpeedOptValue);

  return fanSpeedBin[fanSpeedBinPos] === '1';
}

export function isModeAllowed(mode, handsetType) {
  return Object.keys(getHandsetRule(mode, handsetType)).length > 0;
}

export function isFeatureAllowed(mode, featureName, handsetType) {
  return getHandsetRule(mode, handsetType)[featureName] !== undefined;
}

export default {
  isFeatureAllowed,
  isModeAllowed,
  isFanSpeedOptAllowed,
  getTempMinMax,
};
