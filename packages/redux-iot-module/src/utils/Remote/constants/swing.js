export const SWING_OFF = 'swingOff';
export const SWING_ON = 'swingOn';
export const SWING_UD_LV1 = 'SwingUDLv1';
export const SWING_UD_LV2 = 'SwingUDLv2';
export const SWING_UD_LV3 = 'SwingUDLv3';
export const SWING_UD_LV4 = 'SwingUDLv4';
export const SWING_UD_LV5 = 'SwingUDLv5';
export const SWING_UD_FULL = 'SwingUDFull';
export const SWING_UD_OFF = 'SwingUDOff';

export const SWING_LR_OFF = 'SwingLROff';
export const SWING_LR_FULL = 'SwingLRFull';
export const SWING_LR_LV1 = 'SwingLRLv1';
export const SWING_LR_LV2 = 'SwingLRLv2';
export const SWING_LR_LV3 = 'SwingLRLv3';
export const SWING_LR_LV4 = 'SwingLRLv4';
export const SWING_LR_LV5 = 'SwingLRLv5';
