// Remote control mode constant
export const MODE_AUTO = 'auto';
export const MODE_COOL = 'cool';
export const MODE_HEAT = 'heat';
export const MODE_FAN = 'fan';
export const MODE_DRY = 'dry';
