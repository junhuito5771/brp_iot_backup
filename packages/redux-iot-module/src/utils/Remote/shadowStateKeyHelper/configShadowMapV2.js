// This is the V2 shadow key variable name
// This will override the V1 shadow key if key matched
import {
  SET_SMART_TURBO,
  SET_UDLVR,
  SET_LRLVR,
  SET_SMART_DRIFT,
  SET_SMART_ECOMAX,
  SET_SMART_SLEEP,
  SET_POWER_IND,
  SET_CK_SWING,
} from '../constants/configShadowKey';

export default {
  [SET_SMART_TURBO]: 'Set_SmPwrfulplus',
  [SET_UDLVR]: 'Set_UDLvr',
  [SET_LRLVR]: 'Set_LRLvr',
  [SET_SMART_DRIFT]: 'Set_SmDrift',
  [SET_SMART_ECOMAX]: 'Set_SmEcomax',
  [SET_SMART_SLEEP]: 'Set_SmSleepplus',
  [SET_POWER_IND]: 'Set_PwrInd',
  [SET_CK_SWING]: 'Set_CKSwing',
};
