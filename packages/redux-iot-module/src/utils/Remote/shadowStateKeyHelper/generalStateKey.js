import ModeConfigShadowKey from './configShadowMap';

export default {
  thingName: 'thingName',
  Set_OnOff: 'switch',
  eventType: 'status',
  Set_Mode: 'mode',
  key: 'key',

  // Stats
  Stat_IDRoomTemp: 'indoorTemp',
  Stat_ODAirTemp: 'outdoorTemp',
  Stat_ErrorCode: 'errorCode',
  Stat_ODPowerConsumption: 'powerConsumption',
  Info_ProductSystem: 'isInverter',
  ota_flag: 'otaRunning',
  Inf_IDAlgo: 'handsetType', // 0 = DAMA, 1 = DIL

  // Features
  Enable_Silent: 'enableSilent',
  Enable_Turbo: 'enableTurbo',
  Enable_Sense: 'enableSense',
  Enable_Ecoplus: 'enableEcoplus',
  Enable_eLight: 'enableELight',
  Enable_LEDOff: 'enableLED',
  Enable_Streamer: 'enableStreamer',
  Enable_Breeze: 'enableBreeze',
  Enable_PowerIndication: 'enablePowerInd',
  Enable_SmartPowerfulplus: 'enableSmartTurbo',
  Enable_SmartDrift: 'enableSmartDrift',
  Enable_SmartSleepplus: 'enableSmartSleep',
  Enable_SmartEcomax: 'enableSmartEcomax',

  // general enablement
  Bar_LowFan: 'enableLowFan',
  Bar_AutoFan: 'enableAutoFan',
  Bar_Timer: 'enableTimer',
  Bar_Sleep: 'enableSleep',

  // mode enablement
  enableModes: {
    Bar_AutoMode: 'auto',
    Bar_CoolMode: 'cool',
    Bar_HeatMode: 'heat',
    Bar_FanMode: 'fan',
    Bar_DryMode: 'dry',
  },

  // modeConfig
  modeConfig: {
    // State for fan speed
    [ModeConfigShadowKey.SET_FAN]: 'fan',
    [ModeConfigShadowKey.SET_FAN_EXTEND]: 'fanExtend',
    [ModeConfigShadowKey.SET_SILENT]: 'silent',
    [ModeConfigShadowKey.SET_SLEEP]: 'sleep',
    [ModeConfigShadowKey.SET_TURBO]: 'turbo',
    [ModeConfigShadowKey.SET_ECOPLUS]: 'ecoplus',
    [ModeConfigShadowKey.SET_TEMP]: 'acTemp',
    [ModeConfigShadowKey.SET_SWING]: 'swing',
    [ModeConfigShadowKey.SET_SENSE]: 'sense',
    [ModeConfigShadowKey.SET_SMART_TURBO]: 'smartTurbo',
    [ModeConfigShadowKey.SET_BREEZE]: 'breeze',
    [ModeConfigShadowKey.SET_SMART_DRIFT]: 'smartDrift',
    [ModeConfigShadowKey.SET_STREAMER]: 'streamer',
    [ModeConfigShadowKey.SET_SMART_ECOMAX]: 'smartEcoMax',
    [ModeConfigShadowKey.SET_SMART_SLEEP]: 'smartSleep',
    [ModeConfigShadowKey.SET_LED]: 'led',
    [ModeConfigShadowKey.SET_POWER_IND]: 'powerInd',
  },

  // Other
  msgver: 'ver',
  version: 'firmwareVersion',
  shadowStateVersion: 'version',
};
