import ModeConfigShadowKeyV2 from './configShadowMapV2';

export default {
  // S0.0.2 onwards
  // Stats
  Sta_IDRoomTemp: 'indoorTemp',
  Sta_ODAirTemp: 'outdoorTemp',
  Sta_ErrCode: 'errorCode',
  Sta_ODPwrCon: 'powerConsumption',
  Inf_ProdSys: 'isInverter',
  Inf_ODPwrCon: 'enableEnergyConsumption',

  // Enablement
  Ena_Silent: 'enableSilent',
  Ena_Turbo: 'enableTurbo',
  Ena_Sense: 'enableSense',
  Ena_Ecoplus: 'enableEcoplus',
  Ena_eLight: 'enableELight',
  Ena_LEDOff: 'enableLED',
  Ena_Streamer: 'enableStreamer',
  Ena_Breeze: 'enableBreeze',
  Ena_PwrInd: 'enablePowerInd',
  Ena_SmPwrfulplus: 'enableSmartTurbo',
  Ena_SmDrift: 'enableSmartDrift',
  Ena_SmSleepplus: 'enableSmartSleep',
  Ena_SmEcomax: 'enableSmartEcomax',
  Ena_LRSwing: 'enableLRSwing',
  Ena_LRStep: 'enableLRStep',
  Ena_UDStep: 'enableUDStep',
  Ena_CKSwing: 'enableCKSwing',

  // Bar
  Bar_LowF: 'enableLowFan',
  Bar_AutoF: 'enableAutoFan',

  // modeConfig
  modeConfig: {
    // State for fan speed
    [ModeConfigShadowKeyV2.SET_SMART_TURBO]: 'smartTurbo',
    [ModeConfigShadowKeyV2.SET_LRLVR]: 'leftRightSwing',
    [ModeConfigShadowKeyV2.SET_UDLVR]: 'upDownSwing',
    [ModeConfigShadowKeyV2.SET_SMART_DRIFT]: 'smartDrift',
    [ModeConfigShadowKeyV2.SET_SMART_ECOMAX]: 'smartEcoMax',
    [ModeConfigShadowKeyV2.SET_SMART_SLEEP]: 'smartSleep',
    [ModeConfigShadowKeyV2.SET_POWER_IND]: 'powerInd',
    [ModeConfigShadowKeyV2.SET_CK_SWING]: 'ckSwing',
  },

  // mode enablement
  enableModes: {
    Bar_AutoM: 'auto',
    Bar_CoolM: 'cool',
    Bar_HeatM: 'heat',
    Bar_FanM: 'fan',
    Bar_DryM: 'dry',
  },
};
