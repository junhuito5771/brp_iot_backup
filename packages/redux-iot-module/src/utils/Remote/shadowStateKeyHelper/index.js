import { ObjectHelper } from '@module/utility';

import generalStateKey from './generalStateKey';
import generalStateKeyV2 from './generalStateKeyV2';
import modeConfigShadowKey from './configShadowMap';
import modeConfigShadowKeyV2 from './configShadowMapV2';

export const checkIsV1 = firmwareVersion =>
  firmwareVersion &&
  (firmwareVersion[0] === 'S' ||
    firmwareVersion === 'V0.0.1' ||
    firmwareVersion === 'V0.0.12');

const getGenerateStateKey = firmwareVersion =>
  checkIsV1(firmwareVersion) ? generalStateKey : generalStateKeyV2;

export const getGeneralStateKey = (firmwareVersion, shadowKey) =>
  getGenerateStateKey(firmwareVersion)[shadowKey] || generalStateKey[shadowKey];

export const getModeConfigStateMap = firmwareVersion => {
  const modeConfigMap = {
    ...generalStateKey.modeConfig,
    ...(checkIsV1(firmwareVersion) ? {} : generalStateKeyV2.modeConfig),
  };

  return modeConfigMap;
};

export const getModeConfigStateKey = (firmwareVersion, shadowKey) => {
  const modeConfig = getModeConfigStateMap(firmwareVersion);

  return modeConfig[shadowKey];
};

export const getModeEnablementMap = firmwareVersion =>
  getGenerateStateKey(firmwareVersion).enableModes;

export const getModeEnablementStateKey = (firmwareVersion, shadowKey) => {
  const modeEnablementMap = getModeEnablementMap(firmwareVersion);

  return modeEnablementMap[shadowKey] || generalStateKey.enableModes[shadowKey];
};

export const getModeConfigShadowKeyMap = firmwareVersion => {
  const modeConfigShadowKeyMap = ObjectHelper.addMembers(
    modeConfigShadowKey,
    modeConfigShadowKeyV2,
    !checkIsV1(firmwareVersion)
  );

  return modeConfigShadowKeyMap;
};

export default {
  getModeConfigShadowKeyMap,
  getModeEnablementStateKey,
  getModeEnablementMap,
  getModeConfigStateKey,
  getModeConfigStateMap,
  getGeneralStateKey,
  checkIsV1,
};
