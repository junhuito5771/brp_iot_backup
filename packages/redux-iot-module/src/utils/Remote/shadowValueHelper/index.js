import FanLevelMapper from './fanLevelMapper';
import TurboMapper from './turboMapper';
import ModeMapper from './modeMapper';
import EcoplusMapper from './ecoplusMapper';
import SleepMapper from './sleepMapper';
import SwingMapper from './swingMapper';
import SwingLRMapper from './swingLRMapper';
import LEDMapper from './ledMapper';
import CKSwingMapper from './ckSwingMapper';

export default {
  ...FanLevelMapper,
  ...TurboMapper,
  ...ModeMapper,
  ...EcoplusMapper,
  ...SleepMapper,
  ...SwingMapper,
  ...SwingLRMapper,
  ...LEDMapper,
  ...CKSwingMapper,
};
