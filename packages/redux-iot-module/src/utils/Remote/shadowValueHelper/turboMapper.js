import { ObjectHelper } from '@module/utility';
import * as TurboKeyMap from '../constants/turbo';
import { getModeConfigShadowKeyMap } from '../shadowStateKeyHelper';

const DELIMITER = '_';

// First digit is turbo, second is smart turbo
const turboValueMap = {
  [TurboKeyMap.TURBO_OFF]: '0_0',
  [TurboKeyMap.TURBO_ON]: '1_0',
  [TurboKeyMap.TURBO_SMART_ON]: '0_1',
};

const shadowValMap = ObjectHelper.invertObj(turboValueMap);

function getTurboValueKeyByShadowVal(turbo, smartTurbo) {
  return `${turbo}${DELIMITER}${smartTurbo}`;
}

function getShadowValByTurbo(turboValKey) {
  const [turbo = 0, smartTurbo = 0] = (
    turboValueMap[turboValKey] || turboValueMap[TurboKeyMap.TURBO_OFF]
  ).split(DELIMITER);

  return { turbo, smartTurbo };
}

export default {
  getTurboValueKeyByShadowVal,
  getTurboByShadowVal: (turbo, smartTurbo) => {
    const valueKey = getTurboValueKeyByShadowVal(turbo, smartTurbo);

    return shadowValMap[valueKey] || TurboKeyMap.TURBO_OFF;
  },
  getShadowValByTurbo,
  getShadowObjByTurbo: (turboKey, firmwareVersion) => {
    const modeConfigShadowKey = getModeConfigShadowKeyMap(firmwareVersion);

    const { turbo, smartTurbo } = getShadowValByTurbo(turboKey);

    return {
      [modeConfigShadowKey.SET_TURBO]: parseInt(turbo, 10),
      [modeConfigShadowKey.SET_SMART_TURBO]: parseInt(smartTurbo, 10),
    };
  },
  TurboKeyMap,
  TurboValueMap: turboValueMap,
};
