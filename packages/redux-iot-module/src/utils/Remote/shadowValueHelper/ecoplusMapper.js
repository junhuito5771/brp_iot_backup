import { ObjectHelper } from '@module/utility';
import * as EcoplusKeyMap from '../constants/ecoplus';
import { getModeConfigShadowKeyMap } from '../shadowStateKeyHelper';

const DELIMITER = '_';

// First digit is turbo, second is smart turbo
const ecoplusValueMap = {
  [EcoplusKeyMap.ECOPLUS_OFF]: '0_0',
  [EcoplusKeyMap.ECOPLUS_ON]: '1_0',
  [EcoplusKeyMap.ECO_SMART]: '0_1',
};

const shadowValMap = ObjectHelper.invertObj(ecoplusValueMap);

function getValueKeyByShadowVal(ecoplus, ecoSmart) {
  return `${ecoplus}${DELIMITER}${ecoSmart}`;
}

export default {
  getValueKeyByShadowVal,
  getEcoplusByShadowVal: ({ ecoplus, smartEcoMax }) => {
    const valueKey = getValueKeyByShadowVal(ecoplus, smartEcoMax);

    return shadowValMap[valueKey] || EcoplusKeyMap.ECOPLUS_OFF;
  },
  getShadowValByEcoplus: ecoKey =>
    ecoplusValueMap[ecoKey] || ecoplusValueMap[EcoplusKeyMap.ECOPLUS_OFF],
  getShadowObjByEcoplus: ({
    ecoKey,
    firmwareVersion,
    modeConfig,
    enableCKSwing,
    enableSmartSleep,
    enableEcoplus,
    enableSmartEcomax,
    enableSense,
    isManual,
    manualTemp,
  }) => {
    const modeConfigShadowKey = getModeConfigShadowKeyMap(firmwareVersion);

    const [ecoplus = 0, smartEcoMax = 0] = (
      ecoplusValueMap[ecoKey] || ecoplusValueMap[EcoplusKeyMap.ECOPLUS_OFF]
    ).split(DELIMITER);

    const ecoplusValue = parseInt(ecoplus, 10);
    const smartEcoMaxValue = parseInt(smartEcoMax, 10);

    const needManualTemp =
      !enableEcoplus &&
      !enableSmartEcomax &&
      (ecoplusValue === 1 || smartEcoMaxValue === 1);

    const { ckSwing, sleep, smartSleep, sense } = modeConfig;

    const interlockState = modeConfig
      ? {
          ...(enableCKSwing &&
            ckSwing > 0 && { [modeConfigShadowKey.SET_CK_SWING]: 0 }),
          ...(sleep === 1 && { [modeConfigShadowKey.SET_SLEEP]: 0 }),
          ...(enableSmartSleep &&
            smartSleep === 1 && { [modeConfigShadowKey.SET_SMART_SLEEP]: 0 }),
          ...(enableSense &&
            sense === 1 && { [modeConfigShadowKey.SET_SENSE]: 0 }),
        }
      : {};

    return {
      [modeConfigShadowKey.SET_ECOPLUS]: ecoplusValue,
      [modeConfigShadowKey.SET_SMART_ECOMAX]: smartEcoMaxValue,
      ...(isManual &&
        needManualTemp &&
        manualTemp !== undefined && {
          [modeConfigShadowKey.SET_TEMP]: manualTemp,
        }),
      ...interlockState,
    };
  },
  EcoplusKeyMap,
  EcoplusValueMap: ecoplusValueMap,
};
