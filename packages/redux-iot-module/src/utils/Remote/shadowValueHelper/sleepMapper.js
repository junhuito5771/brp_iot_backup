import { ObjectHelper } from '@module/utility';
import * as SleepKeyMap from '../constants/sleep';
import { getModeConfigShadowKeyMap } from '../shadowStateKeyHelper';

const DELIMITER = '_';

// First digit is sleep, second is smart sleep
const sleepValueMap = {
  [SleepKeyMap.SLEEP_OFF]: '0_0',
  [SleepKeyMap.SLEEP_ON]: '1_0',
  [SleepKeyMap.SLEEP_SMART]: '0_1',
};

const shadowValMap = ObjectHelper.invertObj(sleepValueMap);

function getValueKeyByShadowVal(sleep, smartSleep) {
  return `${sleep}${DELIMITER}${smartSleep}`;
}

export default {
  getValueKeyByShadowVal,
  getSleepByShadowVal: ({ sleep, smartSleep }) => {
    const valueKey = getValueKeyByShadowVal(sleep, smartSleep);

    return shadowValMap[valueKey] || SleepKeyMap.SLEEP_OFF;
  },
  getShadowValBySleep: sleepKey =>
    sleepValueMap[sleepKey] || sleepValueMap[SleepKeyMap.SLEEP_OFF],
  getShadowObjBySleep: ({
    sleepKey,
    firmwareVersion,
    modeConfig,
    enableSense,
    enableEcoplus,
    enableSmartEcomax,
    enableBreeze,
    enableSmartDrift,
  }) => {
    const modeConfigShadowKey = getModeConfigShadowKeyMap(firmwareVersion);

    const [sleep = 0, smartSleep = 0] = (
      sleepValueMap[sleepKey] || sleepValueMap[SleepKeyMap.SLEEO_OFF]
    ).split(DELIMITER);

    const sleepValue = parseInt(sleep, 10);
    const smartSleepValue = parseInt(smartSleep, 10);

    const { sense, ecoplus, smartEcoMax, breeze, smartDrift } = modeConfig;
    const interlockState =
      modeConfig && (sleepValue === 1 || smartSleepValue === 1)
        ? {
            ...(enableSense &&
              sense === 1 && { [modeConfigShadowKey.SET_SENSE]: 0 }),
            ...(enableEcoplus &&
              ecoplus === 1 && { [modeConfigShadowKey.SET_ECOPLUS]: 0 }),
            ...(enableSmartEcomax &&
              smartEcoMax === 1 && {
                [modeConfigShadowKey.SET_SMART_ECOMAX]: 0,
              }),
          }
        : {};

    const smartSleepInterlock =
      modeConfig && smartSleepValue === 1
        ? {
            ...(enableBreeze &&
              breeze === 1 && {
                [modeConfigShadowKey.SET_BREEZE]: 0,
              }),
            ...(enableSmartDrift &&
              smartDrift === 1 && {
                [modeConfigShadowKey.SET_SMART_DRIFT]: 0,
              }),
          }
        : {};

    return {
      [modeConfigShadowKey.SET_SLEEP]: sleepValue,
      [modeConfigShadowKey.SET_SMART_SLEEP]: smartSleepValue,
      ...interlockState,
      ...smartSleepInterlock,
    };
  },
  SleepKeyMap,
  SleepValueMap: sleepValueMap,
};
