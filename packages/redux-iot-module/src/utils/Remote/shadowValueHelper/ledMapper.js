import { ObjectHelper } from '@module/utility';
import * as LEDKeyMap from '../constants/led';
import { getModeConfigShadowKeyMap } from '../shadowStateKeyHelper';

const DELIMITER = '_';

// First digit is LED, second is Power Indication
const ledValueMap = {
  [LEDKeyMap.LED_OFF]: '1_0',
  [LEDKeyMap.LED_ON]: '0_0',
  [LEDKeyMap.POWER_IND]: '0_1',
};

const shadowValMap = ObjectHelper.invertObj(ledValueMap);

function getLEDValueKeyByShadowVal(led, powerInd) {
  return `${led}${DELIMITER}${powerInd}`;
}

export default {
  getLEDValueKeyByShadowVal,
  getLEDByShadowVal: ({
    led,
    powerInd,
    enableLED = true,
    enablePowerInd = true,
  }) => {
    const curLED = enableLED ? led : '0';
    const curPowerInd = enablePowerInd ? powerInd : '0';
    const valueKey = getLEDValueKeyByShadowVal(curLED, curPowerInd);

    return shadowValMap[valueKey] || ledValueMap.LED_OFF;
  },
  getShadowValByLED: ledKey =>
    ledValueMap[ledKey] || ledValueMap[LEDKeyMap.LED_OFF],
  getShadowObjByLED: ({
    ledKey,
    firmwareVersion,
    modeConfig,
    enableCKSwing,
  }) => {
    const modeConfigShadowKey = getModeConfigShadowKeyMap(firmwareVersion);

    const [led = 0, powerInd = 0] = (
      ledValueMap[ledKey] || ledValueMap[LEDKeyMap.LED_OFF]
    ).split(DELIMITER);

    const ledValue = parseInt(led, 10);

    const { ckSwing } = modeConfig;

    return {
      [modeConfigShadowKey.SET_LED]: ledValue,
      [modeConfigShadowKey.SET_POWER_IND]: parseInt(powerInd, 10),
      ...(modeConfig &&
        ledValue === 0 &&
        enableCKSwing &&
        ckSwing > 0 && { [modeConfigShadowKey.SET_CK_SWING]: 0 }),
    };
  },
};
