import { ObjectHelper } from '@module/utility';
import * as SwingKeyMap from '../constants/swing';
import { getModeConfigShadowKeyMap } from '../shadowStateKeyHelper';
import { SET_LRLVR } from '../constants/configShadowKey';

const lrSwingValueMap = {
  [SwingKeyMap.SWING_LR_OFF]: '0',
  [SwingKeyMap.SWING_LR_FULL]: '15',
  [SwingKeyMap.SWING_LR_LV1]: '1',
  [SwingKeyMap.SWING_LR_LV2]: '2',
  [SwingKeyMap.SWING_LR_LV3]: '3',
  [SwingKeyMap.SWING_LR_LV4]: '4',
  [SwingKeyMap.SWING_LR_LV5]: '5',
};

const shadowValMap = ObjectHelper.invertObj(lrSwingValueMap);

export default {
  getSwingLRByShadowVal: ({ leftRightSwing }) =>
    shadowValMap[leftRightSwing] || SwingKeyMap.SWING_LR_OFF,
  getShadowValBySwingLR: lrSwingKey =>
    lrSwingValueMap[lrSwingKey] || lrSwingValueMap[SwingKeyMap.SWING_LR_OFF],
  getShadowObjBySwingLR: ({ lrSwingKey, modeConfig, firmwareVersion }) => {
    const modeConfigShadowKey = getModeConfigShadowKeyMap(firmwareVersion);
    const value =
      lrSwingValueMap[lrSwingKey] || lrSwingValueMap[SwingKeyMap.SWING_LR_OFF];

    const { smartSleep, smartDrift, smartTurbo } = modeConfig;

    const sharedInterlock = {
      ...(smartSleep === 1 && { [modeConfigShadowKey.SET_SMART_SLEEP]: 0 }),
      ...(smartDrift === 1 && { [modeConfigShadowKey.SET_SMART_DRIFT]: 0 }),
      ...(smartTurbo === 1 && { [modeConfigShadowKey.SET_SMART_TURBO]: 0 }),
    };

    const lrValue = parseInt(value, 10);

    return {
      [modeConfigShadowKey[SET_LRLVR]]: lrValue,
      ...(lrValue > 0 && sharedInterlock),
    };
  },
};
