import { ObjectHelper } from '@module/utility';
import * as CKSwingKeyMap from '../constants/ckSwing';
import { getModeConfigShadowKeyMap } from '../shadowStateKeyHelper';

const CKSwingValueMap = {
  [CKSwingKeyMap.CKSWING_FULL]: 0,
  [CKSwingKeyMap.CKSWING_DRAFT]: 1,
  [CKSwingKeyMap.CKSWING_SOIL]: 2,
};

const shadowValMap = ObjectHelper.invertObj(CKSwingValueMap);

export default {
  getCKSwingByShadowVal: ({ ckSwing }) =>
    shadowValMap[ckSwing] || CKSwingKeyMap.CKSWING_FULL,
  getShadowValByCKSwing: ckSwingKey =>
    CKSwingValueMap[ckSwingKey] || CKSwingValueMap[CKSwingKeyMap.CKSWING_FULL],
  getShadowObjByCKSwing: ({ ckSwingKey, firmwareVersion, modeConfig }) => {
    const modeConfigShadowKey = getModeConfigShadowKeyMap(firmwareVersion);

    const {
      sense,
      led,
      ecoplus,
      breeze,
      smartSleep,
      smartDrift,
      smartTurbo,
      smartEcoMax,
      streamer,
    } = modeConfig;

    const ckSwingValue =
      CKSwingValueMap[ckSwingKey] ||
      CKSwingValueMap[CKSwingKeyMap.CKSWING_FULL];

    const interlockState = {
      ...(sense === 1 && {
        [modeConfigShadowKey.SET_SENSE]: 0,
      }),
      ...(led === 1 && {
        [modeConfigShadowKey.SET_LED]: 0,
      }),
      ...(ecoplus === 1 && {
        [modeConfigShadowKey.SET_ECOPLUS]: 0,
      }),
      ...(breeze === 1 && {
        [modeConfigShadowKey.SET_BREEZE]: 0,
      }),
      ...(smartSleep === 1 && {
        [modeConfigShadowKey.SET_SMART_SLEEP]: 0,
      }),
      ...(smartDrift === 1 && {
        [modeConfigShadowKey.SET_SMART_DRIFT]: 0,
      }),
      ...(smartTurbo === 1 && {
        [modeConfigShadowKey.SET_SMART_TURBO]: 0,
      }),
      ...(smartEcoMax === 1 && {
        [modeConfigShadowKey.SET_SMART_ECOMAX]: 0,
      }),
      ...(streamer === 1 && {
        [modeConfigShadowKey.SET_STREAMER]: 0,
      }),
    };

    return {
      [modeConfigShadowKey.SET_CK_SWING]: parseInt(ckSwingValue, 10),
      ...(parseInt(ckSwingValue, 10) > 0 && interlockState),
    };
  },
};
