import { ObjectHelper } from '@module/utility';
import * as SwingKeyMap from '../constants/swing';
import { getModeConfigShadowKeyMap } from '../shadowStateKeyHelper';

const DELIMITER = '_';

// First digit is turbo, second is smart turbo
const swingValueMap = {
  [SwingKeyMap.SWING_OFF]: '0',
  [SwingKeyMap.SWING_ON]: '1',
};

const udSwingValueMap = {
  [SwingKeyMap.SWING_OFF]: '0',
  [SwingKeyMap.SWING_ON]: '1',
  [SwingKeyMap.SWING_UD_LV1]: '0_1',
  [SwingKeyMap.SWING_UD_LV2]: '0_2',
  [SwingKeyMap.SWING_UD_LV3]: '0_3',
  [SwingKeyMap.SWING_UD_LV4]: '0_4',
  [SwingKeyMap.SWING_UD_LV5]: '0_5',
  [SwingKeyMap.SWING_UD_FULL]: '1_15',
  [SwingKeyMap.SWING_UD_OFF]: '0_0',
};

const shadowValMap = ObjectHelper.invertObj(swingValueMap);
const shadowValUpDownMap = ObjectHelper.invertObj(udSwingValueMap);

export default {
  getSwingByShadowVal: ({ swing, upDownSwing, enableUDStep }) => {
    const upDownAllowed = upDownSwing !== undefined && enableUDStep;
    const curShadowValueMap = upDownAllowed ? shadowValUpDownMap : shadowValMap;

    const swingValueKey = upDownAllowed ? `${swing}_${upDownSwing}` : swing;

    return curShadowValueMap[swingValueKey] || SwingKeyMap.SWING_OFF;
  },
  getShadowValBySwing: turboKey =>
    swingValueMap[turboKey] || swingValueMap[SwingKeyMap.SWING_OFF],
  getShadowObjBySwing: ({
    swingValKey,
    enableUDStep,
    modeConfig,
    firmwareVersion,
  }) => {
    const modeConfigShadowKey = getModeConfigShadowKeyMap(firmwareVersion);
    const defaultValue = enableUDStep
      ? udSwingValueMap[SwingKeyMap.SWING_UD_OFF]
      : udSwingValueMap[SwingKeyMap.SWING_OFF];
    const value = udSwingValueMap[swingValKey] || defaultValue;

    const [swing = 0, udSwing = 0] = value.split(DELIMITER);

    const { breeze, smartSleep, smartDrift, smartTurbo } = modeConfig;

    const sharedInterlock = {
      ...(breeze === 1 && { [modeConfigShadowKey.SET_BREEZE]: 0 }),
      ...(smartSleep === 1 && { [modeConfigShadowKey.SET_SMART_SLEEP]: 0 }),
      ...(smartDrift === 1 && { [modeConfigShadowKey.SET_SMART_DRIFT]: 0 }),
      ...(smartTurbo === 1 && { [modeConfigShadowKey.SET_SMART_TURBO]: 0 }),
    };

    const udSwingValue = parseInt(udSwing, 10);
    const swingValue = parseInt(swing, 10);

    return {
      [modeConfigShadowKey.SET_UDLVR]: udSwingValue,
      [modeConfigShadowKey.SET_SWING]: swingValue,
      ...((udSwingValue > 0 || swingValue > 0) && sharedInterlock),
    };
  },
  SwingKeyMap,
  SwingValueMap: swingValueMap,
};
