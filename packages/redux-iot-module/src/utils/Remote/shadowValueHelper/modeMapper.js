import { ObjectHelper } from '@module/utility';
import * as ModeKeyMap from '../constants/mode';

const modeValueMap = {
  [ModeKeyMap.MODE_COOL]: '1',
  [ModeKeyMap.MODE_FAN]: '2',
  [ModeKeyMap.MODE_DRY]: '4',
  [ModeKeyMap.MODE_HEAT]: '8',
  [ModeKeyMap.MODE_AUTO]: '10',
};

const shadowValMap = ObjectHelper.invertObj(modeValueMap);

export default {
  getModeByShadowVal: mode => shadowValMap[mode] || shadowValMap['1'],
  getShadowValByMode: modeKey =>
    parseInt(modeValueMap[modeKey], 10) ||
    parseInt(modeValueMap[ModeKeyMap.MODE_COOL], 10),
  ModeKeyMap,
  ModeValueeMap: modeValueMap,
};
