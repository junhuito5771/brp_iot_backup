import { ObjectHelper } from '@module/utility';
import {
  FANSPEED_AUTO,
  FANSPEED_LV1,
  FANSPEED_LV2,
  FANSPEED_LV3,
  FANSPEED_LV4,
  FANSPEED_LV5,
  FANSPEED_SILENT,
  FANSPEED_OFF,
  FANSPEED_AUTO_OFF,
} from '../constants/fanSpeed';
import { MODE_FAN, MODE_AUTO, MODE_DRY } from '../constants/mode';
import { HANDSET_DIL } from '../constants/handsetType';

const DELIMITER = '_';

const fanLvlMapDIL = {
  [FANSPEED_AUTO]: '128',
  [FANSPEED_LV1]: '2_0',
  [FANSPEED_LV2]: '2_1',
  [FANSPEED_LV3]: '4_0',
  [FANSPEED_LV4]: '8_1',
  [FANSPEED_LV5]: '8_0',
};

const fanLvlMapDAMA = {
  [FANSPEED_AUTO]: '128',
  [FANSPEED_LV1]: '2',
  [FANSPEED_LV2]: '4',
  [FANSPEED_LV3]: '8',
};

const shadowValMapDIL = ObjectHelper.invertObj(fanLvlMapDIL);
const shadowValMapDAMA = ObjectHelper.invertObj(fanLvlMapDAMA);

const getFanLvMapByHandset = handsetType =>
  handsetType === 1 ? fanLvlMapDIL : fanLvlMapDAMA;

const getFanShadowValMapByHandset = handsetType =>
  handsetType === 1 ? shadowValMapDIL : shadowValMapDAMA;

const getFanShadowValKeyByHandset = (handsetType, fan, fanExtend) =>
  handsetType === 1 ? `${fan}_${fanExtend}` : `${fan}`;

export default {
  getFanLvMapByHandset,
  getFanShadowValMapByHandset,
  getFanShadowValKeyByHandset,
  getDefaultFanLvByHandset: (handsetType, modeValue) => {
    if (handsetType === HANDSET_DIL) {
      return FANSPEED_AUTO;
    }

    switch (modeValue) {
      case MODE_AUTO:
      case MODE_FAN:
        return FANSPEED_LV1;
      default:
        return FANSPEED_AUTO;
    }
  },
  getFanLvlByShadowVal: (handsetType, fan, fanExtend, modeConfig, mode) => {
    const { silent, turbo, smartTurbo } = modeConfig || {};
    const interStateOff = turbo === 1 || smartTurbo === 1;
    const shadowValKey = getFanShadowValKeyByHandset(
      handsetType,
      fan,
      fanExtend
    );
    const shadowValMap = getFanShadowValMapByHandset(handsetType);

    if (interStateOff && shadowValMap[shadowValKey] === FANSPEED_AUTO) {
      return FANSPEED_AUTO_OFF;
    }
    if (interStateOff) {
      return FANSPEED_OFF;
    }

    if (mode === MODE_DRY) return FANSPEED_AUTO;
    if (silent === 1) return FANSPEED_SILENT;

    return shadowValMap[shadowValKey] || MODE_AUTO;
  },
  getShadowValByFanLvl: (handsetType, fanLv) => {
    const fanLvlMap = getFanLvMapByHandset(handsetType);

    return fanLvlMap[fanLv] || fanLvlMap.auto;
  },
  getShadowObjByFanLvl: (handsetType, fanLv) => {
    const fanLvlMap = getFanLvMapByHandset(handsetType);
    const [fan, fanExtend = 0] = (fanLvlMap[fanLv] || fanLvlMap.auto).split(
      DELIMITER
    );

    return {
      fan: parseInt(fan, 10),
      fanExtend: parseInt(fanExtend, 10),
    };
  },
};
