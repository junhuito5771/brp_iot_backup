// import { ShadowValueHelper, ShadowStateKeyHelper } from '@module/utility';
import ShadowValueHelper from './Remote/shadowValueHelper';
import {
  checkIsV1,
  getGeneralStateKey,
  getModeConfigStateKey,
  getModeEnablementMap,
  getModeConfigShadowKeyMap,
} from './Remote/shadowStateKeyHelper';

/* eslint-disable no-param-reassign */
export default (shadowData, prevShadowData = {}, version) => {
  const firmwareVersion = shadowData.version || prevShadowData.firmwareVersion;
  const configKey = getModeConfigShadowKeyMap(firmwareVersion);
  const MODE_CONFIG_DEFAULT = {
    [configKey.SET_FAN]: 8,
    [configKey.SET_MODE]: 1,
    [configKey.SET_TEMP]: 24,
    [configKey.SET_SWING]: 0,
  };

  function useDefaultIfNeeded(key, value) {
    if (MODE_CONFIG_DEFAULT[key] !== undefined && value === 0) {
      return MODE_CONFIG_DEFAULT[key];
    }

    return value;
  }

  const keys = Object.keys(shadowData);
  const currentModeNr = shadowData.Set_Mode;

  const EnablementKey = checkIsV1(firmwareVersion) ? 'Enable_' : 'Ena_';

  // BLE shadow state doesn't return us the full shadow,
  // hence, if there is no SET_MODE, meaning there is no mode changed
  const currentMode = currentModeNr
    ? ShadowValueHelper.getModeByShadowVal(currentModeNr)
    : prevShadowData.mode;

  // Initialize the modeEnableList if needed
  let modeEnableList = prevShadowData.modes;
  if (!modeEnableList) {
    const modeEnablement = getModeEnablementMap(firmwareVersion);
    Object.entries(modeEnablement).forEach(([shadowKey, stateKey]) => {
      const modeValue = shadowData[shadowKey];
      if (Number.isInteger(modeValue)) {
        modeEnableList = {
          ...modeEnableList,
          [stateKey]: modeValue,
        };
      }
    });
  }

  const shadowProps = keys.reduce(
    (acc, key) => {
      const generalKey = getGeneralStateKey(firmwareVersion, key);
      const modeConfigKey = getModeConfigStateKey(firmwareVersion, key);
      if (generalKey) {
        if (key.startsWith(EnablementKey)) {
          acc[generalKey] = Boolean(shadowData[key]);
        } else if (key.startsWith('Bar')) {
          acc[generalKey] = shadowData[key] === 0;
        } else if (key === 'Inf_ODPwrCon') {
          acc[generalKey] = Boolean(shadowData[key]);
        } else if (key === 'Set_Mode') {
          acc[generalKey] = currentMode;
        } else {
          acc[generalKey] = shadowData[key];
        }
      } else if (modeConfigKey) {
        acc.modeConfig = {
          ...acc.modeConfig,
          [modeConfigKey]: useDefaultIfNeeded(key, shadowData[key]),
        };
      }
      return acc;
    },
    {
      ...prevShadowData,
      modes: modeEnableList,
      modeConfig: {
        ...prevShadowData.modeConfig,
      },
    }
  );

  if (version) {
    shadowProps.version = version;
  }

  if (shadowProps.enableEnergyConsumption === undefined) {
    shadowProps.enableEnergyConsumption = true;
  }

  return shadowProps;
};
/* eslint-enable no-param-reassign */
