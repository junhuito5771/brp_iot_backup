const features = {
  smartEcomax: {
    title: 'What’s Smart Ecomax ?',
    desc:
      'Detect human presence in room while adjusting set temperature to an eco-friendly level and limiting the maximum power consumption.',
  },
  ecoplus: {
    title: 'What’s Eco + ?',
    desc:
      'Adjusts temperature to an eco-friendly level to ensure optimum energy consumption.',
  },
  ecoNo: {
    title: 'What’s Econo Mode ?',
    desc:
      'Adjusts temperature to an eco-friendly level to ensure optimum energy consumption.',
  },
  smartPowerful: {
    title: 'What’s Smart Powerful + ?',
    desc:
      'Delivers 20 minutes maximum cooling while Automatically directs cooling towards detected human presence area',
  },
  powerful: {
    title: 'What’s Powerful ?',
    desc: 'Delivers 20 minutes maximum cooling.',
  },
  smartSleep: {
    title: 'What’s Smart Sleep + ?',
    desc:
      'Adjusts temperature based on sleeping temperature pattern, prevents uncomfortable drafts and dims LED display intensity',
  },
  sleep: {
    title: 'What’s Sleep ?',
    desc: 'Adjusts temperature based on sleeping temperature pattern.',
  },
  airFlow3D: {
    title: 'What’s 3D Airflow ?',
    desc:
      'Both up-down flap and left-right flap will run in auto-swing for uniform cooling.',
  },
  breeze: {
    title: 'What’s Breeze Airflow ?',
    desc:
      'Recreates the natural pattern of a gentle breeze with random fan speed and up down swing.',
  },
  smartDrift: {
    title: 'What’s Smart Drift ?',
    desc:
      'Prevents uncomfortable draft by setting fan swing in an upward position',
  },
  streamer: {
    title: 'What’s Streamer ?',
    desc:
      'Activates high speed electrons that enable decomposition and removal of allergens such as mold, mites, pollen and hazardous chemical.',
  },
  sense: {
    title: 'What’s Sense ?',
    desc:
      'Adjusts temperature, fan speed and air-flow direction according to the detected level of human activity',
  },
  led: {
    title: 'What’s LED ?',
    desc: 'Turns off LED display on unit.',
  },
  powerInd: {
    title: 'What’s Power Usage Indication ?',
    desc:
      'Power consumption status of unit will be reflected by LED display color.',
  },
  ckSwing: {
    title: 'What’s CK Swing ?',
    desc:
      'Standard: Louver is set to swing at the maximun angle for gentle drafts.\n\nDraft Prevention Setting: With the aid of Coanda effect, direct draft which may lead to disconfort can be avoid.\n\nSoil Prevention Setting: Even distribution of cooling whilst ensuring ceiling to be kept spotless.',
  },
};

export default function (key) {
  return features[key] || {};
}
