import * as FanSpdIcons from './fanSpeed';
import * as TurboIcons from './turbo';
import * as EcoplusIcons from './ecoplus';
import * as SleepIcons from './sleep';
import * as SwingIcons from './swing';
import * as BreezeIcons from './breeze';
import * as SmartDriftIcons from './smartDrift';
import * as StreamerIcons from './streamer';
import * as SenseIcons from './sense';
import * as ledIcons from './led';
import * as ckSwingIcons from './ckSwing';

export {
  FanSpdIcons,
  TurboIcons,
  EcoplusIcons,
  SleepIcons,
  SwingIcons,
  BreezeIcons,
  SmartDriftIcons,
  StreamerIcons,
  SenseIcons,
  ledIcons,
  ckSwingIcons,
};
