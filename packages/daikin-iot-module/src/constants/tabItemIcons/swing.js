export const SWING_ON = require('../../shared/assets/remote/airFlowDirectionOnBlue.png');
export const SWING_OFF = require('../../shared/assets/remote/airFlowDirectionOffBlue.png');

export const SWING_UD_LV1 = require('../../shared/assets/remote/SwingUDLv1On.png');
export const SWING_UD_LV2 = require('../../shared/assets/remote/SwingUDLv2On.png');
export const SWING_UD_LV3 = require('../../shared/assets/remote/SwingUDLv3On.png');
export const SWING_UD_LV4 = require('../../shared/assets/remote/SwingUDLv4On.png');
export const SWING_UD_LV5 = require('../../shared/assets/remote/SwingUDLv5On.png');
export const SWING_UD_FULL = require('../../shared/assets/remote/SwingUDFullOn.png');
export const SWING_UD_OFF = require('../../shared/assets/remote/SwingUDFullOff.png');

export const SWING_LR_LV1 = require('../../shared/assets/remote/SwingLRLv1On.png');
export const SWING_LR_LV2 = require('../../shared/assets/remote/SwingLRLv2On.png');
export const SWING_LR_LV3 = require('../../shared/assets/remote/SwingLRLv3On.png');
export const SWING_LR_LV4 = require('../../shared/assets/remote/SwingLRLv4On.png');
export const SWING_LR_LV5 = require('../../shared/assets/remote/SwingLRLv5On.png');
export const SWING_LR_FULL = require('../../shared/assets/remote/SwingLRFullOn.png');
export const SWING_LR_OFF = require('../../shared/assets/remote/SwingLRFullOff.png');

export const SWING_3D_ON = require('../../shared/assets/remote/Swing3DOn.png');
export const SWING_3D_OFF = require('../../shared/assets/remote/Swing3DOff.png');
