export const ECOPLUS_ON = require('../../shared/assets/remote/Eco+_blue.png');
export const ECOPLUS_OFF = require('../../shared/assets/remote/Eco+_grey.png');

export const ECOPLUS_SMART_ON = require('../../shared/assets/remote/ecoSmartOn.png');
export const ECOPLUS_SMART_OFF = require('../../shared/assets/remote/ecoSmartOff.png');

export const ECONO_ON = require('../../shared/assets/remote/ecoNoOn.png');
export const ECONO_OFF = require('../../shared/assets/remote/ecoNoOff.png');
