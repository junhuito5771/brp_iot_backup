export const SLEEP_ON = require('../../shared/assets/remote/sleepmoodBlue.png');
export const SLEEP_OFF = require('../../shared/assets/remote/sleepmoodDarkGrey.png');
export const SLEEP_SMART_ON = require('../../shared/assets/remote/smartSleepOn.png');
export const SLEEP_SMART_OFF = require('../../shared/assets/remote/smartSleepOff.png');
