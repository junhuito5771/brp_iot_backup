export const LED_ON = require('../../shared/assets/remote/ledOn.png');
export const LED_OFF = require('../../shared/assets/remote/ledOff.png');
export const POWER_IND_ON = require('../../shared/assets/remote/powerIndicatorOn.png');
export const POWER_IND_OFF = require('../../shared/assets/remote/powerIndicatorOff.png');
