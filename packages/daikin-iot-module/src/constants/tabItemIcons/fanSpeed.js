export const FAN_SILENT = require('../../shared/assets/remote/indoorUnitQuietMoodBlue.png');
export const FAN_SILENT_OFF = require('../../shared/assets/remote/indoorUnitQuietMoodDarkGrey.png');
export const FAN_AUTO = require('../../shared/assets/remote/automaticFanBlue.png');
export const FAN_AUTO_OFF = require('../../shared/assets/remote/automaticFanOff.png');
export const FAN_DIL_LV1 = require('../../shared/assets/remote/fanDILLv1.png');
export const FAN_DIL_LV2 = require('../../shared/assets/remote/fanDILLv2.png');
export const FAN_DIL_LV3 = require('../../shared/assets/remote/fanDILLv3.png');
export const FAN_DIL_LV4 = require('../../shared/assets/remote/fanDILLv4.png');
export const FAN_DIL_LV5 = require('../../shared/assets/remote/fanDILLv5.png');
export const FAN_DIL_OFF = require('../../shared/assets/remote/fanDILOff.png');

export const FAN_DAMA_LV1 = require('../../shared/assets/remote/bigFanMoodLevel1.png');
export const FAN_DAMA_LV2 = require('../../shared/assets/remote/bigFanMoodLevel2.png');
export const FAN_DAMA_LV3 = require('../../shared/assets/remote/bigFanMoodLevel3.png');
export const FAN_DAMA_OFF = require('../../shared/assets/remote/bigFanMoodOff.png');
