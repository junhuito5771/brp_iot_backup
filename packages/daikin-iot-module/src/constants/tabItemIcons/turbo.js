export const TURBO_ON = require('../../shared/assets/remote/turbomoondBlue.png');
export const TURBO_OFF = require('../../shared/assets/remote/turbomoondDarkGrey.png');
export const TURBO_SMART_ON = require('../../shared/assets/remote/smartTurboBlue.png');
export const TURBO_SMART_OFF = require('../../shared/assets/remote/smartTurboGrey.png');
