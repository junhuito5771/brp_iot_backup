const fanSpeedIcons = {
  off: {
    label: 'Off',
    value: 'off',
    icons: [
      require('../shared/assets/remote/automaticDarkGrey.png'),
      require('../shared/assets/remote/automaticBlue.png'),
    ],
    tabIcon: require('../shared/assets/remote/bigFanMoodOff.png'),
  },
  auto: {
    label: 'Auto',
    value: 'auto',
    icons: [
      require('../shared/assets/remote/automaticDarkGrey.png'),
      require('../shared/assets/remote/automaticBlue.png'),
    ],
    tabIcon: require('../shared/assets/remote/automaticFanBlue.png'),
  },
  silent: {
    label: 'Quiet',
    value: 'silent',
    icons: [
      require('../shared/assets/remote/indoorUnitQuietDarkGrey.png'),
      require('../shared/assets/remote/indoorUnitQuietBlue.png'),
    ],
    tabIcon: require('../shared/assets/remote/indoorUnitQuietMoodBlue.png'),
  },
  level1: {
    label: 'Level 1',
    value: 'level1',
    icons: [
      require('../shared/assets/remote/fanSpeedLevel1Off.png'),
      require('../shared/assets/remote/fanSpeedLevel1On.png'),
    ],
    tabIcon: require('../shared/assets/remote/bigFanMoodLevel1.png'),
  },
  level2: {
    label: 'Level 2',
    value: 'level2',
    icons: [
      require('../shared/assets/remote/fanSpeedLevel2Off.png'),
      require('../shared/assets/remote/fanSpeedLevel2On.png'),
    ],
    tabIcon: require('../shared/assets/remote/bigFanMoodLevel2.png'),
  },
  level3: {
    label: 'Level 3',
    value: 'level3',
    icons: [
      require('../shared/assets/remote/fanSpeedLevel3Off.png'),
      require('../shared/assets/remote/fanSpeedLevel3On.png'),
    ],
    tabIcon: require('../shared/assets/remote/bigFanMoodLevel3.png'),
  },
  level4: {
    label: 'Level 4',
    value: 'level4',
    icons: [
      require('../shared/assets/remote/fanSpeedLevel4Off.png'),
      require('../shared/assets/remote/fanSpeedLevel4On.png'),
    ],
    tabIcon: require('../shared/assets/remote/bigFanMoodLevel4.png'),
  },
  level5: {
    label: 'Level 5',
    value: 'level5',
    icons: [
      require('../shared/assets/remote/fanSpeedLevel5Off.png'),
      require('../shared/assets/remote/fanSpeedLevel5On.png'),
    ],
    tabIcon: require('../shared/assets/remote/bigFanMoodLevel5.png'),
  },
};

export default fanSpeedIcons;

export const fanSpeedTabIconsDIL = {
  ...fanSpeedIcons,
  off: {
    ...fanSpeedIcons.off,
    tabIcon: require('../shared/assets/remote/fanDILOff.png'),
  },
  level1: {
    ...fanSpeedIcons.level1,
    tabIcon: require('../shared/assets/remote/fanDILLv1.png'),
  },
  level2: {
    ...fanSpeedIcons.level2,
    tabIcon: require('../shared/assets/remote/fanDILLv2.png'),
  },
  level3: {
    ...fanSpeedIcons.level3,
    tabIcon: require('../shared/assets/remote/fanDILLv3.png'),
  },
  level4: {
    ...fanSpeedIcons.level4,
    tabIcon: require('../shared/assets/remote/fanDILLv4.png'),
  },
  level5: {
    ...fanSpeedIcons.level5,
    tabIcon: require('../shared/assets/remote/fanDILLv5.png'),
  },
};
