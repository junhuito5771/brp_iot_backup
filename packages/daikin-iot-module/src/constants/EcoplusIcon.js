export default {
  0: {
    label: 'Off',
    value: 0,
    icons: [
      require('../shared/images/remote/offDarkGrey.png'),
      require('../shared/images/remote/offBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/Eco+_grey.png'),
  },
  1: {
    label: 'Eco+',
    value: 1,
    icons: [
      require('../shared/images/remote/Eco+_grey.png'),
      require('../shared/images/remote/Eco+_blue.png'),
    ],
    tabIcon: require('../shared/images/remote/Eco+_blue.png'),
  },
};
