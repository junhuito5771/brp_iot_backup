import moment from 'moment';

export const PERIOD_DAILY = 'daily';
export const PERIOD_WEEKLY = 'weekly';
export const PERIOD_MONTHLY = 'monthly';
export const PERIOD_YEARLY = 'yearly';

export const STA_TEMP = 'temp';
export const STA_ENERGY = 'energy';

export const HOURS = [
  '12am',
  '1am',
  '2am',
  '3am',
  '4am',
  '5am',
  '6am',
  '7am',
  '8am',
  '9am',
  '10am',
  '11am',
  '12pm',
  '1pm',
  '2pm',
  '3pm',
  '4pm',
  '5pm',
  '6pm',
  '7pm',
  '8pm',
  '9pm',
  '10pm',
  '11pm',
];

export const DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];

export const MONTHS = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];

export const getDayNr = date => {
  const endDayNr = Number(moment(date).endOf('months').format('DD'));

  const result = [];

  for (let i = 1; i <= endDayNr; i += 1) {
    result.push(i.toString());
  }

  return result;
};
