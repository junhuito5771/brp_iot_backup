export default {
  0: {
    label: 'Off',
    value: 0,
    icons: [
      require('../shared/assets/remote/offDarkGrey.png'),
      require('../shared/assets/remote/offBlue.png'),
    ],
    tabIcon: require('../shared/assets/remote/indoorUnitQuietMoodDarkGrey.png'),
  },
  1: {
    label: 'Quiet',
    value: 1,
    icons: [
      require('../shared/assets/remote/indoorUnitQuietDarkGrey.png'),
      require('../shared/assets/remote/indoorUnitQuietBlue.png'),
    ],
    tabIcon: require('../shared/assets/remote/indoorUnitQuietMoodBlue.png'),
  },
};
