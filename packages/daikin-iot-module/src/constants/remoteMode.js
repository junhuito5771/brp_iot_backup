export default {
  COOL_MODE: {
    name: 'Cool',
    icons: [
      require('../shared/images/remote/coolMoodOff.png'),
      require('../shared/images/remote/coolMoodOn.png'),
    ],
  },
  FAN_MODE: {
    name: 'Fan',
    icons: [
      require('../shared/images/remote/fanModeDarkGrey.png'),
      require('../shared/images/remote/fanModeGreen.png'),
    ],
  },
  DRY_MODE: {
    name: 'Dry',
    icons: [
      require('../shared/images/remote/humidityMoodDarkGrey.png'),
      require('../shared/images/remote/humidityMoodBlue.png'),
    ],
  },
  HEAT_MODE: {
    name: 'Heat',
    icons: [
      require('../shared/images/remote/coolMoodOff.png'),
      require('../shared/images/remote/coolMoodOn.png'),
    ],
  },
  AUTO_MODE: {
    name: 'Auto',
    icons: [
      require('../shared/images/remote/coolMoodOff.png'),
      require('../shared/images/remote/coolMoodOn.png'),
    ],
  },
};
