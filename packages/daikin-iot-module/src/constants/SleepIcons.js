export default {
  0: {
    label: 'Off',
    value: 0,
    icons: [
      require('../shared/images/remote/offDarkGrey.png'),
      require('../shared/images/remote/offBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/sleepmoodDarkGrey.png'),
  },
  1: {
    label: 'Sleep',
    value: 1,
    icons: [
      require('../shared/images/remote/sleepmoodDarkGrey.png'),
      require('../shared/images/remote/sleepmoodBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/sleepmoodBlue.png'),
  },
};
