export const SWING_OFF_ACT = require('../../shared/assets/remote/airFlowDirectionOffBlue.png');
export const SWING_OFF_DEACT = require('../../shared/assets/remote/airFlowDirectionOffDarkGrey.png');

export const SWING_ON_ACT = require('../../shared/assets/remote/airFlowDirectionOnBlue.png');
export const SWING_ON_DEACT = require('../../shared/assets/remote/airFlowDirectionOnDarkGrey.png');

export const SWING_UD_OFF_ACT = require('../../shared/assets/remote/offBlue.png');
export const SWING_UD_OFF_DEACT = require('../../shared/assets/remote/offDarkGrey.png');

export const SWING_UD_FULL_ACT = require('../../shared/assets/remote/SwingUDFullOn.png');
export const SWING_UD_FULL_DEACT = require('../../shared/assets/remote/SwingUDFullOff.png');

export const SWING_UD_LV1_ACT = require('../../shared/assets/remote/SwingUDLv1On.png');
export const SWING_UD_LV1_DEACT = require('../../shared/assets/remote/SwingUDLv1Off.png');

export const SWING_UD_LV2_ACT = require('../../shared/assets/remote/SwingUDLv2On.png');
export const SWING_UD_LV2_DEACT = require('../../shared/assets/remote/SwingUDLv2Off.png');

export const SWING_UD_LV3_ACT = require('../../shared/assets/remote/SwingUDLv3On.png');
export const SWING_UD_LV3_DEACT = require('../../shared/assets/remote/SwingUDLv3Off.png');

export const SWING_UD_LV4_ACT = require('../../shared/assets/remote/SwingUDLv4On.png');
export const SWING_UD_LV4_DEACT = require('../../shared/assets/remote/SwingUDLv4Off.png');

export const SWING_UD_LV5_ACT = require('../../shared/assets/remote/SwingUDLv5On.png');
export const SWING_UD_LV5_DEACT = require('../../shared/assets/remote/SwingUDLv5Off.png');

export const SWING_LR_FULL_ACT = require('../../shared/assets/remote/SwingLRFullOn.png');
export const SWING_LR_FULL_DEACT = require('../../shared/assets/remote/SwingLRFullOff.png');

export const SWING_LR_LV1_ACT = require('../../shared/assets/remote/SwingLRLv1On.png');
export const SWING_LR_LV1_DEACT = require('../../shared/assets/remote/SwingLRLv1Off.png');

export const SWING_LR_LV2_ACT = require('../../shared/assets/remote/SwingLRLv2On.png');
export const SWING_LR_LV2_DEACT = require('../../shared/assets/remote/SwingLRLv2Off.png');

export const SWING_LR_LV3_ACT = require('../../shared/assets/remote/SwingLRLv3On.png');
export const SWING_LR_LV3_DEACT = require('../../shared/assets/remote/SwingLRLv3Off.png');

export const SWING_LR_LV4_ACT = require('../../shared/assets/remote/SwingLRLv4On.png');
export const SWING_LR_LV4_DEACT = require('../../shared/assets/remote/SwingLRLv4Off.png');

export const SWING_LR_LV5_ACT = require('../../shared/assets/remote/SwingLRLv5On.png');
export const SWING_LR_LV5_DEACT = require('../../shared/assets/remote/SwingLRLv5Off.png');

export const SWING_3D_ACT = require('../../shared/assets/remote/Swing3DOn.png');
export const SWING_3D_DEACT = require('../../shared/assets/remote/Swing3DOff.png');

export const SWING_3D_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const SWING_3D_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const SWING_UD_FULL = [SWING_UD_FULL_DEACT, SWING_UD_FULL_ACT];
export const SWING_UD_OFF = [SWING_UD_OFF_DEACT, SWING_UD_OFF_ACT];
export const SWING_UD_LV1 = [SWING_UD_LV1_DEACT, SWING_UD_LV1_ACT];
export const SWING_UD_LV2 = [SWING_UD_LV2_DEACT, SWING_UD_LV2_ACT];
export const SWING_UD_LV3 = [SWING_UD_LV3_DEACT, SWING_UD_LV3_ACT];
export const SWING_UD_LV4 = [SWING_UD_LV4_DEACT, SWING_UD_LV4_ACT];
export const SWING_UD_LV5 = [SWING_UD_LV5_DEACT, SWING_UD_LV5_ACT];

export const SWING_LR_LV1 = [SWING_LR_LV1_DEACT, SWING_LR_LV1_ACT];
export const SWING_LR_LV2 = [SWING_LR_LV2_DEACT, SWING_LR_LV2_ACT];
export const SWING_LR_LV3 = [SWING_LR_LV3_DEACT, SWING_LR_LV3_ACT];
export const SWING_LR_LV4 = [SWING_LR_LV4_DEACT, SWING_LR_LV4_ACT];
export const SWING_LR_LV5 = [SWING_LR_LV5_DEACT, SWING_LR_LV5_ACT];
export const SWING_LR_FULL = [SWING_LR_FULL_DEACT, SWING_LR_FULL_ACT];
export const SWING_LR_OFF = [SWING_UD_OFF_DEACT, SWING_UD_OFF_ACT];

export const SWING_OFF = [SWING_OFF_DEACT, SWING_OFF_ACT];
export const SWING_ON = [SWING_ON_DEACT, SWING_ON_ACT];

export const SWING_3D_OFF = [SWING_3D_OFF_DEACT, SWING_3D_OFF_ACT];
export const SWING_3D_ON = [SWING_3D_DEACT, SWING_3D_ACT];
