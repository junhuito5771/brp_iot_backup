export const LED_ON_ACT = require('../../shared/assets/remote/ledOn.png');
export const LED_ON_DEACT = require('../../shared/assets/remote/ledOff.png');
export const LED_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const LED_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const POWER_IND_ACT = require('../../shared/assets/remote/powerIndicatorOn.png');
export const POWER_IND_DEACT = require('../../shared/assets/remote/powerIndicatorOff.png');

export const LED_OFF = [LED_OFF_DEACT, LED_OFF_ACT];
export const LED_ON = [LED_ON_DEACT, LED_ON_ACT];
export const POWER_IND = [POWER_IND_DEACT, POWER_IND_ACT];
