export const SLEEP_ON_ACT = require('../../shared/assets/remote/sleepmoodBlue.png');
export const SLEEP_ON_DEACT = require('../../shared/assets/remote/sleepmoodDarkGrey.png');

export const SLEEP_SMART_ACT = require('../../shared/assets/remote/smartSleepOn.png');
export const SLEEP_SMART_DEACT = require('../../shared/assets/remote/smartSleepOff.png');

export const SLEEP_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const SLEEP_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const SLEEP_OFF = [SLEEP_OFF_DEACT, SLEEP_OFF_ACT];
export const SLEEP_ON = [SLEEP_ON_DEACT, SLEEP_ON_ACT];
export const SLEEP_SMART = [SLEEP_SMART_DEACT, SLEEP_SMART_ACT];
