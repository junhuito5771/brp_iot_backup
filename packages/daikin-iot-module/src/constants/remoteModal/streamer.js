export const STREAMER_ON_ACT = require('../../shared/assets/remote/streamerOn.png');
export const STREAMER_ON_DEACT = require('../../shared/assets/remote/streamerOff.png');
export const STREAMER_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const STREAMER_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const STREAMER_OFF = [STREAMER_OFF_DEACT, STREAMER_OFF_ACT];
export const STREAMER_ON = [STREAMER_ON_DEACT, STREAMER_ON_ACT];
