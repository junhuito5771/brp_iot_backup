export const ECOPLUS_ON_ACT = require('../../shared/assets/remote/Eco+_blue.png');
export const ECOPLUS_ON_DEACT = require('../../shared/assets/remote/Eco+_grey.png');
export const ECOPLUS_SMART_ACT = require('../../shared/assets/remote/ecoSmartOn.png');
export const ECOPLUS_SMART_DEACT = require('../../shared/assets/remote/ecoSmartOff.png');

export const ECOPLUS_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const ECOPLUS_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const ECONO_ON_ACT = require('../../shared/assets/remote/ecoNoOn.png');
export const ECONO_ON_DEACT = require('../../shared/assets/remote/ecoNoOff.png');

export const ECOPLUS_OFF = [ECOPLUS_OFF_DEACT, ECOPLUS_OFF_ACT];
export const ECOPLUS_ON = [ECOPLUS_ON_DEACT, ECOPLUS_ON_ACT];
export const ECOPLUS_SMART = [ECOPLUS_SMART_DEACT, ECOPLUS_SMART_ACT];
export const ECONO_ON = [ECONO_ON_DEACT, ECONO_ON_ACT];
