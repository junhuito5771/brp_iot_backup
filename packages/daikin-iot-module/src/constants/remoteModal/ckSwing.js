export const CK_SWING_FULL_ACT = require('../../shared/assets/remote/ckFullOn.png');
export const CK_SWING_FULL_DEACT = require('../../shared/assets/remote/ckFullOff.png');
export const CK_SWING_SOIL_ACT = require('../../shared/assets/remote/ckSoilOn.png');
export const CK_SWING_SOIL_DEACT = require('../../shared/assets/remote/ckSoilOff.png');
export const CK_SWING_DRAFT_ACT = require('../../shared/assets/remote/ckDraftOn.png');
export const CK_SWING_DRAFT_DEACT = require('../../shared/assets/remote/ckDraftOff.png');

export const CK_SWING_FULL = [CK_SWING_FULL_DEACT, CK_SWING_FULL_ACT];
export const CK_SWING_SOIL = [CK_SWING_SOIL_DEACT, CK_SWING_SOIL_ACT];
export const CK_SWING_DRAFT = [CK_SWING_DRAFT_DEACT, CK_SWING_DRAFT_ACT];
