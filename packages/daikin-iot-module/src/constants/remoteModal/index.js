import * as FanSpeedOptions from './fanSpeed';
import * as TurboOptions from './turbo';
import * as EcoplusOptions from './ecoplus';
import * as SleepOptions from './sleep';
import * as SwingOptions from './swing';
import * as BreezeOptions from './breeze';
import * as SmartDriftOptions from './smartDrift';
import * as StreamerOptions from './streamer';
import * as SenseOptions from './sense';
import * as ledOptions from './led';
import * as ckSwingOptions from './ckSwing';

export {
  FanSpeedOptions,
  TurboOptions,
  EcoplusOptions,
  SleepOptions,
  SwingOptions,
  BreezeOptions,
  SmartDriftOptions,
  StreamerOptions,
  SenseOptions,
  ledOptions,
  ckSwingOptions,
};
