export const SMART_DRIFT_ON_ACT = require('../../shared/assets/remote/smartDriftOn.png');
export const SMART_DRIFT_ON_DEACT = require('../../shared/assets/remote/smartDriftOff.png');
export const SMART_DRIFT_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const SMART_DRIFT_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const SMART_DRIFT_OFF = [SMART_DRIFT_OFF_DEACT, SMART_DRIFT_OFF_ACT];
export const SMART_DRIFT_ON = [SMART_DRIFT_ON_DEACT, SMART_DRIFT_ON_ACT];
