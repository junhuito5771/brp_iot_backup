export const SENSE_ON_ACT = require('../../shared/assets/remote/senseOn.png');
export const SENSE_ON_DEACT = require('../../shared/assets/remote/senseOff.png');
export const SENSE_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const SENSE_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const SENSE_OFF = [SENSE_OFF_DEACT, SENSE_OFF_ACT];
export const SENSE_ON = [SENSE_ON_DEACT, SENSE_ON_ACT];
