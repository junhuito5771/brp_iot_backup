export const BREEZE_ON_ACT = require('../../shared/assets/remote/breezeOn.png');
export const BREEZE_ON_DEACT = require('../../shared/assets/remote/breezeOff.png');
export const BREEZE_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const BREEZE_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const BREEZE_OFF = [BREEZE_OFF_DEACT, BREEZE_OFF_ACT];
export const BREEZE_ON = [BREEZE_ON_DEACT, BREEZE_ON_ACT];
