export const FAN_AUTO_ON = require('../../shared/assets/remote/automaticBlue.png');
export const FAN_AUTO_OFF = require('../../shared/assets/remote/automaticDarkGrey.png');

export const FAN_SILENT_ON = require('../../shared/assets/remote/indoorUnitQuietBlue.png');
export const FAN_SILENT_OFF = require('../../shared/assets/remote/indoorUnitQuietDarkGrey.png');

export const FAN_LV1_ON = require('../../shared/assets/remote/fanSpeedLevel1On.png');
export const FAN_LV1_OFF = require('../../shared/assets/remote/fanSpeedLevel1Off.png');

export const FAN_LV2_ON = require('../../shared/assets/remote/fanSpeedLevel2On.png');
export const FAN_LV2_OFF = require('../../shared/assets/remote/fanSpeedLevel2Off.png');

export const FAN_LV3_ON = require('../../shared/assets/remote/fanSpeedLevel3On.png');
export const FAN_LV3_OFF = require('../../shared/assets/remote/fanSpeedLevel3Off.png');

export const FAN_LV4_ON = require('../../shared/assets/remote/fanSpeedLevel4On.png');
export const FAN_LV4_OFF = require('../../shared/assets/remote/fanSpeedLevel4Off.png');

export const FAN_LV5_ON = require('../../shared/assets/remote/fanSpeedLevel5On.png');
export const FAN_LV5_OFF = require('../../shared/assets/remote/fanSpeedLevel5Off.png');

export const FAN_AUTO = [FAN_AUTO_OFF, FAN_AUTO_ON];
export const FAN_SILENT = [FAN_SILENT_OFF, FAN_SILENT_ON];
export const FAN_LV1 = [FAN_LV1_OFF, FAN_LV1_ON];
export const FAN_LV2 = [FAN_LV2_OFF, FAN_LV2_ON];
export const FAN_LV3 = [FAN_LV3_OFF, FAN_LV3_ON];
export const FAN_LV4 = [FAN_LV4_OFF, FAN_LV4_ON];
export const FAN_LV5 = [FAN_LV5_OFF, FAN_LV5_ON];
