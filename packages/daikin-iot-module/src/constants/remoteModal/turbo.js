export const TURBO_ON_ACT = require('../../shared/assets/remote/turbomoondBlue.png');
export const TURBO_ON_DEACT = require('../../shared/assets/remote/turbomoondDarkGrey.png');

export const TURBO_SMART_ON_ACT = require('../../shared/assets/remote/smartTurboBlue.png');
export const TURBO_SMART_ON_DEACT = require('../../shared/assets/remote/smartTurboGrey.png');

export const TURBO_OFF_ACT = require('../../shared/assets/remote/remoteOffBlue.png');
export const TURBO_OFF_DEACT = require('../../shared/assets/remote/remoteOffGrey.png');

export const TURBO_OFF = [TURBO_OFF_DEACT, TURBO_OFF_ACT];
export const TURBO_ON = [TURBO_ON_DEACT, TURBO_ON_ACT];
export const TURBO_SMART = [TURBO_SMART_ON_DEACT, TURBO_SMART_ON_ACT];
