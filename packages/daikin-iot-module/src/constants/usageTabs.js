export const PERIOD_YEAR = 'Year';
export const PERIOD_MONTH = 'Month';
export const PERIOD_WEEK = 'Week';
export const PERIOD_DAY = 'Day';

export const STA_TEMP = 'Temperature';
export const STA_ENERGY = 'Energy Consumption';
