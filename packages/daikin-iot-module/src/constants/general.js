import { Platform, StatusBar } from 'react-native';

export const HEADER_HEIGHT = Platform.select({
  ios: 45,
  android: StatusBar.currentHeight > 24 ? 32 : 57,
});

export const TAB_ITEM_HEIGHT = 55;
export const HANDLE_HEIGHT = 25;
