import { ShadowValueHelper } from '@module/utility';

const TurboIcons = {
  [ShadowValueHelper.TurboValueMap.turboOff]: {
    label: 'Off',
    value: ShadowValueHelper.TurboValueMap.turboOff,
    icons: [
      require('../shared/images/remote/offDarkGrey.png'),
      require('../shared/images/remote/offBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/smartTurboGrey.png'),
  },
  [ShadowValueHelper.TurboValueMap.turboOn]: {
    label: 'Powerful',
    value: ShadowValueHelper.TurboValueMap.turboOn,
    icons: [
      require('../shared/images/remote/turbomoondDarkGrey.png'),
      require('../shared/images/remote/turbomoondBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/turbomoondBlue.png'),
  },
  [ShadowValueHelper.TurboValueMap.turboSmart]: {
    label: 'Smart powerful',
    value: ShadowValueHelper.TurboValueMap.turboSmart,
    icons: [
      require('../shared/images/remote/smartTurboGrey.png'),
      require('../shared/images/remote/smartTurboBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/smartTurboBlue.png'),
  },
};

export default TurboIcons;
