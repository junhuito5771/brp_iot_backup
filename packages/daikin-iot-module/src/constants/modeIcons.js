const modeIcons = {
  auto: {
    active: require('../shared/assets/remote/automaticBlue.png'),
    inactive: require('../shared/assets/remote/automaticDarkGrey.png'),
  },
  cool: {
    active: require('../shared/assets/remote/coolMoodOn.png'),
    inactive: require('../shared/assets/remote/coolMoodOff.png'),
  },
  heat: {
    active: require('../shared/assets/remote/heat.png'),
    inactive: require('../shared/assets/remote/heatGrey.png'),
  },
  fan: {
    active: require('../shared/assets/remote/fanModeGreen.png'),
    inactive: require('../shared/assets/remote/fanModeDarkGrey.png'),
  },
  dry: {
    active: require('../shared/assets/remote/humidityMoodBlue.png'),
    inactive: require('../shared/assets/remote/humidityMoodDarkGrey.png'),
  },
};

export default modeIcons;

export const modeEnum = {
  auto: 10,
  cool: 1,
  heat: 8,
  fan: 2,
  dry: 4,
};
