export const Fan = {
  AUTO: require('../shared/images/remote/automaticFanBlue.png'),
  QUIET: require('../shared/images/remote/indoorUnitQuietMoodBlue.png'),
  LEVEL_ONE: require('../shared/images/remote/bigFanMoodLevel1.png'),
  LEVEL_TWO: require('../shared/images/remote/bigFanMoodLevel2.png'),
  LEVEL_THREE: require('../shared/images/remote/bigFanMoodLevel3.png'),
};

export const Powerful = {
  TURBO: require('../shared/images/remote/turbomoondBlue.png'),
};

export const AirFlow = {
  SWING_VERTICAL: require('../shared/images/remote/airFlowDirectionOnBlue.png'),
};

export const SleepMode = {
  SLEEP: require('../shared/images/remote/sleepmoodBlue.png'),
};

export const OFF_ACTIVE = require('../shared/images/remote/offBlue.png');
export const OFF_INACTIVE = require('../shared/images/remote/offDarkGrey.png');
export const TIMER = require('../shared/images/remote/timerSmall.png');

export const Mode = {
  AUTO: require('../shared/images/remote/automaticDarkGrey.png'),
};
