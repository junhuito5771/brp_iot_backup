export default {
  0: {
    label: 'Off',
    value: 0,
    icons: [
      require('../shared/images/remote/airFlowDirectionOffDarkGrey.png'),
      require('../shared/images/remote/airFlowDirectionOffBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/airFlowDirectionOffBlue.png'),
  },
  1: {
    label: 'Full swing',
    value: 1,
    icons: [
      require('../shared/images/remote/airFlowDirectionOnDarkGrey.png'),
      require('../shared/images/remote/airFlowDirectionOnBlue.png'),
    ],
    tabIcon: require('../shared/images/remote/airFlowDirectionOnBlue.png'),
  },
};
