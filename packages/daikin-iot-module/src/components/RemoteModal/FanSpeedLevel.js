import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet } from 'react-native';

import { TouchableFeedbackIcon } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

const FanSpeedLevel = ({ levels, selectedIndex, onPress }) => (
  <View style={styles.container}>
    {levels.map((item, index) => (
      <TouchableFeedbackIcon
        icons={item.icons}
        isSelected={index <= selectedIndex}
        onPress={onPress}
      />
    ))}
  </View>
);

FanSpeedLevel.propTypes = {
  levels: PropTypes.array,
  selectedIndex: PropTypes.number,
  onPress: PropTypes.func,
};

export default FanSpeedLevel;
