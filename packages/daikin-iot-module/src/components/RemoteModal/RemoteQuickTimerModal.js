import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { connect, editTimer } from '@daikin-dama/redux-iot';
import { scale } from '@module/utility';
import moment from 'moment';

import { WheelPicker, Text, Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  header: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    justifyContent: 'space-between',
  },
  headerText: {
    fontSize: 16,
  },
  selectionContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  selectionUnitLabel: {
    fontSize: 18,
    alignSelf: 'center',
    marginHorizontal: 5,
  },
  spacing: {
    marginRight: 10,
  },
  picker: {
    minWidth: scale(60),
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
});

const SWITCH_OPTIONS = ['OFF', 'ON'];
const HOUR_OPTIONS = ['0', '1', '2', '3', '4', '5', '6'];
const MIN_OPTIONS = ['00', '10', '20', '30', '40', '50'];

const RemoteQuickTimerModal = ({ title, onClose, onSubmit }) => {
  const [switchPos, setSwitchPos] = useState(0);
  const [hourPos, setHourPos] = useState(0);
  const [minPos, setMinPos] = useState(1);

  useEffect(() => {
    if (hourPos === 0 && minPos === 0) {
      setMinPos(1);
    }
  }, [hourPos, minPos]);

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text bold style={styles.headerText}>
          {title}
        </Text>
        <TouchableOpacity
          hitSlop={styles.hitSlop}
          disabled={hourPos === 0 && minPos === 0}
          onPress={() => {
            if (onSubmit) {
              const hourValue = Number(HOUR_OPTIONS[hourPos]);
              const minValue = Number(MIN_OPTIONS[minPos]);
              const totalHourInMin = hourValue * 60;
              const totalMinutes = totalHourInMin + minValue;
              const curDT = moment();
              const targetDT = curDT.add(totalMinutes, 'minutes');

              onSubmit({
                day: targetDT.days(),
                switchValue: switchPos,
                hour: targetDT.hours(),
                minute: targetDT.minutes(),
              });
            }
            if (onClose) onClose();
          }}>
          <View>
            <Text color={Colors.blue} style={styles.headerText}>
              Done
            </Text>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.selectionContainer}>
        <WheelPicker
          style={[styles.picker, styles.spacing]}
          selectedItemPosition={switchPos}
          onValueChange={(v, index) => setSwitchPos(index)}
          data={SWITCH_OPTIONS}
        />
        <WheelPicker
          style={styles.picker}
          selectedItemPosition={hourPos}
          onValueChange={(v, index) => setHourPos(index)}
          data={HOUR_OPTIONS}
        />
        <Text style={styles.selectionUnitLabel}>Hour</Text>
        <WheelPicker
          style={styles.picker}
          selectedItemPosition={minPos}
          onValueChange={(v, index) => setMinPos(index)}
          data={MIN_OPTIONS}
        />
        <Text style={styles.selectionUnitLabel}>Minute</Text>
      </View>
    </View>
  );
};

RemoteQuickTimerModal.propTypes = {
  title: PropTypes.string,
  onClose: PropTypes.func,
  onSubmit: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  submitQuickTimer: (isCreate, timerInfo) => {
    dispatch(editTimer.submitQuickTimer({ isCreate, timerInfo }));
  },
});

export default connect(null, mapDispatchToProps)(RemoteQuickTimerModal);
