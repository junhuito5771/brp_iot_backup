import React, { useState, useMemo } from 'react';
import { View, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import {
  connect,
  remote,
  RemoteHelper,
  ShadowValueHelper,
  HandsetRuleHelper,
} from '@daikin-dama/redux-iot';
import { NavigationService } from '@module/utility';
import Toast from 'react-native-root-toast';

import { TabBar, Modal, Text, Colors } from '@module/daikin-ui';

import FanSpeedIcon, {
  fanSpeedTabIconsDIL,
} from '../../constants/FanSpeedIcons';
import TurboIcon from '../../constants/turboIcon';
import SwingIcon from '../../constants/SwingIcon';
import EcoplusIcon from '../../constants/EcoplusIcon';

import SleepIcon from '../../constants/SleepIcons';

const getFanSpeedOptions = ({
  mode,
  enableAutoFan,
  enableLowFan,
  handsetType,
  enableSilent,
}) => {
  const components = [];

  if (
    enableAutoFan &&
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_AUTO,
      handsetType
    )
  ) {
    components.push(FanSpeedIcon.auto);
  }

  if (
    enableSilent &&
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_SILENT,
      handsetType
    )
  ) {
    components.push(FanSpeedIcon.silent);
  }

  if (
    enableLowFan &&
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV1,
      handsetType
    )
  ) {
    components.push(FanSpeedIcon.level1);
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV2,
      handsetType
    )
  ) {
    components.push(FanSpeedIcon.level2);
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV3,
      handsetType
    )
  ) {
    components.push(FanSpeedIcon.level3);
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV4,
      handsetType
    )
  ) {
    components.push(FanSpeedIcon.level4);
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV5,
      handsetType
    )
  ) {
    components.push(FanSpeedIcon.level5);
  }

  return components;
};

const getTabItems = ({
  handsetType,
  mode,
  enableAutoFan,
  enableSilent,
  enableLowFan,
  enableSleep,
  enableTurbo,
  enableEcoplus,
}) => {
  const items = [];

  if (
    RemoteHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_FAN_SPEED,
      handsetType
    )
  ) {
    const fanSpdOptionMap =
      handsetType === 0 ? FanSpeedIcon : fanSpeedTabIconsDIL;
    items.push({
      title: 'Fan speed',
      stateKey: 'fan',
      action: remote.setFanSpeed,
      optionMap: fanSpdOptionMap,
      options: getFanSpeedOptions({
        mode,
        enableAutoFan,
        enableLowFan,
        enableSilent,
        handsetType,
      }),
    });
  }

  items.push({
    title: 'Swing',
    stateKey: 'swing',
    action: remote.setSwing,
    optionMap: SwingIcon,
    options: [SwingIcon[0], SwingIcon[1]],
  });

  if (
    enableSleep &&
    RemoteHelper.isFeatureAllowed(mode, RemoteHelper.FEAT_SLEEP, handsetType)
  ) {
    items.push({
      title: 'Sleep',
      stateKey: 'sleep',
      action: remote.setSleep,
      optionMap: SleepIcon,
      options: [SleepIcon[0], SleepIcon[1]],
    });
  }

  if (
    enableTurbo &&
    RemoteHelper.isFeatureAllowed(mode, RemoteHelper.FEAT_TURBO, handsetType)
  ) {
    items.push({
      title: 'Powerful',
      stateKey: 'turbo',
      action: remote.setTurbo,
      optionMap: TurboIcon,
      options: Object.values(TurboIcon),
    });
  }

  if (
    enableEcoplus &&
    RemoteHelper.isFeatureAllowed(mode, RemoteHelper.FEAT_ECOPLUS, handsetType)
  ) {
    items.push({
      title: 'Eco+',
      stateKey: 'ecoplus',
      action: remote.setEcoplus,
      optionMap: EcoplusIcon,
      options: [EcoplusIcon[0], EcoplusIcon[1]],
    });
  }

  return items;
};

const getDay = timer => {
  const daysInStr = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

  return daysInStr[timer.day];
};

const getTimerTimeDetail = timer => {
  const time = `${timer.hour}:${timer.minute}`;

  return `${time} ${getDay(timer)}`;
};

const getTimerModeIcon = mode => {
  switch (mode) {
    case 'auto':
      return require('../../shared/assets/remote/autoMood.png');
    case 'cool':
      return require('../../shared/assets/remote/coolMood.png');
    case 'fan':
      return require('../../shared/assets/remote/fanMood.png');
    case 'heat':
      return require('../../shared/assets/remote/heatMood.png');
    case 'dry':
      return require('../../shared/assets/remote/dryMood.png');
    default:
      return require('../../shared/assets/remote/autoMood.png');
  }
};

const styles = StyleSheet.create({
  timerContainer: {
    flexWrap: 'wrap',
    flex: 1,
  },
  timerTopSection: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  timerBottomSection: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  timerLogoIcon: {
    width: 15,
    height: 15,
    marginRight: 1.5,
  },
  timerModeIcon: {
    width: 18,
    height: 18,
  },
  timerTimeText: {
    fontSize: 10,
  },
  timerOffText: {
    fontSize: 12,
  },
  timerTempText: {
    fontSize: 12,
  },
});

const showToastMsg = msg => {
  Toast.show(msg, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: false,
    animation: true,
    hideOnPress: true,
    delay: 0,
    textColor: Colors.darkGrey,
    backgroundColor: Colors.lightCoolGrey,
    opacity: 0.9,
  });
};

const getCurStateValue = (handsetType, modeConfig, stateKey) => {
  if (stateKey === 'fan') {
    if (modeConfig.turbo === 1) return 'off';
    if (modeConfig.silent === 1) return 'silent';
    return ShadowValueHelper.getFanLvlByShadowVal(
      handsetType,
      modeConfig.fan,
      modeConfig.fanExtend
    );
  }
  if (stateKey === 'turbo') {
    return ShadowValueHelper.getValueKeyByShadowVal(
      modeConfig.turbo,
      modeConfig.smartTurbo
    );
  }

  return modeConfig[stateKey] !== undefined ? modeConfig[stateKey] : 0;
};

const RemoteBottomTab = ({
  style,
  dispatchAnyAction,
  mode,
  enableAutoFan,
  enableSilent,
  enableLowFan,
  enableTurbo,
  enableSleep,
  enableEcoplus,
  modeConfig,
  upcomingTimer,
  thingName,
  handsetType,
  switch: switchValue,
}) => {
  const tabItems = useMemo(
    () =>
      getTabItems({
        mode,
        handsetType,
        enableAutoFan,
        enableSilent,
        enableLowFan,
        enableTurbo,
        enableSleep,
        enableEcoplus,
      }),
    [
      mode,
      handsetType,
      enableAutoFan,
      enableSilent,
      enableLowFan,
      enableTurbo,
      enableSleep,
      enableEcoplus,
    ]
  );
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);

  const selectedTabItem = tabItems[selectedTabIndex] || {};
  const curStateValue = getCurStateValue(
    handsetType,
    modeConfig,
    selectedTabItem.stateKey
  );

  const handleOnPressItem = () => {
    setIsModalVisible(!isModalVisible);
  };

  const handleOnModalClose = () => {
    setIsModalVisible(false);
  };
  // ToDo: Need to be careful with empty tabItems here
  return (
    <>
      {tabItems.length > -1 && selectedTabIndex > -1 && isModalVisible && (
        <Modal
          isVisible={isModalVisible}
          title={selectedTabItem.title}
          key={selectedTabItem.title}
          options={selectedTabItem.options}
          optionMap={selectedTabItem.optionMap}
          selectedValue={curStateValue}
          onPressOption={selectedValue => {
            if (selectedTabItem) {
              dispatchAnyAction(selectedTabItem.action(selectedValue));
            }
          }}
          onClose={handleOnModalClose}
        />
      )}
      <TabBar style={style}>
        {!!upcomingTimer && (
          <TabBar.Item
            onPress={() => {
              const screenKey = getDay(upcomingTimer);
              const scrollToKey = `${thingName}_${upcomingTimer.timerIndex}`;
              NavigationService.drawerReset(screenKey, { scrollToKey });
            }}>
            <View style={styles.timerContainer}>
              <View style={styles.timerTopSection}>
                <Image
                  style={styles.timerLogoIcon}
                  source={require('../../shared/assets/remote/timerSmall.png')}
                />
                <Text style={styles.timerTimeText}>
                  {getTimerTimeDetail(upcomingTimer)}
                </Text>
              </View>
              {upcomingTimer.switchValue === 0 ? (
                <View style={styles.timerBottomSection}>
                  <Text light style={styles.timerOffText}>
                    OFF
                  </Text>
                </View>
              ) : (
                <View style={styles.timerBottomSection}>
                  <Image
                    style={styles.timerModeIcon}
                    source={getTimerModeIcon(upcomingTimer.mode)}
                  />
                  {!!upcomingTimer.temp && (
                    <Text style={styles.timerTempText}>
                      {' '}
                      {`${upcomingTimer.temp}\u2103`}
                    </Text>
                  )}
                </View>
              )}
            </View>
          </TabBar.Item>
        )}
        {tabItems.map((item, index) => {
          const curTabValue = getCurStateValue(
            handsetType,
            modeConfig,
            item.stateKey
          );
          const { title } = item || {};

          return (
            <TabBar.Item
              key={title}
              imgSource={(item.optionMap[curTabValue] || {}).tabIcon}
              onPress={() => {
                // DIL board do not allow to set powerful, sleep and eco+
                // when unit is turn off
                if (handsetType === 1 && switchValue === 0) {
                  if (
                    title === 'Powerful' ||
                    title === 'Sleep' ||
                    title === 'Eco+'
                  ) {
                    showToastMsg(`Please turn on the unit to control ${title}`);
                    return;
                  }
                }

                handleOnPressItem();
                setSelectedTabIndex(index);
              }}
            />
          );
        })}
      </TabBar>
    </>
  );
};

RemoteBottomTab.propTypes = {
  style: PropTypes.object,
  dispatchAnyAction: PropTypes.func,
  mode: PropTypes.string,
  enableAutoFan: PropTypes.bool,
  enableSilent: PropTypes.bool,
  enableLowFan: PropTypes.bool,
  enableTurbo: PropTypes.bool,
  enableSleep: PropTypes.bool,
  enableEcoplus: PropTypes.bool,
  modeConfig: PropTypes.object,
  upcomingTimer: PropTypes.object,
  thingName: PropTypes.string,
  handsetType: PropTypes.number,
  switch: PropTypes.number,
};

const mapDispatchToProps = dispatch => ({
  setFanSpeed: fanSpeed => {
    dispatch(remote.setFanSpeed(fanSpeed));
  },
  dispatchAnyAction: action => {
    dispatch(action);
  },
});

export default connect(null, mapDispatchToProps)(RemoteBottomTab);
