import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { heightPercentage } from '@module/utility';
import { connect } from '@daikin-dama/redux-iot';

import {
  TouchableFeedbackIcon,
  ModalFrame,
  Text,
  Colors,
} from '@module/daikin-ui';

const styles = StyleSheet.create({
  modal: {
    backgroundColor: Colors.white,
    height: 200,
  },
  modalTitle: {
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: Colors.lightCoolGrey,
  },
  modalTitleText: {
    textAlign: 'center',
  },
  modalContent: {
    flex: 1,
  },
  modalButton: {
    height: heightPercentage(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalHeader: {
    height: heightPercentage(5),
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: Colors.sectionHeaderGrey,
  },
  selectionTitle: {
    alignItems: 'center',
    paddingTop: 10,
  },
  selectionContent: {
    height: heightPercentage(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectionOption: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  topBorder: {
    height: 0.5,
  },
  iconContainer: {
    marginHorizontal: 5,
  },
});

const RemoteContent = ({ options, selectedValue }) => {
  const selectedIndex = options.findIndex(
    ({ value }) => value === selectedValue
  );
  const label = selectedIndex > -1 ? options[selectedIndex].label : '';

  return (
    <View styles={styles.modalContent}>
      <View style={styles.selectionTitle}>
        <Text
          style={styles.modalTitleText}
          medium
          color={Colors.blue}>{`--- ${label} ---`}</Text>
      </View>
      <View style={styles.selectionContent}>
        <View style={[styles.selectionOption]}>
          {options.map(option => (
            <TouchableFeedbackIcon
              containerStyle={styles.iconContainer}
              isSelected={option.value === selectedValue}
              key={option.label}
              icons={option.icons}
              onPress={option.onPress}
            />
          ))}
        </View>
      </View>
    </View>
  );
};

const mapStateToProps = (
  { units: { allUnits } },
  { selectedValueHandler, thingName }
) => ({
  selectedValue: selectedValueHandler(allUnits[thingName]),
});

const ConnectedRemoteContent = connect(mapStateToProps)(RemoteContent);

RemoteContent.propTypes = {
  options: PropTypes.array,
  selectedValue: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
};

const RemoteSwingModal = ({
  title,
  sections,
  onClose,
  onBack,
  ...extraProps
}) => (
  <ModalFrame title={title} onClose={onClose} onBack={onBack} {...extraProps}>
    {sections.map(sectionItem => (
      <ConnectedRemoteContent {...sectionItem} />
    ))}
  </ModalFrame>
);

RemoteSwingModal.propTypes = {
  title: PropTypes.string,
  sections: PropTypes.array,
  onClose: PropTypes.func,
  onBack: PropTypes.func,
};

export default RemoteSwingModal;
