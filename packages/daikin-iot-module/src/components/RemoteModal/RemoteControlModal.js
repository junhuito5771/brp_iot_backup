import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { heightPercentage, SAFE_VIEWPORT_WIDTH } from '@module/utility';
import { connect } from '@daikin-dama/redux-iot';

import {
  TouchableFeedbackIcon,
  ModalFrame,
  Text,
  Colors,
} from '@module/daikin-ui';

const styles = StyleSheet.create({
  modal: {
    backgroundColor: Colors.white,
    height: 200,
  },
  modalTitle: {
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: Colors.lightCoolGrey,
  },
  modalTitleText: {
    textAlign: 'center',
  },
  modalContent: {
    flex: 1,
  },
  modalButton: {
    height: heightPercentage(5),
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalHeader: {
    height: heightPercentage(5),
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: Colors.sectionHeaderGrey,
  },
  selectionTitle: {
    alignItems: 'center',
    paddingTop: 10,
  },
  selectionContent: {
    height: heightPercentage(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  selectionOption: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
  },
  topBorder: {
    height: 0.5,
  },
});

const RemoteControlModal = ({
  title,
  subTitle,
  options,
  selectedValue,
  selectedHandler,
  onClose,
  onBack,
  ...extraProps
}) => {
  const selectedIndex = options.findIndex(
    ({ value }) => value === selectedValue
  );
  const label = selectedIndex > -1 ? options[selectedIndex].label : '';

  const optionRatio = options.length > 0 ? Math.min(options.length, 4) / 4 : 1;
  const optionWidth =
    options.length > 0
      ? SAFE_VIEWPORT_WIDTH * optionRatio
      : SAFE_VIEWPORT_WIDTH;

  return (
    <ModalFrame
      title={title}
      subTitle={subTitle}
      onClose={onClose}
      onBack={onBack}
      {...extraProps}>
      <View style={styles.selectionTitle}>
        <Text
          style={styles.modalTitleText}
          medium
          color={Colors.blue}>{`--- ${label} ---`}</Text>
      </View>
      <View style={styles.selectionContent}>
        <View style={[styles.selectionOption, { width: optionWidth }]}>
          {options.map(option => (
            <TouchableFeedbackIcon
              isSelected={
                selectedHandler
                  ? selectedHandler(option.value, selectedValue)
                  : option.value === selectedValue
              }
              key={option.label}
              icons={option.icons}
              onPress={option.onPress}
            />
          ))}
        </View>
      </View>
    </ModalFrame>
  );
};

RemoteControlModal.propTypes = {
  title: PropTypes.string,
  subTitle: PropTypes.string,
  options: PropTypes.array,
  selectedValue: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  onClose: PropTypes.func,
  onBack: PropTypes.func,
  selectedHandler: PropTypes.func,
};

RemoteControlModal.defaultProps = {
  options: [{ label: '', value: 0 }],
  selectedValue: 0,
};

const mapStateToProps = (
  { units: { allUnits } },
  { selectedValueHandler, thingName }
) => ({
  selectedValue: selectedValueHandler(allUnits[thingName]),
});

export default connect(mapStateToProps)(RemoteControlModal);
