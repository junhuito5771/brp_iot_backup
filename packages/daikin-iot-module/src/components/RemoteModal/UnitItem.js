import React from 'react';
import PropTypes from 'prop-types';
import { View, Image, StyleSheet, TouchableOpacity, Alert } from 'react-native';
import { heightPercentage, widthPercentage } from '@module/utility';

import { getACIconFromPath, Colors, Text } from '@module/daikin-ui';

const styles = StyleSheet.create({
  rowContainer: {
    paddingHorizontal: widthPercentage(5),
  },
  bottomSeperator: {
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 0.5,
  },
  itemRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: heightPercentage(10),
  },
  imageContainer: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    marginHorizontal: 10,
  },
  content: {
    textAlign: 'center',
    marginHorizontal: widthPercentage(8),
  },
});

const showAlert = () => {
  Alert.alert(
    'Not authorized',
    'You do not have permission to access this unit. Please log in to the account paired with this unit first.',
    [
      {
        text: 'OK',
      },
    ]
  );
};

const UnitItem = ({
  item: { name, logo, disallowed },
  defaultLogo,
  onPress,
}) => {
  const acLogo = !logo ? defaultLogo : getACIconFromPath(logo);
  const handleOnPress = () => {
    if (disallowed) {
      showAlert();
    } else if (onPress) {
      onPress();
    }
  };

  return (
    <View style={[styles.rowContainer, styles.bottomSeperator]}>
      <TouchableOpacity onPress={handleOnPress}>
        <View key={name} style={styles.itemRow}>
          <View style={styles.imageContainer}>
            <Image style={styles.image} source={acLogo} />
          </View>
          <Text style={styles.content} medium>
            {name}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

UnitItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    logo: PropTypes.string,
    disallowed: PropTypes.bool,
  }),
  defaultLogo: PropTypes.number,
  onPress: PropTypes.func,
};

export default UnitItem;
