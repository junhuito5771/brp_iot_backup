import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import FanSpeedIcons from '../../constants/FanSpeedIcons';

import styles from './styles';

const levels = [
  FanSpeedIcons.level1,
  FanSpeedIcons.level2,
  FanSpeedIcons.level3,
];

const LevelSelector = ({
  selectedValue,
  onSelect,
  containerStyle,
  enableLow,
}) => {
  const selectedIndex = levels.findIndex(
    levelInfo => levelInfo.value.toLowerCase() === selectedValue
  );

  return (
    <View style={StyleSheet.flatten([styles.container, containerStyle])}>
      {levels.map((levelInfo, index) => {
        const isSelected = selectedIndex >= index;
        const iconIndex = Number(isSelected);

        // If the enableLow is false, then don't render the Level 1 fan speed
        if (index === 0 && !enableLow) return <></>;

        return (
          <View style={styles.item} key={levelInfo.value}>
            <TouchableOpacity
              onPress={() => {
                onSelect(levelInfo.value.toLowerCase());
              }}
              disabled={index === selectedIndex}>
              <Image source={levelInfo.icons[iconIndex]} />
            </TouchableOpacity>
          </View>
        );
      })}
    </View>
  );
};

LevelSelector.propTypes = {
  selectedValue: PropTypes.string,
  onSelect: PropTypes.func,
  containerStyle: PropTypes.object,
  enableLow: PropTypes.bool,
};

export default LevelSelector;
