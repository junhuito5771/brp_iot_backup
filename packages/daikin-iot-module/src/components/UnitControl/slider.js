/* eslint-disable no-mixed-operators */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  PanResponder,
  Animated,
  Image,
  Platform,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { connect, remote } from '@daikin-dama/redux-iot';
import { ScaleText } from '@module/utility';
import Svg, { Circle, Line, Text, Path, Rect, G } from 'react-native-svg';
import LottieView from 'lottie-react-native';

import { Colors } from '@module/daikin-ui';

import ModeDisplay from '../ModeDisplay/modeDisplay';
import { modeEnum } from '../../constants/modeIcons';

const styles = StyleSheet.create({
  sliderMarginTop: {
    marginTop: 80,
  },
  absoluteStyle: {
    ...StyleSheet.absoluteFillObject,
  },
  overlayContainer: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
  },
  leaftAnim: {
    position: 'absolute',
    zIndex: 1,
    paddingVertical: 10,
    overflow: 'hidden',
  },
  leafAnimOverlay: {
    backgroundColor: 'white',
    opacity: 0.5,
    position: 'absolute',
    zIndex: 2,
    overflow: 'hidden',
  },
  leafTouchable: {
    position: 'absolute',
    top: 100,
    left: 80,
    zIndex: 4,
    overflow: 'hidden',
  },
  controlLayer: {
    zIndex: 3,
  },
});

const Slider = ({
  isOff,
  scaledSize,
  ratio,
  cx,
  cy,
  modeType,
  minTemp,
  maxTemp,
  minTempColor,
  maxTempColor,
  colorRange,
  hasController,
  fill,
  setMode,
  setTemp,
  acTemp,
  availableModes,
  desired,
  clearDesireTemp,
  showLeafAnim,
}) => {
  const [temperature, setTemperature] = React.useState(acTemp);
  const [showPopup, setShowPopup] = React.useState(false);

  React.useEffect(() => {
    if (desired) {
      if (acTemp === desired.temp) {
        setTemperature(acTemp);
        clearDesireTemp();
      } else if (desired.temp === undefined && acTemp !== 1) {
        setTemperature(acTemp);
      }
    }
  }, [acTemp]);

  const { PI, cos, sin, round, atan } = Math;
  const strokeWidth = round(scaledSize * 0.18);
  const radius = (scaledSize - strokeWidth) / 2;
  const startAngle = PI + PI * 0.25;
  const endAngle = 2 * PI - PI * 0.25;
  const x1 = cx - radius * cos(startAngle);
  const y1 = -radius * sin(startAngle) + cy;
  const x2 = cx - radius * cos(endAngle);
  const y2 = -radius * sin(endAngle) + cy;

  const fadeAnim = new Animated.Value(0);
  const AnimatedCircle = Animated.createAnimatedComponent(Circle);
  const AnimatedText = Animated.createAnimatedComponent(Text);
  const AnimatedPath = Animated.createAnimatedComponent(Path);
  const AnimatedG = Animated.createAnimatedComponent(G);

  const getCoords = angle => {
    const a = ((angle - 270) * PI) / 180.0;
    const x = cx + radius * cos(a);
    const y = cy + radius * sin(a);
    return { x, y };
  };

  const getAngle = (x, y) =>
    round(atan((y - cy) / (x - cx)) / (PI / 180) + (x > cx ? 270 : 90));

  const minAngle = getAngle(x2, y2) + 12;
  const maxAngle = getAngle(x1, y1) - 12;

  if (showPopup) {
    fadeAnim.setValue(0.9);
  }

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onMoveShouldSetPanResponder: () => true,
    onPanResponderGrant: () => {
      fadeAnim.stopAnimation(() => {
        fadeAnim.setValue(0.9);
        setShowPopup(true);
      });
    },
    onPanResponderRelease: () => {
      setTemp(round(temperature));
      Animated.timing(fadeAnim, {
        toValue: 0,
        duration: 500,
        useNativeDriver: Platform.OS === 'android',
      }).start(() => setShowPopup(false));
    },
    onPanResponderMove: ({ nativeEvent: { locationX, locationY } }) => {
      const angle = getAngle(locationX - 40, locationY - 80);
      if (angle >= minAngle && angle <= maxAngle) {
        setTemperature(
          new Animated.Value(angle)
            .interpolate({
              inputRange: [minAngle, maxAngle],
              outputRange: [minTemp, maxTemp],
            })
            .__getValue()
        );
      }
    },
  });

  const intAngle = new Animated.Value(temperature).interpolate({
    inputRange: [minTemp, maxTemp],
    outputRange: [minAngle, maxAngle],
  });

  const points = getCoords(intAngle.__getValue());

  const onIncrement = React.useCallback(() => {
    const roundedTemp = round(temperature);
    if (roundedTemp >= minTemp && roundedTemp < maxTemp) {
      setTemperature(roundedTemp + 1);
      setTemp(roundedTemp + 1);
    }
  }, [temperature, minTemp, maxTemp]);

  const onDecrement = React.useCallback(() => {
    const roundedTemp = round(temperature);
    if (roundedTemp > minTemp && roundedTemp <= maxTemp) {
      setTemperature(roundedTemp - 1);
      setTemp(roundedTemp - 1);
    }
  }, [temperature, minTemp, maxTemp]);

  const textFill = isOff ? Colors.unitOffGrey : Colors.darkGrey;

  const getModeLabel = mode =>
    mode === 'Cool' || mode === 'Heat' ? `${mode}ing` : mode;

  const range = (start, end) =>
    Array(end - start + 1)
      .fill()
      .map((_, index) => start + index);

  const animatedFill = React.useMemo(() => {
    if (isOff) {
      return Colors.unitOffGrey;
    }
    if (modeType === 'Auto') {
      return new Animated.Value(temperature).interpolate({
        inputRange: range(minTemp, maxTemp),
        outputRange: colorRange,
      });
    }
    return new Animated.Value(temperature).interpolate({
      inputRange: [minTemp, maxTemp],
      outputRange: [minTempColor, maxTempColor],
    });
  }, [isOff, temperature, modeType]);

  const iconPosition = {
    1: {
      coords: [{ x: 40 + 103 * ratio, y: 75 - 50 * ratio }],
    },
    2: {
      coords: [
        { x: 40 + 75 * ratio, y: 75 - 41 * ratio },
        { x: 40 + 128 * ratio, y: 75 - 43 * ratio },
      ],
    },
    3: {
      coords: [
        { x: 40 + 47 * ratio, y: 75 - 41 * ratio },
        { x: 40 + 103 * ratio, y: 75 - 50 * ratio },
        { x: 40 + 158 * ratio, y: 75 - 43 * ratio },
      ],
    },
    4: {
      coords: [
        { x: 40 + 22 * ratio, y: 75 - 16 * ratio },
        { x: 40 + 75 * ratio, y: 75 - 41 * ratio },
        { x: 40 + 128 * ratio, y: 75 - 43 * ratio },
        { x: 40 + 182 * ratio, y: 75 - 18 * ratio },
      ],
    },
    5: {
      coords: [
        { x: 40 - 5 * ratio, y: 75 - 10 * ratio },
        { x: 40 + 47 * ratio, y: 75 - 41 * ratio },
        { x: 40 + 103 * ratio, y: 75 - 50 * ratio },
        { x: 40 + 158 * ratio, y: 75 - 43 * ratio },
        { x: 40 + 208 * ratio, y: 75 - 10 * ratio },
      ],
    },
  };

  const { coords } = iconPosition[availableModes.length] || {};

  const handleLeafOnPress = () => {
    Alert.alert(
      'Eco mode',
      'Eco mode is enabled, for better energy savings, the set temperature will be adjusted automatically.'
    );
  };

  return (
    <>
      <View style={styles.overlayContainer}>
        <View
          style={[
            styles.sliderMarginTop,
            {
              width: scaledSize,
              height: scaledSize,
            },
          ]}>
          {showLeafAnim && (
            <>
              <LottieView
                style={[
                  styles.leaftAnim,
                  {
                    width: scaledSize,
                    height: scaledSize,
                    borderRadius: scaledSize / 2,
                  },
                ]}
                source={require('../../shared/assets/lottie/leafFall.json')}
                autoPlay
                loop
              />
              <View
                style={[
                  styles.leafAnimOverlay,
                  {
                    width: scaledSize,
                    height: scaledSize,
                    borderRadius: scaledSize / 2,
                  },
                ]}
              />
            </>
          )}
          {hasController ? (
            <>
              <Svg width="100%" height="100%">
                <AnimatedCircle
                  cx={cx}
                  cy={cy}
                  r={scaledSize / 2}
                  fill={animatedFill}
                />
              </Svg>
              <View
                style={[
                  styles.overlayContainer,
                  {
                    width: scaledSize,
                    height: scaledSize,
                  },
                ]}>
                <Image
                  source={require('./shadow.png')}
                  style={{ width: scaledSize, height: scaledSize }}
                />
              </View>
            </>
          ) : (
            <ModeDisplay
              {...{ isOff, cx, cy, scaledSize, mode: modeType, fill }}
            />
          )}
          {/* Dark background with increase/decrease buttons */}
          {hasController && (
            <Svg
              width="100%"
              height="100%"
              viewBox="0 0 252 252"
              style={styles.absoluteStyle}>
              <Path
                fill="#47474A"
                d="M37.198 214.08l32.186-32.089c14.468 15.126 34.819 24.542 57.36 24.542 21.704 0 41.375-8.727 55.721-22.876l31.886 31.977C191.996 238.114 161.097 252 126.627 252c-35.258 0-66.873-14.528-89.43-37.92z"
              />
              <Path stroke="#EFEFEF" strokeWidth={0.8} d="M125.159 206.5V252" />
              <Path
                fillRule="evenodd"
                clipRule="evenodd"
                d="M79 220c0-1.105.853-2 1.905-2h16.19c1.052 0 1.905.895 1.905 2s-.853 2-1.905 2h-16.19c-1.052 0-1.905-.895-1.905-2z"
                fill="#EFEFEF"
              />
              <Path
                d="M173.094 219.096H166.905V212.905C166.905 211.853 166.052 211 165 211C163.947 211 163.094 211.853 163.094 212.905V219.096H156.905C155.853 219.096 155 219.949 155 221C155 222.052 155.853 222.905 156.905 222.905H163.094V229.096C163.094 230.148 163.947 231.001 165 231.001C166.052 231.001 166.905 230.148 166.905 229.096V222.905H173.094C174.146 222.905 174.999 222.052 174.999 221C174.999 219.949 174.146 219.096 173.094 219.096V219.096Z"
                fill="#EFEFEF"
              />
            </Svg>
          )}
        </View>

        <View style={styles.overlayContainer}>
          <View style={{ width: scaledSize + 80, height: scaledSize + 80 }}>
            {/* Controls layer */}
            <Svg width="100%" height="100%" style={styles.controlLayer}>
              {/* Mode buttons overlay */}
              {availableModes.map((mode, index) => (
                <Rect
                  x={coords[index].x}
                  y={coords[index].y}
                  width={42 * ratio}
                  height={42 * ratio}
                  fill="rgba(0, 0, 0, 0)"
                  onPress={() => setMode(modeEnum[mode])}
                  key={mode}
                />
              ))}
              {hasController && (
                <G x={40} y={80}>
                  <G
                    x={points.x - strokeWidth / 2}
                    y={points.y - strokeWidth / 2}>
                    {showPopup && (
                      <G x={-10 * ratio} y={-82}>
                        <AnimatedCircle
                          cx={32 * ratio}
                          cy={32}
                          r={28}
                          stroke={Colors.darkGrey}
                          strokeWidth={4}
                          fill="white"
                          fillOpacity={fadeAnim}
                          strokeOpacity={fadeAnim}
                        />
                        <G x={((ratio * 100 - 100) / 25) * 8}>
                          <AnimatedPath
                            fillOpacity={fadeAnim}
                            fill={Colors.darkGrey}
                            d="M23,66 C26.0944671,67.0345727 29.3953072,67.5934621 32.8221828,67.5934621 C36.0122784,67.5934621 39.0931538,67.1091374 42,66.2076109 L32.5813753,80 L23,66 Z"
                          />
                        </G>
                        <AnimatedG fillOpacity={fadeAnim}>
                          <Text
                            fill="#59595C"
                            fontSize={ScaleText(24)}
                            fontWeight="400"
                            x={32 * ratio}
                            y={34}
                            textAnchor="middle"
                            alignmentBaseline="middle">
                            {round(temperature)}
                          </Text>
                        </AnimatedG>
                      </G>
                    )}
                    <Circle
                      {...(!showLeafAnim && panResponder.panHandlers)}
                      cx={strokeWidth / 2}
                      cy={strokeWidth / 2}
                      r={strokeWidth / 2}
                      fill="#fff"
                    />
                  </G>
                  <Rect
                    x={70 * ratio}
                    y={205 * ratio}
                    width={30 * ratio}
                    height={30 * ratio}
                    fill="rgba(0,0,0,0)"
                    disabled={showLeafAnim}
                    onPress={() => onDecrement()}
                  />
                  <Rect
                    x={155 * ratio}
                    y={205 * ratio}
                    width={30 * ratio}
                    height={30 * ratio}
                    fill="rgba(0,0,0,0)"
                    disabled={showLeafAnim}
                    onPress={() => onIncrement()}
                  />
                  <AnimatedG
                    fillOpacity={fadeAnim.interpolate({
                      inputRange: [0, 0.9],
                      outputRange: [1, 0],
                    })}>
                    <AnimatedText
                      fill={textFill}
                      fontSize={ScaleText(60)}
                      fontWeight="600"
                      x={cx}
                      y={cy}
                      textAnchor="middle"
                      alignmentBaseline="middle">
                      {round(temperature)}
                    </AnimatedText>
                  </AnimatedG>
                  <Text
                    fill={textFill}
                    fontSize={ScaleText(22)}
                    fontWeight="300"
                    x={cx + 51 * ratio}
                    y={cy + 10 * ratio}
                    textAnchor="middle"
                    alignmentBaseline="middle">
                    °C
                  </Text>
                  <Line
                    x1={cx - 34}
                    y1={cy * 1.2}
                    x2={cx + 38}
                    y2={cy * 1.2}
                    stroke={Colors.veryLightGrey}
                    strokeWidth={0.1}
                  />
                  <Text
                    fill={textFill}
                    fontSize={ScaleText(18)}
                    fontWeight="300"
                    x={cx}
                    y={cy + 45 * ratio}
                    textAnchor="middle"
                    alignmentBaseline="middle">
                    {getModeLabel(
                      modeType.charAt(0).toUpperCase() + modeType.slice(1)
                    )}
                  </Text>
                </G>
              )}
            </Svg>
          </View>
        </View>
        {showLeafAnim && (
          <TouchableOpacity
            onPress={handleLeafOnPress}
            style={[
              styles.leafTouchable,
              {
                width: scaledSize - 30,
                height: scaledSize - 30,
                borderRadius: scaledSize / 2,
              },
            ]}
          />
        )}
      </View>
    </>
  );
};

Slider.defaultProps = {
  minTemp: 18,
  maxTemp: 32,
  minTempColor: '#0576BC',
  maxTempColor: '#76CEF6',
  availableModes: [],
};

Slider.propTypes = {
  scaledSize: PropTypes.number,
  ratio: PropTypes.number,
  cx: PropTypes.number,
  cy: PropTypes.number,
  modeType: PropTypes.string,
  minTemp: PropTypes.number,
  maxTemp: PropTypes.number,
  minTempColor: PropTypes.string,
  maxTempColor: PropTypes.string,
  hasController: PropTypes.bool,
  isOff: PropTypes.bool,
  fill: PropTypes.string,
  setMode: PropTypes.func,
  setTemp: PropTypes.func,
  acTemp: PropTypes.number,
  colorRange: PropTypes.array,
  availableModes: PropTypes.array,
  desired: PropTypes.object,
  clearDesireTemp: PropTypes.func,
  showLeafAnim: PropTypes.bool,
};
const mapDispatchToProps = dispatch => ({
  clearDesireTemp: () => {
    dispatch(remote.clearDesireTemp());
  },
});

export default connect(null, mapDispatchToProps)(Slider);
