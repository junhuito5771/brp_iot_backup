import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image } from 'react-native';
import { Colors } from '@module/daikin-ui';
import getModeIconByKey from '../../helpers/getModeIconByKey';

const styles = StyleSheet.create({
  modeContainer: {
    position: 'relative',
    height: 80,
    flexDirection: 'row',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  imageContainer: {
    position: 'absolute',
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function getModeIcon(modeType, curMode) {
  const modeIcon = getModeIconByKey(curMode);

  return modeType.toLowerCase() === curMode
    ? modeIcon.active
    : modeIcon.inactive;
}

export default function ModeSelector({
  ratio,
  scaledSize,
  modeType,
  availableModes,
}) {
  const iconPosition = {
    1: {
      position: [{ bottom: 14 * ratio, left: 143 * ratio }],
      separator: [
        {
          leftBorderStyle: {
            right: -28,
            bottom: -30,
            transform: [{ rotate: '13deg' }],
          },
          rightBorderStyle: {
            left: -28,
            bottom: 30,
            transform: [{ rotate: '-14deg' }],
          },
        },
      ],
    },
    2: {
      position: [
        { bottom: 5 * ratio, left: 116 * ratio },
        { bottom: 23 * ratio, left: 170 * ratio },
      ],
      separator: [
        {
          leftBorderStyle: {
            right: -28,
            bottom: -30,
            transform: [{ rotate: '0deg' }],
          },
          rightBorderStyle: {
            left: -30,
            bottom: 25,
            transform: [{ rotate: '-14deg' }],
          },
        },
        {
          leftBorderStyle: {
            right: -28,
            bottom: -40,
            transform: [{ rotate: '13deg' }],
          },
        },
      ],
    },
    3: {
      position: [
        { bottom: 5 * ratio, left: 88 * ratio },
        { bottom: 14 * ratio, left: 143 * ratio },
        { bottom: 5 * ratio, left: 198 * ratio },
      ],
      separator: [
        {
          leftBorderStyle: {
            right: -28,
            bottom: -29,
            transform: [{ rotate: '-20deg' }],
          },
          rightBorderStyle: {
            left: -24,
            bottom: 18,
            transform: [{ rotate: '-40deg' }],
          },
        },
        {},
        {
          leftBorderStyle: {
            right: -25,
            bottom: -48,
            transform: [{ rotate: '36deg' }],
          },
          rightBorderStyle: {
            left: -30,
            bottom: 37,
            transform: [{ rotate: '14deg' }],
          },
        },
      ],
    },
    4: {
      position: [
        { bottom: 0 * ratio, left: 64 * ratio },
        { bottom: 30 * ratio, left: 116 * ratio },
        { bottom: 14 * ratio, left: 170 * ratio },
        { bottom: 0 * ratio, left: 224 * ratio },
      ],
      separator: [
        {
          leftBorderStyle: {
            right: -28,
            bottom: -25,
            transform: [{ rotate: '-12deg' }],
          },
          rightBorderStyle: {
            left: -24,
            bottom: 16,
            transform: [{ rotate: '-40deg' }],
          },
        },
        {
          leftBorderStyle: {
            right: -28,
            bottom: -30,
            transform: [{ rotate: '0deg' }],
          },
        },
        {},
        {
          leftBorderStyle: {
            right: -25,
            bottom: -47,
            transform: [{ rotate: '36deg' }],
          },
          rightBorderStyle: {
            left: -28,
            bottom: 38,
            transform: [{ rotate: '8deg' }],
          },
        },
      ],
    },
    5: {
      position: [
        { bottom: -28 * ratio, left: 38 * ratio },
        { bottom: 5 * ratio, left: 88 * ratio },
        { bottom: 14 * ratio, left: 143 * ratio },
        { bottom: 5 * ratio, left: 198 * ratio },
        { bottom: -28 * ratio, left: 248 * ratio },
      ],
      separator: [
        {
          leftBorderStyle: {
            right: -25,
            bottom: -13,
            transform: [{ rotate: '-32deg' }],
          },
          rightBorderStyle: {
            left: -20,
            bottom: 10,
            transform: [{ rotate: '-52deg' }],
          },
        },
        {},
        {
          leftBorderStyle: {
            right: -30,
            bottom: -35,
            transform: [{ rotate: '14deg' }],
          },
          rightBorderStyle: {
            left: -28,
            bottom: 30,
            transform: [{ rotate: '-14deg' }],
          },
        },
        {},
        {
          leftBorderStyle: {
            right: -20,
            bottom: -53,
            transform: [{ rotate: '55deg' }],
          },
          rightBorderStyle: {
            left: -24,
            bottom: 50,
            transform: [{ rotate: '34deg' }],
          },
        },
      ],
    },
  };

  const { position, separator } = iconPosition[availableModes.length];

  return (
    <View style={[styles.modeContainer, { width: scaledSize + 80 }]}>
      {availableModes.map((mode, index) => (
        <ModeButton
          src={getModeIcon(modeType, mode)}
          containerStyle={[
            styles.imageContainer,
            { height: 42 * ratio, width: 42 * ratio },
            position[index],
          ]}
          leftBorderStyle={separator[index].leftBorderStyle}
          rightBorderStyle={separator[index].rightBorderStyle}
          key={mode}
        />
      ))}
    </View>
  );
}

ModeSelector.propTypes = {
  ratio: PropTypes.number,
  scaledSize: PropTypes.number,
  modeType: PropTypes.string,
  availableModes: PropTypes.array,
};

const ModeButton = ({
  containerStyle,
  src,
  leftBorderStyle,
  rightBorderStyle,
}) => (
  <View style={containerStyle}>
    {leftBorderStyle && (
      <View
        style={[
          {
            height: '100%',
            borderWidth: 0.5,
            borderColor: Colors.modeSeparatorGrey,
          },
          leftBorderStyle,
        ]}
      />
    )}
    <Image source={src} />
    {rightBorderStyle && (
      <View
        style={[
          {
            height: '100%',
            borderWidth: 0.5,
            borderColor: Colors.modeSeparatorGrey,
          },
          rightBorderStyle,
        ]}
      />
    )}
  </View>
);

ModeButton.propTypes = {
  containerStyle: PropTypes.array.isRequired,
  src: Image.propTypes.source.isRequired,
  leftBorderStyle: PropTypes.object,
  rightBorderStyle: PropTypes.object,
};
