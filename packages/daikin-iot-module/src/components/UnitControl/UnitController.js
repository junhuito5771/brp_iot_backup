/* eslint-disable no-mixed-operators */
/* eslint-disable no-underscore-dangle */
import React from 'react';
import PropTypes from 'prop-types';
import { View, ActivityIndicator } from 'react-native';
import { widthPercentage } from '@module/utility';
import { Colors } from '@module/daikin-ui';
import { HandsetRuleHelper } from '@daikin-dama/redux-iot';

import ModeSelector from './modeSelector';
import UnitSwitch from './unitSwitch';
import Slider from './slider';

const scaledSize = widthPercentage(68);
const ratio = scaledSize / 252;
const cx = scaledSize / 2;
const cy = scaledSize / 2;

const config = {
  auto: {
    hasController: true,
    minTemp: 18,
    maxTemp: 30,
    colorRange: [
      '#76CEF6',
      '#74CCF5',
      '#69C6F1',
      '#5FBFEE',
      '#59BCEC',
      '#54B9EA',
      '#44AEE3',
      '#329ED8',
      '#208ECD',
      '#0576BC',
      '#DD3827',
      '#DC3726',
      '#DB3526',
    ],
  },
  cool: {
    hasController: true,
    minTemp: 18,
    maxTemp: 32,
    minTempColor: '#0576BC',
    maxTempColor: '#76CEF6',
  },
  heat: {
    hasController: true,
    minTemp: 10,
    maxTemp: 30,
    minTempColor: '#FC805B',
    maxTempColor: '#DB3526',
  },
  fan: {
    hasController: false,
    fill: Colors.fanModeFill,
  },
  dry: {
    hasController: false,
    fill: Colors.dryModeFill,
    minTempColor: '#488bf1',
    maxTempColor: '#b6eafe',
  },
};

const getConfig = (handsetType, modeValue) => {
  const tempMinMax = HandsetRuleHelper.getTempMinMax(modeValue, handsetType);
  const hasController = !!tempMinMax;
  const sliderProps = config[modeValue];

  return {
    ...sliderProps,
    hasController,
    ...(hasController && { minTemp: tempMinMax.min, maxTemp: tempMinMax.max }),
  };
};

const UnitController = ({
  unit: { mode, switch: switchType, modeConfig, modes, handsetType },
  loadingContainer,
  flexStyle,
  setMode,
  setTemp,
  isLoading,
  desired,
}) => {
  const availableModes = Object.keys(modes).filter(
    k => !modes[k] && HandsetRuleHelper.isModeAllowed(k, handsetType)
  );

  const showLeafAnim =
    modeConfig && (modeConfig.ecoplus === 1 || modeConfig.smartEcoMax === 1);
  return (
    <View style={flexStyle}>
      <UnitSwitch />
      <View style={loadingContainer}>
        {isLoading && <ActivityIndicator size="small" />}
      </View>
      <View>
        <ModeSelector
          {...{
            ratio,
            scaledSize,
            modeType: mode,
            availableModes,
          }}
        />
        <Slider
          isOff={switchType === 0}
          {...{
            setTemp,
            setMode,
            modeType: mode,
            scaledSize,
            ratio,
            cx,
            cy,
            availableModes,
            ...getConfig(handsetType, mode),
            ...modeConfig,
            desired,
            showLeafAnim,
          }}
        />
      </View>
    </View>
  );
};

UnitController.propTypes = {
  unit: PropTypes.object,
  flexStyle: PropTypes.object,
  loadingContainer: PropTypes.object,
  modes: PropTypes.object,
  setMode: PropTypes.func,
  setTemp: PropTypes.func,
  mode: PropTypes.string,
  isLoading: PropTypes.bool,
  desired: PropTypes.object,
};

export default UnitController;
