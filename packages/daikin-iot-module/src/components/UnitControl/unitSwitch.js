import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import { connect, remote } from '@daikin-dama/redux-iot';
import {
  ScaleText,
  SetIndoorTemperature,
  viewportWidth,
  widthPercentage,
} from '@module/utility';
import { Text, Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  headerRightStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  alignTextCenter: {
    textAlign: 'center',
  },
  textStyle: { padding: 10, color: Colors.lightWarmGrey },
  flexDirectionRow: {
    flexDirection: 'row',
  },
  flexDirectionColumn: {
    flexDirection: 'column',
  },
  indicatorPadding: {
    paddingTop: 5,
  },
  controlContainer: {
    paddingHorizontal: 18,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: viewportWidth,
  },
  indicatorContainer: {
    flexDirection: 'row',
    alignItems: Platform.OS === 'ios' ? 'baseline' : 'center',
    justifyContent: 'center',
  },
  indicatorContainerMargin: {
    marginRight: 15,
  },
  indicatorMarginRight: {
    marginRight: 5,
  },
  indicatorTextStyle: {
    color: Colors.lightWarmGrey,
    fontWeight: '100',
    fontSize: ScaleText(19),
  },
  switchButton: {
    width: widthPercentage(15),
    height: widthPercentage(15),
  },
});

const onButtons = [
  require('../../shared/assets/controller/turnOn.png'),
  require('../../shared/assets/controller/on.png'),
];

const offButtons = [
  require('../../shared/assets/controller/off.png'),
  require('../../shared/assets/controller/turnOff.png'),
];

const switchValues = {
  ON: 1,
  OFF: 0,
};

const UnitSwitch = ({
  unit: {
    indoorTemp,
    switch: switchType,
    thingName,
    powerConsumption,
    outdoorTemp,
  },
  switchAc,
}) => {
  React.useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <>
      <View style={styles.controlContainer}>
        <TouchableOpacity
          onPress={() => switchAc(thingName, switchValues.OFF)}
          disabled={switchType === switchValues.OFF}>
          <Image source={offButtons[switchType]} style={styles.switchButton} />
        </TouchableOpacity>
        <View style={[styles.flexDirectionColumn, styles.indicatorPadding]}>
          <View style={styles.flexDirectionRow}>
            {!!indoorTemp && (
              <View
                style={[
                  styles.indicatorContainer,
                  styles.indicatorContainerMargin,
                ]}>
                <Image
                  style={styles.indicatorMarginRight}
                  source={require('../../shared/assets/controller/indoorTemp.png')}
                />
                <Text style={styles.indicatorTextStyle}>
                  {SetIndoorTemperature(indoorTemp)}
                </Text>
              </View>
            )}
            {!!outdoorTemp && (
              <View style={styles.indicatorContainer}>
                <Image
                  style={styles.indicatorMarginRight}
                  source={require('../../shared/assets/remote/outdoorTempMediumGrey.png')}
                />
                <Text
                  style={styles.indicatorTextStyle}>{`${outdoorTemp}°C`}</Text>
              </View>
            )}
          </View>
          {!!powerConsumption && (
            <View style={styles.indicatorContainer}>
              <Image
                style={styles.indicatorMarginRight}
                source={require('../../shared/assets/remote/energyCon.png')}
              />
              <Text style={styles.indicatorTextStyle}>{`${
                powerConsumption > 0
                  ? (powerConsumption / 1000).toFixed(1)
                  : powerConsumption
              }kW`}</Text>
            </View>
          )}
        </View>
        <TouchableOpacity
          onPress={() => switchAc(thingName, switchValues.ON)}
          disabled={switchType === switchValues.ON}>
          <Image source={onButtons[switchType]} style={styles.switchButton} />
        </TouchableOpacity>
      </View>
    </>
  );
};

UnitSwitch.propTypes = {
  unit: PropTypes.object,
  switchAc: PropTypes.func,
};

UnitSwitch.defaultProps = {
  unit: {},
};

const mapStateToProps = ({ remote: { thingName }, units: { allUnits } }) => ({
  unit: allUnits[thingName],
});

const mapDispatchToProps = dispatch => ({
  switchAc: (thingName, switchType) => {
    dispatch(remote.setAcSwitch(thingName, switchType));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UnitSwitch);
