import React, { memo, useEffect, useState } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import momentDurationFormat from 'moment-duration-format';
import { widthPercentage } from '@module/utility';

import { Text, Colors } from '@module/daikin-ui';

momentDurationFormat(moment);

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    backgroundColor: Colors.skyBlue,
    flexDirection: 'row',
    paddingVertical: 4,
    width: widthPercentage(60),
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginVertical: 5,
  },
  image: {
    marginRight: 5,
  },
  countDownText: {
    color: Colors.white,
  },
});

function QuickTimerInfo({ 
  hour,
  minute,
  switchValue,
  submitQuickTimer,
  thingName,
 }) {
  const [duration, setDuration] = useState(0);

  useEffect(() => {
    if (hour > 0 && minute > -1) {
      const curDT = moment.now();
      const targetDR = moment({ hour, minute });

      setDuration(moment.duration(targetDR.diff(curDT)).asMilliseconds());
    }
  }, [hour, minute]);

  const checkExpiredTime = moment().isAfter(moment({ hour, minute }));

  useEffect(() => {
    if (checkExpiredTime) {
      submitQuickTimer(false, { thingName });
    }
  }, [checkExpiredTime]);

  useEffect(() => {
    const intervalHandler = setInterval(() => {
      setDuration(prevDuration => prevDuration - 1000);
    }, 1000);

    return () => {
      clearInterval(intervalHandler);
    };
  }, []);

  const momentDuration = moment.duration(duration, 'milliseconds');

  const momentText =
    momentDuration.hours() > 0
      ? momentDuration.format('h [hours] m [minutes]')
      : momentDuration.format('m [minutes] s [seconds]');

  const switchText = switchValue === 1 ? 'ON' : 'OFF';
  return (
    duration > 0 && (
      <View style={styles.container}>
        <Image
          style={styles.image}
          source={require('../../shared/assets/remote/quickTimerIcon.png')}
        />
        <Text
          style={styles.countDownText}>{`${switchText} in ${momentText}`}</Text>
      </View>
    )
  );
}

QuickTimerInfo.propTypes = {
  hour: PropTypes.number,
  minute: PropTypes.number,
  switchValue: PropTypes.number,
  submitQuickTimer: PropTypes.func,
  thingName: PropTypes.string,
};

export default memo(QuickTimerInfo);
