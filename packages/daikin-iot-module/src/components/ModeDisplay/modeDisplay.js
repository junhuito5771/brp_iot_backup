import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Image } from 'react-native';
import Svg, { Circle, Text } from 'react-native-svg';
import { ScaleText } from '@module/utility';
import { Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  overlayContainer: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
  },
  svgContainer: {
    margin: 24,
    alignSelf: 'center',
  },
  textStyle: {
    fontSize: ScaleText(20),
  },
  centerText: {
    textAlign: 'center',
  },
});

const ModeDisplay = ({ cx, cy, scaledSize, mode, isOff, fill }) => (
  <>
    <Svg width="100%" height="100%">
      <Circle
        fill={isOff ? Colors.unitOffGrey : fill}
        r={scaledSize / 2}
        {...{ cx, cy }}
        fillOpacity={0.4}
      />
    </Svg>
    <View
      style={[
        styles.overlayContainer,
        {
          width: scaledSize,
          height: scaledSize,
        },
      ]}>
      <Image
        source={require('./overlayShadow.png')}
        style={{
          width: scaledSize,
          height: scaledSize,
        }}
      />
    </View>
    <View style={styles.overlayContainer}>
      <View style={{ width: scaledSize, height: scaledSize }}>
        <Svg
          width={scaledSize}
          height={scaledSize}
          style={{ ...StyleSheet.absoluteFillObject }}>
          <Text
            fill={isOff ? Colors.unitOffGrey : fill}
            fontSize={ScaleText(20)}
            x={cx}
            y={cy}
            textAnchor="middle"
            alignmentBaseline="middle">
            {mode === 'dry' ? 'Dry' : 'Fan'}
          </Text>
        </Svg>
      </View>
    </View>
  </>
);

ModeDisplay.propTypes = {
  cx: PropTypes.number.isRequired,
  cy: PropTypes.number.isRequired,
  scaledSize: PropTypes.number.isRequired,
  mode: PropTypes.string.isRequired,
  isOff: PropTypes.bool,
  fill: PropTypes.string.isRequired,
};

ModeDisplay.defaultProps = {
  isOff: false,
};

export default ModeDisplay;
