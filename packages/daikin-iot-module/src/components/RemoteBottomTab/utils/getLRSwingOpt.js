import { RemoteHelper } from '@daikin-dama/redux-iot';
import { SwingOptions } from '../../../constants/remoteModal';

export default function getSwingOpt({
  setSwingLR,
  enableLRStep,
  enableLRSwing,
}) {
  const isAdvSwing = enableLRStep && enableLRSwing;

  if (isAdvSwing) {
    const options = [
      {
        label: 'Off',
        value: RemoteHelper.SWING_LR_OFF,
        icons: SwingOptions.SWING_LR_OFF,
        onPress: () => setSwingLR(RemoteHelper.SWING_LR_OFF),
      },
      {
        label: 'Left Swing Full Swing',
        value: RemoteHelper.SWING_LR_FULL,
        icons: SwingOptions.SWING_LR_FULL,
        onPress: () => setSwingLR(RemoteHelper.SWING_LR_FULL),
      },
    ];

    for (let i = 1; i <= 5; i += 1) {
      const label = `Left Right ${i} Swing`;
      const template = 'SWING_LR_LV';
      const value = RemoteHelper[`${template}${i}`];
      const icons = SwingOptions[`${template}${i}`];
      const onPress = () => setSwingLR(RemoteHelper[`${template}${i}`]);
      options.push({ label, value, icons, onPress });
    }

    return options;
  }

  return null;
}
