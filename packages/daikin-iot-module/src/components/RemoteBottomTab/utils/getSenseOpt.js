import { SenseOptions } from '../../../constants/remoteModal';

export default function getBreezeOpt({ setSense }) {
  const options = [
    {
      label: 'Off',
      value: 0,
      icons: SenseOptions.SENSE_OFF,
      onPress: () => setSense(0),
    },
    {
      label: 'Sense',
      value: 1,
      icons: SenseOptions.SENSE_ON,
      onPress: () => setSense(1),
    },
  ];

  return options;
}
