import { RemoteHelper } from '@daikin-dama/redux-iot';
import { ckSwingOptions } from '../../../constants/remoteModal';

export default function getBreezeOpt({ setCKSwing }) {
  const { CKSWING_FULL, CKSWING_DRAFT, CKSWING_SOIL } = RemoteHelper;
  const options = [
    {
      label: 'Standard/Off',
      value: CKSWING_FULL,
      icons: ckSwingOptions.CK_SWING_FULL,
      onPress: () => setCKSwing(CKSWING_FULL),
    },
    {
      label: 'Cold Draft Prevention',
      value: CKSWING_DRAFT,
      icons: ckSwingOptions.CK_SWING_DRAFT,
      onPress: () => setCKSwing(CKSWING_DRAFT),
    },
    {
      label: 'Ceiling Soil Prevention',
      value: CKSWING_SOIL,
      icons: ckSwingOptions.CK_SWING_SOIL,
      onPress: () => setCKSwing(CKSWING_SOIL),
    },
  ];

  return options;
}
