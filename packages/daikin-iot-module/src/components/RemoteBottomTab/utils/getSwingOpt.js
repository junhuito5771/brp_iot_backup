import { RemoteHelper, HandsetRuleHelper } from '@daikin-dama/redux-iot';
import { SwingOptions } from '../../../constants/remoteModal';

export default function getSwingOpt({
  mode,
  handsetType,
  setSwing,
  enableUDStep,
  enableLRStep,
  enableLRSwing,
}) {
  if (
    !HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_SWING,
      handsetType
    )
  ) {
    return null;
  }

  const isAdvSwing = enableUDStep && enableLRStep && enableLRSwing;

  if (isAdvSwing) {
    return [
      {
        label: 'Off',
        value: RemoteHelper.SWING_UD_OFF,
        icons: SwingOptions.SWING_UD_OFF,
        onPress: () => setSwing(RemoteHelper.SWING_UD_OFF),
      },
      {
        label: 'Up Down Full Swing',
        value: RemoteHelper.SWING_UD_FULL,
        icons: SwingOptions.SWING_UD_FULL,
        onPress: () => setSwing(RemoteHelper.SWING_UD_FULL),
      },
      {
        label: 'Up Down Step 1 Swing',
        value: RemoteHelper.SWING_UD_LV1,
        icons: SwingOptions.SWING_UD_LV1,
        onPress: () => setSwing(RemoteHelper.SWING_UD_LV1),
      },
      {
        label: 'Up Down Step 2 Swing',
        value: RemoteHelper.SWING_UD_LV2,
        icons: SwingOptions.SWING_UD_LV2,
        onPress: () => setSwing(RemoteHelper.SWING_UD_LV2),
      },
      {
        label: 'Up Down Step 3 Swing',
        value: RemoteHelper.SWING_UD_LV3,
        icons: SwingOptions.SWING_UD_LV3,
        onPress: () => setSwing(RemoteHelper.SWING_UD_LV3),
      },
      {
        label: 'Up Down Step 4 Swing',
        value: RemoteHelper.SWING_UD_LV4,
        icons: SwingOptions.SWING_UD_LV4,
        onPress: () => setSwing(RemoteHelper.SWING_UD_LV4),
      },
      {
        label: 'Up Down Step 5 Swing',
        value: RemoteHelper.SWING_UD_LV5,
        icons: SwingOptions.SWING_UD_LV5,
        onPress: () => setSwing(RemoteHelper.SWING_UD_LV5),
      },
    ];
  }

  const options = [
    {
      label: 'Off',
      value: RemoteHelper.SWING_OFF,
      icons: SwingOptions.SWING_OFF,
      onPress: () => setSwing(RemoteHelper.SWING_OFF),
    },
    {
      label: 'Full swing',
      value: RemoteHelper.SWING_ON,
      icons: SwingOptions.SWING_ON,
      onPress: () => setSwing(RemoteHelper.SWING_ON),
    },
  ];

  return options;
}
