import { RemoteHelper, HandsetRuleHelper } from '@daikin-dama/redux-iot';
import { TurboOptions } from '../../../constants/remoteModal';

export default function getTurboOpt({
  enableSmartTurbo,
  mode,
  handsetType,
  setTurbo,
}) {
  if (
    !HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_TURBO,
      handsetType
    )
  ) {
    return null;
  }

  const options = [
    {
      label: 'Off',
      value: RemoteHelper.TURBO_OFF,
      icons: TurboOptions.TURBO_OFF,
      onPress: () => setTurbo(RemoteHelper.TURBO_OFF),
    },
  ];

  if (enableSmartTurbo) {
    options.push({
      label: 'Smart powerful+',
      value: RemoteHelper.TURBO_SMART_ON,
      icons: TurboOptions.TURBO_SMART,
      onPress: () => setTurbo(RemoteHelper.TURBO_SMART_ON),
    });
  } else {
    options.push({
      label: 'Powerful',
      value: RemoteHelper.TURBO_ON,
      icons: TurboOptions.TURBO_ON,
      onPress: () => setTurbo(RemoteHelper.TURBO_ON),
    });
  }

  return options;
}
