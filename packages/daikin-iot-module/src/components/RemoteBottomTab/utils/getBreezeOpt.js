import { BreezeOptions } from '../../../constants/remoteModal';

export default function getBreezeOpt({ setBreeze }) {
  const options = [
    {
      label: 'Off',
      value: 0,
      icons: BreezeOptions.BREEZE_OFF,
      onPress: () => setBreeze(0),
    },
    {
      label: 'Breeze',
      value: 1,
      icons: BreezeOptions.BREEZE_ON,
      onPress: () => setBreeze(1),
    },
  ];

  return options;
}
