import { RemoteHelper } from '@daikin-dama/redux-iot';
import { ledOptions } from '../../../constants/remoteModal';

export default function getBreezeOpt({ enableLED, enablePowerInd, setLED }) {
  const options = [
    {
      label: 'Off',
      value: RemoteHelper.LED_OFF,
      icons: ledOptions.LED_OFF,
      onPress: () => setLED(RemoteHelper.LED_OFF),
    },
  ];

  if (enableLED) {
    options.push({
      label: 'LED',
      value: RemoteHelper.LED_ON,
      icons: ledOptions.LED_ON,
      onPress: () => setLED(RemoteHelper.LED_ON),
    });
  }

  if (enablePowerInd) {
    options.push({
      label: 'Power Usage Indication',
      value: RemoteHelper.POWER_IND,
      icons: ledOptions.POWER_IND,
      onPress: () => setLED(RemoteHelper.POWER_IND),
    });
  }

  return options;
}
