import { RemoteHelper } from '@daikin-dama/redux-iot';
import { EcoplusOptions } from '../../../constants/remoteModal';

export default function getEcoplus({
  enableSmartEcomax,
  setEcoplus,
  handsetType,
}) {
  const options = [
    {
      label: 'Off',
      value: RemoteHelper.ECOPLUS_OFF,
      icons: EcoplusOptions.ECOPLUS_OFF,
      onPress: () => setEcoplus(RemoteHelper.ECOPLUS_OFF),
    },
  ];

  if (enableSmartEcomax && handsetType === RemoteHelper.HANDSET_DAMA) {
    options.push({
      label: 'Smart Ecomax',
      value: RemoteHelper.ECO_SMART,
      icons: EcoplusOptions.ECOPLUS_SMART,
      onPress: () => setEcoplus(RemoteHelper.ECO_SMART),
    });
  } else {
    const isDAMA = handsetType === RemoteHelper.HANDSET_DAMA;
    options.push({
      label: isDAMA ? 'Eco+' : 'ECONO',
      value: RemoteHelper.ECOPLUS_ON,
      icons: isDAMA ? EcoplusOptions.ECOPLUS_ON : EcoplusOptions.ECONO_ON,
      onPress: () => setEcoplus(RemoteHelper.ECOPLUS_ON),
    });
  }

  return options;
}
