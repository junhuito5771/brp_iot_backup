import { SmartDriftOptions } from '../../../constants/remoteModal';

export default function getSleepOpt({ setSmartDrift }) {
  return [
    {
      label: 'Off',
      value: 0,
      icons: SmartDriftOptions.SMART_DRIFT_OFF,
      onPress: () => setSmartDrift(0),
    },
    {
      label: 'Smart drift',
      value: 1,
      icons: SmartDriftOptions.SMART_DRIFT_ON,
      onPress: () => setSmartDrift(1),
    },
  ];
}
