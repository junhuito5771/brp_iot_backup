import { RemoteHelper, HandsetRuleHelper } from '@daikin-dama/redux-iot';
import { FanSpeedOptions } from '../../../constants/remoteModal';

export default function getFanSpdOpt({
  enableAutoFan,
  enableSilent,
  enableLowFan,
  mode,
  handsetType,
  setFanSpeed,
}) {
  const {
    FEAT_SILENT,
    FANSPEED_AUTO,
    FANSPEED_SILENT,
    FANSPEED_LV1,
    FANSPEED_LV2,
    FANSPEED_LV3,
    FANSPEED_LV4,
    FANSPEED_LV5,
  } = RemoteHelper;

  const { isFanSpeedOptAllowed, isFeatureAllowed } = HandsetRuleHelper;

  const fanSpeedOpts = [];

  if (enableAutoFan && isFanSpeedOptAllowed(mode, FANSPEED_AUTO, handsetType)) {
    fanSpeedOpts.push({
      label: 'Auto',
      value: FANSPEED_AUTO,
      icons: FanSpeedOptions.FAN_AUTO,
      onPress: () => setFanSpeed(FANSPEED_AUTO),
    });
  }

  if (enableSilent && isFeatureAllowed(mode, FEAT_SILENT, handsetType)) {
    fanSpeedOpts.push({
      label: 'Quiet',
      value: FANSPEED_SILENT,
      icons: FanSpeedOptions.FAN_SILENT,
      onPress: () => setFanSpeed(FANSPEED_SILENT),
    });
  }

  if (enableLowFan && isFanSpeedOptAllowed(mode, FANSPEED_LV1, handsetType)) {
    fanSpeedOpts.push({
      label: 'Level 1',
      value: FANSPEED_LV1,
      icons: FanSpeedOptions.FAN_LV1,
      onPress: () => setFanSpeed(FANSPEED_LV1),
    });
  }

  if (isFanSpeedOptAllowed(mode, FANSPEED_LV2, handsetType)) {
    fanSpeedOpts.push({
      label: 'Level 2',
      value: FANSPEED_LV2,
      icons: FanSpeedOptions.FAN_LV2,
      onPress: () => setFanSpeed(FANSPEED_LV2),
    });
  }

  if (isFanSpeedOptAllowed(mode, FANSPEED_LV3, handsetType)) {
    fanSpeedOpts.push({
      label: 'Level 3',
      value: FANSPEED_LV3,
      icons: FanSpeedOptions.FAN_LV3,
      onPress: () => setFanSpeed(FANSPEED_LV3),
    });
  }

  if (isFanSpeedOptAllowed(mode, FANSPEED_LV4, handsetType)) {
    fanSpeedOpts.push({
      label: 'Level 4',
      value: FANSPEED_LV4,
      icons: FanSpeedOptions.FAN_LV4,
      onPress: () => setFanSpeed(FANSPEED_LV4),
    });
  }

  if (isFanSpeedOptAllowed(mode, FANSPEED_LV5, handsetType)) {
    fanSpeedOpts.push({
      label: 'Level 5',
      value: FANSPEED_LV5,
      icons: FanSpeedOptions.FAN_LV5,
      onPress: () => setFanSpeed(FANSPEED_LV5),
    });
  }

  return fanSpeedOpts;
}
