import { RemoteHelper } from '@daikin-dama/redux-iot';
import { SleepOptions } from '../../../constants/remoteModal';

export default function getSleepOpt({
  enableSleep,
  enableSmartSleep,
  setSleep,
}) {
  const options = [
    {
      label: 'Off',
      value: RemoteHelper.SLEEP_OFF,
      icons: SleepOptions.SLEEP_OFF,
      onPress: () => setSleep(RemoteHelper.SLEEP_OFF),
    },
  ];

  if (enableSmartSleep) {
    options.push({
      label: 'Smart Sleep+',
      value: RemoteHelper.SLEEP_SMART,
      icons: SleepOptions.SLEEP_SMART,
      onPress: () => setSleep(RemoteHelper.SLEEP_SMART),
    });
  } else if (enableSleep) {
    options.push({
      label: 'Sleep',
      value: RemoteHelper.SLEEP_ON,
      icons: SleepOptions.SLEEP_ON,
      onPress: () => setSleep(RemoteHelper.SLEEP_ON),
    });
  }

  return options;
}
