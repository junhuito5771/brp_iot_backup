import { StreamerOptions } from '../../../constants/remoteModal';

export default function getBreezeOpt({ setStreamer }) {
  const options = [
    {
      label: 'Off',
      value: 0,
      icons: StreamerOptions.STREAMER_OFF,
      onPress: () => setStreamer(0),
    },
    {
      label: 'Streamer',
      value: 1,
      icons: StreamerOptions.STREAMER_ON,
      onPress: () => setStreamer(1),
    },
  ];

  return options;
}
