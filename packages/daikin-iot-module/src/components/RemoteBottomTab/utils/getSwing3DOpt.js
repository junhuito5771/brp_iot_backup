import { SwingOptions } from '../../../constants/remoteModal';

export default function getSwing3DOpt({ setSwing3D }) {
  return [
    {
      label: 'Off',
      value: 0,
      icons: SwingOptions.SWING_3D_OFF,
      onPress: () => setSwing3D(0),
    },
    {
      label: '3D Airflow',
      value: 1,
      icons: SwingOptions.SWING_3D_ON,
      onPress: () => setSwing3D(1),
    },
  ];
}
