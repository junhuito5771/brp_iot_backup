import React from 'react';
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { Text, Colors } from '@module/daikin-ui';
import { TAB_ITEM_HEIGHT, HANDLE_HEIGHT } from '../../constants/general';

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: Colors.veryTransBlack,
    zIndex: 9999,
  },
  inner: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: TAB_ITEM_HEIGHT + HANDLE_HEIGHT,
  },
  upperContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  instructText: {
    width: 200,
    color: Colors.white,
    fontSize: 18,
  },
  button: {
    width: 150,
    height: 40,
    borderRadius: 5,
    backgroundColor: Colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 15,
  },
  buttonText: {
    color: Colors.blue,
    fontSize: 14,
    fontWeight: 'bold',
  },
  buttonTouchable: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const Instruction = ({ onCancel }) => (
  <View style={styles.container}>
    <View style={styles.inner}>
      <View style={styles.upperContainer}>
        <Image source={require('../../shared/assets/remote/dragIcon.png')} />
        <Text style={styles.instructText}>
          Drag up to reveal more remote control functions
        </Text>
      </View>
      <View style={styles.button}>
        <TouchableOpacity style={styles.buttonTouchable} onPress={onCancel}>
          <Text style={styles.buttonText}>OK</Text>
        </TouchableOpacity>
      </View>
      <Image
        source={require('../../shared/assets/remote/dragRemoteInsLine.png')}
      />
    </View>
  </View>
);

Instruction.propTypes = {
  onCancel: PropTypes.func,
};

export default Instruction;
