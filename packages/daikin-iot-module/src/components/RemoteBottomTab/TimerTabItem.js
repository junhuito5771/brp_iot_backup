import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';

import { HorizontalTabBar as TabBar, Text } from '@module/daikin-ui';
import { NavigationService } from '@module/utility';

const styles = StyleSheet.create({
  timerContainer: {
    flexWrap: 'wrap',
    flex: 1,
  },
  timerTopSection: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  timerBottomSection: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  timerLogoIcon: {
    width: 15,
    height: 15,
    marginRight: 1.5,
  },
  timerModeIcon: {
    width: 18,
    height: 18,
  },
  timerTimeText: {
    fontSize: 10,
  },
  timerOffText: {
    fontSize: 12,
  },
  timerTempText: {
    fontSize: 12,
  },
});

const getDay = timer => {
  const daysInStr = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

  return daysInStr[timer.day];
};

const getTimerTimeDetail = timer => {
  const time = `${timer.hour}:${timer.minute}`;

  return `${time} ${getDay(timer)}`;
};

const getTimerModeIcon = mode => {
  switch (mode) {
    case 'auto':
      return require('../../shared/assets/remote/autoMood.png');
    case 'cool':
      return require('../../shared/assets/remote/coolMood.png');
    case 'fan':
      return require('../../shared/assets/remote/fanMood.png');
    case 'heat':
      return require('../../shared/assets/remote/heatMood.png');
    case 'dry':
      return require('../../shared/assets/remote/dryMood.png');
    default:
      return require('../../shared/assets/remote/autoMood.png');
  }
};

const TimerTabItem = ({ upcomingTimer, thingName, ...extraProps }) => (
  <TabBar.Item
    {...extraProps}
    onPress={() => {
      const screenKey = getDay(upcomingTimer);
      const scrollToKey = `${thingName}_${upcomingTimer.timerIndex}`;
      NavigationService.drawerReset(screenKey, { scrollToKey });
    }}>
    <View style={styles.timerContainer}>
      <View style={styles.timerTopSection}>
        <Image
          style={styles.timerLogoIcon}
          source={require('../../shared/assets/remote/timerSmall.png')}
        />
        <Text style={styles.timerTimeText}>
          {getTimerTimeDetail(upcomingTimer)}
        </Text>
      </View>
      {upcomingTimer.switchValue === 0 ? (
        <View style={styles.timerBottomSection}>
          <Text light style={styles.timerOffText}>
            OFF
          </Text>
        </View>
      ) : (
        <View style={styles.timerBottomSection}>
          <Image
            style={styles.timerModeIcon}
            source={getTimerModeIcon(upcomingTimer.mode)}
          />
          {!!upcomingTimer.temp && (
            <Text style={styles.timerTempText}>
              {' '}
              {`${upcomingTimer.temp}\u2103`}
            </Text>
          )}
        </View>
      )}
    </View>
  </TabBar.Item>
);

TimerTabItem.propTypes = {
  upcomingTimer: PropTypes.object,
  thingName: PropTypes.string,
};

export default TimerTabItem;
