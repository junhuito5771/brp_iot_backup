import React from 'react';
import PropTypes from 'prop-types';
import { connect } from '@daikin-dama/redux-iot';

import QuickTimerInfo from '../UnitControl/quickTimerInfo';

function QuickTimerInfoContainer(props) {
  const { day } = props;

  const currentDate = new Date();
  const currentDay = currentDate.getDay();

  return currentDay === day && <QuickTimerInfo {...props} />;
}

const mapStateToProps = ({ timer }, { thingName }) => {
  const { quickTimerList } = timer || {};
  const curQuickTimer = quickTimerList[thingName] || {};

  return {
    hour: curQuickTimer.hour,
    minute: curQuickTimer.minute,
    day: curQuickTimer.day,
    switchValue: curQuickTimer.switchValue,
  };
};

QuickTimerInfoContainer.propTypes = {
  day: PropTypes.number,
};

export default connect(mapStateToProps)(QuickTimerInfoContainer);
