import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { Text } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    marginBottom: 10,
    fontWeight: 'bold',
    fontSize: 16,
  },
  desc: {
    fontSize: 16,
  },
  spaced: {
    marginBottom: 10,
  },
});

const TooltipContent = ({ title, desc, hasSpaced }) => (
  <View style={[styles.container, hasSpaced && styles.spaced]}>
    <Text style={styles.title}>{title}</Text>
    <Text style={styles.desc}>{desc}</Text>
  </View>
);

TooltipContent.propTypes = {
  title: PropTypes.string,
  desc: PropTypes.string,
  hasSpaced: PropTypes.bool,
};

const MultipleTooltipContent = ({ tooltips }) => (
  <>
    {tooltips.map((tooltipProps, index) => (
      <TooltipContent
        key={tooltipProps.title}
        {...tooltipProps}
        hasSpaced={index !== tooltips.length}
      />
    ))}
  </>
);

MultipleTooltipContent.propTypes = {
  tooltips: PropTypes.array,
};

MultipleTooltipContent.defaultProps = {
  tooltips: [],
};

export default TooltipContent;
export { MultipleTooltipContent };
