import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';

import { HorizontalTabBar as TabBar } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
    marginVertical: 5,
  },
});

const QUICKTIMER_ON = require('../../shared/assets/remote/quickTimerOn.png');
const QUICKTIMER_OFF = require('../../shared/assets/remote/quickTimerOff.png');

const QuickTimerTabItem = ({ thingName, isActivate, ...extraProps }) => {
  const quickTimerIconState = isActivate ? QUICKTIMER_ON : QUICKTIMER_OFF;

  return (
    <TabBar.Item {...extraProps}>
      <View style={styles.container}>
        <Image source={quickTimerIconState} />
      </View>
    </TabBar.Item>
  );
};

QuickTimerTabItem.propTypes = {
  thingName: PropTypes.string,
  isActivate: PropTypes.bool,
};

export default QuickTimerTabItem;
