import React, { useRef } from 'react';
import { Dimensions, Platform, Alert } from 'react-native';
import PropTypes from 'prop-types';
import {
  connect,
  remote,
  editTimer,
  RemoteHelper,
  ShadowValueHelper,
  HandsetRuleHelper,
} from '@daikin-dama/redux-iot';
import { heightPercentage } from '@module/utility';
import {
  SAFE_VIEWPORT_HEIGHT,
  iphoneXS,
  ConnectionType,
} from '@module/utility';
import Toast from 'react-native-root-toast';

import {
  Colors,
  HorizontalTabBar as TabBar,
  DragablePagerModal,
} from '@module/daikin-ui';
import getFeatureTooltip from '../../constants/featureTooltip';
import { HEADER_HEIGHT, TAB_ITEM_HEIGHT } from '../../constants/general';
import {
  fanSpdIconMapper,
  turboIconMapper,
  ecoplusIconMapper,
  sleepIconMapper,
  swing3DIconMapper,
  breezeIconMapper,
  smartDriftIconMapper,
  streamerIconMapper,
  senseIconMapper,
  ledIconMapper,
  ckSwingIconMapper,
} from '../../helpers/shadValueIconHelper';

import RemoteControlModal from '../RemoteModal/RemoteControlModal';
import RemoteSwingModal from '../RemoteModal/RemoteDoubleModal';
import RemoteQuickTimerModal from '../RemoteModal/RemoteQuickTimerModal';
import TimerTabItem from './TimerTabItem';
import SwingTabItem from './SwingTabItem';
import QuickTimerTabItem from './QuickTimerTabItem';
import TooltipContent, { MultipleTooltipContent } from './TooltipContent';
import {
  getFanSpdOpt,
  getTurboOpt,
  getEcoplusOpt,
  getSleepOpt,
  getSwingOpt,
  getLRSwingOpt,
  getSwing3DOpt,
  getBreezeOpt,
  getSmartDriftOpt,
  getStreamerOpt,
  getSenseOpt,
  getLEDOpt,
  getCKSwingOpt,
} from './utils';

const {
  FEAT_ECOPLUS,
  FEAT_FAN_SPEED,
  FEAT_SLEEP,
  FEAT_SMART_TURBO,
  FEAT_TURBO,
  FEAT_SMART_DRIFT,
  FEAT_BREEZE,
  FEAT_SENSE,
  FEAT_SMART_SLEEP,
  FEAT_SMART_ECOMAX,
} = RemoteHelper;

const { isFeatureAllowed } = HandsetRuleHelper;

const windowHeight = Dimensions.get('window').height;
// const snapRatio = Platform.select({
//   ios: iphoneXS ? 0.75 : 0.7,
//   android:
//     windowHeight > 750
//       ? 0.7 : 0.65,
// });
// Please note that 55 current is the header height
// 50 is the tab item height
const DEFAULT_SNAP_POINTS = [
  SAFE_VIEWPORT_HEIGHT,
  (SAFE_VIEWPORT_HEIGHT -
    TAB_ITEM_HEIGHT -
    HEADER_HEIGHT) -
    heightPercentage(22),
];

function dragableModalRoutes(routeName) {
  switch (routeName) {
    case 'twoLevelModal':
      return RemoteSwingModal;
    case 'quickTimerModal':
      return RemoteQuickTimerModal;
    default:
      return RemoteControlModal;
  }
}

const showToastMsg = msg => {
  Toast.show(msg, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: false,
    animation: true,
    hideOnPress: true,
    delay: 0,
    textColor: Colors.darkGrey,
    backgroundColor: Colors.lightCoolGrey,
    opacity: 0.9,
  });
};

function handleSwingMenuOnPress(config, modalRef) {
  const { enableUDStep, enableLRStep, enableLRSwing } = config;
  // const swingSnapRatio = Platform.select({
  //   ios: 0.5,
  //   android: windowHeight > 750 ? 0.55 : 0.5,
  // });

  const swingSnapPoints = [
    SAFE_VIEWPORT_HEIGHT,
    (SAFE_VIEWPORT_HEIGHT - TAB_ITEM_HEIGHT - HEADER_HEIGHT) - heightPercentage(37),
  ];
  const isAdvSwing = enableUDStep && enableLRStep && enableLRSwing;
  const snapPoints = isAdvSwing ? swingSnapPoints : DEFAULT_SNAP_POINTS;
  const options = getSwingOpt(config);
  const { thingName } = config;

  const routeName = isAdvSwing ? 'twoLevelModal' : 'Swing';

  modalRef.current.navigate(routeName, {
    title: 'Swing',
    options,
    snapPoints,
    thingName,
    sections: [
      {
        key: 'UpDownSwing',
        options,
        selectedValueHandler: unit => {
          const { enableUDStep: curEnableUDStep } = unit;
          const { swing: curSwing, upDownSwing: curUpDownSwing } =
            (unit || {}).modeConfig || {};

          const result = ShadowValueHelper.getSwingByShadowVal({
            swing: curSwing,
            upDownSwing: curUpDownSwing,
            enableUDStep: curEnableUDStep,
          });

          return result;
        },
        thingName,
      },
      {
        key: 'LeftRingSwing',
        options: getLRSwingOpt(config),
        selectedValueHandler: unit => {
          const { leftRightSwing: curLRSwing } = (unit || {}).modeConfig || {};
          const result = ShadowValueHelper.getSwingLRByShadowVal({
            leftRightSwing: curLRSwing,
          });

          return result;
        },
        thingName,
      },
    ],
    selectedValueHandler: unit => {
      const { enableUDStep: curEnableUDStep } = unit;
      const { swing: curSwing, upDownSwing: curUpDownSwing } =
        (unit || {}).modeConfig || {};

      const result = ShadowValueHelper.getSwingByShadowVal({
        swing: curSwing,
        upDownSwing: curUpDownSwing,
        enableUDStep: curEnableUDStep,
      });

      return result;
    },
  });
}

function handleEcoPlusOnPress(config, modalRef) {
  const {
    thingName,
    enableSmartTurbo,
    handsetType,
    switch: switchValue,
  } = config;

  const featureName = enableSmartTurbo ? 'smart sleep' : 'sleep';
  if (handsetType === RemoteHelper.HANDSET_DIL && switchValue === 0) {
    showToastMsg(`Please turn on the unit to control ${featureName}`);
    return;
  }

  const options = getEcoplusOpt(config);
  const damaTooltipKey = enableSmartTurbo ? 'smartEcomax' : 'ecoplus';
  const tooltipKey =
    handsetType === RemoteHelper.HANDSET_DAMA ? damaTooltipKey : 'ecoNo';
  const tooltipParams = getFeatureTooltip(tooltipKey);
  modalRef.current.navigate('ecoplus', {
    title: handsetType === RemoteHelper.HANDSET_DAMA ? 'Eco Mode' : 'ECONO',
    options,
    snapPoints: DEFAULT_SNAP_POINTS,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    selectedValueHandler: unit => {
      const { ecoplus, smartEcoMax } = (unit || {}).modeConfig || {};

      return ShadowValueHelper.getEcoplusByShadowVal({ ecoplus, smartEcoMax });
    },
    thingName,
  });
}

function handleFanSpeedOnPress(config, modalRef) {
  const { thingName, handsetType } = config;
  const options = getFanSpdOpt(config);
  modalRef.current.navigate('fanSpeed', {
    title: 'Fan speed',
    options,
    snapPoints: DEFAULT_SNAP_POINTS,
    selectedHandler: (optValue, selectedValue) => {
      if (
        optValue.match(/\d+/) !== null &&
        selectedValue.match(/\d+/) !== null
      ) {
        return optValue.match(/\d+/) <= selectedValue.match(/\d+/);
      }
      return optValue === selectedValue;
    },
    selectedValueHandler: unit => {
      const { modeConfig, mode } = unit || {};
      const { fan, fanExtend } = modeConfig || {};
      const result = ShadowValueHelper.getFanLvlByShadowVal(
        handsetType,
        fan,
        fanExtend,
        modeConfig,
        mode
      );

      return result;
    },
    thingName,
  });
}

function handleTurboOnPress(config, modalRef) {
  const {
    thingName,
    enableSmartTurbo,
    handsetType,
    switch: switchValue,
  } = config;
  const featureName = enableSmartTurbo ? 'smart powerful' : 'powerful';
  if (handsetType === RemoteHelper.HANDSET_DIL && switchValue === 0) {
    showToastMsg(`Please turn on the unit to control ${featureName}`);
    return;
  }

  const options = getTurboOpt(config);
  const tooltipKey = enableSmartTurbo ? 'smartPowerful' : 'powerful';
  const tooltipParams = getFeatureTooltip(tooltipKey);
  modalRef.current.navigate('turbo', {
    title: 'Powerful',
    options,
    snapPoints: DEFAULT_SNAP_POINTS,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    selectedValueHandler: unit => {
      const { turbo: curTurbo, smartTurbo: curSmartTurbo } =
        (unit || {}).modeConfig || {};

      const result = ShadowValueHelper.getTurboByShadowVal(
        curTurbo,
        curSmartTurbo
      );

      return result;
    },
    thingName,
  });
}

function handleSleepOnPress(config, modalRef) {
  const {
    thingName,
    enableSmartSleep,
    handsetType,
    switch: switchValue,
  } = config;

  const featureName = enableSmartSleep ? 'smart sleep' : 'sleep';
  if (handsetType === RemoteHelper.HANDSET_DIL && switchValue === 0) {
    showToastMsg(`Please turn on the unit to control ${featureName}`);
    return;
  }

  const options = getSleepOpt(config);
  const tooltipKey = enableSmartSleep ? 'smartSleep' : 'sleep';
  const tooltipParams = getFeatureTooltip(tooltipKey);
  modalRef.current.navigate('sleep', {
    title: 'Sleep mode',
    options,
    snapPoints: DEFAULT_SNAP_POINTS,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    selectedValueHandler: unit => {
      const { sleep, smartSleep } = (unit || {}).modeConfig || {};
      const result = ShadowValueHelper.getSleepByShadowVal({
        sleep,
        smartSleep,
      });

      return result;
    },
    thingName,
  });
}

function handleSwing3DOnPress(config, modalRef) {
  const { thingName } = config;
  const options = getSwing3DOpt(config);
  const tooltipParams = getFeatureTooltip('airFlow3D');
  modalRef.current.navigate('3dAirFlow', {
    title: '3D Airflow',
    options,
    snapPoints: DEFAULT_SNAP_POINTS,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    selectedValueHandler: unit => {
      const { swing: curSwing, leftRightSwing: lrSwing, upDownSwing: udSwing } =
        (unit || {}).modeConfig || {};

      return Number(curSwing === 1 && lrSwing === 15 && udSwing === 15);
    },
    thingName,
  });
}

function handleBreezeOnPress(config, modalRef) {
  const { thingName } = config;
  const options = getBreezeOpt(config);
  const tooltipParams = getFeatureTooltip('breeze');
  modalRef.current.navigate('breeze', {
    title: 'Breeze',
    options,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    snapPoints: DEFAULT_SNAP_POINTS,
    selectedValueHandler: unit => {
      const { breeze } = (unit || {}).modeConfig || {};

      return breeze;
    },
    thingName,
  });
}

function handleSmartDriftOnPress(config, modalRef) {
  const { thingName } = config;
  const options = getSmartDriftOpt(config);
  const tooltipParams = getFeatureTooltip('smartDrift');
  modalRef.current.navigate('smartDrift', {
    title: 'Smart drift',
    options,
    snapPoints: DEFAULT_SNAP_POINTS,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    selectedValueHandler: unit => {
      const { smartDrift } = (unit || {}).modeConfig || {};

      return smartDrift;
    },
    thingName,
  });
}

function handleStreamerOnPress(config, modalRef) {
  const { thingName } = config;
  const options = getStreamerOpt(config);
  const tooltipParams = getFeatureTooltip('streamer');
  modalRef.current.navigate('streamer', {
    title: 'Streamer',
    options,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    snapPoints: DEFAULT_SNAP_POINTS,
    selectedValueHandler: unit => {
      const { streamer } = (unit || {}).modeConfig || {};

      return streamer;
    },
    thingName,
  });
}

function handleSenseOnPress(config, modalRef) {
  const { thingName } = config;
  const options = getSenseOpt(config);
  const tooltipParams = getFeatureTooltip('sense');
  modalRef.current.navigate('sense', {
    title: 'Sense',
    options,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    snapPoints: DEFAULT_SNAP_POINTS,
    selectedValueHandler: unit => {
      const { sense } = (unit || {}).modeConfig || {};

      return sense;
    },
    thingName,
  });
}

function handleLEDOnPress(config, modalRef) {
  const { thingName, enableLED, enablePowerInd } = config;
  const options = getLEDOpt(config);
  const tooltipKeys = [];
  if (enableLED) {
    tooltipKeys.push('led');
  }
  if (enablePowerInd) {
    tooltipKeys.push('powerInd');
  }

  const tooltips = tooltipKeys.reduce((acc, curKey) => {
    acc.push(getFeatureTooltip(curKey));

    return acc;
  }, []);

  modalRef.current.navigate('led', {
    title: 'LED',
    options,
    snapPoints: DEFAULT_SNAP_POINTS,
    toolTipMsgComp: <MultipleTooltipContent tooltips={tooltips} />,
    selectedValueHandler: unit => {
      const { led, powerInd } = (unit || {}).modeConfig || {};

      return ShadowValueHelper.getLEDByShadowVal({ led, powerInd });
    },
    thingName,
  });
}

function handleCKSwingOnPress(config, modalRef) {
  const { thingName } = config;
  const options = getCKSwingOpt(config);
  const tooltipParams = getFeatureTooltip('ckSwing');
  modalRef.current.navigate('ckSwing', {
    title: 'CK Swing',
    options,
    toolTipMsgComp: <TooltipContent {...tooltipParams} />,
    snapPoints: DEFAULT_SNAP_POINTS,
    selectedValueHandler: unit => {
      const { ckSwing } = (unit || {}).modeConfig || {};

      return ShadowValueHelper.getCKSwingByShadowVal({ ckSwing });
    },
    thingName,
  });
}

function handleQuickTimerOnPress(isActivate, config, modalRef) {
  if (isActivate) {
    // Prompt user an alert
    Alert.alert('', 'Are you sure you want to turn off your timer?', [
      { text: 'No' },
      {
        text: 'Yes',
        onPress: () => {
          config.submitQuickTimer(false, config);
        },
      },
    ]);
  } else {
    const quickTimersnapRatio = Platform.select({
      ios: 0.65,
      android: windowHeight > 750 ? 0.65 : 0.5,
    });
    const snapPoints = [
      SAFE_VIEWPORT_HEIGHT,
      (SAFE_VIEWPORT_HEIGHT - TAB_ITEM_HEIGHT - HEADER_HEIGHT) *
        quickTimersnapRatio,
    ];
    // Navigate to the quick timer modal
    modalRef.current.navigate('quickTimerModal', {
      title: 'Quick Timer',
      snapPoints,
      onSubmit: timerInfo => {
        config.submitQuickTimer(true, timerInfo);
      },
    });
  }
}

function canRenderSpecialFeat(children) {
  const count = React.Children.toArray(children).filter(child => !!child)
    .length;

  return count > 0;
}

const RemoteDraggableTab = props => {
  const modalRef = useRef();

  const {
    upcomingTimer,
    thingName,
    enableTurbo,
    enableSmartTurbo,
    enableSleep,
    enableSmartSleep,
    enableLRSwing,
    enableLRStep,
    enableUDStep,
    enableBreeze,
    enableSmartDrift,
    enableStreamer,
    enableSense,
    enableLED,
    enablePowerInd,
    enableCKSwing,
    enableSmartEcomax,
    mode,
    handsetType,
    onDrag,
    hasQuickTimer,
    connectionType,
  } = props;

  const isEcoAllowed =
    enableSmartEcomax && handsetType !== RemoteHelper.HANDSET_DIL
      ? isFeatureAllowed(mode, FEAT_SMART_ECOMAX, handsetType)
      : isFeatureAllowed(mode, FEAT_ECOPLUS, handsetType);

  const isTurboAllowed =
    (enableTurbo &&
      !enableSmartTurbo &&
      isFeatureAllowed(mode, FEAT_TURBO, handsetType)) ||
    (enableSmartTurbo && isFeatureAllowed(mode, FEAT_SMART_TURBO, handsetType));
  const isFanSpdAllowed = isFeatureAllowed(mode, FEAT_FAN_SPEED, handsetType);
  const isSleepAllowed =
    (enableSleep &&
      !enableSmartSleep &&
      isFeatureAllowed(mode, FEAT_SLEEP, handsetType)) ||
    (enableSmartSleep && isFeatureAllowed(mode, FEAT_SMART_SLEEP, handsetType));
  const is3DAllowed = enableLRSwing && enableLRStep && enableUDStep;
  const isBreezeAllowed =
    enableBreeze && isFeatureAllowed(mode, FEAT_BREEZE, handsetType);
  const isSmartDriftAllowed =
    enableSmartDrift && isFeatureAllowed(mode, FEAT_SMART_DRIFT, handsetType);
  const isStreamerAllowed = enableStreamer;
  const isSenseAllowed =
    enableSense && isFeatureAllowed(mode, FEAT_SENSE, handsetType);
  const isLEDAllowed = enableLED || enablePowerInd;
  const isCKSwingALlowed = enableCKSwing;

  const canDrag =
    isStreamerAllowed ||
    isSenseAllowed ||
    isCKSwingALlowed ||
    [
      !!upcomingTimer,
      isEcoAllowed,
      isTurboAllowed,
      true, // Swing is always allowed
      isFanSpdAllowed,
      isLEDAllowed,
      isSleepAllowed,
      is3DAllowed,
      isBreezeAllowed,
      isSmartDriftAllowed,
    ].filter(flag => flag).length > 5;

  onDrag(canDrag);

  return (
    <DragablePagerModal
      ref={modalRef}
      router={dragableModalRoutes}
      canDrag={canDrag}>
      <TabBar context={{ modalRef }}>
        <TabBar.Group>
          {!!upcomingTimer && (
            <TimerTabItem thingName={thingName} upcomingTimer={upcomingTimer} />
          )}
          {isEcoAllowed && (
            <TabBar.Item
              name="Ecoplus"
              icon={ecoplusIconMapper(props)}
              onPress={() => handleEcoPlusOnPress(props, modalRef)}
            />
          )}
          {isTurboAllowed && (
            <TabBar.Item
              name="Turbo"
              icon={turboIconMapper(props)}
              onPress={() => handleTurboOnPress(props, modalRef)}
            />
          )}
          <SwingTabItem
            thingName={thingName}
            onPress={() => handleSwingMenuOnPress(props, modalRef)}
          />
          {isFanSpdAllowed && (
            <TabBar.Item
              name="FanSpeed"
              icon={fanSpdIconMapper(props)}
              onPress={() => handleFanSpeedOnPress(props, modalRef)}
            />
          )}
          {ConnectionType.IOT_MODE === connectionType && (
            <QuickTimerTabItem
              name="QuickTimer"
              isActivate={hasQuickTimer}
              onPress={() =>
                handleQuickTimerOnPress(hasQuickTimer, props, modalRef)
              }
            />
          )}
          {isLEDAllowed && (
            <TabBar.Item
              name="LED"
              icon={ledIconMapper(props)}
              onPress={() => handleLEDOnPress(props, modalRef)}
            />
          )}
          {isSleepAllowed && (
            <TabBar.Item
              name="Sleep"
              icon={sleepIconMapper(props)}
              onPress={() => handleSleepOnPress(props, modalRef)}
            />
          )}
          {is3DAllowed && (
            <TabBar.Item
              name="3D"
              icon={swing3DIconMapper(props)}
              onPress={() => handleSwing3DOnPress(props, modalRef)}
            />
          )}
          {isBreezeAllowed && (
            <TabBar.Item
              name="Breeze"
              icon={breezeIconMapper(props)}
              onPress={() => handleBreezeOnPress(props, modalRef)}
            />
          )}
          {isSmartDriftAllowed && (
            <TabBar.Item
              name="SmartDrift"
              icon={smartDriftIconMapper(props)}
              onPress={() => handleSmartDriftOnPress(props, modalRef)}
            />
          )}
        </TabBar.Group>
        <TabBar.Group
          title="Special Features"
          isFixedWidth
          canRenderHandler={canRenderSpecialFeat}>
          {isStreamerAllowed && (
            <TabBar.Item
              name="streamer"
              icon={streamerIconMapper(props)}
              onPress={() => handleStreamerOnPress(props, modalRef)}
            />
          )}
          {isSenseAllowed && (
            <TabBar.Item
              name="Sense"
              icon={senseIconMapper(props)}
              onPress={() => handleSenseOnPress(props, modalRef)}
            />
          )}
          {isCKSwingALlowed && (
            <TabBar.Item
              name="CK Swing"
              icon={ckSwingIconMapper(props)}
              onPress={() => handleCKSwingOnPress(props, modalRef)}
            />
          )}
        </TabBar.Group>
      </TabBar>
    </DragablePagerModal>
  );
};

const mapDisptatchToProps = dispatch => ({
  setEcoplus: value => dispatch(remote.setEcoplus(value, true)),
  setTurbo: value => dispatch(remote.setTurbo(value)),
  setSwing: value => dispatch(remote.setSwing(value)),
  setSwingLR: value => dispatch(remote.setSwingLR(value)),
  setSwing3D: value => dispatch(remote.setSwing3D(value)),
  setFanSpeed: value => dispatch(remote.setFanSpeed(value)),
  setSleep: value => dispatch(remote.setSleep(value)),
  setBreeze: value => dispatch(remote.setBreeze(value)),
  setSmartDrift: value => dispatch(remote.setSmartDrift(value)),
  setStreamer: value => dispatch(remote.setStreamer(value)),
  setSense: value => dispatch(remote.setSense(value)),
  setLED: value => dispatch(remote.setLED(value)),
  setCKSwing: value => dispatch(remote.setCKSwing(value)),
  submitQuickTimer: (isCreate, timerInfo) => {
    dispatch(editTimer.submitQuickTimer({ isCreate, timerInfo }));
  },
});

const mapStateToProps = (
  {
    units: { allUnits },
    timer: { quickTimerList },
    remote: { connectionType },
  },
  { thingName }
) => {
  const unit = allUnits[thingName];
  const quickTimer = quickTimerList[thingName];

  return {
    ...unit,
    hasQuickTimer: !!quickTimer,
    connectionType,
  };
};

RemoteDraggableTab.propTypes = {
  mode: PropTypes.string,
  onDrag: PropTypes.func,
  enableTurbo: PropTypes.bool,
  enableSmartTurbo: PropTypes.bool,
  enableSmartSleep: PropTypes.bool,
  enableSmartEcomax: PropTypes.bool,
  enableSleep: PropTypes.bool,
  enableLRSwing: PropTypes.bool,
  enableLRStep: PropTypes.bool,
  enableUDStep: PropTypes.bool,
  enableBreeze: PropTypes.bool,
  enableSmartDrift: PropTypes.bool,
  enableStreamer: PropTypes.bool,
  enableLED: PropTypes.bool,
  enablePowerInd: PropTypes.bool,
  enableSense: PropTypes.bool,
  enableCKSwing: PropTypes.bool,
  upcomingTimer: PropTypes.object,
  thingName: PropTypes.string,
  handsetType: PropTypes.number,
  hasQuickTimer: PropTypes.bool,
  connectionType: PropTypes.string,
};

export default connect(
  mapStateToProps,
  mapDisptatchToProps
)(RemoteDraggableTab);
