import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from '@daikin-dama/redux-iot';

import { HorizontalTabBar as TabBar } from '@module/daikin-ui';
import {
  swingIconMapper,
  swingLRIconMapper,
} from '../../helpers/shadValueIconHelper';

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
});

const SwingTabItem = ({
  enableLRSwing,
  enableUDStep,
  swing,
  upDownSwing,
  leftRightSwing,
  ...extraProps
}) => {
  const { nrItems, nrItemPerRow } = extraProps;
  const nrItemPerCurRow = Math.min(nrItemPerRow, nrItems);
  const isMaxNrPerRow = nrItemPerCurRow === 5;
  const iconStyle = { minWidth: 30, minHeight: 30 };

  return (
    <TabBar.Item {...extraProps}>
      <View style={styles.item}>
        <TabBar.Icon
          key={`${nrItems}_swingUD`}
          resizeMode="center"
          style={isMaxNrPerRow ? iconStyle : null}
          source={swingIconMapper({
            modeConfig: { swing, upDownSwing },
            enableUDStep,
          })}
        />
        {enableLRSwing && (
          <TabBar.Icon
            key={`${nrItems}_swingLR`}
            resizeMode="center"
            style={isMaxNrPerRow ? iconStyle : null}
            source={swingLRIconMapper({ modeConfig: { leftRightSwing } })}
          />
        )}
      </View>
    </TabBar.Item>
  );
};

SwingTabItem.propTypes = {
  width: PropTypes.number,
  enableLRSwing: PropTypes.bool,
  enableUDStep: PropTypes.bool,
  swing: PropTypes.number,
  upDownSwing: PropTypes.number,
  leftRightSwing: PropTypes.number,
};

const mapStateToProps = ({ units: { allUnits } }, { thingName }) => {
  const unit = allUnits[thingName] || {};
  const { enableLRSwing, enableUDStep } = unit;
  const { swing, upDownSwing, leftRightSwing } = unit.modeConfig || {};

  return {
    swing,
    upDownSwing,
    leftRightSwing,
    enableLRSwing,
    enableUDStep,
  };
};

export default connect(mapStateToProps)(SwingTabItem);
