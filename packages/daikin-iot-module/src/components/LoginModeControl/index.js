import React, { useEffect } from 'react';
import { View, StyleSheet, Platform, Alert } from 'react-native';
import PropTypes from 'prop-types';
import { connect, bleMode, wlanMode, BLEService } from '@daikin-dama/redux-iot';

import { Link as LinkText, Loading } from '@module/daikin-ui';

import { BLUETOOTH_SCREEN, WIFI_SCREEN } from '../../constants/routeNames';

const {
  withBleManager,
  checkBluetoothAndLocation,
  discoverAndProcessUnits,
} = BLEService;

const styles = StyleSheet.create({
  bottomSection: {
    flex: 0.1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 30,
  },
});

function LoginModeControl({
  isScanning,
  isWLANScanning,
  bleSuccess,
  bleError,
  setBleError,
  discoverWLANUnits,
  wlanSuccess,
  wlanError,
  dispatch,
  navigation,
  clearStatus,
}) {
  useEffect(() => {
    if (bleSuccess) {
      navigation.navigate(BLUETOOTH_SCREEN);
    }

    if (bleError) {
      Alert.alert('Bluetooth mode', bleError, [
        {
          text: 'OK',
          onPress: clearStatus,
        },
      ]);
    }
  }, [bleSuccess, bleError]);

  useEffect(() => {
    if (wlanSuccess) {
      navigation.navigate(WIFI_SCREEN);
    }

    if (wlanError) {
      Alert.alert('No units found', wlanError, [
        {
          text: 'OK',
          onPress: clearStatus,
        },
      ]);
    }
  }, [wlanSuccess, wlanError]);
  const checkAndDiscoverBLEUnits = async () => {
    const { errorMsg } = await checkBluetoothAndLocation();

    if (errorMsg) setBleError(errorMsg);
    else {
      await discoverAndProcessUnits(dispatch);
    }
  };
  return (
    <>
      <View style={styles.bottomSection}>
        <LinkText onPress={discoverWLANUnits}>WLAN Mode</LinkText>
        <LinkText onPress={checkAndDiscoverBLEUnits}>Bluetooth Mode</LinkText>
      </View>
      <Loading visible={isScanning || isWLANScanning}>
        Scanning for available units
      </Loading>
    </>
  );
}

const mapStateToProps = ({
  bleMode: { success: bleSuccess, error: bleError, isLoading: isScanning },
  wlanMode: {
    success: wlanSuccess,
    error: wlanError,
    isLoading: isWLANScanning,
  },
}) => ({
  bleSuccess,
  bleError,
  isScanning,
  wlanSuccess,
  wlanError,
  isWLANScanning,
});

const mapDispatchToProps = dispatch => ({
  discoverWLANUnits: () => {
    dispatch(
      wlanMode.discoverUnits({
        setBroadcast: Platform.select({
          ios: true,
          android: false,
        }),
      })
    );
  },
  setBleError: message => {
    dispatch(
      bleMode.setBleStatus({
        messageType: 'error',
        message,
      })
    );
  },
  clearStatus: () => {
    dispatch(bleMode.clearBleStatus());
    dispatch(wlanMode.clearStatus());
  },
  dispatch,
});

LoginModeControl.propTypes = {
  isScanning: PropTypes.bool,
  isWLANScanning: PropTypes.bool,
  bleSuccess: PropTypes.string,
  bleError: PropTypes.string,
  setBleError: PropTypes.func,
  discoverWLANUnits: PropTypes.func,
  wlanSuccess: PropTypes.string,
  wlanError: PropTypes.string,
  dispatch: PropTypes.func,
  navigation: PropTypes.func,
  clearStatus: PropTypes.func,
};

const connectedLoginModeControl = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginModeControl);

export default withBleManager({
  maxScanTimeOut: 10,
  canDuplicate: false,
})(connectedLoginModeControl);
