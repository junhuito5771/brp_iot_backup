import React, { useState } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import { widthPercentage } from '@module/utility';

import { Colors, aircondIcons as acIcons } from '@module/daikin-ui';

const styles = StyleSheet.create({
  iconListContainer: {
    flex: 1,
  },
  iconListWrapper: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  iconContainer: {
    height: widthPercentage(17),
    width: widthPercentage(17),
    justifyContent: 'center',
    alignItems: 'center',
  },
  selected: {
    backgroundColor: Colors.lightBlue,
  },
});

const UnitIcons = ({ onAfterSelect }) => {
  const [selectedIconIndex, setSelectedIconIndex] = useState(null);

  const onSelect = key => {
    setSelectedIconIndex(key);
    onAfterSelect(key);
  };

  return (
    <ScrollView style={styles.iconListContainer}>
      <View style={styles.iconListWrapper}>
        {Object.entries(acIcons).map(([key, path]) => {
          const isSelected = key === selectedIconIndex;
          return (
            <TouchableOpacity key={key} onPress={() => onSelect(key)}>
              <View
                style={[styles.iconContainer, isSelected && styles.selected]}>
                <Image source={path} />
              </View>
            </TouchableOpacity>
          );
        })}
      </View>
    </ScrollView>
  );
};

UnitIcons.propTypes = {
  onAfterSelect: PropTypes.func,
};

export default UnitIcons;
