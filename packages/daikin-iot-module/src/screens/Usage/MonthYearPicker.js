import React, {
  useEffect,
  useState,
  useImperativeHandle,
  forwardRef,
} from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import { ScaleText } from '@module/utility';

import { WheelPicker, Colors } from '@module/daikin-ui';

const MONTH_YEAR_FORMAT = 'MMMM YYYY';

const MONTHS = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const styles = StyleSheet.create({
  container: {
    padding: 5,
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrap: {
    flexDirection: 'row',
  },
  marginLeft: {
    marginLeft: 5,
  },
  wheelItem: {
    color: Colors.black,
    fontSize: ScaleText(12),
  },
});

function getYearList(minimumDate, maximumDate, formatDate) {
  if (!minimumDate || !maximumDate || !formatDate) return [];
  const minYear = moment(minimumDate, formatDate).year();
  const maxYear = moment(maximumDate, formatDate).year();

  const years = [];

  for (let i = minYear; i <= maxYear; i += 1) {
    years.push(i.toString());
  }

  return years;
}

function getMonthID(momentDate) {
  return MONTHS.findIndex(curMonth => curMonth === momentDate.format('MMMM'));
}

function getYearID(momentDate, yearList) {
  return yearList.findIndex(curYear => curYear === momentDate.format('YYYY'));
}

function getMomentMonthYear(dateObj, yearList) {
  const { monthID, yearID } = dateObj;
  const month = MONTHS[monthID];
  const year = yearList[yearID];

  const monthYear = `${month} ${year}`;
  const momentMonthYear = moment(monthYear, MONTH_YEAR_FORMAT);

  return momentMonthYear;
}

const MonthYearPicker = forwardRef(
  (
    { minimumDate, maximumDate, selectedDate, formatDate, showYearOnly },
    ref
  ) => {
    const [selectedItemID, setSelectedItemID] = useState({
      monthID: 0,
      yearID: 0,
    });

    const yearList = getYearList(minimumDate, maximumDate, formatDate);

    useEffect(() => {
      if (selectedDate) {
        const momentDate = moment(selectedDate, formatDate);
        const selMonthID = getMonthID(momentDate);
        const selYearID = getYearID(momentDate, yearList);

        setSelectedItemID({ monthID: selMonthID, yearID: selYearID });
      }
    }, [selectedDate]);

    useEffect(() => {
      const momentMonthYear = getMomentMonthYear(selectedItemID, yearList);
      const momentMinDate = moment(minimumDate, formatDate);
      const momentMaxDate = moment(maximumDate, formatDate).subtract(
        1,
        'months'
      );

      if (momentMonthYear.isBefore(momentMinDate)) {
        // Need to find monthID and yearID and update
        const minMonthID = getMonthID(momentMinDate);
        const minYearID = getYearID(momentMinDate, yearList);

        setSelectedItemID({ monthID: minMonthID, yearID: minYearID });
      } else if (momentMonthYear.isAfter(momentMaxDate)) {
        // Need to find monthID and yearID and update
        const maxMonthID = getMonthID(momentMaxDate);
        const maxYearID = getYearID(momentMaxDate, yearList);

        setSelectedItemID({ monthID: maxMonthID, yearID: maxYearID });
      }
    }, [selectedItemID.monthID, selectedItemID.yearID]);

    useImperativeHandle(
      ref,
      () => ({
        getValue: () => {
          const momentMonthYear = getMomentMonthYear(selectedItemID, yearList);

          return momentMonthYear.format(formatDate);
        },
      }),
      [selectedItemID]
    );

    const handleOnMonthChanged = val => {
      setSelectedItemID({ ...selectedItemID, monthID: val });
    };

    const handleOnYearChanged = val => {
      setSelectedItemID({ ...selectedItemID, yearID: val });
    };

    const wheelWidth = showYearOnly ? '100%' : '50%';

    if (showYearOnly) yearList.splice(-1, 1);

    return (
      <View style={styles.container}>
        <View style={styles.wrap}>
          {!showYearOnly && (
            <WheelPicker
              style={{ width: wheelWidth }}
              selectedItemPosition={selectedItemID.monthID}
              onValueChange={(v, index) => handleOnMonthChanged(index)}
              data={MONTHS}
            />
          )}
          <WheelPicker
            style={{ width: wheelWidth }}
            selectedItemPosition={selectedItemID.yearID}
            onValueChange={(v, index) => handleOnYearChanged(index)}
            data={yearList}
          />
        </View>
      </View>
    );
  }
);

MonthYearPicker.propTypes = {
  minimumDate: PropTypes.string,
  maximumDate: PropTypes.string,
  selectedDate: PropTypes.string,
  formatDate: PropTypes.string,
  showYearOnly: PropTypes.bool,
};

MonthYearPicker.defaultProp = {
  showYearOnly: false,
};

export default MonthYearPicker;
