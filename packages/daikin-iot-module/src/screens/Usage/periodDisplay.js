import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from '@daikin-dama/redux-iot';
import { heightPercentage, ScaleText, timeConvert } from '@module/utility';
import { Text, Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  justifySpace: {
    justifyContent: 'space-evenly',
  },
  column: {
    flexDirection: 'column',
  },
  displayText: {
    color: Colors.black,
    fontSize: 22,
    textAlign: 'center',
  },
  titleText: {
    fontSize: ScaleText(14),
    color: Colors.black,
    marginTop: heightPercentage(1),
  },
  content: {
    marginTop: heightPercentage(1),
    alignItems: 'flex-start',
  },
  unitText: {
    color: Colors.black,
    marginLeft: 5,
    marginBottom: 5,
    textAlign: 'center',
    fontSize: 15,
    alignSelf: 'flex-end',
  },
  labelText: {
    fontSize: 10,
    color: Colors.black,
  },
  loadingCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  marginRight1: {
    marginRight: 3,
  },
  selfCenter: {
    alignSelf: 'center',
  },
  selfStart: {
    alignSelf: 'flex-start',
  },
});

const titleEnum = {
  daily: 'Today',
  weekly: 'This Week',
  monthly: 'This Month',
  yearly: 'This Year',
};

const PeriodDisplay = ({
  displayValue,
  displayAverageKwh,
  isInverter,
  title,
  isLoading,
  displayTnbRate,
  enableEnergyConsumption,
  displayCpRT,
}) => {
  const checkEnergyConsumption = () => {
    if (isInverter === 0 && enableEnergyConsumption) {
      return (
        <>
          <View style={[styles.column, styles.content]}>
            <View style={[styles.row, styles.selfStart]}>
              {isLoading ? (
                <View style={styles.loadingCenter}>
                  <ActivityIndicator size="small" color={Colors.azureRad} />
                </View>
              ) : (
                <Text style={styles.displayText}>
                  {displayAverageKwh === undefined
                    ? '-'
                    : `${displayAverageKwh}`}
                </Text>
              )}
              <Text style={styles.unitText}>kWh</Text>
            </View>
            <View style={[styles.row, styles.selfCenter]}>
              <Text style={styles.labelText}>(total)</Text>
            </View>
          </View>
          <View style={[styles.column, styles.content]}>
            <View style={styles.row}>
              <Text style={[styles.unitText, styles.marginRight1]}>RM</Text>
              {isLoading ? (
                <View style={styles.loadingCenter}>
                  <ActivityIndicator size="small" color={Colors.azureRad} />
                </View>
              ) : (
                <Text style={styles.displayText}>
                  {displayTnbRate === undefined ? '-' : `${displayTnbRate}`}
                </Text>
              )}
            </View>
            <View style={[styles.row, styles.selfCenter]}>
              <Text style={styles.labelText}>(total)</Text>
            </View>
          </View>
        </>
      );
    }
    return null;
  };

  const checkCompressorRunningTime = () => {
    if (isInverter === 1) {
      const { rhours, rminutes } = timeConvert(displayCpRT);
      return (
        <View style={[styles.column, styles.content]}>
          <View style={[styles.row, styles.selfStart]}>
            {isLoading ? (
              <View style={styles.loadingCenter}>
                <ActivityIndicator size="small" color={Colors.azureRad} />
              </View>
            ) : (
              <>
                {rhours > 0 && (
                  <>
                    <Text style={styles.displayText}>
                      {displayCpRT === undefined ? '-' : `${rhours}`}
                    </Text>
                    <Text style={styles.unitText}>hours</Text>
                  </>
                )}
                <Text style={styles.displayText}>
                  {displayCpRT === undefined ? '-' : ` ${rminutes}`}
                </Text>
                <Text style={styles.unitText}>mins</Text>
              </>
            )}
          </View>
          <View style={[styles.row, styles.selfCenter]}>
            <Text style={styles.labelText}>(total)</Text>
          </View>
        </View>
      );
    }
    return null;
  };

  return (
    <View>
      <Text style={styles.titleText}>{title}</Text>
      <View style={[styles.row, styles.justifySpace]}>
        <View style={[styles.column, styles.content]}>
          <View style={styles.row}>
            {isLoading ? (
              <View style={styles.loadingCenter}>
                <ActivityIndicator size="small" color={Colors.azureRad} />
              </View>
            ) : (
              <Text style={styles.displayText}>
                {parseInt(displayValue, 10) || '-'}
              </Text>
            )}
            <Text style={styles.unitText}>°C</Text>
          </View>
          <View style={[styles.row, styles.selfCenter]}>
            <Text style={styles.labelText}>(average)</Text>
          </View>
        </View>
        {checkEnergyConsumption()}
        {checkCompressorRunningTime()}
      </View>
    </View>
  );
};

PeriodDisplay.propTypes = {
  displayValue: PropTypes.string,
  displayAverageKwh: PropTypes.string,
  displayTnbRate: PropTypes.string,
  displayCpRT: PropTypes.string,
  title: PropTypes.string,
  isLoading: PropTypes.bool,
  enableEnergyConsumption: PropTypes.bool,
  isInverter: PropTypes.number,
};

const mapStateToProps = (
  {
    units: { allUnits },
    usage: {
      isLoading,
      usageByPeriod,
      usageSetting: { thingName },
    },
  },
  { periodType }
) => {
  const usageInThingName = usageByPeriod[thingName] || {};
  const units = allUnits[thingName] || {};
  const { isInverter } = units;
  const usageInPeriod = usageInThingName[periodType] || {};
  const { displayValue } = usageInPeriod;
  const { displayAverageKwh } = usageInPeriod;
  const { displayTnbRate } = usageInPeriod;
  const { displayCpRT } = usageInPeriod;

  return {
    title: titleEnum[periodType],
    displayValue,
    displayAverageKwh,
    displayTnbRate,
    isInverter,
    isLoading,
    displayCpRT,
  };
};

export default connect(mapStateToProps)(PeriodDisplay);
