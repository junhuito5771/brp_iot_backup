import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Alert,
  TouchableOpacity,
  Image,
} from 'react-native';
import { connect, usage } from '@daikin-dama/redux-iot';
import {
  widthPercentage,
  heightPercentage,
  NavigationService,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { HeaderBackButton, Colors, HeaderText } from '@module/daikin-ui';

import {
  PERIOD_DAILY,
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
} from '../../constants/timePeriod';

import PagePager from './pagePager';
import UsagePage from './usagePage';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  innerContainer: {
    paddingHorizontal: widthPercentage(7.5),
  },
  scrollContainer: {
    paddingVertical: 10,
    flexGrow: 1,
  },
  buttonContainer: {
    paddingVertical: 5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  tempText: {
    fontSize: 40,
  },
  marginTop: {
    marginTop: heightPercentage(1),
  },
  marginTop1: {
    marginTop: heightPercentage(1),
  },
  marginLeft: {
    marginLeft: 5,
  },
  flexRow: {
    flexDirection: 'row',
  },
  marginRight: {
    marginRight: 25,
  },
  hitSlop: {
    top: 20,
    right: 20,
    left: 20,
    bottom: 20,
  },
});

const pagerList = [
  `${PERIOD_DAILY}`,
  `${PERIOD_WEEKLY}`,
  `${PERIOD_MONTHLY}`,
  `${PERIOD_YEARLY}`,
];

const getPageIndex = (pagerItems, periodType) =>
  pagerItems.findIndex(pageKey => pageKey === `${periodType}`);

const renderNavigationOptions = (route, navigation) => ({
  title: <HeaderText>{route.params.acName}</HeaderText>,
  headerLeft: () => <HeaderBackButton navigation={navigation} />,
  headerRight: () => <ConnectedRefreshButton />,
});

const UsageGraph = ({
  route,
  fetchUsageData,
  error,
  clearUsageStatus,
  periodType,
  electricRate,
}) => {
  const periodPagerRef = useRef();
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  const { thingName, isInverter, enableEnergyConsumption } = route.params;

  const pagerItems = pagerList;
  const initialPage = 1;

  useEffect(() => {
    fetchUsageData({ thingName, periodType });

    if (periodType) {
      const pageIndex = getPageIndex(pagerItems, periodType);

      periodPagerRef.current.setPageWithoutAnimation(pageIndex);
    }
  }, [thingName, periodType, electricRate]);

  useEffect(() => {
    if (error) {
      Alert.alert('Error', error);
      clearUsageStatus();
    }
  }, [error]);

  return (
    <SafeAreaView style={styles.container}>
      <PagePager
        ref={periodPagerRef}
        items={pagerItems}
        initialPage={initialPage}>
        {({ item }) => {
          const [curPeriodType] = item.split('_');
          return (
            <UsagePage
              periodType={curPeriodType}
              thingName={thingName}
              isInverter={isInverter}
              enableEnergyConsumption={enableEnergyConsumption}
            />
          );
        }}
      </PagePager>
    </SafeAreaView>
  );
};

const RefreshButton = ({ fetchUsageData, thingName, periodType }) => (
  <View style={[styles.flexRow, styles.marginRight]}>
    <TouchableOpacity
      hitSlop={styles.hitSlop}
      onPress={() => {
        fetchUsageData({ thingName, periodType });
      }}>
      <Image source={require('../../shared/assets/general/refresh.png')} />
    </TouchableOpacity>
  </View>
);

RefreshButton.propTypes = {
  fetchUsageData: PropTypes.func,
  thingName: PropTypes.string,
  periodType: PropTypes.string,
};

const ConnectedRefreshButton = connect(
  ({
    usage: {
      usageSetting: { thingName, periodType },
    },
  }) => ({
    thingName,
    periodType,
  }),
  dispatch => ({
    fetchUsageData: ({ thingName, periodType }) =>
      dispatch(
        usage.fetchUsageData({
          thingName,
          periodType,
        })
      ),
  })
)(RefreshButton);

UsageGraph.propTypes = {
  fetchUsageData: PropTypes.func,
  error: PropTypes.string,
  clearUsageStatus: PropTypes.func,
  periodType: PropTypes.string,
  route: PropTypes.object,
  electricRate: PropTypes.number,
};

const mapStateToProps = ({
  usage: { curElectricRate, usageSetting = {}, error },
}) => {
  const { periodType, thingName } = usageSetting;

  return {
    periodType,
    thingName,
    error,
    electricRate: curElectricRate,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchUsageData: ({ thingName, periodType }) => {
    dispatch(usage.fetchUsageData({ thingName, periodType }));
  },
  fetchCompareUsageData: ({ thingName, periodType, targetDate }) => {
    dispatch(
      usage.fetchUsageData({
        thingName,
        periodType,
        targetDate,
        isCompare: true,
      })
    );
  },
  clearUsageStatus: () => {
    dispatch(usage.clearUsageStatus());
  },
  setSelectedPeriod: periodType => {
    dispatch(usage.setSelectedPeriod(periodType));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UsageGraph);
