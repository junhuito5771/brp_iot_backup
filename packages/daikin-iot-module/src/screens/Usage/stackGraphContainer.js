import React, { memo, useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Platform,
  TouchableHighlight,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect } from '@daikin-dama/redux-iot';
import {
  widthPercentage,
  heightPercentage,
  timeConvert,
} from '@module/utility';
import Svg, { G } from 'react-native-svg';
import moment from 'moment';
import {
  VictoryGroup,
  VictoryLine,
  VictoryChart,
  VictoryAxis,
  VictoryArea,
  VictoryLabel,
  VictoryBar,
} from 'victory-native';

import { StackGraph, Colors, Text } from '@module/daikin-ui';

import {
  HOURS,
  DAYS,
  MONTHS,
  getDayNr,
  PERIOD_DAILY,
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
} from '../../constants/timePeriod';

const getLabelByPeriod = periodType => {
  switch (periodType) {
    case PERIOD_DAILY:
      return { title: 'Hour', data: HOURS };
    case PERIOD_WEEKLY:
      return { title: 'Day', data: DAYS };
    case PERIOD_MONTHLY:
      return { title: 'Date', data: getDayNr(new Date()) };
    case PERIOD_YEARLY:
      return { title: 'Month', data: MONTHS };
    default:
      return { title: 'Hour', data: HOURS };
  }
};

const styles = StyleSheet.create({
  emptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  graph: {
    marginBottom: 2,
  },
  graphPadding: {
    left: 60,
    right: 60,
    top: 15,
    bottom: 35,
  },
});

const stylesVictory = {
  transparentFilled: {
    fill: Colors.transparent,
  },
  selectedAreaFilled: {
    fill: Colors.areaIndicationBlue,
  },
  yAxis: {
    axis: { stroke: Colors.cerulean },
    ticks: { padding: -3 },
    axisLabel: {
      fontSize: 12,
      color: Colors.black,
      fontFamily: 'Roboto',
      padding: 30,
    },
  },
  yAxisRight: {
    axis: { stroke: Colors.cerulean },
    axisLabel: {
      fontSize: 12,
      color: Colors.black,
      fontFamily: 'Roboto',
      padding: -46,
    },
    tickLabels: { textAnchor: 'start' },
    ticks: { padding: -20 },
  },
  xAxis: {
    axis: { stroke: Colors.midGrey },
    grid: {
      strokeDasharray: '5,5',
      stroke: Colors.midGrey,
      strokeWidth: 0.4,
      fillOpacity: 0.2,
    },
    tickLabels: {
      fontSize: 12,
      color: Colors.black,
      fontFamily: 'Roboto',
    },
  },
};

const handleRenderLine = (groups, bagProps) => {
  const [onOffArea, renderOnOffArea] = useState(null);
  const [onOffArea2, renderOnOffArea2] = useState(null);
  const [selectedKey, setSelectedKey] = useState(null);
  const [externalMutations, setExternalMutations] = useState(null);
  const [shiftedXaxis, shiftedXaxisSet] = useState(null);
  const [shiftedBar, shiftedBarSet] = useState(0);
  const [shiftedComparedBar, shiftedComparedBarSet] = useState(0);

  const {
    setTemp,
    indoorTemp,
    energyConsumption,
    maxDomain,
    chartProps,
    indexData,
    onOffUnit,
    electricRate,
    compareData,
    periodType,
    enableEnergyConsumption,
    isInverter,
    cpRT,
  } = bagProps || {};

  const allColor = {
    setTempColor: [Colors.sandybrown, Colors.khaki],
    indoorTempColor: [Colors.cornflowerblue, Colors.lightBlue],
    energyConsumptionsColor: [Colors.skyBlue, Colors.blue],
    cpRTColor: [Colors.red, Colors.salmon],
    electricRateColor: [Colors.red, Colors.salmon],
  };
  const [color, setColor] = useState(allColor);
  const [yKey, setYKey] = useState([]);

  useEffect(() => {
    const activeColor = {
      setTemp: [
        setTemp ? allColor.setTempColor[0] : 'transparent',
        setTemp ? allColor.setTempColor[1] : 'transparent',
      ],
      indoorTemp: [
        indoorTemp ? allColor.indoorTempColor[0] : 'transparent',
        indoorTemp ? allColor.indoorTempColor[1] : 'transparent',
      ],
      displaykWh: [
        energyConsumption && enableEnergyConsumption && isInverter
          ? allColor.energyConsumptionsColor[0]
          : 'transparent',
        energyConsumption && enableEnergyConsumption && isInverter
          ? allColor.energyConsumptionsColor[1]
          : 'transparent',
      ],
      electricRate: [
        electricRate && enableEnergyConsumption && isInverter
          ? allColor.electricRateColor[0]
          : 'transparent',
        electricRate && enableEnergyConsumption && isInverter
          ? allColor.electricRateColor[1]
          : 'transparent',
      ],
      cpRT: [
        cpRT && !isInverter ? allColor.cpRTColor[0] : 'transparent',
        cpRT && !isInverter ? allColor.cpRTColor[1] : 'transparent',
      ],
    };

    const useYKey = [];
    Object.entries(activeColor).forEach(cl => {
      if (cl[1][0] !== 'transparent') {
        useYKey.push(cl[0]);
      }
    });
    setColor(activeColor);
    setYKey(useYKey.length ? useYKey : ['indoorTemp']);
  }, [
    setTemp,
    indoorTemp,
    energyConsumption,
    electricRate,
    enableEnergyConsumption,
    isInverter,
    cpRT,
  ]);

  const DEFAULT_INTERPOLUTION_LINE = 'stepAfter';
  const xKey = 'updatedOn';
  const drawAreaOnOff = (objData, whichArea, isoffAll = false) => {
    const areaColor =
      whichArea === 0 ? Colors.onOffBlue : Colors.onOffComparedBlue;

    const render = objData.map((id, index) => {
      const currentColor =
        objData[index].SetOnOff === 0 || isoffAll
          ? Colors.transparent
          : areaColor;

      return (
        <VictoryArea
          key={id}
          style={{ data: { fill: currentColor } }}
          data={[
            { x: index + 1, y: maxDomain },
            { x: index + 2, y: maxDomain },
          ]}
        />
      );
    });

    if (whichArea === 0) renderOnOffArea(render);
    else renderOnOffArea2(render);
  };

  useEffect(() => {
    drawAreaOnOff(groups[0].data, 0, !onOffUnit);

    if (typeof compareData !== 'undefined') {
      drawAreaOnOff(compareData, 1, !onOffUnit);
    }

    if (compareData) renderOnOffArea2(null);
  }, [groups, onOffUnit, compareData]);

  useEffect(() => {
    switch (periodType) {
      case 'daily':
        shiftedXaxisSet(14);
        shiftedBarSet(0);
        shiftedComparedBarSet(4);
        break;
      case 'weekly':
        shiftedXaxisSet(21);
        shiftedBarSet(15);
        shiftedComparedBarSet(25);
        break;
      case 'monthly':
        shiftedXaxisSet(5);
        shiftedBarSet(0);
        shiftedComparedBarSet(4);
        break;
      case 'yearly':
        shiftedXaxisSet(14);
        shiftedBarSet(8);
        shiftedComparedBarSet(4);
        break;
      default:
        break;
    }
    return () => {};
  }, [periodType]);

  const DEFAULT_DOMAIN_PADDING = {
    x: 1,
    y: 40,
  };

  const DEFAULT_GRAPH_HEIGHT = heightPercentage(45);

  if (!groups || groups.length === 0) return null;

  const cprtTimeConverted = data => {
    const { rhours, rminutes } = timeConvert(data);
    if (periodType === PERIOD_DAILY) return rminutes;
    return rhours;
  };

  const Wrapper = Platform.select({
    ios: TouchableHighlight,
    android: Svg,
  });

  const getLineData = groupsData => {
    const haveNullData = [];
    let prevData = groupsData[0];
    groupsData.forEach(d => {
      if (d.isPopulate !== 0) {
        haveNullData.push({
          updatedOn: d.updatedOn,
          setTemp: prevData.setTemp,
          indoorTemp: prevData.indoorTemp,
          kWh: d.kWh,
          displaykWh: d.displaykWh,
          electricRate: prevData.electricRate,
          isPopulate: d.isPopulate,
        });
      }

      haveNullData.push({
        updatedOn: d.updatedOn,
        setTemp: d.isPopulate !== 0 ? null : d.setTemp,
        indoorTemp: d.isPopulate !== 0 ? null : d.indoorTemp,
        kWh: d.kWh,
        displaykWh: d.displaykWh,
        electricRate: d.isPopulate !== 0 || d.kWh === 0 ? null : d.electricRate,
        isPopulate: d.isPopulate,
      });

      if (haveNullData.length === 1) {
        haveNullData.push(haveNullData[haveNullData.length - 1]);
      }

      prevData = d;
    });
    return haveNullData;
  };

  const roundToNext5 = val => Math.ceil(val / 5) * 5; // round to next multiple of 5

  const getMaxima = curYkey => {
    let maxima1 = 0;
    const maxima0 = Math.max(
      ...groups[0].data.map(a =>
        curYkey === 'cpRT' ? cprtTimeConverted(a[curYkey]) : a[curYkey]
      )
    );

    if (groups.length > 1) {
      maxima1 = Math.max(
        ...groups[1].data.map(a =>
          curYkey === 'cpRT' ? cprtTimeConverted(a[curYkey]) : a[curYkey]
        )
      );
    }
    const value = maxima1 > maxima0 ? maxima1 : maxima0;

    if (value < 1) return 1;
    if (value % 5 > 0) return value;

    return roundToNext5(value);
  };

  const getMaximaShare = (curYkey, type) => {
    let maxima1 = 0;
    const maxima0 = Math.max(...groups[0].data.map(a => a[curYkey]));

    if (groups.length > 1) {
      maxima1 = Math.max(...groups[1].data.map(a => a[curYkey]));
    }

    let maximaSibling = 0;
    if (type === 'temp') {
      maximaSibling = getMaxima(
        curYkey === 'setTemp' ? 'indoorTemp' : 'setTemp'
      );
    } else {
      maximaSibling = getMaxima(curYkey === 'kWh' ? 'electricRate' : 'kWh');
    }

    const maximaSelf = maxima1 > maxima0 ? maxima1 : maxima0;
    const value = maximaSibling > maximaSelf ? maximaSibling : maximaSelf;

    if (value === 0 || (value === 1 && curYkey !== 'kWh')) {
      return 35;
    }
    if (value === 1 && curYkey === 'kWh') {
      return value;
    }
    if (value % 5 !== 0) {
      return roundToNext5(value);
    }
    return value;
  };

  const normedScale = [
    { max: 1, factora: 0.2 },
    { max: 2, factora: 0.2 },
    { max: 5, factora: 1 },
    { max: 10, factora: 1 },
    { max: 12, factora: 2 },
    { max: 25, factora: 5 },
    { max: 50, factora: 10 },
    { max: 100, factora: 10 },
    { max: 1200, factora: 200 },
    { max: 2000, factora: 250 },
  ];
  let maximakWh = getMaximaShare('kWh');

  let factora = 0;
  for (let i = 0; i < normedScale.length; i += 1) {
    const nextScale = normedScale[i + 1] || { max: 200, factora: 50 };
    if (maximakWh <= nextScale.max && maximakWh >= normedScale[i].max) {
      maximakWh = nextScale.max;
      factora = nextScale.factora;
      break;
    }
  }
  if (factora === 0) {
    let dividenta = 100;
    maximakWh =
      maximakWh % dividenta === 0
        ? maximakWh
        : Math.ceil(maximakWh / dividenta) * dividenta;
    let quotienta = (Math.ceil(maximakWh / dividenta) * dividenta) / dividenta;
    while (quotienta > 10) {
      dividenta *= 10;
      quotienta = (Math.ceil(maximakWh / dividenta) * dividenta) / dividenta;
      maximakWh =
        maximakWh % dividenta === 0
          ? maximakWh
          : Math.ceil(maximakWh / dividenta) * dividenta;
    }
    factora = maximakWh / 100 < 4 ? 50 : 100;
  }

  const normedkWh = [];
  for (let i = factora; i <= maximakWh; i += factora) {
    normedkWh.push(((i / factora) * 1) / (maximakWh / factora));
  }

  const maximaTemp = getMaximaShare('indoorTemp', 'temp');
  factora = 5;
  while (maximaTemp % factora > 0) {
    factora += 1;
  }
  const normedTemp = [];
  for (let i = 5; i <= maximaTemp; i += 5) {
    normedTemp.push(((i / factora) * 1) / (maximaTemp / factora));
  }

  let maximacpRT = getMaxima('cpRT') || 5;
  factora = 5;
  for (let i = 0; i < normedScale.length; i += 1) {
    const nextScale = normedScale[i + 1] || { max: 200, factora: 50 };
    if (maximacpRT <= nextScale.max && maximacpRT >= normedScale[i].max) {
      maximacpRT = nextScale.max;
      factora = nextScale.factora;
      break;
    }
  }
  const normedcpRT = [];
  for (let i = factora; i <= maximacpRT; i += factora) {
    normedcpRT.push(((i / factora) * 1) / (maximacpRT / factora));
  }

  const showDecimal = (t, max) => (max <= 2 ? (t * max).toFixed(1) : t * max);

  return (
    <Wrapper
      width={chartProps.width}
      height={chartProps.height}
      viewBox={`0 0 ${chartProps.width} ${chartProps.height}`}>
      <VictoryChart
        domain={{ y: [0, 1] }}
        maxDomain={{ y: maxDomain }}
        domainPadding={DEFAULT_DOMAIN_PADDING}
        padding={styles.graphPadding}
        height={DEFAULT_GRAPH_HEIGHT} // Default height
        width={chartProps.width}
        externalEventMutations={externalMutations}
        {...chartProps}
        events={[
          {
            childName: 'all',
            eventHandlers: {
              onPressIn: (cl, props) => {
                if (props.data)indexData(props.data[0].x);
                if (props.key.split('-data')[0] !== selectedKey) {
                  setExternalMutations([
                    {
                      childName: selectedKey,
                      target: ['data'],
                      eventKey: 'all',
                      mutation: () => ({
                        style: stylesVictory.transparentFilled,
                      }),
                    },
                  ]);
                }
                setSelectedKey(props.key.split('-data')[0]);
                return [
                  {
                    mutation: () => ({
                      style: stylesVictory.selectedAreaFilled,
                    }),
                  },
                ];
              },
            },
          },
        ]}>
        {onOffArea}
        {onOffArea2}

        <VictoryAxis
          tickLabelComponent={<VictoryLabel dx={shiftedXaxis} />}
          fixLabelOverlap
          events={[
            {
              childName: 'all',
              eventHandlers: {
                onPressIn: () => {},
              },
            },
          ]}
          style={stylesVictory.xAxis}
        />

        <VictoryAxis
          dependentAxis
          label="Temperature (°C)"
          orientation="left"
          tickValues={normedTemp}
          tickFormat={t => showDecimal(t, maximaTemp)}
          style={stylesVictory.yAxis}
        />
        <VictoryGroup>
          {groups.map(({ data }, id) =>
            yKey.map(curYKey => {
              if (
                curYKey !== 'displaykWh' &&
                curYKey !== 'cpRT' &&
                curYKey !== 'electricRate'
              ) {
                const lineData = getLineData(data);
                return (
                  <VictoryLine
                    // key={curYKey}
                    interpolation={DEFAULT_INTERPOLUTION_LINE}
                    data={lineData}
                    x={xKey}
                    y={datum => datum[curYKey] / maximaTemp}
                    y0={curYKey}
                    style={{
                      data: {
                        stroke: () => color[curYKey][id],
                      },
                    }}
                  />
                );
              }
              return null;
            })
          )}
        </VictoryGroup>

        {isInverter && enableEnergyConsumption && (
          <VictoryAxis
            dependentAxis
            height={chartProps.height}
            label="Energy Consumption (kWh)"
            offsetX={chartProps.width - 60}
            tickValues={normedkWh}
            tickFormat={t => showDecimal(t, maximakWh)}
            style={stylesVictory.yAxisRight}
          />
        )}

        {!isInverter && !enableEnergyConsumption && (
          <VictoryAxis
            dependentAxis
            height={chartProps.height}
            label={
              periodType !== PERIOD_DAILY
                ? 'Compressor Running Time (hours)'
                : 'Compressor Running Time (mins)'
            }
            offsetX={chartProps.width - 60}
            tickValues={normedcpRT}
            tickFormat={t => showDecimal(t, maximacpRT)}
            style={stylesVictory.yAxisRight}
          />
        )}

        <VictoryGroup offset={0}>
          {groups.map(({ data }, id) =>
            yKey.map(curYKey => {
              if (curYKey === 'electricRate') {
                const lineData = getLineData(data);
                return (
                  <VictoryLine
                    key={curYKey}
                    interpolation={DEFAULT_INTERPOLUTION_LINE}
                    data={lineData}
                    x={xKey}
                    y={datum => datum[curYKey] / maximakWh}
                    y0={curYKey}
                    style={{
                      data: {
                        stroke: () => color[curYKey][id],
                      },
                    }}
                  />
                );
              }
              if (curYKey === 'displaykWh') {
                if (energyConsumption && enableEnergyConsumption) {
                  return (
                    <VictoryBar
                      key={curYKey}
                      data={data}
                      x="updatedOn"
                      y={datum => datum.kWh / maximakWh}
                      groupComponent={
                        <G
                          transform={`translate(${
                            id === 1 ? shiftedComparedBar : shiftedBar
                          }, 0)`}
                        />
                      }
                      alignment="start"
                      barWidth={groups.length > 1 ? 2 : 5}
                      style={{
                        data: {
                          fill: () => color[curYKey][id],
                        },
                      }}
                    />
                  );
                }
                return null;
              }
              if (cpRT) {
                return (
                  <VictoryBar
                    key={curYKey}
                    data={data}
                    x="updatedOn"
                    y={datum => cprtTimeConverted(datum.cpRT) / maximacpRT}
                    alignment="start"
                    groupComponent={
                      <G
                        transform={`translate(${
                          id === 1 ? shiftedComparedBar : shiftedBar
                        }, 0)`}
                      />
                    }
                    barWidth={groups.length > 1 ? 2 : 5}
                    style={{
                      data: {
                        fill: () => color[curYKey][id],
                      },
                    }}
                  />
                );
              }
              return null;
            })
          )}
        </VictoryGroup>

        {groups[0].data.map((id, i) => {
          const no = i;
          return (
            <VictoryArea
              name={`Area-${i}`}
              key={`Area-${no}`}
              style={{ data: stylesVictory.transparentFilled }}
              data={[
                { x: i, y: maxDomain },
                { x: i + 1, y: maxDomain },
              ]}
            />
          );
        })}
      </VictoryChart>
    </Wrapper>
  );
};

handleRenderLine.propTypes = {
  key: PropTypes.string,
  data: PropTypes.array,
};

export function getGroups(data, compareData, compareDate, periodType) {
  let groups = [];
  const comparedDays = Number(moment(compareDate).endOf('months').format('DD'));
  const curDateDays = Number(moment().endOf('months').format('DD'));

  groups = [{ id: 'group-1', data }];

  if (compareData && compareData.length > 0) {
    if (periodType === PERIOD_MONTHLY && comparedDays !== curDateDays) {
      let newDataArr = Object.values(
        curDateDays > comparedDays ? compareData : data,
      );
      newDataArr.pop();
      const toFillData = new Array(
        (curDateDays > comparedDays ? curDateDays + 1 : comparedDays + 1) -
          newDataArr.length,
      ).fill(null);
      newDataArr = [
        ...newDataArr,
        ...toFillData.map((a, i) => ({
          updatedOn:
            i === toFillData.length - 1 ? ' ' : `${newDataArr.length + 1 + i}`,
          setTemp: 0,
          indoorTemp: 0,
          SetOnOff: 0,
          kWh: 0,
          displaykWh: 0,
          electricRate: 0,
          cpRT: 0,
          isPopulate: -1,
        })),
      ];

      if (curDateDays > comparedDays) {
        groups.push({ id: 'group-2', data: newDataArr });
      } else {
        groups[0].data = newDataArr;
        groups.push({ id: 'group-2', data: compareData });
      }
    } else {
      groups.push({ id: 'group-2', data: compareData });
    }
  }
  return groups;
}

function canRender(prevProps, nextProps) {
  return !nextProps.isFocus;
}

const StackGraphContainer = memo(
  ({
    periodType,
    data,
    compareData,
    setTemp,
    onOffUnit,
    electricRate,
    indoorTemp,
    energyConsumption,
    indexData,
    enableEnergyConsumption,
    isInverter,
    isLoading,
    compareDate,
    cpRT,
  }) => {
    const { title: xLabelTitle, data: xAxisLabels } = getLabelByPeriod(
      periodType
    );

    const graphContainerWidth = widthPercentage(100);

    const graphContainerHeight = Platform.select({
      ios: heightPercentage(35),
      android: heightPercentage(50),
    });

    const groups = getGroups(data, compareData, compareDate, periodType);

    const emptyContainerStyle = StyleSheet.flatten([
      styles.emptyContainer,
      styles.graph,
      { height: graphContainerHeight },
    ]);

    // If current graph container is focus but don't have data
    // Show loading
    if (isLoading) {
      // if (isFocus && !data) {
      return (
        <View style={emptyContainerStyle}>
          <ActivityIndicator size="large" color={Colors.azureRad} />
        </View>
      );
    }

    // If current graph has data and focused
    // Then show the graph
    if (isLoading || data) {
      return (
        <StackGraph
          containerStyle={styles.graph}
          data={groups}
          compareData={compareData}
          renderLine={handleRenderLine}
          maxDomain={1}
          xAxisProps={{ xLabelTitle, xAxisLabels }}
          chartProps={{
            width: graphContainerWidth,
            height: graphContainerHeight,
          }}
          setTemp={setTemp}
          onOffUnit={onOffUnit}
          electricRate={electricRate}
          indoorTemp={indoorTemp}
          energyConsumption={energyConsumption}
          indexData={indexData}
          periodType={periodType}
          enableEnergyConsumption={enableEnergyConsumption}
          isInverter={isInverter}
          cpRT={cpRT}
        />
      );
    }

    // Return the empty container by default
    return (
      <View style={emptyContainerStyle}>
        <Text>No Data</Text>
      </View>
    );
  },
  canRender
);

StackGraphContainer.propTypes = {
  periodType: PropTypes.string,
  data: PropTypes.array,
  compareData: PropTypes.array,
  isLoading: PropTypes.bool,
  setTemp: PropTypes.bool,
  onOffUnit: PropTypes.bool,
  electricRate: PropTypes.bool,
  indoorTemp: PropTypes.bool,
  energyConsumption: PropTypes.bool,
  indexData: PropTypes.func,
  enableEnergyConsumption: PropTypes.bool,
  isInverter: PropTypes.bool,
  cpRT: PropTypes.bool,
  compareDate: PropTypes.any,
};

const mapStateToProps = (
  {
    usage: {
      isLoading,
      usageByPeriod,
      usageToggle,
      usageSetting: { thingName, periodType: curPeriod },
    },
  },
  { periodType }
) => {
  const usageInThingName = usageByPeriod[thingName] || {};
  const usageInPeriod = usageInThingName[periodType] || {};
  const usageToggleData = usageToggle[thingName] || {};
  const usageTogglePeriod = usageToggleData[periodType] || {};
  return {
    data: usageInPeriod.data,
    compareData: usageInPeriod.compareData,
    compareDate: usageInPeriod.compareDate,
    isFocus: curPeriod === periodType,
    isLoading,
    setTemp: usageTogglePeriod.setTemp,
    indoorTemp: usageTogglePeriod.indoorTemp,
    energyConsumption: usageTogglePeriod.energyConsumption,
    onOffUnit: usageTogglePeriod.onOffUnit,
    cpRT: usageTogglePeriod.cpRT,
    electricRate: usageTogglePeriod.electricRate,
  };
};

const mapDispatchToProps = () => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StackGraphContainer);
