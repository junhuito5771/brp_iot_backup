import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from '@daikin-dama/redux-iot';
import moment from 'moment';
import { widthPercentage, timeConvert } from '@module/utility';
import { Text, Colors } from '@module/daikin-ui';
import {
  PERIOD_DAILY,
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
} from '../../constants/timePeriod';
import { getGroups } from './stackGraphContainer';

const STANDARD_DATE_FORMAT = 'DD MMMM YYYY';

const Style = StyleSheet.create({
  container: {
    marginHorizontal: 20,
    marginVertical: 10,
  },
  innerContainer: {
    backgroundColor: Colors.onOffBlue,
    flexDirection: 'row',
    padding: 10,
    justifyContent: 'space-between',
  },
  titleInfoText: {
    color: Colors.blue,
    fontWeight: 'bold',
  },
  titleInfoSubtext: {
    color: Colors.blue,
    fontSize: 12,
    marginTop: 3,
  },
  groupInfo: {
    flexDirection: 'column',
    width: widthPercentage(42),
  },
  marginRight4: {
    marginRight: widthPercentage(2),
  },
  marginLeft10: {
    marginLeft: 10,
  },
  flexRow: {
    flexDirection: 'row',
  },
  legendContainer: {
    padding: 5,
    marginTop: -10,
    marginBottom: 10,
    marginLeft: 20,
  },
  legendColumn: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  legendRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  legendLabel: {
    borderRadius: 15,
    width: 10,
    height: 10,
  },
});

function getDateFormat(periodType, date, newLine = false) {
  switch (periodType) {
    case PERIOD_WEEKLY: {
      const firstDayOfWeek = moment(date, STANDARD_DATE_FORMAT)
        .startOf('isoWeek')
        .format('DD MMM YYYY');
      const lastDayOfWeek = moment(date, STANDARD_DATE_FORMAT)
        .endOf('isoWeek')
        .format('DD MMM YYYY');

      return `${firstDayOfWeek} - ${lastDayOfWeek}  ${newLine ? '\n' : ''}`;
    }
    case PERIOD_MONTHLY:
      return `${moment(date, STANDARD_DATE_FORMAT).format('MMMM YYYY')} ${
        newLine ? '\n' : ''
      }`;
    case PERIOD_YEARLY:
      return `${moment(date, STANDARD_DATE_FORMAT).format('YYYY')}  ${
        newLine ? '\n' : ''
      }`;
    default:
      return `${moment(date, STANDARD_DATE_FORMAT).format(
        STANDARD_DATE_FORMAT
      )}  ${newLine ? '\n' : ''}`;
  }
}

function getCurrentData(indexValue, data) {
  if (typeof data !== 'undefined') {
    const item = indexValue - 1;
    if (isNaN(indexValue)) return 0;
    if (data.length === indexValue) return item;
    if (item === -1) return 0;

    return item;
  }
  return 0;
}

function colorProps(
  setTemp,
  indoorTemp,
  energyConsumption,
  onOffUnit,
  cpRT,
  electricRate
) {
  if (indoorTemp) {
    return {
      color: [Colors.cornflowerblue, Colors.lightBlue],
    };
  }
  if (setTemp) {
    return {
      color: [Colors.sandybrown, Colors.khaki],
    };
  }
  if (energyConsumption) {
    return {
      color: [Colors.skyBlue, Colors.blue],
    };
  }
  if (onOffUnit) {
    return {
      color: [Colors.onOffBlue, Colors.onOffComparedBlue],
    };
  }
  if (cpRT) {
    return {
      color: [Colors.salmon, Colors.red],
    };
  }
  if (electricRate) {
    return {
      color: [Colors.salmon, Colors.red],
    };
  }
  return { color: Colors.transparent };
}

const cprtTimeConverted = (data, index, periodType) => {
  const { rhours, rminutes } = timeConvert(data[index].cpRT);
  if (periodType === PERIOD_DAILY) return `${rminutes} mins`;
  return `${rhours} hours ${rminutes} mins`;
};

const GraphInfo = ({
  periodType,
  data,
  compareData,
  indexValue,
  compareDate,
  isInverter,
  enableEnergyConsumption,
  setTemp,
  indoorTemp,
  energyConsumption,
  onOffUnit,
  cpRT,
  electricRate,
}) => {
  const now = moment().format(STANDARD_DATE_FORMAT);
  const comparisonDate = moment(compareDate).format(STANDARD_DATE_FORMAT);
  const groups = getGroups(data, compareData, compareDate, periodType);
  const curPeriodData = groups[0].data;
  const currentInfoIndicator = getCurrentData(indexValue, curPeriodData);

  const getValue = (type, isCompare) => {
    if (isCompare) {
      if (
        groups[1].data[currentInfoIndicator] === undefined ||
        groups[1].data[currentInfoIndicator][type] === 0 ||
        groups[1].data[currentInfoIndicator][type] === null ||
        groups[1].data[currentInfoIndicator].isPopulate === 1
      ) {
        return '-';
      }
      return groups[1].data[currentInfoIndicator][type]
        .toFixed(type === 'electricRate' ? 2 : 1)
        .toString();
    }

    if (
      curPeriodData[currentInfoIndicator] === undefined ||
      curPeriodData[currentInfoIndicator][type] === 0 ||
      curPeriodData[currentInfoIndicator][type] === null ||
      curPeriodData[currentInfoIndicator].isPopulate === 1
    ) {
      return '-';
    }
    return curPeriodData[currentInfoIndicator][type]
      .toFixed(type === 'electricRate' ? 2 : 1)
      .toString();
  };

  const { color } = colorProps(
    setTemp,
    indoorTemp,
    energyConsumption,
    onOffUnit,
    cpRT,
    electricRate
  );

  if (typeof data !== 'undefined') {
    return (
      <View style={Style.container}>
        {compareData && (
          <View style={Style.legendContainer}>
            <View style={Style.legendColumn}>
              <View style={Style.legendRow}>
                <View
                  style={[Style.legendLabel, { backgroundColor: color[0] }]}
                />
                <Text> {getDateFormat(periodType, moment(), false)}</Text>
              </View>
              <View style={Style.legendRow}>
                <View
                  style={[Style.legendLabel, { backgroundColor: color[1] }]}
                />
                <Text>
                  {' '}
                  {getDateFormat(
                    periodType,
                    moment(compareDate).format(STANDARD_DATE_FORMAT),
                    false
                  )}
                </Text>
              </View>
            </View>
          </View>
        )}
        <View style={Style.flexRow}>
          <Text style={Style.titleInfoText}>
            {getDateFormat(periodType, now, false)}
          </Text>
          <Text style={[Style.titleInfoText, Style.marginLeft10]}>
            {curPeriodData[currentInfoIndicator].updatedOn === ' '
              ? 31
              : `${curPeriodData[currentInfoIndicator].updatedOn}`}
          </Text>
        </View>
        <View style={Style.innerContainer}>
          <View style={Style.groupInfo}>
            <Text style={Style.titleInfoText}>Set Temperature</Text>
            <Text style={Style.titleInfoSubtext}>
              {compareData && getDateFormat(periodType, now, true)}
              {getValue('setTemp')} °C{' '}
            </Text>
            {compareData && (
              <Text style={Style.titleInfoSubtext}>
                {getDateFormat(periodType, comparisonDate, true)}
                {getValue('setTemp', 'isCompare')} °C{' '}
              </Text>
            )}
          </View>
          <View style={[Style.groupInfo, Style.marginRight4]}>
            <Text style={Style.titleInfoText}>Indoor Temperature</Text>
            <Text style={Style.titleInfoSubtext}>
              {compareData && getDateFormat(periodType, now, true)}
              {getValue('indoorTemp')} °C
            </Text>
            {compareData && (
              <Text style={Style.titleInfoSubtext}>
                {getDateFormat(periodType, comparisonDate, true)}
                {getValue('indoorTemp', 'isCompare')} °C{' '}
              </Text>
            )}
          </View>
        </View>
        <View style={Style.innerContainer}>
          {isInverter && enableEnergyConsumption ? (
            <View style={Style.groupInfo}>
              <Text style={Style.titleInfoText}>Electricity Bill</Text>
              <Text style={Style.titleInfoSubtext}>
                {compareData && getDateFormat(periodType, now, true)}
                {`RM ${getValue('electricRate')}`}
              </Text>
              {compareData && (
                <Text style={Style.titleInfoSubtext}>
                  {getDateFormat(periodType, comparisonDate, true)}
                  {`RM ${getValue('electricRate', 'isCompare')}`}
                </Text>
              )}
            </View>
          ) : (
            <View style={Style.groupInfo}>
              <Text style={Style.titleInfoText}>Unit ON/OFF</Text>
              <Text style={Style.titleInfoSubtext}>
                {compareData && getDateFormat(periodType, now, true)}
                {getValue('SetOnOff') === 0 ? 'OFF' : 'ON'}{' '}
              </Text>
              {compareData && (
                <Text style={Style.titleInfoSubtext}>
                  {getDateFormat(periodType, comparisonDate, true)}
                  {getValue('SetOnOff', 'isCompare') === 0 ? 'OFF' : 'ON'}{' '}
                </Text>
              )}
            </View>
          )}
          {isInverter && enableEnergyConsumption ? (
            <View style={[Style.groupInfo, Style.marginRight4]}>
              <Text style={Style.titleInfoText}>Energy Consumption</Text>
              <Text style={Style.titleInfoSubtext}>
                {compareData && getDateFormat(periodType, now, true)}
                {getValue('kWh')} kWh
              </Text>
              {compareData && (
                <Text style={Style.titleInfoSubtext}>
                  {getDateFormat(periodType, comparisonDate, true)}
                  {getValue('kWh', 'isCompare')} kWh
                </Text>
              )}
            </View>
          ) : null}

          {!isInverter && !enableEnergyConsumption && (
            <View style={[Style.groupInfo, Style.marginRight4]}>
              <Text style={Style.titleInfoText}>Compressor Running Time</Text>
              <Text style={Style.titleInfoSubtext}>
                {compareData && getDateFormat(periodType, now, true)}
                {cprtTimeConverted(
                  curPeriodData,
                  currentInfoIndicator,
                  periodType,
                )}
              </Text>
              {compareData && (
                <Text style={Style.titleInfoSubtext}>
                  {getDateFormat(periodType, comparisonDate, true)}
                  {cprtTimeConverted(
                    compareData,
                    currentInfoIndicator,
                    periodType
                  )}
                </Text>
              )}
            </View>
          )}
        </View>
      </View>
    );
  }
  return null;
};

GraphInfo.propTypes = {
  periodType: PropTypes.string,
  data: PropTypes.array,
  compareData: PropTypes.array,
  indexValue: PropTypes.number,
  isInverter: PropTypes.bool,
  enableEnergyConsumption: PropTypes.bool,
  compareDate: PropTypes.any,
  setTemp: PropTypes.bool,
  indoorTemp: PropTypes.bool,
  energyConsumption: PropTypes.bool,
  onOffUnit: PropTypes.bool,
  cpRT: PropTypes.bool,
  electricRate: PropTypes.bool,
};

const mapStateToProps = (
  { usage: { usageByPeriod, usageToggle } },
  { periodType, thingName }
) => {
  const usageInThingName = usageByPeriod[thingName] || {};
  const usageInPeriod = usageInThingName[periodType] || {};
  const usageToggleData = usageToggle[thingName] || {};
  const usageTogglePeriod = usageToggleData[periodType] || {};

  return {
    data: usageInPeriod.data,
    compareData: usageInPeriod.compareData,
    compareDate: usageInPeriod.compareDate,
    setTemp: usageTogglePeriod.setTemp,
    indoorTemp: usageTogglePeriod.indoorTemp,
    energyConsumption: usageTogglePeriod.energyConsumption,
    onOffUnit: usageTogglePeriod.onOffUnit,
    cpRT: usageTogglePeriod.cpRT,
    electricRate: usageTogglePeriod.electricRate,
  };
};

export default connect(mapStateToProps)(GraphInfo);
