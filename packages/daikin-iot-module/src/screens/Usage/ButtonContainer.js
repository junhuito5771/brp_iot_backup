import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from '@daikin-dama/redux-iot';
import { scale, verticalScale } from '@module/utility';
import { Button, Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  innerContainer: {
    paddingHorizontal: scale(18),
    paddingVertical: verticalScale(13),
  },
  buttonContainer: {
    paddingHorizontal: scale(5),
    width: scale(150),
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  passiveButton: {
    backgroundColor: Colors.lightCoolGrey,
  },
  textLabel: {
    color: Colors.white,
  },
});

const ButtonContainer = ({
  onPressCompare,
  onPressClear,
  isLoading,
  compareData,
  enableCompare,
  periodType,
}) => (
  <View style={[styles.innerContainer, styles.row]}>
    <Button
      primary
      disabledPrimary
      disabled={enableCompare && periodType === 'yearly'}
      isLoading={isLoading}
      onPress={onPressCompare}
      style={styles.buttonContainer}
      textStyle={styles.textLabel}>
      Compare
    </Button>
    <Button
      disabled={!compareData}
      isLoading={isLoading}
      onPress={onPressClear}
      style={styles.buttonContainer}>
      Clear
    </Button>
  </View>
);

ButtonContainer.propTypes = {
  onPressCompare: PropTypes.func,
  onPressClear: PropTypes.func,
  isLoading: PropTypes.bool,
  enableCompare: PropTypes.bool,
  compareData: PropTypes.array,
  periodType: PropTypes.string,
};

const mapStateToProps = ({
  usage: {
    isLoading,
    usageByPeriod,
    usageSetting: { thingName, periodType: curPeriod },
  },
}) => {
  const usageInThingName = usageByPeriod[thingName] || {};
  const usageInPeriod = usageInThingName[curPeriod] || {};
  return {
    isLoading,
    compareData: usageInPeriod.compareData,
    periodType: curPeriod,
  };
};

export default connect(mapStateToProps)(ButtonContainer);
