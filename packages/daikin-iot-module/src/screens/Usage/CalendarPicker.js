import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { Calendar } from 'react-native-calendars';
import moment from 'moment';

import { Colors } from '@module/daikin-ui';

const CALENDAR_DATE_FORMAT = 'YYYY-MM-DD';
const TIMESTAMP_FORMAT = 'x';

const styles = StyleSheet.create({
  container: {
    width: 350,
    height: 300,
    padding: 2,
    marginBottom: 50,
  },
  calendar: {
    marginBottom: 20,
  },
});

function getMarkedDates(targetDate, dateFormat) {
  if (!targetDate) return {};
  const momentDate = moment(targetDate, dateFormat);
  return Array(7)
    .fill(0)
    .reduce((acc, cur, index) => {
      const currentDay = momentDate.startOf('isoWeek').add(index, 'd');
      const isFirst = index === 0;
      const isLast = index === 6;
      acc[currentDay.format(CALENDAR_DATE_FORMAT)] = {
        color: Colors.pictonBlue,
        ...(isFirst ? { startingDay: true, selected: true, marked: true } : {}),
        ...(isLast ? { endingDay: true, selected: true, marked: true } : {}),
      };

      return acc;
    }, {});
}

const CalendarPicker = forwardRef(
  ({ minimumDate, maximumDate, formatDate, selectedDate }, ref) => {
    const [curDate, setCurDate] = useState(
      moment(selectedDate, formatDate).format(CALENDAR_DATE_FORMAT)
    );

    const markedDates = getMarkedDates(curDate, CALENDAR_DATE_FORMAT);

    const handleOnDayPressed = date => {
      const dateResult = moment(
        date.timestamp.toString(),
        TIMESTAMP_FORMAT
      ).format(CALENDAR_DATE_FORMAT);

      setCurDate(dateResult);
    };

    useEffect(() => {
      if (selectedDate) {
        setCurDate(
          moment(selectedDate, formatDate).format(CALENDAR_DATE_FORMAT)
        );
      }
    }, [selectedDate]);

    useImperativeHandle(
      ref,
      () => ({
        getValue: () => {
          const markedDateInArray = Object.keys(markedDates);

          return moment(markedDateInArray[0], CALENDAR_DATE_FORMAT).format(
            formatDate
          );
        },
      }),
      [curDate]
    );

    return (
      <View style={styles.container}>
        <Calendar
          style={styles.calendar}
          firstDay={1} // Start from monday
          current={moment(curDate, CALENDAR_DATE_FORMAT).format(
            CALENDAR_DATE_FORMAT
          )}
          minDate={moment(minimumDate, formatDate).format(CALENDAR_DATE_FORMAT)}
          maxDate={moment(maximumDate, formatDate)
            .subtract(7, 'days')
            .format(CALENDAR_DATE_FORMAT)}
          onDayPress={handleOnDayPressed}
          markedDates={markedDates}
          markingType="period"
        />
      </View>
    );
  }
);

CalendarPicker.propTypes = {
  minimumDate: PropTypes.string,
  maximumDate: PropTypes.string,
  formatDate: PropTypes.string,
  selectedDate: PropTypes.string,
};

CalendarPicker.defaultProps = {
  selectedDate: moment().format(CALENDAR_DATE_FORMAT),
};

export default CalendarPicker;
