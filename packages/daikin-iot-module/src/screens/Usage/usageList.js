import React from 'react';
import PropTypes from 'prop-types';
import {
  SafeAreaView,
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {
  isSmallDevice,
  ScaleText,
  verticalScale,
  widthPercentage,
  scale,
  NavigationService,
} from '@module/utility';

import { connect, selector, usage } from '@daikin-dama/redux-iot';
import {
  Text,
  Colors,
  NavDrawerMenu,
  HeaderText,
  getACIconFromPath,
} from '@module/daikin-ui';

import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { PERIOD_WEEKLY } from '../../constants/timePeriod';
import { USAGE_GRAPH_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  bgGrey: {
    backgroundColor: Colors.offWhite,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center',
    paddingHorizontal: 25,
  },
  description: {
    height: verticalScale(60),
  },
  header: {
    height: verticalScale(60),
  },
  title: {
    fontSize: ScaleText(16),
  },
  content: {
    height: verticalScale(70),
    paddingHorizontal: scale(20),
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
    justifyContent: 'space-between',
    backgroundColor: Colors.white,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  grey: {
    color: Colors.grey,
  },
  aircondIcon: {
    marginRight: 15,
    minWidth: 56,
  },
  marginTop: {
    marginTop: 4,
  },
  withBarName: {
    maxWidth: isSmallDevice ? widthPercentage(20) : widthPercentage(30),
  },
  withoutBarName: {
    maxWidth: widthPercentage(70),
  },
  bar: {
    height: 14,
    backgroundColor: Colors.stoneGrey,
    borderRadius: 7,
    overflow: 'hidden',
  },
  barContainer: {
    width: widthPercentage(40),
    alignItems: 'center',
  },
  numbers: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  barColorFill: {
    height: 14,
    backgroundColor: Colors.dryModeFill,
    top: 0,
  },
  blue: {
    color: Colors.dryModeFill,
  },
  width: {
    width: widthPercentage(35),
  },
  kw: {
    alignSelf: 'flex-end',
  },
});

const onPressUnit = (
  thingName,
  acName,
  isInverter,
  setDefaultParams,
  enableEnergyConsumption
) => {
  setDefaultParams();
  NavigationService.navigate(USAGE_GRAPH_SCREEN, {
    thingName,
    acName,
    isInverter,
    enableEnergyConsumption,
  });
};

const setColor = consumption => {
  if (consumption === 0) return styles.grey;
  return styles.blue;
};

const calculateLength = length => {
  // Prevent the bar from overflow
  const ratio = Math.min(length / 5000, 1);
  return {
    width: widthPercentage(35) * ratio,
    borderTopRightRadius: ratio === 1 ? 7.5 : 0,
    borderBottomRightRadius: ratio === 1 ? 7.5 : 0,
  };
};

const UsageList = ({ units, showBar, onPressRow }) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Usage</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  }, [navigation]);

  return units.map(unit => {
    const { unitName, usageValue, thingName, enableEnergyConsumption } = unit;
    return (
      <TouchableOpacity
        // eslint-disable-next-line react/no-array-index-key
        key={thingName}
        onPress={() =>
          onPressUnit(
            thingName,
            unitName,
            showBar,
            onPressRow,
            enableEnergyConsumption
          )
        }>
        <View style={styles.content}>
          <View style={styles.row}>
            <Image
              source={getACIconFromPath(unit.Logo)}
              style={styles.aircondIcon}
              resizeMode="contain"
            />
            <View>
              <Text
                medium
                style={StyleSheet.flatten([
                  showBar && enableEnergyConsumption
                    ? styles.withBarName
                    : styles.withoutBarName,
                ])}
                numberOfLines={1}>
                {unitName}
              </Text>
              {showBar && enableEnergyConsumption ? (
                <Text style={[styles.marginTop, setColor(usageValue)]}>
                  {`${
                    usageValue > 0 ? (usageValue / 1000).toFixed(2) : '--'
                  } kW`}
                </Text>
              ) : null}
            </View>
          </View>

          {showBar && enableEnergyConsumption ? (
            <View style={styles.barContainer}>
              <View style={[styles.numbers, styles.width]}>
                <Text mini>0</Text>
                <Text mini>5</Text>
              </View>
              <View>
                <View style={[styles.bar, styles.width]}>
                  <View
                    style={[styles.barColorFill, calculateLength(usageValue)]}
                  />
                </View>
              </View>
              <View style={styles.width}>
                <Text mini style={styles.kw}>
                  (kW)
                </Text>
              </View>
            </View>
          ) : null}
        </View>
      </TouchableOpacity>
    );
  });
};

UsageList.propTypes = {
  units: PropTypes.array,
  showBar: PropTypes.bool,
};

const handleInit = setPeriod => {
  const defaultPeriod = PERIOD_WEEKLY;
  setPeriod(defaultPeriod);
};

const Usage = ({ usageList, setSelectedPeriod }) => (
  <SafeAreaView style={styles.container}>
    <ScrollView>
      {usageList.map(({ title, data }, index) => {
        const isInverter = index === 0;
        // If the section does not have data, do not show
        if (data && data.length < 1) return null;

        return (
          <View key={title}>
            <View style={[styles.bgGrey, styles.header]}>
              <Text medium>{title}</Text>
            </View>
            <View style={[styles.bgGrey, styles.description]}>
              <Text>{isInverter ? 'Energy Consumption' : 'History Usage'}</Text>
              <Text style={[styles.grey, styles.marginTop]}>
                {isInverter
                  ? 'Tap a unit to view the energy consumption'
                  : 'Tap a unit to view the history usage'}
              </Text>
            </View>
            <UsageList
              units={data}
              showBar={isInverter}
              onPressRow={() => handleInit(setSelectedPeriod)}
            />
          </View>
        );
      })}
    </ScrollView>
  </SafeAreaView>
);
Usage.propTypes = {
  usageList: PropTypes.array,
  setSelectedPeriod: PropTypes.func,
};

Usage.defaultProps = {
  usageList: [],
};

const mapDispatchToProps = dispatch => ({
  setSelectedPeriod: periodType => {
    dispatch(usage.setSelectedPeriod(periodType));
  },
});

const mapStateToProps = state => ({
  usageList: selector.getUnitUsageList(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Usage);
