import React, { useRef, memo, useState } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ScrollView, Alert } from 'react-native';
import { connect, usage } from '@daikin-dama/redux-iot';
import { widthPercentage, heightPercentage } from '@module/utility';
import moment from 'moment';

import {
  PERIOD_DAILY,
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
} from '../../constants/timePeriod';
import ButtonContainer from './ButtonContainer';
import ToggleContainer from './ToggleContainer';
import GraphInfo from './GraphInfo';
import StackGraphContainer from './stackGraphContainer';
import PeriodDisplay from './periodDisplay';
import DateSelection from './dateSection';
import TabBar from './tabBar';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  innerContainer: {
    paddingHorizontal: widthPercentage(7.5),
  },
  scrollContainer: {
    paddingVertical: 10,
    flexGrow: 1,
  },
  buttonContainer: {
    paddingVertical: 5,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  tempText: {
    fontSize: 40,
  },
  marginTop: {
    marginTop: heightPercentage(1),
  },
  marginTop1: {
    marginTop: heightPercentage(1),
  },
  marginLeft: {
    marginLeft: 5,
  },
  flexRow: {
    flexDirection: 'row',
  },
  marginRight: {
    marginRight: 25,
  },
  hitSlop: {
    top: 20,
    right: 20,
    left: 20,
    bottom: 20,
  },
});

const periodTabItems = [
  { label: 'Day', value: PERIOD_DAILY },
  { label: 'Week', value: PERIOD_WEEKLY },
  { label: 'Month', value: PERIOD_MONTHLY },
  { label: 'Year', value: PERIOD_YEARLY },
];

function canRender(prevProps, nextProps) {
  return (
    prevProps.thingName === nextProps.thingName &&
    prevProps.periodType === nextProps.periodType &&
    prevProps.isInverter === nextProps.isInverter &&
    prevProps.setTemp === nextProps.setTemp &&
    prevProps.indoorTemp === nextProps.indoorTemp &&
    prevProps.indexData === nextProps.indexData &&
    prevProps.energyConsumption === nextProps.energyConsumption &&
    prevProps.onOffUnit === nextProps.onOffUnit
  );
}

const UsagePage = memo(
  ({
    thingName,
    periodType,
    isInverter,
    fetchCompareUsageData,
    setPeriod,
    clearCompareData,
    enableEnergyConsumption,
    setToggleButton,
  }) => {
    const dateSelectorRef = useRef();
    const mainPageRef = useRef();
    const [indexValue, setIndexValue] = useState(0);
    const [enableCompare, enableCompareSet] = useState(false);

    const supEnergyConsumption = enableEnergyConsumption && isInverter;

    const handleOnPressCompare = () => {
      const targetDate = dateSelectorRef.current.getValue();
      fetchCompareUsageData({
        thingName,
        periodType,
        targetDate,
      });
    };

    const checkDateCompare = () => {
      const targetDate = dateSelectorRef.current.getValue();
      const targetWeek = moment(targetDate)
        .startOf('isoWeek')
        .format('YYYY-MM-DD');
      const currentWeek = moment().startOf('isoWeek').format('YYYY-MM-DD');

      const targetMonth = moment(targetDate).format('YYYY-MM');
      const currentMonth = moment().format('YYYY-MM');

      const targetYear = moment(targetDate).format('YYYY');
      const currentYear = moment().format('YYYY');

      if (
        periodType === PERIOD_DAILY &&
        targetDate >= moment().format('YYYY-MM-DD')
      ) {
        Alert.alert('Error', 'Please select other dates for comparison');
      } else if (periodType === PERIOD_WEEKLY && targetWeek >= currentWeek) {
        Alert.alert('Error', 'Please select other dates for comparison');
      } else if (periodType === PERIOD_MONTHLY && targetMonth >= currentMonth) {
        Alert.alert('Error', 'Please select other dates for comparison');
      } else if (periodType === PERIOD_YEARLY && targetYear >= currentYear) {
        Alert.alert('Error', 'Please select other dates for comparison');
      } else {
        handleOnPressCompare();
        setToggleButton({
          thingName,
          periodType,
          type: 'setCompare',
          isEnergySup: supEnergyConsumption,
        });
      }
    };

    const getIndexValue = theData => {
      setIndexValue(Math.floor(theData));
    };

    const onPresTabBar = val => {
      setPeriod(val);
    };

    return (
      <ScrollView
        contentContainerStyle={styles.scrollContainer}
        bounces={false}
        ref={mainPageRef}
        scrollEventThrottle={16}>
        <View style={styles.innerContainer}>
          <TabBar
            items={periodTabItems}
            selectedItem={periodType}
            onSelect={onPresTabBar}
          />
        </View>
        <View style={styles.innerContainer}>
          <PeriodDisplay
            periodType={periodType}
            enableEnergyConsumption={enableEnergyConsumption}
          />
        </View>
        <StackGraphContainer
          periodType={periodType}
          indexData={getIndexValue}
          isInverter={isInverter}
          handleOnPressCompare={handleOnPressCompare}
          enableEnergyConsumption={enableEnergyConsumption}
        />

        <GraphInfo
          periodType={periodType}
          indexValue={indexValue}
          isInverter={isInverter}
          enableEnergyConsumption={enableEnergyConsumption}
          thingName={thingName}
        />
        <ToggleContainer
          isInverter={isInverter}
          enableEnergyConsumption={enableEnergyConsumption}
          setToggleButton={setToggleButton}
          periodType={periodType}
          thingName={thingName}
        />
        <DateSelection
          periodType={periodType}
          ref={dateSelectorRef}
          enableCompareSet={enableCompareSet}
        />
        <ButtonContainer
          enableCompare={enableCompare}
          onPressCompare={() => {
            checkDateCompare();
          }}
          onPressClear={() => clearCompareData({ thingName, periodType })}
        />
      </ScrollView>
    );
  },
  canRender
);

UsagePage.propTypes = {
  thingName: PropTypes.string,
  periodType: PropTypes.string,
  fetchCompareUsageData: PropTypes.func,
  isInverter: PropTypes.bool,
  enableEnergyConsumption: PropTypes.bool,
  clearCompareData: PropTypes.func,
  setToggleButton: PropTypes.func,
  setPeriod: PropTypes.func,
};

const mapStateToProps = (
  { usage: { usageToggle } },
  { thingName, periodType }
) => {
  const usageToggleData = usageToggle[thingName] || {};
  const usageTogglePeriod = usageToggleData[periodType] || {};
  return {
    setTemp: usageTogglePeriod.setTemp,
    indoorTemp: usageTogglePeriod.indoorTemp,
    energyConsumption: usageTogglePeriod.energyConsumption,
    onOffUnit: usageTogglePeriod.onOffUnit,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchCompareUsageData: ({ thingName, periodType, targetDate }) => {
    dispatch(
      usage.fetchUsageData({
        thingName,
        periodType,
        targetDate,
        isCompare: true,
      })
    );
  },
  setPeriod: ({ value }) => {
    dispatch(usage.setSelectedPeriod(value));
  },
  setToggleButton: value => {
    dispatch(usage.setToggleButton(value));
  },
  clearCompareData: value => {
    dispatch(usage.clearCompareData(value));
  },
});
export default connect(mapStateToProps, mapDispatchToProps)(UsagePage);
