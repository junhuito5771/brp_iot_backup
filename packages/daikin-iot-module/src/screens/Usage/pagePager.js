import React, { forwardRef, memo } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import ViewPager from '@react-native-community/viewpager';

const styles = StyleSheet.create({
  pageContent: {
    flexGrow: 1,
  },
});

function canRender(prevProps, nextProps) {
  return prevProps.initialPage === nextProps.initialPage;
}

const PagePager = memo(
  forwardRef(({ items, children, initialPage }, ref) => (
    <ViewPager
      style={styles.pageContent}
      ref={ref}
      initialPage={initialPage}
      scrollEnabled={false}>
      {items.map((item, index) => {
        const pageIndex = index;
        return <View key={pageIndex}>{children({ item })}</View>;
      })}
    </ViewPager>
  )),
  canRender
);

PagePager.propTypes = {
  items: PropTypes.array,
  children: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.func,
  ]),
  initialPage: PropTypes.number,
};

export default PagePager;
