import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';
import Modal from 'react-native-modal';
import PropTypes from 'prop-types';
import { connect, usage } from '@daikin-dama/redux-iot';
import {
  scale,
  widthPercentage,
  verticalScale,
  ScaleText,
  heightPercentage,
} from '@module/utility';
import { Colors, Text, Button } from '@module/daikin-ui';

const { Popover } = renderers;
const buttonStyle = StyleSheet.create({
  tempBtnContainer: {
    marginHorizontal: scale(8),
    marginBottom: verticalScale(13),
  },
  setTempBtn: {
    backgroundColor: Colors.sandybrown,
  },
  onOffBtn: {
    backgroundColor: Colors.mediumturquoise,
  },
  electricBtn: {
    backgroundColor: Colors.salmon,
  },
  energyBtn: {
    backgroundColor: Colors.skyBlue,
  },
  cpRTbtn: {
    backgroundColor: Colors.salmon,
  },
  indoorTempBtn: {
    backgroundColor: Colors.cornflowerblue,
  },
  passiveBtn: {
    backgroundColor: Colors.unitOffGrey,
  },
  constantButton: {
    width: scale(159),
    height: verticalScale(32),
    borderRadius: 7,
    alignItems: 'center',
    flexDirection: 'row',
    padding: scale(8),
    marginHorizontal: scale(3),
  },
  activeText: {
    color: Colors.white,
    marginLeft: widthPercentage(2),
    fontSize: ScaleText(12),
  },
  passiveText: {
    color: Colors.sectionBorderGrey,
    marginLeft: widthPercentage(2),
    fontSize: ScaleText(12),
  },
  flexRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

const modalStyles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  wrap: {
    height: heightPercentage(25),
    backgroundColor: Colors.white,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    padding: 30,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  title: {
    fontSize: ScaleText(14),
    fontWeight: 'bold',
  },
  buttonContainer: {
    paddingVertical: 5,
    width: widthPercentage(90),
  },
  buttonText: {
    color: Colors.white,
    fontSize: ScaleText(14),
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    width: 70,
    height: 50,
    fontSize: ScaleText(17),
    backgroundColor: 'transparent',
  },
});

function findOccurences(str, charToCount) {
  return str.split(charToCount).length - 1;
}

const ToggleContainer = ({
  setTemp,
  indoorTemp,
  energyConsumption,
  onOffUnit,
  electricRate,
  isInverter,
  enableEnergyConsumption,
  setToggleButton,
  periodType,
  thingName,
  cpRT,
  tnbRate,
  setTnbRate,
}) => {
  const [isVisible, setIsVisible] = useState(false);
  const [toParseRateValue, setToParseRateValue] = useState(0);
  const [rateValue, setRateValue] = useState('');

  useEffect(() => {
    let num = rateValue;
    const countDot = findOccurences(num, '.');

    if ((num.length >= 2 && !countDot) || countDot) {
      num = num.replace('.', '');
      if (!num) {
        num = '0';
      }
      const baseNumber = 10 ** (num.length - 1);
      num = (parseInt(num, 10) / baseNumber).toFixed(num.length - 1);
      setRateValue(num);
    }
  }, [toParseRateValue]);

  const handleOnConfirm = () => {
    setTnbRate(Number(rateValue));
    setIsVisible(false);
  };

  const optionStyles = {
    optionWrapper: {
      width: widthPercentage(43),
      alignItems: 'center',
      backgroundColor: Colors.lightSalmon,
    },
  };

  return (
    <>
      <Modal
        isVisible={isVisible}
        coverScreen
        hasBackdrop
        backdropColor={Colors.modalBackdrop}
        style={modalStyles.container}
        onBackdropPress={() => setIsVisible(false)}
        useNativeDriver={Platform.OS === 'android'}>
        <KeyboardAvoidingView
          style={modalStyles.content}
          behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
          <View style={modalStyles.wrap}>
            <Text style={modalStyles.title}>Electricity Bill Rate</Text>
            <View style={modalStyles.inputContainer}>
              <Text style={modalStyles.title}>RM </Text>
              <TextInput
                editable
                numeric
                keyboardType="decimal-pad"
                value={`${rateValue}`}
                placeholder={rateValue ? null : '0.001'}
                style={modalStyles.input}
                onChange={({ nativeEvent }) => {
                  setToParseRateValue(nativeEvent.eventCount);
                  setRateValue(nativeEvent.text);
                }}
                maxLength={5}
              />
            </View>

            <Button
              primary
              onPress={handleOnConfirm}
              style={modalStyles.buttonContainer}
              textStyle={modalStyles.buttonText}>
              Confirm
            </Button>
          </View>
        </KeyboardAvoidingView>
      </Modal>

      <View style={buttonStyle.tempBtnContainer}>
        <View style={buttonStyle.flexRow}>
          <TouchableOpacity
            onPress={() =>
              setToggleButton({ thingName, periodType, type: 'setTemp' })
            }
            style={
              setTemp
                ? [buttonStyle.setTempBtn, buttonStyle.constantButton]
                : [buttonStyle.passiveBtn, buttonStyle.constantButton]
            }>
            {setTemp ? (
              <Image
                source={require('../../shared/assets/graph/SetTemperature_ON.png')}
              />
            ) : (
              <Image
                source={require('../../shared/assets/graph/SetTemperature_OFF.png')}
              />
            )}
            <Text
              style={
                setTemp ? buttonStyle.activeText : buttonStyle.passiveText
              }>
              Set Temperature
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() =>
              setToggleButton({ thingName, periodType, type: 'indoorTemp' })
            }
            style={
              indoorTemp
                ? [buttonStyle.indoorTempBtn, buttonStyle.constantButton]
                : [buttonStyle.passiveBtn, buttonStyle.constantButton]
            }>
            {indoorTemp ? (
              <Image
                source={require('../../shared/assets/graph/IndoorTemperature_ON.png')}
              />
            ) : (
              <Image
                source={require('../../shared/assets/graph/IndoorTemperature_OFF.png')}
              />
            )}
            <Text
              small
              style={
                indoorTemp ? buttonStyle.activeText : buttonStyle.passiveText
              }>
              Indoor Temperature
            </Text>
          </TouchableOpacity>
        </View>

        <View style={[buttonStyle.flexRow, { marginTop: 10 }]}>
          {isInverter && enableEnergyConsumption ? (
            <TouchableOpacity
              onPress={() =>
                setToggleButton({
                  thingName,
                  periodType,
                  type: 'energyConsumption',
                })
              }
              style={
                energyConsumption
                  ? [buttonStyle.energyBtn, buttonStyle.constantButton]
                  : [buttonStyle.passiveBtn, buttonStyle.constantButton]
              }>
              {energyConsumption ? (
                <Image
                  source={require('../../shared/assets/graph/EnergyConsumption_ON.png')}
                />
              ) : (
                <Image
                  source={require('../../shared/assets/graph/EnergyConsumption_OFF.png')}
                />
              )}
              <Text
                style={
                  energyConsumption
                    ? buttonStyle.activeText
                    : buttonStyle.passiveText
                }>
                Energy Consumption
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() =>
                setToggleButton({ thingName, periodType, type: 'onOffUnit' })
              }
              style={
                onOffUnit
                  ? [buttonStyle.onOffBtn, buttonStyle.constantButton]
                  : [buttonStyle.passiveBtn, buttonStyle.constantButton]
              }>
              {onOffUnit ? (
                <Image
                  source={require('../../shared/assets/graph/UnitONOFF_ON.png')}
                />
              ) : (
                <Image
                  source={require('../../shared/assets/graph/UnitONOFF_OFF.png')}
                />
              )}

              <Text
                style={
                  onOffUnit ? buttonStyle.activeText : buttonStyle.passiveText
                }>
                Unit ON/OFF
              </Text>
            </TouchableOpacity>
          )}

          {isInverter && enableEnergyConsumption ? (
            <TouchableOpacity
              onPress={() =>
                setToggleButton({ thingName, periodType, type: 'electricRate' })
              }
              style={
                electricRate
                  ? [buttonStyle.electricBtn, buttonStyle.constantButton]
                  : [buttonStyle.passiveBtn, buttonStyle.constantButton]
              }>
              {electricRate ? (
                <Image
                  source={require('../../shared/assets/graph/electric_rate.png')}
                />
              ) : (
                <Image
                  source={require('../../shared/assets/graph/electric_rate_OFF.png')}
                />
              )}

              <Text
                style={
                  electricRate
                    ? buttonStyle.activeText
                    : buttonStyle.passiveText
                }>
                Electricity Bill
              </Text>
              <Menu
                renderer={Popover}
                rendererProps={{
                  preferredPlacement: 'bottom',
                  placement: 'top',
                  anchorStyle: {
                    backgroundColor: 'transparent',
                  },
                }}
                style={{
                  position: 'absolute',
                  right: 5,
                }}>
                <MenuTrigger
                  disabled={!electricRate}
                  customStyles={{
                    TriggerTouchableComponent: TouchableOpacity,
                    triggerWrapper: {
                      height: 30,
                      width: 35,
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                  }}>
                  <Image
                    source={require('../../shared/assets/general/dropdown.png')}
                  />
                </MenuTrigger>
                <MenuOptions
                  customStyles={{
                    optionsContainer: {
                      right: 14,
                      top: -5,
                    },
                    optionsWrapper: {
                      width: widthPercentage(43),
                      justifyContent: 'center',
                      alignItems: 'center',
                    },
                    optionWrapper: {
                      width: widthPercentage(43),
                      alignItems: 'center',
                    },
                  }}>
                  <MenuOption
                    customStyles={tnbRate === 0.218 ? optionStyles : null}
                    onSelect={() => setTnbRate(0.218)}
                    text="RM 0.218"
                  />
                  <MenuOption
                    customStyles={tnbRate === 0.334 ? optionStyles : null}
                    onSelect={() => setTnbRate(0.334)}
                    text="RM 0.334"
                  />
                  <MenuOption
                    customStyles={tnbRate === 0.516 ? optionStyles : null}
                    onSelect={() => setTnbRate(0.516)}
                    text="RM 0.516"
                  />
                  <MenuOption
                    customStyles={tnbRate === 0.546 ? optionStyles : null}
                    onSelect={() => setTnbRate(0.546)}
                    text="RM 0.546"
                  />
                  <MenuOption
                    customStyles={tnbRate === 0.571 ? optionStyles : null}
                    onSelect={() => setTnbRate(0.571)}
                    text="RM 0.571"
                  />
                  <MenuOption
                    customStyles={
                      tnbRate !== 0.218 &&
                      tnbRate !== 0.334 &&
                      tnbRate !== 0.516 &&
                      tnbRate !== 0.546 &&
                      tnbRate !== 0.571
                        ? optionStyles
                        : null
                    }
                    onSelect={() => setIsVisible(true)}
                    text="Others"
                  />
                </MenuOptions>
              </Menu>
            </TouchableOpacity>
          ) : null}

          {!isInverter && (
            <TouchableOpacity
              onPress={() =>
                setToggleButton({ thingName, periodType, type: 'cpRT' })
              }
              style={
                cpRT
                  ? [buttonStyle.cpRTbtn, buttonStyle.constantButton]
                  : [buttonStyle.passiveBtn, buttonStyle.constantButton]
              }>
              {cpRT ? (
                <Image source={require('../../shared/assets/graph/cpRT.png')} />
              ) : (
                <Image
                  source={require('../../shared/assets/graph/cpRT_OFF.png')}
                />
              )}
              <Text
                style={cpRT ? buttonStyle.activeText : buttonStyle.passiveText}>
                Compressor Time
              </Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </>
  );
};

ToggleContainer.propTypes = {
  setToggleButton: PropTypes.func,
  setTemp: PropTypes.bool,
  indoorTemp: PropTypes.bool,
  energyConsumption: PropTypes.bool,
  onOffUnit: PropTypes.bool,
  isInverter: PropTypes.bool,
  cpRT: PropTypes.bool,
  enableEnergyConsumption: PropTypes.bool,
  electricRate: PropTypes.bool,
  thingName: PropTypes.string,
  periodType: PropTypes.string,
  tnbRate: PropTypes.number,
  setTnbRate: PropTypes.func,
};

const mapStateToProps = (
  { usage: { usageToggle, curElectricRate } },
  { thingName, periodType }
) => {
  const usageToggleData = usageToggle[thingName] || {};
  const usageTogglePeriod = usageToggleData[periodType] || {};
  return {
    setTemp: usageTogglePeriod.setTemp,
    indoorTemp: usageTogglePeriod.indoorTemp,
    energyConsumption: usageTogglePeriod.energyConsumption,
    onOffUnit: usageTogglePeriod.onOffUnit,
    cpRT: usageTogglePeriod.cpRT,
    electricRate: usageTogglePeriod.electricRate,
    tnbRate: curElectricRate,
  };
};

const mapDispatchToProps = dispatch => ({
  setTnbRate: value => {
    dispatch(usage.setElectricRate(value));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ToggleContainer);
