import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { heightPercentage, verticalScale } from '@module/utility';
import { connect } from '@daikin-dama/redux-iot';
import { Text, Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  tabbar: {
    width: '100%',
    height: verticalScale(35),
    borderColor: Colors.cerulean,
    borderWidth: 1.5,
    borderRadius: heightPercentage(0.9),
    flexDirection: 'row',
    marginTop: verticalScale(15),
  },
  flex: {
    flex: 1,
  },
  tab: {
    flex: 1,
    padding: 0,
    margin: 0,
  },
  tabInner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 0,
    margin: 0,
  },
  borderLeft: {
    borderLeftWidth: 1.5,
    borderLeftColor: Colors.cerulean,
  },
  text: {
    textAlign: 'center',
  },
  bgBlue: {
    backgroundColor: Colors.cerulean,
  },
  white: {
    color: Colors.white,
  },
  firstItem: {
    marginLeft: 1,
  },
  lastItem: {
    marginRight: 1,
  },
});

const TabBar = ({ onSelect, items, selectedItem, isLoading }) => (
  <View style={styles.tabbar}>
    {items.map(({ label, value }, index) => {
      const isSelected = value === selectedItem;
      const hasBorderLeft = index !== 0;
      const isDisabled = isLoading;
      return (
        <View
          style={[
            styles.tab,
            isSelected && styles.bgBlue,
            hasBorderLeft && styles.borderLeft,
          ]}
          key={value}>
          <TouchableOpacity
            style={styles.flex}
            onPress={() => onSelect({ value, index })}
            disabled={isDisabled || isSelected}>
            <View style={styles.tabInner}>
              <Text style={[styles.text, isSelected && styles.white]}>
                {label}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    })}
  </View>
);

TabBar.defaultProps = {
  items: [],
  selectedItem: '',
};

TabBar.propTypes = {
  onSelect: PropTypes.func,
  items: PropTypes.array,
  selectedItem: PropTypes.string,
  isLoading: PropTypes.bool,
};

const mapStateToProps = ({ usage: { isLoading } }) => ({
  isLoading,
});

export default connect(mapStateToProps)(TabBar);
