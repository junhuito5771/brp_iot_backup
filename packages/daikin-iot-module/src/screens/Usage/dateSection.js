import React, {
  useState,
  forwardRef,
  useImperativeHandle,
  useRef,
  useEffect,
} from 'react';
import { View, StyleSheet, TouchableOpacity, Platform } from 'react-native';
import PropTypes from 'prop-types';
import { heightPercentage, widthPercentage, ScaleText } from '@module/utility';
import moment from 'moment';
import Modal from 'react-native-modal';
import DatePicker from 'react-native-date-picker';

import { Text, Colors } from '@module/daikin-ui';

import {
  PERIOD_DAILY,
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
} from '../../constants/timePeriod';

import CalendarPicker from './CalendarPicker';
import MonthYearPicker from './MonthYearPicker';

const STANDARD_DATE_FORMAT = 'DD MMMM YYYY';
const OUTPUT_DATE_FORMAT = 'YYYY-MM-DD';

const styles = StyleSheet.create({
  container: {
    height: heightPercentage(9),
    backgroundColor: Colors.azureRad,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dateSelection: {
    flexDirection: 'row',
    paddingVertical: 5,
    justifyContent: 'space-between',
  },
  label: {
    color: Colors.white,
    textAlign: 'center',
  },
  dateLabel: {
    marginHorizontal: 10,
  },
  disabledText: {
    opacity: 0.48,
  },
  hitSlopLeft: {
    top: 15,
    bottom: 15,
    left: 20,
    right: 5,
  },
  hitSlopRight: {
    top: 15,
    bottom: 15,
    left: 5,
    right: 20,
  },
  hitSlopMiddle: {
    top: 10,
    bottom: 10,
    left: 0,
    right: 0,
  },
  hitSlopLength: {
    width: 200,
  },
  noUsageLabel: {
    color: Colors.white,
  },
});

const modalStyles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrap: {
    height: heightPercentage(60),
    backgroundColor: Colors.white,
  },
  innerContainer: {
    paddingHorizontal: widthPercentage(4),
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: ScaleText(14),
    fontWeight: 'bold',
  },
  buttonText: {
    fontSize: ScaleText(14),
    fontWeight: 'bold',
    color: Colors.blue,
  },
  titleSection: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    borderBottomWidth: StyleSheet.hairlineWidth,
    paddingHorizontal: widthPercentage(4),
  },
});

const ModalContainer = ({
  height,
  renderContent,
  title,
  onClose,
  onConfirm,
  ...restProps
}) => (
  <Modal
    {...restProps}
    coverScreen
    hasBackdrop
    backdropColor={Colors.modalBackdrop}
    style={modalStyles.container}
    onBackdropPress={onClose}
    useNativeDriver={Platform.os === 'android'}>
    <View style={StyleSheet.flatten([modalStyles.wrap, height && { height }])}>
      <View style={modalStyles.titleSection}>
        <Text style={modalStyles.title}>{title}</Text>
        <TouchableOpacity onPress={onConfirm}>
          <Text style={modalStyles.buttonText}>Done</Text>
        </TouchableOpacity>
      </View>
      <View style={modalStyles.innerContainer}>
        <View style={modalStyles.content}>{renderContent(restProps)}</View>
      </View>
    </View>
  </Modal>
);

ModalContainer.propTypes = {
  title: PropTypes.string.isRequired,
  onClose: PropTypes.func,
  onConfirm: PropTypes.func,
  children: PropTypes.oneOf([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
  renderContent: PropTypes.func,
  height: PropTypes.number,
};

const datePickerStyles = StyleSheet.create({
  container: {
    height: heightPercentage(35),
  },
});

const DayPickerSelector = forwardRef(
  (
    { minimumDate, maximumDate, dateFormat, selectedDate, ...restProps },
    ref
  ) => {
    const [curDate, setCurDate] = useState(moment().toDate());

    useEffect(() => {
      if (selectedDate) {
        setCurDate(moment(selectedDate, dateFormat).toDate());
      }
    }, [selectedDate]);

    useImperativeHandle(
      ref,
      () => ({
        getValue: () => curDate,
      }),
      [curDate]
    );

    return (
      <DatePicker
        style={datePickerStyles.container}
        mode="date"
        date={curDate}
        onDateChange={setCurDate}
        minimumDate={moment(minimumDate, dateFormat).toDate()}
        maximumDate={moment(maximumDate, dateFormat)
          .subtract(1, 'days')
          .toDate()}
        {...restProps}
      />
    );
  }
);

DayPickerSelector.propTypes = {
  selectedDate: PropTypes.string,
  minimumDate: PropTypes.string,
  maximumDate: PropTypes.string,
  dateFormat: PropTypes.string,
};

DayPickerSelector.defaultProps = {
  dateFormat: STANDARD_DATE_FORMAT,
};

function getPickerByPeriod({ periodType, selectedValue, ...props }) {
  const minimumDate = moment('01 January 2020', STANDARD_DATE_FORMAT).format(
    STANDARD_DATE_FORMAT
  );
  const maximumDate = moment().startOf('day').format(STANDARD_DATE_FORMAT);

  const pickerProps = {
    minimumDate,
    maximumDate,
    ref: props.contentRef,
    selectedDate: selectedValue,
    formatDate: STANDARD_DATE_FORMAT,
    ...props,
  };
  switch (periodType) {
    case PERIOD_WEEKLY:
      return <CalendarPicker {...pickerProps} />;
    case PERIOD_MONTHLY:
      return <MonthYearPicker {...pickerProps} />;
    case PERIOD_YEARLY:
      return <MonthYearPicker {...pickerProps} showYearOnly />;
    default:
      return <DayPickerSelector locale="en-GB" {...pickerProps} />;
  }
}

getPickerByPeriod.propTypes = {
  periodType: PropTypes.string,
  contentRef: PropTypes.any,
  selectedValue: PropTypes.string,
};

function getDateFormat(periodType, date) {
  switch (periodType) {
    case PERIOD_WEEKLY: {
      const firstDayOfWeek = moment(date, STANDARD_DATE_FORMAT)
        .startOf('isoWeek')
        .format('DD MMM YYYY');
      const lastDayOfWeek = moment(date, STANDARD_DATE_FORMAT)
        .endOf('isoWeek')
        .format('DD MMM YYYY');

      return `${firstDayOfWeek} - ${lastDayOfWeek}`;
    }
    case PERIOD_MONTHLY:
      return moment(date, STANDARD_DATE_FORMAT).format('MMMM YYYY');
    case PERIOD_YEARLY:
      return moment(date, STANDARD_DATE_FORMAT).format('YYYY');
    default:
      return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT);
  }
}

function getDurationFormat(periodType) {
  switch (periodType) {
    case PERIOD_WEEKLY:
      return 'w';
    case PERIOD_MONTHLY:
      return 'M';
    case PERIOD_YEARLY:
      return 'y';
    default:
      return 'd';
  }
}

function getPeriod(periodType) {
  switch (periodType) {
    case PERIOD_WEEKLY:
      return 'week';
    case PERIOD_MONTHLY:
      return 'month';
    case PERIOD_YEARLY:
      return 'year';
    default:
      return 'date';
  }
}

function getModalHeightByPeriod(periodType) {
  switch (periodType) {
    case PERIOD_WEEKLY:
      return heightPercentage(60);
    default:
      return heightPercentage(50);
  }
}

function getMaxMaxDateByPeriod(periodType) {
  const momentMinDate = moment('01 January 2020', STANDARD_DATE_FORMAT);
  switch (periodType) {
    case PERIOD_YEARLY: {
      const minDate = momentMinDate.endOf('year');
      const maxDate = moment().startOf('day');

      return { minDate, maxDate };
    }
    case PERIOD_MONTHLY: {
      const minDate = momentMinDate.endOf('month');
      const maxDate = moment().startOf('day');

      return { minDate, maxDate };
    }
    default: {
      const minDate = momentMinDate;
      const maxDate = moment().startOf('day');

      return { minDate, maxDate };
    }
  }
}

function sameorBeforePeriod(periodType, date, maxDate) {
  const curMonth = moment(date).format('MMM');
  let maxMonth = moment(maxDate).format('MMM');
  const curYear = moment(date).format('YYYY');
  let maxYear = moment(maxDate).format('YYYY');

  switch (periodType) {
    case PERIOD_YEARLY: {
      maxYear = moment(maxYear).subtract(1, 'years').format('YYYY');
      const isSameYear = curYear === maxYear;
      return isSameYear || moment(curYear, 'YYYY').isBefore(maxYear);
    }
    case PERIOD_MONTHLY: {
      maxMonth = moment(maxDate).subtract(1, 'months').format('MMM');
      const isReachedMaxMonth = curMonth === maxMonth;
      return isReachedMaxMonth || moment(curMonth, 'MMM').isBefore(maxMonth);
    }
    case PERIOD_WEEKLY: {
      const maxDateChange = moment(maxDate)
        .subtract(1, 'days')
        .format(STANDARD_DATE_FORMAT);
      const isSameMonth = curMonth === maxMonth;
      const substractDay =
        parseInt(moment(maxDateChange).subtract(6, 'days').format('DD'), 10) -
        parseInt(moment(date).format('DD'), 10);
      return !isSameMonth
        ? !date.isBefore(maxDateChange)
        : !date.isBefore(maxDateChange) || substractDay < 7;
    }
    default: {
      const maxDateChange = moment(maxDate)
        .subtract(1, 'days')
        .format(STANDARD_DATE_FORMAT);
      return !date.isBefore(maxDateChange);
    }
  }
}

const DateSelection = forwardRef(({ periodType, enableCompareSet }, ref) => {
  const { minDate, maxDate } = getMaxMaxDateByPeriod(periodType);
  const contentRef = useRef();
  const [isVisible, setIsVisible] = useState(false);
  const [hideYearSelection, hideYearSelectionSet] = useState(false);
  const [selectedDate, setSelectedDate] = useState(
    moment().format(STANDARD_DATE_FORMAT)
  );

  let yearListOnlyThisYear = false;

  useEffect(() => {
    let subtractDate = null;
    switch (periodType) {
      case PERIOD_WEEKLY:
        subtractDate = moment(selectedDate, STANDARD_DATE_FORMAT).subtract(
          7,
          'days'
        );
        break;
      case PERIOD_MONTHLY:
        subtractDate = moment(selectedDate, STANDARD_DATE_FORMAT).subtract(
          1,
          'months'
        );
        break;
      case PERIOD_YEARLY:
        subtractDate = moment(selectedDate, STANDARD_DATE_FORMAT).subtract(
          1,
          'years'
        );
        break;
      default:
        subtractDate = moment(selectedDate, STANDARD_DATE_FORMAT).subtract(
          1,
          'days'
        );
        break;
    }
    setSelectedDate(
      moment(subtractDate, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT)
    );

    yearListOnlyThisYear =
      moment(maxDate).format('YYYY') === moment(minDate).format('YYYY');
    enableCompareSet(yearListOnlyThisYear);
    hideYearSelectionSet(yearListOnlyThisYear);
  }, [periodType]);

  const currentDateInMoment = moment(selectedDate, STANDARD_DATE_FORMAT);
  const incDisabled = sameorBeforePeriod(
    periodType,
    currentDateInMoment,
    maxDate
  );
  const decDisabled = !currentDateInMoment.isAfter(minDate);

  const currentPeriod = getPeriod(periodType);

  const handleOnClose = () => {
    setIsVisible(false);
  };

  const handleOnConfirm = () => {
    const currentDate = contentRef.current.getValue();
    setSelectedDate(
      moment(currentDate, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT)
    );
    handleOnClose();
  };

  const handleOnDecrement = () => {
    const currentDate = moment(selectedDate, STANDARD_DATE_FORMAT)
      .subtract(1, getDurationFormat(periodType))
      .format(STANDARD_DATE_FORMAT);

    setSelectedDate(currentDate);
  };

  const handOnIncrement = () => {
    const currentDate = moment(selectedDate, STANDARD_DATE_FORMAT)
      .add(1, getDurationFormat(periodType))
      .format(STANDARD_DATE_FORMAT);

    setSelectedDate(currentDate);
  };

  useImperativeHandle(
    ref,
    () => ({
      getValue: () =>
        moment(selectedDate, STANDARD_DATE_FORMAT).format(OUTPUT_DATE_FORMAT),
    }),
    [selectedDate]
  );

  return (
    <>
      <ModalContainer
        title={`Select ${currentPeriod}`}
        isVisible={isVisible}
        onClose={handleOnClose}
        onConfirm={handleOnConfirm}
        renderContent={getPickerByPeriod}
        contentRef={contentRef}
        periodType={periodType}
        selectedValue={selectedDate}
        height={getModalHeightByPeriod(periodType)}
      />
      <View style={styles.container}>
        {hideYearSelection && periodType === PERIOD_YEARLY ? (
          <Text style={styles.noUsageLabel}>
            You have no usage in the past year
          </Text>
        ) : (
          <View>
            <Text bold style={styles.label}>
              {`Select ${currentPeriod} to compare with`}
            </Text>
            <View style={styles.dateSelection}>
              <TouchableOpacity
                disabled={decDisabled}
                onPress={handleOnDecrement}
                hitSlop={styles.hitSlopLeft}>
                <Text
                  bold
                  style={[styles.label, decDisabled && styles.disabledText]}>
                  {'<'}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setIsVisible(true)}
                style={styles.hitSlopLength}
                hitSlop={styles.hitSlopMiddle}>
                <Text style={[styles.label, styles.dateLabel]}>
                  {getDateFormat(periodType, selectedDate)}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={incDisabled}
                onPress={handOnIncrement}
                hitSlop={styles.hitSlopRight}>
                <Text
                  bold
                  style={[styles.label, incDisabled && styles.disabledText]}>
                  {'>'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    </>
  );
});

DateSelection.propTypes = {
  periodType: PropTypes.oneOf([
    PERIOD_DAILY,
    PERIOD_WEEKLY,
    PERIOD_MONTHLY,
    PERIOD_YEARLY,
  ]),
  enableCompareSet: PropTypes.func,
};

DateSelection.defaultProps = {
  periodType: PERIOD_DAILY,
};

export default DateSelection;
