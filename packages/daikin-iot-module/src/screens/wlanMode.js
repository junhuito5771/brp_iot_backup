import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform,
  Alert,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import { connect, wlanMode } from '@daikin-dama/redux-iot';
import { ConnectionType, NavigationService } from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  CollapsibleBox,
  Loading,
  LastUpdateClock,
  HeaderText,
  getACIconFromPath,
  HeaderBackButton,
} from '@module/daikin-ui';

import { OTHER_MORE_REMOTE_SCREEN } from '../constants/routeNames';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  marginRight: {
    marginRight: 25,
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
});

const showAlert = () => {
  Alert.alert(
    'Not authorized',
    'You do not have permission to access this unit. Please log in to the account paired with this unit first.',
    [
      {
        text: 'OK',
      },
    ]
  );
};

const renderNavigationOptions = (route, navigation) => {
  const { params } = route;

  if (params && params.refreshButtonParams) {
    const { refreshButtonParams } = params;
    return {
      title: <HeaderText>Available Units</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
      headerRight: () => (
        <View style={[styles.row, styles.marginRight]}>
          <TouchableOpacity hitSlop={styles.hitSlop} {...refreshButtonParams}>
            <Image source={require('../shared/assets/general/refresh.png')} />
          </TouchableOpacity>
        </View>
      ),
    };
  }

  return {};
};

const WlanUnits = ({
  units,
  isLoading,
  discoverWLANUnits,
  pairWLANUnit,
  isPairLoading,
  pairSuccess,
  pairError,
  connectedUnitName,
  lastUpdated,
  clearStatus,
  route,
}) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      ...renderNavigationOptions(route, navigation),
    });
  }, [navigation]);

  React.useEffect(() => {
    navigation.setParams({
      refreshButtonParams: {
        onPress: discoverWLANUnits,
      },
    });

    // return () => {
    //   navigation.setParams({});
    // };
  }, []);

  React.useEffect(() => {
    if (pairSuccess) {
      NavigationService.navigate(OTHER_MORE_REMOTE_SCREEN, {
        unit: { ACName: connectedUnitName },
        mode: ConnectionType.BLE_MDOE,
      });
    } else if (pairError) {
      Alert.alert('Error', pairError);
    }

    clearStatus();
  }, [pairSuccess, pairError]);

  const handleOnPressItem = unit => {
    if (unit.disallowed) {
      showAlert();
    } else {
      pairWLANUnit(unit);
    }
  };

  return (
    <SafeAreaView style={styles.flex}>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={discoverWLANUnits}
          />
        }>
        <LastUpdateClock time={lastUpdated} />
        <CollapsibleBox
          data={units}
          onPressItem={handleOnPressItem}
          getIconPath={getACIconFromPath}
        />
      </ScrollView>
      <Loading visible={isPairLoading}>Connecting to unit</Loading>
    </SafeAreaView>
  );
};

WlanUnits.propTypes = {
  units: PropTypes.array,
  isLoading: PropTypes.bool,
  discoverWLANUnits: PropTypes.func,
  pairWLANUnit: PropTypes.func,
  isPairLoading: PropTypes.bool,
  pairSuccess: PropTypes.string,
  pairError: PropTypes.string,
  connectedUnitName: PropTypes.string,
  lastUpdated: PropTypes.number,
  clearStatus: PropTypes.func,
  route: PropTypes.object,
};

const mapDispatchToProps = dispatch => ({
  discoverWLANUnits: () => {
    dispatch(
      wlanMode.discoverUnits({
        setBroadcast: Platform.select({
          ios: true,
          android: false,
        }),
      })
    );
  },
  pairWLANUnit: unitProps => dispatch(wlanMode.pairUnit(unitProps)),
  clearStatus: () => dispatch(wlanMode.clearStatus()),
});

const mapStateToProps = state => ({
  units: state.wlanMode.units,
  isLoading: state.wlanMode.isLoading,
  lastUpdated: state.wlanMode.lastUpdated,
  isPairLoading: state.wlanMode.isPairLoading,
  pairSuccess: state.wlanMode.pairSuccess,
  pairError: state.wlanMode.pairError,
  connectedUnitName: state.units.allUnits[state.remote.thingName]
    ? state.units.allUnits[state.remote.thingName].ACName
    : '',
});

export default connect(mapStateToProps, mapDispatchToProps)(WlanUnits);
