import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image } from 'react-native';
import PropTypes from 'prop-types';
import { connect, settings } from '@daikin-dama/redux-iot';
import moment from 'moment';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { Colors, HeaderBackButton, Text, HeaderText } from '@module/daikin-ui';
import { NavigationService } from '@module/utility';
import { APP_SETTINGS_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
  },
  row: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
  icon: {
    height: 17,
    width: 17,
    resizeMode: 'contain',
  },
});

const validityPeriod = [
  { label: '5 minutes', value: moment.duration(5, 'minutes').asMilliseconds() },
  {
    label: '30 minutes',
    value: moment.duration(30, 'minutes').asMilliseconds(),
  },
  { label: '1 Hour', value: moment.duration(1, 'hours').asMilliseconds() },
  { label: '6 Hours', value: moment.duration(6, 'hours').asMilliseconds() },
  { label: '1 Day', value: moment.duration(1, 'days').asMilliseconds() },
  { label: '1 Month', value: moment.duration(1, 'months').asMilliseconds() },
  { label: '1 Year', value: moment.duration(1, 'years').asMilliseconds() },
];

const onSetValidityPeriod = (period, setValidityPeriod) => {
  setValidityPeriod(period);
  NavigationService.navigate(APP_SETTINGS_SCREEN);
};

const QRCodeValidity = ({ setValidityPeriod, qrCodeValidity }) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>App Settings</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  });

  return (
    <View style={styles.center}>
      {validityPeriod.map(period => (
        <TouchableOpacity
          onPress={() => onSetValidityPeriod(period, setValidityPeriod)}
          style={styles.row}
          key={period.label}>
          <Text small>{period.label}</Text>
          {qrCodeValidity === period.label && (
            <Image
              source={require('../../shared/assets/general/submitTick.png')}
              style={styles.icon}
            />
          )}
        </TouchableOpacity>
      ))}
    </View>
  );
};

const mapStateToProps = state => ({
  qrCodeValidity: state.settings.qrValidityPeriod.label,
});

const mapDispatchToProps = dispatch => ({
  setValidityPeriod: period => dispatch(settings.setValidityPeriod(period)),
});

QRCodeValidity.propTypes = {
  setValidityPeriod: PropTypes.func,
  qrCodeValidity: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToProps)(QRCodeValidity);
