import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from 'react-native';
import { connect } from '@daikin-dama/redux-iot';
import { StoreHelper, NavigationService } from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { Text, Colors, HeaderText, NavDrawerMenu } from '@module/daikin-ui';

import {
  AGREEMENT_SCREEN,
  QRCODE_VALIDITY_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  center: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.white,
  },
  row: {
    width: '100%',
  },
  flexRow: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
});

const row = (title, desc, onPress) => {
  const Touchable = title.includes('Version')
    ? TouchableWithoutFeedback
    : TouchableOpacity;
  return (
    <Touchable key={title} onPress={onPress} style={styles.row}>
      <View style={styles.flexRow}>
        <Text small>{title}</Text>
        {desc && <Text small>{desc}</Text>}
      </View>
    </Touchable>
  );
};

const AppSettings = ({ qrCodeValidity }) => {
  const navigation = useNavigation();
  const [version, setVersion] = React.useState(0);
  const [buildNumber, setBuildNumber] = React.useState(0);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>App Settings</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
    });
  });

  React.useEffect(() => {
    setVersion(StoreHelper.getCurrentVersion());
    setBuildNumber(StoreHelper.getCurrentBuildNumber());
  }, []);

  const settings = [
    {
      title: 'QR Code Validity',
      onPress: () => NavigationService.navigate(QRCODE_VALIDITY_SCREEN),
      desc: qrCodeValidity.label,
    },
    {
      title: 'Terms of Use',
      onPress: () => NavigationService.navigate(AGREEMENT_SCREEN),
    },
    // Hide this screen at the moment untill further notice from daikin team
    // {
    //   title: 'Open Source License',
    //   onPress: () => NavigationService.navigate(AGREEMENT_SCREEN),
    // },
    {
      title: 'Application Version',
      desc: `${version} (${buildNumber})`,
    },
  ];

  return (
    <View style={styles.center}>
      {settings.map(setting =>
        row(setting.title, setting.desc, setting.onPress)
      )}
    </View>
  );
};

const mapStateToProps = state => ({
  qrCodeValidity: state.settings.qrValidityPeriod,
});

AppSettings.propTypes = {
  qrCodeValidity: PropTypes.object,
};

export default connect(mapStateToProps)(AppSettings);
