import React from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Linking } from 'react-native';
import WebView from 'react-native-webview';
import { connect } from '@module/redux-auth';
import { privacyPolicy } from '@module/redux-marketing';
import { Colors, HeaderBackButton, Text, HeaderText } from '@module/daikin-ui';
import { NavigationService } from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  error: {
    paddingTop: 30,
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

const ErrorMessage = () => (
  <View style={styles.error}>
    <Text>An error has occurred, please try again later</Text>
  </View>
);

const UserAgreement = ({ init, content }) => {
  const webviewRef = React.useRef(null);
  const navigation = useNavigation();

  useFocusEffect(
    React.useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Terms Of Use</HeaderText>,
        headerLeft: () => <HeaderBackButton navigation={navigation} />,
      });
    }, [])
  );

  useFocusEffect(
    React.useCallback(() => {
      if (init) init();
    }, [])
  );

  return (
    <View style={styles.container}>
      {content && (
        <WebView
          ref={webviewRef}
          source={{ uri: content ? content.hyperlink : null }}
          renderError={() => <ErrorMessage />}
          onShouldStartLoadWithRequest={event => {
            if (event.url !== content.hyperlink) {
              webviewRef.current.stopLoading();
              Linking.openURL(event.url);
              return false;
            }
            return true;
          }}
        />
      )}
    </View>
  );
};

UserAgreement.propTypes = {
  init: PropTypes.func,
  content: PropTypes.object,
};

UserAgreement.defaultProps = {
  init: () => {},
  content: {},
};

const mapStateToProps = ({ privacyPolicy: privacyPolicyState }) => ({
  content: privacyPolicyState.content,
});

const mapDispatchToProps = dispatch => ({
  init: () => {
    dispatch(privacyPolicy.init());
  },
});

UserAgreement.propTypes = {};

export default connect(mapStateToProps, mapDispatchToProps)(UserAgreement);
