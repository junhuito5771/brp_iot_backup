import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View, StyleSheet, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { heightPercentage, NavigationService } from '@module/utility';
import { connect, remote } from '@daikin-dama/redux-iot';

import { Text, Colors, HeaderText, HeaderBackButton } from '@module/daikin-ui';

import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { getErrorCodeDetail } from '../../constants/errorCodes';

const styles = StyleSheet.create({
  rowContainer: {
    backgroundColor: Colors.white,
    width: '100%',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    padding: heightPercentage(2),
    height: 'auto',
  },
  leftColumn: {
    flex: 0.3,
  },
  rightColumn: {
    flex: 0.7,
  },
  textMargin: {
    marginVertical: 4,
  },
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  greyText: {
    color: Colors.lightGrey,
  },
});

const RowContent = ({ leftText, rightText }) => (
  <View style={{ flexDirection: 'row' }}>
    <View style={styles.leftColumn}>
      <Text style={styles.textMargin} small>
        {leftText}
      </Text>
    </View>
    <View style={styles.rightColumn}>
      <Text style={styles.textMargin} small>
        {rightText}
      </Text>
    </View>
  </View>
);

const renderNavigationOptions = navigation => ({
  title: <HeaderText>Error History</HeaderText>,
  headerLeft: () => <HeaderBackButton navigation={navigation} />,
});

const ErrorHistory = ({
  onPageLoad,
  errorHistory,
  route,
  onClearHistory,
  isLoading,
}) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(navigation)
    );
  }, [navigation]);

  React.useEffect(() => {
    const { thingName } = route.params;
    onPageLoad(thingName);
    return () => onClearHistory();
  }, []);

  if (isLoading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="small" color={Colors.grey} />
      </View>
    );
  }
  return (
    <View style={styles.flex}>
      {errorHistory.length > 0 ? (
        <ScrollView>
          {errorHistory.map((obj, index) => {
            const errorData = getErrorCodeDetail(obj.code);

            if (!errorData) return null;

            const { label, description } = errorData;

            const key = `${obj.updatedOn}_${label}_${index}`;

            return (
              // eslint-disable-next-line react/no-array-index-key
              <View key={key} style={styles.rowContainer}>
                <RowContent
                  leftText={moment(obj.updatedOn).format('DD/MM/YYYY')}
                  rightText={`Error ${label}`}
                />
                <RowContent
                  leftText={moment(obj.updatedOn).format('h:mm A')}
                  rightText={description}
                />
              </View>
            );
          })}
        </ScrollView>
      ) : (
        <View style={styles.container}>
          <Text small style={styles.greyText}>
            No error history available
          </Text>
        </View>
      )}
    </View>
  );
};

RowContent.propTypes = {
  leftText: PropTypes.string,
  rightText: PropTypes.string,
};

ErrorHistory.propTypes = {
  onPageLoad: PropTypes.func,
  onClearHistory: PropTypes.func,
  errorHistory: PropTypes.array,
  route: PropTypes.object,
  isLoading: PropTypes.bool,
};

const mapStateToProps = ({ remote: { errorHistory, isLoading } }) => ({
  errorHistory,
  isLoading,
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: value => dispatch(remote.fetchErrorHistory(value)),
  onClearHistory: () => dispatch(remote.clearErrorHistory()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorHistory);
