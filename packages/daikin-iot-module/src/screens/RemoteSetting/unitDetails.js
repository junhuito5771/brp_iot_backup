import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  SafeAreaView,
  View,
  StyleSheet,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Alert,
  Platform,
  BackHandler,
} from 'react-native';
import { connect, updateFirmware } from '@daikin-dama/redux-iot';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  Text,
  Loading,
  HeaderBackButton,
  Colors,
  HeaderText,
} from '@module/daikin-ui';

import { NavigationService } from '@module/utility';
import {
  UNIT_USERS_SCREEN,
  ERROR_HISTORY_SCREEN,
  WIFI_CONFIG_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
  },
  row: {
    width: '100%',
  },
  flexRow: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
});

const renderNavigationOptions = (route, navigation) => {
  const { params: { unit, updateFirmwareLoading } = {} } = route;
  const { ACName } = unit;

  return {
    title: (
      <HeaderText>{`${ACName.slice(0, 20)}${
        ACName.length > 20 ? '...' : ''
      }`}</HeaderText>
    ),
    headerLeft: () => (
      <HeaderBackButton
        navigation={navigation}
        disabled={updateFirmwareLoading}
      />
    ),
    gestureEnabled: !updateFirmwareLoading,
  };
};

const UnitDetails = ({
  unit,
  route,
  submitFirmwareUpgrade,
  updateFirmwareLoading,
  updateFirmwareLoadingText,
  updateFirmwareSuccess,
  updateFirmwareError,
  clearFirmwareUpgradeStatus,
  cancelFirmwareUpgradeTask,
}) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  useEffect(() => {
    navigation.setParams({
      unit,
    });
  }, [unit]);

  useEffect(() => {
    let backListener;
    if (updateFirmwareLoading) {
      backListener = BackHandler.addEventListener('hardwareBackPress', () => {
        Alert.alert(
          'Firmware update in progress',
          'Please wait for the update to be completed',
          [
            {
              text: 'OK',
            },
          ]
        );

        return true;
      });
    }

    return () => {
      if (backListener) backListener.remove();
      BackHandler.removeEventListener('hardwareBackPress', () => {});
    };
  }, [updateFirmwareLoading]);

  useEffect(() => {
    navigation.setParams({
      updateFirmwareLoading,
    });
  }, [updateFirmwareLoading]);

  const row = (title, desc, onPress) => {
    const Touchable = desc ? TouchableWithoutFeedback : TouchableOpacity;
    return (
      <Touchable key={title} style={styles.row} onPress={onPress}>
        <View style={styles.flexRow}>
          <Text small>{title}</Text>
          {desc && <Text small>{desc}</Text>}
        </View>
      </Touchable>
    );
  };

  useEffect(() => {
    if (updateFirmwareSuccess) {
      Alert.alert('Firmware update successful', updateFirmwareSuccess);
    } else if (updateFirmwareError) {
      if (updateFirmwareError.includes('verify firmware version')) {
        Alert.alert('Firmware update cannot be verified', updateFirmwareError);
      } else if (updateFirmwareError.includes('already the latest version')) {
        Alert.alert('Firmware update not necessary', updateFirmwareError);
      } else {
        Alert.alert('Firmware update failure', updateFirmwareError);
      }
    }

    clearFirmwareUpgradeStatus();
  }, [updateFirmwareSuccess, updateFirmwareError]);

  const details = [
    {
      title: 'User Management',
      onPress: () => NavigationService.navigate(UNIT_USERS_SCREEN),
    },
    {
      title: 'Change Wi-Fi Password',
      onPress: () => {
        NavigationService.navigate(WIFI_CONFIG_SCREEN, {
          thingName: unit.thingName,
          thingType: unit.ThingType,
          isReconnect: true,
        });
      },
    },
    {
      title: 'Firmware Version',
      desc: unit.firmwareVersion,
    },
    {
      title: 'Update Firmware Version',
      onPress: () => submitFirmwareUpgrade(unit.thingName),
    },
    {
      title: 'Error History',
      onPress: () =>
        NavigationService.navigate(ERROR_HISTORY_SCREEN, {
          thingName: unit.thingName,
        }),
    },
  ];

  const handleFirmwareCancel = () => {
    Alert.alert(
      'Cancel firmware upgrade',
      'Are you sure you want to cancel the firmware upgrade?',
      [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          onPress: cancelFirmwareUpgradeTask,
        },
      ]
    );
  };

  return (
    <SafeAreaView style={styles.flex}>
      <Loading visible={updateFirmwareLoading} onCancel={handleFirmwareCancel}>
        {updateFirmwareLoadingText}
      </Loading>
      <View style={styles.center}>
        {details.map(detail => row(detail.title, detail.desc, detail.onPress))}
      </View>
    </SafeAreaView>
  );
};

UnitDetails.propTypes = {
  unit: PropTypes.object,
  submitFirmwareUpgrade: PropTypes.func,
  updateFirmwareLoading: PropTypes.bool,
  updateFirmwareLoadingText: PropTypes.string,
  updateFirmwareSuccess: PropTypes.string,
  updateFirmwareError: PropTypes.string,
  clearFirmwareUpgradeStatus: PropTypes.func,
  cancelFirmwareUpgradeTask: PropTypes.func,
  route: PropTypes.object,
};

UnitDetails.defaultProps = {
  unit: {},
};

const mapStateToProps = ({
  units: { allUnits },
  remote: { thingName },
  updateFirmware: {
    isLoading: updateFirmwareLoading,
    loadingText: updateFirmwareLoadingText,
    success: updateFirmwareSuccess,
    error: updateFirmwareError,
  },
}) => ({
  unit: allUnits[thingName],
  updateFirmwareLoading,
  updateFirmwareLoadingText,
  updateFirmwareSuccess,
  updateFirmwareError,
});

const mapDisptachToProps = dispatch => ({
  submitFirmwareUpgrade: thingName =>
    dispatch(
      updateFirmware.submitFirmwareUpgrade({
        ios: Platform.select({ ios: true, android: false }),
        thingName,
      })
    ),
  clearFirmwareUpgradeStatus: () =>
    dispatch(updateFirmware.clearUpdateStatus()),
  cancelFirmwareUpgradeTask: () =>
    dispatch(updateFirmware.cancelFirmwareUpgrade()),
});

export default connect(mapStateToProps, mapDisptachToProps)(UnitDetails);
