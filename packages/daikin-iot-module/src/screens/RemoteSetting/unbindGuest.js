import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  SectionList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, unbindDevice } from '@daikin-dama/redux-iot';
import { heightPercentage, iphoneXS, NavigationService } from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  Checkbox,
  HeaderText,
  TabBar,
  Loading,
  Colors,
  HeaderBackButton,
} from '@module/daikin-ui';

import { UNIT_USERS_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  sectionHeader: {
    backgroundColor: Colors.userListSectionGrey,
    height: heightPercentage(4),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  sectionContent: {
    height: iphoneXS ? heightPercentage(8) : heightPercentage(9),
  },
  sectionContentBorder: {
    borderBottomColor: Colors.sectionBorderGrey,
    borderBottomWidth: 0.5,
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  full: {
    width: '100%',
    height: '100%',
  },
  desc: {
    flex: 1,
    justifyContent: 'space-around',
    paddingHorizontal: 10,
  },
  padding: {
    paddingHorizontal: 20,
  },
  inner: {
    justifyContent: 'center',
    flex: 1,
    flexDirection: 'row',
    marginVertical: 5,
  },
  textGray: {
    color: Colors.sectionContentGrey,
  },
  sectionTitle: {
    color: Colors.sectionHeaderTitleGrey,
  },
  bottomContainer: {
    height: heightPercentage(9),
  },
});

const unbindGuest = ({
  unitUserList,
  removeGuests,
  thingName,
  guestSuccess,
  guestError,
  isGuestLoading,
  clearRemoveGuestStatus,
}) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Remove Guest</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const [checkList, setCheckList] = useState(
    Array(unitUserList.length).fill(false)
  );

  useEffect(() => {
    setCheckList(Array(unitUserList.length).fill(false));
  }, [unitUserList]);

  useEffect(() => {
    if (guestSuccess) {
      NavigationService.navigate(UNIT_USERS_SCREEN);
    } else if (guestError) {
      Alert.alert('Error', guestError);
    }

    clearRemoveGuestStatus();
  }, [guestSuccess, guestError]);

  const toggleCheckbox = index => {
    setCheckList(prevCheckList => {
      const newCheckList = [...prevCheckList];
      newCheckList[index] = !prevCheckList[index];
      return newCheckList;
    });
  };

  return (
    <SafeAreaView style={styles.flex}>
      <Loading visible={isGuestLoading}>Unbinding the guests...</Loading>
      <ScrollView contentContainerStyle={styles.flex}>
        <SectionList
          sections={unitUserList}
          keyExtractor={({ email }) => email}
          renderItem={({ item, index }) => (
            <View
              style={[
                styles.sectionContent,
                styles.row,
                styles.sectionContentBorder,
              ]}>
              <TouchableOpacity
                style={styles.full}
                onPress={() => toggleCheckbox(index)}>
                <View style={[styles.inner, styles.padding]}>
                  <Checkbox value={checkList[index]} disabled />
                  <View style={styles.desc}>
                    <Text>{item.name}</Text>
                    <Text style={styles.textGray}>{item.email}</Text>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          )}
          renderSectionHeader={({ section: { title } }) => (
            <View style={[styles.sectionHeader, styles.row, styles.padding]}>
              <Text style={styles.sectionTitle}>{title}</Text>
            </View>
          )}
        />
      </ScrollView>
      <TabBar style={styles.bottomContainer}>
        <TabBar.Item
          key="Unbind guest"
          label="Unbind"
          imgSource={require('../../shared/assets/unitUser/unbind.png')}
          onPress={() => removeGuests(checkList, thingName)}
        />
      </TabBar>
    </SafeAreaView>
  );
};

unbindGuest.propTypes = {
  unitUserList: PropTypes.array,
  removeGuests: PropTypes.func,
  thingName: PropTypes.string,
  guestSuccess: PropTypes.string,
  guestError: PropTypes.string,
  isGuestLoading: PropTypes.bool,
  clearRemoveGuestStatus: PropTypes.func,
};

unbindGuest.defaultProps = {
  unitUserList: [],
};

const mapStateToProps = ({
  unitUsers: { unitUserList },
  remote: { thingName },
  unbindDevice: { guestSuccess, guestError, isGuestLoading },
}) => ({
  unitUserList: unitUserList.slice(1, unitUserList.length),
  thingName,
  guestSuccess,
  guestError,
  isGuestLoading,
});

const mapDispatchToProps = dispatch => ({
  removeGuests: (checklist, thingName) =>
    dispatch(unbindDevice.submitRemoveGuests({ checklist, thingName })),
  clearRemoveGuestStatus: () =>
    dispatch(unbindDevice.clearUnbindDeviceStatus()),
});

export default connect(mapStateToProps, mapDispatchToProps)(unbindGuest);
