import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ActivityIndicator, Alert } from 'react-native';
import { connect, unitUserControl } from '@daikin-dama/redux-iot';
import { heightPercentage, NavigationService } from '@module/utility';
import moment from 'moment';

import {
  Colors,
  Text,
  Switch,
  HeaderBackButton,
  HeaderText,
} from '@module/daikin-ui';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { LIMIT_USAGE_SCREEN } from '../../constants/routeNames';

const EXPIRY_TIME_FORMAT = 'DD MMMM YYYY, hh:mm A';
const EXPIRY_TIME_MOMENT_FORMAT = 'YYYY-MM-DD HH:mm:ss';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  column: {
    flexDirection: 'column',
  },
  padding: {
    paddingHorizontal: 20,
  },
  sectionHeader: {
    backgroundColor: Colors.offWhite,
    height: heightPercentage(4),
    shadowColor: Colors.darkBlack,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  sectionContent: {
    height: heightPercentage(7),
  },
  sectionContentBorder: {
    borderBottomColor: Colors.lightCoolGrey,
    borderBottomWidth: 0.5,
  },
  sectionTitle: {
    color: Colors.sectionHeaderTitleGrey,
  },
  inner: {
    flex: 1,
    justifyContent: 'space-around',
    marginVertical: 5,
  },
  innerRow: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  marginRight: {
    marginRight: 3,
  },
  textGrey: {
    color: Colors.sectionContentGrey,
  },
});

const renderNavigationOptions = (route, navigation) => ({
  title: <HeaderText>{route.params.user.name}</HeaderText>,
  headerLeft: () => <HeaderBackButton navigation={navigation} />,
});

const UnitUser = ({
  setUserControl,
  route,
  setUnitUser,
  unitUser,
  shareUnitLoading,
  usageLimitLoading,
  overwriteScheduleLoading,
  error,
  canControlLimit,
}) => {
  const navigation = useNavigation();
  const { user } = route.params;

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  useEffect(() => {
    setUnitUser(user);
  }, [user]);

  useEffect(() => {
    if (error !== '') {
      Alert.alert('Something went wrong', 'Please try again later', [
        {
          text: 'OK',
        },
      ]);
    }
  }, [error]);

  const setControl = controlType => {
    if (controlType === 'limitUsage') {
      if (unitUser.expiryTime !== '-') {
        setUserControl({ expiryTime: '-' });
      } else {
        NavigationService.navigate(LIMIT_USAGE_SCREEN, {
          dateData: unitUser.expiryTime,
        });
      }
    } else {
      const switchType = unitUser[controlType] ? 0 : 1;
      setUserControl({ [controlType]: switchType });
    }
  };

  const displayExpiryTime = date => {
    const momentDate = moment(date, EXPIRY_TIME_MOMENT_FORMAT);
    return `${moment(momentDate).format(EXPIRY_TIME_FORMAT)}`;
  };

  return (
    <View>
      <View style={[styles.sectionHeader, styles.row, styles.padding]}>
        <Text style={styles.sectionTitle}>Usability control</Text>
      </View>
      <View
        style={[
          styles.sectionContent,
          styles.row,
          styles.sectionContentBorder,
        ]}>
        <View style={[styles.inner, styles.innerRow, styles.padding]}>
          <Text>Share unit</Text>
          <View style={styles.row}>
            {shareUnitLoading && (
              <ActivityIndicator
                size="small"
                color={Colors.grey}
                style={styles.marginRight}
              />
            )}
            <Switch
              onPress={() => setControl('shareUnit')}
              value={unitUser.shareUnit !== 0}
            />
          </View>
        </View>
      </View>
      <View
        style={[
          styles.sectionContent,
          styles.row,
          styles.sectionContentBorder,
        ]}>
        <View style={[styles.inner, styles.innerRow, styles.padding]}>
          <Text>Overwrite weekly timer</Text>
          <View style={styles.row}>
            {overwriteScheduleLoading && (
              <ActivityIndicator
                size="small"
                color={Colors.grey}
                style={styles.marginRight}
              />
            )}
            <Switch
              onPress={() => setControl('overwriteSchedule')}
              value={unitUser.overwriteSchedule !== 0}
            />
          </View>
        </View>
      </View>
      {canControlLimit && (
        <View
          style={[
            styles.sectionContent,
            styles.row,
            styles.sectionContentBorder,
          ]}>
          <View style={[styles.inner, styles.innerRow, styles.padding]}>
            <View style={styles.column}>
              <Text>Limit Usage Time</Text>
              {unitUser.expiryTime !== '-' && !usageLimitLoading && (
                <Text style={styles.textGrey}>
                  {displayExpiryTime(unitUser.expiryTime)}
                </Text>
              )}
            </View>
            <View style={styles.row}>
              {usageLimitLoading && (
                <ActivityIndicator
                  size="small"
                  color={Colors.grey}
                  style={styles.marginRight}
                />
              )}
              <Switch
                onPress={() => setControl('limitUsage')}
                value={unitUser.expiryTime !== '-'}
              />
            </View>
          </View>
        </View>
      )}
    </View>
  );
};

UnitUser.propTypes = {
  setUnitUser: PropTypes.func,
  setUserControl: PropTypes.func,

  unitUser: PropTypes.object,
  route: PropTypes.object,
  shareUnitLoading: PropTypes.bool,
  usageLimitLoading: PropTypes.bool,
  canControlLimit: PropTypes.bool,
  overwriteScheduleLoading: PropTypes.bool,
  error: PropTypes.string,
};

const mapStateToProps = state => ({
  unitUser: state.unitUserControl.unitUser,
  shareUnitLoading: state.unitUserControl.shareUnitLoading,
  overwriteScheduleLoading: state.unitUserControl.overwriteScheduleLoading,
  usageLimitLoading: state.unitUserControl.usageLimitLoading,
  error: state.unitUserControl.error,
  canControlLimit: state.unitUsers.canControlLimit,
});

const mapDispatchToProps = dispatch => ({
  setUserControl: controlType => {
    dispatch(unitUserControl.setUserControl(controlType));
  },
  setUnitUser: user => {
    dispatch(unitUserControl.setUnitUser(user));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UnitUser);
