import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  Image,
  SectionList,
  Alert,
  RefreshControl,
} from 'react-native';
import {
  connect,
  unitUsers,
  unbindDevice,
  selector,
  units,
} from '@daikin-dama/redux-iot';
import {
  useFocusEffect,
  useNavigation,
  useIsFocused,
} from '@react-navigation/native';
import {
  heightPercentage,
  iphoneXS,
  ScaleText,
  StringHelper,
  NavigationService,
} from '@module/utility';

import {
  TabBar,
  Text,
  Loading,
  Colors,
  HeaderText,
  HeaderBackButton,
  getACIconFromPath,
} from '@module/daikin-ui';

import QXInfo from '../../constants/qxInfo';
import {
  QRCODE_SHARE_SCREEN,
  HOME_SCREEN,
  UNIT_USER_SCREEN,
  UNBIND_GUEST_SCREEN,
  LIMIT_USAGE_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  row: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  padding: {
    paddingHorizontal: 20,
  },
  unitDesc: {
    height: heightPercentage(10),
  },
  sectionHeader: {
    backgroundColor: Colors.userListSectionGrey,
    height: heightPercentage(4),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
  },
  sectionContent: {
    height: iphoneXS ? heightPercentage(8) : heightPercentage(9),
  },
  sectionContentBorder: {
    borderBottomColor: Colors.lightCoolGrey,
    borderBottomWidth: 0.5,
  },
  sectionTitle: {
    color: Colors.sectionHeaderTitleGrey,
  },
  textGray: {
    color: Colors.sectionContentGrey,
  },
  unitIcon: {
    marginRight: 10,
  },
  unitName: {
    paddingVertical: 5,
    color: 'rgb(74, 74, 74)',
  },
  flex: {
    flex: 1,
  },
  inner: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 15,
    flexDirection: 'row',
  },
  bottomContainer: {
    height: heightPercentage(9),
  },
  topContainer: {
    backgroundColor: 'blue',
  },
  full: {
    width: '100%',
    height: '100%',
  },
  headerRight: {
    flexDirection: 'row',
    marginRight: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hitSlop: {
    top: 20,
    bottom: 20,
    right: 15,
    left: 15,
  },
  marginRight: {
    marginRight: 15,
  },
  flexDCol: {
    flexDirection: 'column',
  },
  expiryWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  expiryContainer: {
    backgroundColor: Colors.blue,
    borderRadius: 15,
    padding: 1,
    paddingHorizontal: 15,
    marginLeft: 12,
  },
  expiryLabel: {
    color: Colors.white,
    fontSize: ScaleText(11),
  },
});

const handleShare = (totalShared, shareMaxLimit) => {
  if (totalShared < shareMaxLimit) {
    Alert.alert(
      '',
      'Do you want to allow user to share this device with others and overwrite weekly timer?\n\nYou can still change this.',
      [
        {
          text: 'Yes',
          onPress: () => {
            NavigationService.navigate(QRCODE_SHARE_SCREEN, {
              Qx: QXInfo.GUEST_SHARE_ALLOWED,
            });
          },
        },
        {
          text: 'No',
          onPress: () => {
            NavigationService.navigate(QRCODE_SHARE_SCREEN, {
              Qx: QXInfo.GUEST_SHARE_NOT_ALLOWED,
            });
          },
        },
        {
          text: 'Cancel',
          onPress: () => {},
        },
      ]
    );
  } else {
    Alert.alert('Error', 'You have exceeded the maximum guest limit for this unit.', [
      {
        text: 'OK',
        onPress: () => {},
      },
    ]);
  }
};

const checkIsHost = value => value === 'Host User';
const checkExpiryTime = value => {
  if (value) return value !== '-';
  return false;
};
const EXPIRY_TIME_FORMAT = 'DD MMMM YYYY, hh:mm A';
const EXPIRY_TIME_MOMENT_FORMAT = 'YYYY-MM-DD HH:mm:s';

const renderNavigationOptions = (route, navigation) => {
  const { params } = route;
  const { refreshButtonParams, showRemoveGuest, removeGuestButtonParams } =
    params || {};

  return {
    title: <HeaderText>User Management</HeaderText>,
    headerLeft: () => <HeaderBackButton navigation={navigation} />,
    headerRight: () => (
      <View style={styles.headerRight}>
        {refreshButtonParams && (
          <TouchableOpacity
            style={styles.marginRight}
            hitSlop={styles.hitSlop}
            {...refreshButtonParams}>
            <Image
              source={require('../../shared/assets/general/refresh.png')}
            />
          </TouchableOpacity>
        )}
        {showRemoveGuest && removeGuestButtonParams && (
          <TouchableOpacity
            hitSlop={styles.hitSlop}
            {...removeGuestButtonParams}>
            <Text>Remove guest</Text>
          </TouchableOpacity>
        )}
      </View>
    ),
  };
};

const UnitUsers = ({
  unit,
  unitUserList,
  isLoading,
  fetchUnitUsers,
  route,
  unbindUnitSuccess,
  unbindUnitError,
  unbindUnitLoading,
  submitUnbindUnit,
  submitUnbindAllGuest,
  clearUnbindUnitStatus,
  unbindGuestSuccess,
  unbindGuestError,
  isGuestLoading,
  profile,
  getAllUnits,
  canControlLimit,
  totalShared,
}) => {
  const isFocused = useIsFocused();
  const navigation = useNavigation();
  const onlyOneRecord = unitUserList && unitUserList.length === 1;
  const showRemoveGuest =
    unit && unit.qx === QXInfo.HOST && unitUserList && unitUserList.length > 1;
  const canShareUnit = unit.shareUnit === 1;
  const shareMaxLimit =
    unit.planFeatures && Number(unit.planFeatures.Lim_Guest);

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  const onRefresh = () => {
    getAllUnits(profile.email);
    fetchUnitUsers();
  };

  useFocusEffect(
    React.useCallback(() => {
      navigation.setParams({
        refreshButtonParams: {
          onPress: () => {
            onRefresh();
          },
        },
        showRemoveGuest,
        removeGuestButtonParams: {
          onPress: () => {
            NavigationService.navigate(UNBIND_GUEST_SCREEN);
          },
        },
      });
    }, [unitUserList, unit.shareUnit])
  );

  useEffect(() => {
    if (isFocused) fetchUnitUsers();
    else clearUnbindUnitStatus();

    return clearUnbindUnitStatus;
  }, [isFocused]);

  useEffect(() => {
    if (unbindUnitSuccess || unbindUnitError === 'Unauthorized access') {
      NavigationService.navigate(HOME_SCREEN);
    } else if (unbindUnitError) {
      Alert.alert('Error', unbindUnitError);
    }
  }, [unbindUnitSuccess, unbindUnitError]);

  useEffect(() => {
    if (unbindGuestSuccess) {
      fetchUnitUsers();
    } else if (unbindGuestError) {
      Alert.alert('Error', unbindGuestError);
    }

    clearUnbindUnitStatus();
  }, [unbindGuestSuccess, unbindGuestError]);

  const getTabItems = qx => {
    const tabItems = [];
    // Only allow share feature on guest with share allowed and host
    if (canShareUnit || qx === QXInfo.HOST) {
      tabItems.push({
        label: 'Share',
        imgSource: require('../../shared/assets/unitUser/share.png'),
        onPress: () => handleShare(totalShared, shareMaxLimit),
      });
    }

    if (showRemoveGuest) {
      tabItems.push({
        label: 'Remove all guests',
        imgSource: require('../../shared/assets/unitUser/unbindGuest.png'),
        onPress: () => {
          Alert.alert(
            'Remove all guests',
            'Are you sure you want to remove all guests for this unit?',
            [
              {
                text: 'Yes',
                onPress: () => submitUnbindAllGuest(unit.thingName, true),
              },
              {
                text: 'No',
              },
            ]
          );
        },
      });
    }

    tabItems.push({
      label: qx === QXInfo.HOST ? 'Remove all users' : 'Remove unit',
      imgSource: require('../../shared/assets/unitUser/unbind.png'),
      onPress: () => {
        Alert.alert(
          'Remove all users',
          'Are you sure you want to remove all users including yourself for this unit?',
          [
            {
              text: 'Yes',
              onPress: () => submitUnbindUnit(unit.thingName),
            },
            {
              text: 'No',
            },
          ]
        );
      },
    });

    return tabItems;
  };

  const displayExpiryTime = date => {
    const momentDate = moment(date, EXPIRY_TIME_MOMENT_FORMAT);
    return `${moment(momentDate).format(EXPIRY_TIME_FORMAT)}`;
  };

  return (
    <SafeAreaView style={styles.flex}>
      <Loading visible={unbindUnitLoading}>Unbinding unit...</Loading>
      <Loading visible={isGuestLoading}>Unbinding all guest users...</Loading>
      <ScrollView
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={onRefresh}
            enabled
          />
        }>
        <View style={[styles.row, styles.unitDesc, styles.padding]}>
          <Image
            style={styles.unitIcon}
            source={getACIconFromPath(unit.Logo)}
          />
          <View>
            <Text medium style={styles.unitName}>
              {unit.ACName}
            </Text>
            <Text small>{`Module ID: ${StringHelper.removePrefix(
              unit.thingName
            )}`}</Text>
          </View>
        </View>
        <SectionList
          sections={unitUserList}
          keyExtractor={(email, index) => email + index}
          /* eslint-disable no-unused-vars */
          renderItem={({ item, index, section }) => (
            <View
              style={[
                styles.sectionContent,
                styles.row,
                (!checkIsHost(section.title) || onlyOneRecord) &&
                  styles.sectionContentBorder,
              ]}>
              <TouchableOpacity
                style={styles.full}
                disabled={checkIsHost(section.title) || unit.qx !== QXInfo.HOST}
                onPress={() =>
                  NavigationService.navigate(UNIT_USER_SCREEN, {
                    user: item,
                  })
                }>
                <View style={[styles.inner, styles.padding]}>
                  <View style={styles.flexDCol}>
                    <View style={styles.expiryWrap}>
                      <Text>{item.name}</Text>
                      {!checkIsHost(section.title) &&
                        checkExpiryTime(item.expiryTime) &&
                        canControlLimit && (
                          <View style={styles.expiryContainer}>
                            <Text style={styles.expiryLabel}>
                              {displayExpiryTime(item.expiryTime)}
                            </Text>
                          </View>
                        )}
                    </View>
                    <Text style={styles.textGray}>{item.email}</Text>
                  </View>
                  {!checkIsHost(section.title) &&
                    checkExpiryTime(item.expiryTime) &&
                    unitUserList.length > 1 &&
                    canControlLimit && (
                      <TouchableOpacity
                        onPress={() =>
                          NavigationService.navigate(LIMIT_USAGE_SCREEN, {
                            dateData: item.expiryTime,
                          })
                        }
                        hitSlop={styles.hitSlop}>
                        <Image
                          source={require('../../shared/assets/unitUser/editLightGrey.png')}
                        />
                      </TouchableOpacity>
                    )}
                </View>
              </TouchableOpacity>
            </View>
          )}
          /* eslint-enable no-unused-vars */
          renderSectionHeader={({ section: { title } }) => (
            <View style={[styles.sectionHeader, styles.row, styles.padding]}>
              <Text style={styles.sectionTitle}>{title}</Text>
            </View>
          )}
        />
      </ScrollView>
      <TabBar style={styles.bottomContainer}>
        {getTabItems(unit.qx).map(item => (
          <TabBar.Item
            key={item.label}
            label={item.label}
            imgSource={item.imgSource}
            onPress={item.onPress}
          />
        ))}
      </TabBar>
    </SafeAreaView>
  );
};

UnitUsers.propTypes = {
  unit: PropTypes.object,
  unitUserList: PropTypes.array,
  isLoading: PropTypes.bool,
  fetchUnitUsers: PropTypes.func,
  route: PropTypes.object,
  unbindUnitSuccess: PropTypes.string,
  unbindUnitError: PropTypes.string,
  unbindUnitLoading: PropTypes.bool,
  submitUnbindUnit: PropTypes.func,
  submitUnbindAllGuest: PropTypes.func,
  clearUnbindUnitStatus: PropTypes.func,
  unbindGuestSuccess: PropTypes.string,
  unbindGuestError: PropTypes.string,
  isGuestLoading: PropTypes.bool,
  profile: PropTypes.object,
  getAllUnits: PropTypes.func,
  canControlLimit: PropTypes.bool,
  totalShared: PropTypes.number,
};

UnitUsers.defaultProps = {
  unitUserList: [],
};

UnitUsers.defaultProps = {
  unit: {},
};

const mapStateToProps = state => {
  const {
    unitUsers: { unitUserList, isLoading, canControlLimit, totalShared },
    unbindDevice: {
      success: unbindUnitSuccess,
      error: unbindUnitError,
      isLoading: unbindUnitLoading,
      guestSuccess: unbindGuestSuccess,
      guestError: unbindGuestError,
      isGuestLoading,
    },
    user: { profile },
  } = state;
  return {
    unit: selector.getRemoteUnit(state),
    unitUserList,
    isLoading,
    unbindUnitSuccess,
    unbindUnitError,
    unbindUnitLoading,
    unbindGuestSuccess,
    unbindGuestError,
    isGuestLoading,
    profile,
    canControlLimit,
    totalShared,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchUnitUsers: () => {
    dispatch(unitUsers.fetchUnitUsers());
  },
  getAllUnits: email => {
    dispatch(units.getAllUnits({ email }));
  },
  submitUnbindUnit: thingName => {
    dispatch(unbindDevice.submitUnbindDevice({ thingName }));
  },
  submitUnbindAllGuest: (thingName, isRemoveAllGuest) => {
    dispatch(unbindDevice.submitRemoveGuests({ thingName, isRemoveAllGuest }));
  },
  clearUnbindUnitStatus: () => {
    dispatch(unbindDevice.clearUnbindDeviceStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UnitUsers);
