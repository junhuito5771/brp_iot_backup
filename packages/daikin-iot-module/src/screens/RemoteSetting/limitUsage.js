import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { View, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { connect, unitUserControl } from '@daikin-dama/redux-iot';
import { verticalScale, NavigationService } from '@module/utility';
import { useFocusEffect } from '@react-navigation/native';
import { Calendar } from 'react-native-calendars';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';

import {
  Colors,
  Text,
  useDebounce,
  Loading,
  HeaderText,
  HeaderBackButton,
} from '@module/daikin-ui';

const CALENDAR_DATE_FORMAT = 'YYYY-MM-DD';
const EXPIRY_TIME_FORMAT = 'YYYY-MM-DD HH:mm:ss';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 20,
  },
  marginTop20: {
    marginTop: 20,
  },
  timeStyle: {
    height: 120,
    alignSelf: 'center',
  },
  submitStyle: {
    marginRight: 10,
  },
  hitslop: {
    top: 30,
    bottom: 30,
    right: 30,
    left: 30,
  },
  disabledTick: {
    tintColor: Colors.lightGrey,
  },
});

const renderNavigationOptions = (
  canSubmit,
  setUserControl,
  currentDateTime
) => ({
  headerRight: () => (
    <TouchableOpacity
      hitSlop={styles.hitSlop}
      onPress={() => setUserControl({ expiryTime: currentDateTime })}
      disabled={!canSubmit}
      style={styles.submitStyle}>
      <Image
        source={require('../../shared/assets/general/submitTick.png')}
        style={StyleSheet.flatten([!canSubmit && styles.disabledTick])}
      />
    </TouchableOpacity>
  ),
});

const LimitUsageScreen = ({
  navigation,
  setUserControl,
  usageLimitLoading,
  route,
  success,
}) => {
  const { dateData } = route.params;
  const [time, setTime] = useState(new Date());
  const [date, setDate] = useState(moment().format(CALENDAR_DATE_FORMAT));
  const [canSubmit, setCanSubmit] = useState(false);
  const [currentDateTime, setCurrentDateTime] = useState('');
  const debouncedTime = useDebounce(time, 500);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Limit Usage</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
      ...renderNavigationOptions(canSubmit, setUserControl, currentDateTime),
    });
  }, [navigation]);

  useEffect(() => {
    // get previous data
    if (dateData !== '-') {
      const getDate = dateData.split(' ')[0];
      const getTime = dateData.split(' ')[1];
      const getHour = getTime.split(':')[0];
      const getMinute = getTime.split(':')[1];

      setDate(moment(getDate).format(CALENDAR_DATE_FORMAT));

      const curDate = new Date();
      curDate.setHours(getHour);
      curDate.setMinutes(getMinute);
      setTime(curDate);
    }
  }, []);

  useEffect(() => {
    if (success) {
      navigation.goBack();
    }
  }, [success]);

  useFocusEffect(
    React.useCallback(() => {
      let combineDateTime = '';

      if (debouncedTime) {
        const debouncedHour = debouncedTime.getHours();
        const debouncedMinute = debouncedTime.getMinutes();
        const debouncedSeconds = debouncedTime.getSeconds();
        combineDateTime = `${date} ${debouncedHour}:${debouncedMinute}:${debouncedSeconds}`;
        const combineDateTimeMoment = moment(
          combineDateTime,
          EXPIRY_TIME_FORMAT
        );
        const availableDateTime = moment().isSameOrBefore(
          combineDateTimeMoment
        );
        setCanSubmit(availableDateTime);
        setCurrentDateTime(combineDateTime);
      }
    }, [debouncedTime, date, canSubmit])
  );

  return (
    <>
      <Loading visible={usageLimitLoading}>Please wait</Loading>
      <View style={styles.container}>
        <Text bold medium>
          {' '}
          Select Date{' '}
        </Text>
        <Calendar
          style={{
            height: verticalScale(370),
          }}
          firstDay={1} // Start from monday
          current={date}
          hideExtraDays
          minDate={time}
          onDayPress={day => {
            setDate(day.dateString);
          }}
          markedDates={{
            [date]: { selected: true, color: Colors.lightturquoise },
          }}
          markingType="period"
        />
        <View style={styles.marginTop20}>
          <Text bold medium>
            {' '}
            Select Time{' '}
          </Text>
          <DatePicker
            locale="en-US" // enable 12h format
            is24hourSource="locale"
            date={time}
            onDateChange={setTime}
            mode="time"
            style={styles.timeStyle}
          />
        </View>
      </View>
    </>
  );
};

LimitUsageScreen.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object,
  usageLimitLoading: PropTypes.bool,
  setUserControl: PropTypes.func,
  success: PropTypes.string,
};

const mapStateToProps = state => ({
  usageLimitLoading: state.unitUserControl.usageLimitLoading,
  success: state.unitUserControl.success,
});

const mapDispatchToProps = dispatch => ({
  setUserControl: controlType => {
    dispatch(unitUserControl.setUserControl(controlType));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LimitUsageScreen);
