import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Image,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import QRCode from 'react-native-qrcode-svg';
import Config from 'react-native-config';
import { connect, selector, QRCodeHelper } from '@daikin-dama/redux-iot';
import {
  widthPercentage,
  heightPercentage,
  ScaleText,
  StringHelper,
  NavigationService,
} from '@module/utility';

import {
  Text,
  Timer,
  Colors,
  HeaderText,
  HeaderBackButton,
} from '@module/daikin-ui';

import { useFocusEffect, useNavigation } from '@react-navigation/native';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  daikinIcon: {
    width: widthPercentage(60),
  },
  frameBackground: {
    width: widthPercentage(50),
    height: widthPercentage(50),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    marginTop: heightPercentage(4),
    marginBottom: heightPercentage(1),
    fontSize: ScaleText(12),
  },
  description: {
    marginTop: heightPercentage(2),
  },
  countDownText: {
    color: Colors.blue,
  },
});

const ShareQR = ({
  thingName,
  acGroup,
  acName,
  logo,
  route,
  randomKey,
  expiredDuration,
}) => {
  const navigation = useNavigation();
  const { Qx: qxInfo } = route.params;
  const [isRefresh, setRefresh] = useState(false);
  const [qrCodeText, setQRCodeText] = useState('');

  const toggleRefresh = () => {
    setRefresh(prevRefresh => !prevRefresh);
  };

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Share QR Code</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  useEffect(() => {
    setQRCodeText('');
    if (
      thingName &&
      acName &&
      acGroup &&
      logo &&
      randomKey &&
      expiredDuration
    ) {
      const encryQRCode = QRCodeHelper.getEncryptedQRPayload({
        thingName: StringHelper.removePrefix(thingName),
        acGroup,
        acName,
        logo,
        randomKey,
        expiredDuration,
        qxInfo,
        secretKey: Config.QRCODE_SECRET_KEY,
        iv: Config.QRCODE_IV,
        unitCode: 'D',
      });

      if (encryQRCode) {
        setQRCodeText(encryQRCode);
      }
    }
  }, [isRefresh, thingName, acName, acGroup, logo, randomKey, expiredDuration]);

  return (
    <SafeAreaView style={[styles.flex, styles.center]}>
      <Image
        source={require('../../shared/assets/logo/daikin.png')}
        style={styles.daikinIcon}
        resizeMode="contain"
      />
      <Text style={styles.title}>{`Module ID: ${StringHelper.removePrefix(
        thingName
      )}`}</Text>
      <ImageBackground
        source={require('../../shared/assets/shareQR/shareQRFrame.png')}
        style={styles.frameBackground}>
        {!qrCodeText ? (
          <ActivityIndicator size="large" />
        ) : (
          <QRCode value={qrCodeText} size={widthPercentage(40)} />
        )}
      </ImageBackground>
      <Text style={styles.description}>
        QR Code will be refreshed in{' '}
        <Timer
          style={styles.countDownText}
          duration={expiredDuration}
          onTimeOut={toggleRefresh}
          isAutoRefresh
        />
      </Text>
    </SafeAreaView>
  );
};

ShareQR.propTypes = {
  thingName: PropTypes.string,
  acGroup: PropTypes.string,
  acName: PropTypes.string,
  logo: PropTypes.string,
  route: PropTypes.object,
  randomKey: PropTypes.string,
  expiredDuration: PropTypes.number,
};

const mapStateToProps = state => {
  const {
    settings: { qrValidityPeriod },
  } = state;

  const {
    ThingName: thingName,
    ACGroup: acGroup,
    ACName: acName,
    Logo: logo,
    key: randomKey,
  } = selector.getRemoteUnit(state);

  return {
    thingName,
    acGroup,
    acName,
    logo,
    randomKey,
    expiredDuration: qrValidityPeriod.value,
  };
};

export default connect(mapStateToProps)(ShareQR);
