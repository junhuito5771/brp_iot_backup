import React, { useCallback } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Linking,
  Image,
  Platform,
  NativeModules,
} from 'react-native';
import Config from 'react-native-config';
import { connect, preferredUnits } from '@daikin-dama/redux-iot';
import { URLHelper, NavigationService } from '@module/utility';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { Text, Button, HeaderText } from '@module/daikin-ui';

import {
  ALEXA_LWA_SCREEN,
  ALEXA_AUTH_SCREEN,
  ALEXA_MANUAL_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    flex: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appNameText: {
    marginTop: 10,
  },
  desc: {
    marginTop: 30,
    marginHorizontal: 20,
  },
  descText: {
    textAlign: 'center',
  },
  bottom: {
    marginHorizontal: 20,
  },
  alexaIcon: {
    width: 80,
    height: 80,
  },
});

function getAlexaAppLink() {
  const baseUrl = Config.ALEXA_APP_LINK;
  const clientId = Config.ALEXA_CLIENT_ID;
  const scope = 'alexa::skills:account_linking';
  const skillStage = Config.ALEXA_SKILL_STAGE;
  const responseType = 'code';
  const redirectUrl = `https://${Config.UNI_LINK}`;
  const state = 'development';

  return (
    `https://${baseUrl}` +
    `&client_id=${clientId}` +
    `&scope=${scope}` +
    `&skill_stage=${skillStage}` +
    `&response_type=${responseType}` +
    `&redirect_uri=${redirectUrl}` +
    `&state=${state}`
  );
}

function checkIsAlexaInstalledInAndroid() {
  return NativeModules.PkgManagerModule.isAppInstalled(
    'com.amazon.dee.app',
    866607211
  );
}

function checkIsAlexaInstalledInIos() {
  return Linking.canOpenURL('alexa://open');
}

async function getIsAlexaInstalled() {
  let isInstalled;
  try {
    isInstalled = await Platform.select({
      ios: checkIsAlexaInstalledInIos,
      android: checkIsAlexaInstalledInAndroid,
    })();
  } catch (error) {
    isInstalled = false;
  }

  return isInstalled;
}

async function handleOnPressLink() {
  const isAlexaInstalled = await getIsAlexaInstalled();

  if (isAlexaInstalled) {
    Linking.openURL(getAlexaAppLink());
  } else {
    NavigationService.navigate(ALEXA_LWA_SCREEN);
  }
}

function AlexaLinkScreen({
  isLoading,
  fetchAlexaToken,
  alexaLinkStatus,
  setAlexaAuthPending,
}) {
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Alexa</HeaderText>,
      });
    }, [navigation])
  );

  useFocusEffect(
    useCallback(() => {
      const onReceiveUrl = ({ url }) => {
        const code = URLHelper.getUrlParam(url, 'code');
        if (code) {
          fetchAlexaToken({
            code,
            clientId: Config.ALEXA_CLIENT_ID,
            clientSecret: Config.ALEXA_CLIENT_SECRET,
            tokenUrl: Config.ALEXA_TOKEN_URL,
            redirectUrl: Config.UNI_LINK,
            skillStage: Config.ALEXA_SKILL_STAGE,
          });
        }
      };
      Linking.addEventListener('url', onReceiveUrl);

      return () => {
        Linking.removeEventListener('url', onReceiveUrl);
      };
    }, [])
  );

  useFocusEffect(
    useCallback(() => {
      if (alexaLinkStatus && alexaLinkStatus === 'tokenSuccess') {
        setAlexaAuthPending();
        NavigationService.navigate(ALEXA_AUTH_SCREEN);
      }
    }, [alexaLinkStatus])
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.top}>
        <Image
          style={styles.alexaIcon}
          source={require('../../shared/assets/alexa/alexaAppIcon.png')}
          resizeMode="contain"
        />
        <View style={styles.desc}>
          <Text style={styles.descText}>
            You can use your voice to control Daikin units by linking to Amazon
            smart speakers using Alexa
          </Text>
        </View>
      </View>
      <View style={styles.bottom}>
        <Button primary onPress={handleOnPressLink} isLoading={isLoading}>
          Link with Alexa
        </Button>
        <Button onPress={() => NavigationService.navigate(ALEXA_MANUAL_SCREEN)}>
          Alexa User manual
        </Button>
      </View>
    </SafeAreaView>
  );
}

AlexaLinkScreen.propTypes = {
  isLoading: PropTypes.bool,
  fetchAlexaToken: PropTypes.func,
  alexaLinkStatus: PropTypes.string,
  setAlexaAuthPending: PropTypes.func,
};

const mapStateToProps = ({ preferredUnits: preferredUnitsState }) => ({
  isLoading: preferredUnitsState.isLoading,
  alexaLinkStatus: preferredUnitsState.alexaLinkStatus,
});

const mapDispatchToProps = dispatch => ({
  fetchAlexaToken: params => dispatch(preferredUnits.fetchAlexaToken(params)),
  setAlexaAuthPending: () =>
    dispatch(
      preferredUnits.setAlexaTokenStatus({
        alexaLinkStatus: 'authPending',
      })
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AlexaLinkScreen);
