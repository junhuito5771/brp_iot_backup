import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  SectionList,
  Alert,
  SafeAreaView,
} from 'react-native';
import { connect, selector, preferredUnits } from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  widthPercentage,
  NavigationService,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  getACIconFromPath,
  Button,
  HeaderText,
  NavDrawerMenu,
  Colors,
  Text,
} from '@module/daikin-ui';
import { ALEXA_COMMAND_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  sectionHeader: {
    backgroundColor: Colors.lightCoolGrey,
    paddingVertical: heightPercentage(2.5),
    paddingHorizontal: widthPercentage(15),
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  sectionItem: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
  },
  sectionInnerItem: {
    paddingVertical: heightPercentage(1),
    paddingLeft: widthPercentage(10),
  },
  aircondIcon: {
    marginHorizontal: widthPercentage(3),
    minWidth: 56,
  },
  lengthText: {
    marginLeft: 10,
  },
  buttonContainer: {
    paddingHorizontal: widthPercentage(5),
  },
  marginRight: {
    marginRight: 10,
  },
  manualIcon: {
    width: 20,
    height: 20,
    right: 5,
  },
  container: {
    flex: 1,
  },
  sectionList: {
    flexGrow: 1,
  },
});
const checkboxIconPath = value =>
  value
    ? require('../../shared/assets/checkbox/checkBoxOn.png')
    : require('../../shared/assets/checkbox/checkBoxOff.png');

const PreferredUnitScreen = ({
  units,
  toggleUnit,
  canSubmit,
  isLoading,
  error,
  success,
  submitPreferredUnits,
  clearStatus,
  alexaCommand,
  resetAlexaRefreshStatus,
}) => {
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Preferred Units</HeaderText>,
        headerLeft: () => <NavDrawerMenu navigation={navigation} />,
        headerRight: () => (
          <View style={[styles.flexRow, styles.marginRight]}>
            <TouchableOpacity
              hitSlop={styles.hitSlop}
              onPress={() =>
                NavigationService.navigate(ALEXA_COMMAND_SCREEN, {
                  ...alexaCommand,
                  onLoadEnd: resetAlexaRefreshStatus,
                })
              }
              style={styles.marginManualIcon}>
              <Image
                style={styles.manualIcon}
                source={require('../../shared/assets/general/questionMark.png')}
              />
            </TouchableOpacity>
          </View>
        ),
      });
    }, [navigation, alexaCommand])
  );

  useFocusEffect(
    useCallback(() => {
      if (success) {
        Alert.alert(
          'Success',
          "You've successfully set your units as preferred units in Alexa"
        );
      } else if (error) {
        Alert.alert('Error', 'Unexpected error occured');
      }

      if (success || error) clearStatus();
    }, [success, error])
  );

  return (
    <SafeAreaView style={styles.container}>
      <SectionList
        contentContainerStyle={styles.sectionList}
        sections={units}
        keyExtractor={item => item.thingName}
        renderItem={itemRow => {
          const { item } = itemRow;
          return (
            <View style={styles.sectionItem}>
              <TouchableOpacity onPress={() => toggleUnit(item.thingName)}>
                <View style={[styles.flexRow, styles.sectionInnerItem]}>
                  <Image
                    source={checkboxIconPath(item.isAlexaPreferred)}
                    resizeMode="contain"
                  />
                  <Image
                    source={getACIconFromPath(item.logo)}
                    style={styles.aircondIcon}
                    resizeMode="contain"
                  />
                  <Text medium numberOfLines={1}>
                    {item.unitName}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          );
        }}
        renderSectionHeader={({ section }) => {
          const { title, data = [] } = section;

          return (
            <View style={[styles.flexRow, styles.sectionHeader]}>
              <Text medium numberOfLines={1}>
                {title}
              </Text>
              <Text small style={styles.lengthText}>
                ({data.length})
              </Text>
            </View>
          );
        }}
      />
      <View style={styles.buttonContainer}>
        <Button
          primary
          isLoading={isLoading}
          disabled={!canSubmit}
          disabledPrimary
          onPress={submitPreferredUnits}>
          Save
        </Button>
      </View>
    </SafeAreaView>
  );
};

const mapStateToProps = state => {
  const {
    preferredUnits: { canSubmit, isLoading, error, success, alexaCommand },
  } = state;
  return {
    units: selector.getAlexaPreferredUnits(state),
    canSubmit,
    isLoading,
    error,
    success,
    alexaCommand,
  };
};

const mapDispatchToProps = dispatch => ({
  toggleUnit: thingName => dispatch(preferredUnits.setToggleUnit(thingName)),
  submitPreferredUnits: () => dispatch(preferredUnits.submitAlexaPreferred()),
  clearStatus: () => dispatch(preferredUnits.clearStatus()),
  resetAlexaRefreshStatus: () =>
    dispatch(preferredUnits.resetAlexaCommandRefresh()),
});

PreferredUnitScreen.propTypes = {
  units: PropTypes.array,
  toggleUnit: PropTypes.func,
  canSubmit: PropTypes.bool,
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  submitPreferredUnits: PropTypes.func,
  clearStatus: PropTypes.func,
  alexaCommand: PropTypes.object,
  resetAlexaRefreshStatus: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreferredUnitScreen);
