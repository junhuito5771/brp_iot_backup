import React, { useCallback } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import WebView from 'react-native-webview';
import PropTypes from 'prop-types';

import { HeaderBackButton, Colors, HeaderText } from '@module/daikin-ui';
import { NavigationService } from '@module/utility';

const styles = StyleSheet.create({
  loadingCenter: {
    flex: 1,
    justifyContent: 'flex-start',
  },
});

const AlexaCommandScreen = ({ route }) => {
  const navigation = useNavigation();

  const { title, url, needRefresh, onLoadEnd } = route.params;

  useFocusEffect(
    useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>{title}</HeaderText>,
        headerLeft: () => <HeaderBackButton navigation={navigation} />,
      });
    }, [navigation])
  );

  const renderLoading = useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  return (
    <WebView
      {...(needRefresh && { incognito: true })}
      startInLoadingState
      renderLoading={renderLoading}
      source={{
        uri: url,
      }}
      onLoadEnd={() => {
        if (onLoadEnd) onLoadEnd();
      }}
    />
  );
};

AlexaCommandScreen.propTypes = {
  route: PropTypes.object,
};

export default AlexaCommandScreen;
