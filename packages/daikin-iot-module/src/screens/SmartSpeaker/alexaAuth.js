import React, { useState, useCallback } from 'react';
import { ActivityIndicator, Alert, StyleSheet, View } from 'react-native';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import WebView from 'react-native-webview';
import Config from 'react-native-config';
import { connect, preferredUnits } from '@daikin-dama/redux-iot';
import { URLHelper, NavigationService } from '@module/utility';
import PropTypes from 'prop-types';

import { Colors, Text, HeaderText, HeaderBackButton } from '@module/daikin-ui';

import { ALEXA_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  loadingCenter: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  actProgressContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actProgressText: {
    marginTop: 5,
  },
});

const AlexaAuthScreen = ({ alexaLinkStatus, activateSkill }) => {
  const navigation = useNavigation();
  const [activeProgress, setActiveProgress] = useState(false);

  useFocusEffect(
    useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Verify with Daikin App Account</HeaderText>,
        headerLeft: () => <HeaderBackButton navigation={navigation} />,
      });
    }, [navigation])
  );

  useFocusEffect(
    useCallback(() => {
      if (alexaLinkStatus && alexaLinkStatus === 'activateSuccess') {
        Alert.alert(
          'Alexa link success',
          "You've successfully link Daikin units to Alexa",
          [
            {
              text: 'OK',
              onPress: () => {
                navigation.replace(ALEXA_SCREEN);
              },
            },
          ]
        );
      }
    }, [alexaLinkStatus])
  );

  const renderLoading = useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  const onNavigationChanged = state => {
    if (state && state.url && state.url.indexOf('code=') !== -1) {
      const code = URLHelper.getUrlParam(state.url, 'code');
      if (!activeProgress) {
        setActiveProgress(true);
        activateSkill({
          code,
          skillStage: Config.ALEXA_SKILL_STAGE,
          redirectUrl: Config.UNI_LINK,
          skillEnaUrl: Config.ALEXA_SKILL_ENA_URL,
        });
      }
    }
  };

  return (
    <>
      {activeProgress ? (
        <View style={styles.actProgressContainer}>
          <ActivityIndicator size="large" color={Colors.azureRad} />
          <Text medium style={styles.actProgressText}>
            Activating the skill
          </Text>
        </View>
      ) : (
        <WebView
          incognito
          startInLoadingState
          renderLoading={renderLoading}
          onNavigationStateChange={onNavigationChanged}
          source={{
            uri: `https://${Config.COGNITO_LOGIN_URL}`,
          }}
        />
      )}
    </>
  );
};

AlexaAuthScreen.propTypes = {
  alexaLinkStatus: PropTypes.string,
  activateSkill: PropTypes.func,
};

const mapStateToProps = ({ preferredUnits: preferredUnitsState }) => ({
  alexaLinkStatus: preferredUnitsState.alexaLinkStatus,
});

const mapDispatchToProps = dispatch => ({
  activateSkill: params => dispatch(preferredUnits.activateAlexaSkill(params)),
  setActivatePending: () =>
    dispatch(
      preferredUnits.setAlexaTokenStatus({
        alexaLinkStatus: 'activatePending',
      })
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AlexaAuthScreen);
