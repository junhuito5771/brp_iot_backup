import React, { useCallback } from 'react';
import Pdf from 'react-native-pdf';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import Config from 'react-native-config';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { NavigationService } from '@module/utility';
import { HeaderBackButton, HeaderText } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pdf: {
    height: '100%',
    width: '100%',
  },
});

const uri = { uri: Config.ALEXA_MANUAL, cache: true };

const AlexaManualScreen = () => {
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      NavigationService.setOptions(navigation, {
        title: <HeaderText>Alexa Manual</HeaderText>,
        headerLeft: () => <HeaderBackButton navigation={navigation} />,
      });
    }, [navigation])
  );

  return (
    <View style={styles.container}>
      <Pdf
        style={styles.pdf}
        source={uri}
        activityIndicator={<ActivityIndicator size="large" />}
      />
    </View>
  );
};

export default AlexaManualScreen;
