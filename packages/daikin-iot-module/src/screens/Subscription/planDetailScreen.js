import React from 'react';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import WebView from 'react-native-webview';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import {
  verticalScale,
  viewportHeight,
  NavigationService,
} from '@module/utility';

import { Colors, HeaderText, HeaderBackButton } from '@module/daikin-ui';

// eslint-disable-next-line import/no-unresolved
import { subscription, connect } from '@module/redux-marketing';

const styles = StyleSheet.create({
  loadingCenter: {
    paddingTop: verticalScale(30),
    height: viewportHeight,
    justifyContent: 'flex-start',
  },
});

const PlanDetailScreen = ({ init, planDetail }) => {
  const navigation = useNavigation();

  useFocusEffect(
    React.useCallback(() => {
      init();
    }, [])
  );

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Plans</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const renderLoading = React.useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  return (
    <WebView
      startInLoadingState
      renderLoading={renderLoading}
      source={{ uri: planDetail.length ? planDetail[0].hyperlink : null }}
    />
  );
};

PlanDetailScreen.propTypes = {
  init: PropTypes.func.isRequired,
  planDetail: PropTypes.object.isRequired,
};

const mapStateToProps = state => {
  const {
    subscription: { planDetail },
  } = state;

  return { planDetail };
};

const mapDispatchToProps = dispatch => ({
  init: () => {
    dispatch(subscription.init());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PlanDetailScreen);
