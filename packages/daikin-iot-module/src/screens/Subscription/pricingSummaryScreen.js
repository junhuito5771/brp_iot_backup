import React from 'react';
import PropTypes from 'prop-types';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from 'react-native';
// eslint-disable-next-line import/no-unresolved
import { connect } from '@module/redux-marketing';
import {
  scale,
  ScaleText,
  verticalScale,
  NavigationService,
} from '@module/utility';

import {
  Colors,
  HeaderText,
  HeaderBackButton,
  Button,
  getACIconFromPath,
} from '@module/daikin-ui';

const styles = StyleSheet.create({
  flex: {
    display: 'flex',
  },
  row: {
    flexDirection: 'row',
  },
  unitHeader: {
    alignItems: 'center',
    height: verticalScale(60),
    borderColor: Colors.pricingBlue,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  unitLabel: {
    color: Colors.darkGrey,
    fontSize: ScaleText(18),
    fontWeight: '400',
  },
  unitDescription: {
    color: Colors.sectionContentGrey,
    fontSize: ScaleText(12),
  },
  unitLogo: {
    marginHorizontal: scale(12),
  },
  unitPriceLabel: {
    color: Colors.proBlue,
    marginHorizontal: scale(20),
    fontWeight: '700',
  },
  totalPrice: {
    color: Colors.black,
    fontWeight: '700',
  },
  rightAlign: {
    justifyContent: 'flex-end',
  },
  pricingBox: {
    flex: 1,
    alignItems: 'flex-end',
  },
  stickBottom: {
    justifyContent: 'flex-end',
    paddingHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
});

const PricingSummaryScreen = ({ allUnits, route }) => {
  const navigation = useNavigation();
  const { selectedUnits } = route.params;

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Pricing Summary</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const TotalPriceRow = () => (
    <View style={[styles.unitHeader, styles.row, styles.rightAlign]}>
      <Text style={styles.totalPrice}>Total price:</Text>
      <Text style={styles.unitPriceLabel}>RM35.70/year</Text>
    </View>
  );

  const UnitPricingList = () => {
    const allUnitsArray = Object.values(allUnits);
    const units = allUnitsArray.filter(unit =>
      selectedUnits.includes(unit.key)
    );

    return (
      <>
        {units.map(unit => (
          <View key={unit.key} style={[styles.unitHeader, styles.row]}>
            <Image
              style={styles.unitLogo}
              source={getACIconFromPath(unit.Logo)}
            />
            <View style={styles.flex}>
              <Text style={styles.unitLabel}>{unit.ACName}</Text>
              <Text style={styles.unitDescription}>
                {unit.planID === '2' ? 'Expiry Date' : 'Standard Plan'}
              </Text>
            </View>
            <View style={styles.pricingBox}>
              <Text style={styles.unitPriceLabel}>RM11.90/year</Text>
            </View>
          </View>
        ))}
      </>
    );
  };

  return (
    <SafeAreaView style={styles.flex}>
      <ScrollView style={styles.flex}>
        <UnitPricingList />
        <TotalPriceRow />
      </ScrollView>
      <View style={styles.stickBottom}>
        <Button primary disabledPrimary onPress={() => {}}>
          Purchase Now
        </Button>
      </View>
    </SafeAreaView>
  );
};

PricingSummaryScreen.propTypes = {
  allUnits: PropTypes.object,
  route: PropTypes.object,
};

const mapStateToProps = state => {
  const {
    units: { allUnits },
  } = state;

  return { allUnits };
};

export default connect(mapStateToProps, null)(PricingSummaryScreen);
