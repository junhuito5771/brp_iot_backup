import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  scale,
  ScaleText,
  verticalScale,
  NavigationService,
} from '@module/utility';
// eslint-disable-next-line import/no-unresolved
import { connect } from '@module/redux-marketing';
import { selector } from '@daikin-dama/redux-iot';

import {
  Colors,
  HeaderText,
  getACIconFromPath,
  NavDrawerMenu,
} from '@module/daikin-ui';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  container: {
    backgroundColor: Colors.white,
  },
  headerBar: {
    backgroundColor: Colors.sectionHeaderGrey,
    paddingHorizontal: scale(11),
    paddingVertical: verticalScale(10),
    justifyContent: 'center',
  },
  headerText: {
    color: Colors.sectionHeaderTitleGrey,
    fontSize: ScaleText(12),
  },
  groupHeader: {
    alignItems: 'center',
    height: verticalScale(48),
    backgroundColor: Colors.modeSeparatorGrey,
  },
  groupLabel: {
    color: Colors.darkGrey,
    fontSize: ScaleText(18),
    fontWeight: '400',
    paddingHorizontal: scale(14),
  },
  unitHeader: {
    alignItems: 'center',
    height: verticalScale(60),
    borderColor: Colors.pricingBlue,
    borderTopWidth: 1,
    borderBottomWidth: 1,
  },
  unitLabel: {
    color: Colors.darkGrey,
    fontSize: ScaleText(18),
    fontWeight: '400',
  },
  unitDescription: {
    color: Colors.sectionContentGrey,
    fontSize: ScaleText(12),
  },
  unitLogo: {
    marginHorizontal: scale(12),
  },
  unitPlanLabel: {
    color: Colors.proBlue,
    marginHorizontal: scale(20),
  },
  planButton: {
    alignItems: 'center',
    paddingHorizontal: scale(11),
  },
  planButtonText: {
    color: Colors.blue,
    fontSize: ScaleText(11),
  },
  checkbox: {
    alignItems: 'center',
    justifyContent: 'center',
    marginStart: scale(20),
    borderColor: Colors.sectionBorderGrey,
    borderRadius: 2,
    height: 20,
    width: 20,
    borderWidth: 1.5,
  },
  checkboxChecked: {
    backgroundColor: Colors.dayBlue,
    borderWidth: 0,
  },
  renewLabel: {
    color: Colors.sectionContentGrey,
    textAlign: 'center',
    marginVertical: verticalScale(10),
  },
  stickBottom: {
    justifyContent: 'flex-end',
    paddingHorizontal: scale(10),
    marginVertical: verticalScale(16),
  },
});

const SubscriptionScreen = ({ unitList }) => {
  const navigation = useNavigation();
  // const [isEditMode, setIsEditMode] = React.useState(false);
  const [checkedUnits, setCheckedUnits] = React.useState([]);

  // TODO: Hide Purchase Flow at current phase
  // const handleRenewPress = () => {
  //   setIsEditMode(!isEditMode);

  //   if (isEditMode) {
  //     navigation.navigate('PricingSummary', {
  //       selectedUnits: checkedUnits,
  //     });
  //   }
  // };

  const handleRenewUnit = unitKey => {
    if (checkedUnits.includes(unitKey)) {
      setCheckedUnits(checkedUnits.filter(key => key !== unitKey));
    } else {
      setCheckedUnits([...checkedUnits, unitKey]);
    }
  };

  // TODO: Hide Purchase Flow at current phase
  // const handleSelectAll = () => {
  //   const allUnitsArray = Object.values(allUnits);

  //   setCheckedUnits(checkedUnits.length < allUnitsArray.length ? (
  //     allUnitsArray.map(unit => unit.key)
  //   ) : []);
  // };

  // const SelectAllButton = () => {
  //   const allUnitsArray = Object.values(allUnits);

  //   return (
  //     <TouchableOpacity
  //       style={styles.planButton}
  //       onPress={handleSelectAll}
  //     >
  //       <Text style={styles.selectLabel}>
  //         {checkedUnits.length < allUnitsArray.length ? 'Select All' : 'Deselect All'}
  //       </Text>
  //     </TouchableOpacity>
  //   );
  // };

  const PlanDetailButton = React.memo(() => (
    <TouchableOpacity
      style={styles.planButton}
      onPress={() => navigation.navigate('PlanDetail')}>
      <Image source={require('../../shared/assets/subscribe/planDetail.png')} />
      <Text style={styles.planButtonText}>Plans</Text>
    </TouchableOpacity>
  ));

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Subscription</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      headerRight: () => <PlanDetailButton />,
      // TODO: Hide Purchase Flow at current phase
      // headerRight: () => (isEditMode ? (<SelectAllButton />) : (<PlanDetailButton />)),
    });
  }, [navigation]);

  const UnitList = () =>
    unitList.map(({ group, units }) => (
      <React.Fragment key={group.groupIndex}>
        <View style={[styles.groupHeader, styles.row]}>
          <Text style={styles.groupLabel}>{group.groupName}</Text>
          <Text>({units.length})</Text>
        </View>
        {units.map(unit => (
          <TouchableOpacity
            key={unit.key}
            onPress={() => handleRenewUnit(unit.key)}>
            <View style={[styles.unitHeader, styles.row]}>
              {/*
              // TODO: Hide Purchase Flow at current phase
              {isEditMode && (
                <TouchableOpacity onPress={() => handleRenewUnit(unit.key)}>
                  {checkedUnits.includes(unit.key) ? (
                    <View style={[styles.checkbox, styles.checkboxChecked]}>
                      <Image source={require('common/shared/images/general/check.png')} />
                    </View>
                  ) : (
                    <View style={styles.checkbox} />
                  )}
                </TouchableOpacity>
              )}
            */}
              <Image
                style={styles.unitLogo}
                source={getACIconFromPath(unit.Logo)}
              />
              <View style={styles.flex}>
                <Text style={styles.unitLabel}>{unit.ACName}</Text>
                <Text style={styles.unitDescription}>
                  {unit.planID === '2' ? `Expiry Date: ${moment(unit.planExpiredDate).format('D/M/YYYY')}`: 'Standard Plan'}
                </Text>
              </View>
              <Text style={styles.unitPlanLabel}>
                {unit.planID === '2' && 'PRO'}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </React.Fragment>
    ));

  return (
    <SafeAreaView style={[styles.flex, styles.container]}>
      <ScrollView scrollIndicatorInsets={{ right: 1 }} style={styles.flex}>
        <View style={styles.headerBar}>
          <Text style={styles.headerText}>Paired Units</Text>
        </View>
        <UnitList />
        {/*
          // TODO: Hide Purchase Flow at current phase
          {isEditMode && (
            <Text style={styles.renewLabel}>--- Select unit to renew ---</Text>
          )}
        */}
      </ScrollView>
      {/*
        // TODO: Hide Purchase Flow at current phase
        <View style={styles.stickBottom}>
          <Button
            primary
            disabledPrimary
            disabled={!checkedUnits.length && isEditMode}
            onPress={handleRenewPress}
          >
            { isEditMode ? 'Next' : 'Renew' }
          </Button>
        </View>
      */}
    </SafeAreaView>
  );
};

SubscriptionScreen.propTypes = {
  unitList: PropTypes.array,
};

const mapStateToProps = state => {
  const {
    units: { allUnits },
  } = state;

  return {
    unitList: selector.getHomepageList(state),
    allUnits,
  };
};

export default connect(mapStateToProps, null)(SubscriptionScreen);
