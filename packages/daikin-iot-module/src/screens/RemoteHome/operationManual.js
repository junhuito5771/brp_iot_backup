import React from 'react';
import { View, StyleSheet, Platform } from 'react-native';
import PropTypes from 'prop-types';
import Pdf from 'react-native-pdf';
import { NavigationService } from '@module/utility';
import { HeaderText, NavDrawerMenu, HeaderBackButton } from '@module/daikin-ui';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pdf: {
    height: '100%',
    width: '100%',
  },
});

const uriIos = require('../../shared/manual.pdf');

const uriAndroid = { uri: 'bundle-assets://manual.pdf' };

const uri = Platform.select({ ios: uriIos, android: uriAndroid });

const OperationManual = ({ route }) => {
  const lockDrawer = route.params && route.params.lockDrawer;
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Operation Manual</HeaderText>,
      headerLeft: () =>
        lockDrawer ? (
          <HeaderBackButton navigation={navigation} />
        ) : (
          <NavDrawerMenu navigation={navigation} />
        ),
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <Pdf style={styles.pdf} source={uri} />
    </View>
  );
};

OperationManual.propTypes = {
  route: PropTypes.object,
};

export default OperationManual;
