import React, { useEffect } from 'react';
import { Alert } from 'react-native';
import PropTypes from 'prop-types';
import { connect, updateFirmware } from '@daikin-dama/redux-iot';

import { Loading } from '@module/daikin-ui';

const UpdateFirmwareLoading = ({
  updateFirmwareLoading,
  updateFirmwareLoadingText,
  updateFirmwareSuccess,
  updateFirmwareError,
  clearFirmwareUpgradeStatus,
  cancelFirmwareUpgradeTask,
}) => {
  const handleFirmwareCancel = () => {
    Alert.alert(
      'Cancel firmware upgrade',
      'Are you sure you want to cancel the firmware upgrade?',
      [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          onPress: cancelFirmwareUpgradeTask,
        },
      ]
    );
  };
  useEffect(() => {
    if (updateFirmwareSuccess) {
      Alert.alert('Firmware update successful', updateFirmwareSuccess);
    } else if (updateFirmwareError) {
      if (updateFirmwareError.includes('verify firmware version')) {
        Alert.alert('Firmware update cannot be verified', updateFirmwareError);
      } else if (updateFirmwareError.includes('already the latest version')) {
        Alert.alert('Firmware update not necessary', updateFirmwareError);
      } else {
        Alert.alert('Firmware update failure', updateFirmwareError);
      }
    }

    clearFirmwareUpgradeStatus();
  }, [updateFirmwareSuccess, updateFirmwareError]);

  return (
    <Loading visible={updateFirmwareLoading} onCancel={handleFirmwareCancel}>
      {updateFirmwareLoadingText}
    </Loading>
  );
};

UpdateFirmwareLoading.propTypes = {
  updateFirmwareLoading: PropTypes.bool,
  updateFirmwareLoadingText: PropTypes.string,
  updateFirmwareSuccess: PropTypes.string,
  updateFirmwareError: PropTypes.string,
  clearFirmwareUpgradeStatus: PropTypes.func,
  cancelFirmwareUpgradeTask: PropTypes.func,
};

const mapStateToProps = state => ({
  updateFirmwareLoading: state.updateFirmware.isLoading,
  updateFirmwareLoadingText: state.updateFirmware.loadingText,
  updateFirmwareSuccess: state.updateFirmware.success,
  updateFirmwareError: state.updateFirmware.error,
});

const mapDispatchToProps = dispatch => ({
  clearFirmwareUpgradeStatus: () =>
    dispatch(updateFirmware.clearUpdateStatus()),
  cancelFirmwareUpgradeTask: () =>
    dispatch(updateFirmware.cancelFirmwareUpgrade()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateFirmwareLoading);
