import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  Alert,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
  AppState,
  NativeModules,
} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {
  heightPercentage,
  isSmallDevice,
  widthPercentage,
  ConnectionType,
  SetIndoorTemperature,
  LocationHelper,
  scale,
  NavigationService,
} from '@module/utility';
import {
  connect,
  units,
  signUp,
  homepageIotShadow,
  editUnits,
  remote,
  selector,
  updateFirmware,
  controlUsageLimit,
} from '@daikin-dama/redux-iot';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import {
  PERMISSIONS,
  RESULTS,
  requestMultiple,
  checkMultiple,
  openSettings,
} from 'react-native-permissions';

import Collapsible from 'react-native-collapsible';
import Toast from 'react-native-root-toast';

import { useNetInfo } from '@react-native-community/netinfo';
import {
  getACIconFromPath,
  NavDrawerMenu,
  HeaderText,
  Button,
  Text,
  LastUpdateClock,
  Colors,
} from '@module/daikin-ui';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  QR_SCANNER_SCREEN,
  EDIT_HOMEPAGE_SCREEN,
  REMOTE_SCREEN,
  OPERATION_MANUAL_SCREEN,
  BRP_PAIRING_INSTRUCT_SCREEN,
  NETWORK_DEBUG_SCREEN,
} from '../../constants/routeNames';
import getModeIconByKey from '../../helpers/getModeIconByKey';
import UpdateFirmwareLoading from './updateFirmwareLoading';

import dgram from 'react-native-udp'; //ADDED BY JUN

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  scroll: {
    paddingBottom: 30,
  },
  loading: {
    paddingVertical: 10,
  },
  padding: {
    paddingHorizontal: 20,
  },
  center: {
    alignSelf: 'center',
  },
  marginTopFive: {
    marginTop: heightPercentage(5),
  },
  marginTopThree: {
    marginTop: heightPercentage(3),
  },
  updateText: {
    marginVertical: heightPercentage(0.5),
    color: Colors.lightWarmGrey,
  },
  section: {
    justifyContent: 'center',
    paddingHorizontal: Platform.select({
      ios: widthPercentage(5),
      android: isSmallDevice ? widthPercentage(2) : widthPercentage(5),
    }),
  },
  collapsibleHeader: {
    backgroundColor: Colors.lightCoolGrey,
    paddingVertical: heightPercentage(1),
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  flexRow: {
    flexDirection: 'row',
  },
  headerText: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  headerContent: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  collapseButton: {
    height: 10,
    marginRight: 10,
  },
  offButton: {
    marginRight: widthPercentage(1),
  },
  button: {
    width: 50,
    height: 50,
  },
  collapsibleContent: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingVertical: heightPercentage(1),
    paddingLeft: widthPercentage(5),
  },
  aircondIcon: {
    marginRight: 15,
    minWidth: 56,
  },
  alignItems: {
    alignItems: 'center',
  },
  modeIcon: {
    height: 23,
    width: 18,
    marginRight: 5,
  },
  lightGrey: {
    color: Colors.lightWarmGrey,
  },
  grey: {
    color: Colors.darkGrey,
  },
  marginTop: {
    marginTop: Platform.OS === 'ios' ? 5 : 0,
  },
  marginRight: {
    marginRight: 10,
  },
  textSpaced: {
    marginRight: 5,
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
  editContainer: {
    position: 'absolute',
    zIndex: 1,
    right: 0,
    height: heightPercentage(6),
    width: scale(147),
    backgroundColor: Colors.white,
    paddingLeft: 25,
    justifyContent: 'center',
    shadowOpacity: 0.5,
    shadowColor: Colors.midLightGrey,
    shadowRadius: 5,
  },
  unitNameText: {
    maxWidth: isSmallDevice ? widthPercentage(30) : widthPercentage(35),
  },
  groupNameContainer: {
    maxWidth: widthPercentage(45),
    flexDirection: 'row',
  },
  infoIconContainer: {
    marginLeft: 5,
  },
  optionIcon: {
    height: 17,
    width: 10,
    resizeMode: 'contain',
    marginHorizontal: widthPercentage(3),
  },
  optionsWrapperStyle: {
    margin: 5,
  },
  optionsContainerStyle: {
    marginTop: 37,
    width: 135,
    marginLeft: 10,
  },
  optionWrapperStyle: {
    padding: 7,
    left: 10,
  },
  refreshIcon: {
    marginLeft: 7,
    marginRight: 7,
  },
  marginRight5: {
    marginRight: widthPercentage(5),
  },
  manualIcon: {
    width: 20,
    height: 20,
    right: 5,
  },
  marginManualIcon: {
    marginRight: 13,
  },
});

const offButtons = [
  require('../../shared/assets/controller/off.png'),
  require('../../shared/assets/controller/turnOff.png'),
];

const showToastMsg = msg => {
  Toast.show(msg, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: false,
    animation: true,
    hideOnPress: true,
    delay: 0,
    textColor: Colors.darkGrey,
    backgroundColor: Colors.lightCoolGrey,
    opacity: 0.9,
  });
};

const showAlert = () =>
  showToastMsg('Unit is disconnected.\nPull down to refresh for updates');

const showShadowLoadingAlert = () =>
  showToastMsg(
    'Please wait while the unit is getting the shadow data from the server'
  );

const checkCameraPermission = async () => {
  const checkPermissions = Platform.select({
    ios: [PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.LOCATION_WHEN_IN_USE],
    android: [
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ],
  });

  const checkPermissionStatus = await checkMultiple(checkPermissions);

  const cameraReqNeeded =
    checkPermissionStatus[checkPermissions[0]] !== RESULTS.GRANTED;
  const locationReqNeeded =
    checkPermissionStatus[checkPermissions[1]] !== RESULTS.GRANTED;

  const requestPermissions = [];

  if (cameraReqNeeded) {
    requestPermissions.push(checkPermissions[0]);
  }

  if (locationReqNeeded) {
    requestPermissions.push(checkPermissions[1]);
  }

  const permissionStatus =
    requestPermissions.length > 0
      ? await requestMultiple(requestPermissions)
      : undefined;

  const isCameraGranted =
    !cameraReqNeeded ||
    (permissionStatus &&
      permissionStatus[checkPermissions[0]] === RESULTS.GRANTED);
  const isLocationGranted =
    !locationReqNeeded ||
    (permissionStatus &&
      permissionStatus[checkPermissions[1]] === RESULTS.GRANTED);

  Platform.select({
    ios: () => {
      const privacyList = [];
      if (!isCameraGranted) {
        privacyList.push('Camera');
      }

      if (!isLocationGranted) {
        privacyList.push('Location');
      }

      if (privacyList && privacyList.length > 0) {
        Alert.alert(
          `No ${privacyList.join('/')} Access`,
          "To scan QR code and pair units, change permission in your device's app settings to allow GO DAIKIN app access to Camera.",
          [
            { text: 'Cancel', style: 'cancel' },
            { text: 'Settings', onPress: openSettings },
          ]
        );
      } else {
        // NavigationService.navigate(QR_SCANNER_SCREEN);
		  NavigationService.navigate(BRP_PAIRING_INSTRUCT_SCREEN, { thingName: "Daikin_D0C5D37099D2" });
		 //NavigationService.navigate(BRP_PAIRING_INSTRUCT_SCREEN);
      }
    },
    android: async () => {
      if (isCameraGranted && isLocationGranted) {
        const isLocationOn = await LocationHelper.enableLocationIfNeeded();

        // if (isLocationOn) NavigationService.navigate(QR_SCANNER_SCREEN);
        if (isLocationOn) NavigationService.navigate(BRP_PAIRING_INSTRUCT_SCREEN, { thingName: "Daikin_D0C5D37099D2" });
        //if (isLocationOn) NavigationService.navigate(BRP_PAIRING_INSTRUCT_SCREEN);
      }
    },
  })();
};

const goNetworkDebugScreen = () => {
	// NavigationService.navigate(NETWORK_DEBUG_SCREEN);
	console.log("TO INSTRUCT SCREEN?");
	NavigationService.navigate(BRP_PAIRING_INSTRUCT_SCREEN, { thingName: "Daikin_D0C5D37099D2" });
}

// ADDED BY JUN
const objectToQueryString = (obj) => {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}


const processPayload = (obj) => {

}
// ADDED BY JUN

const toggleSwitch = (groupKey, device, switchAc) => {
  if (device.status === 'connected') {
    const curSwitchValue = device.switch || 0;
    const targetedSwitchValue = 1 - curSwitchValue;

    console.warn("===== HERE IS TOGGLE SWITCH =====");
    console.warn("groupKey = " + groupKey);
    console.warn("targetedSwitchValue = " + targetedSwitchValue);
    console.warn("curSwitchValue = " + curSwitchValue);
    console.warn("===== HERE IS TOGGLE SWITCH =====");

    // const socket = dgram.createSocket({
    //   type: 'udp4',
    //   // reusePort: ,
    //   debug: true,
    // });

    // const isBroadcast = false;
    // const localport = 30050;
    // const targetport = 30050;
    // const targetAddress = '255.255.255.255';
    // const message = 'DAIKIN_UDP/common/basic_info';

    // socket.bind(targetport, function(){
    //   console.log("SOCKET BOUND!");
    // });
    // socket.once('listening', function() {
    //   console.log("IS LISTENING?");
    // })

    // socket.send(message, 0, message.length, targetport, '192.168.0.103', function(err) {
    // // socket.send(message, 0, message.length, targetport, '192.168.0.1', function(err) {
    //   if (err) throw err;

    //   console.log('Message sent!');
    // })

    // socket.on( "error", (error) => {
    //   console.log("UDP socket ERROR", error);
    // });

    // socket.on('message', function(msg, rinfo) {
    //   console.log('Message received', msg);

    //   // let decrypText = String.fromCharCode.apply(null, new Uint8Array(msg));
      
    //   // let decrypText = TextDecoder.decode(msg);
    //   let decrypText = decodeURIComponent(msg);
    //   console.log(decrypText);

    //   console.log(msg[0].data);
    //   console.log(rinfo);
    //   const promise = new Promise((resolve, reject) => {
    //     setTimeout(() => {
    //         resolve(msg);
    //     }, 5000);
    //   });

    //   promise.then(values => {
    //     console.log(values);
    //   });


    // })


    // ADDED BY JUN
    if(device.qx == "10"){ // IF IS BRP MODULE
      // BODY REQUEST
      const { BrpModule } = NativeModules;

      console.log("RUNNING BRP TOGGLE");
      // GET CURRENT MODULE STATUS

      BrpModule.getControlInfo((result)=>{

        console.log("PRINT RESULT!");
        console.log(result);

        let currentState = {};
        // CONVERT RESULT TO ARRAY WITH KEY
        for (let entry of result.split(",")) {
          let pair = entry.split("=");
          currentState[pair[0]] = pair[1];
        }

        // IF ERROR OCCUR, STOP THE PROCESS
        if(currentState['err']) return;

        var body = {
          "pow": currentState['pow'] == '1'?'0':'1',
          // "pow": targetedSwitchValue,
          "mode": currentState['mode'],
          "adv": currentState['adv'],
          "stemp": currentState['stemp'],
          "shum": currentState['shum'],
          "dt1": currentState['dt1'],
          "dt2": currentState['dt2'],
          "dt3": currentState['dt3'],
          "dt4": currentState['dt4'],
          "dt5": currentState['dt5'],
          "dt7": currentState['dt7'],
          "dh1": currentState['dh1'],
          "dh2": currentState['dh2'],
          "dh3": currentState['dh3'],
          "dh4": currentState['dh4'],
          "dh5": currentState['dh5'],
          "dh7": currentState['dh7'],
          "dhh": currentState['dhh'],
          "b_mode": currentState['b_mode'],
          "b_stemp": currentState['b_stemp'],
          "b_shum": currentState['b_shum'],
          "alert": currentState['alert'],
          "f_rate": currentState['f_rate'],
          "b_f_rate": currentState['b_f_rate'],
          "dfr1": currentState['dfr1'],
          "dfr2": currentState['dfr2'],
          "dfr3": currentState['dfr3'],
          "dfr4": currentState['dfr4'],
          "dfr5": currentState['dfr5'],
          "dfr6": currentState['dfr6'],
          "dfr7": currentState['dfr7'],
          "dfrh": currentState['dfrh'],
          "f_dir": currentState['f_dir'],
          "b_f_dir": currentState['b_f_dir'],
          "dfd1": currentState['dfd1'],
          "dfd2": currentState['dfd2'],
          "dfd3": currentState['dfd3'],
          "dfd4": currentState['dfd4'],
          "dfd5": currentState['dfd5'],
          "dfd6": currentState['dfd6'],
          "dfd7": currentState['dfd7'],
          "dfdh": currentState['dfdh'],
        }

        // BRP MODULE TOGGLE POWER ON/OFF
        BrpModule.setControlInfo(objectToQueryString(body), (result)=>{
          console.log("RESULT : ", result);
        });
      });

      

      
    
      
    }else{
      // RUN OTHERS TOGGLE FUNCTION
      // switchAc(groupKey, device.thingName, targetedSwitchValue);
    }
    // ADDED BY JUN


  } else {
    showAlert();
  }
};

const switchGroupAlert = ({ group }, switchValue, switchGroupAc) => {
  if (!group.isConnected) {
    showAlert();
    return;
  }
  console.warn("===== HERE IS START GROUP PROPERTY =====");
  console.warn(group);
  console.warn("=====  HERE IS END GROUP PROPERTY  =====");
  const switchType = switchValue ? 'on' : 'off';

  Alert.alert('', `Turn ${switchType} all units in this group?`, [
    { text: 'Cancel' },
    {
      text: 'OK',
      onPress: () => {
        switchGroupAc(group.groupIndex, switchValue);
      },
    },
  ]);
};

const showOnIcon = (switchState, status) => {
  if (status === 'disconnected' || status === undefined) {
    return require('../../shared/assets/controller/disableOn.png');
  }
  if (switchState === 0) {
    return require('../../shared/assets/controller/turnOn.png');
  }
  return require('../../shared/assets/controller/on.png');
};

const showOffIcon = (switchState, status) => {
  if (status === 'connected' && switchState === 0) {
    return offButtons[0];
  }

  return offButtons[1];
};

const showGroupOnIcon = group => {
  if (!group.isConnected) {
    return require('../../shared/assets/controller/disableOn.png');
  }
  if (group.switch) {
    return require('../../shared/assets/controller/on.png');
  }

  return require('../../shared/assets/controller/turnOn.png');
};

const showGroupOffIcon = group => {
  if (group && group.isConnected && !group.switch) {
    return offButtons[0];
  }

  return offButtons[1];
};

const switchHandler = (
  groupIndex,
  unit,
  switchAc,
  unitLoadingList,
  isFetchUnitsLoading
) => {
  if (isFetchUnitsLoading) {
    return (
      <ActivityIndicator
        size="small"
        color={Colors.black}
        style={styles.marginRight5}
      />
    );
  }
  if (unitLoadingList[unit.thingName]) {
    return (
      <ActivityIndicator
        size="small"
        color={Colors.black}
        style={styles.marginRight5}
      />
    );
  }
  if (unit.errorCode) {
    return <Image source={require('../../shared/assets/home/error.png')} />;
  }

  // BRP MODULE UI HANDLER
  if(unit.ACName.substring(0,3) == "BRP"){
    const { BrpModule } = NativeModules;

    // GET CURRENT MODULE STATUS
    let currentState = {};
    let acValue = 0;
    BrpModule.getControlInfo((result)=>{
      
      
      // CONVERT RESULT TO ARRAY WITH KEY
      for (let entry of result.split(",")) {
        let pair = entry.split("=");
        currentState[pair[0]] = pair[1];
      }

      if(currentState['err']){
        console.log("SOMETHING WENT ERROR!");
        console.log(currentState['err']);
        currentState['pow'] = '0';
      }

      acValue = parseInt(currentState['pow']);

      console.log("MY POW VALUE!");
      console.log(currentState['pow']);

      console.log("RUN BRP GET STATUS FIRST?");
    });

    console.log("AFTER BRP GET STATUS FIRST?");
    console.log(currentState['pow']);

    return (
      <>
        <TouchableOpacity
          style={styles.offButton}
          onPress={() => toggleSwitch(groupIndex, unit, switchAc)}
          disabled={unit.status === 'connected' && currentState['pow'] === '0'}>
          <Image
            source={showOffIcon(acValue, unit.status)}
            style={styles.button}
          />
        </TouchableOpacity>
  
        <TouchableOpacity
          onPress={() => toggleSwitch(groupIndex, unit, switchAc)}
          disabled={unit.status === 'connected' && currentState['pow'] === '1'}>
          <Image
            source={showOnIcon(acValue, unit.status)}
            style={styles.button}
          />
        </TouchableOpacity>
      </>
    );

  }else{
    return (
      <>
        <TouchableOpacity
          style={styles.offButton}
          onPress={() => toggleSwitch(groupIndex, unit, switchAc)}
          disabled={unit.status === 'connected' && unit.switch === 0}>
          <Image
            source={showOffIcon(unit.switch, unit.status)}
            style={styles.button}
          />
        </TouchableOpacity>
  
        <TouchableOpacity
          onPress={() => toggleSwitch(groupIndex, unit, switchAc)}
          disabled={unit.status === 'connected' && unit.switch === 1}>
          <Image
            source={showOnIcon(unit.switch, unit.status)}
            style={styles.button}
          />
        </TouchableOpacity>
      </>
    );
  }

  
};

const CollapsibleHeader = ({
  row,
  itemLength,
  isCollapsible,
  toggleCollapsible,
  switchGroupAc,
  sectionIndex,
}) => {
  const { group } = row;
  return (
    <View style={[styles.collapsibleHeader, styles.section]}>
      <View style={styles.headerContent}>
        <TouchableOpacity
          style={styles.headerText}
          onPress={() => toggleCollapsible(sectionIndex)}>
          <Image
            source={
              isCollapsible
                ? require('../../shared/assets/home/uncollapse.png')
                : require('../../shared/assets/home/collapse.png')
            }
            style={styles.collapseButton}
          />
          <View style={styles.groupNameContainer}>
            <Text medium numberOfLines={1}>
              {group.groupName}
            </Text>
            <Text small> ({itemLength})</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.flexRow}>
          <TouchableOpacity
            style={styles.offButton}
            onPress={() => switchGroupAlert(row, false, switchGroupAc)}
            disabled={group.isConnected && !group.switch}>
            <Image source={showGroupOffIcon(group)} style={styles.button} />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => switchGroupAlert(row, true, switchGroupAc)}
            disabled={group.isConnected && group.switch}>
            <Image source={showGroupOnIcon(group)} style={styles.button} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const CollapsibleContent = ({
  content,
  isCollapsible,
  switchAc,
  onPress,
  groupIndex,
  showFirmwareUpgradeNeedAlert,
  unitLoadingList,
  isFetchUnitsLoading,
}) =>
  content.map(unit => (
    <Collapsible key={unit.thingName} collapsed={isCollapsible}>
      <View style={[styles.section, styles.collapsibleContent]}>
        <View style={styles.headerContent}>
          <TouchableOpacity onPress={() => onPress(unit.thingName, unit)}>
            <View style={[styles.flexRow, styles.alignItems]}>
              <Image
                source={getACIconFromPath(unit.Logo)}
                style={styles.aircondIcon}
                resizeMode="contain"
              />
              <View style={styles.nameContainer}>
                <View style={[styles.flexRow, styles.alignItems]}>
                  <Text medium numberOfLines={1} style={styles.unitNameText}>
                    {unit.ACName}
                  </Text>
                  {!!unit.needUpgrade && (
                    <TouchableOpacity
                      style={styles.infoIconContainer}
                      hitSlop={styles.hitSlop}
                      onPress={() =>
                        showFirmwareUpgradeNeedAlert(
                          unit.thingName,
                          unit.needUpgrade
                        )
                      }>
                      <Image
                        source={require('../../shared/assets/home/info.png')}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  )}
                </View>
                <View
                  style={[styles.flexRow, styles.alignItems, styles.marginTop]}>
                  <Image
                    resizeMode="contain"
                    source={require('../../shared/assets/controller/indoorTemp.png')}
                    style={[
                      styles.modeIcon,
                      {
                        tintColor:
                          unit.status === 'disconnected'
                            ? Colors.lightWarmGrey
                            : Colors.darkGrey,
                      },
                    ]}
                  />
                  <Text
                    medium
                    style={[
                      styles.grey,
                      styles.textSpaced,
                      unit.status === 'disconnected' && styles.lightGrey,
                    ]}>
                    {SetIndoorTemperature(unit.indoorTemp)}{' '}
                  </Text>
                  <Image
                    resizeMode="contain"
                    source={getModeIconByKey(unit.mode).inactive}
                    style={[
                      styles.modeIcon,
                      {
                        tintColor:
                          unit.status === 'disconnected'
                            ? Colors.lightWarmGrey
                            : Colors.darkGrey,
                      },
                    ]}
                  />

                  <Text
                    medium
                    style={[
                      styles.grey,
                      unit.status === 'disconnected' && styles.lightGrey,
                    ]}>
                    {`${
                      unit.mode &&
                      unit.modeConfig &&
                      unit.modeConfig.acTemp &&
                      unit.mode !== 'fan' &&
                      unit.mode !== 'dry'
                        ? unit.modeConfig.acTemp
                        : '-'
                    }°C`}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableOpacity>
          <View style={styles.flexRow}>
            {switchHandler(
              groupIndex,
              unit,
              switchAc,
              unitLoadingList,
              isFetchUnitsLoading
            )}
          </View>
        </View>
      </View>
    </Collapsible>
  ));

const renderNavigationOptions = options => {
  const params = options;

  if (params && params.refreshButtonParams && params.editButtonParams) {
    const { refreshButtonParams, editButtonParams } = params;

    const editUnitHandler = () => {
      if (editButtonParams.loading) {
        return require('../../shared/assets/home/optionGrey.png');
      }
      return require('../../shared/assets/home/option.png');
    };

    return {
      headerRight: () => (
        <View style={[styles.flexRow, styles.marginRight]}>
          <TouchableOpacity
            hitSlop={styles.hitSlop}
            onPress={() =>
              NavigationService.navigate(OPERATION_MANUAL_SCREEN, {
                lockDrawer: true,
              })
            }
            style={styles.marginManualIcon}>
            <Image
              style={styles.manualIcon}
              source={require('../../shared/assets/home/questionMark.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            hitSlop={styles.hitSlop}
            // onPress={() => checkCameraPermission()}
            onPress={() => goNetworkDebugScreen()} // TEMPORARY EDIT BY JUN
            style={styles.marginRight}>
            <Image
              source={require('../../shared/assets/home/qrScanSmallBlue.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
            hitSlop={styles.hitSlop}
            {...refreshButtonParams}
            style={params.unitCount > 0 && styles.refreshIcon}>
            <Image
              source={require('../../shared/assets/general/refresh.png')}
            />
          </TouchableOpacity>
          <View hitSlop={styles.hitSlop} {...editButtonParams} elevation={5}>
            {params.unitCount > 0 && (
              <Menu>
                <MenuTrigger
                  disabled={editButtonParams.loading}
                  customStyles={{
                    TriggerTouchableComponent: TouchableOpacity,
                    triggerTouchable: { underlayColor: 'transparent' },
                  }}>
                  <Image source={editUnitHandler()} style={styles.optionIcon} />
                </MenuTrigger>
                <MenuOptions
                  customStyles={{
                    optionsWrapper: styles.optionsWrapperStyle,
                    optionsContainer: styles.optionsContainerStyle,
                    OptionTouchableComponent: TouchableOpacity,
                    optionWrapper: styles.optionWrapperStyle,
                  }}>
                  <MenuOption onSelect={editButtonParams.onPressEdit}>
                    <Text>Edit units</Text>
                  </MenuOption>
                </MenuOptions>
              </Menu>
            )}
          </View>
        </View>
      ),
    };
  }

  return params;
};

const Home = ({
  getAllUnits,
  unitCount,
  userEmail,
  confirmSuccess,
  clearConfirmMsg,
  switchAc,
  switchGroupAc,
  lastUpdated,
  editSuccess,
  clearEditStatus,
  setRemoteUnit,
  onRefreshUnit,
  isFetchUnitsLoading,
  homepageList,
  fetchUnitsError,
  clearUnitStatus,
  submitFirmwareUpgrade,
  unitLoadingList,
  toastMsg,
  clearToastMsg,
  initEditUnits,
  checkExpiryTime,
}) => {
  const navigation = useNavigation();
  const [showPopover, setShowPopover] = React.useState(false);
  const [activeSections, setActiveSections] = React.useState([]);
  const scrollRef = React.useRef({});
  const [isBlurBefore, setIsBlueBefore] = React.useState(false);

  const onRefresh = () => {
    getAllUnits(userEmail);
  };

  useFocusEffect(() => {
    checkExpiryTime();
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Home</HeaderText>,
      headerLeft: () => <NavDrawerMenu navigation={navigation} />,
      ...renderNavigationOptions({
        refreshButtonParams: {
          onPress: () => {
            onRefresh();
          },
        },
        editButtonParams: {
          onPress: () => {
            setShowPopover(!showPopover);
          },
          loading: isFetchUnitsLoading,
          onPressEdit: () => {
            initEditUnits(homepageList);
            NavigationService.navigate(EDIT_HOMEPAGE_SCREEN);
          },
        },
        unitCount,
      }),
    });
  }, [navigation, homepageList]);

  const showFirmwareUpgradeNeedAlert = (thingName, message) => {
    Alert.alert('Firmware update available', message, [
      {
        text: 'Later',
      },
      {
        text: 'Update now',
        onPress: () => submitFirmwareUpgrade(thingName),
      },
    ]);
  };

  React.useEffect(() => {
    if (
      isFetchUnitsLoading &&
      Platform.OS === 'ios' &&
      homepageList.length === 0
    ) {
      scrollRef.current.scrollTo({ x: 0, y: -160, animated: true });
    }
  }, [isFetchUnitsLoading]);

  React.useEffect(() => {
    if (homepageList && homepageList.length > -1) {
      setActiveSections(Array(homepageList.length).fill(false));
    }
  }, [homepageList]);

  React.useEffect(() => {
    if (toastMsg) {
      showToastMsg(toastMsg);

      clearToastMsg();
    }
  }, [toastMsg]);

  // check the internet connection error & fetchUnits error
  const netInfo = useNetInfo();
  React.useEffect(() => {
    if (!netInfo.isInternetReachable && fetchUnitsError) {
      Alert.alert(
        'No Connection Found',
        'Please check your network connection'
      );
    } else if (fetchUnitsError && !isBlurBefore) {
      Alert.alert('Error', fetchUnitsError);
    }

    clearUnitStatus();

    if (isBlurBefore && !isFetchUnitsLoading) {
      setIsBlueBefore(false);
    }
  }, [fetchUnitsError, isBlurBefore, isFetchUnitsLoading]);

  const initAndNavigateToRemote = (thingName, unit = {}) => {
    if (unit.indoorTemp) {
      setRemoteUnit(thingName);
      NavigationService.navigate(REMOTE_SCREEN, {
        unit,
        onRefreshUnit,
        mode: ConnectionType.IOT_MODE,
      });
    } else {
      showShadowLoadingAlert();
    }
  };

  React.useEffect(() => {
    SplashScreen.hide();
  }, []);

  React.useEffect(() => {
    if (confirmSuccess) {
      Alert.alert('Success!', confirmSuccess, [
        {
          text: 'OK',
          onPress: () => clearConfirmMsg(),
        },
      ]);
    }
  }, [confirmSuccess]);

  React.useEffect(() => {
    getAllUnits(userEmail);
  }, []);

  React.useEffect(() => {
    if (editSuccess) {
      Alert.alert('Success', 'Units updated successfully');
      getAllUnits(userEmail);
      clearEditStatus();
    }
  }, [editSuccess]);

  const handleAppStateChange = nextAppState => {
    const isBackground = nextAppState.match(/inactive|background/);

    if (isBackground) {
      setIsBlueBefore(isBackground);
    }
  };

  React.useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, []);

  const toggleCollapsible = sectionIndex => {
    setActiveSections(prevActiveSections => {
      const curActiveSections = [...prevActiveSections];
      curActiveSections[sectionIndex] = !prevActiveSections[sectionIndex];

      return curActiveSections;
    });
  };

  return (
    <View style={styles.container}>
      <UpdateFirmwareLoading />
      <ScrollView
        ref={scrollRef}
        scrollToOverflowEnabled
        contentContainerStyle={styles.scroll}
        refreshControl={
          <RefreshControl
            refreshing={isFetchUnitsLoading}
            onRefresh={onRefresh}
            size={18}
            title="Refreshing"
            titleColor={Colors.lightWarmGrey}
          />
        }>
        <View>
          <LastUpdateClock time={lastUpdated} />
          {homepageList.length < 1 && !isFetchUnitsLoading ? (
            <View style={styles.padding}>
              <Text bold medium style={[styles.marginTopFive, styles.center]}>
                Welcome, let&apos;s get started!
              </Text>
              <Text small style={styles.marginTopFive}>
                Add a Daikin Smart Control unit by clicking on the Add Unit
                button below
              </Text>
              <Text small style={styles.marginTopThree}>
                Pull down to refresh if connected units are not displayed.
              </Text>
              <Button
                style={styles.marginTopFive}
                onPress={() => checkCameraPermission()}>
                Add Unit
              </Button>
            </View>
          ) : (
            homepageList.map((row, index) => {
              const { group, units: unitsInGroup } = row;
              const { groupName, groupIndex } = group;

              console.log("PRINTING HERE");
              console.log(row);
              console.log(group);

              return (
                <View key={group.groupIndex}>
                  <CollapsibleHeader
                    row={row}
                    sectionIndex={index}
                    groupIndex={groupIndex}
                    itemLength={unitsInGroup.length}
                    isCollapsible={activeSections[index]}
                    toggleCollapsible={toggleCollapsible}
                    switchGroupAc={switchGroupAc}
                  />
                  <CollapsibleContent
                    content={unitsInGroup}
                    onPress={initAndNavigateToRemote}
                    // Refer to array index
                    sectionIndex={index}
                    // Refer to group index from server
                    groupIndex={groupIndex}
                    sectionKey={groupName}
                    isCollapsible={activeSections[index]}
                    switchAc={switchAc}
                    showFirmwareUpgradeNeedAlert={showFirmwareUpgradeNeedAlert}
                    unitLoadingList={unitLoadingList}
                    isFetchUnitsLoading={isFetchUnitsLoading}
                  />
                </View>
              );
            })
          )}
        </View>
      </ScrollView>
    </View>
  );
};

Home.propTypes = {
  homepageList: PropTypes.array,
  getAllUnits: PropTypes.func,
  clearConfirmMsg: PropTypes.func,
  switchAc: PropTypes.func,
  switchGroupAc: PropTypes.func,
  clearEditStatus: PropTypes.func,
  setRemoteUnit: PropTypes.func,
  onRefreshUnit: PropTypes.func,

  unitCount: PropTypes.number,
  userEmail: PropTypes.string,
  confirmSuccess: PropTypes.string,
  lastUpdated: PropTypes.number,
  editSuccess: PropTypes.string,
  isFetchUnitsLoading: PropTypes.bool,
  fetchUnitsError: PropTypes.string,
  clearUnitStatus: PropTypes.func,
  submitFirmwareUpgrade: PropTypes.func,
  unitLoadingList: PropTypes.object,
  toastMsg: PropTypes.string,
  clearToastMsg: PropTypes.func,
  initEditUnits: PropTypes.func,
  checkExpiryTime: PropTypes.func,
};

CollapsibleHeader.propTypes = {
  row: PropTypes.object,
  itemLength: PropTypes.number,
  isCollapsible: PropTypes.bool,
  toggleCollapsible: PropTypes.func,
  switchGroupAc: PropTypes.func,
  sectionIndex: PropTypes.number,
};

CollapsibleContent.propTypes = {
  content: PropTypes.array,
  isCollapsible: PropTypes.bool,
  switchAc: PropTypes.func,
  onPress: PropTypes.func,
  showFirmwareUpgradeNeedAlert: PropTypes.func,
  groupIndex: PropTypes.number,
};

const mapStateToProps = state => ({
  homepageList: selector.getHomepageList(state),
  unitCount: state.units.unitCount,
  mqttStatus: state.homepageIotShadow.status,
  userEmail: state.user.profile.email,
  isFetchUnitsLoading: state.units.isLoading,
  success: state.units.success,
  error: state.units.error,
  confirmSuccess: state.signUp.confirmSuccess,
  lastUpdated: state.units.lastUpdated,
  editSuccess: state.editUnits.success,
  fetchUnitsError: state.units.error,
  unitLoadingList: state.homepageIotShadow.unitLoadingList,
  toastMsg: state.homepageIotShadow.toastMsg,
});

const mapDispatchToProps = dispatch => ({
  // JUN
  initEditUnits: homepageList =>
    dispatch(editUnits.initEditUnit({ homepageList })),
  clearToastMsg: () => dispatch(homepageIotShadow.setToastMsg('')),
  clearConfirmMsg: () => {
    dispatch(signUp.clearConfirmSuccessMsg());
  },
  getAllUnits: (email, mqttConfig) => {
    dispatch(units.getAllUnits({ email, mqttConfig }));
  },
  switchAc: (targetGroupKey, thingName, switchType) => {
    dispatch(homepageIotShadow.switchAc(targetGroupKey, thingName, switchType));
  },
  switchGroupAc: (targetGroupKey, targetUnits, switchType) => {
    console.warn("SWITCH GROUP AC FUNCTION");
    console.warn("targetGroupKey = " + targetGroupKey);
    console.warn("targetUnits = " + targetUnits);
    console.warn("switchType = " + switchType);
    /*
    dispatch(
      homepageIotShadow.switchGroupAC(targetGroupKey, targetUnits, switchType)
    );
    */
  },
  setRemoteUnit: thingName => {
    dispatch(remote.setRemoteUnit({ thingName }));
  },
  clearEditStatus: () => {
    dispatch(editUnits.clearEditStatus());
  },
  checkExpiryTime: () => {
    dispatch(controlUsageLimit.checkExpiryTime());
  },
  onRefreshUnit: thingName => dispatch(remote.refreshUnit(thingName)),
  clearUnitStatus: () => dispatch(units.clearUnitsStatus()),
  submitFirmwareUpgrade: thingName => {
    dispatch(
      updateFirmware.submitFirmwareUpgrade({
        ios: Platform.select({ ios: true, android: false }),
        thingName,
      })
    );
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
