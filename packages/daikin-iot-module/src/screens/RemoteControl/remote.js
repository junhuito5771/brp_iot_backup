import React, { useEffect, useState } from 'react';
import { View, StyleSheet, SafeAreaView, Alert, Linking } from 'react-native';
import PropTypes from 'prop-types';
import {
  connect,
  remote,
  units,
  selector,
  settings,
  timer,
  unitUsers,
  unbindDevice,
  editTimer,
} from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  ConnectionType,
  widthPercentage,
  SAFE_AREA_INSET_BOTTOM,
  verticalScale,
  NavigationService,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  HeaderBackButton,
  HeaderIconButton,
  Button,
  Loading,
  Colors,
  LastUpdateClock,
  Text,
  ErrorButton,
  HeaderText,
  InfoIcon,
} from '@module/daikin-ui';

import Instruction from '../../components/RemoteBottomTab/Instruction';
import RemoteBottomTab from '../../components/RemoteBottomTab/RemoteDraggableTab';
import QuickTimerInfo from '../../components/RemoteBottomTab/QuickTimerInfo';
import UnitController from '../../components/UnitControl/UnitController';

import { TAB_ITEM_HEIGHT } from '../../constants/general';
import { getErrorCodeDetail } from '../../constants/errorCodes';

import {
  UNIT_DETAILS_SCREEN,
  WIFI_CONFIG_SCREEN,
  HOME_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  top: {
    flex: 0.9,
  },
  bottom: {
    flex: 0.1,
  },
  loadingContainer: {
    height: 5,
  },
  headerRightStyle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  updateText: {
    marginVertical: heightPercentage(0.5),
    color: Colors.lightWarmGrey,
  },
  overlay: {
    backgroundColor: Colors.transparentBlack,
    height: heightPercentage(100),
    width: widthPercentage(100),
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'center',
    paddingTop: heightPercentage(25),
    zIndex: 99,
  },
  text: {
    color: Colors.black,
    textAlign: 'center',
  },
  overlayWrapper: {
    backgroundColor: Colors.white,
    width: widthPercentage(80),
    height: verticalScale(250),
    paddingHorizontal: widthPercentage(4),
    borderRadius: 10,
    justifyContent: 'center',
  },
  overlayTitle: {
    alignItems: 'center',
  },
  overlayContent: {
    marginTop: 20,
  },
  infoButton: {
    position: 'absolute',
    right: 10,
    bottom: TAB_ITEM_HEIGHT + SAFE_AREA_INSET_BOTTOM + 20,
  },
});

const callDaikin = () => {
  const phoneNumber = 'tel:130088324546';
  Linking.openURL(phoneNumber);
};

const showErrorAlert = errorCode => {
  const errorDetail = getErrorCodeDetail(errorCode);

  if (errorDetail) {
    Alert.alert(
      `Error ${errorDetail.label}`,
      `${errorDetail.description}
    \nIn case the air conditioner is found to be faulty, contact your local dealer or call our warranty service line at 1-300-88-324546 (DAIKIN).`,
      [{ text: 'Call DAIKIN', onPress: callDaikin }, { text: 'OK' }]
    );
  }
};

const handleNavigateToHome = () => {
  NavigationService.navigate(HOME_SCREEN);
};

const RemoteOverlayModal = ({
  isConnected,
  isOTARunning,
  isReconnect,
  isIoTMode,
  navigateToWifiConfig,
  isPubSubDisconnected,
}) => {
  // Only show reconnect overlay modal if it this IoTMode
  if (!isConnected && !isReconnect && isIoTMode) {
    return (
      <View style={styles.overlay}>
        <View style={styles.overlayWrapper}>
          <View style={styles.overlayTitle}>
            <Text medium style={styles.text}>
              Controls are disabled
            </Text>
            <Text medium style={styles.text}>
              while the unit is disconnected.
            </Text>
          </View>
          <View style={styles.overlayContent}>
            <Button onPress={navigateToWifiConfig}>
              Change Wi-Fi Password
            </Button>
            <Button onPress={navigateToWifiConfig}>Reconnect</Button>
          </View>
        </View>
      </View>
    );
  }
  if (isIoTMode && isPubSubDisconnected) {
    return (
      <View style={styles.overlay}>
        <View style={styles.overlayWrapper}>
          <View style={styles.overlayTitle}>
            <Text medium style={styles.text}>
              Unable to retrieve unit data from server
            </Text>
            <Text medium style={styles.text}>
              The unit is reconnecting...
            </Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    isOTARunning && (
      <View style={styles.overlay}>
        <View style={styles.overlayWrapper}>
          <View style={styles.overlayTitle}>
            <Text medium style={styles.text}>
              Controls are disabled
            </Text>
            <Text medium style={styles.text}>
              while the unit is performing firmware upgrade.
            </Text>
          </View>
        </View>
      </View>
    )
  );
};

RemoteOverlayModal.propTypes = {
  isConnected: PropTypes.bool,
  isOTARunning: PropTypes.bool,
  isReconnect: PropTypes.bool,
  isIoTMode: PropTypes.bool,
  navigateToWifiConfig: PropTypes.func,
  isPubSubDisconnected: PropTypes.bool,
};

const renderNavigationOptions = (route, navigation) => {
  const { params: { unit, onRefreshUnit, mode } = {} } = route;

  const { ACName, thingName } = unit || {};

  const isIoTMode = mode === ConnectionType.IOT_MODE;

  return {
    title: (
      <HeaderText>{`${ACName.slice(0, 20)}${
        ACName.length > 20 ? '...' : ''
      }`}</HeaderText>
    ),
    headerLeft: () => <HeaderBackButton navigation={navigation} />,
    headerRight: () => (
      <View style={styles.headerRightStyle}>
        {isIoTMode && onRefreshUnit && (
          <HeaderIconButton
            onPress={() => onRefreshUnit(thingName)}
            src={require('../../shared/assets/general/refresh.png')}
          />
        )}
        {isIoTMode && unit && (
          <HeaderIconButton
            src={require('../../shared/assets/general/setting.png')}
            onPress={() =>
              NavigationService.navigate(UNIT_DETAILS_SCREEN, { unit })
            }
          />
        )}
      </View>
    ),
  };
};

const Remote = ({
  unit,
  onMountUnit,
  onUnmountUnit,
  setMode,
  setTemp,
  isLoading,
  notifiedUnitError,
  updateNotifiedError,
  upcomingTimer,
  isReconnect,
  checkUnitAuth,
  cancelCheckUnitAuth,
  desired,
  connectionType,
  showRemoteInstruct,
  setRemoteInstruct,
  connectionStatus,
  isQuickTimerLoading,
  hasQuickTimer,
  quickTimerError,
  clearQuickTimerError,
  clearUnitUsers,
  route,
  clearRemoteStatus,
  clearRemoveGuestStatus,
  guestSuccess,
  submitQuickTimer,
}) => {
  const [showInstruct, setShowInstruct] = useState(false);
  const errorCodeDetail = getErrorCodeDetail(unit.errorCode);
  const isIoTMode = connectionType === ConnectionType.IOT_MODE;
  const [dragable, setDragable] = useState();

  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  useEffect(() => {
    if (isIoTMode && !Object.entries(unit).length) {
      handleNavigateToHome();
      Alert.alert('No access', 'This unit is no longer bind to your account.');

      clearRemoteStatus();
    }
  }, [unit, isIoTMode]);

  useEffect(() => {
    if (isIoTMode || guestSuccess) {
      checkUnitAuth(handleNavigateToHome);

      return () => {
        cancelCheckUnitAuth();
        clearUnitUsers();
        clearRemoveGuestStatus();
      };
    }

    return () => {};
  }, [isIoTMode, guestSuccess]);

  useEffect(() => {
    if (showRemoteInstruct) {
      setShowInstruct(true);
      setRemoteInstruct(false);
    }
  }, []);

  useEffect(() => {
    onMountUnit(handleNavigateToHome);

    return () => onUnmountUnit();
  }, [unit, unit.thingName]);

  useEffect(() => {
    if (
      unit.errorCode &&
      errorCodeDetail &&
      !notifiedUnitError &&
      !showRemoteInstruct
    ) {
      showErrorAlert(unit.errorCode);
      updateNotifiedError(unit.thingName, true);
    }
  }, [unit.errorCode, notifiedUnitError]);

  useEffect(() => {
    if (quickTimerError) {
      Alert.alert('Quick timer error', quickTimerError);

      clearQuickTimerError();
    }
  }, [quickTimerError]);

  const navigateToWifiConfig = () => {
    NavigationService.navigate(WIFI_CONFIG_SCREEN, {
      thingName: unit.thingName,
      thingType: unit.ThingType,
      isReconnect: true,
    });
  };

  const handleOnDrag = draggable => {
    setDragable(draggable);
  };

  return (
    <SafeAreaView style={styles.flex}>
      <Loading visible={isReconnect}>
        Please wait while the unit is reconnecting to network
      </Loading>
      <Loading visible={isQuickTimerLoading}>
        {hasQuickTimer ? 'Removing quick timer...' : 'Creating quick timer...'}
      </Loading>
      <RemoteOverlayModal
        isConnected={unit.status === 'connected'}
        navigateToWifiConfig={navigateToWifiConfig}
        isOTARunning={unit.otaRunning === 1}
        isReconnect={isReconnect}
        isIoTMode={isIoTMode}
        isPubSubDisconnected={
          connectionStatus === 'DISCONNECTED' ||
          connectionStatus === 'RECONNECTING'
        }
      />
      {showInstruct && <Instruction onCancel={() => setShowInstruct(false)} />}
      {unit.status && (
        <>
          <LastUpdateClock time={unit.lastUpdated} />
          {errorCodeDetail && (
            <ErrorButton
              errorCode={errorCodeDetail.label}
              color={errorCodeDetail.color}
              onPress={() => showErrorAlert(unit.errorCode)}
            />
          )}
          {ConnectionType.IOT_MODE === connectionType && (
            <QuickTimerInfo
              thingName={unit.thingName}
              submitQuickTimer={submitQuickTimer}
            />
          )}
          <UnitController
            flexStyle={styles.flex}
            loadingContainer={styles.loadingContainer}
            {...{ unit, setMode, setTemp, isLoading, desired }}
          />
          {dragable ? (
            <InfoIcon
              containerStyle={styles.infoButton}
              onPress={() => setShowInstruct(true)}
            />
          ) : null}
          <RemoteBottomTab
            style={styles.bottom}
            thingName={unit.thingName}
            upcomingTimer={upcomingTimer}
            onDrag={handleOnDrag}
          />
        </>
      )}
    </SafeAreaView>
  );
};

Remote.defaultProps = {
  unit: {},
  isLoading: false,
};

Remote.propTypes = {
  unit: PropTypes.object,
  setMode: PropTypes.func,
  setTemp: PropTypes.func,
  onMountUnit: PropTypes.func,
  onUnmountUnit: PropTypes.func,
  onRefresh: PropTypes.func,
  isLoading: PropTypes.bool,
  notifiedUnitError: PropTypes.bool,
  updateNotifiedError: PropTypes.func,
  upcomingTimer: PropTypes.object,
  isReconnect: PropTypes.bool,
  checkUnitAuth: PropTypes.func,
  cancelCheckUnitAuth: PropTypes.func,
  desired: PropTypes.object,
  temp: PropTypes.number,
  setExpectedTemp: PropTypes.func,
  clearDesireTemp: PropTypes.func,
  connectionType: PropTypes.string,
  showRemoteInstruct: PropTypes.bool,
  setRemoteInstruct: PropTypes.func,
  connectionStatus: PropTypes.string,
  isQuickTimerLoading: PropTypes.bool,
  hasQuickTimer: PropTypes.bool,
  quickTimerError: PropTypes.string,
  clearQuickTimerError: PropTypes.func,
  clearUnitUsers: PropTypes.func,
  route: PropTypes.object,
  error: PropTypes.string,
  clearRemoteStatus: PropTypes.string,
  clearRemoveGuestStatus: PropTypes.func,
  guestSuccess: PropTypes.string,
  submitQuickTimer: PropTypes.func,
};

const mapStateToProps = state => {
  const {
    remote: {
      thingName,
      isLoading,
      isReconnect,
      desired: { temp },
      connectionType,
      connectionStatus,
      error,
    },
    units: { allUnits, hasNotifiedError },
    settings: { showRemoteInstruct },
    timer: { isQuickTimerLoading, quickTimerList, quickTimerError },
    unbindDevice: { success },
  } = state;

  return {
    unit: allUnits[thingName],
    upcomingTimer: selector.getUpcomingTimer(state),
    notifiedUnitError: hasNotifiedError[thingName],
    isLoading,
    isReconnect,
    desired: { temp },
    connectionType,
    showRemoteInstruct,
    connectionStatus,
    isQuickTimerLoading,
    hasQuickTimer: !!quickTimerList[thingName],
    quickTimerError,
    error,
    guestSuccess: success,
  };
};

const mapDispatchToProps = dispatch => ({
  setExpectedTemp: desired => {
    dispatch(remote.setExpectedTemp(desired));
  },
  setMode: mode => {
    dispatch(remote.setMode(mode));
  },
  onMountUnit: onUnbind => {
    dispatch(remote.onMountUnit({ onUnbind }));
  },
  onUnmountUnit: () => {
    dispatch(remote.onUnmountUnit());
  },
  setTemp: value => dispatch(remote.setAcTemp(value)),
  updateNotifiedError: (thingName, value) =>
    dispatch(units.updateNotifiedError(thingName, value)),
  checkUnitAuth: onUnbind => dispatch(remote.checkUnitAuth({ onUnbind })),
  cancelCheckUnitAuth: () => dispatch(remote.cancelCheckUnitAuth()),
  setRemoteInstruct: value => dispatch(settings.setRemoteInstruct(value)),
  clearQuickTimerError: () => dispatch(timer.clearQuickTimerError()),
  clearUnitUsers: () => dispatch(unitUsers.clearUnitUsers()),
  clearRemoteStatus: () => dispatch(remote.clearRemoteStatus()),
  clearRemoveGuestStatus: () =>
    dispatch(unbindDevice.clearUnbindDeviceStatus()),
  submitQuickTimer: (isCreate, timerInfo) => {
    dispatch(editTimer.submitQuickTimer({ isCreate, timerInfo }));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Remote);
