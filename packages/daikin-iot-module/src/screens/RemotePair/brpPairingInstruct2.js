import React, { useState } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity, TextInput, Alert, ScrollView, NativeModules } from 'react-native';
import Colors from '../../shared/Colors';
import { widthPercentage, heightPercentage, NavigationService } from '@module/utility';
import { BRP_PAIRING_INSTRUCT3_SCREEN } from '../../constants/routeNames';
// import { fetch } from 'react-native-ssl-pinning';
import randomString from '../../shared/randomString';

import RNFetchBlob from 'react-native-fetch-blob'; // ADDED BY JUN
// import pinch from 'react-native-pinch-new'; // ADDED BY JUN

import { UDPService } from '@module/utility';

// ADDED BY JUN
import { startNetworkLogging } from 'react-native-network-logger';
console.log("IS RUNNING!");
startNetworkLogging();
// ADDED BY JUN

import axios from 'axios';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: widthPercentage(5),
    fontSize: 15,
  },
  input: {
    borderColor: 'black',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.lightCoolGrey,
    paddingHorizontal: 10,
  },
  button: {
    marginTop: heightPercentage(5),
    borderWidth: 1,
    borderColor: Colors.blue,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 40,
    marginVertical: 5,
    marginBottom: heightPercentage(5),
  },
  buttonText: {
    color: Colors.blue,
    fontWeight: 'bold',
  },
  image: {
    height: heightPercentage(80),
    marginTop: heightPercentage(3),
    width: '100%',
  },
});

const showError = () => {
  Alert.alert('Error', "Connection to BRP adapter is unsuccessful. Please ensure you are connected to the adapter's network. ", [
    { text: 'OK' },
  ]);
};

const showErrorMessage = (e) => {
  Alert.alert('Error', e, [
    { text: 'OK' },
  ]);
};

// ADDED BY JUN
const objectToQueryString = (obj) => {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}
// ADDED BY JUN


const pairBRP = async (brpThingName, brpThingKey) => {
  const randomKey = randomString();
  // const randomKey = "d53b108a0e5e4fb5ab94a343b7d4b74c";
  
  const { BrpModule } = NativeModules;
  // BrpModule.getBasicInfo((result)=>{
	  // console.log("RESULT : ");
	// console.log(result);
	
  // });
  
  BrpModule.registerTerminal("0135000093663",(result)=>{
	  console.log("RESULT : ", result);
	  if(result!=null){
		  NavigationService.navigate(BRP_PAIRING_INSTRUCT3_SCREEN, { thingName: brpThingName, thingKey: "0135000093663", thingUUID: "b934deb8ee5b453c976bd2ecc9ba7d8f" })
	  }else{
		  showErrorMessage("Something went error");
	  }
  });
  
	// var body = {
		// "pow": "0",
		// "mode": "3",
		// "adv": "",
		// "stemp": "29.0",
		// "shum": "0",
		// "dt1": "26.0",
		// "dt2": "28.0",
		// "dt3": "28.0",
		// "dt4": "26.0",
		// "dt5": "26.0",
		// "dt7": "26.0",
		// "dh1": "50",
		// "dh2": "0",
		// "dh3": "0",
		// "dh4": "50",
		// "dh5": "50",
		// "dh7": "50",
		// "dhh": "50",
		// "b_mode": "3",
		// "b_stemp": "28.0",
		// "b_shum": "0",
		// "alert": "255",
		// "f_rate": "A",
		// "b_f_rate": "A",
		// "dfr1": "A",
		// "dfr2": "A",
		// "dfr3": "A",
		// "dfr4": "A",
		// "dfr5": "A",
		// "dfr6": "A",
		// "dfr7": "A",
		// "dfrh": "A",
		// "f_dir": "1",
		// "b_f_dir": "1",
		// "dfd1": "0",
		// "dfd2": "0",
		// "dfd3": "1",
		// "dfd4": "0",
		// "dfd5": "0",
		// "dfd6": "0",
		// "dfd7": "0",
		// "dfdh": "0"
	// }

  // BrpModule.setControlInfo(objectToQueryString(body), (result)=>{
	  // console.log("RESULT : ", result);
  // });
  
  // console.log("=========> FETCHING");
  // console.log("=========> FETCHING");
  // console.log("=========> FETCHING");
  // console.log("=========> FETCHING");
  // console.log("=========> FETCHING");
  /*
   failed to connect to /10.0.2.2 (port 443) from /192.168.127.2 (port 40186) after 10000ms
  */
  
  // let url = "https://192.168.127.1/common/register_terminal?key=" + brpThingKey;
  // let url = "https://192.168.127.1/common/register_terminal?key=0135000093663";
  // let url = "https://10.0.2.2/common/register_terminal?key=0135000093663";
  // let url = "https://10.0.2.2/common/register_terminal?key=" + brpThingKey;
  // let url = "https://192.168.127.1/common/basic_info";
  // let url = "https://jsonplaceholder.typicode.com/todos/1";
  // console.log(url);
  
  
	// AXIOS
	
	// const agent = new https.Agent({  
		// rejectUnauthorized: false
	// });

	// axios.get(url, { httpsAgent: agent });
	
	// axios.get(url)
	  // .then(function(response) {
		// console.log(response.data);
		// console.log(response.status);
		// console.log(response.statusText);
		// console.log(response.headers);
		// console.log(response.config);
	// });
  
	// AXIOS
  
  
  
  
	// pinch.fetch("https://jsonplaceholder.typicode.com/todos/1", {
		// method: 'get',
		// headers: { "X-Daikin-uuid": "b934deb8ee5b453c976bd2ecc9ba7d8f", },
		// body: '{"firstName": "Jake", "lastName": "Moxey"}',
		// timeoutInterval: 10000, // timeout after 10 seconds
		// ignore_ssl:true, // This can be used to ignore SSL pinning
		// sslPinning: {} // omit the `cert` or `certs` key, `sslPinning` can be ommited as well
	// }, (err, res) => {
		// if (err) {
			// console.error(`Whoopsy doodle! Error - ${err}`);
			// return null;
		// }
		
		// console.log(`We got your response! Response - ${res}`);
	// })
	
	// fetch(url,{
		// method: 'GET',
		// mode: 'no-cors',
		// headers: new Headers({
			// "X-Daikin-uuid": "b934deb8ee5b453c976bd2ecc9ba7d8f",
		// }),
	// }
	// ).then( response => { console.log(response);})
	// .catch(err => console.log(err))
	  
	
	
	// var request = new XMLHttpRequest();
	// request.onreadystatechange = (e) => {
		// console.log("NETWORK STATE CHANGE =====>");
		// console.log("NETWORK STATE CHANGE =====>");
		// console.log("NETWORK STATE CHANGE =====>");
		// console.log("NETWORK STATE CHANGE =====>");
		// console.log("NETWORK STATE CHANGE =====>");
		// console.log(e);
	  // if (request.readyState !== 4) {
		// return;
	  // }

	  // if (request.status === 200) {
		// console.log('success', request.responseText);
	  // } else {
		// console.warn('error');
	  // }
	// };
	
	// request.open('GET', url, true);
	// request.setRequestHeader("X-Daikin-uuid", "b934deb8ee5b453c976bd2ecc9ba7d8f");
	// request.send();
	

	
	
	// UDP CLIENT CONNECT TO SERVER - BY JUN
	
	
	
	// UDP CLIENT CONNECT TO SERVER - BY JUN

  // RNFetchBlob.config({
    // trusty : true
  // })
  // .fetch('GET', url)
  // .then((resp) => {
    // ...
    // console.log(resp.data);
    // console.log(resp.respInfo.status);
    // console.log(resp);
  // })

  /*
  RNFetchBlob.config({
    trusty : true
  })
  .fetch('GET', url, {
    "X-Daikin-uuid": "b934deb8ee5b453c976bd2ecc9ba7d8f",
	// "Access-Control-Allow-Origin": "*",
	// "e_platform": "mobile",
  })
  .then((resp) => {
    console.log(resp.respInfo.status);
    console.log(resp);
  }).catch(err => {
	console.log("=====ERROR=====");  
	console.log(err);  
	console.log("=====ERROR=====");  
	  
  })
  */
  
  /*
  RNFetchBlob.config({
    trusty : true
  })
  .fetch('GET', url, {
    // "X-Daikin-uuid": "b934deb8ee5b453c976bd2ecc9ba7d8f",
	'Content-Type': 'application/json',
	// "e_platform": "mobile",
  })
  .then((resp) => {
    console.log(resp.respInfo.status);
    console.log(resp);
  }).catch(err => {
	console.log("=====ERROR=====");  
	console.log(err);  
	console.log("=====ERROR=====");  
	  
  })
  */
  
	
	// react-native-ssl-pinning
	// fetch(
		// url,{
		// method: "GET",
		// timeoutInterval: 30000, // 30s
		// disableAllSecurity: false,
		// caseSensitiveHeaders: true,
		// headers: {
			// "X-Daikin-uuid": "b934deb8ee5b453c976bd2ecc9ba7d8f",
		// }
	// }).then(response => {
		// console.log(response);
	// }).catch(error => {
		// console.log(error);
	// })
	
  
  
  /*
  fetch(url, {
    method: "GET",
    timeoutInterval: 60000,
    disableAllSecurity: true,
    caseSensitiveHeaders: true,
    headers: {
      "X-Daikin-uuid": "5a2d77d0b86211eb85290242ac130003",
      // "X-Daikin-uuid": randomKey
    }
    
    
  })
  
  
    .then(response => {
      console.log(response);
      
      //result = response.bodyString.split("=")
      //if (response.status == 200 && result[1] == "OK") {
      if (response.status == 200 ) {
          console.log("hahahahahaa");
          console.log("hahahahahaa");
          console.log("hahahahahaa");
          console.log("hahahahahaa");
        //NavigationService.navigate(BRP_PAIRING_INSTRUCT3_SCREEN, { thingName: brpThingName, thingKey: brpThingKey, thingUUID: randomKey })
      }
      else {
          console.log("=========> SHOW_ERROR1");
          console.log("=========> SHOW_ERROR1");
          console.log("=========> SHOW_ERROR1");
        showError();
      }
    })
    .catch(err => {
        console.log("=========> SHOW_ERROR2");
        console.log("=========> SHOW_ERROR2");
        console.log("=========> SHOW_ERROR2");
        console.log("=========> ", err);
      //showErrorMessage(err);
    })
  */
  
  // const udpClient = UDPService.createUDPSocket({
    // type: 'udp4',
    // reuseAddr: true,
  // });
  // const isBroadcast = true;
  // const localport = 30050;
  // const targetport = 30050;
  // const targetAddress = '255.255.255.255';
  // const message = 'DAIKIN_UDP/common/basic_info';
  // const response = await UDPService.sendMessage({
    // localport,
    // isBroadcast,
    // targetport,
    // targetAddress,
    // udpClient,
    // message,
    // noResponseMessage: 'We are unable to detect any available units. Please ensure that the mobile phone is connected to the adapter Wi-Fi network',
  // });
  // for (i = 0; i < response.length; i++) {
    // if (response[i].data != message) {
      // console.log(response[i].source.address)
      // console.log(response[i].data)
    // }
  // }
};

const BrpPairingInstruct2 = ({ route, navigation }) => {
  //const brpThingName = navigation.getParam('thingName');
  const brpThingName = route.params.thingName;
  const [brpThingKey, setText] = useState('');
  return (
    <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
          <Text>{"\n"}Please refer to the Wi-Fi details located at the back of the adapter and enter the adapter's Wi-Fi password. {"\n"}</Text>
          <TextInput
            style={styles.input}
            placeholder="BRP Adapter's Key"
            defaultValue="0135000093663"
            onChangeText={brpThingKey => setText(brpThingKey)}
          />
          <Image
            source={require('../../shared/images/brp/brp_instruct_2.png')}
            style={styles.image}
          />
          <TouchableOpacity onPress={() => pairBRP(brpThingName, brpThingKey)}>
            <View style={styles.button}>
              <Text style={styles.buttonText}>Continue</Text>
            </View>
          </TouchableOpacity>
        </ScrollView>
    </View>
  );
}

export default BrpPairingInstruct2;