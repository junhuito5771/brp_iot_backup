import React from 'react';
import NetworkLogger from 'react-native-network-logger';

const networkDebug = () => <NetworkLogger/>;

export default networkDebug;