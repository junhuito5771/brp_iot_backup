import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native';
import Colors from '../../shared/Colors';
// import { ApiService, UDPService, NetworkHelper, widthPercentage, heightPercentage, NavigationService } from '@module/utility';
import { ApiService, NetworkHelper, widthPercentage, heightPercentage, NavigationService } from '@module/utility';
import { HOME_SCREEN } from '../../constants/routeNames';
import PropTypes from 'prop-types';
import { connect, units } from '@module/redux';
import * as dgram from 'react-native-udp';

import { call, put, select } from 'redux-saga/effects';
import getUnitIndex from '../../../../redux-iot-module/src/utils/getUnitIndex';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: widthPercentage(5),
    fontSize: 15,
  },
  button: {
    marginTop: heightPercentage(5),
    borderWidth: 1,
    borderColor: Colors.blue,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 40,
    marginVertical: 5,
  },
  buttonText: {
    color: Colors.blue,
    fontWeight: 'bold',
  },
});

const BrpPairingInstruct5 = ({ route, navigation, getAllUnits }) => {
  const brpThingName = route.params.thingName;
  const brpThingUUID = route.params.thingUUID;
  const userEmail = route.params.userEmail;
  const units = route.params.units;

  const showMessage = (title, msg) => {
    Alert.alert(title, msg, [
      { text: 'OK' },
    ]);
  };

  const bindBRPmasterinfo = async (brpThingName, brpThingUUID, userEmail, units) => {
    const connectedSSID = await NetworkHelper.getSSID();
    console.log(connectedSSID);

    if (connectedSSID.startsWith("DaikinAP")) {
      showMessage("Error", "Please connect to your Wi-Fi network");
    }
    else {

      var ip;
      // const udpClient = UDPService.createUDPSocket({
      //   type: 'udp4',
      //   reuseAddr: true,
      // });      
	  
	  // const socket = dgram.createSocket({
    //     type: 'udp4',
		// debug: true,
    //   });
      const isBroadcast = false;
      const localport = 30050;
      const targetport = 30050;
      const targetAddress = '255.255.255.255';
      const message = 'DAIKIN_UDP/common/basic_info';
	  
		// socket.bind(localport, function(){
		// 	console.log('bound');
		// });
		
		// socket.once('listening', function() {
		// 	console.warn("RUNNING");
		//   socket.send(message, undefined, undefined, targetport, targetAddress, function(err) {
		// 	if (err){
		// 		console.log("something error");
		// 		throw err;
		// 	}

		// 	console.log('Message sent!')
		//   });
		  
		// });
		// console.log("DONE?");
		// socket.on('message', function(msg, rinfo) {
		//   console.log('Message received', msg);
		// });

      // try {
      //   const response = await dgram.sendMessage({
      //     localport,
      //     isBroadcast,
      //     targetport,
      //     targetAddress,
      //     udpClient,
      //     message,
      //     noResponseMessage: 'We are unable to detect any available units. Please ensure that the mobile phone is connected to the Wi-Fi network',
      //   });

      //   console.warn("DONE SENDING MSG?");

      //   for (i = 0; i < response.length; i++) {
      //     if (response[i].data.startsWith("ret=")) {
      //       ip = response[i].source.address;
      //     }
      //   }
      // } catch (error) {
      //   alert(error + ". Please try again.")
      // }

      // console.log("ip")
      // console.log(ip)

	  // ip = "192.168.127.1"; // TESTING PURPOSE
      // const newUnitIndex = getUnitIndex(units, true, -1, brpThingName);
	  // export default function* getUnitIndex(isGroupExist, groupIndex, thingName) { // PARAM REFERENCE
      // const newUnitIndex = getUnitIndex(true, -1, brpThingName);
	  const res = await getUnitIndex(true, -1, brpThingName);
	  console.log(brpThingName);
	  // console.log(state);

      // try {
        // const result = await ApiService.post(
          // 'bindbrpmasterinfo',
          // {
            // requestData: {
              // /*username: userEmail,*/
              // username: "a52bb8d9-bbe6-4f5a-82b9-cef156237db1",
              // thingName: brpThingName,
              // thingType: "AC",
              // qx: "10",
              // acName: "BRP072C42",
              // acGroup: "All Units",
              // logo: "2.png",
              // groupIndex: -1,
              // unitIndex: getUnitIndex(true, -1, brpThingName),
              // key: brpThingUUID,
              // ip: ip
            // }
          // },
          // type = 'json'
        // );
		// console.log(result);
        // getAllUnits(userEmail);
        // showMessage("Success", "Pairing completed!")
      // } catch (error) {
        // alert(error + ". Please try again.")
      // }

      // NavigationService.navigate(HOME_SCREEN)
	  
	  
    }
  };

  // const getUnitIndex = (units, isGroupExist, groupIndex, thingName) => {
    // const allUnits = units.allUnits;
    // const groupUnits = units.groupUnits;

    // let unitIndex = 0;

    // if (isGroupExist) {
      // const isSameGroup = groupUnits[groupIndex].find(item => item === thingName);
      // const unit = allUnits[thingName];

      // if (isSameGroup && unit) {
        // reuse
        // unitIndex = unit.unitIndex;
      // } else {
        // Need to compute the highest unit index based within the same group
        // unitIndex =
          // groupUnits[groupIndex].reduce((acc, key) => {
            // const curUnitIndex = Number.parseInt(allUnits[key].unitIndex, 10);
            // return acc > curUnitIndex ? acc : curUnitIndex;
          // }, -1) + 1;
      // }
    // }
    // return unitIndex;
  // }

  return (
    <View style={styles.container}>
      <Text>{"\n"}Almost done! Go to your phone's Wi-Fi setting and disconnect the Adapter's Wi-Fi and connect to your Wi-Fi network.</Text>
      <TouchableOpacity onPress={() => bindBRPmasterinfo(brpThingName, brpThingUUID, userEmail, units)}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Continue</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

BrpPairingInstruct5.propTypes = {
  getAllUnits: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  // getAllUnits: (email, mqttConfig) => {
    // dispatch(units.getAllUnits({ email, mqttConfig }));
  // },
  getAllUnits: (email) => {
    dispatch(units.getAllUnits({ email }));
  },
});

const mapStateToProps = state => ({
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BrpPairingInstruct5);