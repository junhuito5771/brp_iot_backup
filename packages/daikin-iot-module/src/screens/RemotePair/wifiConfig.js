import React, { createRef, useState } from 'react';
import {
  View,
  Keyboard,
  TouchableWithoutFeedback,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, pairDevice, BLEService } from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  widthPercentage,
  StringHelper,
  passwordPreValidator,
  NavigationService,
} from '@module/utility';
import { withFormik } from 'formik';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  FormikTextInput,
  FormikButton,
  Loading,
  Text,
  CountdownText,
  Colors,
  HeaderText,
  HeaderBackButton,
  TouchableIcon,
} from '@module/daikin-ui';

import wifiConfigValidator from './wifiConfigValidator';

import {
  DEVICE_CONFIG_SCREEN,
  REMOTE_SCREEN,
  SSID_SELECTION_SCREEN,
} from '../../constants/routeNames';

const {
  withBleManager,
  checkBluetoothAndLocation,
  generateRandomString,
} = BLEService;

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container: {
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
  },
  marginTop: {
    marginTop: 50,
  },
  textSpaced: {
    marginBottom: 10,
  },
  errorContainer: {
    flexDirection: 'row',
  },
  errorText: {
    color: Colors.red,
    paddingVertical: 5,
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
});

const WifiConfig = ({
  loadingStatus,
  loadingText,
  success,
  error,
  clearIoTStatus,
  route,
  values,
}) => {
  const navigation = useNavigation();

  const [canSubmit, setCanSubmit] = useState(true);
  const passwordInputRef = createRef(null);
  const [ssidInput, setSSIDInput] = useState('');

  const { ssid: selectedSSID } = route.params;

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Wi-Fi Configuration</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  React.useEffect(() => {
    const { isReconnect } = route.params;
    if (success && isReconnect) {
      // go back to the remote unit screen
      NavigationService.navigate(REMOTE_SCREEN);
    } else if (success) {
      NavigationService.navigate(DEVICE_CONFIG_SCREEN, {
        thingName: route.params.thingName,
        thingType: route.params.thingType,
        randomKey: route.params.randomKey,
      });
    }
  }, [success, error]);

  React.useEffect(() => clearIoTStatus, []);

  React.useEffect(() => {
    if (selectedSSID && ssidInput !== selectedSSID) {
      setSSIDInput(selectedSSID);
    }
  }, [selectedSSID]);

  React.useEffect(() => {
    setCanSubmit(!error);
  }, [error]);

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={styles.container}>
          <Text small style={styles.textSpaced}>
            Enter SSID and password to send the wifi configuration to the unit
            module for pairing
          </Text>
          <FormikTextInput
            name="ssid"
            placeholder="SSID"
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            forceValue={ssidInput}
            // rightComponent={
            //   // <TouchableIcon
            //   //   source={require('../../shared/assets/wifiScan/wifiScan.png')}
            //   //   onPress={() => {
            //   //     NavigationService.navigate(SSID_SELECTION_SCREEN, {
            //   //       defaultSSID: values.ssid,
            //   //     });
            //   //   }}
            //   // />
            // }
            onSubmitEditing={() => passwordInputRef.current.focus()}
          />
          <FormikTextInput
            name="password"
            placeholder="Password"
            textContentType="password"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            forwardRef={passwordInputRef}
            validator={passwordPreValidator}
            submitOnEditEnd
          />
          <FormikButton
            secondary
            style={styles.marginTop}
            disabled={!canSubmit}>
            Connect
          </FormikButton>
          {!!error && (
            <CountdownText
              countDown={10}
              style={styles.errorText}
              desc={error}
              onFinish={() => {
                setCanSubmit(true);
              }}
            />
          )}
          <Loading visible={loadingStatus > 0}>{loadingText}</Loading>
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

WifiConfig.propTypes = {
  loadingText: PropTypes.string,
  loadingStatus: PropTypes.number,
  success: PropTypes.string,
  error: PropTypes.string,
  clearIoTStatus: PropTypes.func,
  route: PropTypes.object,
  values: PropTypes.object,
};

const mapStateToProps = state => ({
  success: state.pairDevice.success,
  error: state.pairDevice.error,
  loadingText: state.pairDevice.loadingText,
  loadingStatus: state.pairDevice.loadingStatus,
  lastSavedSSID: state.pairDevice.lastSavedSSID,
});

const mapDispatchToProps = dispatch => ({
  checkIoTStatus: checkIOTParams => {
    dispatch(pairDevice.checkIOTStatus(checkIOTParams));
  },
  updateProgress: progress => {
    dispatch(
      pairDevice.setIoTStatusLoading({
        loadingStatus: progress.status,
        loadingText: progress.text,
      })
    );
  },
  setIoTErrorStatus: error => {
    dispatch(
      pairDevice.setIoTStatus({
        messageType: 'error',
        message: error,
      })
    );
  },
  clearIoTStatus: () => {
    dispatch(pairDevice.clearIoTStatus());
  },
  createUnitInIoT: thingName => {
    dispatch(pairDevice.createUnitInIoT(thingName));
  },
});

const formikWifiConfig = withFormik({
  validate: wifiConfigValidator,
  mapPropsToValues: ({ ssid, password, route, lastSavedSSID }) => {
    const { connectedSSID } = route.params;

    const finalSSID = ssid || connectedSSID || lastSavedSSID;

    return { ssid: finalSSID, password };
  },
  handleSubmit: async (values, formikBag) => {
    try {
      const randomKey = await generateRandomString();
      const { thingName } = formikBag.props.route.params;
      const { isReconnect } = formikBag.props.route.params;
      const { factoryKey } = formikBag.props.route.params;

      // Remove the smart punctuation for the iOS 11
      const ssid = StringHelper.removeSmartPunc(values.ssid);
      const password = StringHelper.removeSmartPunc(values.password);

      const message = `<{"thingName":"${thingName}","action":"bleconfig","ssid":"${ssid}","password":"${password}","setkey":"${randomKey}"}>`;
      const messageConfig = {
        deviceName: thingName,
        serviceID: '18aa',
        charID: '2bb2',
        message,
      };

      formikBag.props.navigation.setParams({ randomKey });
      Keyboard.dismiss();
      formikBag.props.clearIoTStatus();

      const { isAllowed, errorMsg } = await checkBluetoothAndLocation();
      if (isAllowed) {
        formikBag.props.checkIoTStatus({
          thingName,
          isReconnect,
          factoryKey,
          ssid,
          sendMessage: notifyID =>
            formikBag.props.bleManager.sendMessage({
              ...messageConfig,
              notifyID,
            }),
          randomKey,
        });
      }

      if (errorMsg) {
        formikBag.props.setIoTErrorStatus(errorMsg);
      }
    } catch (error) {
      formikBag.props.setIoTErrorStatus(error.message);
      formikBag.props.updateProgress({
        status: 0,
        text: '',
      });
    }
  },
})(WifiConfig);

const bleWifiConfig = withBleManager({
  maxScanTimeOut: 10,
  canDuplicate: false,
  updateProgress: (progress, props) => {
    props.updateProgress(progress);
  },
})(formikWifiConfig);

const connectedWifiConfig = connect(
  mapStateToProps,
  mapDispatchToProps
)(bleWifiConfig);

export default connectedWifiConfig;
