import React, { createRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Keyboard,
  TouchableWithoutFeedback,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {
  heightPercentage,
  widthPercentage,
  NavigationService,
  generalInputPreValidator,
} from '@module/utility';
import {
  connect,
  deviceConfig as deviceConfigAction,
} from '@daikin-dama/redux-iot';
import { withFormik } from 'formik';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  FormikTextInput,
  FormikButton,
  FormikSelector,
  Text,
  HeaderText,
  Colors,
  HeaderBackButton,
} from '@module/daikin-ui';

import deviceConfigValidator from './deviceConfigValidator';

import {
  DEVICE_ICON_CONFIG_SCREEN,
  SELECT_GROUP_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container: {
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
  },
  marginTop: {
    marginTop: 50,
  },
  textSpaced: {
    marginBottom: 10,
  },
});

const DeviceConfig = ({ setSelectedGroup, selectedGroup }) => {
  const groupInputRef = createRef(null);
  const [groupInput, setGroupInput] = useState(null);
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Name Your Unit</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  React.useEffect(() => {
    setSelectedGroup(null);
  }, []);

  const onChangeText = React.useCallback(
    text => {
      if (selectedGroup) {
        setSelectedGroup(null);
      }
      setGroupInput(text);
    },
    [selectedGroup, groupInput]
  );

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={styles.container}>
          <Text small style={styles.textSpaced}>
            Enter the unit name and group to complete the pairing process
          </Text>
          <FormikTextInput
            name="name"
            placeholder="Unit Name"
            validator={generalInputPreValidator}
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            onSubmitEditing={() => groupInputRef.current.focus()}
          />
          <FormikSelector
            name="group"
            placeholder="Unit Group"
            validator={generalInputPreValidator}
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            forwardRef={groupInputRef}
            submitOnEditEnd
            onSelectorPress={() =>
              NavigationService.navigate(SELECT_GROUP_SCREEN, {
                currentValue: selectedGroup || groupInput,
              })
            }
            onChangeText={onChangeText}
            selectedValue={selectedGroup}
          />
          <FormikButton secondary style={styles.marginTop}>
            Next
          </FormikButton>
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

const formikDeviceConfig = withFormik({
  validate: deviceConfigValidator,
  mapPropsToValues: ({ name, group }) => ({ name, group }),
  handleSubmit: (values, formikBag) => {
    const deviceConfigParams = {
      deviceName: values.name,
      deviceGroup: values.group,
      thingName: formikBag.props.route.params.thingName,
      thingType: formikBag.props.route.params.thingType,
      randomKey: formikBag.props.route.params.randomKey,
    };

    NavigationService.navigate(DEVICE_ICON_CONFIG_SCREEN, {
      deviceConfigParams,
    });
  },
})(DeviceConfig);

const mapStateToProps = state => ({
  selectedGroup: state.deviceConfig.selectedGroup,
});

const mapDispatchToProps = dispatch => ({
  setSelectedGroup: selectedGroup =>
    dispatch(deviceConfigAction.setSelectedGroup(selectedGroup)),
});

DeviceConfig.propTypes = {
  setSelectedGroup: PropTypes.func,
  selectedGroup: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToProps)(formikDeviceConfig);
