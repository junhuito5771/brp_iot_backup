import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import Colors from '../../shared/Colors';
import { widthPercentage, heightPercentage, NavigationService } from '@module/utility';
import { BRP_PAIRING_INSTRUCT2_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: widthPercentage(5),
    fontSize: 15,
  },
  marginTopFive: {
    marginTop: heightPercentage(5),
  },
  image: {   // brp_instruct_1.png
    marginTop: heightPercentage(5),
    width: '100%',
    height: '40%',
  },
  button: {
    marginTop: heightPercentage(5),
    borderWidth: 1,
    borderColor: Colors.blue,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 40,
    marginVertical: 5,
  },
  buttonText: {
    color: Colors.blue,
    fontWeight: 'bold',
  }
});

const BrpPairingInstruct = ({ route, navigation }) => {
  //const brpThingName = navigation.getParam('thingName');
  const brpThingName = route.params.thingName;
  console.log("=> => =>");
  console.log("=> => =>");
  console.log("=> => =>");
  console.log("=> => =>");
  console.log("=> => =>");
  console.log("=> => =>");
  console.log("=> => =>", route.params.thingName);
  console.log(navigation);
  return (
    <View style={styles.container}>
      <Text>{"\n"}Go to your phone's Wi-Fi setting and connect to the Adapter's Wi-Fi.{"\n"}</Text>
      <Text>Please refer to the Wi-Fi details located at the back of the Adapter.</Text>
      <Image
        source={require('../../shared/images/brp/brp_instruct_1.png')}
        style={styles.image}
      />
      <TouchableOpacity onPress={() => NavigationService.navigate(BRP_PAIRING_INSTRUCT2_SCREEN, { thingName: brpThingName })}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Continue</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default BrpPairingInstruct;



