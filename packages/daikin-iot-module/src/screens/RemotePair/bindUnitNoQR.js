import React, { createRef, useState } from 'react';
import { View, Keyboard, SafeAreaView, StyleSheet, Alert } from 'react-native';
import PropTypes from 'prop-types';
import { connect, pairDevice, BLEService } from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  widthPercentage,
  StringHelper,
  ScaleText,
  NavigationService,
  generalInputPreValidator,
  passwordPreValidator,
} from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { withFormik } from 'formik';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import {
  FormikTextInput,
  FormikButton,
  Loading,
  Text,
  TouchableIcon,
  CountdownText,
  Colors,
  HeaderText,
  HeaderBackButton,
} from '@module/daikin-ui';

import bindNoQRValidator from './bindUnitNoQRValidator';

import { SSID_SELECTION_SCREEN, HOME_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  container: {
    flex: 1,
    paddingTop: heightPercentage(3),
    paddingHorizontal: widthPercentage(5),
  },
  marginTop: {
    marginTop: 10,
  },
  textSpaced: {
    marginBottom: 10,
    textAlign: 'justify',
  },
  errorContainer: {
    flexDirection: 'row',
  },
  errorText: {
    color: Colors.red,
    paddingVertical: 5,
  },
  textPoint: {
    textAlign: 'justify',
  },
  textPointContent: {
    textAlign: 'justify',
    flex: 1,
  },
  pointContainer: {
    paddingLeft: 5,
    flexDirection: 'row',
    marginBottom: 5,
  },
  flexGrow: {
    flexGrow: 1,
  },
  infoContainer: {
    marginBottom: 10,
  },
  nestedTextPointContent: {
    flex: 1,
  },
  nestedText: {
    fontSize: ScaleText(13),
  },
  button: {
    marginBottom: 20,
  },
});

const BindUnitNoQR = ({
  loadingStatus,
  loadingText,
  success,
  error,
  clearIoTStatus,
  route,
  values,
  cancelBindNoQR,
}) => {
  const navigation = useNavigation();

  const passwordInputRef = createRef(null);
  const unitNameInputRef = createRef(null);
  const [canSubmit, setCanSubmit] = useState(true);
  const [ssidInput, setSSIDInput] = useState('');

  const selectedSSID = route.params.ssid;

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>More options</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  React.useEffect(() => {
    if (success) {
      NavigationService.navigate(HOME_SCREEN);
    }
  }, [success]);

  React.useEffect(() => {
    BLEService.start();

    return clearIoTStatus;
  }, []);

  React.useEffect(() => {
    if (selectedSSID && ssidInput !== selectedSSID) {
      setSSIDInput(selectedSSID);
    }
  }, [selectedSSID]);

  React.useEffect(() => {
    setCanSubmit(!error);
  }, [error]);

  const handleNoQRBindCancel = () => {
    Alert.alert(
      'Cancel pairing',
      'Are you sure you want to cancel the pairing?',
      [
        {
          text: 'No',
        },
        {
          text: 'Yes',
          onPress: cancelBindNoQR,
        },
      ]
    );
  };

  return (
    <SafeAreaView style={styles.flex}>
      <KeyboardAwareScrollView
        enableOnAndroid
        contentContainerStyle={styles.flexGrow}
        extraScrollHeight={40}>
        <View style={styles.container}>
          <View style={styles.infoContainer}>
            <Text small style={styles.textSpaced}>
              For proper and more convenient pairing process, please go back to
              previous page and scan the QR code located on the warranty card,
              control box or on the back of the network adaptor.
            </Text>
            <Text small style={styles.textSpaced}>
              Please follow the steps below to initiate pairing without QR code:
            </Text>
            <View style={styles.pointContainer}>
              <Text small style={styles.textPoint}>
                1.
              </Text>
              <Text small style={styles.textPointContent}>
                Please stand within 1 meter from the air-conditioner.
              </Text>
            </View>
            <View style={styles.pointContainer}>
              <Text small>2.</Text>
              <View style={styles.nestedTextPointContent}>
                <Text style={styles.nestedText}>
                  With power supply on, using your wireless remote control, set
                  your air-conditioner to:
                </Text>
                <Text style={styles.nestedText}>a. COOL mode</Text>
                <Text style={styles.nestedText}>
                  b. Set temperature at 30 degree celsius
                </Text>
                <Text style={styles.nestedText}>
                  c. Fan mode at Medium speed
                </Text>
                <Text style={styles.nestedText}>d. Swing mode to Auto</Text>
              </View>
            </View>
            <View style={styles.pointContainer}>
              <Text small style={styles.textPoint}>
                3.
              </Text>
              <Text small style={styles.textPointContent}>
                Switch off the power supply then wait for 5 seconds, then switch
                power supply back on.
              </Text>
            </View>
          </View>
          <FormikTextInput
            name="ssid"
            placeholder="SSID"
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            forceValue={ssidInput}
            rightComponent={
              <TouchableIcon
                source={require('../../shared/assets/wifiScan/wifiScan.png')}
                onPress={() => {
                  NavigationService.navigate(SSID_SELECTION_SCREEN, {
                    defaultSSID: values.ssid,
                  });
                }}
              />
            }
            onSubmitEditing={() => passwordInputRef.current.focus()}
          />
          <FormikTextInput
            name="password"
            placeholder="Password"
            textContentType="password"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            forwardRef={passwordInputRef}
            validator={passwordPreValidator}
            onSubmitEditing={() => unitNameInputRef.current.focus()}
          />
          <FormikTextInput
            name="unitName"
            placeholder="Unit Name"
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            validator={generalInputPreValidator}
            submitOnEditEnd
            forwardRef={unitNameInputRef}
          />
          <FormikButton
            secondary
            style={styles.marginTop}
            disabled={!canSubmit}>
            Connect
          </FormikButton>
          {!!error && (
            <CountdownText
              countDown={10}
              style={styles.errorText}
              desc={error}
              onFinish={() => {
                setCanSubmit(true);
              }}
            />
          )}
          <Loading visible={loadingStatus > 0} onCancel={handleNoQRBindCancel}>
            {loadingText}
          </Loading>
        </View>
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

BindUnitNoQR.propTypes = {
  loadingText: PropTypes.string,
  loadingStatus: PropTypes.number,
  success: PropTypes.string,
  error: PropTypes.string,
  clearIoTStatus: PropTypes.func,
  route: PropTypes.object,
  values: PropTypes.object,
  cancelBindNoQR: PropTypes.func,
};

const mapStateToProps = state => ({
  success: state.pairDevice.success,
  error: state.pairDevice.error,
  loadingText: state.pairDevice.loadingText,
  loadingStatus: state.pairDevice.loadingStatus,
  lastSavedSSID: state.pairDevice.lastSavedSSID,
});

const mapDispatchToProps = dispatch => ({
  updateProgress: progress => {
    dispatch(
      pairDevice.setIoTStatusLoading({
        loadingStatus: progress.status,
        loadingText: progress.text,
      })
    );
  },
  setIoTErrorStatus: error => {
    dispatch(
      pairDevice.setIoTStatus({
        messageType: 'error',
        message: error,
      })
    );
  },
  clearIoTStatus: () => {
    dispatch(pairDevice.clearIoTStatus());
  },
  submitBindNoQR: (ssid, password, unitName) => {
    dispatch(pairDevice.submitBindNoQR({ ssid, password, unitName }));
  },
  cancelBindNoQR: () => {
    dispatch(pairDevice.cancelBindNoQR());
  },
});

const formikBindUnitNoQR = withFormik({
  validate: bindNoQRValidator,
  mapPropsToValues: ({ ssid, password, route, lastSavedSSID, unitName }) => {
    const { connectedSSID } = route.params;

    const finalSSID = ssid || connectedSSID || lastSavedSSID;

    return { ssid: finalSSID, password, unitName };
  },
  handleSubmit: async (values, formikBag) => {
    try {
      Keyboard.dismiss();

      // Remove the smart punctuation for the iOS 11
      const ssid = StringHelper.removeSmartPunc(values.ssid);
      const password = StringHelper.removeSmartPunc(values.password);
      const unitName = StringHelper.removeSmartPunc(values.unitName);

      formikBag.props.submitBindNoQR(ssid, password, unitName);
    } catch (error) {
      formikBag.props.setIoTErrorStatus(error.message);
      formikBag.props.updateProgress({
        status: 0,
        text: '',
      });
    }
  },
})(BindUnitNoQR);

const connectedBindUnitNoQR = connect(
  mapStateToProps,
  mapDispatchToProps
)(formikBindUnitNoQR);

export default connectedBindUnitNoQR;
