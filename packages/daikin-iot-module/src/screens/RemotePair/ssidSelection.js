import React, { useState, useEffect, useRef } from 'react';
import {
  View,
  SafeAreaView,
  TouchableOpacity,
  StyleSheet,
  Image,
  RefreshControl,
  FlatList,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, scanSSID } from '@daikin-dama/redux-iot';
import { widthPercentage, NavigationService } from '@module/utility';

import { Text, Colors, HeaderText, HeaderBackButton } from '@module/daikin-ui';

import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { WIFI_CONFIG_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollViewContainer: {
    flexGrow: 1,
  },
  row: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
  },
  touchable: {
    width: '100%',
  },
  rowInner: {
    width: '100%',
    paddingLeft: 40,
    paddingRight: 25,
    paddingVertical: 15,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: widthPercentage(6),
  },
  rowRightContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tickIcon: {
    marginLeft: 10,
    height: 17,
    width: 17,
    resizeMode: 'contain',
  },
  emptySelection: {
    marginLeft: 10,
    height: 17,
    width: 17,
  },
  errorContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyContainer: {
    marginTop: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 50,
  },
  emptyText: {
    textAlign: 'center',
  },
});

function getWifiIconByRSSI(rssi) {
  if (rssi >= -30) {
    return require('../../shared/assets/wifiScan/wifiFull.png');
  }
  if (rssi >= -67) {
    return require('../../shared/assets/wifiScan/wifiMid.png');
  }

  return require('../../shared/assets/wifiScan/wifiLow.png');
}

const SSIDSelection = ({
  route,
  ssidList,
  isLoading,
  runScanSSID,
  clearScanStatus,
}) => {
  const navigation = useNavigation();
  const { defaultSSID } = route.params;

  const [selectedSSID, setSelectedSSID] = useState(defaultSSID);
  const flatListRef = useRef();

  const handleOnSelect = label => {
    setSelectedSSID(label);
    navigation.navigate(WIFI_CONFIG_SCREEN, { ssid: label });
  };

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Select SSID</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  useEffect(() => {
    runScanSSID();
    return clearScanStatus;
  }, []);

  useEffect(() => {
    if (isLoading && Platform.OS === 'ios' && flatListRef.current) {
      setTimeout(
        () =>
          flatListRef.current.scrollToOffset({ offset: -160, animated: true }),
        500
      );
    }
  }, [isLoading]);

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        ref={flatListRef}
        data={ssidList}
        contentContainerStyle={styles.scrollViewContainer}
        keyExtractor={({ ssid }) => ssid}
        ListEmptyComponent={
          !isLoading && (
            <View style={styles.emptyContainer}>
              <Text style={styles.emptyText}>
                Unable to find any nearby SSID. Please try to refresh again
              </Text>
            </View>
          )
        }
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={runScanSSID}
            size={18}
            title="Scanning nearby SSID"
            titleColor={Colors.lightWarmGrey}
          />
        }
        renderItem={({ item }) => {
          const { ssid: label, rssi } = item;
          return (
            <View key={label} style={styles.row}>
              <TouchableOpacity
                style={styles.touchable}
                onPress={() => handleOnSelect(label)}>
                <View style={styles.rowInner}>
                  <Text>{label}</Text>
                  <View style={styles.rowRightContainer}>
                    <Image source={getWifiIconByRSSI(rssi)} />
                    {selectedSSID === label ? (
                      <Image
                        source={require('../../shared/assets/general/submitTick.png')}
                        style={styles.tickIcon}
                      />
                    ) : (
                      <View style={styles.emptySelection} />
                    )}
                  </View>
                </View>
              </TouchableOpacity>
            </View>
          );
        }}
      />
    </SafeAreaView>
  );
};

SSIDSelection.propTypes = {
  runScanSSID: PropTypes.func,
  clearScanStatus: PropTypes.func,
  ssidList: PropTypes.array,
  isLoading: PropTypes.bool,
  route: PropTypes.object,
};

SSIDSelection.defaultProps = {
  ssidList: [],
};

const mapDispatchToProps = dispatch => ({
  runScanSSID: () => dispatch(scanSSID.scanSSID()),
  clearScanStatus: () => {
    dispatch(scanSSID.clearScanSSIDStatus());
    dispatch(scanSSID.cancelScanSSID());
  },
});

const mapStateToProps = ({ scanSSID: { ssidList, isLoading } }) => ({
  ssidList,
  isLoading,
});

export default connect(mapStateToProps, mapDispatchToProps)(SSIDSelection);
