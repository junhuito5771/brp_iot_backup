import React from 'react';
import PropTypes from 'prop-types';
import { connect, deviceConfig } from '@daikin-dama/redux-iot';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import { Colors, Text, HeaderText, HeaderBackButton } from '@module/daikin-ui';

import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { NavigationService } from '@module/utility';

const styles = StyleSheet.create({
  rowContainer: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingHorizontal: 25,
    paddingVertical: 15,
  },
  touchable: {
    height: '100%',
    width: '100%',
  },
  row: {
    flexDirection: 'row',
  },
  groupLabel: {
    flexGrow: 1,
  },
  icon: {
    height: 17,
    width: 17,
    resizeMode: 'contain',
    justifyContent: 'flex-end',
  },
});

const SelectGroup = ({ route, groups, setSelectedGroup }) => {
  const navigation = useNavigation();
  const { currentValue } = route.params;

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Select Group</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const groupsArray = React.useMemo(
    () =>
      Object.values(groups)
        .filter(group => group.groupIndex >= 0)
        .map(group => ({
          name: group.groupName,
        })),
    [groups]
  );

  const handleSetSelectedGroup = React.useCallback(
    groupName => {
      setSelectedGroup(groupName);
      navigation.goBack();
    },
    [setSelectedGroup]
  );

  return (
    <ScrollView>
      {groupsArray.map(group => (
        <View key={group.name} style={styles.rowContainer}>
          <TouchableOpacity
            onPress={() => handleSetSelectedGroup(group.name)}
            style={styles.touchable}
            hitSlop={{
              top: 10,
              bottom: 10,
            }}>
            <View style={styles.row}>
              <Text style={styles.groupLabel}>{group.name}</Text>
              {currentValue === group.name && (
                <Image
                  source={require('../../shared/assets/general/submitTick.png')}
                  style={styles.icon}
                />
              )}
            </View>
          </TouchableOpacity>
        </View>
      ))}
    </ScrollView>
  );
};

const mapStateToProps = state => ({
  groups: state.units.groupInfo.groups,
});

const mapDispatchToProps = dispatch => ({
  setSelectedGroup: selectedGroup =>
    dispatch(deviceConfig.setSelectedGroup(selectedGroup)),
});

SelectGroup.propTypes = {
  route: PropTypes.object,
  setSelectedGroup: PropTypes.func,
  groups: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectGroup);
