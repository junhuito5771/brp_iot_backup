import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity } from 'react-native';
import Colors from '../../shared/Colors';
import { widthPercentage, heightPercentage, NavigationService } from '@module/utility';
import { BRP_PAIRING_INSTRUCT3_SCREEN, BRP_PAIRING_INSTRUCT5_SCREEN } from '../../constants/routeNames';
import PropTypes from 'prop-types';
import { connect } from '@module/redux';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: widthPercentage(5),
    fontSize: 15,
  },
  marginTopFive: {
    marginTop: heightPercentage(5),
  },
  button: {
    marginTop: heightPercentage(5),
    borderWidth: 1,
    borderColor: Colors.blue,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 40,
    marginVertical: 5,
  },
  buttonText: {
    color: Colors.blue,
    fontWeight: 'bold',
  },
  image: {  // brp_instruct_3.png
    marginTop: heightPercentage(5),
    width: '100%',
    height: '30%',
  },
});

const BrpPairingInstruct4 = ({ route, navigation, userID, userEmail, units }) => {
  const brpThingName = route.params.thingName;
  const brpThingKey = route.params.thingKey;
  const brpThingUUID = route.params.thingUUID;
  
  console.log(userEmail);
  console.log(userEmail);
  console.log(userID);
  console.log(navigation);
  console.log(route);

  return (
    <View style={styles.container}>
      <Text>
        {"\n"}The Wi-Fi adapter is attempting to connect to your Wi-Fi. The "RUN" LED light will switch from Blinking to on continuously within 2 minutes if it is connected.
        {"\n"}{"\n"}If it is still blinking after 2 minutes, please press "Blinking" to repeat the pairing process. Otherwise, press "Continuously ON" to proceed for pairing. {"\n"}
      </Text>
      <Image
        source={require('../../shared/images/brp/brp_instruct_3.png')}
        style={styles.image}
      />
      <Text>If the RUN LED keeps blinking, the adapter failed to connect to the network. </Text>
      <TouchableOpacity onPress={() => NavigationService.navigate(BRP_PAIRING_INSTRUCT3_SCREEN, { thingName: brpThingName, thingKey: brpThingKey, thingUUID: brpThingUUID })}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Blinking</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => NavigationService.navigate(BRP_PAIRING_INSTRUCT5_SCREEN, { thingName: brpThingName, thingUUID: brpThingUUID, userEmail: userEmail, units: units })}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Continously ON</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const mapStateToProps = state => ({
  userEmail: state.user.profile.email,
  units: state.units,
  userID: state.user
});

BrpPairingInstruct4.propTypes = {
  userEmail: PropTypes.string,
  units: PropTypes.object,
  userID: PropTypes.object,
};

export default connect(
  mapStateToProps
)(BrpPairingInstruct4);