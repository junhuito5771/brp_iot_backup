import React, { useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput, Alert, NativeModules } from 'react-native';
import Colors from '../../shared/Colors';
import { widthPercentage, heightPercentage, NavigationService } from '@module/utility';
import { BRP_PAIRING_INSTRUCT4_SCREEN } from '../../constants/routeNames';
//import { fetch } from 'react-native-ssl-pinning';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
    paddingHorizontal: widthPercentage(5),
    fontSize: 15,
  },
  marginTopFive: {
    marginTop: heightPercentage(5),
  },
  input: {
    borderColor: 'black',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.lightCoolGrey,
    paddingHorizontal: 10,
  },
  inputPassword: {
    marginTop: heightPercentage(2),
    borderColor: 'black',
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: Colors.lightCoolGrey,
    paddingHorizontal: 10,
  },
  button: {
    marginTop: heightPercentage(5),
    borderWidth: 1,
    borderColor: Colors.blue,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    height: 40,
    marginVertical: 5,
  },
  buttonText: {
    color: Colors.blue,
    fontWeight: 'bold',
  },
});

const showError = () => {
  Alert.alert('Error', "Connection to BRP adapter is unsuccessful. Please ensure you are connected to the adapter's network. ", [
    { text: 'OK' },
  ]);
};

const showErrorMessage = (e) => {
  Alert.alert('Error', e, [
    { text: 'OK' },
  ]);
};

const pairWifi = async (brpThingName, brpThingKey, brpThingUUID, ssid, password) => {
  if (ssid.length == 0 || password.length == 0) {
    showErrorMessage("Wi-Fi credentials cannot be empty")
  }
  else {
	  
	const { BrpModule } = NativeModules;
	BrpModule.setWifiSetting( ssid, "mixed", password, (result) => {
		
	  console.log("RESULT : ", result);
	  if(result!=null){
		  console.log("SUCCESS!");
		  NavigationService.navigate(BRP_PAIRING_INSTRUCT4_SCREEN, { thingName: brpThingName, thingKey: "0135000093663", thingUUID: "b934deb8ee5b453c976bd2ecc9ba7d8f" })
	  }else{
		  showErrorMessage("Something went error");
	  }
	});
	  /*
    fetch("https://192.168.127.1/common/set_wifi_setting?ssid=" + ssid + "&security=mixed&key=" + password, {
      method: "GET",
      disableAllSecurity: true,
      headers: {
        "X-Daikin-uuid": brpThingUUID
      }
    })
      .then(response => {
        console.log(response)
        result = response.bodyString.split("=")
        if (response.status == 200 && result[1] == "OK") {

          fetch("https://192.168.127.1/common/start_wifi_scan", {
            method: "GET",
            disableAllSecurity: true,
            headers: {
              "X-Daikin-uuid": brpThingUUID
            }
          })
            .then(response => {
              console.log(response)

              fetch("https://192.168.127.1/common/start_wifi_connection?key=" + brpThingKey, {
                method: "GET",
                disableAllSecurity: true,
                headers: {
                  "X-Daikin-uuid": brpThingUUID
                }
              })
                .then(response => {
                  console.log(response)
                  result = response.bodyString.split(",")
                  if (response.status == 200 && result[0].split("=")[1] == "OK") {
                    NavigationService.navigate(BRP_PAIRING_INSTRUCT4_SCREEN, { thingName: brpThingName, thingKey: brpThingKey, thingUUID: brpThingUUID })
                  }
                  else {
                    showError();
                  }
                })
                .catch(err => {
                  console.log(err);
                  // showError();
                  showErrorMessage(err);
                })
            })
            .catch(err => {
              console.log(err);
              // showError();
              showErrorMessage(err);
            })
        }
        else {
          showError();
        }
      })
      .catch(err => {
        console.log(err);
        // showError();
        showErrorMessage(err);
      })
	  */
  }

};

const BrpPairingInstruct3 = ({ route, navigation }) => {
  const brpThingName = route.params.thingName;
  const brpThingKey = route.params.thingKey;
  const brpThingUUID = route.params.thingUUID;

  const [ssid, setTextSSID] = useState('');
  const [password, setTextPassword] = useState('');

  return (
    <View style={styles.container}>
      <Text>{"\n"}Enter your network SSID and password to send the wifi configuration to the unit module for pairing {"\n"}</Text>
      <TextInput
        style={styles.input}
        placeholder="Wi-Fi SSID"
        defaultValue="DSRSB2-1303 2.4Ghz"
        onChangeText={ssid => setTextSSID(ssid)}
      />
      <TextInput
        style={styles.inputPassword}
        placeholder="Wi-Fi Password"
        defaultValue="DSR112233"
        onChangeText={password => setTextPassword(password)}
      />
      <TouchableOpacity onPress={() => pairWifi(brpThingName, brpThingKey, brpThingUUID, ssid, password)}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Continue</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default BrpPairingInstruct3;
