import React from 'react';
import {
  View,
  StyleSheet,
  Image,
  Alert,
  Text,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import SplashScreen from 'react-native-splash-screen';
import Toast from 'react-native-root-toast';
import {
  viewportWidth,
  viewportHeight,
  NetworkHelper,
  NavigationService,
} from '@module/utility';
import { connect, bindGuestDevice, QRCodeHelper } from '@daikin-dama/redux-iot';
import Config from 'react-native-config';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  Loading,
  Colors,
  HeaderText,
  HeaderBackButton,
} from '@module/daikin-ui';

import {
  WIFI_CONFIG_SCREEN,
  HOME_SCREEN,
  BIND_UNIT_NO_QR_SCREEN,
} from '../../constants/routeNames';

let CameraComponent = null;

// Dynamic import the library based on provider
if (Config.PROVIDER === 'HMS') {
  CameraComponent = require('@module/react-native-hms-camera').default;
} else {
  CameraComponent = require('react-native-camera').RNCamera;
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  frameContainer: {
    flexDirection: 'row',
    width: viewportWidth,
  },
  cameraStyle: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  overlayContainer: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  topOverlay: {
    flex: 1,
    backgroundColor: Colors.transparentBlack,
  },
  bottomOverlay: {
    flex: 1,
    backgroundColor: Colors.transparentBlack,
    alignItems: 'center',
  },
  leftAndRightOverlay: {
    flex: 1,
    backgroundColor: Colors.transparentBlack,
  },
  text: {
    fontSize: 12,
    color: 'white',
    marginTop: 10,
  },
  moreOptText: {
    fontSize: 12,
    color: Colors.white,
    marginTop: 15,
    textDecorationLine: 'underline',
  },
});

const showToast = (message, config) => {
  let toast = Toast.show(message, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: false,
    animation: true,
    hideOnPress: true,
    delay: 0,
    onHidden: () => {
      toast.destroy();
      toast = null;
    },
    ...config,
  });
};

const QrScan = ({
  isLoading,
  success,
  error,
  submitBindGuestDevice,
  clearBindGuestStatus,
}) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>QR Scan</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  React.useEffect(() => {
    SplashScreen.hide();
    clearBindGuestStatus();

    return clearBindGuestStatus;
  }, []);

  React.useEffect(() => {
    if (success) {
      NavigationService.navigate(HOME_SCREEN);
    } else if (error) {
      Alert.alert('Error', error, [
        { text: 'OK', onPress: clearBindGuestStatus },
      ]);
    }
  }, [success, error]);

  const checkQrcodeAuth = item => {
    const { factoryKey } = item;
    const itemCode = factoryKey.slice(0, 1);

    return itemCode === 'D';
  };

  const handleQrScan = async data => {
	
	// ----- START ADDED BY JUN -----///
	// BRP072C42\rD0C5D37099D2    SAMPLE DATA QR CODE FROM BRP MODULE
    if (data.startsWith("BRP")) {
      qrBRP = data.split("\r");
      brpThingName = "Daikin_" + qrBRP[1];
	  console.log("HERE ======================>>>>>>", data);
	  console.log("HERE ======================>>>>>>", data);
	  console.log("HERE ======================>>>>>>", data);
	  console.log("HERE ======================>>>>>>", data);
      NavigationService.navigate(BRP_PAIRING_INSTRUCT_SCREEN, { thingName: brpThingName });
    } 
	else{ 
		//----- ORIGINAL CODE-----//
		if (!navigation.isFocused() || isLoading || !!error || !!success) {
		  return false;
		}

		const processQRResult = QRCodeHelper.QRHandler(
		  data,
		  Config.QRCODE_SECRET_KEY,
		  Config.QRCODE_IV
		);

		const {
		  type,
		  error: QRResultError,
		  data: qrData,
		  version,
		} = processQRResult;

		if (type === 'host' && (version === 3 || version === 2)) {
		  const itemCode = checkQrcodeAuth(qrData);
		  if (!itemCode) {
			showToast('Invalid QR Code');
			return false;
		  }
		}

		if (QRResultError) {
		  showToast(QRResultError);
		  return false;
		}
		if (qrData.unitCode && qrData.unitCode !== 'D') {
		  showToast('Invalid QR Code');
		  return false;
		}

		if (type === 'guest' && qrData.unitCode === 'D') {
		  submitBindGuestDevice(qrData, version);
		  return true;
		}

		// Only navigate to wifi config if thingName is not empty
		// and only this screen is focused
		if (qrData && qrData.thingName && navigation.isFocused()) {
		  NavigationService.navigate(WIFI_CONFIG_SCREEN, {
			...qrData,
			connectedSSID: await NetworkHelper.getSSID(),
		  });
		}
		//----- ORIGINAL -----//
	}
	// ----- END ADDED BY JUN -----///
	
    return true;
  };

  const navigateToMoreOpt = async () => {
    NavigationService.navigate(BIND_UNIT_NO_QR_SCREEN, {
      connectedSSID: await NetworkHelper.getSSID(),
    });
  };

  return (
    <View style={styles.mainContainer}>
      <Loading visible={isLoading}>Binding unit to your account...</Loading>
      <CameraComponent
        style={styles.cameraStyle}
        cameraViewDimensions={{ width: viewportWidth, height: viewportHeight }}
        rectOfInterest={{ x: 0.27, y: 0.125, width: 0.45, height: 0.75 }}
        captureAudio={false}
        barCodeTypes={[
          CameraComponent.Constants.BarCodeType.qr,
          CameraComponent.Constants.BarCodeType.datamatrix,
        ]}
        onBarCodeRead={
          navigation.isFocused() ? e => handleQrScan(e.data) : null
        }
        autoFocus
        autoFocusPointOfInterest={{ x: 0.5, y: 0.5 }}>
        <View style={styles.overlayContainer}>
          <View style={styles.topOverlay} />
          <View style={styles.frameContainer}>
            <View style={styles.leftAndRightOverlay} />
            <Image source={require('../../shared/assets/general/frame.png')} />
            <View style={styles.leftAndRightOverlay} />
          </View>
          <View style={styles.bottomOverlay}>
            <Text style={styles.text}>
              Position the QR Code within the frame to scan
            </Text>
            <TouchableOpacity onPress={navigateToMoreOpt}>
              <Text style={styles.moreOptText}>More options</Text>
            </TouchableOpacity>
          </View>
        </View>
      </CameraComponent>
    </View>
  );
};

QrScan.propTypes = {
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  submitBindGuestDevice: PropTypes.func,
  clearBindGuestStatus: PropTypes.func,
};

const mapStateToProps = ({
  bindGuestDevice: { isLoading, success, error },
}) => ({
  isLoading,
  success,
  error,
});

const mapDispatchToProps = dispatch => ({
  submitBindGuestDevice: (bindGuestDeviceParams, version) => {
    dispatch(
      bindGuestDevice.submitBindGuestDevice(bindGuestDeviceParams, version)
    );
  },
  clearBindGuestStatus: () => {
    dispatch(bindGuestDevice.clearBindGuestStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(QrScan);
