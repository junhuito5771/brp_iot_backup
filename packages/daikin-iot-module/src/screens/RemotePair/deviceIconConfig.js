import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Keyboard,
  TouchableWithoutFeedback,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {
  heightPercentage,
  widthPercentage,
  NavigationService,
} from '@module/utility';
import { connect, deviceConfig } from '@daikin-dama/redux-iot';

import { Text, Button, HeaderText, HeaderBackButton } from '@module/daikin-ui';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import UnitIcons from '../../components/UnitIcons';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingVertical: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
  },
});

const DeviceIconConfig = ({
  route,
  error,
  success,
  isLoading,
  submitDeviceConfigParams,
  clearDeviceConfigStatus,
}) => {
  const [selectedIconIndex, setSelectedIconIndex] = useState(null);
  const canSave = selectedIconIndex !== null;

  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Choose An Icon</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  useEffect(() => {
    if (success) {
      navigation.popToTop();
    }
  }, [success, error]);

  useEffect(() => clearDeviceConfigStatus, []);

  const handleOnSave = () => {
    const { deviceConfigParams } = route.params;

    submitDeviceConfigParams({
      ...deviceConfigParams,
      logo: `${selectedIconIndex}.png`,
    });
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={styles.container}>
          <UnitIcons onAfterSelect={key => setSelectedIconIndex(key)} />
          <View style={styles.buttonContainer}>
            <Button
              secondary
              style={styles.marginTop}
              disabled={!canSave || isLoading}
              isLoading={isLoading}
              onPress={handleOnSave}>
              Save
            </Button>
            {!!error && <Text error>{error}</Text>}
          </View>
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

DeviceIconConfig.propTypes = {
  route: PropTypes.object,
  error: PropTypes.string,
  success: PropTypes.string,
  submitDeviceConfigParams: PropTypes.func,
  clearDeviceConfigStatus: PropTypes.func,
  isLoading: PropTypes.bool,
};

const mapStateToProps = ({ deviceConfig: { isLoading, error, success } }) => ({
  error,
  success,
  isLoading,
});

const mapDispatchToProps = dispatch => ({
  submitDeviceConfigParams: deviceConfigParams => {
    dispatch(deviceConfig.submitDeviceConfigParams(deviceConfigParams));
  },
  clearDeviceConfigStatus: () => {
    dispatch(deviceConfig.clearDeviceConfigStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(DeviceIconConfig);
