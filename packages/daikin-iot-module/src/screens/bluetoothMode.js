import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Alert,
  RefreshControl,
  SafeAreaView,
} from 'react-native';
import { connect, bleControl, BLEService } from '@daikin-dama/redux-iot';
import { ConnectionType, NavigationService } from '@module/utility';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import {
  Colors,
  Loading,
  LastUpdateClock,
  CollapsibleBox,
  getACIconFromPath,
  HeaderText,
  HeaderBackButton,
  bleIcon,
} from '@module/daikin-ui';

import { OTHER_MORE_REMOTE_SCREEN } from '../constants/routeNames';

const { discoverAndProcessUnits } = BLEService;

const getBleIconPath = key => {
  const defaultIcon = key === 'ble.png' ? bleIcon : undefined;
  return getACIconFromPath(key, defaultIcon);
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  flexGrow: {
    flexGrow: 1,
  },
  marginRight: {
    marginRight: 25,
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
  disabledIcon: {
    tintColor: Colors.lightGrey,
  },
});

const showAlert = () => {
  Alert.alert(
    'Not authorized',
    'You do not have permission to access this unit. Please log in to the account paired with this unit first.',
    [
      {
        text: 'OK',
      },
    ]
  );
};

const renderNavigationOptions = (route, navigation) => {
  const { params } = route;

  if (params && params.refreshButtonParams) {
    const { refreshButtonParams } = params;
    return {
      title: <HeaderText>Available Units</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
      headerRight: () => (
        <View style={[styles.row, styles.marginRight]}>
          <TouchableOpacity hitSlop={styles.hitSlop} {...refreshButtonParams}>
            <Image source={require('../shared/assets/general/refresh.png')} />
          </TouchableOpacity>
        </View>
      ),
    };
  }

  return {};
};

const BluetoothDevices = ({
  bleManager,
  isLoading,
  units,
  dispatch,
  connectUnit,
  pairLoading,
  pairSuccess,
  pairError,
  connectedUnitName,
  lastUpdated,
  clearStatus,
  route,
}) => {
  const navigation = useNavigation();
  const scrollViewRef = useRef(null);
  const handleOnRefresh = () => {
    discoverAndProcessUnits(dispatch, bleManager, isLoading);
  };

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      ...renderNavigationOptions(route, navigation),
    });
  }, [navigation]);

  React.useEffect(() => {
    navigation.setParams({
      refreshButtonParams: {
        onPress: handleOnRefresh,
      },
    });

    // return () => {
    //   navigation.setParams({});
    // };
  }, [isLoading]);

  React.useEffect(() => {
    if (pairSuccess) {
      NavigationService.navigate(OTHER_MORE_REMOTE_SCREEN, {
        unit: { ACName: connectedUnitName },
        mode: ConnectionType.BLE_MDOE,
      });
    } else if (pairError) {
      Alert.alert('Error', pairError);
    }

    clearStatus();
  }, [pairSuccess, pairError]);

  const handleOnPressItem = ({ thingName, id, disallowed }) => {
    if (disallowed) {
      showAlert();
    } else {
      connectUnit(thingName, id);
    }
  };

  return (
    <SafeAreaView style={styles.flex}>
      <ScrollView
        ref={scrollViewRef}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={handleOnRefresh} />
        }>
        <LastUpdateClock time={lastUpdated} />
        <CollapsibleBox
          data={units}
          onPressItem={handleOnPressItem}
          getIconPath={getBleIconPath}
        />
      </ScrollView>
      <Loading visible={!!pairLoading}>{pairLoading}</Loading>
    </SafeAreaView>
  );
};

BluetoothDevices.propTypes = {
  bleManager: PropTypes.object,
  units: PropTypes.array,
  isLoading: PropTypes.bool,
  dispatch: PropTypes.func,
  connectUnit: PropTypes.func,
  lastUpdated: PropTypes.number,

  pairLoading: PropTypes.string,
  pairSuccess: PropTypes.string,
  pairError: PropTypes.string,
  connectedUnitName: PropTypes.string,
  clearStatus: PropTypes.func,
  route: PropTypes.object,
};

BluetoothDevices.defaultProps = {
  isLoading: false,
  pairLoading: '',
};

const mapStateToProps = state => ({
  units: Object.values(state.bleMode.units),
  isLoading: state.bleMode.isLoading,
  pairLoading: state.bleControl.pairLoading,
  pairSuccess: state.bleControl.success,
  pairError: state.bleControl.error,
  lastUpdated: state.bleMode.lastUpdated,
  connectedUnitName: state.units.allUnits[state.remote.thingName]
    ? state.units.allUnits[state.remote.thingName].ACName
    : '',
});

const mapDispatchToProps = dispatch => ({
  connectUnit: (thingName, id) => {
    dispatch(bleControl.connectBLEUnit({ thingName, id }));
  },
  clearStatus: () => dispatch(bleControl.clearBLEControlStatus()),
  dispatch,
});

const connectedBluetoothMode = connect(
  mapStateToProps,
  mapDispatchToProps
)(BluetoothDevices);

export default connectedBluetoothMode;
