import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  SafeAreaView,
  View,
  ScrollView,
  Alert,
  RefreshControl,
  findNodeHandle,
  InteractionManager,
  ActivityIndicator,
  TouchableOpacity,
  Image,
} from 'react-native';
import {
  connect,
  units,
  timer,
  selector,
  editTimer,
} from '@daikin-dama/redux-iot';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { Colors, HeaderText, NavDrawerMenu } from '@module/daikin-ui';

import { NavigationService } from '@module/utility';
import { EDIT_WEEKLY_TIMER_SCREEN } from '../../constants/routeNames';
import WeeklyTimerContainer from './WeeklyTimerContainer';

const styles = StyleSheet.create({
  flex: {
    backgroundColor: Colors.white,
    flex: 1,
  },
  container: {
    padding: 20,
  },
  marginRight20: {
    marginRight: 20,
  },
});

function runAfterAnimation(cb) {
  let called = false;
  const timeHandler = setTimeout(() => {
    called = true;
    cb();
  }, 500);

  InteractionManager.runAfterInteractions(() => {
    if (called) return;
    clearTimeout(timeHandler);
    cb();
  });
}

const renderNavigationOptions = (route, navigation) => ({
  title: <HeaderText>Weekly Timer</HeaderText>,
  headerLeft: () => <NavDrawerMenu navigation={navigation} />,
  headerRight: () => (
    <View style={styles.marginRight20}>
      <TouchableOpacity
        hitSlop={styles.hitSlop}
        {...route.params.refreshButtonParams}>
        <Image source={require('../../shared/assets/general/refresh.png')} />
      </TouchableOpacity>
    </View>
  ),
});

const WeeklyTimer = ({
  timerList,
  fetchTimerLoading,
  submitToggleTimer,
  fetchUnitsAndTimers,
  toggleLoadingList,
  setEditTimerConfig,
  setEditTimerNewConfig,
  route,
  email,
}) => {
  const navigation = useNavigation();

  const [interactionsComplete, setInteractionsComplete] = useState(false);
  const { dayIndex, scrollToKey } = route.params;
  const scrollViewRef = useRef(null);
  const timersPosition = useRef({});

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      ...renderNavigationOptions(route, navigation),
    });
  }, [navigation]);

  const setTimersPosition = ({ thingName, timerIndex, itemRef }) => {
    if (thingName && timerIndex >= 0) {
      const key = `${thingName}_${timerIndex}`;

      timersPosition.current = {
        ...timersPosition.current,
        [key]: itemRef,
      };
    }
  };

  const onRefresh = () => {
    fetchUnitsAndTimers(email);
  };

  useEffect(() => {
    runAfterAnimation(() => {
      setInteractionsComplete(true);
      onRefresh();
    });
  }, []);

  useEffect(() => {
    navigation.setParams({
      refreshButtonParams: {
        onPress: () => {
          onRefresh();
        },
      },
    });

    return () => {
      // navigation.setParams({});
    };
  }, []);

  useEffect(() => {
    if (interactionsComplete) {
      InteractionManager.runAfterInteractions(() => {
        if (scrollToKey) {
          const targetItem = timersPosition.current[scrollToKey];

          if (targetItem) {
            targetItem.measureLayout(
              findNodeHandle(scrollViewRef.current),
              (x, y) => {
                scrollViewRef.current.scrollTo({ x, y, animated: true });
              }
            );
          }
        }
      });
    }
  }, [scrollToKey, timerList, interactionsComplete]);

  const handleOnPressAdd = (curThingName, curUnit, dayNr) => {
    if (curUnit.timers.length < curUnit.timerLimit) {
      setEditTimerNewConfig(curThingName, curUnit, dayNr, email);
      NavigationService.navigate(EDIT_WEEKLY_TIMER_SCREEN);
    } else {
      Alert.alert(
        'Maximum timers reached',
        `Each unit can only create up to ${curUnit.timerLimit} timers`
      );
    }
  };

  const handleOverwriteSchedule = (type, curThingName, curTimer) => {
    const curTimerList = [curTimer];

    if (curTimer.canEdit) {
      if (type === 'edit') {
        setEditTimerConfig(curThingName, curTimerList, curTimer.day);
        NavigationService.navigate(EDIT_WEEKLY_TIMER_SCREEN);
      } else if (type === 'toogle') {
        submitToggleTimer(curThingName, curTimer.timerIndex);
      }
    } else {
      Alert.alert(
        'Unauthorized',
        'You do not have permission to overwrite the timer.\n\nPlease contact your host user to request for permission.'
      );
    }
  };

  const handleOnPressEdit = (curThingName, curTimer) => {
    handleOverwriteSchedule('edit', curThingName, curTimer);
  };

  const handleOnPressToggle = (curThingName, curTimer) => {
    handleOverwriteSchedule('toogle', curThingName, curTimer);
  };

  return (
    <SafeAreaView style={styles.flex}>
      {!interactionsComplete ? (
        <View style={styles.container}>
          <ActivityIndicator size="large" />
        </View>
      ) : (
        <ScrollView
          ref={scrollViewRef}
          refreshControl={
            <RefreshControl
              refreshing={fetchTimerLoading}
              onRefresh={onRefresh}
            />
          }>
          <WeeklyTimerContainer
            data={timerList}
            onPressAdd={handleOnPressAdd}
            onPressEdit={handleOnPressEdit}
            dayIndex={dayIndex}
            onPressToggle={handleOnPressToggle}
            toggleLoadingList={toggleLoadingList}
            navigation={navigation}
            setTimersPosition={setTimersPosition}
            userProfile={email}
          />
        </ScrollView>
      )}
    </SafeAreaView>
  );
};

WeeklyTimer.propTypes = {
  timerList: PropTypes.array,
  fetchTimerLoading: PropTypes.bool,
  submitToggleTimer: PropTypes.func,
  fetchUnitsAndTimers: PropTypes.func,
  toggleLoadingList: PropTypes.object,
  setEditTimerConfig: PropTypes.func,
  setEditTimerNewConfig: PropTypes.func,
  email: PropTypes.string,
  route: PropTypes.object,
};

const momorizedMapStateToProps = () => {
  const getTimeListSelector = selector.getTimerList();
  const mapStateToProps = (state, props) => {
    const {
      timer: { isLoading: fetchTimerLoading, toggleLoadingList },
      user: {
        profile: { email },
      },
    } = state;

    return {
      timerList: getTimeListSelector(state, props),
      fetchTimerLoading,
      toggleLoadingList,
      email,
    };
  };

  return mapStateToProps;
};

const mapDispatchToProps = dispatch => ({
  fetchUnitsAndTimers: email => {
    dispatch(units.getAllUnits({ email }));
  },
  submitToggleTimer: (thingName, timerIndex) => {
    dispatch(timer.submitToggleTimer(thingName, timerIndex));
  },
  setEditTimerConfig: (thingName, timerList, dayIndex) => {
    dispatch(editTimer.setEditTimerConfig({ thingName, timerList, dayIndex }));
  },
  setEditTimerNewConfig: (thingName, unit, dayIndex, user) => {
    dispatch(
      editTimer.setEditTimerNewConfig({ thingName, unit, dayIndex, user })
    );
  },
});

export default connect(
  momorizedMapStateToProps,
  mapDispatchToProps
)(WeeklyTimer);
