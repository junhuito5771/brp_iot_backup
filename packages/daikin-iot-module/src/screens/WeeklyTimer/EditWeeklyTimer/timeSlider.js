import React, { useState, useEffect } from 'react';
import {
  View,
  TouchableOpacity,
  SafeAreaView,
  StyleSheet,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, editTimer } from '@daikin-dama/redux-iot';
import DatePicker from 'react-native-date-picker';

import {
  Text,
  Colors,
  HeaderText,
  HeaderBackButton,
  useDebounce,
} from '@module/daikin-ui';

import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { NavigationService } from '@module/utility';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  hitslop: {
    top: 30,
    bottom: 30,
    left: 30,
    right: 30,
  },
  marginRight: {
    marginRight: 15,
  },
  disabledTick: {
    tintColor: Colors.lightGrey,
  },
  errorMsg: {
    color: Colors.red,
    marginBottom: 10,
  },
  headerRightContainer: {
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const renderNavigationOptions = (route, navigation) => {
  const { params } = route;

  if (params && params.submitButtonParams) {
    const { submitButtonParams, canSubmitButton } = params;

    return {
      title: <HeaderText>Set Time</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
      headerRight: () => (
        <View
          style={[
            styles.flexRow,
            styles.marginRight,
            styles.headerRightContainer,
          ]}>
          <TouchableOpacity
            hitSlop={styles.hitSlop}
            onPress={submitButtonParams}
            disabled={!canSubmitButton}>
            <Image
              source={require('../../../shared/assets/general/submitTick.png')}
              style={StyleSheet.flatten([
                !canSubmitButton && styles.disabledTick,
              ])}
            />
          </TouchableOpacity>
        </View>
      ),
    };
  }

  return params;
};

const TimerPickerScreen = ({
  route,
  submitTime,
  hour,
  minute,
  timers,
  timerIndex,
  days,
  isNew,
}) => {
  const navigation = useNavigation();
  const [time, setTime] = useState(new Date());
  const [canSubmit, setCanSubmit] = useState(false);
  const [isDuplicated, setIsDuplicated] = useState(false);
  const debouncedTime = useDebounce(time, 500);

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  useEffect(() => {
    if (hour && minute) {
      const curDate = new Date();
      curDate.setHours(hour);
      curDate.setMinutes(minute);
      setTime(curDate);
    }
  }, []);

  useEffect(() => {
    if (debouncedTime) {
      const debouncedHour = debouncedTime.getHours();
      const debouncedMinute = debouncedTime.getMinutes();
      const hasValueChanged =
        debouncedHour !== hour || debouncedMinute !== minute;

      let isUnique = false;

      if (hasValueChanged) {
        isUnique =
          timers.findIndex(
            timerInfo =>
              (isNew || timerInfo.timerIndex !== timerIndex) &&
              days[timerInfo.day] &&
              timerInfo.hour === debouncedHour &&
              timerInfo.minute === debouncedMinute
          ) === -1;

        setIsDuplicated(!isUnique);
      }

      const isValid = hasValueChanged && isUnique;

      setCanSubmit(isValid);
    }
  }, [debouncedTime]);

  useFocusEffect(
    React.useCallback(() => {
      navigation.setParams({
        canSubmitButton: canSubmit,
        submitButtonParams: () => {
          submitTime(time);
          navigation.goBack();
        },
      });
    }, [debouncedTime, canSubmit])
  );

  return (
    <SafeAreaView style={styles.container}>
      {isDuplicated && (
        <Text small style={styles.errorMsg}>
          This timer is already existed.
        </Text>
      )}
      <DatePicker
        locale="fr" // enable 24h format
        is24hourSource="locale"
        mode="time"
        date={time}
        onDateChange={date => {
          setCanSubmit(false);
          setTime(date);
        }}
      />
    </SafeAreaView>
  );
};

TimerPickerScreen.propTypes = {
  route: PropTypes.object,
  submitTime: PropTypes.func,
  timers: PropTypes.array,
  timerIndex: PropTypes.number,
  days: PropTypes.array,
  hour: PropTypes.number,
  minute: PropTypes.number,
  isNew: PropTypes.bool,
};

TimerPickerScreen.defaultProps = {
  days: [],
  timers: [],
};

const mapDispatchToProps = dispatch => ({
  submitTime: time => {
    const hour = time.getHours();
    const minute = time.getMinutes();
    dispatch(
      editTimer.setEditTimerParam({ key: 'hour', value: hour, timerIndex: 0 })
    );
    dispatch(
      editTimer.setEditTimerParam({
        key: 'minute',
        value: minute,
        timerIndex: 0,
      })
    );
  },
  clearCanSubmitTime: () => {
    dispatch(editTimer.clearCanSubmitTime());
  },
});

const mapStateToProps = ({
  editTimer: {
    canSubmitTime,
    submitTimeMsg,
    timerList: editTimerList,
    days,
    isNew,
    thingName,
  },
  timer: { timerList },
}) => {
  const { hour, minute, timerIndex } = editTimerList[0] || {};

  return {
    timerIndex,
    hour,
    minute,
    canSubmitTime,
    submitTimeMsg,
    timers: timerList[thingName],
    days,
    isNew,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TimerPickerScreen);
