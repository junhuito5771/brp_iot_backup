import React, { useState, useEffect } from 'react';
import {
  SafeAreaView,
  ScrollView,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Alert,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, editTimer } from '@daikin-dama/redux-iot';
import { ScaleText, StringHelper, NavigationService } from '@module/utility';
import Collapsible from 'react-native-collapsible';

import {
  Text,
  TimePicker,
  HeaderText,
  HeaderBackButton,
  Colors,
} from '@module/daikin-ui';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import modeIcons from '../../../constants/modeIcons';

import LevelSelector from '../../../components/LevelSelector';
import FanSpeedIcon from '../../../constants/FanSpeedIcons';
import SilentIcon from '../../../constants/QuietIcons';

import {
  TEMP_SLIDER_SCREEN,
  TIME_SLIDER_SCREEN,
} from '../../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  flexGrow: {
    flexGrow: 1,
  },
  section: {
    backgroundColor: Colors.offWhite,
    shadowColor: Colors.darkBlack,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,
    elevation: 2,
    paddingHorizontal: 20,
    paddingVertical: 5,
    justifyContent: 'center',
  },
  sectionTitle: {
    color: Colors.sectionHeaderTitleGrey,
  },
  sectionContent: {
    paddingVertical: 5,
  },
  timeText: {
    fontSize: ScaleText(45),
    textAlign: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  alignEnd: {
    alignItems: 'flex-end',
  },
  flexRow: {
    flexDirection: 'row',
  },
  seperator: {
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  buttonContainer: {
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  levelContainer: {
    paddingVertical: 5,
  },
  modeTitle: {
    textAlign: 'center',
    marginVertical: 5,
    color: Colors.blue,
  },
  selectionContainer: {
    paddingVertical: 10,
  },
  modeIconsContainer: {
    marginTop: 10,
  },
  circleContent: {
    height: 40,
    width: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: Colors.sectionContentGrey,
    justifyContent: 'center',
    alignItems: 'center',
  },
  circle: {
    marginHorizontal: 5,
  },
  selectedCircle: {
    backgroundColor: Colors.dayBlue,
    borderColor: Colors.dayBlue,
  },
  selectedCircleText: {
    color: Colors.white,
  },
  dayText: {
    color: Colors.sectionContentGrey,
  },
  tempText: {
    fontSize: 45,
  },
  bottomContainer: {
    paddingVertical: 10,
    backgroundColor: Colors.offWhite,
  },
  deleteText: {
    color: Colors.red,
    textAlign: 'center',
    fontSize: 20,
  },
  marginRight: {
    marginRight: 15,
  },
  hitslop: {
    top: 50,
    bottom: 50,
    right: 30,
    left: 30,
  },
  headerRightContainer: {
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  disabledTick: {
    tintColor: Colors.lightGrey,
  },
});

const REMOTE_ON_BUTTONS = [
  require('../../../shared/assets/remote/onOff.png'),
  require('../../../shared/assets/remote/onOn.png'),
];

const REMOTE_OFF_BUTTONS = [
  require('../../../shared/assets/remote/offOff.png'),
  require('../../../shared/assets/remote/offOn.png'),
];

const modes = {
  auto: {
    title: 'Auto',
    value: 'auto',
    ...modeIcons.auto,
  },
  cool: {
    title: 'Cool',
    value: 'cool',
    ...modeIcons.cool,
  },
  fan: {
    title: 'Fan',
    value: 'fan',
    ...modeIcons.fan,
  },
  dry: {
    title: 'Dry',
    value: 'dry',
    ...modeIcons.dry,
  },
  heat: {
    title: 'Heat',
    value: 'heat',
    ...modeIcons.heat,
  },
};

const days = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'];

const FanOptionSelection = ({
  showAutoOnly,
  fanValue,
  silentValue,
  setFanValue,
  enableAutoFan,
  enableLowFan,
  enableSilent,
}) => (
  <View style={[styles.selectionContainer, styles.center]}>
    <Text medium style={styles.modeTitle}>
      {`${silentValue === 1 ? 'Silent' : (FanSpeedIcon[fanValue] || {}).label}`}
    </Text>
    <View style={[styles.flexRow, styles.modeIconsContainer, styles.alignEnd]}>
      {enableAutoFan && (
        <View style={[styles.center, styles.buttonContainer]}>
          <TouchableOpacity
            onPress={() => {
              setFanValue(FanSpeedIcon.auto.value.toLowerCase(), 0);
            }}
            disabled={fanValue === FanSpeedIcon.auto.value.toLowerCase()}>
            <Image
              source={
                FanSpeedIcon.auto.icons[
                  Number(
                    silentValue === 0 && fanValue === FanSpeedIcon.auto.value
                  )
                ]
              }
            />
          </TouchableOpacity>
        </View>
      )}
      {!showAutoOnly && enableSilent && (
        <View style={[styles.center, styles.buttonContainer]}>
          <TouchableOpacity
            onPress={() => {
              setFanValue(fanValue, 1 - silentValue);
            }}>
            <Image source={SilentIcon[1].icons[silentValue]} />
          </TouchableOpacity>
        </View>
      )}
      {!showAutoOnly && (
        <LevelSelector
          containerStyle={styles.levelContainer}
          enableLow={enableLowFan}
          selectedValue={silentValue === 1 ? '' : fanValue}
          onSelect={curFanValue => {
            setFanValue(curFanValue, 0);
          }}
        />
      )}
    </View>
  </View>
);

FanOptionSelection.propTypes = {
  enableAutoFan: PropTypes.bool,
  enableLowFan: PropTypes.bool,
  enableSilent: PropTypes.bool,
  fanValue: PropTypes.string,
  silentValue: PropTypes.number,
  setFanValue: PropTypes.func,
  showAutoOnly: PropTypes.bool,
};

const renderNavigationOptions = (route, navigation) => {
  const { params } = route;

  if (params && params.submitButtonParams) {
    const { submitButtonParams, isLoading, canSubmitButton } = params;

    return {
      title: params.isNew ? (
        <HeaderText>Create weekly timer</HeaderText>
      ) : (
        <HeaderText>Edit weekly timer</HeaderText>
      ),
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
      headerRight: () => (
        <View
          style={[
            styles.flexRow,
            styles.headerRightContainer,
            styles.marginRight,
          ]}>
          {isLoading ? (
            <ActivityIndicator size="small" />
          ) : (
            <TouchableOpacity
              hitSlop={styles.hitSlop}
              onPress={submitButtonParams}
              disabled={!canSubmitButton}>
              <Image
                source={require('../../../shared/assets/general/submitTick.png')}
                style={StyleSheet.flatten([
                  !canSubmitButton && styles.disabledTick,
                ])}
              />
            </TouchableOpacity>
          )}
        </View>
      ),
    };
  }

  return params;
};

const editTimerScreen = ({
  hour,
  minute,
  switchValue,
  setSwitchValue,
  modeValue,
  setModeValue,
  daysValue,
  setDaysValue,
  tempValue,
  silentValue,
  fanValue,
  setFanValue,
  timerIndex,
  submitTimer,
  route,
  unit,
  deleteTimer,
  clearEditTimerStatus,
  success,
  error,
  isLoading,
  // clearEditTimerConfig,
  canSubmit,
  timers,
  isNew,
}) => {
  const navigation = useNavigation();

  const [canSubmitLocal, setCanSubmitLocal] = useState(false);
  const [timePickerModal, setTimePickerModal] = useState(false);
  const [showMoreOptions, setShowMoreOptions] = useState({
    showTemp: true,
    showAutoOnly: true,
  });

  const availableModes = Object.keys(unit.modes).filter(
    modeKey => !unit.modes[modeKey]
  );

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      ...renderNavigationOptions(route, navigation),
    });
  }, [navigation]);

  useEffect(() => {
    const isUnique =
      timers.findIndex(
        timerInfo =>
          daysValue[timerInfo.day] &&
          timerInfo.timerIndex !== timerIndex &&
          timerInfo.hour === hour &&
          timerInfo.minute === minute
      ) === -1;

    setCanSubmitLocal(canSubmit && isUnique);
  }, [canSubmit, daysValue, hour, minute]);

  useFocusEffect(
    React.useCallback(() => {
      navigation.setParams({
        canSubmitButton: canSubmitLocal,
        submitButtonParams: submitTimer,
        isLoading,
        isNew,
      });
    }, [navigation, isLoading, canSubmitLocal])
  );

  useEffect(() => {
    const showTemp = !(modeValue === 'fan' || modeValue === 'dry');
    const showAutoOnly = modeValue === 'dry';
    setShowMoreOptions({ showTemp, showAutoOnly });
  }, [modeValue]);

  // useEffect(() => clearEditTimerConfig, []);

  useEffect(() => {
    if (success) {
      navigation.goBack();
    } else if (
      error ===
      'The maximum number of timers that are allowed to set is reached.'
    ) {
      Alert.alert(
        'Maximum timers reached',
        'Each unit can only create up to 42 timers'
      );
    } else if (error) {
      Alert.alert('Error', 'Error while trying to update the timer');
    }
    clearEditTimerStatus();
  }, [success, error]);

  const handleDeleteTimer = () => {
    Alert.alert('Delete timer', 'Are you sure you want to delete this timer?', [
      {
        text: 'No',
      },
      {
        text: 'Yes',
        onPress: () => deleteTimer(timerIndex),
      },
    ]);
  };

  return (
    <SafeAreaView style={styles.flex}>
      <ScrollView style={styles.flexGrow}>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>Time</Text>
        </View>
        <View style={styles.sectionContent}>
          <TouchableOpacity
            onPress={() => {
              NavigationService.navigate(TIME_SLIDER_SCREEN);
            }}>
            <Text style={styles.timeText}>
              {StringHelper.getTimeInString(hour, minute)}
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.section}>
          <Text style={styles.sectionTitle}>Operational settings</Text>
        </View>
        <View style={styles.sectionContent}>
          <View style={[styles.flexRow, styles.seperator, styles.center]}>
            <View style={[styles.center, styles.buttonContainer]}>
              <TouchableOpacity
                onPress={() => setSwitchValue(1 - switchValue)}
                disabled={switchValue === 0}>
                <Image source={REMOTE_OFF_BUTTONS[1 - switchValue]} />
              </TouchableOpacity>
            </View>
            <View style={[styles.center, styles.buttonContainer]}>
              <TouchableOpacity
                onPress={() => setSwitchValue(1 - switchValue)}
                disabled={switchValue === 1}>
                <Image source={REMOTE_ON_BUTTONS[switchValue]} />
              </TouchableOpacity>
            </View>
          </View>
          <Collapsible collapsed={Boolean(1 - switchValue)}>
            <View
              style={[
                styles.selectionContainer,
                styles.center,
                styles.seperator,
              ]}>
              <Text medium style={styles.modeTitle}>
                {(modes[modeValue] || {}).title}
              </Text>
              <View style={[styles.flexRow, styles.modeIconsContainer]}>
                {availableModes.map(modeKey => {
                  const curMode = modes[modeKey];
                  return (
                    <View
                      style={[styles.center, styles.buttonContainer]}
                      key={curMode.title}>
                      <TouchableOpacity
                        disabled={curMode.value === modeValue}
                        onPress={() => {
                          setModeValue(curMode.value);
                        }}>
                        <Image
                          source={
                            curMode.value === modeValue
                              ? curMode.active
                              : curMode.inactive
                          }
                        />
                      </TouchableOpacity>
                    </View>
                  );
                })}
              </View>
            </View>
            {showMoreOptions.showTemp && (
              <View style={[styles.selectionContainer, styles.seperator]}>
                <TouchableOpacity
                  onPress={() =>
                    NavigationService.navigate(TEMP_SLIDER_SCREEN)
                  }>
                  <View style={[styles.center]}>
                    <Text style={styles.tempText}>{tempValue} &#8451;</Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
            <FanOptionSelection
              {...{
                showAutoOnly: showMoreOptions.showAutoOnly,
                silentValue,
                fanValue,
                setFanValue,
                enableAutoFan: unit.enableAutoFan,
                enableLowFan: unit.enableLowFan,
                enableSilent: unit.enableSilent,
              }}
            />
          </Collapsible>
        </View>
        {isNew && (
          <>
            <View style={styles.section}>
              <Text style={styles.sectionTitle}>Apply to</Text>
            </View>
            <View style={styles.sectionContent}>
              <View
                style={[
                  styles.selectionContainer,
                  styles.flexRow,
                  styles.center,
                ]}>
                {days.map((day, index) => {
                  const isSelected = daysValue[index];

                  return (
                    <View style={styles.circle} key={day}>
                      <TouchableOpacity onPress={() => setDaysValue(index)}>
                        <View
                          style={[
                            styles.circleContent,
                            isSelected && styles.selectedCircle,
                          ]}>
                          <Text
                            style={[
                              styles.dayText,
                              isSelected && styles.selectedCircleText,
                            ]}>
                            {day}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  );
                })}
              </View>
            </View>
          </>
        )}
      </ScrollView>
      {!isNew && (
        <TouchableOpacity onPress={handleDeleteTimer}>
          <View style={[styles.bottomContainer, styles.center]}>
            <Text style={styles.deleteText}>Delete</Text>
          </View>
        </TouchableOpacity>
      )}
      <TimePicker
        isVisible={timePickerModal}
        onClose={() => setTimePickerModal(false)}
      />
    </SafeAreaView>
  );
};

editTimerScreen.propTypes = {
  hour: PropTypes.number,
  minute: PropTypes.number,
  switchValue: PropTypes.number,
  setSwitchValue: PropTypes.func,
  modeValue: PropTypes.string,
  setModeValue: PropTypes.func,
  daysValue: PropTypes.array,
  setDaysValue: PropTypes.func,
  unit: PropTypes.object,
  tempValue: PropTypes.number,
  silentValue: PropTypes.number,
  fanValue: PropTypes.string,
  setFanValue: PropTypes.func,
  submitTimer: PropTypes.func,
  route: PropTypes.object,
  timerIndex: PropTypes.number,
  clearEditTimerStatus: PropTypes.func,
  deleteTimer: PropTypes.func,
  success: PropTypes.string,
  error: PropTypes.string,
  isLoading: PropTypes.bool,
  // clearEditTimerConfig: PropTypes.func,
  canSubmit: PropTypes.bool,
  timers: PropTypes.array,
  isNew: PropTypes.bool,
};

editTimerScreen.defaultProps = {
  switchValue: 1,
  daysValue: Array(7).fill(false),
  timers: [],
};

const mapDispatchToProps = dispatch => ({
  setDaysValue: dayIndex => {
    dispatch(editTimer.setEditTimerDays(dayIndex));
  },
  setSwitchValue: switchValue => {
    dispatch(
      editTimer.setEditTimerParam({
        key: 'switchValue',
        value: switchValue,
        timerIndex: 0,
      })
    );
  },
  setModeValue: modeValue => {
    dispatch(
      editTimer.setEditTimerParam({
        key: 'mode',
        value: modeValue,
        timerIndex: 0,
      })
    );
  },
  setFanValue: (fanValue, silent) => {
    dispatch(
      editTimer.setEditTimerParam({
        key: 'fanLevel',
        value: fanValue,
        timerIndex: 0,
      })
    );
    dispatch(
      editTimer.setEditTimerParam({
        key: 'silent',
        value: silent,
        timerIndex: 0,
      })
    );
  },
  submitTimer: () => dispatch(editTimer.submitTimerParams()),
  deleteTimer: timerIndex => {
    dispatch(editTimer.setAchieveTimer(timerIndex));
    dispatch(editTimer.submitTimerParams());
  },
  clearEditTimerStatus: () => dispatch(editTimer.clearEditTimerStatus()),
  // clearEditTimerConfig: () => dispatch(editTimer.clearEditTimerConfig()),
});

const mapStateToProps = ({
  editTimer: {
    timerList: editTimerList,
    success,
    error,
    canSubmit,
    isLoading,
    days: daysValue,
    thingName,
    isNew,
  },
  timer: { timerList: allTimers },
  units: { allUnits },
}) => {
  const {
    hour,
    minute,
    switchValue,
    mode: modeValue,
    temp: tempValue,
    fanLevel: fanValue,
    silent: silentValue,
    timerIndex,
  } = editTimerList[0] || {};

  return {
    hour,
    minute,
    switchValue,
    modeValue,
    daysValue,
    tempValue,
    fanValue,
    silentValue,
    unit: allUnits[thingName],
    timerIndex,
    success,
    error,
    isLoading,
    canSubmit,
    timers: allTimers[thingName],
    isNew,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(editTimerScreen);
