import React, { useRef, useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  widthPercentage,
  heightPercentage,
  StringHelper,
  NavigationService,
} from '@module/utility';
import { connect, editTimer } from '@daikin-dama/redux-iot';

import { Colors, HeaderText, HeaderBackButton } from '@module/daikin-ui';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import Slider from '../../../components/UnitControl/slider';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  slider: {
    marginTop: heightPercentage(15),
  },
  hitSlop: {
    top: 30,
    bottom: 30,
    left: 30,
    right: 30,
  },
  marginRight: {
    marginRight: 15,
  },
  disabledTick: {
    tintColor: Colors.lightGrey,
  },
  headerRightContainer: {
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const scaledSize = widthPercentage(68);
const ratio = scaledSize / 252;
const cx = scaledSize / 2;
const cy = scaledSize / 2;

const config = {
  auto: {
    hasController: true,
    minTemp: 18,
    maxTemp: 30,
    colorRange: [
      '#76CEF6',
      '#74CCF5',
      '#69C6F1',
      '#5FBFEE',
      '#59BCEC',
      '#54B9EA',
      '#44AEE3',
      '#329ED8',
      '#208ECD',
      '#0576BC',
      '#DD3827',
      '#DC3726',
      '#DB3526',
    ],
  },
  cool: {
    hasController: true,
    minTemp: 18,
    maxTemp: 32,
    minTempColor: '#0576BC',
    maxTempColor: '#76CEF6',
  },
  heat: {
    hasController: true,
    minTemp: 10,
    maxTemp: 30,
    minTempColor: '#FC805B',
    maxTempColor: '#DB3526',
  },
  fan: {
    hasController: false,
    fill: Colors.fanModeFill,
  },
  dry: {
    hasController: false,
    fill: Colors.dryModeFill,
  },
};

const renderNavigationOptions = (route, navigation) => {
  const { params } = route;

  if (params && params.submitButtonParams) {
    const { submitButtonParams, canSubmitButton } = params;

    return {
      title: <HeaderText>Set temperature</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
      headerRight: () => (
        <View
          style={[
            styles.flexRow,
            styles.marginRight,
            styles.headerRightContainer,
          ]}>
          <TouchableOpacity
            hitSlop={styles.hitSlop}
            onPress={submitButtonParams}
            disabled={!canSubmitButton}>
            <Image
              source={require('../../../shared/assets/general/submitTick.png')}
              style={StyleSheet.flatten([
                !canSubmitButton && styles.disabledTick,
              ])}
            />
          </TouchableOpacity>
        </View>
      ),
    };
  }

  return params;
};

const TempSliderScreen = ({ modeValue, tempValue, route, setTempValue }) => {
  const navigation = useNavigation();
  const currentTemp = useRef(18);
  const [canSubmitButton, setCanSubmitButton] = useState(false);

  useFocusEffect(() => {
    NavigationService.setOptions(
      navigation,
      renderNavigationOptions(route, navigation)
    );
  }, [navigation]);

  useEffect(() => {
    navigation.setParams({
      canSubmitButton,
      submitButtonParams: () => {
        setTempValue(currentTemp.current);
        navigation.goBack();
      },
    });
  }, [canSubmitButton]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.slider}>
        <Slider
          {...{
            ratio,
            cx,
            cy,
            scaledSize,
            isOff: false,
            modeType: StringHelper.toCamelCase(modeValue) || 'Cool',
            acTemp: tempValue,
            minTemp: 18,
            maxTemp: 32,
            minTempColor: '#0576BC',
            maxTempColor: '#76CEF6',
            setTemp: value => {
              currentTemp.current = value;
              setCanSubmitButton(value !== tempValue);
            },
            ...config[modeValue],
          }}
        />
      </View>
    </SafeAreaView>
  );
};

TempSliderScreen.propTypes = {
  modeValue: PropTypes.string,
  tempValue: PropTypes.number,
  route: PropTypes.object,
  setTempValue: PropTypes.func,
  availableModes: PropTypes.array,
};

const mapStateToProps = ({ editTimer: { timerList } }) => {
  const { mode: modeValue, temp: tempValue } = timerList[0] || {};
  return {
    modeValue,
    tempValue,
  };
};

const mapDispatchToProps = dispatch => ({
  setTempValue: tempValue => {
    dispatch(
      editTimer.setEditTimerParam({
        key: 'temp',
        value: tempValue,
        timerIndex: 0,
      })
    );
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(TempSliderScreen);
