import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import Svg, { Text as SvgText, Path, Rect, G, TSpan } from 'react-native-svg';
import Collapsible from 'react-native-collapsible';

import { Text, Switch, getACIconFromPath } from '@module/daikin-ui';

import styles from './styles';

const DEFAULT_LOGO = getACIconFromPath();

const ADD_LOGO = require('../../../shared/assets/general/add.png');
const USER_TIMER_ICON = require('../../../shared/assets/general/user-timer-icon.png');
const COLLAPSIBLE_BUTTONS = [
  require('../../../shared/assets/general/uncollapse.png'),
  require('../../../shared/assets/general/collapse.png'),
];

const getTime = (hour, minute) => {
  if (Number.isNaN(hour) || Number.isNaN(minute)) return '';

  return `${hour.toString().padStart(2, '0')}:${minute
    .toString()
    .padStart(2, '0')}`;
};

const getModeLogo = mode => {
  switch (mode) {
    case 'cool':
      return require('../../../shared/assets/remote/coolMoodOff.png');
    case 'auto':
      return require('../../../shared/assets/remote/automaticDarkGrey.png');
    case 'heat':
      return require('../../../shared/assets/remote/heatGrey.png');
    case 'fan':
      return require('../../../shared/assets/remote/fanModeDarkGrey.png');
    case 'dry':
      return require('../../../shared/assets/remote/humidityMoodDarkGrey.png');
    default:
      return require('../../../shared/assets/remote/automaticDarkGrey.png');
  }
};

const TimerDetailRow = ({ switchValue, mode, temp }) => {
  if (switchValue === 0) {
    return (
      <View style={styles.timerDetailRow}>
        <Text light style={styles.offText}>
          OFF
        </Text>
      </View>
    );
  }

  return (
    <View style={[styles.flexRow, styles.timerDetailRow]}>
      {!!mode && (
        <View style={styles.modeContainer}>
          <Image
            source={getModeLogo(mode)}
            style={styles.modeLogo}
            resizeMode="contain"
          />
        </View>
      )}
      {temp > 0 && (
        <Text light style={styles.celciusText}>
          {`${temp} \u2103`}
        </Text>
      )}
    </View>
  );
};

TimerDetailRow.propTypes = {
  switchValue: PropTypes.number,
  mode: PropTypes.string,
  temp: PropTypes.number,
};

TimerDetailRow.defaultProps = {
  switchValue: 0,
  temp: 0,
  mode: '',
};

const START_X = 13;
const END_X = 325;
const LENGTH = END_X - START_X;

const getPoint = time => {
  const u = (time * LENGTH) / 2400;
  return u + START_X;
};

const getBarPath = time => {
  const curPoint = getPoint(time);

  return `M${curPoint} 11h2v15h-2z`;
};

const getColorFromMode = mode => {
  switch (mode) {
    case 'cool':
      return '#29A3DF';
    case 'heat':
      return '#FC805B';
    case 'fan':
      return '#549A49';
    case 'dry':
      return '#29A3DF';
    default:
      return '#549A49';
  }
};

const getDurationBarPath = (startTime, endTime) => {
  let startPoint = getPoint(startTime);
  let endPoint = getPoint(endTime);
  let roundStartPath = 'v15';
  let roundEndPath = '';

  if (startTime < 0) {
    startPoint = 3;
    roundEndPath = 'Q-2 18 3 11';
  }

  if (endTime > 2400) {
    endPoint = 335;
    roundStartPath = 'Q342 18 334 26';
  }

  return `M${startPoint} 11 H${endPoint} ${roundStartPath} H${startPoint} ${roundEndPath}z`;
};

const WeeklyTimerBar = ({ timers, pairs }) => (
  <View style={[styles.timerBarContainer, styles.contentWrapperPadding]}>
    <Svg viewBox="0 0 338 27" width="100%" height="100%">
      <G fill="none" fillRule="evenodd">
        <G transform="translate(0 10)">
          <Rect fill="#E6E7E9" y={1} width={338} height={15} rx={7.5} />
          <Path
            stroke="#979797"
            d="M13 1v15 M91 1v15 M169 1v15 M247 1v15 M325 1v15"
          />
        </G>
        <SvgText
          fontFamily="Roboto"
          fontSize={12}
          fill="#9B9B9B"
          transform="translate(-7 -2)">
          <TSpan x={13} y={11}>
            00
          </TSpan>
        </SvgText>
        <SvgText
          fontFamily="Roboto"
          fontSize={12}
          fill="#9B9B9B"
          transform="translate(-7 -2)">
          <TSpan x={91} y={11}>
            06
          </TSpan>
        </SvgText>
        <SvgText
          fontFamily="Roboto"
          fontSize={12}
          fill="#9B9B9B"
          transform="translate(-7 -2)">
          <TSpan x={169} y={11}>
            12
          </TSpan>
        </SvgText>
        <SvgText
          fontFamily="Roboto"
          fontSize={12}
          fill="#9B9B9B"
          transform="translate(-7 -2)">
          <TSpan x={247} y={11}>
            18
          </TSpan>
        </SvgText>
        <SvgText
          fontFamily="Roboto"
          fontSize={12}
          fill="#9B9B9B"
          transform="translate(-7 -2)">
          <TSpan x={325} y={11}>
            24
          </TSpan>
        </SvgText>
        <G>
          {timers
            .filter(timerInfo => timerInfo.isActive)
            .map(timerInfo => {
              const key = `${timerInfo.timerIndex}_${timerInfo.hour}_${timerInfo.minute}`;
              const fillColor =
                timerInfo.switchValue === 0
                  ? '#757575'
                  : getColorFromMode(timerInfo.mode);
              const hourInNumber = timerInfo.hour * 100;
              const timeInNumber = hourInNumber + timerInfo.minute;
              const path = getBarPath(timeInNumber);

              return <Path key={key} fill={fillColor} d={path} />;
            })}
          {pairs.map(pairInfo => {
            const { start, end, timerIndex } = pairInfo;
            const key = `${timerIndex}_${start.day}_${start.hour}_${start.minute}_${end.day}_${end.hour}_${end.minute}`;
            const startHourInNumber = start.hour * 100;
            const startInNumber = startHourInNumber + start.minute;
            const endHourInNumber = end.hour * 100;
            const endInNumber = endHourInNumber + end.minute;
            const fillColor = getColorFromMode(pairInfo.mode);
            const durationPath = getDurationBarPath(startInNumber, endInNumber);

            return (
              <Path
                key={key}
                fill={fillColor}
                opacity={0.503}
                d={durationPath}
              />
            );
          })}
        </G>
      </G>
    </Svg>
  </View>
);

WeeklyTimerBar.propTypes = {
  timers: PropTypes.array,
  pairs: PropTypes.array,
};

export const WeeklyTimerGroupHeader = ({ title, count }) => (
  <View style={[styles.groupHeader, styles.rowContainer]}>
    <View
      style={[
        styles.groupHeaderContainer,
        styles.flexRow,
        styles.textContainer,
      ]}>
      <Text medium numberOfLines={1}>
        {title}
      </Text>
      <Text small> ({count})</Text>
    </View>
  </View>
);

WeeklyTimerGroupHeader.propTypes = {
  title: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
};

export const WeeklyTimerHeader = ({
  title,
  isCollapsible,
  onPressCollapse,
  onPressAdd,
}) => (
  <View style={[styles.header, styles.rowContainer, styles.spacebetween]}>
    <View style={[styles.flexRow, styles.alignCenter]}>
      <View style={[styles.flexRow, styles.alignCenter, styles.textContainer]}>
        <Image source={DEFAULT_LOGO} style={styles.logoIcon} />
        <Text medium numberOfLines={1}>
          {title}
        </Text>
      </View>
    </View>
    <View style={[styles.flexRow, styles.alignCenter]}>
      <TouchableOpacity hitSlop={styles.hitslop} onPress={onPressCollapse}>
        <Image
          source={COLLAPSIBLE_BUTTONS[Number(isCollapsible)]}
          style={styles.collapseIcon}
        />
      </TouchableOpacity>
      <TouchableOpacity hitSlop={styles.hitslop} onPress={onPressAdd}>
        <Image source={ADD_LOGO} style={styles.addIcon} />
      </TouchableOpacity>
    </View>
  </View>
);

WeeklyTimerHeader.propTypes = {
  title: PropTypes.string.isRequired,
  isCollapsible: PropTypes.bool,
  onPressCollapse: PropTypes.func.isRequired,
  onPressAdd: PropTypes.func.isRequired,
};

WeeklyTimerHeader.defaultProps = {
  isCollapsible: false,
};

const WeeklyTimerContent = ({
  timers,
  pairs,
  isCollapsible,
  thingName,
  onPressEdit,
  onPressToggle,
  toggleLoadingList,
  setTimersPosition,
  userProfile,
}) => (
  <Collapsible collapsed={isCollapsible}>
    <View style={styles.contentContainer}>
      <WeeklyTimerBar timers={timers} pairs={pairs} thingName={thingName} />
      {timers.length < 1 ? (
        <View
          style={[
            styles.contentWrapperPadding,
            styles.center,
            styles.contentRowContainer,
          ]}>
          <Text medium>Tap '+' to add an action</Text>
        </View>
      ) : (
        timers.map((timerInfo, index) => (
          <View
            ref={ref =>
              setTimersPosition({
                thingName,
                timerIndex: timerInfo.timerIndex,
                itemRef: ref,
              })
            }
            key={`${thingName}_${timerInfo.timerIndex}_${timerInfo.day}_${timerInfo.hour}_${timerInfo.minute}`}
            style={[
              styles.flexRow,
              styles.contentWrapperPadding,
              styles.contentRowContainer,
              styles.alignCenter,
              index < timers.length - 1 && styles.borderBottom,
            ]}>
            <View style={styles.contentWrapper}>
              <TouchableOpacity
                style={styles.flex}
                onPress={() => {
                  onPressEdit(thingName, timerInfo);
                }}>
                <View style={[styles.flexRow, styles.alignCenter]}>
                  <View style={styles.userIcon}>
                    {userProfile === timerInfo.user ? (
                      <Image source={USER_TIMER_ICON} />
                    ) : null}
                  </View>
                  <Text light style={styles.timerText}>
                    {getTime(timerInfo.hour, timerInfo.minute)}
                  </Text>
                  <TimerDetailRow
                    switchValue={timerInfo.switchValue}
                    temp={timerInfo.temp}
                    mode={timerInfo.mode}
                  />
                </View>
              </TouchableOpacity>
              <View style={styles.flexRow}>
                {(toggleLoadingList[thingName] || {})[timerInfo.timerIndex] && (
                  <ActivityIndicator size="small" />
                )}
                <Switch
                  value={timerInfo.isActive}
                  onPress={() => onPressToggle(thingName, timerInfo)}
                />
              </View>
            </View>
          </View>
        ))
      )}
    </View>
  </Collapsible>
);

WeeklyTimerContent.propTypes = {
  timers: PropTypes.arrayOf(PropTypes.object),
  pairs: PropTypes.arrayOf(PropTypes.object).isRequired,
  isCollapsible: PropTypes.bool,
  thingName: PropTypes.string.isRequired,
  onPressEdit: PropTypes.func.isRequired,
  onPressToggle: PropTypes.func.isRequired,
  toggleLoadingList: PropTypes.objectOf(PropTypes.object).isRequired,
  setTimersPosition: PropTypes.func.isRequired,
  userProfile: PropTypes.string.isRequired,
};

WeeklyTimerContent.defaultProps = {
  timers: [],
  isCollapsible: false,
};

const WeeklyTimerUnitsContainer = ({
  units,
  onPressAdd,
  dayIndex,
  onPressEdit,
  onPressToggle,
  toggleLoadingList,
  setTimersPosition,
  userProfile,
}) => {
  const [activeSections, setActiveSections] = useState([]);

  useEffect(() => {
    if (units && units.length > -1) {
      setActiveSections(Array(units.length).fill(false));
    }
  }, [units]);

  const toggleCollapsible = sectionIndex => {
    setActiveSections(prevActiveSections => {
      const curActiveSections = [...prevActiveSections];
      curActiveSections[sectionIndex] = !prevActiveSections[sectionIndex];

      return curActiveSections;
    });
  };

  return units.map((unit, index) => {
    const { unitName, thingName, logo, timers, pairs } = unit;
    return (
      <React.Fragment key={thingName}>
        <WeeklyTimerHeader
          title={unitName}
          logo={logo}
          isCollapsible={activeSections[index]}
          onPressAdd={() => {
            onPressAdd(thingName, unit, dayIndex);
          }}
          onPressCollapse={() => toggleCollapsible(index)}
        />
        <WeeklyTimerContent
          thingName={thingName}
          onPressEdit={onPressEdit}
          onPressToggle={onPressToggle}
          timers={timers}
          pairs={pairs}
          isCollapsible={activeSections[index]}
          toggleLoadingList={toggleLoadingList}
          setTimersPosition={setTimersPosition}
          userProfile={userProfile}
        />
      </React.Fragment>
    );
  });
};

WeeklyTimerUnitsContainer.propTypes = {
  units: PropTypes.arrayOf(PropTypes.object).isRequired,
  onPressAdd: PropTypes.func,
  onPressEdit: PropTypes.func,
  dayIndex: PropTypes.number,
  onPressToggle: PropTypes.func,
  toggleLoadingList: PropTypes.objectOf(PropTypes.object),
  setTimersPosition: PropTypes.func,
};

const WeeklyTimerContainer = ({ data, ...extraProps }) =>
  data.map((timerItem, index) => {
    const { groupName, count, units } = timerItem;
    const key = `${timerItem}_${index}`;

    return (
      <React.Fragment key={key}>
        <WeeklyTimerGroupHeader title={groupName} count={count} />
        <WeeklyTimerUnitsContainer units={units} {...extraProps} />
      </React.Fragment>
    );
  });

WeeklyTimerContainer.propTypes = {
  data: PropTypes.array,
};

export default WeeklyTimerContainer;
