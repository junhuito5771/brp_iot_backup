import { StyleSheet, Platform } from 'react-native';
import { heightPercentage, widthPercentage, ScaleText } from '@module/utility';

import { Colors } from '@module/daikin-ui';

export default StyleSheet.create({
  flexRow: {
    flexDirection: 'row',
  },
  flex: {
    flex: 1,
    flexDirection: 'row',
  },
  groupHeader: {
    backgroundColor: Colors.weeklyTimerGroupHeaderGrey,
    borderBottomColor: Colors.weeklyTimerGroupHeaderGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    alignItems: 'center',
  },
  groupHeaderContainer: {
    alignItems: 'flex-end',
  },
  header: {
    backgroundColor: Colors.userListSectionGrey,
    borderBottomColor: Colors.userListSectionGrey,
    borderBottomWidth: StyleSheet.hairlineWidth,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
  },
  rowContainer: {
    height: Platform.select({
      ios: heightPercentage(8),
      android: heightPercentage(10),
    }),
    paddingHorizontal: widthPercentage(5),
  },
  alignCenter: {
    alignItems: 'center',
  },

  userIcon: {
    position: 'absolute',
    left: -28,
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  spacebetween: {
    justifyContent: 'space-between',
  },
  collapseButton: {
    marginRight: 10,
    height: 10,
  },
  logoIcon: {
    marginRight: 15,
  },
  collapseIcon: {
    marginHorizontal: 15,
  },
  addIcon: {
    marginHorizontal: 10,
  },
  hitslop: {
    top: 50,
    bottom: 50,
  },
  timerBarContainer: {
    height: heightPercentage(8),
  },
  contentSpaceBetween: {
    justifyContent: 'space-between',
  },
  contentContainer: {
    backgroundColor: Colors.white,
    paddingLeft: widthPercentage(5),
  },
  contentRowContainer: {
    paddingVertical: 10,
  },
  borderBottom: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderlightGray,
  },
  contentWrapperPadding: {
    paddingRight: widthPercentage(5),
  },
  contentWrapper: {
    paddingLeft: widthPercentage(10),
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    alignItems: 'center',
  },
  modeContainer: {
    alignItems: 'center',
    marginRight: 15,
  },
  offText: {
    fontSize: ScaleText(20),
    fontWeight: Platform.select({ ios: '300', android: '100' }),
    textAlign: 'center',
  },
  timerText: {
    fontSize: ScaleText(35),
    fontWeight: Platform.select({ ios: '300', android: '100' }),
    textAlign: 'center',
  },
  celciusText: {
    fontSize: ScaleText(25),
    fontWeight: Platform.select({ ios: '300', android: '100' }),
    textAlign: 'center',
  },
  timerDetailRow: {
    marginHorizontal: 30,
    alignItems: 'center',
  },
  textContainer: {
    maxWidth: widthPercentage(40),
  },
});
