import React, { useRef } from 'react';
import {
  View,
  Image,
  TouchableOpacity,
  StyleSheet,
  Animated,
  ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  heightPercentage,
  NavigationService,
  generalInputPreValidator,
} from '@module/utility';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';

import { TextInput, Text, Colors, getACIconFromPath } from '@module/daikin-ui';

import { EDIT_ICONS_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingLeft: 25,
    height: 70,
    width: '100%',
    zIndex: 1,
  },
  activateContainer: {
    elevation: 99,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  justifyContent: {
    justifyContent: 'center',
  },
  alignItems: {
    alignItems: 'center',
  },
  flexRow: {
    flexDirection: 'row',
  },
  handler: { marginLeft: 20, marginRight: 10 },
  borderRight: {
    marginLeft: 15,
    borderRightWidth: 1,
    borderRightColor: Colors.lightCoolGrey,
    height: heightPercentage(7),
  },
  aircondIcon: {
    marginHorizontal: 15,
  },
  optionIconStyle: {
    margin: 10,
  },
  inputContainer: {
    flex: 0.9,
    paddingBottom: 0,
    marginLeft: 10,
  },
  hitSlop: {
    top: 10,
    right: 10,
    bottom: 10,
    left: 10,
  },
  unitPadding: {
    paddingLeft: 10,
  },
  optionStyle: {
    padding: 10,
  },
  optionScrollStyle: {
    maxHeight: heightPercentage(20),
  },
});

const UnitRow = ({
  sectionId,
  id,
  iconSource,
  name,
  isActive,
  setInputRef,
  editName,
  onFocus,
  drag,
  moveGroup,
  groupSelection,
  setLogo,
}) => {
  const rowRef = useRef();

  const handleEditName = text => {
    editName(id, generalInputPreValidator(text));
  };

  return (
    <Animated.View
      style={[
        styles.container,
        styles.flexRow,
        styles.alignItems,
        isActive && styles.activateContainer,
      ]}
      ref={rowRef}>
      <TouchableOpacity
        hitSlop={styles.hitSlop}
        onPressIn={drag}
        disabled={isActive}>
        <Image
          source={require('../../shared/assets/general/hamburgerGrey.png')}
          style={styles.handler}
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.flexRow, styles.alignItems]}
        onPress={() => {
          NavigationService.navigate(EDIT_ICONS_SCREEN, {
            iconThing: id,
            setAcIcon: (icon, target) => setLogo(target, icon),
          });
        }}>
        <Image
          source={getACIconFromPath(iconSource)}
          style={styles.aircondIcon}
        />
        <Image
          source={require('../../shared/assets/general/dropdown.png')}
          style={styles.dropdownIcon}
        />
      </TouchableOpacity>
      <View style={styles.borderRight} />
      <TextInput
        ref={e => {
          if (setInputRef) {
            setInputRef(id, e);
          }
        }}
        style={styles.inputContainer}
        onChangeText={handleEditName}
        value={name}
        onFocus={onFocus}
      />
      <Menu>
        <MenuTrigger
          customStyles={{
            TriggerTouchableComponent: TouchableOpacity,
            triggerOuterWrapper: styles.unitPadding,
          }}>
          <Image
            source={require('../../shared/assets/general/optionGrey.png')}
            style={styles.optionIconStyle}
          />
        </MenuTrigger>
        <MenuOptions customStyles={{ optionText: styles.text }}>
          <ScrollView style={styles.optionScrollStyle}>
            {groupSelection.map(({ label, value }) => (
              <MenuOption
                // eslint-disable-next-line react/no-array-index-key
                key={label}
                onSelect={() => {
                  moveGroup(sectionId, id, value);
                }}>
                <Text style={styles.optionStyle} small>
                  {label}
                </Text>
              </MenuOption>
            ))}
          </ScrollView>
        </MenuOptions>
      </Menu>
    </Animated.View>
  );
};

UnitRow.propTypes = {
  sectionId: PropTypes.string,
  name: PropTypes.string,
  id: PropTypes.string,
  iconSource: PropTypes.string,
  drag: PropTypes.func,
  isActive: PropTypes.bool,
  setInputRef: PropTypes.func,
  editName: PropTypes.func,
  onFocus: PropTypes.func,
  moveGroup: PropTypes.func,
  groupSelection: PropTypes.array,
  setLogo: PropTypes.func,
};

export default UnitRow;
