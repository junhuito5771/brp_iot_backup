import React from 'react';
import { View, TouchableOpacity, StyleSheet, Image, Alert } from 'react-native';
import { generalInputPreValidator } from '@module/utility';
import PropTypes from 'prop-types';
import Animated from 'react-native-reanimated';

import { Colors, Text, TextInput } from '@module/daikin-ui';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.lightCoolGrey,
    paddingHorizontal: 5,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 2,
    height: 70,
    width: '100%',
  },
  activateContainer: {
    elevation: 99,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  justifyContent: {
    justifyContent: 'center',
  },
  innerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  alignItems: {
    alignItems: 'center',
  },
  handler: { margin: 10 },
  inputContainer: {
    flex: 0.8,
    paddingBottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputInner: {
    borderColor: Colors.lightGrey,
  },
  label: {
    marginLeft: 15,
  },
  rightContainer: {
    flexDirection: 'row',
  },
  hitSlop: {
    top: 10,
    right: 10,
    bottom: 10,
    left: 10,
  },
  groupName: {
    marginHorizontal: 15,
  },
  deleteIcon: {
    marginRight: 20,
  },
});

const GroupRow = ({
  id,
  isEditable,
  name,
  setInputRef,
  handleOnLongPress,
  isActive,
  editName,
  onFocus,
  removeGroup,
}) => {
  const handleEditName = text => {
    editName(id, generalInputPreValidator(text));
  };

  const handleRemoveGroup = () => {
    Alert.alert('Ungroup units', 'Are you sure you want to ungroup units?', [
      { text: 'No' },
      { text: 'Yes', onPress: () => removeGroup(id) },
    ]);
  };

  return (
    <Animated.View
      style={[
        styles.container,
        styles.justifyContent,
        isActive && styles.activateContainer,
      ]}>
      <View style={[styles.innerContainer, styles.alignItems]}>
        <View style={[styles.rightContainer, styles.alignItems]}>
          {isEditable && (
            <TouchableOpacity
              hitSlop={styles.hitSlop}
              onLongPress={handleOnLongPress}>
              <Image
                source={require('../../shared/assets/general/hamburgerGrey.png')}
                style={styles.handler}
              />
            </TouchableOpacity>
          )}
          {isEditable ? (
            <TextInput
              ref={e => {
                if (setInputRef) {
                  setInputRef(id, e);
                }
              }}
              style={styles.inputContainer}
              innerStyle={styles.inputInner}
              onChangeText={handleEditName}
              value={name}
              placeholder="Group Name *"
              onFocus={onFocus}
            />
          ) : (
            <View style={styles.groupName}>
              <Text medium>{name}</Text>
            </View>
          )}
        </View>
        {isEditable && (
          <TouchableOpacity onPress={handleRemoveGroup}>
            <Image
              style={styles.deleteIcon}
              source={require('../../shared/assets/general/delete.png')}
            />
          </TouchableOpacity>
        )}
      </View>
    </Animated.View>
  );
};

GroupRow.propTypes = {
  isEditable: PropTypes.bool,
  name: PropTypes.string,
  handleOnLongPress: PropTypes.func,
  setInputRef: PropTypes.func,
  isActive: PropTypes.bool,
  id: PropTypes.string,
  editName: PropTypes.func,
  onFocus: PropTypes.func,
  removeGroup: PropTypes.func,
};

export default GroupRow;
