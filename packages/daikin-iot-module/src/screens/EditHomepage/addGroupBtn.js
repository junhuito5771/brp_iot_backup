import React from 'react';
import { TouchableOpacity, View, Image } from 'react-native';
import PropTypes from 'prop-types';
import { Text } from '@module/daikin-ui';
import styles from './styles';

const AddGroupBtn = ({ onPress }) => (
  <View style={[styles.addGroup, styles.justifyContent]}>
    <TouchableOpacity
      style={[styles.flexRow, styles.alignItems]}
      {...{ onPress }}>
      <Image
        source={require('../../shared/assets/general/add.png')}
        style={styles.addGroupImage}
      />
      <Text medium>Add group</Text>
    </TouchableOpacity>
  </View>
);

AddGroupBtn.propTypes = {
  onPress: PropTypes.func,
};

export default AddGroupBtn;
