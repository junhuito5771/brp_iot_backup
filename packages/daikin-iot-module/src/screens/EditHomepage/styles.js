import { StyleSheet } from 'react-native';
import { widthPercentage, heightPercentage } from '@module/utility';
import { Colors } from '@module/daikin-ui';

const styles = StyleSheet.create({
  backBtnStyle: {
    width: 50,
    alignItems: 'center',
  },
  hitSlop: {
    top: 20,
    bottom: 20,
    left: 20,
    right: 20,
  },
  header: {
    backgroundColor: Colors.lightCoolGrey,
    paddingHorizontal: 20,
    borderBottomColor: Colors.lightGrey,
    borderBottomWidth: 2,
    height: heightPercentage(9),
  },
  flex: {
    flex: 1,
  },
  flexGrow: {
    flexGrow: 1,
  },
  headerTextbox: {
    flex: 0.8,
    paddingVertical: 8,
  },
  headerContent: {
    justifyContent: 'space-between',
  },
  contentTextbox: {
    flex: 0.9,
    paddingVertical: 10,
  },
  textbox: {
    marginLeft: 10,
  },
  flexRow: {
    flexDirection: 'row',
  },
  width: {
    width: widthPercentage(90),
  },
  hamburgerButton: {
    height: 10,
    margin: 10,
  },
  unitHamburgerBtn: {
    marginLeft: 20,
    marginRight: 10,
  },
  borderRight: {
    marginLeft: 15,
    borderRightWidth: 1,
    borderRightColor: Colors.lightCoolGrey,
    height: heightPercentage(7),
  },
  content: {
    backgroundColor: Colors.white,
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    height: heightPercentage(10),
    paddingLeft: 25,
  },
  aircondIcon: {
    marginHorizontal: 15,
  },
  alignItems: {
    alignItems: 'center',
  },
  justifyContent: {
    justifyContent: 'center',
  },
  lightGrey: {
    borderColor: Colors.lightGrey,
  },
  groupName: {
    marginLeft: 15,
  },
  unitPadding: {
    paddingLeft: 10,
  },
  optionIconStyle: {
    margin: 10,
  },
  addGroup: {
    paddingLeft: 25,
    height: heightPercentage(8),
  },
  addGroupImage: { height: 15, width: 15, marginRight: 25 },
  optionStyle: {
    padding: 10,
  },
});

export default styles;
