import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  backBtnStyle: {
    width: 50,
    alignItems: 'center',
  },
  listContent: {
    flex: 1,
    paddingBottom: 10,
  },
});
