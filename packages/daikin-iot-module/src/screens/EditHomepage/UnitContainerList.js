import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { connect, editUnits } from '@daikin-dama/redux-iot';

import { SortableScrollView } from '@module/daikin-ui';

import { UnitContainer } from './RowContainer';

const renderItem = ({ item, drag, sectionId }) => (
  <UnitContainer {...{ id: item, drag, sectionId }} />
);

renderItem.propTypes = {
  item: PropTypes.object,
  drag: PropTypes.func,
  sectionId: PropTypes.string,
};

function UnitContainerList({
  sectionId,
  units,
  extraData,
  setOrder,
  parentListRef,
}) {
  return (
    <View style={{ flex: 1 }}>
      <SortableScrollView
        parentListRef={parentListRef}
        otherData={extraData}
        data={units}
        keyExtractor={item => item}
        renderItem={({ item, drag }) => renderItem({ item, drag, sectionId })}
        onDragEnd={({ data }) => {
          setOrder(sectionId, data);
        }}
      />
    </View>
  );
}

const mapDispatchToProps = dispatch => ({
  setOrder: (key, order) => dispatch(editUnits.setItemChildOrder(key, order)),
});

const mapStateToProps = ({ editUnits: { items } }) => ({
  extraData: items,
});

UnitContainerList.propTypes = {
  sectionId: PropTypes.string,
  units: PropTypes.array,
  extraData: PropTypes.object,
  setOrder: PropTypes.func,
  parentListRef: PropTypes.any,
};

export default connect(mapStateToProps, mapDispatchToProps)(UnitContainerList);
