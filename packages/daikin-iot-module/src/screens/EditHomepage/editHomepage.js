/* eslint-disable no-underscore-dangle */
import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Keyboard,
  Alert,
  View,
  Platform,
  SafeAreaView,
  KeyboardAvoidingView,
} from 'react-native';
import Animated from 'react-native-reanimated';

import { connect, editUnits } from '@daikin-dama/redux-iot';
import { StringHelper, NavigationService } from '@module/utility';
import {
  HeaderButton,
  SortableFlatList,
  HeaderText,
  HeaderBackButton,
  useDebounce,
} from '@module/daikin-ui';

import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { HOME_SCREEN } from '../../constants/routeNames';

import styles from './styles_1';
import AddGroupBtn from './addGroupBtn';
import { GroupContainer } from './RowContainer';
import UnitContainerList from './UnitContainerList';

const ROW_HEIGHT = 70;

const RowItem = ({ item, setInputRef, drag }) => {
  if (!item) return null;

  const { id } = item;

  const baseProps = {
    id,
    setInputRef,
    handleOnLongPress: () => {
      drag();
    },
  };

  return (
    <Animated.View>
      <GroupContainer {...baseProps} />
      <UnitContainerList {...{ sectionId: id, units: item.childs }} />
    </Animated.View>
  );
};

RowItem.propTypes = {
  item: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
  data: PropTypes.object,
  isActive: PropTypes.bool,
  drag: PropTypes.func,
  setInputRef: PropTypes.func,
  editName: PropTypes.func,
  removeGroup: PropTypes.func,
  onFocus: PropTypes.func,
};

const renderNavigationOptions = route => {
  const { params } = route;
  if (params && params.saveButtonParams) {
    const { saveButtonParams } = params;
    return {
      headerRight: () => (
        <HeaderButton {...saveButtonParams}>Save</HeaderButton>
      ),
    };
  }
  return {};
};

const getInputData = (data) => {
  const result = data.filter(k => k.name.trim() === '');
  return result.length > 0;
};

const EditHomepageForm = ({
  itemIds,
  items,
  isLoading,
  success,
  error,
  submitEditParams,
  clearEditStatus,
  addNewGroup,
  setItemOrder,
  canSave,
  route,
}) => {
  const navigation = useNavigation();
  const debounceItems = useDebounce(items, 500);
  const [canSaveLocal, setCanSaveLocal] = useState(canSave);

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Edit</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
      ...renderNavigationOptions(route),
    });
  }, [navigation]);

  const listProps = useRef({
    flatlistHeight: -1,
    topOffset: -1,
    scrollOffset: 0,
  });
  const containerRef = useRef();
  const listRef = useRef();
  const inputRef = useRef({});
  const needScrollDown = useRef(false);

  const layoutOffsetMap = useRef(new Map());
  const isBlankInput = getInputData(Object.values(items));

  useEffect(() => {
    if (canSave) {
      setCanSaveLocal(
        Object.values(debounceItems).every(
          ({ name }) => StringHelper.trim(name) !== ''
        )
      );
    }
  }, [debounceItems, canSave]);

  useFocusEffect(
    React.useCallback(() => {
      const unsubscribe = navigation.addListener('beforeRemove', event => {
        if (canSaveLocal && !success) {
          event.preventDefault();

          Alert.alert(
            'Are you sure?',
            'You have unsaved changes. Do you want to leave before saving ?',
            [
              {
                text: 'No',
              },
              {
                text: 'Yes',
                onPress: () => navigation.dispatch(event.data.action),
              },
            ]
          );
        }
      });

      navigation.setParams({
        saveButtonParams: {
          isLoading,
          disabled: !canSaveLocal,
          onPress: () => {
            Keyboard.dismiss();
            if (isBlankInput) {
              Alert.alert('Error', 'Please insert Unit/Group name', [
                { text: 'Ok' },
              ]);
            } else submitEditParams();
          },
        },
      });

      return () => {
        // navigation.setParams({});
        unsubscribe();
      };
    }, [isLoading, canSaveLocal, navigation,isBlankInput, success])
  );

  useEffect(() => {
    if (success) {
      NavigationService.navigate(HOME_SCREEN);
      clearEditStatus();
    } else if (error) {
      Alert.alert('Error', error);
      clearEditStatus();
    }
  }, [success, error]);

  const handleAddGroup = () => {
    addNewGroup();
    needScrollDown.current = true;
  };

  const setInputRef = (key, ref) => {
    inputRef.current[key] = ref;
  };

  const onInputFocus = key => {
    const index = itemIds.findIndex(({ id }) => id === key);
    listRef.current.scrollToIndex({
      animated: true,
      index,
      viewPosition: 0.5,
    });
  };

  const handleRenderItem = props => (
    <RowItem {...props} {...{ setInputRef, onInputFocus }} />
  );

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 110 : 0}>
        <View style={{ height: '100%' }}>
          <AddGroupBtn onPress={handleAddGroup} />
          <View
            style={styles.container}
            ref={containerRef}
            onLayout={e => {
              listProps.current.flatlistHeight = e.nativeEvent.layout.height;

              containerRef.current.measureInWindow((_x, y) => {
                listProps.current.topOffset = y;
              });
            }}>
            <SortableFlatList
              removeClippedSubviews={!Platform.OS === 'android'}
              ref={listRef}
              style={styles.listContent}
              onScroll={e => {
                listProps.current.scrollOffset = e.nativeEvent.contentOffset.y;
              }}
              keyExtractor={({ id }) => id}
              data={itemIds}
              otherData={items}
              renderItem={handleRenderItem}
              getItemLayout={(item, index) => {
                const { childs } = item[index];
                const childRowHeight = ROW_HEIGHT * childs.length;
                const rowHeight =
                  childs && childs.length > 0
                    ? childRowHeight + ROW_HEIGHT
                    : ROW_HEIGHT;

                const prevOffset =
                  layoutOffsetMap.current.get(index - 1) !== undefined
                    ? layoutOffsetMap.current.get(index - 1)
                    : 0;
                layoutOffsetMap.current.set(index, prevOffset + rowHeight);

                const offset =
                  layoutOffsetMap.current.get(index - 1) !== undefined
                    ? layoutOffsetMap.current.get(index - 1)
                    : 0;

                return {
                  length: rowHeight,
                  offset,
                  index,
                };
              }}
              onContentSizeChange={() => {
                if (needScrollDown.current) {
                  needScrollDown.current = false;

                  listRef.current.scrollToIndex({
                    animated: true,
                    index: itemIds.length - 1,
                    viewOffset: -50,
                    viewPosition: 0.5,
                  });
                  requestAnimationFrame(() => {
                    const index = Number(itemIds[itemIds.length - 1].id);
                    const targetInputRef = inputRef.current[index];
                    if (targetInputRef) {
                      targetInputRef.focus();
                    }
                  });
                }
              }}
              onDragEnd={({ data }) => {
                setItemOrder(data);
              }}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

EditHomepageForm.propTypes = {
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  submitEditParams: PropTypes.func,
  clearEditStatus: PropTypes.func,
  addNewGroup: PropTypes.func,
  itemIds: PropTypes.array,
  items: PropTypes.object,
  setItemOrder: PropTypes.func,
  canSave: PropTypes.bool,
  route: PropTypes.object,
};

const mapStateToProps = state => ({
  itemIds: state.editUnits.itemIds,
  items: state.editUnits.items,
  canSave: state.editUnits.canSave,
  isLoading: state.editUnits.isLoading,
  success: state.editUnits.success,
  error: state.editUnits.error,
  groupOrder: state.editUnits.groupOrder,
});

const mapDispatchToProps = dispatch => ({
  addNewGroup: () => dispatch(editUnits.addEditItem()),
  submitEditParams: () => dispatch(editUnits.submitEditParams()),
  clearEditStatus: () => dispatch(editUnits.clearEditStatus()),
  setItemOrder: orders => dispatch(editUnits.setItemOrder(orders)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditHomepageForm);
