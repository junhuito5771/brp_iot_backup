import React from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Keyboard,
  TouchableWithoutFeedback,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {
  heightPercentage,
  widthPercentage,
  NavigationService,
} from '@module/utility';

import { useFocusEffect, useNavigation } from '@react-navigation/native';

import { HeaderText, HeaderBackButton } from '@module/daikin-ui';
import UnitIcons from '../../components/UnitIcons';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingVertical: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
  },
});

const EditIcons = ({ route }) => {
  const navigation = useNavigation();

  useFocusEffect(() => {
    NavigationService.setOptions(navigation, {
      title: <HeaderText>Select A New Icon</HeaderText>,
      headerLeft: () => <HeaderBackButton navigation={navigation} />,
    });
  }, [navigation]);

  const onSelect = key => {
    route.params.setAcIcon(`${key}.png`, route.params.iconThing);
    navigation.goBack();
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={styles.container}>
          <UnitIcons onAfterSelect={key => onSelect(key)} />
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

EditIcons.propTypes = {
  route: PropTypes.object,
};

export default EditIcons;
