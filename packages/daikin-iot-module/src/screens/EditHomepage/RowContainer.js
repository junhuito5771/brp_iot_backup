import React from 'react';
import PropTypes from 'prop-types';
import { connect, editUnits, selector } from '@daikin-dama/redux-iot';

import GroupRow from './groupRow';
import UnitRow from './unitRow';

const GroupContainer = ({ id, data, ...extraProps }) => {
  const baseProps = {
    id,
    isEditable: id !== '-1',
    name: data.name,
  };

  return <GroupRow {...baseProps} {...extraProps} />;
};

const groupMapStateToProps = (state, props) => {
  const {
    editUnits: { items },
  } = state;

  return {
    data: items[props.id],
  };
};

const groupMapDispatchToProps = dispatch => ({
  editName: (id, name) => dispatch(editUnits.setItemName({ id, name })),
  removeGroup: index => dispatch(editUnits.removeGroup({ index })),
});

GroupContainer.defaultProps = {
  id: '',
  data: {},
};

GroupContainer.propTypes = {
  id: PropTypes.string,
  data: PropTypes.object,
};

const conGroupContainer = connect(
  groupMapStateToProps,
  groupMapDispatchToProps
)(GroupContainer);

const UnitContainer = ({ id, data, ...extraProps }) => {
  const baseProps = {
    id,
    name: data.name,
    iconSource: data.logo,
  };

  return <UnitRow {...baseProps} {...extraProps} />;
};

const unitMapStateToProps = (state, props) => {
  const {
    editUnits: { items },
  } = state;

  return {
    data: items[props.id],
    groupSelection: selector.getGroupSelectionList(state, props),
  };
};

const unitMapDispatchToProps = dispatch => ({
  editName: (id, name) => dispatch(editUnits.setItemName({ id, name })),
  moveGroup: (fromGroup, unit, targetGroup) =>
    dispatch(editUnits.setItemToDiffGroup({ fromGroup, unit, targetGroup })),
  setLogo: (id, logo) => dispatch(editUnits.setItemIcon(id, logo)),
});

UnitContainer.defaultProps = {
  id: '',
  data: {},
};

UnitContainer.propTypes = {
  id: PropTypes.string,
  data: PropTypes.object,
};

const conUnitContainer = connect(
  unitMapStateToProps,
  unitMapDispatchToProps
)(UnitContainer);

export { conGroupContainer as GroupContainer };
export { conUnitContainer as UnitContainer };
