import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { connect, preferredUnits } from '@daikin-dama/redux-iot';
import PropTypes from 'prop-types';

import { Colors } from '@module/daikin-ui';
import AlexaLinkScreen from '../screens/SmartSpeaker/alexaLink';
import AlexaAuthScreen from '../screens/SmartSpeaker/alexaAuth';
import AlexaCommandScreen from '../screens/SmartSpeaker/alexaCommand';
import AlexaLWAScreen from '../screens/SmartSpeaker/alexaLWA';
import AlexaScreen from '../screens/SmartSpeaker/preferredUnits';
import AlexaManualScreen from '../screens/SmartSpeaker/alexaManual';

import {
  ALEXA_SCREEN,
  ALEXA_LINK_SCREEN,
  ALEXA_AUTH_SCREEN,
  ALEXA_COMMAND_SCREEN,
  ALEXA_LWA_SCREEN,
  ALEXA_MANUAL_SCREEN,
} from '../constants/routeNames';

const Stack = createStackNavigator();

const AlexaStack = ({ isAlexaLinked, fetchAlexaCommandUrl }) => {
  useEffect(() => {
    fetchAlexaCommandUrl();
  }, []);
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: { backgroundColor: Colors.white },
      }}>
      {isAlexaLinked ? (
        <Stack.Screen name={ALEXA_SCREEN} component={AlexaScreen} />
      ) : (
        <Stack.Screen name={ALEXA_LINK_SCREEN} component={AlexaLinkScreen} />
      )}
      <Stack.Screen name={ALEXA_AUTH_SCREEN} component={AlexaAuthScreen} />
      <Stack.Screen name={ALEXA_LWA_SCREEN} component={AlexaLWAScreen} />
      <Stack.Screen
        name={ALEXA_COMMAND_SCREEN}
        component={AlexaCommandScreen}
      />
      <Stack.Screen name={ALEXA_MANUAL_SCREEN} component={AlexaManualScreen} />
    </Stack.Navigator>
  );
};

const mapStateToProps = ({ preferredUnits: { alexaLinkStatus } }) => ({
  isAlexaLinked: alexaLinkStatus === 'activateSuccess',
});

const mapDispatchToProps = dispatch => ({
  fetchAlexaCommandUrl: () => dispatch(preferredUnits.fetchAlexaCommandUrl()),
});

AlexaStack.propTypes = {
  isAlexaLinked: PropTypes.bool,
  fetchAlexaCommandUrl: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(AlexaStack);
