import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import {
  BLUETOOTH_SCREEN,
  FORGOT_PASSWORD_SCREEN,
  LOGIN_SCREEN,
  OTHER_MORE_REMOTE_SCREEN,
  PRIVACY_POLICY_SCREEN,
  RESET_PASSWORD_SCREEN,
  SIGNUP_SCREEN,
  SIGN_UP_VERIFICATION_SCREEN,
  WIFI_SCREEN,
} from 'common/constants/routeNames';
import Colors from 'components/RemoteBottomTab/node_modules/common/shared/Colors';

import LoginScreen from '../screens/login';
import SignUpScreen from '../screens/signUp';
import SignUpVerificationScreen from '../screens/signUpVerification';
import ForgotPassword from '../screens/forgotPassword';
import ResetPassword from '../screens/resetPassword';
import Wifi from '../screens/wlanMode';
import Bluetooth from '../screens/bluetoothMode';
import RemoteScreen from '../screens/RemoteControl/remote';
import PrivacyPolicyScreen from '../screens/privacyPolicy';

const Stack = createStackNavigator();

const AuthStack = () => (
  <Stack.Navigator
    screenOptions={{
      cardStyle: { backgroundColor: Colors.white },
    }}>
    <Stack.Screen name={LOGIN_SCREEN} component={LoginScreen} />
    <Stack.Screen
      name={PRIVACY_POLICY_SCREEN}
      component={PrivacyPolicyScreen}
    />
    <Stack.Screen name={SIGNUP_SCREEN} component={SignUpScreen} />
    <Stack.Screen
      name={SIGN_UP_VERIFICATION_SCREEN}
      component={SignUpVerificationScreen}
    />
    <Stack.Screen name={FORGOT_PASSWORD_SCREEN} component={ForgotPassword} />
    <Stack.Screen name={RESET_PASSWORD_SCREEN} component={ResetPassword} />
    <Stack.Screen name={WIFI_SCREEN} component={Wifi} />
    <Stack.Screen name={BLUETOOTH_SCREEN} component={Bluetooth} />
    <Stack.Screen name={OTHER_MORE_REMOTE_SCREEN} component={RemoteScreen} />
  </Stack.Navigator>
);

export default AuthStack;
