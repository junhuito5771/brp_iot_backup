import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Colors } from '@module/daikin-ui';
import {
  AGREEMENT_SCREEN,
  APP_SETTINGS_SCREEN,
  QRCODE_VALIDITY_SCREEN,
} from '../constants/routeNames';
import SettingsScreen from '../screens/AppSetting/settings';
import AgreementScreen from '../screens/AppSetting/userAgreement';
import QRCodeValidityScreen from '../screens/AppSetting/qrCodeValidity';

const Stack = createStackNavigator();

const SettingsStack = () => (
  <Stack.Navigator
    screenOptions={{
      cardStyle: { backgroundColor: Colors.white },
    }}>
    <Stack.Screen name={APP_SETTINGS_SCREEN} component={SettingsScreen} />
    <Stack.Screen name={AGREEMENT_SCREEN} component={AgreementScreen} />
    <Stack.Screen
      name={QRCODE_VALIDITY_SCREEN}
      component={QRCodeValidityScreen}
    />
  </Stack.Navigator>
);

export default SettingsStack;
