import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet } from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import Animated from 'react-native-reanimated';
import { ScaleText, widthPercentage } from '@module/utility';

import { Colors } from '@module/daikin-ui';
import {
  EDIT_WEEKLY_TIMER_SCREEN,
  TEMP_SLIDER_SCREEN,
  TIME_SLIDER_SCREEN,
} from '../constants/routeNames';

import WeeklyTimerScreen from '../screens/WeeklyTimer/weeklyTimer';
import EditWeeklyTimerScreen from '../screens/WeeklyTimer/EditWeeklyTimer';
import TempSliderScreen from '../screens/WeeklyTimer/EditWeeklyTimer/tempSlider';
import TimeSliderScreen from '../screens/WeeklyTimer/EditWeeklyTimer/timeSlider';

const styles = StyleSheet.create({
  indicator: {
    backgroundColor: Colors.blue,
    position: 'absolute',
    left: 0,
    bottom: -3,
    right: 0,
    height: 3,
  },
  container: {
    backgroundColor: Colors.stoneGrey,
    justifyContent: 'center',
    height: 40,
    paddingHorizontal: widthPercentage(5),
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  tab: {
    flexGrow: 1,
    justifyContent: 'center',
    padding: 0,
  },
  label: {
    fontFamily: 'Roboto-bold',
    fontSize: ScaleText(14),
    marginTop: 0,
    color: Colors.midGrey,
    fontWeight: 'bold',
    textTransform: 'capitalize',
  },
  indicatorStyle: {
    backgroundColor: Colors.blue,
    paddingHorizontal: widthPercentage(5),
    bottom: -3,
    height: 3,
  },
});

const indicatorOffset = widthPercentage(2);
const indicatorHalfOffset = indicatorOffset / 2;

const renderIndicator = props => {
  const { width, position, navigationState } = props;
  const paddingOffset = styles.indicatorStyle.paddingHorizontal;
  const paddingHorizontalOffset = styles.indicatorStyle.paddingHorizontal * 2;
  const indicatorPaddingOffset =
    paddingHorizontalOffset / navigationState.routes.length;
  const indicatorWidth =
    widthPercentage(parseFloat(width)) -
    indicatorPaddingOffset -
    indicatorOffset;

  const translateX = Animated.add(
    Animated.multiply(
      Animated.interpolate(position, {
        inputRange: [-1, navigationState.routes.length],
        outputRange: [-1, navigationState.routes.length],
        extrapolate: 'clamp',
      }),
      indicatorWidth + indicatorOffset
    ),
    paddingOffset + indicatorHalfOffset
  );

  return (
    <Animated.View
      style={[
        styles.indicator,
        {
          width: indicatorWidth > 0 ? indicatorWidth : 0.1,
          transform: [{ translateX }],
        },
        props.indicatorStyle,
      ]}
    />
  );
};

renderIndicator.propTypes = {
  position: PropTypes.any,
  navigationState: PropTypes.object,
  indicatorStyle: PropTypes.object,
  width: PropTypes.number,
};

const dayIndexMap = {
  Su: 0,
  Mo: 1,
  Tu: 2,
  We: 3,
  Th: 4,
  Fr: 5,
  Sa: 6,
};

const Tab = createMaterialTopTabNavigator();

const TimerTabs = () => (
  <Tab.Navigator
    initialRouteName="Su"
    tabBarOptions={{
      renderIndicator,
      allowFontScaling: false,
      activeTintColor: Colors.blue,
      inactiveTintColor: Colors.black,
      style: styles.container,
      indicatorStyle: styles.indicator,
      tabStyle: styles.tab,
      labelStyle: styles.label,
    }}
    lazy>
    {Object.keys(dayIndexMap).map(dayName => (
      <Tab.Screen
        key={dayName}
        name={dayName}
        component={WeeklyTimerScreen}
        initialParams={{
          dayIndex: dayIndexMap[dayName],
        }}
      />
    ))}
  </Tab.Navigator>
);

const Stack = createStackNavigator();

const timerStack = () => (
  <Stack.Navigator
    screenOptions={{
      cardStyle: { backgroundColor: Colors.white },
    }}>
    <Stack.Screen name="TimerTab" component={TimerTabs} />
    <Stack.Screen
      name={EDIT_WEEKLY_TIMER_SCREEN}
      component={EditWeeklyTimerScreen}
    />
    <Stack.Screen name={TEMP_SLIDER_SCREEN} component={TempSliderScreen} />
    <Stack.Screen name={TIME_SLIDER_SCREEN} component={TimeSliderScreen} />
  </Stack.Navigator>
);

export default timerStack;
