import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Colors } from '@module/daikin-ui';
import { USAGE_GRAPH_SCREEN, USAGE_LIST_SCREEN } from '../constants/routeNames';

import UsageListScreen from '../screens/Usage/usageList';
import UsageGraphScreen from '../screens/Usage/usageGraph';
// import ViewPagerScreen from '../screens/EnergyConsumption/viewPager';

const Stack = createStackNavigator();

const EnergyStack = () => (
  <Stack.Navigator
    screenOptions={{
      cardStyle: { backgroundColor: Colors.white },
    }}>
    <Stack.Screen name={USAGE_LIST_SCREEN} component={UsageListScreen} />
    <Stack.Screen name={USAGE_GRAPH_SCREEN} component={UsageGraphScreen} />
  </Stack.Navigator>
);

export default EnergyStack;
