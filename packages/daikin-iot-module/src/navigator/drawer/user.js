import {
  HOME_SCREEN,
  WEEKLY_TIMER_SCREEN,
  OPERATION_MANUAL_SCREEN,
  SUBSCRIPTION_SCREEN,
  USAGE_LIST_SCREEN,
  ALEXA_SCREEN,
  APP_SETTINGS_SCREEN,
} from '../../constants/routeNames';

import RemoteHomeStack from '../home';
import WeeklyTimerStack from '../weeklyTimer';
import OperationManualScreen from '../../screens/RemoteHome/operationManual';
import SubscriptionStack from '../subscription';
import UsageGraphStack from '../usage';
import SmartSpeakerStack from '../alexa';
import AppSettingStack from '../settings';

const Drawer = {
  [HOME_SCREEN]: {
    name: HOME_SCREEN,
    label: 'Smart Control',
    component: RemoteHomeStack,
    icon: require('../../shared/assets/sidebar/remote.png'),
  },
  [WEEKLY_TIMER_SCREEN]: {
    name: WEEKLY_TIMER_SCREEN,
    label: 'Weekly Timer',
    component: WeeklyTimerStack,
    icon: require('../../shared/assets/sidebar/schedule.png'),
  },
  [USAGE_LIST_SCREEN]: {
    name: USAGE_LIST_SCREEN,
    label: 'Usage',
    component: UsageGraphStack,
    icon: require('../../shared/assets/sidebar/energy.png'),
  },
  [OPERATION_MANUAL_SCREEN]: {
    name: OPERATION_MANUAL_SCREEN,
    label: 'Operation Manual',
    component: OperationManualScreen,
    icon: require('../../shared/assets/sidebar/questionMark.png'),
  },
  [ALEXA_SCREEN]: {
    name: ALEXA_SCREEN,
    label: 'Smart Speaker',
    component: SmartSpeakerStack,
    icon: require('../../shared/assets/sidebar/alexa.png'),
  },
  [SUBSCRIPTION_SCREEN]: {
    name: SUBSCRIPTION_SCREEN,
    label: 'Subscription',
    component: SubscriptionStack,
    icon: require('../../shared/assets/sidebar/subscription.png'),
  },
  [APP_SETTINGS_SCREEN]: {
    name: APP_SETTINGS_SCREEN,
    label: 'Settings',
    component: AppSettingStack,
    icon: require('../../shared/assets/sidebar/appsettings.png'),
  },
};

export default Drawer;
