import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import SubscriptionScreen from '../screens/Subscription/subscriptionScreen';
import PlanDetailScreen from '../screens/Subscription/planDetailScreen';
import PricingSummaryScreen from '../screens/Subscription/pricingSummaryScreen';

const Stack = createStackNavigator();

const SubscriptionStack = () => (
  <Stack.Navigator>
    <Stack.Screen name="Subscription" component={SubscriptionScreen} />
    <Stack.Screen name="PlanDetail" component={PlanDetailScreen} />
    <Stack.Screen name="PricingSummary" component={PricingSummaryScreen} />
  </Stack.Navigator>
);

export default SubscriptionStack;
