import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { Colors } from '@module/daikin-ui';
import {
  BIND_UNIT_NO_QR_SCREEN,
  DEVICE_CONFIG_SCREEN,
  DEVICE_ICON_CONFIG_SCREEN,
  EDIT_HOMEPAGE_SCREEN,
  EDIT_ICONS_SCREEN,
  ERROR_HISTORY_SCREEN,
  HOME_SCREEN,
  OPERATION_MANUAL_SCREEN,
  QRCODE_SHARE_SCREEN,
  QR_SCANNER_SCREEN,
  REMOTE_SCREEN,
  SELECT_GROUP_SCREEN,
  SSID_SELECTION_SCREEN,
  UNBIND_GUEST_SCREEN,
  UNIT_DETAILS_SCREEN,
  UNIT_USERS_SCREEN,
  UNIT_USER_SCREEN,
  WIFI_CONFIG_SCREEN,
  LIMIT_USAGE_SCREEN,
  BRP_PAIRING_INSTRUCT_SCREEN,
  BRP_PAIRING_INSTRUCT2_SCREEN,
  BRP_PAIRING_INSTRUCT3_SCREEN,
  BRP_PAIRING_INSTRUCT4_SCREEN,
  BRP_PAIRING_INSTRUCT5_SCREEN,
  NETWORK_DEBUG_SCREEN,
} from '../constants/routeNames';

import HomeScreen from '../screens/RemoteHome/home';
import OperationManualScreen from '../screens/RemoteHome/operationManual';
import QrScanScreen from '../screens/RemotePair/qrScan';
import BindUnitNoQRScreen from '../screens/RemotePair/bindUnitNoQR';
import EditHomepageScreen from '../screens/EditHomepage/editHomepage';
import RemoteScreen from '../screens/RemoteControl/remote';
import WifiConfigScreen from '../screens/RemotePair/wifiConfig';
import DeviceConfigScreen from '../screens/RemotePair/deviceConfig';
import DeviceConfigIconScreen from '../screens/RemotePair/deviceIconConfig';
import SSIDSelectionScreen from '../screens/RemotePair/ssidSelection';
import SelectGroupScreen from '../screens/RemotePair/selectGroup';
import UnitDetailsScreen from '../screens/RemoteSetting/unitDetails';
import EditIconsScreen from '../screens/EditHomepage/editIcons';

import ErrorHistoryScreen from '../screens/RemoteSetting/errorHistory';
import UnitUsersScreen from '../screens/RemoteSetting/unitUsers';
import UnitUserScreen from '../screens/RemoteSetting/unitUser';
import QRCodeShareScreen from '../screens/RemoteSetting/shareQR';
import UnbindGuestScreen from '../screens/RemoteSetting/unbindGuest';

import LimitScreenScreen from '../screens/RemoteSetting/limitUsage';


import BrpPairingInstructScreen from '../screens/RemotePair/brpPairingInstruct';     // ThisLine Oscar: DIT Wi-Fi Integration - Pairing handling
import BrpPairingInstruct2Screen from '../screens/RemotePair/brpPairingInstruct2';     // ThisLine Oscar: DIT Wi-Fi Integration - Pairing handling
import BrpPairingInstruct3Screen from '../screens/RemotePair/brpPairingInstruct3';     // ThisLine Oscar: DIT Wi-Fi Integration - Pairing handling
import BrpPairingInstruct4Screen from '../screens/RemotePair/brpPairingInstruct4';     // ThisLine Oscar: DIT Wi-Fi Integration - Pairing handling
import BrpPairingInstruct5Screen from '../screens/RemotePair/brpPairingInstruct5';     // ThisLine Oscar: DIT Wi-Fi Integration - Pairing handling
import NetworkDebugScreen from '../screens/RemotePair/networkDebug';     // ThisLine Oscar: DIT Wi-Fi Integration - Pairing handling




const Stack = createStackNavigator();

const HomeStack = () => (
  <Stack.Navigator
    screenOptions={{
      cardStyle: { backgroundColor: Colors.white },
    }}>
    <Stack.Screen name={HOME_SCREEN} component={HomeScreen} />
    <Stack.Screen
      name={OPERATION_MANUAL_SCREEN}
      component={OperationManualScreen}
    />
	
	
	<Stack.Screen name={BRP_PAIRING_INSTRUCT_SCREEN} component={BrpPairingInstructScreen} />
	<Stack.Screen name={BRP_PAIRING_INSTRUCT2_SCREEN} component={BrpPairingInstruct2Screen} />
	<Stack.Screen name={BRP_PAIRING_INSTRUCT3_SCREEN} component={BrpPairingInstruct3Screen} />
	<Stack.Screen name={BRP_PAIRING_INSTRUCT4_SCREEN} component={BrpPairingInstruct4Screen} />
	<Stack.Screen name={BRP_PAIRING_INSTRUCT5_SCREEN} component={BrpPairingInstruct5Screen} />
	<Stack.Screen name={NETWORK_DEBUG_SCREEN} component={NetworkDebugScreen} />

	
    <Stack.Screen name={QR_SCANNER_SCREEN} component={QrScanScreen} />

    <Stack.Screen
      name={BIND_UNIT_NO_QR_SCREEN}
      component={BindUnitNoQRScreen}
    />
    <Stack.Screen name={EDIT_HOMEPAGE_SCREEN} component={EditHomepageScreen} />
    <Stack.Screen name={REMOTE_SCREEN} component={RemoteScreen} />
    <Stack.Screen name={WIFI_CONFIG_SCREEN} component={WifiConfigScreen} />
    <Stack.Screen name={DEVICE_CONFIG_SCREEN} component={DeviceConfigScreen} />
    <Stack.Screen
      name={DEVICE_ICON_CONFIG_SCREEN}
      component={DeviceConfigIconScreen}
    />
    <Stack.Screen
      name={SSID_SELECTION_SCREEN}
      component={SSIDSelectionScreen}
    />

    <Stack.Screen name={SELECT_GROUP_SCREEN} component={SelectGroupScreen} />
    <Stack.Screen name={UNIT_DETAILS_SCREEN} component={UnitDetailsScreen} />
    <Stack.Screen name={EDIT_ICONS_SCREEN} component={EditIconsScreen} />

    <Stack.Screen name={ERROR_HISTORY_SCREEN} component={ErrorHistoryScreen} />
    <Stack.Screen name={UNIT_USERS_SCREEN} component={UnitUsersScreen} />
    <Stack.Screen name={UNIT_USER_SCREEN} component={UnitUserScreen} />
    <Stack.Screen name={LIMIT_USAGE_SCREEN} component={LimitScreenScreen} /> 
    <Stack.Screen name={QRCODE_SHARE_SCREEN} component={QRCodeShareScreen} />
    <Stack.Screen name={UNBIND_GUEST_SCREEN} component={UnbindGuestScreen} />
  </Stack.Navigator>
);

export default HomeStack;
