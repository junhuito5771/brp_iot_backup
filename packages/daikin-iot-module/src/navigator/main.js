import React from 'react';
import { Image, Alert, StyleSheet } from 'react-native';
import { getActiveChildNavigationOptions } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import { connect, userProfile } from '@daikin-dama/redux-iot';
import { ScaleText } from '@module/utility';

import Text from 'common/components/TextDrawer';
import DrawerMenuContainer from 'common/components/Drawer';
import Colors from 'components/RemoteBottomTab/node_modules/common/shared/Colors';
import NavDrawerMenu from 'common/container/NavDrawerMenu';
import { LOGIN_SCREEN, LOGOUT_SCREEN } from 'common/constants/routeNames';

import HomeStack from 'iot/navigator/home';
import UsageStack from 'iot/navigator/usage';
import TimerTabs from 'iot/navigator/weeklyTimer';
import SettingsStack from 'iot/navigator/settings';
import HomeScreen from 'iot/screens/home';
import UserProfileScreen from 'iot/screens/userProfile';
import OperationManualScreen from 'iot/constants/operationManual';
import AlexaStack from 'iot/navigator/alexa';

import NavigationService from 'components/RemoteBottomTab/node_modules/app/navigationService';

const styles = StyleSheet.create({
  manualIcon: {
    width: 20,
    height: 20,
  },
});

const handleOnItemPressDrawerItem = (dispatch, navigation) => ({
  route: { routeName },
}) => {
  if (routeName === LOGOUT_SCREEN) {
    Alert.alert('Log out', 'Are you sure you want to logout?', [
      { text: 'Cancel' },
      {
        text: 'OK',
        onPress: () => {
          dispatch(userProfile.logOut());
          NavigationService.navigate(LOGIN_SCREEN);
        },
      },
    ]);
  } else {
    const { state } = navigation;
    const isEditing =
      state.routes[state.index].params &&
      state.routes[state.index].params.isEditing;

    if (isEditing) {
      Alert.alert(
        'Are you sure?',
        `You have unsaved changes on your ${
          state.routes[state.index].params.screen
        }. Do you want to leave before saving?`,
        [
          {
            text: 'No',
            onPress: () => {
              navigation.closeDrawer();
            },
          },
          {
            text: 'Yes',
            onPress: () => {
              NavigationService.navigate(routeName);
            },
          },
        ]
      );
    } else {
      NavigationService.navigate(routeName);
    }
  }
};

const ConnectedDrawerMenu = connect()(DrawerMenuContainer);

const DrawerStack = createDrawerNavigator(
  {
    Home: {
      screen: HomeStack,
      navigationOptions: ({ navigation, screenProps }) => ({
        drawerLabel: <Text>Home</Text>,
        drawerIcon: () => (
          <Image source={require('common/shared/images/sidebar/home.png')} />
        ),
        ...getActiveChildNavigationOptions(navigation, screenProps),
      }),
    },
    WeeklyTimer: {
      screen: TimerTabs,
      navigationOptions: ({ navigation, screenProps }) => ({
        drawerLabel: <Text>Weekly Timer</Text>,
        drawerIcon: () => (
          <Image
            source={require('common/shared/images/sidebar/schedule.png')}
          />
        ),
        ...getActiveChildNavigationOptions(navigation, screenProps),
      }),
    },
    Statistics: {
      screen: UsageStack,
      navigationOptions: ({ navigation, screenProps }) => ({
        drawerLabel: <Text>Usage</Text>,
        drawerIcon: () => (
          <Image source={require('common/shared/images/sidebar/energy.png')} />
        ),
        ...getActiveChildNavigationOptions(navigation, screenProps),
      }),
    },
    Profile: {
      screen: UserProfileScreen,
      navigationOptions: () => ({
        drawerLabel: <Text>Profile</Text>,
        drawerIcon: () => (
          <Image source={require('common/shared/images/sidebar/profile.png')} />
        ),
        defaultNavigationOptions: {
          cardStyle: {
            backgroundColor: 'white',
          },
        },
      }),
    },
    Alexa: {
      screen: AlexaStack,
      navigationOptions: () => ({
        drawerLabel: <Text>Preffered Units</Text>,
        drawerIcon: () => (
          <Image source={require('common/shared/images/sidebar/profile.png')} />
        ),
        defaultNavigationOptions: {
          cardStyle: {
            backgroundColor: 'white',
          },
        },
      }),
    },
    AppSettings: {
      screen: SettingsStack,
      navigationOptions: ({ navigation, screenProps }) => ({
        drawerLabel: <Text>App Settings</Text>,
        drawerIcon: () => (
          <Image
            source={require('common/shared/images/sidebar/appsettings.png')}
          />
        ),
        ...getActiveChildNavigationOptions(navigation, screenProps),
      }),
    },
    OperationManual: {
      screen: OperationManualScreen,
      navigationOptions: () => ({
        drawerLabel: <Text>Operation Manual</Text>,
        drawerIcon: () => (
          <Image
            style={styles.manualIcon}
            source={require('common/shared/images/sidebar/questionMark.png')}
          />
        ),
      }),
    },
    Logout: {
      screen: HomeScreen,
      navigationOptions: () => ({
        drawerLabel: <Text>Logout</Text>,
        drawerIcon: () => (
          <Image source={require('common/shared/images/sidebar/logout.png')} />
        ),
      }),
    },
  },
  {
    contentComponent: props => (
      <ConnectedDrawerMenu
        handleOnItemPress={handleOnItemPressDrawerItem}
        {...props}
      />
    ),
    drawerWidth: 280,
  }
);

const MainStack = createStackNavigator(
  {
    Drawer: {
      screen: DrawerStack,
      navigationOptions: navigator => {
        const { navigation, screenProps } = navigator;
        const title = navigation.state.routes[navigation.state.index].routeName;
        return {
          title: title.match(/[A-Z][a-z]+|[0-9]+/g).join(' '),
          headerLeft: () => <NavDrawerMenu navigation={navigation} />,
          ...getActiveChildNavigationOptions(navigation, screenProps),
        };
      },
    },
  },
  {
    defaultNavigationOptions: {
      headerTitleContainerStyle: {
        justifyContent: 'flex-start',
        flexDirection: 'row',
        alignItems: 'center',
      },
      headerStyle: {
        backgroundColor: Colors.veryLightGrey,
      },
      headerTitleStyle: {
        color: Colors.black,
        fontFamily: 'Roboto',
        fontWeight: '400',
        fontSize: ScaleText(16),
      },
      headerTitleAllowFontScaling: false,
      cardStyle: {
        backgroundColor: 'white',
      },
      headerTitleAlign: 'left',
    },
    headerMode: 'float',
  }
);

export default MainStack;
