import { ShadowValueHelper, RemoteHelper } from '@daikin-dama/redux-iot';
import { SwingIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.SWING_OFF]: SwingIcons.SWING_OFF,
  [RemoteHelper.SWING_ON]: SwingIcons.SWING_ON,
  [RemoteHelper.SWING_UD_LV1]: SwingIcons.SWING_UD_LV1,
  [RemoteHelper.SWING_UD_LV2]: SwingIcons.SWING_UD_LV2,
  [RemoteHelper.SWING_UD_LV3]: SwingIcons.SWING_UD_LV3,
  [RemoteHelper.SWING_UD_LV4]: SwingIcons.SWING_UD_LV4,
  [RemoteHelper.SWING_UD_LV5]: SwingIcons.SWING_UD_LV5,
  [RemoteHelper.SWING_UD_FULL]: SwingIcons.SWING_UD_FULL,
  [RemoteHelper.SWING_UD_OFF]: SwingIcons.SWING_UD_OFF,
};

export default function getSwingIconByShadowValue({
  modeConfig,
  enableUDStep,
}) {
  const { swing, upDownSwing } = modeConfig;

  const key = ShadowValueHelper.getSwingByShadowVal({
    swing,
    upDownSwing,
    enableUDStep,
  });

  return shadValIconMap[key] || shadValIconMap[RemoteHelper.SWING_OFF];
}
