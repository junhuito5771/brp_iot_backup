import fanSpdIconMapper from './fanSpdIconMapper';
import turboIconMapper from './turboIconMapper';
import ecoplusIconMapper from './ecoplusIconMapper';
import sleepIconMapper from './sleepIconMapper';
import swingIconMapper from './swingIconMapper';
import swingLRIconMapper from './swingLRIconMapper';
import swing3DIconMapper from './swing3DMapper';
import breezeIconMapper from './breezeIconMapper';
import smartDriftIconMapper from './smartDriftIconMapper';
import streamerIconMapper from './streamerIconMapper';
import senseIconMapper from './senseIconMapper';
import ledIconMapper from './ledIconMapper';
import ckSwingIconMapper from './ckSwingIconMapper';

export {
  fanSpdIconMapper,
  turboIconMapper,
  ecoplusIconMapper,
  sleepIconMapper,
  swingIconMapper,
  swingLRIconMapper,
  swing3DIconMapper,
  breezeIconMapper,
  smartDriftIconMapper,
  streamerIconMapper,
  senseIconMapper,
  ledIconMapper,
  ckSwingIconMapper,
};
