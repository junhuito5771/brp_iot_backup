import { RemoteHelper } from '@daikin-dama/redux-iot';
import { BreezeIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.BREEZE_OFF]: BreezeIcons.BREEZE_OFF,
  [RemoteHelper.BREEZE_ON]: BreezeIcons.BREEZE_ON,
};

export default function getBreezeIconByShadowValue({
  enableBreeze,
  modeConfig,
}) {
  const { breeze } = modeConfig;

  if (enableBreeze && breeze === 1) {
    return shadValIconMap[RemoteHelper.BREEZE_ON];
  }

  return shadValIconMap[RemoteHelper.BREEZE_OFF];
}
