import { RemoteHelper } from '@daikin-dama/redux-iot';
import { SmartDriftIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.SMART_DRIFT_OFF]: SmartDriftIcons.SMART_DRIFT_OFF,
  [RemoteHelper.SMART_DRIFT_ON]: SmartDriftIcons.SMART_DRIFT_ON,
};

export default function getSmartDriftIconByShadowValue({ modeConfig }) {
  const { smartDrift } = modeConfig;

  if (smartDrift === 1) return shadValIconMap[RemoteHelper.SMART_DRIFT_ON];

  return shadValIconMap[RemoteHelper.SMART_DRIFT_OFF];
}
