import { RemoteHelper } from '@daikin-dama/redux-iot';
import { SenseIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.SENSE_OFF]: SenseIcons.SENSE_OFF,
  [RemoteHelper.SENSE_ON]: SenseIcons.SENSE_ON,
};

export default function getSenseIconByShadowValue({ modeConfig }) {
  const { sense } = modeConfig;

  if (sense === 1) {
    return shadValIconMap[RemoteHelper.SENSE_ON];
  }

  return shadValIconMap[RemoteHelper.SENSE_OFF];
}
