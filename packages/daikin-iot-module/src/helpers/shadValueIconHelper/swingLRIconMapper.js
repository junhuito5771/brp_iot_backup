import { ShadowValueHelper, RemoteHelper } from '@daikin-dama/redux-iot';
import { SwingIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.SWING_LR_LV1]: SwingIcons.SWING_LR_LV1,
  [RemoteHelper.SWING_LR_LV2]: SwingIcons.SWING_LR_LV2,
  [RemoteHelper.SWING_LR_LV3]: SwingIcons.SWING_LR_LV3,
  [RemoteHelper.SWING_LR_LV4]: SwingIcons.SWING_LR_LV4,
  [RemoteHelper.SWING_LR_LV5]: SwingIcons.SWING_LR_LV5,
  [RemoteHelper.SWING_LR_FULL]: SwingIcons.SWING_LR_FULL,
  [RemoteHelper.SWING_LR_OFF]: SwingIcons.SWING_LR_OFF,
};

export default function getSwingLRIconByShadowValue({ modeConfig }) {
  const { leftRightSwing } = modeConfig;

  const key = ShadowValueHelper.getSwingLRByShadowVal({
    leftRightSwing,
  });

  return shadValIconMap[key] || shadValIconMap[RemoteHelper.SWING_OFF];
}
