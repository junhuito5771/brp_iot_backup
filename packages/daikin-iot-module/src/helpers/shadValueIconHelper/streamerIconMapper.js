import { RemoteHelper } from '@daikin-dama/redux-iot';
import { StreamerIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.STREAMER_OFF]: StreamerIcons.STREAMER_OFF,
  [RemoteHelper.STREAMER_ON]: StreamerIcons.STREAMER_ON,
};

export default function getStreamerIconByShadowValue({ modeConfig }) {
  const { streamer } = modeConfig;

  if (streamer === 1) {
    return shadValIconMap[RemoteHelper.STREAMER_ON];
  }

  return shadValIconMap[RemoteHelper.STREAMER_OFF];
}
