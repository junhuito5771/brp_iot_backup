import { RemoteHelper } from '@daikin-dama/redux-iot';
import { SwingIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.SWING_3D_OFF]: SwingIcons.SWING_3D_OFF,
  [RemoteHelper.SWING_3D_ON]: SwingIcons.SWING_3D_ON,
};

export default function getSwingIconByShadowValue({ modeConfig }) {
  const { swing, upDownSwing, leftRightSwing } = modeConfig;

  if (swing === 1 && upDownSwing === 15 && leftRightSwing === 15) {
    return shadValIconMap[RemoteHelper.SWING_3D_ON];
  }

  return shadValIconMap[RemoteHelper.SWING_3D_OFF];
}
