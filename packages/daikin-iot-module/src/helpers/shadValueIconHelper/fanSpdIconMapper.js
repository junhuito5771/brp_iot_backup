import { ShadowValueHelper, RemoteHelper } from '@daikin-dama/redux-iot';
import { FanSpdIcons } from '../../constants/tabItemIcons';

const shadValDILIconMap = {
  [RemoteHelper.FANSPEED_AUTO]: FanSpdIcons.FAN_AUTO,
  [RemoteHelper.FANSPEED_AUTO_OFF]: FanSpdIcons.FAN_AUTO_OFF,
  [RemoteHelper.FANSPEED_OFF]: FanSpdIcons.FAN_DIL_OFF,
  [RemoteHelper.FANSPEED_LV1]: FanSpdIcons.FAN_DIL_LV1,
  [RemoteHelper.FANSPEED_LV2]: FanSpdIcons.FAN_DIL_LV2,
  [RemoteHelper.FANSPEED_LV3]: FanSpdIcons.FAN_DIL_LV3,
  [RemoteHelper.FANSPEED_LV4]: FanSpdIcons.FAN_DIL_LV4,
  [RemoteHelper.FANSPEED_LV5]: FanSpdIcons.FAN_DIL_LV5,
};

const shadValDAMAIconMap = {
  [RemoteHelper.FANSPEED_AUTO]: FanSpdIcons.FAN_AUTO,
  [RemoteHelper.FANSPEED_AUTO_OFF]: FanSpdIcons.FAN_AUTO_OFF,
  [RemoteHelper.FANSPEED_OFF]: FanSpdIcons.FAN_DAMA_OFF,
  [RemoteHelper.FANSPEED_LV1]: FanSpdIcons.FAN_DAMA_LV1,
  [RemoteHelper.FANSPEED_LV2]: FanSpdIcons.FAN_DAMA_LV2,
  [RemoteHelper.FANSPEED_LV3]: FanSpdIcons.FAN_DAMA_LV3,
};

export default function getFanIconByShadowValue({
  handsetType,
  modeConfig,
  mode,
}) {
  const { silent, turbo, smartTurbo, fan, fanExtend } = modeConfig;
  const interStateOff = turbo === 1 || smartTurbo === 1;

  if (silent === 1 && interStateOff) {
    return FanSpdIcons.FAN_SILENT_OFF;
  }
  if (silent === 1) return FanSpdIcons.FAN_SILENT;

  if (mode === RemoteHelper.MODE_DRY) return FanSpdIcons.FAN_AUTO;

  const shadValIconMap =
    handsetType === 0 ? shadValDAMAIconMap : shadValDILIconMap;

  const shadValIconKey = ShadowValueHelper.getFanLvlByShadowVal(
    handsetType,
    fan,
    fanExtend,
    modeConfig
  );

  return shadValIconMap[shadValIconKey] || shadValIconMap.auto;
}
