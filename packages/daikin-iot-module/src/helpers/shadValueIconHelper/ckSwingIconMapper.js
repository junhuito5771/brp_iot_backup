import { ShadowValueHelper, RemoteHelper } from '@daikin-dama/redux-iot';
import { ckSwingIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.CKSWING_FULL]: ckSwingIcons.CK_SWING_FULL,
  [RemoteHelper.CKSWING_SOIL]: ckSwingIcons.CK_SWING_SOIL,
  [RemoteHelper.CKSWING_DRAFT]: ckSwingIcons.CK_SWING_DRAFT,
};

export default function getCKSwingIconByShadowValue({ modeConfig }) {
  const { ckSwing } = modeConfig;

  const ckSwingKey = ShadowValueHelper.getCKSwingByShadowVal({ ckSwing });

  return shadValIconMap[ckSwingKey] || ckSwingIcons.CK_SWING_FULL;
}
