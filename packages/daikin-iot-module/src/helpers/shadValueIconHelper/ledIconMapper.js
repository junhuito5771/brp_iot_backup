import { ShadowValueHelper, RemoteHelper } from '@daikin-dama/redux-iot';
import { ledIcons } from '../../constants/tabItemIcons';

const shadValIconMap = {
  [RemoteHelper.LED_OFF]: ledIcons.LED_OFF,
  [RemoteHelper.LED_ON]: ledIcons.LED_ON,
  [RemoteHelper.POWER_IND]: ledIcons.POWER_IND_ON,
};

export default function getLEDIconByShadowValue({
  enableLED,
  enablePowerInd,
  modeConfig,
}) {
  const { led, powerInd } = modeConfig;

  const iconKey = ShadowValueHelper.getLEDByShadowVal({
    led,
    powerInd,
    enableLED,
    enablePowerInd,
  });

  return shadValIconMap[iconKey] || shadValIconMap[RemoteHelper.LED_OFF];
}
