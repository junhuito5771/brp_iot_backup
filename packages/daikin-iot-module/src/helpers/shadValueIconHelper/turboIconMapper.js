import { TurboIcons } from '../../constants/tabItemIcons';

export default function getTurboIconByShadowValue({
  enableTurbo,
  enableSmartTurbo,
  modeConfig,
}) {
  const { turbo, smartTurbo } = modeConfig;

  if (enableSmartTurbo && smartTurbo === 1) {
    return TurboIcons.TURBO_SMART_ON;
  }
  if (enableSmartTurbo && smartTurbo === 0) {
    return TurboIcons.TURBO_SMART_OFF;
  }
  if (enableTurbo && turbo === 1) {
    return TurboIcons.TURBO_ON;
  }

  return TurboIcons.TURBO_OFF;
}
