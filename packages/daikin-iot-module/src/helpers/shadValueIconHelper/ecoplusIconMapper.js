import { RemoteHelper } from '@daikin-dama/redux-iot';
import { EcoplusIcons } from '../../constants/tabItemIcons';

export default function getEcoIconByShadowValue({
  enableSmartEcomax,
  handsetType,
  modeConfig,
}) {
  const { ecoplus, smartEcoMax } = modeConfig;

  if (enableSmartEcomax && handsetType === RemoteHelper.HANDSET_DAMA) {
    return smartEcoMax === 1
      ? EcoplusIcons.ECOPLUS_SMART_ON
      : EcoplusIcons.ECOPLUS_SMART_OFF;
  }
  if (handsetType === RemoteHelper.HANDSET_DIL) {
    return ecoplus === 1 ? EcoplusIcons.ECONO_ON : EcoplusIcons.ECONO_OFF;
  }

  // Ecoplus also support to those units that has enableEcoplus = false
  return ecoplus === 1 ? EcoplusIcons.ECOPLUS_ON : EcoplusIcons.ECOPLUS_OFF;
}
