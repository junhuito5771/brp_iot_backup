import { SleepIcons } from '../../constants/tabItemIcons';

export default function getSleepIconByShadowValue({
  enableSleep,
  enableSmartSleep,
  modeConfig,
}) {
  const { sleep, smartSleep } = modeConfig;
  if (enableSmartSleep) {
    return smartSleep === 1
      ? SleepIcons.SLEEP_SMART_ON
      : SleepIcons.SLEEP_SMART_OFF;
  }
  if (enableSleep) {
    return sleep === 1 ? SleepIcons.SLEEP_ON : SleepIcons.SLEEP_OFF;
  }

  return SleepIcons.SLEEP_OFF;
}
