import modeIcons from '../constants/modeIcons';

export default function getModeIconByKey(mode) {
  return modeIcons[mode] || modeIcons.auto;
}
