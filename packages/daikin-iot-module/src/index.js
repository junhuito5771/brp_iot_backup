// eslint-disable-next-line import/prefer-default-export
import { configuration } from '@daikin-dama/redux-iot';
import BluetoothModeScreen from './screens/bluetoothMode';
import WlanModeScreen from './screens/wlanMode';
import RemoteControlScreen from './screens/RemoteControl/remote';
import LoginModeControl from './components/LoginModeControl';

const isIoTModule = true;

// eslint-disable-next-line import/prefer-default-export
export { Drawer } from './navigator';
export * from '@daikin-dama/redux-iot';

function init(config) {
  configuration.init({
    region: config.REGION,
    iotEndpoint: config.IOT_ENDPOINT,
  });
}

export { init };

export {
  BluetoothModeScreen,
  WlanModeScreen,
  RemoteControlScreen,
  LoginModeControl,
  isIoTModule,
};
