import React from 'react';
import {
  View,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
  StyleSheet,
  Image,
} from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import {
  connect,
  selector,
  usage,
  tutorialFlag as tutorialState,
} from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  ScaleText,
  verticalScale,
  widthPercentage,
  scale,
  NavigationService,
} from '@module/utility';
import PropTypes from 'prop-types';

import { Colors, Text, TutorialOverlay } from '@module/acson-ui';

import TapIcon from '../../shared/images/tutorial/TapIcon';

import { PERIOD_WEEKLY } from '../../constants/timePeriod';
import { USAGE_GRAPH_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  titleLabel: {
    fontWeight: '600',
    fontSize: ScaleText(18),
  },
  content: {
    height: heightPercentage(8),
    width: widthPercentage(85),
    justifyContent: 'center',
    backgroundColor: '#fff',
    paddingLeft: scale(20),
    paddingRight: scale(20),
    marginTop: verticalScale(7),
    borderRadius: 9,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  row1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  row2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: verticalScale(13),
  },
  grey: {
    color: Colors.grey,
  },
  marginTopLeft: {
    marginTop: 4,
    marginLeft: 2,
  },
  withBarName: {
    maxWidth: widthPercentage(60),
  },
  withoutBarName: {
    maxWidth: widthPercentage(70),
  },
  bar: {
    height: 14,
    backgroundColor: Colors.stoneGrey,
    borderRadius: 7,
    width: widthPercentage(60),
    overflow: 'hidden',
    marginLeft: scale(5),
    marginRight: scale(5),
  },
  barColorFill: {
    height: 14,
    backgroundColor: Colors.violet,
    top: 0,
  },
  blue: {
    color: Colors.violet,
  },
  energyIcon: {
    width: scale(15),
    height: scale(15),
    resizeMode: 'cover',
  },
  marginTop20: {
    marginTop: verticalScale(20),
  },
  margin30: {
    marginHorizontal: scale(30),
    marginVertical: verticalScale(20),
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    color: Colors.titleGrey,
  },
  flexRow: {
    flexDirection: 'row',
  },
  headerRight: {
    marginRight: 25,
    flexDirection: 'row',
  },
});

const onPressUnit = (
  thingName,
  acName,
  isInverter,
  enableEnergyConsumption
) => {
  NavigationService.navigate(USAGE_GRAPH_SCREEN, {
    thingName,
    acName,
    isInverter,
    enableEnergyConsumption,
  });
};
const setColor = consumption => {
  if (consumption === 0) return styles.grey;
  return styles.blue;
};

const getHeight = value => {
  if (value) return { height: heightPercentage(10) };
  return { height: heightPercentage(7) };
};

const calculateLength = length => {
  // Prevent the bar from overflow
  const ratio = Math.min(length / 5000, 1);
  return {
    width: widthPercentage(35) * ratio,
    borderTopRightRadius: 7.5,
    borderBottomRightRadius: 7.5,
  };
};

const ListUnit = ({ units, showBar }) =>
  units.map(unit => {
    const { unitName, usageValue, thingName, enableEnergyConsumption } = unit;
    return (
      <TouchableOpacity
        key={thingName}
        onPress={() =>
          onPressUnit(thingName, unitName, showBar, enableEnergyConsumption)
        }>
        <View style={[styles.content, getHeight(showBar)]}>
          <View style={styles.row1}>
            <View>
              <Text
                medium
                style={StyleSheet.flatten([
                  showBar && enableEnergyConsumption
                    ? styles.withBarName
                    : styles.withoutBarName,
                ])}
                numberOfLines={1}>
                {unitName}
              </Text>
            </View>
            {showBar && enableEnergyConsumption ? (
              <View style={styles.row2}>
                <Image
                  style={styles.energyIcon}
                  source={
                    usageValue > 0
                      ? require('../../shared/images/graph/energy_blue.png')
                      : require('../../shared/images/graph/energy_grey.png')
                  }
                />
                <Text style={[styles.marginTopLeft, setColor(usageValue)]}>
                  {`${
                    usageValue > 0 ? (usageValue / 1000).toFixed(2) : '--'
                  } kW`}
                </Text>
              </View>
            ) : null}
          </View>
          <View>
            {showBar && enableEnergyConsumption ? (
              <View style={styles.flexRow}>
                <Text mini style={styles.grey}>
                  0kW
                </Text>
                <View>
                  <View style={styles.bar}>
                    <View
                      style={[styles.barColorFill, calculateLength(usageValue)]}
                    />
                  </View>
                </View>
                <Text mini style={styles.grey}>
                  5kW
                </Text>
              </View>
            ) : null}
          </View>
        </View>
      </TouchableOpacity>
    );
  });

ListUnit.propTypes = {
  units: PropTypes.array,
  showBar: PropTypes.bool,
};

const handleInit = setPeriod => {
  const defaultPeriod = PERIOD_WEEKLY;
  setPeriod(defaultPeriod);
};

const UsageListScreen = ({
  navigation,

  usageList,
  setSelectedStatistic,

  tutorialFlag,
  setIsGraphNavigateDisplayed,
}) => {
  const isFocused = useIsFocused();

  // Tutorial Steps
  const stepsGroup = {
    navigateGraph: {
      position: { x: widthPercentage(30), y: heightPercentage(25) },
      icon: <TapIcon />,
      description: 'Tap to navigate\nto the graph',
      onClose: () => setIsGraphNavigateDisplayed(true),
    },
  };

  React.useEffect(() => {
    const onShowTutorialOverlay = () => {
      setIsGraphNavigateDisplayed(false);
    };

    if (usageList) {
      navigation.setParams({ onShowTutorialOverlay });
    }
  }, [usageList]);

  // Show differnet tutorial overlay based on flags
  const showTutorialOverlay = React.useCallback(() => {
    let currentStep = null;

    if (!tutorialFlag.isGraphNavigateDisplayed) {
      currentStep = stepsGroup.navigateGraph;
    }

    return (
      !!currentStep &&
      isFocused && (
        <TutorialOverlay
          iconRightAlign
          onClose={currentStep.onClose}
          visible={!!currentStep}
          currentStep={currentStep}
        />
      )
    );
  }, [tutorialFlag, stepsGroup, isFocused]);

  return (
    <>
      {showTutorialOverlay()}
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={styles.marginTop20}>
            <View style={styles.center}>
              <Text style={styles.label}>
                Tap an unit for to see the amount of power{' '}
              </Text>
            </View>
          </View>
          {usageList.map(({ title, data }, index) => {
            const isInverter = index === 0;

            // If the section does not have data, do not show
            if (data && data.length < 1) return null;

            return (
              <View key={title} style={styles.margin30}>
                <View>
                  <Text medium style={styles.titleLabel}>
                    {title}
                  </Text>
                </View>
                <ListUnit
                  units={data}
                  showBar={isInverter}
                  onPressRow={() => handleInit(setSelectedStatistic)}
                />
              </View>
            );
          })}
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

UsageListScreen.navigationOptions = ({ navigation }) => {
  const { params } = navigation.state;

  return {
    headerRight: () => (
      <View style={styles.headerRight}>
        {params && params.onShowTutorialOverlay && (
          <View>
            <TouchableOpacity
              hitSlop={styles.hitSlop}
              onPress={params.onShowTutorialOverlay}>
              <Image
                source={require('../../shared/images/general/help.png')}
                style={{ tintColor: Colors.linkGrey }}
              />
            </TouchableOpacity>
          </View>
        )}
      </View>
    ),
  };
};

UsageListScreen.propTypes = {
  navigation: PropTypes.object,

  usageList: PropTypes.array,
  setSelectedStatistic: PropTypes.func,
  tutorialFlag: PropTypes.shape({
    isGraphNavigateDisplayed: PropTypes.bool,
  }),
  setIsGraphNavigateDisplayed: PropTypes.func,
};

UsageListScreen.defaultProps = {
  usageList: [],
};

const mapDispatchToProps = dispatch => ({
  setSelectedStatistic: staType => {
    dispatch(usage.setSelectedStatistic(staType));
  },
  setSelectedPeriod: periodType => {
    dispatch(usage.setSelectedPeriod(periodType));
  },
  setIsGraphNavigateDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphNavigateDisplayed(isDisplayed));
  },
});

const mapStateToProps = state => ({
  usageList: selector.getUnitUsageList(state),
  tutorialFlag: state.tutorialFlag,
});

export default connect(mapStateToProps, mapDispatchToProps)(UsageListScreen);
