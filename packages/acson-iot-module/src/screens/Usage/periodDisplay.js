import React from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Image,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, tutorialFlag as tutorialState } from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  widthPercentage,
  ScaleText,
  timeConvert,
  verticalScale,
  scale,
} from '@module/utility';
import moment from 'moment';
import { Colors, Text, TutorialOverlay, Button } from '@module/acson-ui';

import TapIcon from '../../shared/images/tutorial/TapIcon';
import {
  PERIOD_DAILY,
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
} from '../../constants/timePeriod';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  justifySpace: {
    justifyContent: 'space-evenly',
  },
  column: {
    flexDirection: 'column',
  },
  displayText: {
    color: Colors.black,
    fontSize: 22,
    textAlign: 'center',
  },
  titleText: {
    fontSize: ScaleText(14),
    color: Colors.black,
    marginTop: heightPercentage(1),
  },
  content: {
    marginTop: heightPercentage(1),
    alignItems: 'flex-start',
  },
  labelText: {
    fontSize: 10,
    marginLeft: 5,
    marginBottom: 5,
    color: Colors.titleGrey,
  },
  loadingCenter: {
    justifyContent: 'center',
    alignSelf: 'center',
  },
  marginRight1: {
    marginRight: 3,
  },
  selfCenter: {
    alignSelf: 'center',
  },
  selfStart: {
    alignSelf: 'flex-start',
  },
  margin10: {
    margin: 10,
  },
  unitText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  marginTop5: {
    marginTop: 5,
  },
  periodContainer: {
    width: widthPercentage(95),
    height: heightPercentage(7),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  periodDisplayContainer: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    padding: 5,
  },
  periodDisplayIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
  },
  periodDisplayLabel: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  khaki: {
    color: Colors.khaki,
  },
  darkkhaki: {
    color: Colors.darkkhaki,
  },
  salmon: {
    color: Colors.salmon,
  },
  groupRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: heightPercentage(1.5),
    justifyContent: 'center',
  },
  rangeText: {
    fontSize: 12,
  },
  lightsalmon: {
    color: Colors.lightsalmon,
  },
  marginTop1: {
    marginTop: 1,
    marginLeft: 15,
    color: Colors.titleGrey,
    fontSize: ScaleText(14),
  },
  dateContainer: {
    backgroundColor: Colors.offWhite,
    height: heightPercentage(4),
    width: widthPercentage(38),
    borderRadius: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    padding: 5,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  marginRight5: {
    marginRight: 5,
  },
  buttonCompare: {
    marginHorizontal: scale(5),
  },
});

const buttonStyles = {
  height: verticalScale(28),
  width: scale(150),
  fontSize: { fontSize: ScaleText(12) },
};

const titleEnum = {
  daily: 'Today',
  weekly: 'This Week',
  monthly: 'This Month',
  yearly: 'This Year',
};

const PeriodDisplay = ({
  displayValue,
  displayAverageKwh,
  isInverter,
  isLoading,
  displayTnbRate,
  displayCpRT,
  enableEnergyConsumption,
  acName,
  periodType,
  selectDate,
  selectedFromDate,
  selectedFromWeek,
  selectedFromMonth,
  selectedFromYear,
  bottomModalRef,
  checkDateCompare,
  clearCompareData,
  thingName,

  // tutorials props
  tutorialFlag,
  setIsGraphCompareDateDisplayed,
  setIsGraphClearResultDisplayed,
  setIsGraphToggleIndicatorDisplayed,
  setIsGraphScrollLineIndicatorDisplayed,
  setIsGraphElectricityBillRateDisplayed,
  setIsGraphNavigatePeriodDisplayed,
}) => {
  const STANDARD_DATE_FORMAT = 'DD MMMM YYYY';
  const now = moment().format(STANDARD_DATE_FORMAT);
  const getDateFormatTo = date => {
    switch (periodType) {
      case PERIOD_WEEKLY: {
        if (selectedFromWeek === null) {
          return moment().endOf('isoWeek').format('DD MMM YYYY');
        }
        return moment(selectedFromWeek).endOf('isoWeek').format('DD MMM YYYY');
      }
      case PERIOD_MONTHLY:
        return moment(date, STANDARD_DATE_FORMAT).format('MMMM YYYY');
      case PERIOD_YEARLY:
        return moment(date, STANDARD_DATE_FORMAT).format('YYYY');
      default:
        return moment(date, STANDARD_DATE_FORMAT).format(STANDARD_DATE_FORMAT);
    }
  };

  const currentDateFrom = () => {
    if (selectedFromDate && periodType === PERIOD_DAILY) {
      return moment(selectedFromDate).format(STANDARD_DATE_FORMAT);
    }
    if (selectedFromWeek && periodType === PERIOD_WEEKLY) {
      const firstDayOfWeek = moment(selectedFromWeek)
        .startOf('isoWeek')
        .format('DD MMM YYYY');
      return firstDayOfWeek;
    }
    if (selectedFromMonth && periodType === PERIOD_MONTHLY) {
      return selectedFromMonth;
    }
    if (selectedFromYear && periodType === PERIOD_YEARLY) {
      return selectedFromYear;
    }

    return `Select ${selectDate(periodType)}`;
  };

  const disableCompare = () => {
    if (!selectedFromDate && periodType === PERIOD_DAILY) {
      return true;
    }
    if (!selectedFromWeek && periodType === PERIOD_WEEKLY) {
      return true;
    }
    if (!selectedFromMonth && periodType === PERIOD_MONTHLY) {
      return true;
    }
    if (!selectedFromYear && periodType === PERIOD_YEARLY) {
      return true;
    }
    return false;
  };

  const checkEnergyConsumption = () => {
    if (isInverter && enableEnergyConsumption) {
      return (
        <>
          <View style={styles.periodDisplayContainer}>
            {isLoading ? (
              <View style={styles.loadingCenter}>
                <ActivityIndicator size="small" />
              </View>
            ) : (
              <>
                <View style={styles.row}>
                  <Image
                    source={require('../../shared/images/graph/average_energy.png')}
                    style={styles.marginRight5}
                  />
                  <Text style={[styles.periodDisplayLabel, styles.khaki]}>
                    {`${displayAverageKwh}`}
                  </Text>
                  <Text style={styles.khaki}>kWh</Text>
                </View>
              </>
            )}
            <View style={{ alignSelf: 'center', flexDirection: 'row' }}>
              <Text style={styles.labelText}>(total)</Text>
            </View>
          </View>
          <View style={styles.periodDisplayContainer}>
            {isLoading ? (
              <View style={styles.loadingCenter}>
                <ActivityIndicator size="small" />
              </View>
            ) : (
              <>
                <View style={{ flexDirection: 'row' }}>
                  <Image
                    source={require('../../shared/images/graph/tnb_green.png')}
                    style={styles.marginRight5}
                  />
                  <Text style={styles.darkkhaki}>RM </Text>
                  <Text style={[styles.periodDisplayLabel, styles.darkkhaki]}>
                    {`${displayTnbRate}`}
                  </Text>
                </View>
              </>
            )}
            <View style={{ alignSelf: 'center', flexDirection: 'row' }}>
              <Text style={styles.labelText}>(total)</Text>
            </View>
          </View>
        </>
      );
    }
    return null;
  };

  const checkCompressorRunningTime = () => {
    if (!isInverter) {
      const { rhours, rminutes } = timeConvert(displayCpRT);
      return (
        <View style={[styles.column, styles.content]}>
          <View style={[styles.row, styles.selfStart]}>
            {isLoading ? (
              <View style={styles.loadingCenter}>
                <ActivityIndicator size="small" />
              </View>
            ) : (
              <>
                <Image
                  source={require('../../shared/images/graph/cprt_red.png')}
                  style={styles.marginRight5}
                />
                {rhours > 0 && (
                  <>
                    <Text style={[styles.periodDisplayLabel, styles.salmon]}>
                      {displayCpRT === undefined ? '-' : `${rhours}`}
                    </Text>
                    <Text style={styles.salmon}>hours</Text>
                  </>
                )}
                <Text style={[styles.periodDisplayLabel, styles.salmon]}>
                  {displayCpRT === undefined ? '-' : ` ${rminutes}`}
                </Text>
                <Text style={styles.salmon}>mins</Text>
              </>
            )}
          </View>
          <View style={[styles.row, styles.selfCenter]}>
            <Text style={styles.labelText}>(total)</Text>
          </View>
        </View>
      );
    }
    return null;
  };

  // Tutorial Steps
  const stepsGroup = {
    compareDate: {
      position: { x: widthPercentage(5), y: heightPercentage(30) },
      icon: <TapIcon />,
      description: 'Select date to\ncompare',
      onClose: () => setIsGraphCompareDateDisplayed(true),
    },
    clearResult: {
      position: { x: widthPercentage(30), y: heightPercentage(35) },
      icon: <TapIcon />,
      description: 'Tap clear to clear\nthe result',
      onClose: () => setIsGraphClearResultDisplayed(true),
    },
    toggleIndicator: {
      position: { x: widthPercentage(30), y: heightPercentage(70) },
      icon: <TapIcon />,
      description: 'Toggle to view the\nindicator value on\nthe graph',
      onClose: () => setIsGraphToggleIndicatorDisplayed(true),
    },
    scrollLineIndicator: {
      position: { x: widthPercentage(25), y: heightPercentage(50) },
      icon: <TapIcon />,
      description: 'Scroll the line to\nview the toggled\nindicator value',
      onClose: () => setIsGraphScrollLineIndicatorDisplayed(true),
    },
    electricityBillRate: {
      position: { x: widthPercentage(30), y: heightPercentage(85) },
      icon: <TapIcon />,
      description: 'Tap to select\nelectricity bill\nrate',
      onClose: () => setIsGraphElectricityBillRateDisplayed(true),
    },
    navigatePeriod: {
      position: { x: widthPercentage(30), y: heightPercentage(90) },
      icon: <TapIcon />,
      description: 'Tap to navigate between\ndifferent period',
      onClose: () => setIsGraphNavigatePeriodDisplayed(true),
    },
  };

  // Show differnet tutorial overlay based on flags
  const showTutorialOverlay = React.useCallback(() => {
    let currentStep = null;

    if (!tutorialFlag.isGraphNavigatePeriodDisplayed) {
      currentStep = stepsGroup.navigatePeriod;
    }
    if (!tutorialFlag.isGraphElectricityBillRateDisplayed) {
      currentStep = stepsGroup.electricityBillRate;
    }
    if (!tutorialFlag.isGraphScrollLineIndicatorDisplayed) {
      currentStep = stepsGroup.scrollLineIndicator;
    }
    if (!tutorialFlag.isGraphToggleIndicatorDisplayed) {
      currentStep = stepsGroup.toggleIndicator;
    }
    if (!tutorialFlag.isGraphClearResultDisplayed) {
      currentStep = stepsGroup.clearResult;
    }
    if (!tutorialFlag.isGraphCompareDateDisplayed) {
      currentStep = stepsGroup.compareDate;
    }
    return (
      !!currentStep && (
        <TutorialOverlay
          iconRightAlign
          onClose={currentStep.onClose}
          visible={!!currentStep}
          currentStep={currentStep}
        />
      )
    );
  }, [tutorialFlag, stepsGroup]);

  return (
    <>
      {showTutorialOverlay()}
      <View style={styles.margin10}>
        <Text style={styles.unitText}>{acName}</Text>
        <Text style={styles.marginTop5}>{titleEnum[periodType]}</Text>

        <View style={styles.periodContainer}>
          <View style={styles.periodDisplayContainer}>
            {isLoading ? (
              <View style={styles.loadingCenter}>
                <ActivityIndicator size="small" />
              </View>
            ) : (
              <View style={[styles.justifySpace, { flexDirection: 'row' }]}>
                <Image
                  source={require('../../shared/images/graph/indoor_temp_orange.png')}
                  style={[styles.periodDisplayIcon, styles.marginRight5]}
                />
                <Text style={[styles.periodDisplayLabel, styles.lightsalmon]}>
                  {displayValue || '-'}
                </Text>
                <Text style={styles.lightsalmon}> °C</Text>
              </View>
            )}
            <Text style={styles.labelText}>(average)</Text>
          </View>
          {checkEnergyConsumption()}
          {checkCompressorRunningTime()}
        </View>
      </View>

      <Text style={styles.marginTop1}>
        {`Select ${selectDate(periodType)} to compare`}{' '}
      </Text>

      <View style={styles.groupRow}>
        <Text style={styles.rangeText}>From </Text>
        <TouchableWithoutFeedback
          onPress={() => {
            bottomModalRef.current.open();
          }}>
          <View style={[styles.dateContainer, { marginRight: 20 }]}>
            <Text>{currentDateFrom()}</Text>
            <Image
              source={require('../../shared/images/general/collapse.png')}
              style={styles.collapseIcon}
            />
          </View>
        </TouchableWithoutFeedback>
        <Text style={styles.rangeText}>To </Text>
        <TouchableWithoutFeedback
          onPress={() => bottomModalRef.current.close()}>
          <View style={styles.dateContainer}>
            <Text>{getDateFormatTo(now)}</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
      <View style={styles.buttonContainer}>
        <Button
          containerStyle={styles.buttonCompare}
          height={buttonStyles.height}
          width={buttonStyles.width}
          textStyle={buttonStyles.fontSize}
          raised
          disabledRaised
          disabled={disableCompare()}
          onPress={() => {
            checkDateCompare();
          }}>
          Compare
        </Button>
        <Button
          containerStyle={styles.buttonCompare}
          disabled={disableCompare()}
          height={buttonStyles.height}
          width={buttonStyles.width}
          textStyle={buttonStyles.fontSize}
          onPress={() => {
            clearCompareData({ thingName, periodType });
          }}>
          Clear
        </Button>
      </View>
    </>
  );
};

PeriodDisplay.propTypes = {
  displayValue: PropTypes.string,
  displayAverageKwh: PropTypes.string,
  displayTnbRate: PropTypes.string,
  displayCpRT: PropTypes.string,
  isLoading: PropTypes.bool,
  enableEnergyConsumption: PropTypes.bool,
  isInverter: PropTypes.bool,
  acName: PropTypes.string,
  periodType: PropTypes.string,
  selectDate: PropTypes.any,
  selectedFromDate: PropTypes.any,
  selectedFromWeek: PropTypes.any,
  selectedFromMonth: PropTypes.any,
  selectedFromYear: PropTypes.any,
  bottomModalRef: PropTypes.any,
  checkDateCompare: PropTypes.func,
  clearCompareData: PropTypes.func,
  thingName: PropTypes.string,

  tutorialFlag: PropTypes.shape({
    isGraphCompareDateDisplayed: PropTypes.bool,
    isGraphClearResultDisplayed: PropTypes.bool,
    isGraphToggleIndicatorDisplayed: PropTypes.bool,
    isGraphScrollLineIndicatorDisplayed: PropTypes.bool,
    isGraphElectricityBillRateDisplayed: PropTypes.bool,
    isGraphNavigatePeriodDisplayed: PropTypes.bool,
  }),
  setIsGraphCompareDateDisplayed: PropTypes.func,
  setIsGraphClearResultDisplayed: PropTypes.func,
  setIsGraphToggleIndicatorDisplayed: PropTypes.func,
  setIsGraphScrollLineIndicatorDisplayed: PropTypes.func,
  setIsGraphElectricityBillRateDisplayed: PropTypes.func,
  setIsGraphNavigatePeriodDisplayed: PropTypes.func,
};

const mapStateToProps = (
  {
    usage: {
      isLoading,
      usageByPeriod,
      usageSetting: { thingName },
    },
    tutorialFlag,
  },
  { periodType }
) => {
  const usageInThingName = usageByPeriod[thingName] || {};
  const usageInPeriod = usageInThingName[periodType] || {};
  const { displayValue } = usageInPeriod;
  const { displayAverageKwh } = usageInPeriod;
  const { displayTnbRate } = usageInPeriod;
  const { displayCpRT } = usageInPeriod;

  return {
    displayValue,
    displayAverageKwh,
    displayTnbRate,
    isLoading,
    thingName,
    displayCpRT,
    tutorialFlag,
  };
};

const mapDispatchToProps = dispatch => ({
  setIsGraphCompareDateDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphCompareDateDisplayed(isDisplayed));
  },
  setIsGraphClearResultDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphClearResultDisplayed(isDisplayed));
  },
  setIsGraphToggleIndicatorDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphToggleIndicatorDisplayed(isDisplayed));
  },
  setIsGraphScrollLineIndicatorDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphScrollLineIndicatorDisplayed(isDisplayed));
  },
  setIsGraphElectricityBillRateDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphElectricityBillRateDisplayed(isDisplayed));
  },
  setIsGraphNavigatePeriodDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphNavigatePeriodDisplayed(isDisplayed));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PeriodDisplay);
