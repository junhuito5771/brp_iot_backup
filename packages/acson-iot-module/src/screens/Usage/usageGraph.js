import React, { useState, useRef, useEffect, useCallback } from 'react';
import {
  View,
  StyleSheet,
  Image,
  ScrollView,
  TouchableOpacity,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {
  connect,
  usage,
  tutorialFlag as tutorialState,
} from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  widthPercentage,
  iphoneXS,
  timeConvert,
  verticalScale,
} from '@module/utility';
import DatePicker from 'react-native-date-picker';
import { Calendar } from 'react-native-calendars';
import PropTypes from 'prop-types';
import moment from 'moment';
import {
  VictoryLine,
  VictoryGroup,
  VictoryChart,
  VictoryAxis,
  VictoryCursorContainer,
  Line,
  VictoryLabel,
} from 'victory-native';
import {
  Colors,
  Text,
  BottomModal,
  WheelPicker,
  Button,
  ModalService,
} from '@module/acson-ui';
import { useFocusEffect } from '@react-navigation/native';

import {
  PERIOD_DAILY,
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
  MONTHS_FULL,
} from '../../constants/timePeriod';
import ToogleContainer from './toogleContainer';
import PeriodDisplay from './periodDisplay';

const styles = StyleSheet.create({
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
  whiteBackground: {
    backgroundColor: Colors.white,
  },
  periodContainer: {
    flexDirection: 'row',
    marginVertical: 10,
    justifyContent: 'space-around',
    height: heightPercentage(5),
    width: widthPercentage(100),
  },
  flexCol: {
    flexDirection: 'column',
  },
  greenBar: {
    width: widthPercentage(5),
    backgroundColor: Colors.limeGreen,
    height: heightPercentage(0.5),
    alignSelf: 'center',
    marginTop: 5,
  },
  titleLabel: {
    position:'absolute',
    top:0,
  },
  marginHor: {
    position:'absolute',
    bottom:10,
    width: widthPercentage(90)
  },
  loadingContainer: {
    justifyContent: 'center',
    alignContent: 'center',
    width: widthPercentage(100),
    height: heightPercentage(30), // must same height with graph chart
  },
  bottomModalContainer: {
    flex:1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height:iphoneXS ? heightPercentage(50) : heightPercentage(63),
  },
  headerRight: {
    marginRight: 25,
    flexDirection: 'row',
  },
  btnHeader: {
    marginRight: 15,
    alignItems: 'center',
  },
});

const graphstyle = {
  indicatorLine: { stroke: Colors.red, strokeWidth: 2 },
  tempAxis: {
    axis: { stroke: Colors.black },
    ticks: { padding: -3 },
    axisLabel: {
      fontSize: 12,
      color: Colors.black,
      fontFamily: 'Roboto',
      padding: 30,
    },
  },
  energyAxis: {
    axis: { stroke: Colors.black },
    axisLabel: {
      fontSize: 12,
      color: Colors.black,
      fontFamily: 'Roboto',
      padding: -46,
    },
    tickLabels: { textAnchor: 'start' },
    ticks: { padding: -20 },
  },
  xAxis: {
    tickLabels: {
      fontSize: 12,
      color: Colors.black,
      fontFamily: 'Roboto',
    },
    axis: { stroke: Colors.midGrey },
    grid: {
      strokeDasharray: '5,5',
      stroke: Colors.borderlightGray,
      strokeWidth: 0.6,
      fillOpacity: 0.2,
    },
  },
};

const STANDARD_DATE_FORMAT = 'DD MMMM YYYY';
const CALENDAR_DATE_FORMAT = 'YYYY-MM-DD';
const TIMESTAMP_FORMAT = 'x';
const DEFAULT_DOMAIN_PADDING = {
  x: 0,
  y: 5,
};
const DEFAULT_GRAPH_HEIGHT = verticalScale(220);
const DEFAULT_INTERPOLATION_LINE = 'stepAfter';
const xKey = 'updatedOn';
const graphPadding = { top: 15, bottom: 35, left: 50, right: 60 };
const graphDomain = { y: [0, 1] };

function getGroups(data, compareData, compareDate, periodType) {
  if (typeof data !== 'undefined') {
    let groups = [];

    const comparedDays = Number(
      moment(compareDate).endOf('months').format('DD')
    );

    const curDateDays = Number(moment().endOf('months').format('DD'));

    groups = [{ id: 'group-1', graphData: data }];

    if (compareData && compareData.length > 0) {
      if (periodType === PERIOD_MONTHLY && comparedDays !== curDateDays) {
        let newDataArr = Object.values(
          curDateDays > comparedDays ? compareData : data
        );
        newDataArr.pop();
        const toFillData = new Array(
          (curDateDays > comparedDays ? curDateDays + 1 : comparedDays + 1) -
            newDataArr.length,
        ).fill(null);
        newDataArr = [
          ...newDataArr,
          ...toFillData.map((a, i) => ({
            updatedOn:
              i === toFillData.length - 1
                ? ' '
                : `${newDataArr.length + 1 + i}`,
            setTemp: 0,
            indoorTemp: 0,
            SetOnOff: 0,
            kWh: 0,
            displaykWh: 0,
            electricRate: 0,
            cpRT: 0,
            isPopulate: -1,
          })),
        ];

        if (curDateDays > comparedDays) {
          groups.push({ id: 'group-2', graphData: newDataArr });
        } else {
          groups[0].graphData = newDataArr;
          groups.push({ id: 'group-2', graphData: compareData });
        }
      } else {
        groups.push({ id: 'group-2', graphData: compareData });
      }
    }

    return groups;
  }
  return [];
}

function Capitalize(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

function selectDate(periodType) {
  switch (periodType) {
    case PERIOD_WEEKLY:
      return 'week';
    case PERIOD_MONTHLY:
      return 'month';
    case PERIOD_YEARLY:
      return 'year';
    default:
      return 'date';
  }
}

function getMarkedDates(targetDate, dateFormat) {
  if (!targetDate) return {};
  const momentDate = moment(targetDate, dateFormat);
  return Array(7)
    .fill(0)
    .reduce((acc, cur, index) => {
      const currentDay = momentDate.startOf('isoWeek').add(index, 'days');
      const isFirst = index === 0;
      const isLast = index === 6;
      // eslint-disable-next-line no-param-reassign
      acc[currentDay.format(CALENDAR_DATE_FORMAT)] = {
        color: Colors.switchFill,
        ...(isFirst ? { startingDay: true, selected: true } : {}),
        ...(isLast ? { endingDay: true, selected: true } : {}),
      };

      return acc;
    }, {});
}

function getYearList(minimumDate, maximumDate, formatDate) {
  if (!minimumDate || !maximumDate || !formatDate) return [];
  const minYear = moment(minimumDate, formatDate).year();
  const maxYear = moment(maximumDate, formatDate).year();

  const years = [];

  for (let i = minYear; i <= maxYear; i += 1) {
    years.push({
      value: i.toString(),
      label: i.toString(),
    });
  }
  return years;
}

function getPickerByPeriod(
  periodType,
  minimumDate,
  maximumDate,
  setCurDate,
  setCurWeek,
  setCurMonth,
  setCurYear
) {
  const pickerProps = {
    minimumDate,
    maximumDate,
    selectedDate: setCurDate,
    selectedWeek: setCurWeek,
    selectedMonth: setCurMonth,
    selectedYear: setCurYear,
    formatDate: STANDARD_DATE_FORMAT,
  };

  switch (periodType) {
    case PERIOD_WEEKLY:
      return <CalendarPicker {...pickerProps} />;
    case PERIOD_MONTHLY:
      return <MonthYearPicker {...pickerProps} />;
    case PERIOD_YEARLY:
      return <MonthYearPicker {...pickerProps} showYearOnly />;
    default:
      return <DayPickerSelector locale="en-GB" {...pickerProps} />;
  }
}

const datePickerStyles = StyleSheet.create({
  container: {
    width: widthPercentage(80),
    height: Platform.select({
      ios: heightPercentage(40),
      android: heightPercentage(40),
    }),
  },
});

const DayPickerSelector = ({
  minimumDate,
  maximumDate,
  selectedDate,
  formatDate,
}) => {
  const [currentDate, setCurrentDate] = useState(moment().toDate());

  const curDated = moment(currentDate, formatDate).subtract(1, 'days').toDate();

  const curDatedMoment = moment(currentDate, formatDate)
    .subtract(1, 'days')
    .format('DD MMMM YYYY');

  const setDate = e => {
    setCurrentDate(moment(e, formatDate).add(1, 'days').toDate());
    selectedDate(e);
  };

  useEffect(() => {
    selectedDate(moment(curDatedMoment, formatDate).toDate());
  }, [currentDate]);

  return (
    <DatePicker
      style={datePickerStyles.container}
      mode="date"
      date={curDated}
      onDateChange={e => setDate(e)}
      minimumDate={moment(minimumDate,formatDate).toDate()}
      maximumDate={moment(maximumDate, formatDate).subtract(1, 'days').toDate()}
    />
  );
};

DayPickerSelector.propTypes = {
  selectedDate: PropTypes.func,
  minimumDate: PropTypes.string,
  maximumDate: PropTypes.string,
  formatDate: PropTypes.string,
};
DayPickerSelector.defaultProps = {
  formatDate: STANDARD_DATE_FORMAT,
};

const calendarStyle = StyleSheet.create({
  container: {
    width: widthPercentage(80),
    height: Platform.select({
      ios: iphoneXS ? heightPercentage(42) : heightPercentage(60),
      android: heightPercentage(53),
    }),
    marginTop:15
  },
  calendar: {
    position: 'relative',
  },
});

const CalendarPicker = ({ minimumDate, selectedWeek, formatDate }) => {
  const [currentDate, setCurrentDate] = useState(
    moment().format(CALENDAR_DATE_FORMAT)
  );

  const subtractWeek = moment(currentDate)
    .subtract(7, 'days')
    .format(CALENDAR_DATE_FORMAT);

  const maxDate = moment().subtract(7, 'days').format(formatDate);

  const markedDates = getMarkedDates(subtractWeek, CALENDAR_DATE_FORMAT);

  const handleOnDayPressed = date => {
    const dateResult = moment(date.timestamp.toString(), TIMESTAMP_FORMAT);

    setCurrentDate(dateResult.add(7, 'days').format(CALENDAR_DATE_FORMAT));
    selectedWeek(dateResult.subtract(7, 'days').format(CALENDAR_DATE_FORMAT));
  };

  useEffect(() => {
    selectedWeek(subtractWeek);
  }, [subtractWeek]);

  return (
    <View style={calendarStyle.container}>
      <Calendar
        style={calendarStyle.calendar}
        firstDay={1} // Start from monday
        current={subtractWeek}
        minDate={moment(minimumDate, formatDate).format(CALENDAR_DATE_FORMAT)}
        maxDate={moment(maxDate, formatDate).format(CALENDAR_DATE_FORMAT)}
        onDayPress={handleOnDayPressed}
        markedDates={markedDates}
        markingType="period"
        theme={{
          'stylesheet.calendar.header': {
            week: {
              marginTop: 2,
              flexDirection: 'row',
              justifyContent: 'space-between',
            },
            monthText: {
              margin: 3,
            },
            header: {
              flexDirection: 'row',
              justifyContent: 'space-between',
              paddingLeft: 10,
              paddingRight: 10,
              marginTop: 3,
              alignItems: 'center',
            },
          },
        }}
      />
    </View>
  );
};

CalendarPicker.propTypes = {
  selectedWeek: PropTypes.func,
  minimumDate: PropTypes.string,
  formatDate: PropTypes.string,
};

const monthYearPicker = StyleSheet.create({
  container: {
    height: Platform.select({
      ios: heightPercentage(40),
      android: heightPercentage(40),
    }),
    width: widthPercentage(80),
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrap: {
    flexDirection: 'row',
  },
});

const MonthYearPicker = ({
  minimumDate,
  maximumDate,
  selectedMonth,
  selectedYear,
  formatDate,
  showYearOnly,
}) => {
  const [currentMonth, setCurrentMonth] = useState(moment().format('MMMM'));
  const [currentYear, setCurrentYear] = useState(moment().format('YYYY'));
  const [sameYear, setSameYear] = useState(
    moment().format('YYYY') === currentYear,
  );
  const yearList = getYearList(minimumDate, maximumDate, formatDate);
  const monthNow = moment(currentMonth, formatDate)
    .subtract(1, 'months')
    .format('MMMM');

  const yearNow = moment(currentMonth, formatDate)
    .subtract(1, 'years')
    .format('YYYY');

  const monthID = MONTHS_FULL.findIndex(
    d => d.value === moment().format('MMMM')
  );
  const YearID = yearList.findIndex(k => k.value === moment().format('YYYY'));

  const MONTHS = MONTHS_FULL.slice(0, monthID);
  const YEARS = yearList.slice(0, YearID);

  useEffect(() => {
    let monthYear = `${monthNow} ${sameYear ? currentYear : yearNow}`;
    const isAfterMonthYear = moment(monthYear, 'MMMM YYYY').isSameOrAfter(
      moment().format('MMMM YYYY'),
    );
    if (isAfterMonthYear) monthYear = monthYear.replace(monthNow, 'January');
    selectedMonth(monthYear);

    if (showYearOnly) {
      selectedYear(yearNow);
    }
  }, [currentMonth, currentYear]);

  const handleOnMonthChanged = itemValue => {
    const selectedMonthId = moment(itemValue, 'MMMM')
      .add(1, 'months')
      .format('MMMM');
    setCurrentMonth(selectedMonthId);
  };

  const handleOnYearChanged = itemValue => {
    if (moment().format('YYYY') === itemValue) setSameYear(true);
    else setSameYear(false);
    setCurrentYear(itemValue);
  };

  const wheelWidth = showYearOnly ? '100%' : '50%';

  const getWheelStyle = () => {
    if (Platform.OS === 'android') return { width: widthPercentage(40), height: heightPercentage(20) };
    return { width: widthPercentage(40),   };
  };

  return (
    <View style={monthYearPicker.container}>
      <View style={monthYearPicker.wrap}>
        {!showYearOnly && (
          <View style={monthYearPicker.wrap}>
            <WheelPicker
              style={getWheelStyle()}
              selectedValue={monthNow}
              onValueChange={handleOnMonthChanged}
              data={sameYear ? MONTHS : MONTHS_FULL}
            />
            <WheelPicker
              style={getWheelStyle()}
              selectedValue={currentYear}
              onValueChange={handleOnYearChanged}
              data={yearList}
            />
          </View>
        )}
        {showYearOnly && YEARS.length === 0 && (
          <Text>You have no usage in the past year</Text>
        )}
        {showYearOnly && YEARS.length > 0 && (
          <WheelPicker
            style={getWheelStyle()}
            selectedValue={yearNow}
            onValueChange={handleOnYearChanged}
            data={!showYearOnly ? yearList : YEARS}
          />
        )}
      </View>
    </View>
  );
};

MonthYearPicker.propTypes = {
  selectedMonth: PropTypes.func,
  selectedYear: PropTypes.func,
  minimumDate: PropTypes.string,
  maximumDate: PropTypes.string,
  formatDate: PropTypes.string,
  showYearOnly: PropTypes.bool,
};
MonthYearPicker.defaultProp = {
  showYearOnly: false,
};

const UsageGraphScreen = ({
  navigation,
  data,
  fetchUsageData,
  periodType,
  compareData,
  compareDate,
  setSelectedPeriod,
  isLoading,
  error,
  clearUsageStatus,
  fetchCompareUsageData,
  clearCompareData,
  setToggleButton,
  setSelectedDate,
  setSelectedWeek,
  setSelectedMonth,
  setSelectedYear,
  selectedFromDate,
  selectedFromWeek,
  selectedFromMonth,
  selectedFromYear,
  setTemperature,
  indoorTemperature,
  energyConsumptions,
  cpRT,
  tnbRate,
  electricRate,
  route,

  // tutorial props
  setIsGraphCompareDateDisplayed,
  setIsGraphClearResultDisplayed,
  setIsGraphToggleIndicatorDisplayed,
  setIsGraphScrollLineIndicatorDisplayed,
  setIsGraphElectricityBillRateDisplayed,
  setIsGraphNavigatePeriodDisplayed,
}) => {
  const {
    thingName,
    acName,
    isInverter,
    enableEnergyConsumption,
  } = route.params;
  const bottomModalRef = useRef();
  const srollViewRef = useRef();
  const [indexValue, setIndexValue] = useState(0);
  const [count, setCount] = useState(1);
  const groups = getGroups(data, compareData, compareDate, periodType);
  const graphContainerWidth = widthPercentage(100);
  const indexData = theData => {
    setIndexValue(Math.floor(theData));
  };

  useEffect(() => {
    fetchUsageData({ thingName, periodType });
  }, [thingName, periodType, tnbRate]);

  useEffect(() => {
    if (error) {
      ModalService.show({
        title: 'Error',
        desc: error,
        buttons: [{ title: 'Ok', onPress: clearUsageStatus(), isRaised: true }],
      });
    }
  }, [error]);

  useFocusEffect(
    useCallback(() => {
      const onShowTutorialOverlay = () => {
        setIsGraphClearResultDisplayed(false);
        setIsGraphCompareDateDisplayed(false);
        setIsGraphElectricityBillRateDisplayed(false);
        setIsGraphNavigatePeriodDisplayed(false);
        setIsGraphScrollLineIndicatorDisplayed(false);
        setIsGraphToggleIndicatorDisplayed(false);
      };
      navigation.setOptions({
        headerRight: () => (
          <View style={styles.headerRight}>
            {onShowTutorialOverlay && (
              <View style={styles.btnHeader}>
                <TouchableOpacity
                  hitSlop={styles.hitSlop}
                  onPress={onShowTutorialOverlay}>
                  <Image
                    source={require('../../shared/images/general/help.png')}
                    style={{ tintColor: Colors.linkGrey }}
                  />
                </TouchableOpacity>
              </View>
            )}
            <ConnectedRefreshButton />
          </View>
        ),
      });
    }, [])
  );

  const allColor = {
    setTemperature: [Colors.lightskyblue, Colors.solitude],
    indoorTemperature: [Colors.lightsalmon, Colors.peach],
    energyConsumptions: [Colors.caramel, Colors.silk],
    cpRT: [Colors.salmon, Colors.PalePink],
    electricRate: [Colors.yellowGreen, Colors.beige],
  };
  const [color, setColor] = useState(allColor);
  const [yKey, setYKey] = useState([]);

  useEffect(() => {
    const activeColor = {
      setTemp: [
        setTemperature ? allColor.setTemperature[0] : 'transparent',
        setTemperature ? allColor.setTemperature[1] : 'transparent',
      ],
      indoorTemp: [
        indoorTemperature ? allColor.indoorTemperature[0] : 'transparent',
        indoorTemperature ? allColor.indoorTemperature[1] : 'transparent',
      ],
      displaykWh: [
        energyConsumptions && enableEnergyConsumption && isInverter
          ? allColor.energyConsumptions[0]
          : 'transparent',
        energyConsumptions && enableEnergyConsumption && isInverter
          ? allColor.energyConsumptions[1]
          : 'transparent',
      ],
      cpRT: [
        cpRT && !isInverter ? allColor.cpRT[0] : 'transparent',
        cpRT && !isInverter ? allColor.cpRT[1] : 'transparent',
      ],
      electricRate: [
        electricRate && isInverter ? allColor.electricRate[0] : 'transparent',
        electricRate && isInverter ? allColor.electricRate[1] : 'transparent',
      ],
    };

    const useYKey = [];
    Object.entries(activeColor).forEach(cl => {
      if (cl[1][0] !== 'transparent') {
        useYKey.push(cl[0]);
      }
    });
    setColor(activeColor);
    setYKey(useYKey.length ? useYKey : ['indoorTemp']);
  }, [
    setTemperature,
    indoorTemperature,
    energyConsumptions,
    enableEnergyConsumption,
    isInverter,
    cpRT,
    electricRate,
  ]);

  const now = moment().format(STANDARD_DATE_FORMAT);

  const [curDate, setCurDate] = useState(
    moment(now).format(CALENDAR_DATE_FORMAT)
  );

  const [curWeek, setCurWeek] = useState(null);
  const [curMonth, setCurMonth] = useState(null);
  const [curYear, setCurYear] = useState(null);

  const minimumDate = moment('01 January 2020', STANDARD_DATE_FORMAT).format(
    STANDARD_DATE_FORMAT
  );

  const maximumDate = moment().startOf('day').format(STANDARD_DATE_FORMAT);

  const handleSelectDate = () => {
    switch (periodType) {
      case PERIOD_DAILY:
        return setSelectedDate(curDate);
      case PERIOD_WEEKLY:
        return setSelectedWeek(curWeek);
      case PERIOD_MONTHLY:
        return setSelectedMonth(curMonth);
      case PERIOD_YEARLY:
        return setSelectedYear(curYear);
      default:
        return null;
    }
  };

  const handleOnPressCompare = () => {
    let value = null;
    if (selectedFromDate && periodType === PERIOD_DAILY) {
      value = moment(selectedFromDate).format(CALENDAR_DATE_FORMAT);
    }
    if (selectedFromWeek && periodType === PERIOD_WEEKLY) {
      value = moment(selectedFromWeek).format(CALENDAR_DATE_FORMAT);
    }
    if (selectedFromMonth && periodType === PERIOD_MONTHLY) {
      value = moment(selectedFromMonth, 'MMM YYYY').format(
        CALENDAR_DATE_FORMAT
      );
    }
    if (selectedFromYear && periodType === PERIOD_YEARLY) {
      value = moment(selectedFromYear).format(CALENDAR_DATE_FORMAT);
    }
    const targetDate = moment(value).format(CALENDAR_DATE_FORMAT);

    fetchCompareUsageData({
      thingName,
      periodType,
      targetDate,
    });
  };

  const alertModalValidation = type => {
    const description =
      type === 'insert'
        ? 'Please insert dates for comparison'
        : 'Please select other dates for comparison';
    ModalService.show({
      title: 'Error',
      desc: description,
      buttons: [{ title: 'Ok', onPress: clearUsageStatus(), isRaised: true }],
    });
  };

  const checkDateCompare = () => {
    const currentDate = moment().format('YYYY-MM-DD');
    const targetWeek = moment(selectedFromWeek)
      .startOf('isoWeek')
      .format('YYYY-MM-DD');

    const currentWeek = moment().startOf('isoWeek').format('YYYY-MM-DD');
    const currentMonth = moment().format('YYYY-MM');
    const currentYear = moment().format('YYYY');

    const targetDate =
      selectedFromDate === null
        ? currentDate
        : moment(selectedFromDate).format('YYYY-MM-DD');
    const targetMonth =
      selectedFromMonth === null
        ? currentMonth
        : moment(selectedFromMonth, 'MMM YYYY');
    const targetYear =
      selectedFromYear === null
        ? currentYear
        : moment(selectedFromYear).format('YYYY');

    if (periodType === PERIOD_DAILY) {
      if (selectedFromDate === null) {
        alertModalValidation('insert');
      } else if (targetDate >= currentDate) {
        alertModalValidation();
      } else {
        handleOnPressCompare();
        setToggleButton({ thingName, periodType, type: 'setCompare' });
      }
    }
    if (periodType === PERIOD_WEEKLY) {
      if (selectedFromWeek === null) {
        alertModalValidation('insert');
      } else if (targetWeek >= currentWeek) {
        alertModalValidation();
      } else {
        handleOnPressCompare();
        setToggleButton({ thingName, periodType, type: 'setCompare' });
      }
    }
    if (periodType === PERIOD_MONTHLY) {
      if (selectedFromMonth === null) {
        alertModalValidation('insert');
      } else if (targetMonth >= currentMonth) {
        alertModalValidation();
      } else {
        handleOnPressCompare();
        setToggleButton({ thingName, periodType, type: 'setCompare' });
      }
    }
    if (periodType === PERIOD_YEARLY) {
      if (selectedFromYear === null) {
        alertModalValidation('insert');
      } else if (targetYear >= currentYear) {
        alertModalValidation();
      } else {
        handleOnPressCompare();
        setToggleButton({ thingName, periodType, type: 'setCompare' });
      }
    }
  };

  useEffect(() => {
    // make indicator line reset to 1
    setCount(1);
    indexData(1);
    srollViewRef.current.scrollTo({ y: 0, animated: false });

    return () => {};
  }, [periodType]);

  const replaceLastLineData = (dataPlot, value) => {
    dataPlot.pop();
    dataPlot.push(value);
    return dataPlot;
  };

  const cprtTimeConverted = val => {
    const { rhours } = timeConvert(val);
    if (periodType === PERIOD_DAILY) return val;
    return rhours;
  };

  const getLineData = groupsData => {
    const dataSplit = [];
    let dataSplitBreak = 0;
    let dataPlot = [];
    let dataType = null;
    let dataPrev = groupsData[0];

    groupsData.forEach((ar, i) => {
      // when isPopulate has changed
      if (ar.isPopulate !== dataPrev.isPopulate) {
        if (dataPrev.isPopulate === 1) dataType = 'dashed';
        else if (dataPrev.isPopulate === 0) dataType = 'solid';
        else dataType = 'transparent';
        dataPlot = groupsData.slice(dataSplitBreak, i + 1);

        if (dataSplitBreak === 0) {
          if (dataPlot.length === 1) {
            dataPlot.unshift({
              updatedOn: ' ',
              setTemp: 0,
              indoorTemp: 0,
              SetOnOff: 0,
              cpRT: 0.0,
              kWh: 0,
              displaykWh: 0,
              electricRate: 0,
              isPopulate: dataPrev.isPopulate,
            });
          }
        } else {
          dataPlot.unshift(groupsData[dataSplitBreak - 1]);
        }

        if (ar.updatedOn.trim().length === 0) {
          dataPlot = replaceLastLineData(dataPlot, {
            updatedOn: ar.updatedOn,
            setTemp: dataPrev.setTemp,
            indoorTemp: dataPrev.indoorTemp,
            SetOnOff: dataPrev.SetOnOff,
            kWh: dataPrev.kWh,
            displaykWh: dataPrev.displaykWh,
            electricRate: dataPrev.electricRate,
            cpRT: cprtTimeConverted(dataPrev.cpRT),
            isPopulate: dataPrev.isPopulate,
          });
        }

        if (dataType === 'solid') {
          dataPlot = replaceLastLineData(dataPlot, {
            updatedOn: ar.updatedOn,
            setTemp: ar.setTemp === 0 ? dataPrev.setTemp : ar.setTemp,
            indoorTemp:
              ar.indoorTemp === 0 ? dataPrev.indoorTemp : ar.indoorTemp,
            SetOnOff: dataPrev.SetOnOff,
            cpRT: dataPrev.cpRT,
            kWh: ar.kWh === 0 ? dataPrev.kWh : ar.kWh,
            electricRate:
              ar.electricRate === 0 ? dataPrev.electricRate : ar.electricRate,
            displaykWh: ar.displaykWh,
            isPopulate:
              ar.isPopulate === 0 ? dataPrev.isPopulate : ar.isPopulate,
          });
        }

        dataSplit.push({
          type: dataType,
          data: dataPlot,
        });
        dataSplitBreak = i + 1;
      }

      dataPrev = ar;
    });

    if (dataSplitBreak !== 0) {
      dataPlot = replaceLastLineData(dataPlot, {
        updatedOn: groupsData[dataSplitBreak - 1].updatedOn,
        setTemp: groupsData[dataSplitBreak - 2].setTemp,
        indoorTemp: groupsData[dataSplitBreak - 2].indoorTemp,
        SetOnOff: groupsData[dataSplitBreak - 2].SetOnOff,
        kWh: groupsData[dataSplitBreak - 2].kWh,
        electricRate: groupsData[dataSplitBreak - 2].electricRate,
        cpRT: groupsData[dataSplitBreak - 2].cpRT,
        displaykWh: groupsData[dataSplitBreak - 2].displaykWh,
        isPopulate: groupsData[dataSplitBreak - 2].isPopulate,
      });
    }

    dataPlot = groupsData.slice(dataSplitBreak, groupsData.length);

    if (groupsData[dataSplitBreak - 1] !== undefined) {
      dataPlot.unshift(groupsData[dataSplitBreak - 1]);
    }

    if (dataPlot.length === 1) {
      dataPlot.push(dataPlot[0]);
    }

    dataSplit.push({
      type: 'transparent',
      data: dataPlot,
    });

    return dataSplit;
  };

  const roundToNext5 = val => Math.ceil(val / 5) * 5; // round to next multiple of 5

  const getMaxima = curYkey => {
    if (groups.length === 0) return 0;
    let maxima1 = 0;
    const maxima0 =
      Math.max(
        ...groups[0].graphData.map(a =>
          curYkey === 'cpRT' ? cprtTimeConverted(a[curYkey]) : a[curYkey]
        )
      ) || 0;

    if (groups.length > 1) {
      maxima1 =
        Math.max(
          ...groups[1].graphData.map(a =>
            curYkey === 'cpRT' ? cprtTimeConverted(a[curYkey]) : a[curYkey]
          )
        ) || 0;
    }
    const value = maxima1 > maxima0 ? maxima1 : maxima0;

    if (value < 1) return 1;
    if (value % 2 === 0) return value + 2;
    if (value % 5 > 0) return value;

    return roundToNext5(value);
  };

  const getMaximaShare = (curYkey, type) => {
    if (groups.length === 0) return 35;
    let maxima1 = 0;
    const maxima0 = Math.max(...groups[0].graphData.map(a => a[curYkey]));

    if (groups.length > 1) {
      maxima1 = Math.max(...groups[1].graphData.map(a => a[curYkey]));
    }

    let maximaSibling = 0;
    if (type === 'temp') {
      maximaSibling = getMaxima(
        curYkey === 'setTemp' ? 'indoorTemp' : 'setTemp'
      );
    } else {
      maximaSibling = getMaxima(curYkey === 'kWh' ? 'electricRate' : 'kWh');
    }

    const maximaSelf = maxima1 > maxima0 ? maxima1 : maxima0;
    const value = maximaSibling > maximaSelf ? maximaSibling : maximaSelf;

    if (value === 0 || value === 1) {
      return 35;
    }
    if (value === 1 && curYkey === 'kWh') return value;
    if (value % 5 !== 0) {
      return roundToNext5(value);
    }
    // add 5 if (max = value)
    return value + 5;
  };

  const normedScale = [
    { max: 1, factora: 0.2 },
    { max: 2, factora: 0.2 },
    { max: 5, factora: 1 },
    { max: 10, factora: 1 },
    { max: 12, factora: 2 },
    { max: 25, factora: 5 },
    { max: 50, factora: 10 },
    { max: 100, factora: 10 },
    { max: 1200, factora: 200 },
    { max: 2000, factora: 250 },
  ];
  let maximakWh = getMaximaShare('kWh');

  let factora = 0;
  for (let i = 0; i < normedScale.length; i += 1) {
    const nextScale = normedScale[i + 1] || { max: 200, factora: 50 };
    if (maximakWh <= nextScale.max && maximakWh >= normedScale[i].max) {
      maximakWh = nextScale.max;
      factora = nextScale.factora;
      break;
    }
  }
  if (factora === 0) {
    let dividenta = 100;
    maximakWh =
      maximakWh % dividenta === 0
        ? maximakWh
        : Math.ceil(maximakWh / dividenta) * dividenta;
    let quotienta = (Math.ceil(maximakWh / dividenta) * dividenta) / dividenta;
    while (quotienta > 10) {
      dividenta *= 10;
      quotienta = (Math.ceil(maximakWh / dividenta) * dividenta) / dividenta;
      maximakWh =
        maximakWh % dividenta === 0
          ? maximakWh
          : Math.ceil(maximakWh / dividenta) * dividenta;
    }
    factora = maximakWh / 100 < 4 ? 50 : 100;
  }

  const normedkWh = [];
  for (let i = factora; i <= maximakWh; i += factora) {
    normedkWh.push(((i / factora) * 1) / (maximakWh / factora));
  }

  const maximaTemp = getMaximaShare('indoorTemp', 'temp');
  factora = 5;
  while (maximaTemp % factora > 0) {
    factora += 1;
  }
  const normedTemp = [];
  for (let i = 5; i <= maximaTemp; i += 5) {
    normedTemp.push(((i / factora) * 1) / (maximaTemp / factora));
  }

  let maximacpRT = getMaxima('cpRT') || 5;
  factora = 5;
  for (let i = 0; i < normedScale.length; i += 1) {
    const nextScale = normedScale[i + 1] || { max: 200, factora: 50 };
    if (maximacpRT <= nextScale.max && maximacpRT >= normedScale[i].max) {
      maximacpRT = nextScale.max;
      factora = nextScale.factora;
      break;
    }
  }

  const normedcpRT = [];
  for (let i = factora; i <= maximacpRT; i += factora) {
    normedcpRT.push(((i / factora) * 1) / (maximacpRT / factora));
  }

  const showDecimal = (t, max) => (max <= 2 ? (t * max).toFixed(1) : t * max);

  const renderLine = (id, lineData) =>
    yKey.map(curYKey =>
      lineData.map(ar => {
        let renderStyle = null;
        switch (ar.type) {
          case 'solid':
            renderStyle = {
              stroke: () => color[curYKey][id],
            };
            break;
          case 'dashed':
            renderStyle = {
              strokeDasharray: '3,3',
              stroke: () => color[curYKey][id],
            };
            break;
          default:
            renderStyle = {
              stroke: 'transparent',
            };
            break;
        }

        if (curYKey === 'cpRT') {
          return (
            <VictoryLine
              key={curYKey}
              interpolation={DEFAULT_INTERPOLATION_LINE}
              data={ar.data}
              x={xKey}
              y={datum => cprtTimeConverted(datum.cpRT) / maximacpRT}
              y0={curYKey}
              style={{
                data: renderStyle,
              }}
            />
          );
        }
        if (curYKey === 'electricRate') {
          return (
            <VictoryLine
              key={curYKey}
              interpolation={DEFAULT_INTERPOLATION_LINE}
              data={ar.data}
              x={xKey}
              y={datum => datum[curYKey] / maximakWh}
              y0={curYKey}
              style={{
                data: renderStyle,
              }}
            />
          );
        }
        if (
          curYKey !== 'displaykWh' &&
          curYKey !== 'cpRT' &&
          curYKey !== 'electricRate'
        ) {
          return (
            <VictoryLine
              key={curYKey}
              interpolation={DEFAULT_INTERPOLATION_LINE}
              data={ar.data}
              x={xKey}
              y={datum => datum[curYKey] / maximaTemp}
              y0={curYKey}
              style={{
                data: renderStyle,
              }}
            />
          );
        }

        return (
          <VictoryLine
            key={curYKey}
            interpolation={DEFAULT_INTERPOLATION_LINE}
            data={ar.data}
            x={xKey}
            y={datum => datum.kWh / maximakWh}
            y0={curYKey}
            style={{
              data: renderStyle,
            }}
          />
        );
      })
    );
  return (
    <>
      <ScrollView
        ref={srollViewRef}
        style={styles.whiteBackground}
        bounces={false}>
        <PeriodDisplay
          acName={acName}
          periodType={periodType}
          selectDate={selectDate}
          selectedFromDate={selectedFromDate}
          selectedFromWeek={selectedFromWeek}
          selectedFromMonth={selectedFromMonth}
          selectedFromYear={selectedFromYear}
          bottomModalRef={bottomModalRef}
          compareData={compareData}
          checkDateCompare={checkDateCompare}
          clearCompareData={clearCompareData}
          isInverter={isInverter}
          enableEnergyConsumption={enableEnergyConsumption}
        />
        <ScrollView
          horizontal
          scrollEnabled={false}
          style={{
            maxHeight: DEFAULT_GRAPH_HEIGHT,
            backgroundColor: 'transparent',
            width: widthPercentage(100),
          }}>
          {isLoading ? (
            <View style={styles.loadingContainer}>
              <ActivityIndicator />
            </View>
          ) : (
            <VictoryChart
              height={DEFAULT_GRAPH_HEIGHT}
              domainPadding={DEFAULT_DOMAIN_PADDING}
              padding={graphPadding}
              width={graphContainerWidth}
              domain={graphDomain}
              containerComponent={
                <VictoryCursorContainer
                  cursorDimension="x"
                  defaultCursorValue={count}
                  onCursorChange={value => {
                    if (value) {
                      setCount(value);
                      indexData(value);
                    }
                  }}
                  cursorComponent={<Line style={graphstyle.indicatorLine} />}
                />
              }>
              <VictoryAxis
                dependentAxis
                orientation="left"
                label="Temperature (°C)"
                tickValues={normedTemp}
                tickFormat={t => showDecimal(t, maximaTemp)}
                style={graphstyle.tempAxis}
              />

              <VictoryAxis
                tickLabelComponent={
                  <VictoryLabel
                    dx={() => (periodType === 'monthly' ? 5 : 24)}
                  />
                }
                fixLabelOverlap
                style={graphstyle.xAxis}
              />

              <VictoryGroup>
                {groups.map(({ graphData }, id) => {
                  const lineData = getLineData(graphData);
                  return renderLine(id, lineData);
                })}
              </VictoryGroup>

              {isInverter && enableEnergyConsumption && (
                <VictoryAxis
                  dependentAxis
                  label="Energy Consumption (kWh)"
                  offsetX={graphContainerWidth - 60}
                  tickValues={normedkWh}
                  tickFormat={t => showDecimal(t, maximakWh)}
                  style={graphstyle.energyAxis}
                />
              )}

              {!isInverter && (
                <VictoryAxis
                  dependentAxis
                  offsetX={graphContainerWidth - 60}
                  label={
                    periodType !== PERIOD_DAILY
                      ? 'Compressor Running Time (hours)'
                      : 'Compressor Running Time (mins)'
                  }
                  tickValues={normedcpRT}
                  tickFormat={t => showDecimal(t, maximacpRT)}
                  style={graphstyle.energyAxis}
                />
              )}
            </VictoryChart>
          )}
        </ScrollView>

        <ToogleContainer
          indexValue={indexValue}
          isInverter={isInverter}
          thingName={thingName}
          enableEnergyConsumption={enableEnergyConsumption}
          setTemperature={setTemperature}
          indoorTemperature={indoorTemperature}
          energyConsumptions={energyConsumptions}
          cpRT={cpRT}
          isLoading={isLoading}
          setToggleButton={setToggleButton}
          electricRate={electricRate}
          groups={groups}
        />

        <View style={styles.periodContainer}>
          <TouchableOpacity
            disabled={isLoading}
            onPress={() => setSelectedPeriod(PERIOD_DAILY)}
            style={styles.flexCol}>
            <Text small>Day</Text>
            {periodType === PERIOD_DAILY && <View style={styles.greenBar} />}
          </TouchableOpacity>
          <TouchableOpacity
            disabled={isLoading}
            onPress={() => setSelectedPeriod(PERIOD_WEEKLY)}
            style={styles.flexCol}>
            <Text small>Week</Text>
            {periodType === PERIOD_WEEKLY && <View style={styles.greenBar} />}
          </TouchableOpacity>
          <TouchableOpacity
            disabled={isLoading}
            onPress={() => setSelectedPeriod(PERIOD_MONTHLY)}
            style={styles.flexCol}>
            <Text small>Month</Text>
            {periodType === PERIOD_MONTHLY && <View style={styles.greenBar} />}
          </TouchableOpacity>
          <TouchableOpacity
            disabled={isLoading}
            onPress={() => setSelectedPeriod(PERIOD_YEARLY)}
            style={styles.flexCol}>
            <View>
              <Text small>Year</Text>
              {periodType === PERIOD_YEARLY && <View style={styles.greenBar} />}
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <BottomModal
        ref={bottomModalRef}
        renderContent={() => (
          <>
            <View style={styles.bottomModalContainer}>
              <View style={styles.titleLabel}>
                <Text medium bold>
                  Select {Capitalize(selectDate(periodType))}
                </Text>
              </View>
                {getPickerByPeriod(
                  periodType,
                  minimumDate,
                  maximumDate,
                  setCurDate,
                  setCurWeek,
                  setCurMonth,
                  setCurYear
                )}
              <View style={styles.marginHor}>
                <Button
                  raised
                  onPress={() => {
                    handleSelectDate();
                    bottomModalRef.current.close();
                  }}>
                  Select
                </Button>
              </View>
            </View>
          </>
        )}
      />
    </>
  );
};
UsageGraphScreen.propTypes = {
  thingName: PropTypes.string,
  periodType: PropTypes.string,
  error: PropTypes.string,
  setSelectedPeriod: PropTypes.func,
  isLoading: PropTypes.bool,
  navigation: PropTypes.object,
  compareData: PropTypes.array,
  compareDate: PropTypes.any,
  data: PropTypes.array,
  fetchUsageData: PropTypes.func,
  clearUsageStatus: PropTypes.func,
  fetchCompareUsageData: PropTypes.func,
  clearCompareData: PropTypes.func,
  enableEnergyConsumption: PropTypes.bool,
  setSelectedDate: PropTypes.func,
  setSelectedWeek: PropTypes.func,
  setSelectedMonth: PropTypes.func,
  setSelectedYear: PropTypes.func,
  setToggleButton: PropTypes.func,
  selectedFromDate: PropTypes.any,
  selectedFromWeek: PropTypes.any,
  selectedFromMonth: PropTypes.any,
  selectedFromYear: PropTypes.any,
  setTemperature: PropTypes.bool,
  indoorTemperature: PropTypes.bool,
  energyConsumptions: PropTypes.bool,
  cpRT: PropTypes.bool,

  setIsGraphCompareDateDisplayed: PropTypes.func,
  setIsGraphClearResultDisplayed: PropTypes.func,
  setIsGraphToggleIndicatorDisplayed: PropTypes.func,
  setIsGraphScrollLineIndicatorDisplayed: PropTypes.func,
  setIsGraphElectricityBillRateDisplayed: PropTypes.func,
  setIsGraphNavigatePeriodDisplayed: PropTypes.func,
  electricRate: PropTypes.bool,
  tnbRate: PropTypes.number,
  route: PropTypes.object,
};

const RefreshButton = ({ fetchUsageData, thingName, periodType }) => (
  <View style={[styles.flexRow]}>
    <TouchableOpacity
      hitSlop={styles.hitSlop}
      onPress={() => {
        fetchUsageData({ thingName, periodType });
      }}>
      <Image
        source={require('../../shared/images/general/refresh.png')}
        style={{ tintColor: Colors.titleGrey }}
      />
    </TouchableOpacity>
  </View>
);

RefreshButton.propTypes = {
  fetchUsageData: PropTypes.func,
  thingName: PropTypes.string,
  periodType: PropTypes.string,
};

const ConnectedRefreshButton = connect(
  ({
    usage: {
      usageSetting: { thingName, periodType },
    },
  }) => ({
    thingName,
    periodType,
  }),
  dispatch => ({
    fetchUsageData: ({ thingName, periodType }) =>
      dispatch(
        usage.fetchUsageData({
          thingName,
          periodType,
        })
      ),
  })
)(RefreshButton);

// UsageGraphScreen.navigationOptions = ({ navigation }) => {
//   const { params } = navigation.state;

//   return {
//     headerRight: () => (
//       <View style={styles.headerRight}>
//         {params && params.onShowTutorialOverlay && (
//           <View style={styles.btnHeader}>
//             <TouchableOpacity
//               hitSlop={styles.hitSlop}
//               onPress={params.onShowTutorialOverlay}>
//               <Image
//                 source={require('../../shared/images/general/help.png')}
//                 style={{ tintColor: Colors.linkGrey }}
//               />
//             </TouchableOpacity>
//           </View>
//         )}
//         <ConnectedRefreshButton />
//       </View>
//     ),
//   };
// };

const mapStateToProps = ({
  usage: {
    isLoading,
    error,
    usageToggle,
    selectedFromDate,
    selectedFromWeek,
    selectedFromMonth,
    selectedFromYear,
    usageByPeriod,
    curElectricRate,
    usageSetting: { thingName, periodType: curPeriod },
  },
}) => {
  const usageInThingName = usageByPeriod[thingName] || {};
  const usageInPeriod = usageInThingName[curPeriod] || {};
  const usageToggleData = usageToggle[thingName] || {};
  const usageTogglePeriod = usageToggleData[curPeriod] || {};
  const { displayValue } = usageInPeriod;
  const { displayAverageKwh } = usageInPeriod;
  return {
    data: usageInPeriod.data,
    compareData: usageInPeriod.compareData,
    compareDate: usageInPeriod.compareDate,
    periodType: curPeriod,
    setTemperature: usageTogglePeriod.setTemp,
    indoorTemperature: usageTogglePeriod.indoorTemp,
    energyConsumptions: usageTogglePeriod.energyConsumption,
    cpRT: usageTogglePeriod.cpRT,
    electricRate: usageTogglePeriod.electricRate,
    tnbRate: curElectricRate,
    isLoading,
    error,
    displayValue,
    displayAverageKwh,
    selectedFromDate,
    selectedFromWeek,
    selectedFromMonth,
    selectedFromYear,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchUsageData: ({ thingName, periodType }) => {
    dispatch(usage.fetchUsageData({ thingName, periodType }));
  },
  fetchCompareUsageData: ({ thingName, periodType, targetDate }) => {
    dispatch(
      usage.fetchUsageData({
        thingName,
        periodType,
        targetDate,
        isCompare: true,
      })
    );
  },
  clearUsageStatus: () => {
    dispatch(usage.clearUsageStatus());
  },
  setSelectedPeriod: periodType => {
    dispatch(usage.setSelectedPeriod(periodType));
  },
  clearCompareData: value => {
    dispatch(usage.clearCompareData(value));
  },
  setSelectedDate: value => {
    dispatch(usage.setSelectedDate(value));
  },
  setSelectedWeek: value => {
    dispatch(usage.setSelectedWeek(value));
  },
  setSelectedMonth: value => {
    dispatch(usage.setSelectedMonth(value));
  },
  setSelectedYear: value => {
    dispatch(usage.setSelectedYear(value));
  },
  setToggleButton: value => {
    dispatch(usage.setToggleButton(value));
  },
  setIsGraphCompareDateDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphCompareDateDisplayed(isDisplayed));
  },
  setIsGraphClearResultDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphClearResultDisplayed(isDisplayed));
  },
  setIsGraphToggleIndicatorDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphToggleIndicatorDisplayed(isDisplayed));
  },
  setIsGraphScrollLineIndicatorDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphScrollLineIndicatorDisplayed(isDisplayed));
  },
  setIsGraphElectricityBillRateDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphElectricityBillRateDisplayed(isDisplayed));
  },
  setIsGraphNavigatePeriodDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsGraphNavigatePeriodDisplayed(isDisplayed));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UsageGraphScreen);
