import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  ActivityIndicator,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  TextInput,
} from 'react-native';
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
  renderers,
} from 'react-native-popup-menu';
import Modal from 'react-native-modal';
import { connect, usage } from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  widthPercentage,
  timeConvert,
  ScaleText,
  scale,
  verticalScale,
} from '@module/utility';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Colors, Text, Button } from '@module/acson-ui';

import {
  PERIOD_MONTHLY,
  PERIOD_WEEKLY,
  PERIOD_YEARLY,
  PERIOD_DAILY,
} from '../../constants/timePeriod';

const STANDARD_DATE_FORMAT = 'DD MMMM YYYY';
const modalStyles = StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  wrap: {
    height: heightPercentage(25),
    backgroundColor: Colors.white,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    padding: 30,
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  content: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  title: {
    fontSize: ScaleText(14),
    fontWeight: 'bold',
  },
  buttonContainer: {
    paddingVertical: 5,
    width: widthPercentage(90),
  },
  buttonText: {
    color: Colors.white,
    fontSize: ScaleText(14),
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    width: 70,
    height: 50,
    fontSize: ScaleText(17),
    backgroundColor: 'transparent',
  },
});

const toggleStyles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  indoorIcon: {
    width: scale(20),
    height: scale(20),
    resizeMode: 'contain',
    marginRight: 10,
  },
  setTempIcon: {
    width: scale(35),
    height: scale(20),
    resizeMode: 'contain',
    marginRight: scale(10),
  },
  electricIcon: {
    width: 25,
    height: 25,
    resizeMode: 'contain',
    marginRight: 10,
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    margin: scale(8),
  },
  energyRowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
    marginLeft: 10,
  },
  indoorHeader: {
    backgroundColor: Colors.lightsalmon,
    flexDirection: 'row',
    borderRadius: 15,
    paddingHorizontal: 8,
    minWidth: widthPercentage(45),
    width: 'auto',
    height: heightPercentage(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  indoorContent: {
    backgroundColor: Colors.peach,
    borderRadius: 15,
  },
  padding10: {
    padding: 7,
  },
  valueContent: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  indicationLabel: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  flexCol: {
    flexDirection: 'column',
  },
  valueLabel: {
    fontWeight: '700',
    fontSize: ScaleText(18),
    marginRight: scale(5),
  },
  unitLabel: {
    fontWeight: '400',
    fontSize: ScaleText(12),
  },
  centerLayout: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  centerAlign: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleLabel: {
    fontSize: ScaleText(12),
  },
  dateLabel: {
    fontSize: ScaleText(10),
  },
  setTempContent: {
    backgroundColor: Colors.solitude,
    borderRadius: 15,
  },
  setTempHeader: {
    backgroundColor: Colors.lightskyblue,
    flexDirection: 'row',
    borderRadius: 15,
    paddingHorizontal: 8,
    minWidth: widthPercentage(45),
    width: 'auto',
    height: heightPercentage(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  energyContent: {
    backgroundColor: Colors.silk,
    borderRadius: 15,
  },
  energyHeader: {
    backgroundColor: Colors.caramel,
    flexDirection: 'row',
    borderRadius: 15,
    paddingHorizontal: 8,
    width: 'auto',
    minWidth: widthPercentage(45),
    height: heightPercentage(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  cprtContent: {
    backgroundColor: Colors.PalePink,
    borderRadius: 15,
  },
  cprtHeader: {
    backgroundColor: Colors.salmon,
    flexDirection: 'row',
    borderRadius: 15,
    paddingHorizontal: 8,
    width: 'auto',
    minWidth: widthPercentage(45),
    height: heightPercentage(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  tnbContent: {
    backgroundColor: Colors.beige,
    borderRadius: 15,
  },
  tnbHeader: {
    backgroundColor: Colors.yellowGreen,
    flexDirection: 'row',
    borderRadius: 15,
    paddingHorizontal: 8,
    width: 'auto',
    minWidth: widthPercentage(45),
    height: heightPercentage(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  passiveHeader: {
    backgroundColor: Colors.gainsboro,
    flexDirection: 'row',
    borderRadius: 15,
    paddingHorizontal: 8,
    width: 'auto',
    minWidth: widthPercentage(45),
    height: heightPercentage(5),
    justifyContent: 'center',
    alignItems: 'center',
  },
  passiveContent: {
    backgroundColor: Colors.whiteSmoke,
    borderRadius: 15,
  },
  legendLabel: {
    width: scale(10),
    height: verticalScale(10),
    marginRight: scale(3),
  },
  marginRight5: {
    marginRight: scale(5),
  },
  loading: {
    height: 5,
    width: 10,
    padding: 5,
  },
  row: {
    flexDirection: 'row',
  },
});

function getDateFormat(periodType, date) {
  switch (periodType) {
    case PERIOD_WEEKLY: {
      const firstDayOfWeek = moment(date)
        .startOf('isoWeek')
        .format('DD MMM YYYY');
      const lastDayOfWeek = moment(date).endOf('isoWeek').format('DD MMM YYYY');

      return `${firstDayOfWeek} - ${lastDayOfWeek}`;
    }
    case PERIOD_MONTHLY:
      return moment(date).format('MMMM YYYY');
    case PERIOD_YEARLY:
      return moment(date).format('YYYY');
    default:
      return moment(date).format(STANDARD_DATE_FORMAT);
  }
}

function findOccurences(str, charToCount) {
  return str.split(charToCount).length - 1;
}

function getCurrentData(indexValue, data) {
  if (typeof data !== 'undefined') {
    const item = indexValue - 1;
    if (data.length === indexValue) return item;
    if (data.length < indexValue) return 0;
    if (item === -1) return 0;
    return item;
  }
  return 0;
}

const ToogleContainer = ({
  data,
  periodType,
  indexValue,
  isInverter,
  compareData,
  comparedDate,
  setTemperature,
  indoorTemperature,
  energyConsumptions,
  enableEnergyConsumption,
  cpRT,
  isLoading,
  setToggleButton,
  thingName,
  electricRate,
  tnbRate,
  setTnbRate,
  groups,
}) => {
  const { Popover } = renderers;
  const [isVisible, setIsVisible] = useState(false);
  const [toParseRateValue, setToParseRateValue] = useState(0);
  const [rateValue, setRateValue] = useState('');
 
  const curPeriodData = data ? groups[0].graphData : data;
  const comparedPeriodData = compareData ? groups[1].graphData : compareData;

  const now = moment();
  const currentInfoIndicator = getCurrentData(indexValue, curPeriodData);

  const getValue = (type, isCompare) => {

    if (isCompare) {
      if (
        comparedPeriodData[currentInfoIndicator] === undefined ||
        comparedPeriodData[currentInfoIndicator][type] === 0 ||
        comparedPeriodData[currentInfoIndicator][type] === null
      ) {
        return '-';
      }
      return comparedPeriodData[currentInfoIndicator][type].toString();
    }

    if (
      curPeriodData === undefined ||
      curPeriodData[currentInfoIndicator][type] === 0 ||
      curPeriodData[currentInfoIndicator][type] === null
    ) {
      return '-';
    }

    if (curPeriodData[currentInfoIndicator].updatedOn === ' ') {
      return '31';
    }

    return curPeriodData[currentInfoIndicator][type].toString();
  };

  const getLengendColor = (type, isCompare) => {
    if (type === 'setTemp' && setTemperature) {
      return isCompare
        ? { borderWidth: 1, borderRadius: 15, backgroundColor: Colors.solitude }
        : {
            borderWidth: 1,
            borderRadius: 15,
            backgroundColor: Colors.lightskyblue,
          };
    }
    if (type === 'indoorTemp' && indoorTemperature) {
      return isCompare
        ? { borderWidth: 1, borderRadius: 15, backgroundColor: Colors.peach }
        : {
            borderWidth: 1,
            borderRadius: 15,
            backgroundColor: Colors.lightsalmon,
          };
    }
    if (type === 'kWh' && energyConsumptions) {
      return isCompare
        ? { borderWidth: 1, borderRadius: 15, backgroundColor: Colors.silk }
        : { borderWidth: 1, borderRadius: 15, backgroundColor: Colors.caramel };
    }
    if (type === 'cpRT' && cpRT) {
      return isCompare
        ? { borderWidth: 1, borderRadius: 15, backgroundColor: Colors.PalePink }
        : { borderWidth: 1, borderRadius: 15, backgroundColor: Colors.salmon };
    }
    if (type === 'electricRate' && electricRate) {
      return isCompare
        ? { borderWidth: 1, borderRadius: 15, backgroundColor: Colors.beige }
        : {
            borderWidth: 1,
            borderRadius: 15,
            backgroundColor: Colors.yellowGreen,
          };
    }
    return null;
  };

  const cprtTimeConverted = val => {
    const index = getCurrentData(indexValue, curPeriodData);
    if (val !== undefined) {
      if (val[index].cpRT >= 61 && periodType !== PERIOD_DAILY) {
        const { rhours, rminutes } = timeConvert(val[index].cpRT);
        return (
          <View style={toggleStyles.row}>
            <Text style={toggleStyles.valueLabel}>{`${rhours}`}</Text>
            <Text style={toggleStyles.unitLabel}>hours </Text>
            <Text style={toggleStyles.valueLabel}>{`${rminutes}`}</Text>
            <Text style={toggleStyles.unitLabel}>mins</Text>
          </View>
        );
      }
      return (
        <View style={toggleStyles.row}>
          <Text style={toggleStyles.valueLabel}>{`${Math.round(
            val[index].cpRT
          )}`}</Text>
          <Text style={toggleStyles.unitLabel}> mins</Text>
        </View>
      );
    }
    return null;
  };

  useEffect(() => {
    let num = rateValue;
    const countDot = findOccurences(num, '.');

    if ((num.length >= 2 && !countDot) || countDot) {
      num = num.replace('.', '');
      if (!num) {
        num = '0';
      }
      const baseNumber = 10 ** (num.length - 1);
      num = (parseInt(num, 10) / baseNumber).toFixed(num.length - 1);
      setRateValue(num);
    }
  }, [toParseRateValue]);

  const handleOnConfirm = () => {
    setTnbRate(Number(rateValue));
    setIsVisible(false);
  };

  const optionStyles = {
    optionWrapper: {
      width: widthPercentage(43),
      alignItems: 'center',
      backgroundColor: Colors.palegoldenrod,
    },
  };

  if (typeof data !== 'undefined') {
    return (
      <View style={toggleStyles.container}>
        <Modal
          isVisible={isVisible}
          coverScreen
          hasBackdrop
          backdropColor={Colors.modalBackdrop}
          style={modalStyles.container}
          onBackdropPress={() => setIsVisible(false)}
          useNativeDriver={Platform.OS === 'android'}>
          <KeyboardAvoidingView
            style={modalStyles.content}
            behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
            <View style={modalStyles.wrap}>
              <Text style={modalStyles.title}>Electricity Bill Rate</Text>
              <View style={modalStyles.inputContainer}>
                <Text style={modalStyles.title}>RM </Text>
                <TextInput
                  editable
                  numeric
                  keyboardType="decimal-pad"
                  value={`${rateValue}`}
                  placeholder={rateValue ? null : '0.001'}
                  style={modalStyles.input}
                  onChange={({ nativeEvent }) => {
                    setToParseRateValue(nativeEvent.eventCount);
                    setRateValue(nativeEvent.text);
                  }}
                  maxLength={5}
                />
              </View>

              <View style={modalStyles.buttonContainer}>
                <Button
                  raised
                  onPress={handleOnConfirm}
                  textStyle={modalStyles.buttonText}>
                  Confirm
                </Button>
              </View>
            </View>
          </KeyboardAvoidingView>
        </Modal>

        <View style={toggleStyles.rowContainer}>
          <TouchableWithoutFeedback
            onPress={() => {
              setToggleButton({ thingName, periodType, type: 'indoorTemp' });
            }}>
            <View
              style={
                indoorTemperature
                  ? toggleStyles.indoorContent
                  : toggleStyles.passiveContent
              }>
              <View
                style={
                  indoorTemperature
                    ? toggleStyles.indoorHeader
                    : toggleStyles.passiveHeader
                }>
                <Image
                  source={require('../../shared/images/graph/indoorTemp_grey.png')}
                  style={toggleStyles.indoorIcon}
                />
                <Text style={toggleStyles.titleLabel}>
                  Indoor {'\n'}Temperature
                </Text>
              </View>
              <View style={toggleStyles.padding10}>
                <View style={{ flexDirection: 'column' }}>
                  {compareData && (
                    <View style={toggleStyles.flexCol}>
                      <View style={toggleStyles.centerLayout}>
                        <View style={toggleStyles.centerAlign}>
                          <View
                            style={[
                              toggleStyles.legendLabel,
                              getLengendColor('indoorTemp', true),
                            ]}
                          />
                          {isLoading ? (
                            <View style={toggleStyles.loading}>
                              <ActivityIndicator size="small" />
                            </View>
                          ) : (
                            <View style={toggleStyles.centerAlign}>
                              <Text style={toggleStyles.valueLabel}>
                                {getValue('indoorTemp', 'isCompare')}
                              </Text>
                              <Text style={toggleStyles.unitLabel}>°C</Text>
                            </View>
                          )}
                        </View>
                      </View>
                      <View style={toggleStyles.indicationLabel}>
                        <Text style={toggleStyles.dateLabel}>
                          {`${getDateFormat(
                            periodType,
                            comparedDate
                          )} ${getValue('updatedOn')}`}
                        </Text>
                      </View>
                    </View>
                  )}
                  <View style={toggleStyles.flexCol}>
                    <View style={toggleStyles.centerLayout}>
                      <View style={toggleStyles.centerAlign}>
                        {compareData && (
                          <View
                            style={[
                              toggleStyles.legendLabel,
                              getLengendColor('indoorTemp', false),
                            ]}
                          />
                        )}
                        {isLoading ? (
                          <View style={toggleStyles.loading}>
                            <ActivityIndicator size="small" />
                          </View>
                        ) : (
                          <View style={toggleStyles.centerAlign}>
                            <Text style={toggleStyles.valueLabel}>
                              {getValue('indoorTemp')}
                            </Text>
                            <Text style={toggleStyles.unitLabel}>°C</Text>
                          </View>
                        )}
                      </View>
                    </View>
                    <View style={toggleStyles.indicationLabel}>
                      <Text style={toggleStyles.dateLabel}>
                        {`${getDateFormat(periodType, now)} ${getValue(
                          'updatedOn'
                        )}`}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>

          <TouchableWithoutFeedback
            onPress={() => {
              setToggleButton({ thingName, periodType, type: 'setTemp' });
            }}>
            <View
              style={
                setTemperature
                  ? toggleStyles.setTempContent
                  : toggleStyles.passiveContent
              }>
              <View
                style={
                  setTemperature
                    ? toggleStyles.setTempHeader
                    : toggleStyles.passiveHeader
                }>
                <Image
                  source={require('../../shared/images/graph/setTemp_grey.png')}
                  style={toggleStyles.setTempIcon}
                />
                <Text style={toggleStyles.titleLabel}>
                  Set {'\n'}Temperature
                </Text>
              </View>
              <View style={toggleStyles.padding10}>
                <View style={{ flexDirection: 'column' }}>
                  {compareData && (
                    <View style={toggleStyles.flexCol}>
                      <View style={toggleStyles.centerLayout}>
                        <View style={toggleStyles.centerAlign}>
                          <View
                            style={[
                              toggleStyles.legendLabel,
                              getLengendColor('setTemp', true),
                            ]}
                          />
                          {isLoading ? (
                            <View style={toggleStyles.loading}>
                              <ActivityIndicator size="small" />
                            </View>
                          ) : (
                            <View style={toggleStyles.centerAlign}>
                              <Text style={toggleStyles.valueLabel}>
                                {getValue('setTemp', 'isCompare')}
                              </Text>
                              <Text style={toggleStyles.unitLabel}>°C</Text>
                            </View>
                          )}
                        </View>
                      </View>
                      <View style={toggleStyles.indicationLabel}>
                        <Text style={toggleStyles.dateLabel}>
                          {`${getDateFormat(
                            periodType,
                            comparedDate
                          )} ${getValue('updatedOn')}`}
                        </Text>
                      </View>
                    </View>
                  )}
                  <View style={toggleStyles.flexCol}>
                    <View style={toggleStyles.centerLayout}>
                      <View style={toggleStyles.centerAlign}>
                        {compareData && (
                          <View
                            style={[
                              toggleStyles.legendLabel,
                              getLengendColor('setTemp', false),
                            ]}
                          />
                        )}
                        {isLoading ? (
                          <View style={toggleStyles.loading}>
                            <ActivityIndicator size="small" />
                          </View>
                        ) : (
                          <View style={toggleStyles.centerAlign}>
                            <Text style={toggleStyles.valueLabel}>
                              {getValue('setTemp')}
                            </Text>
                            <Text style={toggleStyles.unitLabel}>°C</Text>
                          </View>
                        )}
                      </View>
                    </View>
                    <View style={[toggleStyles.indicationLabel]}>
                      <Text style={toggleStyles.dateLabel}>
                        {`${getDateFormat(periodType, now)} ${getValue(
                          'updatedOn'
                        )}`}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </View>

        {isInverter && enableEnergyConsumption ? (
          <View style={toggleStyles.rowContainer}>
            <TouchableWithoutFeedback
              onPress={() => {
                setToggleButton({
                  thingName,
                  periodType,
                  type: 'energyConsumption',
                });
              }}>
              <View
                style={
                  energyConsumptions
                    ? toggleStyles.energyContent
                    : toggleStyles.passiveContent
                }>
                <View
                  style={
                    energyConsumptions
                      ? toggleStyles.energyHeader
                      : toggleStyles.passiveHeader
                  }>
                  <Image
                    source={require('../../shared/images/graph/energy_grey.png')}
                    style={toggleStyles.indoorIcon}
                  />
                  <Text style={toggleStyles.titleLabel}>
                    Energy {'\n'}Consumption
                  </Text>
                </View>
                <View style={toggleStyles.padding10}>
                  <View style={{ flexDirection: 'column' }}>
                    {compareData && (
                      <View
                        style={[
                          toggleStyles.flexCol,
                          toggleStyles.marginRight5,
                        ]}>
                        <View style={toggleStyles.centerLayout}>
                          <View style={toggleStyles.centerAlign}>
                            <View
                              style={[
                                toggleStyles.legendLabel,
                                getLengendColor('kWh', true),
                              ]}
                            />
                            {isLoading ? (
                              <View style={toggleStyles.loading}>
                                <ActivityIndicator size="small" />
                              </View>
                            ) : (
                              <View style={toggleStyles.centerAlign}>
                                <Text style={toggleStyles.valueLabel}>
                                  {getValue('kWh', 'isCompare')}
                                </Text>
                                <Text style={toggleStyles.unitLabel}>kWh</Text>
                              </View>
                            )}
                          </View>
                        </View>
                        <View style={toggleStyles.indicationLabel}>
                          <Text style={toggleStyles.dateLabel}>
                            {`${getDateFormat(
                              periodType,
                              comparedDate
                            )} ${getValue('updatedOn')}`}
                          </Text>
                        </View>
                      </View>
                    )}
                    <View style={toggleStyles.flexCol}>
                      <View style={toggleStyles.centerLayout}>
                        <View style={toggleStyles.centerAlign}>
                          {compareData && (
                            <View
                              style={[
                                toggleStyles.legendLabel,
                                getLengendColor('kWh', false),
                              ]}
                            />
                          )}
                          {isLoading ? (
                            <View style={toggleStyles.loading}>
                              <ActivityIndicator size="small" />
                            </View>
                          ) : (
                            <View style={toggleStyles.centerAlign}>
                              <Text style={toggleStyles.valueLabel}>
                                {getValue('kWh')}
                              </Text>
                              <Text style={toggleStyles.unitLabel}>kWh</Text>
                            </View>
                          )}
                        </View>
                      </View>
                      <View style={toggleStyles.indicationLabel}>
                        <Text style={toggleStyles.dateLabel}>
                          {`${getDateFormat(periodType, now)} ${getValue(
                            'updatedOn'
                          )}`}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback
              onPress={() => {
                setToggleButton({
                  thingName,
                  periodType,
                  type: 'electricRate',
                });
              }}>
              <View
                style={
                  electricRate
                    ? toggleStyles.tnbContent
                    : toggleStyles.passiveContent
                }>
                <View
                  style={
                    electricRate
                      ? toggleStyles.tnbHeader
                      : toggleStyles.passiveHeader
                  }>
                  <Image
                    source={require('../../shared/images/graph/tnb_rate_ON.png')}
                    style={toggleStyles.electricIcon}
                  />
                  <Text style={toggleStyles.titleLabel}>
                    Electricity {'\n'}Bill
                  </Text>
                  <Menu
                    renderer={Popover}
                    rendererProps={{
                      preferredPlacement: 'bottom',
                      placement: 'top',
                      anchorStyle: {
                        backgroundColor: 'transparent',
                      },
                    }}
                    style={{
                      position: 'absolute',
                      right: 5,
                    }}>
                    <MenuTrigger
                      disabled={!electricRate}
                      customStyles={{
                        TriggerTouchableComponent: TouchableOpacity,
                        triggerWrapper: {
                          height: 30,
                          width: 35,
                          justifyContent: 'center',
                          alignItems: 'center',
                        },
                      }}>
                      <Image
                        source={require('../../shared/images/graph/dropDown.png')}
                      />
                    </MenuTrigger>
                    <MenuOptions
                      customStyles={{
                        optionsContainer: {
                          right: 14,
                        },
                        optionsWrapper: {
                          width: widthPercentage(43),
                          justifyContent: 'center',
                          alignItems: 'center',
                          backgroundColor: Colors.white,
                          borderBottomWidth: 1,
                        },
                        optionWrapper: {
                          width: widthPercentage(43),
                          alignItems: 'center',
                        },
                      }}>
                      <MenuOption
                        customStyles={tnbRate === 0.218 ? optionStyles : null}
                        onSelect={() => setTnbRate(0.218)}
                        text="RM 0.218"
                      />
                      <MenuOption
                        customStyles={tnbRate === 0.334 ? optionStyles : null}
                        onSelect={() => setTnbRate(0.334)}
                        text="RM 0.334"
                      />
                      <MenuOption
                        customStyles={tnbRate === 0.516 ? optionStyles : null}
                        onSelect={() => setTnbRate(0.516)}
                        text="RM 0.516"
                      />
                      <MenuOption
                        customStyles={tnbRate === 0.546 ? optionStyles : null}
                        onSelect={() => setTnbRate(0.546)}
                        text="RM 0.546"
                      />
                      <MenuOption
                        customStyles={tnbRate === 0.571 ? optionStyles : null}
                        onSelect={() => setTnbRate(0.571)}
                        text="RM 0.571"
                      />
                      <MenuOption
                        customStyles={
                          tnbRate !== 0.218 &&
                          tnbRate !== 0.334 &&
                          tnbRate !== 0.516 &&
                          tnbRate !== 0.546 &&
                          tnbRate !== 0.571
                            ? optionStyles
                            : null
                        }
                        onSelect={() => setIsVisible(true)}
                        text="Others"
                      />
                    </MenuOptions>
                  </Menu>
                </View>
                <View style={toggleStyles.padding10}>
                  <View style={{ flexDirection: 'column' }}>
                    {compareData && (
                      <View style={toggleStyles.flexCol}>
                        <View style={toggleStyles.centerLayout}>
                          <View style={toggleStyles.centerAlign}>
                            <View
                              style={[
                                toggleStyles.legendLabel,
                                getLengendColor('electricRate', true),
                              ]}
                            />
                            {isLoading ? (
                              <View style={toggleStyles.loading}>
                                <ActivityIndicator size="small" />
                              </View>
                            ) : (
                              <View style={toggleStyles.centerAlign}>
                                <Text style={toggleStyles.unitLabel}>RM </Text>
                                <Text style={toggleStyles.valueLabel}>
                                  {getValue('electricRate', 'isCompare')}
                                </Text>
                              </View>
                            )}
                          </View>
                        </View>
                        <View style={toggleStyles.indicationLabel}>
                          <Text style={toggleStyles.dateLabel}>
                            {`${getDateFormat(
                              periodType,
                              comparedDate
                            )} ${getValue('updatedOn')}`}
                          </Text>
                        </View>
                      </View>
                    )}
                    <View style={toggleStyles.flexCol}>
                      <View style={toggleStyles.centerLayout}>
                        <View style={toggleStyles.centerAlign}>
                          {compareData && (
                            <View
                              style={[
                                toggleStyles.legendLabel,
                                getLengendColor('electricRate', false),
                              ]}
                            />
                          )}
                          {isLoading ? (
                            <View style={toggleStyles.loading}>
                              <ActivityIndicator size="small" />
                            </View>
                          ) : (
                            <View style={toggleStyles.centerAlign}>
                              <Text style={toggleStyles.unitLabel}>RM </Text>
                              <Text style={toggleStyles.valueLabel}>
                                {getValue('electricRate')}
                              </Text>
                            </View>
                          )}
                        </View>
                      </View>
                      <View style={toggleStyles.indicationLabel}>
                        <Text style={toggleStyles.dateLabel}>
                          {`${getDateFormat(periodType, now)} ${getValue(
                            'updatedOn'
                          )}`}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        ) : (
          <View
            style={[
              toggleStyles.rowContainer,
              { justifyContent: 'flex-start' },
            ]}>
            <TouchableWithoutFeedback
              onPress={() => {
                setToggleButton({ thingName, periodType, type: 'cpRT' });
              }}>
              <View
                style={
                  cpRT ? toggleStyles.cprtContent : toggleStyles.passiveContent
                }>
                <View
                  style={
                    cpRT ? toggleStyles.cprtHeader : toggleStyles.passiveHeader
                  }>
                  <Image
                    source={require('../../shared/images/graph/cprt_black.png')}
                    style={toggleStyles.indoorIcon}
                  />
                  <Text style={toggleStyles.titleLabel}>
                    Compressor {'\n'}Time
                  </Text>
                </View>
                <View style={toggleStyles.padding10}>
                  <View style={{ flexDirection: 'column' }}>
                    {compareData && (
                      <View style={toggleStyles.flexCol}>
                        <View style={toggleStyles.centerLayout}>
                          <View style={toggleStyles.centerAlign}>
                            <View
                              style={[
                                toggleStyles.legendLabel,
                                getLengendColor('cpRT', true),
                              ]}
                            />
                            {isLoading ? (
                              <View style={toggleStyles.loading}>
                                <ActivityIndicator size="small" />
                              </View>
                            ) : (
                              cprtTimeConverted(comparedPeriodData)
                            )}
                          </View>
                        </View>
                        <View style={toggleStyles.indicationLabel}>
                          <Text style={toggleStyles.dateLabel}>
                            {`${getDateFormat(
                              periodType,
                              comparedDate
                            )} ${getValue('updatedOn')}`}
                          </Text>
                        </View>
                      </View>
                    )}
                    <View style={toggleStyles.flexCol}>
                      <View style={toggleStyles.centerLayout}>
                        <View style={toggleStyles.centerAlign}>
                          {compareData && (
                            <View
                              style={[
                                toggleStyles.legendLabel,
                                getLengendColor('cpRT', false),
                              ]}
                            />
                          )}
                          {isLoading ? (
                            <View style={toggleStyles.loading}>
                              <ActivityIndicator size="small" />
                            </View>
                          ) : (
                            cprtTimeConverted(curPeriodData)
                          )}
                        </View>
                      </View>
                      <View style={toggleStyles.indicationLabel}>
                        <Text style={toggleStyles.dateLabel}>
                          {`${getDateFormat(periodType, now)} ${getValue(
                            'updatedOn'
                          )}`}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        )}
      </View>
    );
  }
  return null;
};

ToogleContainer.propTypes = {
  isInverter: PropTypes.bool,
  enableEnergyConsumption: PropTypes.bool,
  compareData: PropTypes.array,
  data: PropTypes.array,
  comparedDate: PropTypes.any,
  periodType: PropTypes.string,
  thingName: PropTypes.string,
  indexValue: PropTypes.number,
  setTemperature: PropTypes.bool,
  indoorTemperature: PropTypes.bool,
  energyConsumptions: PropTypes.bool,
  cpRT: PropTypes.bool,
  electricRate: PropTypes.bool,
  isLoading: PropTypes.bool,
  setToggleButton: PropTypes.func,
  setTnbRate: PropTypes.func,
  tnbRate: PropTypes.number,
  groups: PropTypes.array,
};

const mapStateToProps = ({
  usage: {
    usageByPeriod,
    curElectricRate,
    usageSetting: { thingName, periodType },
  },
}) => {
  const usageInThingName = usageByPeriod[thingName] || {};
  const usageInPeriod = usageInThingName[periodType] || {};

  return {
    data: usageInPeriod.data,
    compareData: usageInPeriod.compareData,
    comparedDate: usageInPeriod.compareDate,
    tnbRate: curElectricRate,
    periodType,
  };
};

const mapDispatchToProps = dispatch => ({
  setTnbRate: value => {
    dispatch(usage.setElectricRate(value));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ToogleContainer);
