import React, { useEffect, useRef, useCallback } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  ActivityIndicator,
  SectionList,
  Image,
  TouchableOpacity,
  RefreshControl,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  connect,
  selector,
  unitUsers,
  unbindDevice,
  units,
  controlUsageLimit,
} from '@daikin-dama/redux-iot';
import {
  ScaleText,
  heightPercentage,
  StringHelper,
  NavigationService,
} from '@module/utility';

import { useFocusEffect } from '@react-navigation/native';
import {
  Card,
  Text,
  Loading,
  Colors,
  useHandleResponse,
  ModalService,
} from '@module/acson-ui';
import UserCard from '../../components/UserCard';
import QXInfo from '../../constants/qxInfo';
import {
  SHARE_QR_SCREEN,
  HOME_SCREEN,
  UNIT_USER_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flexGrow: 1,
  },
  container: {
    paddingVertical: 25,
    backgroundColor: Colors.white,
  },
  unitTitle: {
    lineHeight: ScaleText(20),
    fontWeight: '700',
  },
  loading: {
    marginTop: heightPercentage(5),
  },
  header: {
    marginTop: heightPercentage(3),
    marginHorizontal: 25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  name: {
    fontSize: 16,
    fontWeight: '500',
  },
  hipslop: {
    top: 10,
    bottom: 10,
    right: 20,
    left: 20,
  },
  icon: {
    height: 30,
  },
  marginTop: {
    marginTop: 5,
  },
});

const handleShare = () => {
  ModalService.show({
    desc:
      'Do you want to allow user to share this device with others and overwrite weekly timer?\n\nYou can still change this.',
    buttons: [
      {
        title: 'Yes',
        onPress: () => {
          NavigationService.navigate(SHARE_QR_SCREEN, {
            Qx: QXInfo.GUEST_SHARE_ALLOWED,
          });
        },
      },
      {
        title: 'No',
        onPress: () => {
          NavigationService.navigate(SHARE_QR_SCREEN, {
            Qx: QXInfo.GUEST_SHARE_NOT_ALLOWED,
          });
        },
        isRaised: true,
      },
      {
        title: 'Cancel',
        hasBorder: false,
      },
    ],
  });
};

const getLoadingMsg = isGuestLoading => {
  if (isGuestLoading) return 'Removing guest user...';

  return 'Removing unit...';
};

const UnitUsers = ({
  ACName,
  thingName,
  firmwareVersion,
  fetchUnitUsers,
  unitUserList,
  isLoading,
  qx,
  profile,
  submitUnbindUnit,
  unbindUnitLoading,
  unbindUnitSuccess,
  unbindUnitError,
  unbindGuestSuccess,
  unbindGuestError,
  isGuestLoading,
  removeGuest,
  clearRemoveGuestStatus,
  clearUnbindUnitStatus,
  submitUnbindAllGuest,
  canShareUnit,
  getAllUnits,
  checkExpiryTime,
}) => {
  const sectionListRef = useRef([]);

  useFocusEffect(
    useCallback(() => {
      fetchUnitUsers();
      clearRemoveGuestStatus();
    }, [])
  );

  const onRefresh = () => {
    checkExpiryTime({ thingName });
    getAllUnits(profile.email);
    fetchUnitUsers();
  };

  useEffect(() => {
    const canSetRef =
      unitUserList &&
      unitUserList[1] &&
      unitUserList[1].data &&
      unitUserList[1].data.length > 0;
    if (canSetRef) {
      sectionListRef.current = new Array(unitUserList[1].data.length)
        .fill(0)
        .map(() => React.createRef());
    }
  }, [unitUserList]);

  const handleUnbind = () => {
    ModalService.show({
      title: 'Remove',
      desc: 'Are you sure you want to remove this\n unit?',
      buttons: [
        {
          title: 'Yes',
          onPress: () => submitUnbindUnit(thingName),
        },
        {
          title: 'No',
          isRaised: true,
        },
      ],
    });
  };

  const handleUnbindGuest = index => {
    const totalGuest =
      unitUserList && unitUserList[1] && unitUserList[1].data
        ? unitUserList[1].data.length
        : 0;
    const checkList = Array(totalGuest).fill(false);
    checkList[index] = true;
    ModalService.show({
      title: 'Remove guest',
      desc: 'Are you sure you want to remove this\n guest user?',
      buttons: [
        {
          title: 'Yes',
          onPress: () => removeGuest(checkList, thingName),
        },
        {
          title: 'No',
          onPress: () => {
            const curRef = sectionListRef.current[index];
            if (curRef) {
              // Close the drawer if user click no
              curRef.current.snapTo({ index: 0 });
            }
          },
          isRaised: true,
        },
      ],
    });
  };

  const handleUnbindAllGuest = () => {
    ModalService.show({
      title: 'Remove all guests',
      desc: 'Are you sure you want to remove all the guests for this unit?',
      buttons: [
        {
          title: 'Yes',
          onPress: () => submitUnbindAllGuest(thingName, true),
        },
        {
          title: 'No',
          isRaised: true,
        },
      ],
    });
  };

  useHandleResponse({
    success: unbindUnitSuccess,
    error: unbindUnitError,
    showSuccess: true,
    onSuccess: () => NavigationService.navigate(HOME_SCREEN),
    clearStatus: clearUnbindUnitStatus,
  });

  useHandleResponse({
    success: unbindGuestSuccess,
    error: unbindGuestError,
    showSuccess: true,
    clearStatus: clearRemoveGuestStatus,
  });

  return (
    <SafeAreaView style={styles.flex}>
      <Loading visible={unbindUnitLoading || isGuestLoading}>
        {getLoadingMsg(isGuestLoading)}
      </Loading>
      <ScrollView
        contentContainerStyle={[styles.flex, styles.container]}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={onRefresh}
            size={15}
            titleColor={Colors.titleGrey}
          />
        }>
        <Card>
          <Card.Content>
            <Text medium style={styles.unitTitle}>
              {ACName}
            </Text>
            <View style={styles.marginTop}>
              <Text color={Colors.grey}>
                {`Module ID: ${StringHelper.removePrefix(thingName)}`}
              </Text>
            </View>
            <Text color={Colors.grey}>Firmware Version: {firmwareVersion}</Text>
          </Card.Content>
          {(qx === QXInfo.HOST || canShareUnit) && (
            <Card.Button
              iconSource={require('../../shared/images/unitUser/share.png')}
              onPress={handleShare}
            />
          )}
          <Card.Button
            iconSource={require('../../shared/images/unitUser/block.png')}
            onPress={handleUnbind}
          />
        </Card>
        {isLoading ? (
          <View style={styles.loading}>
            <ActivityIndicator size="small" />
          </View>
        ) : (
          <SectionList
            sections={unitUserList}
            keyExtractor={(email, index) => email + index}
            /* eslint-disable no-unused-vars */
            renderItem={({ item, index, section }) => (
              <View>
                {(qx === QXInfo.HOST ||
                  section.title === 'Host User' ||
                  profile.email === item.email) && (
                  <UserCard
                    isHost={section.title === 'Host User'}
                    name={item.name}
                    email={item.email}
                    expiryTime={item.expiryTime}
                    onPressCard={() =>
                      NavigationService.navigate(UNIT_USER_SCREEN, {
                        user: item,
                      })
                    }
                    onDragRight={() => handleUnbindGuest(index)}
                    canDrag={
                      section.title === 'Guest User' &&
                      profile.email !== item.email
                    }
                    currentUserIsHost={qx === QXInfo.HOST}
                    ref={
                      section.title !== 'Host User'
                        ? sectionListRef.current[index]
                        : null
                    }
                  />
                )}
              </View>
            )}
            /* eslint-enable no-unused-vars */
            renderSectionHeader={({ section: { title } }) => (
              <View style={styles.header}>
                <Text style={styles.name}>{title}</Text>
                {title === 'Guest User' && qx === QXInfo.HOST && (
                  <View
                    style={{
                      marginRight: 30,
                      alignItems: 'flex-end',
                    }}>
                    <TouchableOpacity
                      hitSlop={styles.hipslop}
                      onPress={handleUnbindAllGuest}>
                      <Image
                        source={require('../../shared/images/unitUser/unbindGuest.png')}
                        resizeMode="contain"
                        style={styles.icon}
                      />
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            )}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

UnitUsers.propTypes = {
  fetchUnitUsers: PropTypes.func,
  submitUnbindUnit: PropTypes.func,
  removeGuest: PropTypes.func,
  clearRemoveGuestStatus: PropTypes.func,
  clearUnbindUnitStatus: PropTypes.func,
  submitUnbindAllGuest: PropTypes.func,

  ACName: PropTypes.string,
  thingName: PropTypes.string,
  firmwareVersion: PropTypes.string,
  unitUserList: PropTypes.array,
  isLoading: PropTypes.bool,
  qx: PropTypes.string,
  profile: PropTypes.object,
  unbindUnitLoading: PropTypes.bool,
  unbindUnitSuccess: PropTypes.string,
  unbindUnitError: PropTypes.string,
  unbindGuestSuccess: PropTypes.string,
  unbindGuestError: PropTypes.string,
  isGuestLoading: PropTypes.bool,
  canShareUnit: PropTypes.bool,
  getAllUnits: PropTypes.func,
  checkExpiryTime: PropTypes.func,
};

const mapStateToProps = state => {
  const {
    ACName,
    thingName,
    qx,
    firmwareVersion,
    shareUnit,
  } = selector.getRemoteUnit(state);
  const {
    unitUsers: { unitUserList, isLoading },
    user: { profile },
    unbindDevice: {
      success: unbindUnitSuccess,
      error: unbindUnitError,
      isLoading: unbindUnitLoading,
      guestSuccess: unbindGuestSuccess,
      guestError: unbindGuestError,
      isGuestLoading,
    },
  } = state;

  return {
    ACName,
    thingName,
    firmwareVersion,
    unitUserList,
    isLoading,
    qx,
    profile,
    unbindUnitSuccess,
    unbindUnitError,
    unbindUnitLoading,
    unbindGuestSuccess,
    unbindGuestError,
    isGuestLoading,
    canShareUnit: shareUnit === 1,
  };
};

const mapDispatchToProps = dispatch => ({
  fetchUnitUsers: () => {
    dispatch(unitUsers.fetchUnitUsers());
  },
  getAllUnits: email => {
    dispatch(units.getAllUnits({ email }));
  },
  submitUnbindUnit: thingName => {
    dispatch(unbindDevice.submitUnbindDevice({ thingName }));
  },
  clearUnbindUnitStatus: () => {
    dispatch(unbindDevice.clearUnbindDeviceStatus());
  },
  removeGuest: (checklist, thingName) =>
    dispatch(unbindDevice.submitRemoveGuests({ checklist, thingName })),
  clearRemoveGuestStatus: () =>
    dispatch(unbindDevice.clearUnbindDeviceStatus()),
  submitUnbindAllGuest: (thingName, isRemoveAllGuest) => {
    dispatch(unbindDevice.submitRemoveGuests({ thingName, isRemoveAllGuest }));
  },
  checkExpiryTime: () => {
    dispatch(controlUsageLimit.checkExpiryTime());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UnitUsers);
