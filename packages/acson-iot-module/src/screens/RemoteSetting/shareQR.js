import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  ImageBackground,
  ActivityIndicator,
} from 'react-native';
import PropTypes from 'prop-types';
import QRCode from 'react-native-qrcode-svg';
import Config from 'react-native-config';
import {
  heightPercentage,
  widthPercentage,
  ScaleText,
  StringHelper,
  scale,
} from '@module/utility';
import { connect, selector, QRCodeHelper } from '@daikin-dama/redux-iot';

import { Text, Colors, Timer } from '@module/acson-ui';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    backgroundColor: Colors.darkRed,
  },
  inner: {
    borderRadius: 15,
    backgroundColor: Colors.white,
    height: heightPercentage(65),
    marginTop: heightPercentage(8),
    marginHorizontal: widthPercentage(10),
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  frameBackground: {
    width: scale(200),
    height: scale(200),
  },
  unitTitle: {
    fontSize: ScaleText(20),
    lineHeight: ScaleText(22),
    fontWeight: 'bold',
    marginBottom: 8,
  },
  unitDesc: {
    fontSize: ScaleText(15),
    lineHeight: 20,
    color: Colors.grey,
  },
  content: {},
  timer: {
    marginTop: 15,
    fontSize: 12,
    textAlign: 'center',
  },
});

const ShareQR = ({
  thingName,
  acGroup,
  acName,
  logo,
  randomKey,
  expiredDuration,
  route,
}) => {
  const qxInfo = route.params.Qx;
  const [isRefresh, setRefresh] = useState(false);
  const [qrCodeText, setQRCodeText] = useState('');

  const toggleRefresh = () => {
    setRefresh(prevRefresh => !prevRefresh);
  };

  useEffect(() => {
    setQRCodeText('');
    if (
      thingName &&
      acName &&
      acGroup &&
      logo &&
      randomKey &&
      expiredDuration
    ) {
      const encryQRCode = QRCodeHelper.getEncryptedQRPayload({
        thingName: StringHelper.removePrefix(thingName),
        acGroup,
        acName,
        logo,
        randomKey,
        expiredDuration,
        qxInfo,
        secretKey: Config.QRCODE_SECRET_KEY,
        iv: Config.QRCODE_IV,
        unitCode: 'A',
      });

      if (encryQRCode) {
        setQRCodeText(encryQRCode);
      }
    }
  }, [isRefresh, thingName, acName, acGroup, logo, randomKey, expiredDuration]);

  return (
    <SafeAreaView style={[styles.flex, styles.container]}>
      <View style={[styles.inner, styles.center]}>
        <View style={styles.content}>
          <View style={{ marginBottom: 30 }}>
            <Text medium style={styles.unitTitle}>
              {acName}
            </Text>
            <Text style={styles.unitDesc}>Module ID:</Text>
            <Text style={styles.unitDesc}>
              {StringHelper.removePrefix(thingName)}
            </Text>
          </View>
          <ImageBackground
            source={require('../../shared/images/general/frame.png')}
            style={[styles.frameBackground, styles.center]}>
            {!qrCodeText ? (
              <ActivityIndicator size="large" />
            ) : (
              <QRCode value={qrCodeText} size={widthPercentage(40)} />
            )}
          </ImageBackground>
          <Text small style={styles.timer}>
            QR Code will be refreshed in{' '}
            <Timer
              duration={expiredDuration}
              onTimeOut={toggleRefresh}
              isAutoRefresh
              momentFormat
            />
          </Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

ShareQR.propTypes = {
  thingName: PropTypes.string,
  acGroup: PropTypes.string,
  acName: PropTypes.string,
  logo: PropTypes.string,
  randomKey: PropTypes.string,
  expiredDuration: PropTypes.number,
  route: PropTypes.object,
};

const mapStateToProps = state => {
  const {
    settings: { qrValidityPeriod },
  } = state;

  const {
    ThingName: thingName,
    ACGroup: acGroup,
    ACName: acName,
    Logo: logo,
    key: randomKey,
  } = selector.getRemoteUnit(state);

  return {
    thingName,
    acGroup,
    acName,
    logo,
    randomKey,
    expiredDuration: qrValidityPeriod.value,
  };
};

export default connect(mapStateToProps)(ShareQR);
