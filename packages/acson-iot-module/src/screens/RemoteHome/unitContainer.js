import React, { useState } from 'react';
import { Platform } from 'react-native';
import PropTypes from 'prop-types';
import {
  connect,
  homepageIotShadow,
  remote,
  updateFirmware,
  editUnits,
} from '@daikin-dama/redux-iot';
import Toast from 'react-native-root-toast';

import { UnitCard, Text, Colors } from '@module/acson-ui';
import { NavigationService } from '@module/utility';
import { REMOTE_SCREEN } from '../../constants/routeNames';

import MoveGroupSelectModal from './moveGroupSelectModal';

function showAlert() {
  Toast.show(
    <Text maxFontSizeMultiplier={1}>
      {' '}
      This unit{'\n'}is disconnected.{'\n'}Please reconnect first.
    </Text>,
    {
      duration: Toast.durations.LONG,
      position: Toast.positions.BOTTOM,
      shadow: false,
      animation: true,
      hideOnPress: true,
      delay: 0,
      textColor: Colors.darkGrey,
      backgroundColor: Colors.lightCoolGrey,
      opacity: 0.6,
    }
  );
}

function UnitContainer({
  item,
  groupKey,
  unit,
  drag,
  unitLoadingList,
  switchAc,
  setRemoteUnit,
  isFetchUnitsLoading,
  submitFirmwareUpgrade,
  isEditLoading,
  moveGroup,
  isEdit,
}) {
  const [showMoveGroupModal, setShowMoveGroupModal] = useState(false);

  const onModalCancel = () => setShowMoveGroupModal(false);

  const handleOnPressMoveGroup = () => setShowMoveGroupModal(true);

  const onSelect = targetKey => {
    moveGroup(groupKey, item, targetKey);
  };

  const toggleSwitch = () => {
    const switchType = unit.switch === 1 ? 0 : 1;
    if (unit.status === 'connected') {
      switchAc(groupKey, unit.ThingName, switchType);
    } else {
      showAlert();
    }
  };

  const handleOnPressUnit = () => {
    if (unit.indoorTemp) {
      setRemoteUnit(unit.thingName);
      NavigationService.navigate(REMOTE_SCREEN);
    }
  };

  return (
    <>
      <UnitCard
        key={unit.thingName}
        status={unit.status}
        switchState={unit.switch}
        outdoorTemp={unit.outdoorTemp}
        indoorTemp={unit.indoorTemp}
        groupName={unit.ACGroup}
        unitName={unit.ACName}
        thingName={unit.thingName}
        unitLoadingList={unitLoadingList}
        onPressSwitch={toggleSwitch}
        onPressCard={handleOnPressUnit}
        needUpgrade={unit.needUpgrade}
        submitFirmwareUpgrade={submitFirmwareUpgrade}
        isFetchUnitsLoading={isFetchUnitsLoading}
        onPressMoveGroup={handleOnPressMoveGroup}
        isEditLoading={isEditLoading}
        drag={drag}
        isEdit={isEdit}
      />
      <MoveGroupSelectModal
        isVisible={showMoveGroupModal}
        onCancel={onModalCancel}
        onSelect={onSelect}
        sectionId={groupKey}
      />
    </>
  );
}

const mapStateToProps = (
  {
    units: { allUnits, isLoading: isFetchUnitsLoading },
    homepageIotShadow: { unitLoadingList },
    editUnits: { isLoading: isEditLoading, isEdit },
  },
  { item }
) => ({
  unit: allUnits[item] || {},
  unitLoadingList,
  isFetchUnitsLoading,
  isEditLoading,
  isEdit,
});

const mapDispatchToProps = dispatch => ({
  switchAc: (targetGroupKey, thingName, switchType) => {
    dispatch(homepageIotShadow.switchAc(targetGroupKey, thingName, switchType));
  },
  setRemoteUnit: thingName => {
    dispatch(remote.setRemoteUnit({ thingName }));
  },
  submitFirmwareUpgrade: thingName => {
    dispatch(
      updateFirmware.submitFirmwareUpgrade({
        ios: Platform.select({ ios: true, android: false }),
        thingName,
      })
    );
  },
  moveGroup: (fromGroup, unit, targetGroup) => {
    dispatch(editUnits.setItemToDiffGroup({ fromGroup, unit, targetGroup }));
  },
});

UnitContainer.propTypes = {
  item: PropTypes.string,
  groupKey: PropTypes.string,
  unit: PropTypes.object,
  drag: PropTypes.func,
  unitLoadingList: PropTypes.object,
  switchAc: PropTypes.func,
  setRemoteUnit: PropTypes.func,
  submitFirmwareUpgrade: PropTypes.func,
  isFetchUnitsLoading: PropTypes.bool,
  isEditLoading: PropTypes.bool,
  moveGroup: PropTypes.func,
  isEdit: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(UnitContainer);
