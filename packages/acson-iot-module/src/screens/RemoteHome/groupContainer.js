import React, { useState } from 'react';
import { View, Image, TouchableOpacity, Alert } from 'react-native';

import PropTypes from 'prop-types';
import Toast from 'react-native-root-toast';
import {
  connect,
  editUnits as editUnitsAction,
  homepageIotShadow,
} from '@daikin-dama/redux-iot';

import {
  SortableScrollView,
  ModalService,
  Colors,
  Text,
} from '@module/acson-ui';

import styles, { GROUP_TITLE_HEIGHT } from './styles';
import UnitContainer from './unitContainer';
import EditGroupModal from './groupModal';

const EditGroupIcon = ({
  canEdit,
  groupName,
  groupKey,
  submitEditGroupName,
  isLoading,
}) => {
  const [editModalShow, setEditModalShow] = useState(false);

  const onCancel = () => {
    setEditModalShow(false);
  };

  const onSubmit = name => {
    submitEditGroupName(groupKey, name);
    onCancel();
  };

  return (
    canEdit && (
      <>
        <TouchableOpacity
          onPress={() => setEditModalShow(true)}
          delayLongPress={300}>
          <Image
            source={require('../../shared/images/general/pencil.png')}
            style={styles.pencilIcon}
          />
        </TouchableOpacity>
        <EditGroupModal
          isVisible={editModalShow}
          value={groupName}
          onCancel={onCancel}
          onSubmit={onSubmit}
          title="Edit group name"
          customSubmitText="Confirm"
          isLoading={isLoading}
          inputPlaceHolder="Group Name"
        />
      </>
    )
  );
};

const mapEditGroupIconDispatchToProps = dispatch => ({
  submitEditGroupName: (id, name) => {
    dispatch(editUnitsAction.setItemName({ id, name }));
  },
  clearEditStatus: () => dispatch(editUnitsAction.clearEditStatus()),
});

const mapEditGroupIconStateToProps = ({ editUnits: { isLoading } }) => ({
  isLoading,
});

EditGroupIcon.propTypes = {
  canEdit: PropTypes.bool,
  groupName: PropTypes.string,
  groupKey: PropTypes.string,
  submitEditGroupName: PropTypes.func,
  isLoading: PropTypes.bool,
};

const EditGroupIconContainer = connect(
  mapEditGroupIconStateToProps,
  mapEditGroupIconDispatchToProps
)(EditGroupIcon);

const GroupContainer = ({
  item,
  editGroupData,
  groupData,
  drag,
  switchGroupAc,
  removeGroup,
  setOrder,
  isEdit,
}) => {
  const canEditDragGroup = item && item.id !== '-1';

  const groupOnOffIcon = group => {
    if (group.switch) {
      // If the units in the group that is connected is turn on
      return require('../../shared/images/remote/on.png');
    }
    if (!group.isConnected) {
      // If there is no units being connected, then group switch is consider off
      return require('../../shared/images/remote/off.png');
    }

    return require('../../shared/images/remote/turnOn.png');
  };

  const showAlert = offlineUnit => {
    if (offlineUnit) {
      Toast.show(
        <Text maxFontSizeMultiplier={1}>
          All unitsn{'\n'} are disconnected.{'\n'}Please reconnect your unit
          first
        </Text>,
        {
          duration: Toast.durations.LONG,
          position: Toast.positions.BOTTOM,
          shadow: false,
          animation: true,
          hideOnPress: true,
          delay: 0,
          textColor: Colors.darkGrey,
          backgroundColor: Colors.lightCoolGrey,
          opacity: 0.6,
        }
      );
    } else {
      Toast.show(
        <Text maxFontSizeMultiplier={1}>
          {' '}
          This unit{'\n'}is disconnected.{'\n'}Please reconnect first.
        </Text>,
        {
          duration: Toast.durations.LONG,
          position: Toast.positions.BOTTOM,
          shadow: false,
          animation: true,
          hideOnPress: true,
          delay: 0,
          textColor: Colors.darkGrey,
          backgroundColor: Colors.lightCoolGrey,
          opacity: 0.6,
        }
      );
    }
  };

  const showUngroupAlert = () => {
    ModalService.show({
      title: 'Are you sure you want to delete this group?',
      desc: 'Your units in this group will become ungrouped.',
      buttons: [
        {
          title: 'Yes',
          onPress: () => removeGroup(item.id),
          isRaised: true,
        },
        { title: 'No' },
      ],
    });
  };

  const switchGroupAlert = () => {
    const switchType = groupData.switch ? 'off' : 'on';
    if (!groupData.isConnected) {
      const offUnit = true;
      showAlert(offUnit);
      return;
    }

    Alert.alert('', `Turn ${switchType} all units in this group?`, [
      { text: 'Cancel' },
      {
        text: 'OK',
        onPress: () => {
          switchGroupAc(groupData.groupIndex, !groupData.switch);
        },
      },
    ]);
  };

  return (
    <View key={groupData.groupIndex}>
      <View style={[styles.groupRow, styles.marginTop4]}>
        <View style={styles.groupRowInner}>
          <View style={styles.center}>
            {canEditDragGroup && isEdit && (
              <TouchableOpacity
                onLongPress={drag}
                hitSlop={styles.hitSlop}
                style={styles.marginRight20}>
                <Image
                  source={require('../../shared/images/general/hamburger.png')}
                />
              </TouchableOpacity>
            )}
            <View style={styles.centerRow}>
              <Text medium style={styles.groupNameText} numberOfLines={1}>
                {isEdit ? editGroupData.name : groupData.groupName}
              </Text>
              {isEdit && (
                <EditGroupIconContainer
                  canEdit={canEditDragGroup}
                  groupName={editGroupData.name}
                  groupKey={item.id}
                />
              )}
            </View>
            {isEdit ? (
              item.id !== '-1' && (
                <View style={[styles.right, styles.row, { marginRight: 20 }]}>
                  <TouchableOpacity onPress={showUngroupAlert}>
                    <Image
                      source={require('../../shared/images/general/delete.png')}
                    />
                  </TouchableOpacity>
                </View>
              )
            ) : (
              <TouchableOpacity
                onPress={switchGroupAlert}
                style={styles.slightRight}
                disabled={!groupData.isConnected}>
                <Image
                  source={groupOnOffIcon(groupData)}
                  style={styles.onOffIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
      {item &&
        item.childs &&
        item.childs.length > 0 &&
        (isEdit ? (
          <SortableScrollView
            data={item.childs}
            keyExtractor={key => key}
            renderItem={props => (
              <UnitContainer {...props} groupKey={item.id} />
            )}
            onDragEnd={({ data }) => {
              setOrder(item.id, data);
            }}
          />
        ) : (
          item.childs.map(({ thingName }) => (
            <UnitContainer
              item={thingName}
              groupKey={item.id.toString()}
              key={thingName}
            />
          ))
        ))}
    </View>
  );
};

GroupContainer.propTypes = {
  item: PropTypes.object,
  editGroupData: PropTypes.object,
  groupData: PropTypes.object,
  groupOptions: PropTypes.array,
  allUnits: PropTypes.object,
  unitItems: PropTypes.number,
  editParams: PropTypes.object,

  switchGroupAc: PropTypes.func,
  unitLoadingList: PropTypes.object,
  showDeleteAlert: PropTypes.func,
  toggleRowActive: PropTypes.func,
  dispatch: PropTypes.func,
  onActivateRow: PropTypes.func,
  onReleaseRow: PropTypes.func,
  editGroupName: PropTypes.func,
  setEditState: PropTypes.func,
  assignUnitGroup: PropTypes.func,
  drag: PropTypes.func,
  removeGroup: PropTypes.func,
  setOrder: PropTypes.func,
  isEdit: PropTypes.bool,
};

const mapStateToProps = (
  {
    editUnits: { items, isEdit },
    units: {
      groupInfo: { groups },
    },
  },
  { item }
) => {
  if (item && item.id !== undefined) {
    const groupData =
      (Object.values(groups) || []).find(
        ({ groupIndex }) => groupIndex === Number(item.id)
      ) || {};

    return {
      isEdit,
      editGroupData: isEdit
        ? items[item.id]
        : {
            name: groupData.groupName,
            id: groupData.groupIndex,
            childs: item.childs,
          },
      groupData,
    };
  }

  return {
    isEdit,
    editGroupData: {},
    groupData: {},
  };
};

const mapDispatchToProps = dispatch => ({
  removeGroup: index => {
    dispatch(editUnitsAction.removeGroup({ index }));
  },
  setOrder: (key, order) => {
    dispatch(editUnitsAction.setItemChildOrder(key, order));
  },
  switchGroupAc: (groupKey, switchType) => {
    dispatch(homepageIotShadow.switchGroupAC(groupKey, switchType));
  },
});

GroupContainer.defaultProps = {
  groupData: {},
  editGroupData: {},
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupContainer);
export { GROUP_TITLE_HEIGHT };
