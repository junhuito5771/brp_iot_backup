import React from 'react';
import PropTypes from 'prop-types';
import { connect, updateFirmware } from '@daikin-dama/redux-iot';

import { Loading, ModalService, useHandleResponse } from '@module/acson-ui';

const FirmwareLoading = ({
  loadingText,
  success,
  error,
  clearStatus,
  onCancel,
}) => {
  useHandleResponse({ success, showSuccess: true, error, clearStatus });

  const handleFirmwareUpgradeCancel = () => {
    ModalService.show({
      title: 'Cancel firmware upgrade',
      desc: 'Are you sure you want to cancel firmware upgrade?',
      buttons: [
        { title: 'Yes', onPress: onCancel, isRaised: true },
        { title: 'No' },
      ],
    });
  };

  return (
    <Loading visible={!!loadingText} onCancel={handleFirmwareUpgradeCancel}>
      {loadingText}
    </Loading>
  );
};

FirmwareLoading.propTypes = {
  loadingText: PropTypes.string,
  success: PropTypes.string,
  error: PropTypes.string,
  clearStatus: PropTypes.func,
  onCancel: PropTypes.func,
};

const mapStateToProps = ({
  updateFirmware: { loadingText, success, error },
}) => ({
  loadingText,
  success,
  error,
});

const mapDispatchToProps = dispatch => ({
  clearStatus: () => dispatch(updateFirmware.clearUpdateStatus()),
  onCancel: () => dispatch(updateFirmware.cancelFirmwareUpgrade()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FirmwareLoading);
