import { StyleSheet } from 'react-native';
import { heightPercentage, widthPercentage } from '@module/utility';
import { Colors } from '@module/acson-ui';

const GROUP_TITLE_HEIGHT = heightPercentage(8);

export { GROUP_TITLE_HEIGHT };

export default StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.offWhite,
  },
  container: {
    alignItems: 'center',
    marginBottom: heightPercentage(3),
  },
  loading: {
    paddingTop: 20,
  },
  largeText: {
    color: Colors.titleGrey,
    fontSize: 21,
  },
  text: {
    fontSize: 18,
  },
  lightGrey: {
    color: Colors.linkGrey,
  },
  marginTop8: {
    marginTop: heightPercentage(8),
  },
  marginTop1: {
    marginTop: heightPercentage(1),
  },
  marginTop4: {
    // marginTop: heightPercentage(4),
  },
  button: {
    width: widthPercentage(80),
    backgroundColor: Colors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 18,
    paddingLeft: 34,
    borderColor: Colors.titleGrey,
    borderWidth: 0.5,
    borderRadius: 5,
  },
  red: {
    color: Colors.red,
  },
  groupRow: {
    width: widthPercentage(82),
    height: GROUP_TITLE_HEIGHT,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    alignSelf: 'center',
  },
  groupRowInner: {
    flex: 1,
  },
  center: {
    flexDirection: 'row',
    alignItems: 'center',
    height: heightPercentage(5),
  },
  marginLeft: {
    marginLeft: 20,
  },
  marginRight20: {
    marginRight: 20,
  },
  onOffIcon: {
    width: 34,
    height: 34,
  },
  marginRight: {
    marginRight: 25,
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
  lastUpdated: {
    marginTop: heightPercentage(3),
    alignSelf: 'center',
  },
  deleteBackground: {
    backgroundColor: Colors.darkRed,
    width: 50,
    height: heightPercentage(5),
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  row: { flexDirection: 'row' },
  right: { position: 'absolute', right: 0 },
  slightRight: { position: 'absolute', right: 5 },
  drawerHeader: {
    width: '100%',
    backgroundColor: Colors.lightWhite,
    zIndex: 10,
    height: heightPercentage(5),
    justifyContent: 'center',
  },
  line: {
    borderLeftWidth: 2,
    borderLeftColor: Colors.lightCoolGrey,
    marginRight: 5,
  },
  modal: {
    padding: 30,
    alignSelf: 'center',
    width: widthPercentage(85),
    backgroundColor: Colors.offWhite,
    borderRadius: 12,
  },
  modalTitle: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '500',
  },
  buttonText: {
    fontSize: 14,
  },
  view: {
    marginVertical: heightPercentage(3),
  },
  addButtonContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: Colors.lightGrey,
    height: heightPercentage(6),
    width: widthPercentage(83),
    borderRadius: heightPercentage(6) / 2,
    flexDirection: 'row',
    paddingHorizontal: 25,
    alignSelf: 'center',
  },
  grey: {
    color: Colors.linkGrey,
  },
  groupNameText: {
    maxWidth: widthPercentage(50),
    marginRight: 8,
  },
  centerRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pencilIcon: {
    tintColor: Colors.darkGrey,
    height: 13,
    resizeMode: 'contain',
  },
  listContent: {
    flex: 1,
    paddingBottom: 10,
  },
});
