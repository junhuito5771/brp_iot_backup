import { View } from 'react-native';
import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { heightPercentage, generalInputPreValidator } from '@module/utility';
import Modal from 'react-native-modal';

import { TextInput, Button, Text } from '@module/acson-ui';
import styles from './styles';

const EditValueModal = ({
  isVisible,
  onSubmit,
  onBeforeSubmit,
  onCancel,
  customSubmitText,
  isLoading,
  value,
  title,
  inputPlaceHolder,
}) => {
  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    if (value) {
      setInputValue(value);
    }
  }, [value, isVisible]);

  const handleOnSubmit = () => {
    if (onBeforeSubmit) onBeforeSubmit(inputValue);
    if (onSubmit) onSubmit(inputValue);
  };

  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={onCancel}
      avoidKeyboard
      hideModalContentWhileAnimating
      backdropTransitionOutTiming={0}>
      <View style={styles.modal}>
        <Text style={styles.modalTitle}>{title}</Text>
        <TextInput
          placeholder={inputPlaceHolder}
          onChangeText={(text) => {
            setInputValue(generalInputPreValidator(text));
          }}
          value={inputValue}
          errorMsg={
            !inputValue.trim() ? `Please insert ${inputPlaceHolder}` : ''
          }
        />
        <Button
          raised
          onPress={handleOnSubmit}
          height={heightPercentage(5)}
          textStyle={styles.buttonText}
          isLoading={isLoading}
          disabled={!inputValue.trim()}
          >
          {customSubmitText || 'Ok'}
        </Button>
        <Button
          onPress={onCancel}
          textStyle={styles.buttonText}
          containerStyle={[styles.marginTop1, { height: heightPercentage(5) }]}
          disabled={isLoading}>
          Cancel
        </Button>
      </View>
    </Modal>
  );
};

EditValueModal.propTypes = {
  isVisible: PropTypes.bool,
  onSubmit: PropTypes.func,
  onBeforeSubmit: PropTypes.func,
  onCancel: PropTypes.func,
  customSubmitText: PropTypes.string,
  isLoading: PropTypes.bool,
  value: PropTypes.string,
  title: PropTypes.string,
  inputPlaceHolder: PropTypes.string,
};

export default EditValueModal;
