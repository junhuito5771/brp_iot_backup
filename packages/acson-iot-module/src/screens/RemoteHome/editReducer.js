import produce from 'immer';

export const initialState = {
  homepageList: [],
  unitOrder: {},
  groupOrder: [],
  groupsData: {},
  unitsGroup: {},
  units: {},
  editParams: {},
  groupOptions: [],
  deleteIndex: null,
  isEdited: false,
  isGroupUnitsEdited: false,
  isNameEdited: false,
  editLoading: false,
  addLoading: false,
};

export const reducer = (state, action) => {
  switch (action.type) {
    case 'SET_STATE':
      return { ...action.value };
    case 'SET_LIST':
      return { ...state, homepageList: action.value };
    case 'SET_EDITED':
      return { ...state, isEdited: action.value };
    case 'SET_NAME_EDITED':
      return { ...state, isNameEdited: action.value };
    case 'SET_EDIT_LOADING':
      return { ...state, editLoading: action.value };
    case 'SET_ADD_LOADING':
      return { ...state, addLoading: action.value };
    case 'SET_GROUP_OPTIONS':
      return { ...state, groupOptions: action.value };
    case 'SET_ALL_UNITS':
      return { ...state, units: action.value };
    case 'SET_EDIT_PARAMS':
      return { ...state, editParams: action.value };
    case 'SET_DELETE_INDEX':
      return { ...state, deleteIndex: action.value };
    case 'SET_GROUPS_DATA':
      return { ...state, groupsData: action.value };
    case 'SET_GROUP_UNITS_EDITED':
      return { ...state, isGroupUnitsEdited: action.value };
    case 'SET_UNITS_GROUP':
      return { ...state, unitsGroup: action.value };
    case 'SET_UNIT_ORDER':
      return { ...state, unitOrder: action.value };
    case 'UPDATE_UNIT_ORDER':
      return {
        ...state,
        unitOrder: { ...state.unitOrder, [action.groupIndex]: action.newOrder },
        isEdited: true,
      };
    case 'CLEAR_UNIT_ORDER':
      return {
        ...state,
        groupOrder: [],
        unitOrder: {},
      };
    case 'SET_GROUP_ORDER':
      return { ...state, groupOrder: action.groupNewOrder, isEdited: true };
    case 'SET_NEW_GROUP':
      return {
        ...state,
        editParams: {
          ...state.editParams,
          [action.lastGroupIndex + 1]: action.text,
        },
      };
    case 'SET_GROUP_TEXT':
      return {
        ...state,
        editParams: { ...state.editParams, [action.groupIndex]: action.text },
        groupOptions: produce(state.groupOptions, draft => {
          const targetItem = draft.find(
            i => i.groupIndex === action.groupIndex
          );
          targetItem.groupName = action.text;
        }),
      };
    case 'SET_UNIT_TEXT':
      return {
        ...state,
        editParams: {
          ...state.editParams,
          [action.target]: {
            ...state.editParams[action.target],
            name: action.text,
          },
        },
      };
    case 'SET_AC_ICON':
      return {
        ...state,
        editParams: {
          ...state.editParams,
          [action.target]: {
            ...state.editParams[action.target],
            logo: action.icon,
          },
        },
      };
    default:
      return state;
  }
};
