import { View, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import PropTypes from 'prop-types';
import { connect, editUnits } from '@daikin-dama/redux-iot';

import { Text } from '@module/acson-ui';
import GroupModal from './groupModal';
import styles from './styles';

const addGroupMapDispatchToProps = dispatch => ({
  onSubmit: name => dispatch(editUnits.addEditItem(name)),
});

const AddGroupModal = connect(null, addGroupMapDispatchToProps)(GroupModal);

const AddGroupContainer = ({ onBeforeSubmit }) => {
  const [showModal, setShowModal] = React.useState(false);

  const handleOnCancel = () => {
    setShowModal(false);
  };

  return (
    <View style={styles.view}>
      <TouchableOpacity onPress={() => setShowModal(true)}>
        <View style={styles.addButtonContainer}>
          <Text medium style={styles.grey}>
            Add Group
          </Text>
          <Image source={require('../../shared/images/general/plusGrey.png')} />
        </View>
      </TouchableOpacity>
      <AddGroupModal
        isVisible={showModal}
        onCancel={handleOnCancel}
        onBeforeSubmit={onBeforeSubmit}
        customSubmitText="Add"
        title="New Group"
        inputPlaceHolder="Group Name"
      />
    </View>
  );
};

AddGroupContainer.propTypes = {
  onBeforeSubmit: PropTypes.func,
};

export default AddGroupContainer;
