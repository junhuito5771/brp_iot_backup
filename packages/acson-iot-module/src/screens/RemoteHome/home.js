import React, { useRef, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Alert,
  TouchableOpacity,
  Image,
  RefreshControl,
  Platform,
  // ActivityIndicator,
  AppState,
  FlatList,
  UIManager,
  findNodeHandle,
} from 'react-native';
import Config from 'react-native-config';
import SplashScreen from 'react-native-splash-screen';
import {
  heightPercentage,
  widthPercentage,
  LocationHelper,
  verticalScale,
  NavigationService,
} from '@module/utility';
import { useNetInfo } from '@react-native-community/netinfo';
import { useFocusEffect, useIsFocused } from '@react-navigation/native';
import {
  connect,
  units as unitsData,
  homepageIotShadow,
  remote,
  selector,
  editUnits,
  controlUsageLimit,
  tutorialFlag as tutorialState,
} from '@daikin-dama/redux-iot';
// eslint-disable-next-line import/no-unresolved
import moment from 'moment';
import {
  PERMISSIONS,
  RESULTS,
  requestMultiple,
  checkMultiple,
  openSettings,
} from 'react-native-permissions';

import {
  Colors,
  Text,
  Button,
  SortableFlatList,
  useHandleResponse,
  UNIT_CARD_HEIGHT,
  ModalService,
  TutorialOverlay,
} from '@module/acson-ui';

import TapIcon from '../../shared/images/tutorial/TapIcon';
import SwipeIcon from '../../shared/images/tutorial/SwipeIcon';
import DragIcon from '../../shared/images/tutorial/DragIcon';
import { QR_SCANNER_SCREEN } from '../../constants/routeNames';

import GroupContainer, { GROUP_TITLE_HEIGHT } from './groupContainer';
import AddGroupContainer from './addGroupContainer';
import FirmwareLoading from './firmwareLoading';
import EmptyUnitContent from './emptyUnitContent';

LocationHelper.init(Config.PROVIDER);

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    backgroundColor: Colors.lightWhite,
  },
  listContent: {
    paddingBottom: 50,
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: heightPercentage(3),
  },
  loading: {
    paddingTop: 20,
  },
  largeText: {
    color: Colors.titleGrey,
    fontSize: 21,
  },
  text: {
    fontSize: 18,
  },
  lightGrey: {
    color: Colors.linkGrey,
  },
  marginTop8: {
    marginTop: heightPercentage(8),
  },
  marginTop1: {
    marginTop: heightPercentage(1),
  },
  marginTop4: {
    marginTop: heightPercentage(4),
  },
  button: {
    width: widthPercentage(80),
    backgroundColor: Colors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 18,
    paddingLeft: 34,
    borderColor: Colors.titleGrey,
    borderWidth: 0.5,
    borderRadius: 5,
  },
  red: {
    color: Colors.red,
  },
  groupRow: {
    width: widthPercentage(82),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: heightPercentage(2),
  },
  center: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  marginLeft: {
    marginLeft: 20,
  },
  onOffIcon: {
    width: 34,
    height: 34,
  },
  marginRight: {
    marginRight: 25,
  },
  hitSlop: {
    top: 10,
    bottom: 10,
    left: 10,
    right: 10,
  },
  lastUpdated: {
    marginTop: heightPercentage(3),
    alignSelf: 'center',
  },
  headerRight: {
    marginRight: 25,
    flexDirection: 'row',
  },
  editUnit: {
    marginRight: 15,
    alignItems: 'center',
  },
  tutorial: {
    marginRight: 10,
    alignItems: 'center',
  },
  saveButton: {
    marginHorizontal: 25,
    marginBottom: 10,
  },
});

const checkCameraPermission = async () => {
  const checkPermissions = Platform.select({
    ios: [PERMISSIONS.IOS.CAMERA, PERMISSIONS.IOS.LOCATION_WHEN_IN_USE],
    android: [
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
    ],
  });

  const checkPermissionStatus = await checkMultiple(checkPermissions);

  const cameraReqNeeded =
    checkPermissionStatus[checkPermissions[0]] !== RESULTS.GRANTED;
  const locationReqNeeded =
    checkPermissionStatus[checkPermissions[1]] !== RESULTS.GRANTED;

  const requestPermissions = [];

  if (cameraReqNeeded) {
    requestPermissions.push(checkPermissions[0]);
  }

  if (locationReqNeeded) {
    requestPermissions.push(checkPermissions[1]);
  }

  const permissionStatus =
    requestPermissions.length > 0
      ? await requestMultiple(requestPermissions)
      : undefined;

  const isCameraGranted =
    !cameraReqNeeded ||
    (permissionStatus &&
      permissionStatus[checkPermissions[0]] === RESULTS.GRANTED);
  const isLocationGranted =
    !locationReqNeeded ||
    (permissionStatus &&
      permissionStatus[checkPermissions[1]] === RESULTS.GRANTED);

  Platform.select({
    ios: () => {
      const privacyList = [];
      if (!isCameraGranted) {
        privacyList.push('Camera');
      }

      if (!isLocationGranted) {
        privacyList.push('Location');
      }

      if (privacyList && privacyList.length > 0) {
        Alert.alert(
          `No ${privacyList.join('/')} Access`,
          "To scan QR code and pair units, change permission in your device's app settings to allow Acson app access to Camera.",
          [
            { text: 'Cancel', style: 'cancel' },
            { text: 'Settings', onPress: openSettings },
          ]
        );
      } else {
        NavigationService.navigate(QR_SCANNER_SCREEN);
      }
    },
    android: async () => {
      if (isCameraGranted && isLocationGranted) {
        const isLocationOn = await LocationHelper.enableLocationIfNeeded();

        if (isLocationOn) NavigationService.navigate(QR_SCANNER_SCREEN);
      }
    },
  })();
};

const RowItem = ({ item, drag, isActive }) => {
  const groupProps =
    item && item.group
      ? { item: { id: item.group.groupIndex, childs: item.units } }
      : { item, drag, isActive };

  return <GroupContainer {...groupProps} />;
};

RowItem.propTypes = {
  item: PropTypes.object,
  isActive: PropTypes.bool,
  drag: PropTypes.func,
};

const Home = ({
  getAllUnits,
  userEmail,
  isFetchUnitsLoading,
  lastUpdated,
  clearUnitsStatus,
  fetchUnitsError,
  clearEditStatus,

  editSuccess,
  editError,
  toastMsg,
  clearToastMsg,
  initEditUnits,
  itemIds,
  homepageList,
  setItemOrder,
  isEdit,
  navigation,
  submitEditPrams,
  isEditLoading,
  canSubmitEdit,
  isFocused,
  checkExpiryTime,

  tutorialFlag,
  setIsRefreshDisplayed,
  setIsAddNewGroupDisplayed,
  setIsRearrangeGroupDisplayed,
  setIsRearrangeUnitDisplayed,
  setIsEditNameDisplayed,
  setIsDeleteGroupDisplayed,
  // clearStatusHeatLoadCalculator,
}) => {
  const isScreenFocused = useIsFocused();
  const [isBlurBefore, setIsBlueBefore] = React.useState(false);
  const groupContainerRef = useRef();
  const listRef = useRef();
  const listProps = useRef({
    flatlistHeight: -1,
    topOffset: -1,
    scrollOffset: 0,
  });
  const [groupContainerPosition, setGroupContainerPosition] = React.useState(0);
  const needScrollDown = useRef(false);
  const layoutOffsetMap = useRef(new Map());

  const handleAppStateChange = nextAppState => {
    const isBackground = nextAppState.match(/inactive|background/);

    if (isBackground) {
      setIsBlueBefore(isBackground);
    }
  };

  useFocusEffect(
    useCallback(() => {
      const onClickEdit = isEdit
        ? () => clearEditStatus()
        : () => initEditUnits(homepageList);

      const onShowTutorialOverlay = isEdit
        ? () => {
            setIsAddNewGroupDisplayed(false);
            setIsEditNameDisplayed(false);
            setIsRearrangeGroupDisplayed(false);
            setIsRearrangeUnitDisplayed(false);
            setIsDeleteGroupDisplayed(false);
          }
        : () => setIsRefreshDisplayed(false);
      NavigationService.setOptions(
        navigation,
        {
          headerRight: () => (
            <View style={styles.headerRight}>
              {onShowTutorialOverlay && (
                <View style={styles.tutorial}>
                  <TouchableOpacity
                    hitSlop={styles.hitSlop}
                    onPress={onShowTutorialOverlay}>
                    <Image
                      source={require('../../shared/images/general/help.png')}
                      style={{ tintColor: Colors.linkGrey }}
                    />
                  </TouchableOpacity>
                </View>
              )}
              {onClickEdit && (
                <View style={styles.editUnit}>
                  <TouchableOpacity
                    hitSlop={styles.hitSlop}
                    onPress={onClickEdit}>
                    <Image
                      source={require('../../shared/images/home/editUnit.png')}
                      style={{
                        tintColor: isEdit ? Colors.darkGrey : Colors.linkGrey,
                      }}
                    />
                  </TouchableOpacity>
                </View>
              )}
              <View>
                <TouchableOpacity
                  hitSlop={styles.hitSlop}
                  onPress={() => checkCameraPermission()}>
                  <Image
                    source={require('../../shared/images/home/qrScanSmallBlue.png')}
                    style={{ tintColor: Colors.linkGrey }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          ),
        },
        false
      );
    }, [navigation, isEdit, homepageList])
  );
  // check the internet connection error & fetchUnits error
  const netInfo = useNetInfo();

  // Tutorial Steps
  const stepsGroup = React.useMemo(() => {
    /* eslint-disable no-mixed-operators */

    const offset1 = (groupContainerPosition || 0) + verticalScale(25);
    const offset2 = (groupContainerPosition || 0) - verticalScale(40);

    return {
      refresh: {
        position: { x: widthPercentage(25), y: heightPercentage(12) },
        icon: <DragIcon />,
        description: 'Pull up\nto Refresh',
        onClose: () => setIsRefreshDisplayed(true),
      },
      addGroup: {
        position: { x: widthPercentage(25), y: offset2 },
        icon: <TapIcon />,
        description: 'Tap here to add\nnew group',
        onClose: () => setIsAddNewGroupDisplayed(true),
      },
      arrangeUnit: {
        /* eslint-disable no-mixed-operators */
        position: { x: widthPercentage(10), y: offset1 + UNIT_CARD_HEIGHT / 2 },
        icon: <SwipeIcon />,
        description: 'Long press + up down to\nrearrange unit',
        onClose: () => setIsRearrangeUnitDisplayed(true),
      },
      arrangeGroup: {
        position: { x: widthPercentage(2), y: offset1 },
        icon: <SwipeIcon />,
        description: 'Long press + up down to\nrearrange group',
        onClose: () => setIsRearrangeGroupDisplayed(true),
      },
      editName: {
        position: { x: widthPercentage(20), y: offset1 },
        icon: <TapIcon />,
        description: 'Tap on edit icon\nto edit name',
        onClose: () => setIsEditNameDisplayed(true),
      },
      deleteGroup: {
        position: { x: widthPercentage(40), y: offset1 },
        icon: <TapIcon />,
        iconRightAlign: true,
        description: 'Tap on delete icon\nto delete group',
        onClose: () => setIsDeleteGroupDisplayed(true),
      },
    };
  }, [groupContainerPosition, itemIds]);

  useHandleResponse({
    errorTitle:
      !netInfo.isInternetReachable && fetchUnitsError
        ? 'No Connection Found'
        : 'Error',
    error:
      !netInfo.isInternetReachable && fetchUnitsError
        ? 'Please check your network connection'
        : fetchUnitsError,
    showError: !isBlurBefore,
    onError: () => {
      clearUnitsStatus();

      if (isBlurBefore && !isFetchUnitsLoading) {
        setIsBlueBefore(false);
      }
    },
  });

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);

    return () => {
      AppState.removeEventListener('change', handleAppStateChange);
    };
  }, []);

  useEffect(() => {
    SplashScreen.hide();
    // clearStatusHeatLoadCalculator();
    return () => {
      clearUnitsStatus();
    };
  }, []);

  const reinitialize = () => {
    checkExpiryTime();
    getAllUnits(userEmail);
  };

  useEffect(() => {
    if (navigation.isFocused()) {
      reinitialize();
    }

    if (isEdit) {
      clearEditStatus();
    }
  }, [navigation.isFocused()]);

  const handleSubmitEditUnits = () => {
    if (!itemIds || itemIds.some(({ childs }) => childs.length === 0)) {
      ModalService.show({
        title: 'Group cannot be empty',
        desc: 'Please move at least one unit into the empty group.',
        buttons: [
          {
            title: 'OK',
            isRaised: true,
          },
        ],
      });
    } else {
      submitEditPrams();
    }
  };

  const onRefresh = () => {
    reinitialize();
  };

  useHandleResponse({
    info: toastMsg,
    onInfo: clearToastMsg,
  });

  useHandleResponse({
    success: editSuccess,
    showSuccess: isFocused,
    onSuccess: onRefresh,
    error: editError,
    showError: isFocused,
    clearStatus: () => {
      setTimeout(() => {
        clearEditStatus();
      }, 500);
    },
  });

  useEffect(
    React.useCallback(() => {
      if (groupContainerRef.current) {
        UIManager.measure(
          findNodeHandle(groupContainerRef.current),
          (x, y, width, height, pageX, pageY) => {
            setGroupContainerPosition(pageY);
          }
        );
      }
    }, [groupContainerRef])
  );

  // Show differnet tutorial overlay based on flags
  const showTutorialOverlay = React.useCallback(() => {
    let currentStep = null;

    if (!tutorialFlag.isDeleteGroupDisplayed && isEdit) {
      currentStep = stepsGroup.deleteGroup;
    }
    if (!tutorialFlag.isEditNameDisplayed && isEdit) {
      currentStep = stepsGroup.editName;
    }
    if (!tutorialFlag.isRearrangeGroupDisplayed && isEdit) {
      currentStep = stepsGroup.arrangeGroup;
    }
    if (!tutorialFlag.isRearrangeUnitDisplayed && isEdit) {
      currentStep = stepsGroup.arrangeUnit;
    }
    if (!tutorialFlag.isAddNewGroupDisplayed && isEdit) {
      currentStep = stepsGroup.addGroup;
    }
    if (!tutorialFlag.isRefreshDisplayed) {
      currentStep = stepsGroup.refresh;
    }

    return (
      !!currentStep &&
      isScreenFocused && (
        <TutorialOverlay
          onClose={currentStep.onClose}
          visible={!!currentStep}
          currentStep={currentStep}
        />
      )
    );
  }, [tutorialFlag, stepsGroup, isScreenFocused]);

  const ListHeader = () => (
    <>
      {Platform.OS === 'android' && (
        <Text style={styles.lastUpdated}>{`-- Last updated ${moment(
          lastUpdated
        ).format('HH:mm')} --`}</Text>
      )}
      {isEdit && (
        <>
          <AddGroupContainer
            onBeforeSubmit={() => {
              needScrollDown.current = true;
            }}
          />
          <View ref={groupContainerRef} collapsable={false} />
        </>
      )}
    </>
  );

  return (
    <>
      {showTutorialOverlay()}
      {/* {isFetchUnitsLoading && homepageList.length < 1 && (
        <View style={styles.container}>
          <ActivityIndicator
            size="small"
            color={Colors.grey}
            style={styles.loading}
          />
        </View>
      )} */}
      <FirmwareLoading />
      {isEdit ? (
        <SortableFlatList
          style={styles.flex}
          ref={listRef}
          contentContainerStyle={styles.listContent}
          onScroll={e => {
            listProps.current.scrollOffset = e.nativeEvent.contentOffset.y;
          }}
          keyExtractor={({ id }) => id}
          data={itemIds}
          ListHeaderComponent={<ListHeader />}
          renderItem={RowItem}
          getItemLayout={(item, index) => {
            const { childs } = item[index];
            const childRowHeight = UNIT_CARD_HEIGHT * childs.length;
            const rowHeight =
              childs && childs.length > 0
                ? childRowHeight + GROUP_TITLE_HEIGHT
                : GROUP_TITLE_HEIGHT;

            const prevOffset =
              layoutOffsetMap.current.get(index - 1) !== undefined
                ? layoutOffsetMap.current.get(index - 1)
                : 0;
            layoutOffsetMap.current.set(index, prevOffset + rowHeight);

            const offset =
              layoutOffsetMap.current.get(index - 1) !== undefined
                ? layoutOffsetMap.current.get(index - 1)
                : 0;

            return {
              length: rowHeight,
              offset,
              index,
            };
          }}
          onContentSizeChange={() => {
            if (needScrollDown.current) {
              needScrollDown.current = false;

              listRef.current.scrollToIndex({
                animated: true,
                index: itemIds.length - 1,
                viewOffset: -50,
                viewPosition: 0.5,
              });
            }
          }}
          onDragEnd={({ data }) => {
            setItemOrder(data);
          }}
        />
      ) : (
        <FlatList
          ListEmptyComponent={
            <EmptyUnitContent checkCameraPermission={checkCameraPermission} />
          }
          style={styles.flex}
          ref={listRef}
          contentContainerStyle={styles.listContent}
          refreshControl={
            <RefreshControl
              refreshing={isFetchUnitsLoading}
              onRefresh={onRefresh}
              size={15}
              title={`Last updated ${moment(lastUpdated).format('HH:mm')}`}
              titleColor={Colors.titleGrey}
            />
          }
          onScroll={e => {
            listProps.current.scrollOffset = e.nativeEvent.contentOffset.y;
          }}
          keyExtractor={({ group }) => group && group.groupName}
          data={homepageList}
          ListHeaderComponent={<ListHeader />}
          renderItem={RowItem}
        />
      )}
      {isEdit && (
        <View style={styles.saveButton}>
          <Button
            raised
            disabledRaised
            onPress={handleSubmitEditUnits}
            isLoading={isEditLoading}
            disabled={!canSubmitEdit}>
            Save
          </Button>
        </View>
      )}
    </>
  );
};

Home.propTypes = {
  getAllUnits: PropTypes.func,
  clearUnitsStatus: PropTypes.func,
  clearEditStatus: PropTypes.func,

  lastUpdated: PropTypes.number,
  userEmail: PropTypes.string,
  isFetchUnitsLoading: PropTypes.bool,
  fetchUnitsError: PropTypes.string,

  homepageList: PropTypes.array,
  editSuccess: PropTypes.string,
  editError: PropTypes.string,

  toastMsg: PropTypes.string,
  clearToastMsg: PropTypes.func,
  itemIds: PropTypes.array,
  setItemOrder: PropTypes.func,
  initEditUnits: PropTypes.func,
  isEdit: PropTypes.bool,
  submitEditPrams: PropTypes.func,
  isEditLoading: PropTypes.bool,
  canSubmitEdit: PropTypes.bool,
  navigation: PropTypes.object,
  isFocused: PropTypes.bool,
  checkExpiryTime: PropTypes.func,

  tutorialFlag: PropTypes.shape({
    isRefreshDisplayed: PropTypes.bool,
    isAddNewGroupDisplayed: PropTypes.bool,
    isRearrangeGroupDisplayed: PropTypes.bool,
    isRearrangeUnitDisplayed: PropTypes.bool,
    isEditNameDisplayed: PropTypes.bool,
    isDeleteGroupDisplayed: PropTypes.bool,
  }),
  setIsRefreshDisplayed: PropTypes.func,
  setIsAddNewGroupDisplayed: PropTypes.func,
  setIsRearrangeGroupDisplayed: PropTypes.func,
  setIsRearrangeUnitDisplayed: PropTypes.func,
  setIsEditNameDisplayed: PropTypes.func,
  setIsDeleteGroupDisplayed: PropTypes.func,
  // clearStatusHeatLoadCalculator: PropTypes.func,
};

const mapStateToProps = state => ({
  unitCount: state.units.unitCount,
  userEmail: state.user.profile.email,
  isFetchUnitsLoading: state.units.isLoading,
  success: state.user.success,
  lastUpdated: state.units.lastUpdated,
  fetchUnitsError: state.units.error,
  homepageList: selector.getHomepageList(state),
  editSuccess: state.editUnits.success,
  editError: state.editUnits.error,
  unitLoadingList: state.homepageIotShadow.unitLoadingList,
  toastMsg: state.homepageIotShadow.toastMsg,

  items: state.editUnits.items,
  itemIds: state.editUnits.itemIds,
  isEdit: state.editUnits.isEdit,
  isEditLoading: state.editUnits.isLoading,
  canSubmitEdit: state.editUnits.canSave,

  tutorialFlag: state.tutorialFlag,
});

const mapDispatchToProps = dispatch => ({
  clearToastMsg: () => dispatch(homepageIotShadow.setToastMsg('')),
  getAllUnits: (email, mqttConfig) => {
    dispatch(unitsData.getAllUnits({ email, mqttConfig }));
  },
  switchAc: (targetGroupKey, thingName, switchType) => {
    dispatch(homepageIotShadow.switchAc(targetGroupKey, thingName, switchType));
  },
  switchGroupAc: (targetGroupKey, targetUnits, switchType) => {
    dispatch(
      homepageIotShadow.switchGroupAC(targetGroupKey, targetUnits, switchType)
    );
  },
  clearUnitsStatus: () => {
    dispatch(unitsData.clearUnitsStatus());
  },
  setRemoteUnit: thingName => {
    dispatch(remote.setRemoteUnit({ thingName }));
  },
  clearEditStatus: () => {
    dispatch(editUnits.clearEditStatus());
  },
  initEditUnits: homepageList =>
    dispatch(editUnits.initEditUnit({ homepageList })),
  setItemOrder: orders => {
    dispatch(editUnits.setItemOrder(orders));
  },
  submitEditPrams: () => {
    dispatch(editUnits.submitEditParams());
  },
  setIsRefreshDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsRefreshDisplayed(isDisplayed));
  },
  setIsAddNewGroupDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsAddNewGroupDisplayed(isDisplayed));
  },
  setIsRearrangeGroupDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsRearrangeGroupDisplayed(isDisplayed));
  },
  setIsRearrangeUnitDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsRearrangeUnitDisplayed(isDisplayed));
  },
  setIsEditNameDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsEditNameDisplayed(isDisplayed));
  },
  setIsDeleteGroupDisplayed: isDisplayed => {
    dispatch(tutorialState.setIsDeleteGroupDisplayed(isDisplayed));
  },
  // clearStatusHeatLoadCalculator: () => {
  //   dispatch(heatLoadCalculator.clearStatus());
  // },
  checkExpiryTime: () => {
    dispatch(controlUsageLimit.checkExpiryTime());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
