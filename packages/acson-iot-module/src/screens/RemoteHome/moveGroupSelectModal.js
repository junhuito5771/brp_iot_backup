import React from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { connect, selector } from '@daikin-dama/redux-iot';
import { heightPercentage, widthPercentage } from '@module/utility';
import Modal from 'react-native-modal';

import { Button, Text, Colors } from '@module/acson-ui';

const styles = StyleSheet.create({
  modal: {
    height: heightPercentage(53),
    padding: 30,
    alignSelf: 'center',
    width: widthPercentage(85),
    backgroundColor: Colors.offWhite,
    borderRadius: 12,
  },
  modalTitle: {
    alignSelf: 'center',
    fontSize: 18,
    fontWeight: '500',
    marginBottom: heightPercentage(1),
  },
  buttonText: {
    fontSize: 14,
    maxWidth: widthPercentage(50),
  },
  marginTop1: {
    marginTop: heightPercentage(1),
  },
});

function MoveGroupSelectModal({
  isVisible,
  onCancel,
  onSelect,
  groupSelectionList,
  isLoading,
}) {
  return (
    <Modal
      isVisible={isVisible}
      onBackdropPress={onCancel}
      avoidKeyboard
      hideModalContentWhileAnimating
      backdropTransitionOutTiming={0}>
      <View style={styles.modal}>
        <Text style={styles.modalTitle}>Move to...</Text>
        <ScrollView>
          {groupSelectionList.map(({ label, value }) => (
            <Button
              key={label}
              raised
              onPress={() => onSelect(value)}
              height={heightPercentage(5)}
              textStyle={styles.buttonText}
              containerStyle={styles.marginTop1}
              disabled={isLoading}>
              {label}
            </Button>
          ))}
        </ScrollView>
        <Button
          onPress={onCancel}
          height={heightPercentage(5)}
          textStyle={styles.buttonText}
          containerStyle={styles.marginTop1}
          disabled={isLoading}>
          Cancel
        </Button>
      </View>
    </Modal>
  );
}

const mapStateToProps = (state, props) => ({
  groupSelectionList: selector.getGroupSelectionList(state, props),
});

MoveGroupSelectModal.propTypes = {
  isVisible: PropTypes.bool,
  onCancel: PropTypes.func,
  onSelect: PropTypes.func,
  groupSelectionList: PropTypes.array,
  isLoading: PropTypes.bool,
};

MoveGroupSelectModal.defaultProps = {
  groupSelectionList: [],
};

export default connect(mapStateToProps)(MoveGroupSelectModal);
