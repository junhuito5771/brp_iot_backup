import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';

import { Text, Colors } from '@module/acson-ui';
import { heightPercentage, widthPercentage } from '@module/utility';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: heightPercentage(3),
  },
  firstLineText: {
    color: Colors.titleGrey,
    fontSize: 21,
    marginTop: heightPercentage(8),
  },
  secondLineText: {
    fontSize: 18,
    color: Colors.linkGrey,
    marginTop: heightPercentage(1),
  },
  button: {
    width: widthPercentage(80),
    backgroundColor: Colors.white,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 18,
    paddingLeft: 34,
    borderColor: Colors.titleGrey,
    borderWidth: 0.5,
    borderRadius: 5,
    marginTop: heightPercentage(4),
  },
  buttonText: {
    fontSize: 18,
    color: Colors.red,
  },
});

export default function EmptyUnitContent({ checkCameraPermission }) {
  return (
    <View style={styles.container}>
      <Text style={styles.firstLineText}>No paired units yet</Text>
      <Text style={styles.secondLineText}>Add units now</Text>
      <TouchableOpacity onPress={checkCameraPermission}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>Add units</Text>
          <Image source={require('../../shared/images/general/plus.png')} />
        </View>
      </TouchableOpacity>
    </View>
  );
}

EmptyUnitContent.propTypes = {
  checkCameraPermission: PropTypes.func,
};
