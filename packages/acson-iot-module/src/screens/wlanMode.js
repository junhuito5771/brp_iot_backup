import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  RefreshControl,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, wlanMode } from '@daikin-dama/redux-iot';
import { ConnectionType, NavigationService } from '@module/utility';

import {
  Loading,
  useHandleResponse,
  ModalService,
  CollapsibleContainer,
} from '@module/acson-ui';
import { OTHER_MODE_REMOTE_SCREEN } from '../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    paddingHorizontal: 25,
    paddingTop: 25,
    paddingBottom: 50,
  },
});

const WlanModeScreen = ({
  units,
  pairWLANUnit,
  clearStatus,
  isLoading,
  isPairLoading,
  pairSuccess,
  pairError,
  connectedUnitName,
  discoverWLANUnits,
}) => {
  const handleNavigateToWlanMode = () => {
    NavigationService.navigate(OTHER_MODE_REMOTE_SCREEN, {
      unit: { ACName: connectedUnitName },
      mode: ConnectionType.WLAN_MODE,
    });
  };

  const handleOnPressItem = unit => {
    if (unit.disallowed) {
      ModalService.show({
        title: 'Unauthorized unit',
        desc:
          'You do not have permission to access this unit. Please log in to the account paired with this unit first.',
      });
    } else {
      pairWLANUnit(unit);
    }
  };

  useHandleResponse({
    success: pairSuccess,
    error: pairError,
    onSuccess: handleNavigateToWlanMode,
    clearStatus,
  });

  return (
    <SafeAreaView style={styles.flex}>
      <Loading visible={isPairLoading}>Connecting to unit</Loading>
      <ScrollView
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={isLoading}
            onRefresh={discoverWLANUnits}
          />
        }>
        <CollapsibleContainer data={units} onPressItem={handleOnPressItem} />
      </ScrollView>
    </SafeAreaView>
  );
};

WlanModeScreen.propTypes = {
  units: PropTypes.array,
  connectedUnitName: PropTypes.string,
  clearStatus: PropTypes.func,
  isLoading: PropTypes.bool,
  isPairLoading: PropTypes.bool,
  pairSuccess: PropTypes.string,
  pairError: PropTypes.string,
  pairWLANUnit: PropTypes.func,
  discoverWLANUnits: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  discoverWLANUnits: () => {
    dispatch(
      wlanMode.discoverUnits({
        setBroadcast: Platform.select({
          ios: true,
          android: false,
        }),
      })
    );
  },
  pairWLANUnit: unitProps => dispatch(wlanMode.pairUnit(unitProps)),
  clearStatus: () => dispatch(wlanMode.clearStatus()),
});

const mapStateToProps = state => ({
  units: state.wlanMode.units,
  isLoading: state.wlanMode.isLoading,
  lastUpdated: state.wlanMode.lastUpdated,
  isPairLoading: state.wlanMode.isPairLoading,
  pairSuccess: state.wlanMode.pairSuccess,
  pairError: state.wlanMode.pairError,
  connectedUnitName: state.units.allUnits[state.remote.thingName]
    ? state.units.allUnits[state.remote.thingName].ACName
    : '',
});

export default connect(mapStateToProps, mapDispatchToProps)(WlanModeScreen);
