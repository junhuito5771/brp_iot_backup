import React from 'react';
import PropTypes from 'prop-types';
import { ScrollView, View, StyleSheet, ActivityIndicator } from 'react-native';
import moment from 'moment';
import { heightPercentage, widthPercentage, ScaleText } from '@module/utility';
import { connect, remote } from '@daikin-dama/redux-iot';

import { Text, BackButton, Colors } from '@module/acson-ui';

import { getErrorCodeDetail } from '../../constants/errorCodes';

const styles = StyleSheet.create({
  rowContainer: {
    width: '100%',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingHorizontal: widthPercentage(7),
    paddingVertical: heightPercentage(1.5),
    height: 'auto',
  },
  rightColumn: {
    position: 'absolute',
    right: 0,
    flexDirection: 'row',
  },
  textMargin: {
    marginVertical: 4,
  },
  textLabel: {
    fontWeight: 'bold',
    fontSize: ScaleText(16),
    color: Colors.black,
  },
  textTimestamp: {
    fontWeight: '100',
    fontSize: ScaleText(10),
    color: Colors.grey,
    paddingHorizontal: 10,
  },
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  greyText: {
    color: Colors.lightGrey,
  },
  flexColumn: {
    flexDirection: 'column',
  },
  upperRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  noHistory: {
    height: heightPercentage(70),
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const RowContent = ({ dateText, timeText, labelText, descriptionText }) => (
  <View style={styles.flexColumn}>
    <View style={styles.upperRow}>
      <Text style={(styles.textMargin, styles.textLabel)}>{labelText}</Text>
      <View style={styles.rightColumn}>
        <Text style={styles.textTimestamp}>{dateText}</Text>
        <Text style={styles.textTimestamp}>{timeText}</Text>
      </View>
    </View>

    <View>
      <Text small style={styles.textMargin}>
        {descriptionText}
      </Text>
    </View>
  </View>
);

const ErrorHistoryList = errorData =>
  errorData.length > 0 ? (
    errorData.map((obj, index) => {
      const errorList = getErrorCodeDetail(obj.code);

      if (!errorList) {
        return null;
      }

      const { label, description } = errorList;

      const key = `${obj.updatedOn}_${label}_${index}`;

      return (
        // eslint-disable-next-line react/no-array-index-key
        <View key={key} style={styles.rowContainer}>
          <RowContent
            dateText={moment(obj.updatedOn).format('DD/MM/YYYY')}
            labelText={`Error ${label}`}
            descriptionText={description}
            timeText={moment(obj.updatedOn).format('HH:mm')}
          />
        </View>
      );
    })
  ) : (
    <>
      <View style={styles.noHistory}>
        <Text small style={styles.greyText}>
          No error history available
        </Text>
      </View>
    </>
  );

const ErrorHistory = ({
  onPageLoad,
  errorHistory,
  onClearHistory,
  isLoading,
  route
}) => {

  React.useEffect(() => {
    const { thingName } = route.params;
    onPageLoad(thingName);
    return () => onClearHistory();
  }, []);

  if (isLoading) {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="small" color={Colors.grey} />
      </View>
    );
  }

  return (
    <View style={styles.flex}>
      {/* Fix scrollbar position issue on iOS 13 */}
      <ScrollView scrollIndicatorInsets={{ right: 1 }}>
        {ErrorHistoryList(errorHistory)}
      </ScrollView>
    </View>
  );
};


RowContent.propTypes = {
  labelText: PropTypes.string,
  dateText: PropTypes.string,
  descriptionText: PropTypes.string,
  timeText: PropTypes.string,
};

ErrorHistory.propTypes = {
  onPageLoad: PropTypes.func,
  onClearHistory: PropTypes.func,
  errorHistory: PropTypes.array,
  route: PropTypes.object,
  isLoading: PropTypes.bool,
};

const mapStateToProps = ({ remote: { errorHistory, isLoading } }) => ({
  errorHistory,
  isLoading,
});

const mapDispatchToProps = dispatch => ({
  onPageLoad: value => dispatch(remote.fetchErrorHistory(value)),
  onClearHistory: () => dispatch(remote.clearErrorHistory()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ErrorHistory);
