import React, { useState, useEffect } from 'react';
import {
  TouchableOpacity,
  Image,
  StyleSheet,
  View,
  Platform,
  ActivityIndicator,
  Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import {
  connect,
  remote,
  units,
  editUnits,
  updateFirmware,
  unitUsers,
} from '@daikin-dama/redux-iot';
import {
  heightPercentage,
  widthPercentage,
  ConnectionType,
  ScaleText,
  SetIndoorTemperature,
  viewportWidth,
  iphoneXS,
  NavigationService,
} from '@module/utility';

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
import { useNetInfo } from '@react-native-community/netinfo';

import {
  useHandleResponse,
  Button,
  Text,
  ModalService,
  Colors,
  BackButton,
  Loading,
} from '@module/acson-ui';

import TempSlider from '../../components/UnitControl/slider';
import BottomSheet from '../../components/BottomSheet';
import { getErrorCodeDetail } from '../../constants/errorCodes';
import { MODE_ENUM, getTempValue } from '../../constants/remoteControl';
import ModeButton from '../../components/ModeButton';

import {
  UNIT_USERS_SCREEN,
  WIFI_CONFIG_SCREEN,
  HOME_SCREEN,
  ERROR_HISTORY_SCREEN,
} from '../../constants/routeNames';
import EditUnitNameModal from '../RemoteHome/groupModal';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 25,
    paddingTop: 0,
  },
  titleRow: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    marginRight: 40,
  },
  text: {
    fontSize: 26,
    fontWeight: '700',
    maxWidth: widthPercentage(60),
  },
  onOffIcon: {
    width: 38,
    height: 38,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  marginTop: {
    marginTop: heightPercentage(3),
  },
  marginLeft: {
    marginLeft: widthPercentage(10),
  },
  tempText: {
    marginLeft: 10,
    fontSize: 17,
  },
  grey: {
    color: Colors.linkGrey,
  },
  white: {
    color: Colors.white,
  },
  black: {
    color: Colors.black,
  },
  iconContainer: {
    height: 17,
    width: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  optionIcon: {
    height: 17,
    width: 10,
    resizeMode: 'contain',
    marginHorizontal: widthPercentage(3),
  },
  setTempText: {
    color: Colors.titleGrey,
    fontSize: 14,
  },
  setTemp: {
    fontSize: 50,
    fontWeight: '600',
    marginBottom: heightPercentage(2),
  },
  modeText: {
    color: Colors.titleGrey,
    fontSize: 12,
    marginTop: heightPercentage(1),
  },
  align: {
    alignItems: 'center',
  },
  rightIcon: {
    marginLeft: widthPercentage(5),
    width: 70,
  },
  justify: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  marginTop2: {
    marginTop: heightPercentage(2),
  },
  width: {
    width: widthPercentage(40),
  },
  content: {
    marginTop: heightPercentage(10),
  },
  paddingLeft: {
    paddingLeft: 6,
  },
  headerContainer: {
    height: heightPercentage(8),
    width: viewportWidth,
    ...Platform.select({
      ios: {
        marginTop: iphoneXS ? heightPercentage(5.4) : heightPercentage(4),
      },
    }),
  },
  rowContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  alignEnd: { alignItems: 'flex-end', padding: 10, flexDirection: 'row' },
  hitSlop: {
    bottom: 10,
    left: 10,
  },
  optionsWrapperStyle: { margin: 5 },
  optionsContainerStyle: { marginTop: 22, width: 147 },
  paddingBottom: {
    paddingBottom: 10,
  },
  refreshIcon: {
    tintColor: Colors.titleGrey,
    marginRight: 10,
  },
  loading: {
    height: 15,
  },
  connectionRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  connectionIcon: {
    height: widthPercentage(3),
    width: widthPercentage(3),
    borderRadius: widthPercentage(1.5),
    backgroundColor: Colors.limeGreen,
  },
  connectionText: {
    marginRight: 12,
  },
  bgDisconnected: {
    backgroundColor: Colors.grey,
    marginRight: 5,
  },
  bgConnected: {
    backgroundColor: Colors.limeGreen,
    marginRight: 5,
  },
  disconnected: {
    color: Colors.grey,
  },
  connected: {
    color: Colors.limeGreen,
  },
  borderBottom: {
    borderBottomWidth: 0.2,
    borderBottomColor: Colors.lightCoolGrey,
  },
  optionWrapperStyle: {
    paddingTop: 8,
  },
  pencilIcon: {
    tintColor: Colors.darkGrey,
    marginLeft: 8,
  },
  error: {
    fontWeight: '600',
    fontSize: 16,
    marginTop: Platform.select({ ios: 2, default: 5 }),
  },
  overlay: {
    backgroundColor: Colors.transparentBlack,
    height: heightPercentage(100),
    width: widthPercentage(100),
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'center',
    paddingTop: heightPercentage(25),
    zIndex: 999,
  },
  overlayWrapper: {
    backgroundColor: Colors.white,
    width: widthPercentage(80),
    height: heightPercentage(30),
    paddingHorizontal: widthPercentage(4),
    borderRadius: 10,
    justifyContent: 'center',
  },
  overlayTitle: {
    alignItems: 'center',
  },
  overlayContent: {
    marginTop: 20,
  },
  overlayText: {
    color: Colors.black,
    textAlign: 'center',
  },
  overlayButton: {
    height: heightPercentage(5),
    marginBottom: 8,
  },
  overlayButtonText: {
    fontSize: ScaleText(12),
  },
});

const handleNavigateToHome = () => {
  NavigationService.navigate(HOME_SCREEN);
};

const EditUnitNameIcon = ({
  unitName,
  isLoading,
  thingName,
  submitEditUnitName,
  success,
  error,
  clearEditStatus,
}) => {
  const [showEditModal, setShowEditModal] = useState(false);

  useHandleResponse({
    success,
    showSuccess: true,
    error,
    clearStatus: () => {
      if (showEditModal) setShowEditModal(false);
      setTimeout(() => {
        clearEditStatus();
      }, 500);
    },
  });

  const onCancel = () => setShowEditModal(false);

  const onSubmit = unitNameValue => {
    submitEditUnitName(thingName, unitNameValue);
  };

  return (
    <>
      <TouchableOpacity onPress={() => setShowEditModal(true)}>
        <Image
          source={require('../../shared/images/general/pencil.png')}
          style={styles.pencilIcon}
        />
      </TouchableOpacity>
      <EditUnitNameModal
        isVisible={showEditModal}
        onCancel={onCancel}
        onSubmit={onSubmit}
        title="Edit Unit Name"
        inputPlaceHolder="Unit Name"
        value={unitName}
        isLoading={isLoading}
      />
    </>
  );
};

const mapEditUnitIconDispatchToProps = dispatch => ({
  submitEditUnitName: (thingName, unitName) => {
    dispatch(editUnits.submitEditUnitName({ thingName, unitName }));
  },
  clearEditStatus: () => dispatch(editUnits.clearEditStatus()),
});

const mapEditUnitIconStateToProps = ({
  editUnits: { success, error, isLoading },
}) => ({
  success,
  error,
  isLoading,
});

EditUnitNameIcon.propTypes = {
  unitName: PropTypes.string,
  isLoading: PropTypes.bool,
  thingName: PropTypes.string,
  submitEditUnitName: PropTypes.func,
  success: PropTypes.string,
  error: PropTypes.string,
  clearEditStatus: PropTypes.func,
};

const EditUnitNameIconContainer = connect(
  mapEditUnitIconStateToProps,
  mapEditUnitIconDispatchToProps
)(EditUnitNameIcon);

const RemoteOverlayModal = ({
  isConnected,
  isOTARunning,
  isIoTMode,
  isReconnect,
  navigateToWifiConfig,
  isPubSubDisconnected,
}) => {
  if (!isConnected && !isReconnect && isIoTMode) {
    return (
      <View style={styles.overlay}>
        <View style={styles.overlayWrapper}>
          <View style={styles.overlayTitle}>
            <Text medium style={styles.overlayText}>
              Controls are disabled
            </Text>
            <Text medium style={styles.overlayText}>
              while the unit is disconnected.
            </Text>
          </View>
          <View style={styles.overlayContent}>
            <Button
              containerStyle={styles.overlayButton}
              textStyle={styles.overlayButtonText}
              onPress={navigateToWifiConfig}>
              Wi-Fi Network Configuration
            </Button>
            <Button
              containerStyle={styles.overlayButton}
              textStyle={styles.overlayButtonText}
              onPress={navigateToWifiConfig}>
              Reconnect
            </Button>
          </View>
        </View>
      </View>
    );
  }
  if (isIoTMode && isPubSubDisconnected) {
    return (
      <View style={styles.overlay}>
        <View style={styles.overlayWrapper}>
          <View style={styles.overlayTitle}>
            <Text medium style={styles.overlayText}>
              Unable to retrieve unit data from server
            </Text>
            <Text medium style={styles.overlayText}>
              The unit is reconnecting...
            </Text>
          </View>
        </View>
      </View>
    );
  }

  return (
    isOTARunning && (
      <View style={styles.overlay}>
        <View style={styles.overlayWrapper}>
          <View style={styles.overlayTitle}>
            <Text medium style={styles.overlayText}>
              Controls are disabled
            </Text>
            <Text medium style={styles.overlayText}>
              while the unit is performing firmware upgrade.
            </Text>
          </View>
        </View>
      </View>
    )
  );
};

RemoteOverlayModal.propTypes = {
  isConnected: PropTypes.bool,
  isOTARunning: PropTypes.bool,
  isReconnect: PropTypes.bool,
  isIoTMode: PropTypes.bool,
  navigateToWifiConfig: PropTypes.func,
  isPubSubDisconnected: PropTypes.bool,
};

const Remote = ({
  onMountUnit,
  switchAc,
  unit,
  setMode,
  setTemp,
  allUnits,
  setAllUnits,
  navigation,
  onUnmountUnit,
  remoteConnectionType,
  dispatch,
  onRefreshUnit,
  isLoading,
  submitFirmwareUpgrade,
  updateFirmwareLoading,
  updateFirmwareLoadingText,
  updateFirmwareSuccess,
  updateFirmwareError,
  clearFirmwareUpgradeStatus,
  notifiedUnitError,
  updateNotifiedError,
  isReconnect,
  cancelFirmwareUpgradeTask,
  checkUnitAuth,
  cancelCheckUnitAuth,
  remoteError,
  clearRemoteStatus,
  desired,
  connectionStatus,
  clearUnitUsers,
}) => {
  const isIoTMode = remoteConnectionType === ConnectionType.IOT_MODE;
  const acTemp = unit.status ? unit.modeConfig.acTemp : 18;
  const [temperature, setTemperature] = useState(parseInt(acTemp, 10) || 18);

  const connected = unit.status === 'connected';
  const disconnected = unit.status === 'disconnected';

  const errorInfo = getErrorCodeDetail(unit.errorCode);

  const showPopup = () => {
    ModalService.show({
      title: `${errorInfo.label}`,
      desc: `${errorInfo.description}`,
      buttons: [{ title: 'No', isRaised: true }],
    });
  };

  useEffect(() => {
    if (isIoTMode && !Object.entries(unit).length) {
      handleNavigateToHome();

      Alert.alert('No access', 'This unit is no longer bind to your account.');

      clearRemoteStatus();
    }
  }, [unit, isIoTMode]);

  useEffect(() => {
    if (isIoTMode) {
      checkUnitAuth(handleNavigateToHome);

      return cancelCheckUnitAuth;
    }

    return () => {};
  }, [isIoTMode]);

  useHandleResponse({ error: remoteError, onError: clearRemoteStatus });

  useHandleResponse({
    success: updateFirmwareSuccess,
    showSuccess: true,
    error: updateFirmwareError,
    clearStatus: clearFirmwareUpgradeStatus,
    onLeave: clearUnitUsers,
  });

  useEffect(() => {
    if (unit.errorCode && unit.errorCode > 0 && !notifiedUnitError) {
      showPopup();
      updateNotifiedError(unit.thingName, true);
    }
  }, [unit.errorCode, notifiedUnitError]);

  useEffect(() => {
    if (unit && unit.mode === 'heat') {
      const heatMode = 'cool';
      setMode(MODE_ENUM[heatMode]);
    }
  }, [unit.mode]);

  useEffect(() => {
    if (unit.thingName) {
      onMountUnit(unit.thingName);

      return () => {
        onUnmountUnit(unit.thingName);
      };
    }

    return undefined;
  }, [unit, unit.thingName]);

  const navigateToUsersScreen = () => {
    NavigationService.navigate(UNIT_USERS_SCREEN);
  };

  const toggleSwitch = () => {
    const switchType = unit.switch === 1 ? 0 : 1;
    if (connected) {
      switchAc(unit.ThingName, switchType);
    }
  };

  const textColor = () => {
    if (disconnected) {
      return styles.grey;
    }
    return styles.black;
  };

  const indoorTempIcon = () => {
    if (disconnected) {
      return require('../../shared/images/remote/indoorTempLightGrey.png');
    }
    return require('../../shared/images/remote/indoorTempGrey.png');
  };

  const tempIcon = () => {
    if (disconnected) {
      return require('../../shared/images/remote/outdoorTempLightGrey.png');
    }
    return require('../../shared/images/remote/outdoorTempGrey.png');
  };

  const switchIcon = () => {
    if (disconnected) {
      return require('../../shared/images/remote/off.png');
    }
    if (unit.switch === 1) {
      return require('../../shared/images/remote/on.png');
    }
    return require('../../shared/images/remote/turnOn.png');
  };

  const unitModeButton = mode => (
    <ModeButton
      onPress={() => setMode(MODE_ENUM[mode])}
      mode={mode}
      currentMode={unit.mode}
      status={unit.status}
      switchValue={unit.switch}
    />
  );

  const connectedToInternet = useNetInfo().isConnected;

  const unitConnectionColor = disconnected
    ? styles.bgDisconnected
    : styles.bgConnected;
  const appConnectionColor = connectedToInternet
    ? styles.bgConnected
    : styles.bgDisconnected;

  const unitConnectionTextColor = disconnected
    ? styles.disconnected
    : styles.connected;
  const appConnectionTextColor = connectedToInternet
    ? styles.connected
    : styles.disconnected;

  const navigateToWifiConfig = () => {
    NavigationService.navigate(WIFI_CONFIG_SCREEN, {
      thingName: unit.thingName,
      thingType: unit.ThingType,
      isReconnect: true,
    });
  };

  const navigateToErrorHistory = () => {
    NavigationService.navigate(ERROR_HISTORY_SCREEN, {
      thingName: unit.thingName,
      isReconnect: true,
    });
  };

  const handleFirmwareUpgradeCancel = () => {
    ModalService.show({
      title: 'Cancel firmware upgrade',
      desc: 'Are you sure you want to cancel firmware upgrade?',
      buttons: [
        { title: 'No' },
        { title: 'Yes', onPress: cancelFirmwareUpgradeTask, isRaised: true },
      ],
    });
  };
  return (
    <>
      <View style={styles.headerContainer}>
        <View style={[styles.rowContainer, styles.paddingBottom]}>
          <View>
            <BackButton {...{ navigation }} />
          </View>
          <View style={styles.alignEnd}>
            <TouchableOpacity
              onPress={() => onRefreshUnit(unit.thingName)}
              hitSlop={styles.hitSlop}
              disabled={isLoading}>
              <Image
                source={require('../../shared/images/general/refresh.png')}
                style={styles.refreshIcon}
              />
            </TouchableOpacity>
            <View hitSlop={styles.hitSlop}>
              {isIoTMode && (
                <Menu>
                  <MenuTrigger
                    customStyles={{
                      TriggerTouchableComponent: TouchableOpacity,
                    }}>
                    <Image
                      source={require('../../shared/images/general/option.png')}
                      style={styles.optionIcon}
                    />
                  </MenuTrigger>
                  <MenuOptions
                    customStyles={{
                      optionsWrapper: styles.optionsWrapperStyle,
                      optionsContainer: styles.optionsContainerStyle,
                      OptionTouchableComponent: TouchableOpacity,
                      optionWrapper: styles.optionWrapperStyle,
                    }}>
                    <MenuOption
                      onSelect={navigateToUsersScreen}
                      customStyles={{ optionWrapper: styles.borderBottom }}>
                      <Text>User Management</Text>
                    </MenuOption>
                    <MenuOption
                      onSelect={navigateToWifiConfig}
                      customStyles={{ optionWrapper: styles.borderBottom }}>
                      <Text>Wi-Fi Network Configuration</Text>
                    </MenuOption>
                    <MenuOption
                      onSelect={() => submitFirmwareUpgrade(unit.thingName)}
                      customStyles={{ optionWrapper: styles.borderBottom }}>
                      <Text>Update Firmware Version</Text>
                    </MenuOption>
                    <MenuOption onSelect={navigateToErrorHistory}>
                      <Text>Error History</Text>
                    </MenuOption>
                  </MenuOptions>
                </Menu>
              )}
            </View>
          </View>
        </View>
      </View>
      {/* Only show when it is reconnecting */}
      <Loading visible={!updateFirmwareLoading && isReconnect}>
        Please wait while the unit is reconnecting to network...
      </Loading>
      <Loading
        visible={updateFirmwareLoading}
        onCancel={handleFirmwareUpgradeCancel}>
        {updateFirmwareLoadingText}
      </Loading>

      {unit.status && (
        <>
          <View style={styles.container}>
            {!updateFirmwareLoading && (
              <RemoteOverlayModal
                isConnected={unit.status === 'connected'}
                navigateToWifiConfig={navigateToWifiConfig}
                isOTARunning={unit.otaRunning === 1}
                isReconnect={isReconnect}
                isIoTMode={isIoTMode}
                isPubSubDisconnected={connectionStatus === 'DISCONNECTED'}
              />
            )}
            <View>
              <View style={styles.connectionRow}>
                <View style={styles.connectionRow}>
                  <Text small style={styles.connectionText}>
                    Unit
                  </Text>
                  <View style={[styles.connectionIcon, unitConnectionColor]} />
                  <Text small style={unitConnectionTextColor}>
                    {disconnected ? 'Offline' : 'Online'}
                  </Text>
                </View>
                <View style={styles.connectionRow}>
                  <Text
                    small
                    style={[styles.connectionText, styles.marginLeft]}>
                    App
                  </Text>
                  <View style={[styles.connectionIcon, appConnectionColor]} />
                  <Text small style={appConnectionTextColor}>
                    {connectedToInternet ? 'Online' : 'Offline'}
                  </Text>
                </View>
              </View>
            </View>
            {unit.errorCode !== 0 && errorInfo && (
              <TouchableOpacity onPress={() => showPopup()}>
                <Text style={[styles.error, { color: errorInfo.color }]}>
                  {`Error ${errorInfo.label}!`}
                </Text>
              </TouchableOpacity>
            )}
            <View style={styles.titleRow}>
              <View style={styles.row}>
                <Text style={styles.text} numberOfLines={1}>
                  {unit.ACName}
                </Text>
                {isIoTMode && (
                  <EditUnitNameIconContainer
                    unitName={unit.ACName}
                    thingName={unit.thingName}
                  />
                )}
              </View>

              <TouchableOpacity
                onPress={() => toggleSwitch()}
                disabled={disconnected}>
                <Image source={switchIcon()} style={styles.onOffIcon} />
              </TouchableOpacity>
            </View>
            <View style={styles.loading}>
              {isLoading && (
                <ActivityIndicator size="small" color={Colors.grey} />
              )}
            </View>
            <View style={styles.row}>
              <Image source={indoorTempIcon()} />
              <Text style={[styles.tempText, textColor()]}>
                {SetIndoorTemperature(unit.indoorTemp)}{' '}
              </Text>
              {unit.outdoorTemp > 0 && (
                <>
                  <Image source={tempIcon()} style={styles.marginLeft} />
                  <Text style={[styles.tempText, textColor()]}>{`${
                    unit.outdoorTemp > 0 ? unit.outdoorTemp : '-'
                  }°C`}</Text>
                </>
              )}
            </View>
            <View style={styles.justify}>
              <View style={[styles.width, styles.content]}>
                <Text style={styles.setTempText}>Set Temperature</Text>
                <Text style={[styles.setTemp, textColor()]}>{`${
                  getTempValue(temperature, unit.mode) || '-'
                }°C`}</Text>
                <View style={[styles.row, styles.marginTop2]}>
                  <View style={styles.align}>
                    {unitModeButton('cool')}
                    <Text style={styles.modeText}>Cool</Text>
                  </View>
                  <View style={[styles.align, styles.rightIcon]}>
                    {unitModeButton('fan')}
                    <Text style={styles.modeText}>Fan Only</Text>
                  </View>
                </View>
                <View style={[styles.row, styles.marginTop2]}>
                  <View style={styles.align}>
                    {unitModeButton('dry')}
                    <Text style={styles.modeText}>Dry</Text>
                  </View>
                  {/* <View style={[styles.align, styles.rightIcon]}>
                    {unitModeButton('auto')}
                    <Text style={styles.modeText}>Automatic</Text>
                  </View> */}
                </View>
              </View>
              <TempSlider
                unit={unit}
                allUnits={allUnits}
                setTemp={temp => setTemp(temp)}
                setAllUnits={newUnits => setAllUnits(newUnits)}
                temperature={temperature}
                setTemperature={setTemperature}
                {...{ unit, setMode, setTemp, isLoading, desired }}
              />
            </View>
          </View>
          <BottomSheet
            {...{
              unit,
              dispatch,
              disabled: connectionStatus === 'DISCONNECTED',
            }}
          />
        </>
      )}
    </>
  );
};

Remote.navigationOptions = () => ({
  headerShown: false,
});

Remote.propTypes = {
  onMountUnit: PropTypes.func,
  switchAc: PropTypes.func,
  setTemp: PropTypes.func,
  setMode: PropTypes.func,
  setAllUnits: PropTypes.func,
  dispatch: PropTypes.func,
  onRefreshUnit: PropTypes.func,
  submitEditParams: PropTypes.func,
  onUnmountUnit: PropTypes.func,
  submitFirmwareUpgrade: PropTypes.func,
  clearFirmwareUpgradeStatus: PropTypes.func,
  updateNotifiedError: PropTypes.func,

  remoteConnectionType: PropTypes.string,
  unit: PropTypes.object,
  allUnits: PropTypes.object,
  navigation: PropTypes.object,
  isLoading: PropTypes.bool,
  updateFirmwareLoading: PropTypes.bool,
  updateFirmwareLoadingText: PropTypes.string,
  updateFirmwareSuccess: PropTypes.string,
  updateFirmwareError: PropTypes.string,
  notifiedUnitError: PropTypes.bool,
  isReconnect: PropTypes.bool,
  cancelFirmwareUpgradeTask: PropTypes.func,
  checkUnitAuth: PropTypes.func,
  cancelCheckUnitAuth: PropTypes.func,
  remoteError: PropTypes.string,
  clearRemoteStatus: PropTypes.func,
  desired: PropTypes.object,
  connectionStatus: PropTypes.string,
  clearUnitUsers: PropTypes.func,
};

Remote.defaultProps = {
  unit: {},
};

const mapStateToProps = ({
  remote: {
    thingName,
    connectionType: remoteConnectionType,
    isLoading,
    isReconnect,
    error: remoteError,
    desired: { temp },
    connectionStatus,
  },
  units: { allUnits, hasNotifiedError },
  updateFirmware: {
    isLoading: updateFirmwareLoading,
    loadingText: updateFirmwareLoadingText,
    success: updateFirmwareSuccess,
    error: updateFirmwareError,
  },
}) => ({
  unit: allUnits[thingName],
  allUnits,
  remoteConnectionType,
  isLoading,
  updateFirmwareLoading,
  updateFirmwareLoadingText,
  updateFirmwareSuccess,
  updateFirmwareError,
  notifiedUnitError: hasNotifiedError[thingName],
  isReconnect,
  remoteError,
  desired: { temp },
  connectionStatus,
});

const mapDispatchToProps = dispatch => ({
  onMountUnit: thingName => {
    dispatch(remote.onMountUnit(thingName));
  },
  onUnmountUnit: thingName => dispatch(remote.onUnmountUnit(thingName)),
  switchAc: (thingName, switchType) => {
    dispatch(remote.setAcSwitch(thingName, switchType));
  },
  setTemp: value => dispatch(remote.setAcTemp(value)),
  setMode: mode => {
    dispatch(remote.setMode(mode));
  },
  setExpectedTemp: desired => {
    dispatch(remote.setExpectedTemp(desired));
  },
  setAllUnits: allUnits => {
    dispatch(units.setAllUnits(allUnits));
  },
  submitEditParams: editParams =>
    dispatch(editUnits.submitEditParams(editParams)),
  dispatch,
  onRefreshUnit: thingName => dispatch(remote.refreshUnit(thingName)),
  submitFirmwareUpgrade: thingName =>
    dispatch(
      updateFirmware.submitFirmwareUpgrade({
        thingName,
        ios: Platform.select({ ios: true, android: false }),
      })
    ),
  clearFirmwareUpgradeStatus: () =>
    dispatch(updateFirmware.clearUpdateStatus()),
  updateNotifiedError: (thingName, value) =>
    dispatch(units.updateNotifiedError(thingName, value)),
  cancelFirmwareUpgradeTask: () =>
    dispatch(updateFirmware.cancelFirmwareUpgrade()),
  checkUnitAuth: onUnbind => dispatch(remote.checkUnitAuth({ onUnbind })),
  cancelCheckUnitAuth: () => dispatch(remote.cancelCheckUnitAuth()),
  clearRemoteStatus: () => dispatch(remote.clearRemoteStatus()),
  clearUnitUsers: () => dispatch(unitUsers.clearUnitUsers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Remote);
