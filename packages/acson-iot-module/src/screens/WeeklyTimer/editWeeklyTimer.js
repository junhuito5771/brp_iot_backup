import React, { useState, useRef, useEffect } from 'react';
import {
  View,
  SafeAreaView,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  Image,
  Platform,
  BackHandler,
} from 'react-native';
import PropTypes from 'prop-types';
import DatePicker from 'react-native-date-picker';
import {
  ScaleText,
  heightPercentage,
  StringHelper,
  scale,
  verticalScale,
} from '@module/utility';
import { connect, editTimer } from '@daikin-dama/redux-iot';

import {
  ModalService,
  Text,
  Colors,
  Button,
  BottomModal,
  WheelPicker,
  useHandleResponse,
} from '@module/acson-ui';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  addTimerButton: {
    marginHorizontal: scale(10),
    marginVertical: verticalScale(15),
    backgroundColor: Colors.lightGrey,
    borderRadius: 25,
    padding: scale(15),
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  addTimerText: {
    color: Colors.titleGrey,
    marginLeft: 10,
  },
  dayContainer: {
    paddingVertical: verticalScale(15),
    paddingHorizontal: scale(25),
    backgroundColor: Colors.white,
    borderColor: Colors.borderlightGray,
    borderWidth: 1,
    borderRadius: 15,
    marginVertical: verticalScale(10),
  },
  dayOptionContainer: {
    paddingHorizontal: scale(10),
    justifyContent: 'space-between',
  },
  dayText: {
    color: Colors.titleGrey,
  },
  selectedDayText: {
    color: Colors.red,
    fontWeight: 'bold',
  },
  unitNameContainer: {
    paddingVertical: verticalScale(10),
  },
  labelContainer: {
    marginTop: verticalScale(5),
    marginBottom: verticalScale(15),
  },
  labelText: {
    fontSize: ScaleText(14),
    fontWeight: '600',
  },
  contentContainer: {
    marginTop: verticalScale(5),
    paddingHorizontal: scale(15),
  },
  timerTitleContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: verticalScale(10),
  },
  trashIcon: {
    padding: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
  },
  hitslop: {
    top: 20,
    bottom: 20,
    right: 20,
    left: 20,
  },
  timerOptionContainer: {
    paddingHorizontal: scale(10),
    borderRadius: 10,
    borderColor: Colors.borderlightGray,
    borderWidth: 1,
  },
  hasSpaced: {
    marginBottom: heightPercentage(5),
  },
  timerOptionItem: {
    flexDirection: 'row',
    paddingVertical: verticalScale(15),
    paddingHorizontal: scale(10),
    justifyContent: 'space-between',
  },
  bottomBorder: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.borderlightGray,
  },
  timerOptionLabelText: {},
  timerOptionValueText: {},
  saveButtonContainer: {
    marginVertical: verticalScale(10),
    marginHorizontal: scale(15),
  },
});

const ADD_ICON = require('../../shared/images/timer/add.png');
const TRASH_ICON = require('../../shared/images/timer/trash.png');

const days = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];

const DaySelector = ({ selectedValue, onSelectionChange }) => {
  const hasDaySelected = selectedValue.some(v => v);

  return (
    <>
      {!hasDaySelected && (
        <Text small color={Colors.red}>
          Please select at least 1 day
        </Text>
      )}
      <View style={styles.dayContainer}>
        <View style={styles.labelContainer}>
          <Text style={styles.labelText}>Select Day</Text>
        </View>
        <View style={[styles.flexRow, styles.dayOptionContainer]}>
          {days.map((day, index) => {
            const isSelected = selectedValue[index];
            const key = `${day}_${index}`;

            return (
              <TouchableOpacity
                key={key}
                onPress={() => onSelectionChange(index)}
                hitSlop={styles.hitslop}>
                <Text
                  style={[
                    styles.dayText,
                    isSelected && styles.selectedDayText,
                  ]}>
                  {day}
                </Text>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    </>
  );
};

DaySelector.propTypes = {
  selectedValue: PropTypes.array,
  onSelectionChange: PropTypes.func,
};

const getOptionProps = ({
  option,
  filteredModes,
  selectedMode,
  enableSilent,
  enableAutoFan,
  enableLowFan,
  timer,
  setParam,
}) => {
  const { key } = option;
  switch (key) {
    case 'power':
      return {
        title: 'Power',
        values: [
          { label: 'ON', value: 1 },
          { label: 'OFF', value: 0 },
        ],
        selectedValue: timer.switchValue,
        onConfirm: (value, timerIndex) =>
          setParam('switchValue', value, timerIndex),
      };
    case 'mode': {
      const availableModes = Object.keys(filteredModes).filter(
        modeKey => filteredModes[modeKey] === 0
      );
      const values = [];

      availableModes.forEach(mode => {
        values.push({ label: StringHelper.toCamelCase(mode), value: mode });
      });

      return {
        title: 'Select mode',
        values,
        selectedValue: timer.mode,
        onConfirm: (value, timerIndex) => setParam('mode', value, timerIndex),
      };
    }
    case 'temp': {
      const values = [];
      for (let i = 18; i <= 32; i += 1) {
        values.push({ label: i.toString(), value: i });
      }
      return {
        title: 'Temperature',
        values,
        selectedValue: timer.temp,
        onConfirm: (value, timerIndex) => setParam('temp', value, timerIndex),
      };
    }
    case 'fan': {
      const values = [];
      if (enableAutoFan) values.push({ label: 'Auto', value: 'auto' });

      if (selectedMode !== 'dry') {
        if (enableSilent) values.push({ label: 'Silent', value: 'silent' });
        if (enableLowFan) values.push({ label: 'Low', value: 'level1' });

        values.push({ label: 'Medium', value: 'level2' });
        values.push({ label: 'High', value: 'level3' });
      }

      const selectedValue = timer.silent === 1 ? 'silent' : timer.fanLevel;

      return {
        title: 'Fan speed',
        values,
        selectedValue,
        onConfirm: (value, timerIndex) => {
          const silentValue = value === 'silent' ? 1 : 0;
          setParam('silent', silentValue, timerIndex);
          if (value !== 'silent') {
            setParam('fanLevel', value, timerIndex);
          }
        },
      };
    }
    case 'time': {
      const curDT = new Date();
      curDT.setHours(timer.hour);
      curDT.setMinutes(timer.minute);
      return {
        title: 'Time',
        component: DatePicker,
        locale: Platform.select({ ios: 'fr', android: 'en' }),
        mode: 'time',
        selectedValue: curDT,
        onConfirm: (value, timerIndex) => {
          const hour = value.getHours();
          const minute = value.getMinutes();
          setParam('hour', hour, timerIndex);
          setParam('minute', minute, timerIndex);
        },
      };
    }
    default:
      return { title: 'Unknown', values: [] };
  }
};

const getFanValue = (fanLevel, silent) => {
  if (silent === 1) return 'Silent';

  switch (fanLevel) {
    case 'auto':
      return 'Auto';
    case 'level2':
      return 'Medium';
    case 'level3':
      return 'High';
    default:
      return 'Low';
  }
};

const canShowTemperature = mode => mode !== 'fan' && mode !== 'dry';

const ModalContent = ({
  title,
  values,
  selectedValue,
  onConfirm,
  option,
  closeModal,
  component,
  ...extraProps
}) => {
  const [curValue, setCurValue] = useState(selectedValue);

  const ModalComponent = component || WheelPicker;

  useEffect(() => {
    if (selectedValue) {
      setCurValue(selectedValue);
    }
  }, [selectedValue]);

  return (
    <View>
      <View
        style={[
          {
            paddingHorizontal: 35,
            justifyContent: 'center',
            alignItems: 'center',
          },
        ]}>
        <Text medium bold>
          {title}
        </Text>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 20,
          }}>
          <ModalComponent
            selectedValue={curValue}
            onValueChange={itemValue => setCurValue(itemValue)}
            date={curValue}
            onDateChange={date => {
              setCurValue(date);
            }}
            data={values}
            {...extraProps}
          />
        </View>
      </View>
      <View style={{ marginVertical: 10, paddingHorizontal: 20 }}>
        <Button
          raised
          onPress={() => {
            onConfirm(curValue, option.timerIndex);
            closeModal();
          }}>
          Select
        </Button>
      </View>
    </View>
  );
};

ModalContent.propTypes = {
  title: PropTypes.string,
  values: PropTypes.array,
  selectedValue: PropTypes.any,
  onConfirm: PropTypes.func,
  option: PropTypes.object,
  closeModal: PropTypes.func,
  component: PropTypes.any,
};

const EditWeeklyTimerScreen = ({
  unitName,
  timerList,
  thingName,
  modes,
  selectedDays,
  setDaysValue,
  enableSilent,
  enableAutoFan,
  enableLowFan,
  setParam,
  removeTimer,
  addNewTimer,
  isNew,
  submitTimer,
  success,
  error,
  isLoading,
  navigation,
  clearStatus,
  canSubmit,
  email,
}) => {
  const bottomModalRef = useRef();
  const scrollViewRef = useRef();
  const [curOption, setCurOption] = useState({ timerIndex: 0, key: 'power' });

  const curTimer = timerList[curOption.timerIndex] || {};

  const filteredModes = Object.keys(modes)
    .filter(mode => mode !== 'auto' && mode !== 'heat')
    .reduce((fmode, key) => {
      fmode[key] = modes[key]; // eslint-disable-line no-param-reassign
      return fmode;
    }, {});

  const curMode =
    curTimer.mode in filteredModes
      ? curTimer.mode
      : Object.keys(filteredModes)[0];

  // navigation back handler if changes have been made
  useEffect(() => {
    const modalProps = {
      title: 'Are you sure?',
      desc: 'Changes made will not be saved.',
      buttons: [
        {
          title: 'Yes',
          onPress: () => navigation.goBack(),
        },
        {
          title: 'No',
          isRaised: true,
        },
      ],
    };

    let backListener;
    if (canSubmit) {
      navigation.setParams({ modalProps });
      backListener = BackHandler.addEventListener('hardwareBackPress', () => {
        ModalService.show({
          ...modalProps,
        });
        return true;
      });
    } else {
      navigation.setParams({ modalProps: undefined });
    }

    return () => {
      if (backListener) backListener.remove();
      BackHandler.removeEventListener('hardwareBackPress', () => {});
    };
  }, [canSubmit]);

  useHandleResponse({
    success,
    error,
    onSuccess: () => navigation.goBack(),
    clearStatus,
  });

  useEffect(() => {
    navigation.setParams({ isNew });
  }, [isNew]);

  const handleRemoveTimer = timerIndex => {
    ModalService.show({
      title: 'Remove timer',
      desc: 'Are you sure you want to remove this timer?',
      buttons: [
        { title: 'Yes', onPress: () => removeTimer(timerIndex) },
        { title: 'No', isRaised: true },
      ],
    });
  };

  const handleAddNewTimer = () => {
    addNewTimer(filteredModes, email);

    requestAnimationFrame(() => {
      scrollViewRef.current.scrollToEnd({ animation: true });
    });
  };

  return (
    <SafeAreaView style={styles.flex}>
      <ScrollView
        contentContainerStyle={styles.contentContainer}
        ref={scrollViewRef}>
        <View style={styles.unitNameContainer}>
          <Text medium>{unitName}</Text>
        </View>
        {isNew && (
          <DaySelector
            selectedValue={selectedDays}
            onSelectionChange={setDaysValue}
          />
        )}
        {isNew && (
          <TouchableOpacity onPress={handleAddNewTimer}>
            <View style={[styles.addTimerButton, styles.flexRow]}>
              <Text style={styles.addTimerText}>Add Timer</Text>
              <Image source={ADD_ICON} />
            </View>
          </TouchableOpacity>
        )}
        {isNew && timerList.length === 0 && (
          <Text small color={Colors.red}>
            Please create at least 1 timer
          </Text>
        )}
        {timerList.map((timerInfo, index) => {
          const {
            hour,
            minute,
            switchValue,
            mode,
            temp,
            fanLevel,
            silent,
            timerIndex,
          } = timerInfo;

          const isPowerOn = switchValue === 1;

          const key = `${thingName}_${timerIndex}_${hour}_${minute}`;

          const filteredMode =
            mode in filteredModes ? mode : Object.keys(filteredModes)[0];

          return (
            <View
              key={key}
              style={StyleSheet.flatten([
                index < timerList.length && styles.hasSpaced,
              ])}>
              <View style={[styles.flexRow, styles.timerTitleContainer]}>
                <Text medium>{`Timer ${index + 1}`}</Text>
                <TouchableOpacity
                  hitslop={styles.hitslop}
                  onPress={() => handleRemoveTimer(timerIndex)}>
                  <View style={styles.trashIcon}>
                    <Image source={TRASH_ICON} />
                  </View>
                </TouchableOpacity>
              </View>
              <View style={styles.timerOptionContainer}>
                <View style={styles.bottomBorder}>
                  <TouchableOpacity
                    style={styles.flex}
                    onPress={() => {
                      bottomModalRef.current.open();
                      setCurOption({
                        timerIndex: index,
                        key: 'power',
                      });
                    }}>
                    <View style={styles.timerOptionItem}>
                      <Text style={styles.timerOptionLabelText}>Power</Text>
                      <Text style={styles.timerOptionValueText}>
                        {isPowerOn ? 'ON' : 'OFF'}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View
                  style={StyleSheet.flatten([
                    isPowerOn && styles.bottomBorder,
                  ])}>
                  <TouchableOpacity
                    style={styles.flex}
                    onPress={() => {
                      bottomModalRef.current.open();
                      setCurOption({ timerIndex: index, key: 'time' });
                    }}>
                    <View style={styles.timerOptionItem}>
                      <Text style={styles.timerOptionLabelText}>
                        Select Time
                      </Text>
                      <Text style={styles.timerOptionValueText}>
                        {StringHelper.getTimeInString(hour, minute)}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
                {isPowerOn && (
                  <>
                    <View style={styles.bottomBorder}>
                      <TouchableOpacity
                        style={styles.flex}
                        onPress={() => {
                          bottomModalRef.current.open();
                          setCurOption({
                            timerIndex: index,
                            key: 'mode',
                          });
                        }}>
                        <View style={styles.timerOptionItem}>
                          <Text style={styles.timerOptionLabelText}>
                            Select Mode
                          </Text>
                          <Text
                            style={
                              styles.timerOptionValueText
                            }>{`${StringHelper.toCamelCase(
                            filteredMode
                          )} mode`}</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                    {canShowTemperature(filteredMode) && (
                      <View style={styles.bottomBorder}>
                        <TouchableOpacity
                          style={styles.flex}
                          onPress={() => {
                            bottomModalRef.current.open();
                            setCurOption({
                              timerIndex: index,
                              key: 'temp',
                            });
                          }}>
                          <View
                            style={[
                              styles.timerOptionItem,
                              styles.bottomBorder,
                            ]}>
                            <Text style={styles.timerOptionLabelText}>
                              Temperature
                            </Text>
                            <Text
                              style={
                                styles.timerOptionValueText
                              }>{`${temp} \u2103`}</Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    )}
                    <View>
                      <TouchableOpacity
                        style={styles.flex}
                        onPress={() => {
                          bottomModalRef.current.open();
                          setCurOption({
                            timerIndex: index,
                            key: 'fan',
                          });
                        }}>
                        <View style={styles.timerOptionItem}>
                          <Text style={styles.timerOptionLabelText}>
                            Fan Speed
                          </Text>
                          <Text style={styles.timerOptionValueText}>
                            {getFanValue(fanLevel, silent)}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </>
                )}
              </View>
            </View>
          );
        })}
      </ScrollView>
      <Button
        disabled={!canSubmit}
        disabledRaised
        raised
        containerStyle={styles.saveButtonContainer}
        isLoading={isLoading}
        onPress={submitTimer}>
        Save
      </Button>

      <BottomModal
        ref={bottomModalRef}
        renderContent={() => (
          <ModalContent
            key={`${curOption.key}_${new Date().getTime()}`}
            option={curOption}
            closeModal={() => bottomModalRef.current.close()}
            {...getOptionProps({
              option: curOption,
              filteredModes,
              selectedMode: curMode,
              enableAutoFan,
              enableLowFan,
              enableSilent,
              timer: curTimer,
              setParam,
            })}
          />
        )}
      />
    </SafeAreaView>
  );
};

EditWeeklyTimerScreen.propTypes = {
  timerList: PropTypes.array,
  thingName: PropTypes.string,
  selectedDays: PropTypes.array,
  setDaysValue: PropTypes.func,
  modes: PropTypes.object,
  enableSilent: PropTypes.bool,
  enableAutoFan: PropTypes.bool,
  enableLowFan: PropTypes.bool,
  setParam: PropTypes.func,
  removeTimer: PropTypes.func,
  addNewTimer: PropTypes.func,
  isNew: PropTypes.bool,
  submitTimer: PropTypes.func,
  success: PropTypes.string,
  error: PropTypes.string,
  isLoading: PropTypes.bool,
  navigation: PropTypes.object,
  clearStatus: PropTypes.func,
  unitName: PropTypes.string,
  canSubmit: PropTypes.bool,
  email: PropTypes.string,
};

EditWeeklyTimerScreen.navigationOptions = ({ navigation }) => {
  const { params } = navigation.state;

  if (params) {
    const { isNew } = params;

    return {
      title: isNew ? 'Create weekly timer' : 'Edit weekly timer',
    };
  }

  return params;
};

const mapStateToProps = state => {
  const {
    editTimer: {
      isNew,
      timerList,
      thingName,
      days: selectedDays,
      success,
      error,
      isLoading,
      canSubmit,
    },
    units: { allUnits },
    user: {
      profile: { email },
    },
  } = state;

  const { ACName, modes, enableSilent, enableAutoFan, enableLowFan } =
    allUnits[thingName] || {};

  return {
    isNew,
    timerList,
    thingName,
    selectedDays,
    modes,
    enableSilent,
    enableAutoFan,
    enableLowFan,
    success,
    error,
    isLoading,
    unitName: ACName,
    canSubmit,
    email,
  };
};

const mapDispatchToProps = dispatch => ({
  setParam: (key, value, timerIndex) => {
    dispatch(editTimer.setEditTimerParam({ key, value, timerIndex }));
  },
  setDaysValue: dayIndex => {
    dispatch(editTimer.setEditTimerDays(dayIndex));
  },
  addNewTimer: (modes, user) =>
    dispatch(editTimer.addEditTimerNewTimer({ modes, user })),
  submitTimer: () => dispatch(editTimer.submitTimerParams(false)),
  removeTimer: timerIndex =>
    dispatch(editTimer.removeEditTimerItem(timerIndex)),
  clearStatus: () => dispatch(editTimer.clearEditTimerStatus()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditWeeklyTimerScreen);
