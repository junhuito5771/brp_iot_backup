import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  RefreshControl,
} from 'react-native';
import { connect, selector, editTimer, units } from '@daikin-dama/redux-iot';
import { verticalScale, scale, NavigationService } from '@module/utility';
import PropTypes from 'prop-types';

import { Colors, DropDownAlertService } from '@module/acson-ui';
import { EDIT_WEEKLY_TIMER_SCREEN } from '../../constants/routeNames';

import WeeklyTimerContainer from '../../components/WeeklyTimer';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    paddingTop: verticalScale(15),
    paddingHorizontal: scale(15),
    flexGrow: 1,
  },
  groupHeader: {
    paddingVertical: verticalScale(10),
  },
  timerContainer: {
    marginBottom: verticalScale(15),
  },
  timerUnit: {
    borderColor: Colors.borderlightGray,
    borderWidth: 1,
    borderRadius: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.white,
    marginBottom: verticalScale(10),
  },
  flexRow: {
    flexDirection: 'row',
  },
  header: {
    paddingVertical: verticalScale(15),
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: Colors.borderlightGray,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  row: {
    paddingHorizontal: scale(15),
    paddingVertical: verticalScale(10),
  },
  hitslop: {
    top: 20,
    bottom: 20,
    left: 30,
    right: 30,
  },
  alignCenter: {
    alignItems: 'center',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  emptyTimer: {
    paddingVertical: scale(10),
    paddingHorizontal: verticalScale(25),
  },
  emptyTimerText: {
    color: Colors.borderlightGray,
  },
  timerItem: {
    justifyContent: 'space-around',
  },
  modeIcon: {
    marginRight: 5,
  },
  borderBottom: {
    borderBottomColor: Colors.borderlightGray,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  item: {
    justifyContent: 'space-around',
  },
});

const WeeklyTimerScreen = ({
  timerList,
  fetchTimerLoading,
  setEditTimerConfig,
  setEditTimerNewConfig,
  fetchUnitsAndTimers,
  email,
  route,
}) => {
  const handleOnPressEdit = (curThingName, curTimeList, editTimerIndex) => {
    if (curTimeList[editTimerIndex] && curTimeList[editTimerIndex].canEdit) {
      setEditTimerConfig(
        curThingName,
        curTimeList,
        curTimeList[editTimerIndex].day
      );
      NavigationService.navigate(EDIT_WEEKLY_TIMER_SCREEN);
    } else {
      DropDownAlertService.showErrorMsg({
        title: 'Unauthorized',
        description:
          'You do not have permission to overwrite the timer. Please contact your host user to request for permission',
      });
    }
  };

  const { dayIndex } = route.params;

  const handleOnPressAdd = (thingName, unit, day) => {
    if (unit.canCreate) {
      setEditTimerNewConfig(thingName, unit, day, email);
      NavigationService.navigate(EDIT_WEEKLY_TIMER_SCREEN);
    } else {
      DropDownAlertService.showErrorMsg({
        title: 'Maximum timers reached',
        description: 'Each unit can only create up to 42 timers',
      });
    }
  };

  return (
    <SafeAreaView style={styles.flex}>
      <ScrollView
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl
            refreshing={fetchTimerLoading}
            onRefresh={() => fetchUnitsAndTimers(email)}
          />
        }>
        {timerList.map((groupTimer, index) => {
          const key = `${groupTimer.groupName}_${index}`;

          return (
            <WeeklyTimerContainer
              key={key}
              {...groupTimer}
              hasSpaced={index < timerList.length - 1}
              onPressAdd={handleOnPressAdd}
              onPressEdit={handleOnPressEdit}
              dayIndex={dayIndex}
              userProfile={email}
            />
          );
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

WeeklyTimerScreen.propTypes = {
  timerList: PropTypes.array,
  fetchTimerLoading: PropTypes.bool,
  setEditTimerConfig: PropTypes.func,
  setEditTimerNewConfig: PropTypes.func,
  fetchUnitsAndTimers: PropTypes.func,
  email: PropTypes.string,
  route: PropTypes.object,
};

const momorizedMapStateToProps = () => {
  const getTimeListSelector = selector.getTimerList();
  const mapStateToProps = (state, props) => {
    const {
      timer: { isLoading: fetchTimerLoading },
      units: { allUnits },
      user: {
        profile: { email },
      },
    } = state;

    return {
      timerList: getTimeListSelector(state, props),
      fetchTimerLoading,
      allUnits,
      email,
    };
  };

  return mapStateToProps;
};

const mapDispatchToProps = dispatch => ({
  setEditTimerConfig: (thingName, timerList, dayIndex) => {
    dispatch(editTimer.setEditTimerConfig({ thingName, timerList, dayIndex }));
  },
  setEditTimerNewConfig: (thingName, unit, dayIndex, user) => {
    dispatch(
      editTimer.setEditTimerNewConfig({ thingName, unit, dayIndex, user })
    );
  },
  fetchUnitsAndTimers: email => {
    dispatch(units.getAllUnits({ email }));
  },
});

export default connect(
  momorizedMapStateToProps,
  mapDispatchToProps
)(WeeklyTimerScreen);
