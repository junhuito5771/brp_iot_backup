/* eslint react/prop-types: 0 */
import React, { useCallback } from 'react';
import {
  SafeAreaView,
  View,
  SectionList,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import { connect, selector, preferredUnits } from '@daikin-dama/redux-iot';
import { heightPercentage, widthPercentage } from '@module/utility';
import PropTypes from 'prop-types';
import { useFocusEffect } from '@react-navigation/native';

import {
  Text,
  Button,
  Checkbox,
  Colors,
  useHandleResponse,
} from '@module/acson-ui';
import { ALEXA_COMMAND_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  sectionHeader: {
    paddingVertical: heightPercentage(2.5),
    paddingHorizontal: widthPercentage(10),
    borderBottomColor: Colors.lightCoolGrey,
    borderBottomWidth: 0.5,
  },
  headerText: {
    fontWeight: '700',
  },
  lengthText: {
    fontWeight: '700',
    marginLeft: 5,
  },
  sectionItem: {
    borderBottomWidth: 0.5,
    borderBottomColor: Colors.lightCoolGrey,
  },
  sectionInnerItem: {
    justifyContent: 'space-between',
    paddingVertical: heightPercentage(2.5),
    paddingLeft: widthPercentage(13),
    paddingRight: widthPercentage(10),
  },
  buttonContainer: {
    marginHorizontal: widthPercentage(5),
    marginBottom: 10,
  },
  headerRight: {
    marginRight: 20,
  },
  hitSlop: {
    top: 10,
    right: 10,
    left: 10,
    bottom: 10,
  },
});

function PreferredUnitsScreen({
  units,
  toggleUnit,
  canSubmit,
  isLoading,
  error,
  success,
  submitPreferredUnits,
  clearStatus,
  alexaCommand,
  navigation,
  resetAlexaRefreshStatus,
}) {
  useFocusEffect(
    useCallback(() => {
      const handleHelpOnPress = () => {
        navigation.navigate(ALEXA_COMMAND_SCREEN, {
          title: alexaCommand.title,
          url: alexaCommand.url,
          incognito: alexaCommand.needRefresh,
          onLoadEnd: resetAlexaRefreshStatus,
        });
      };
      navigation.setOptions({
        headerRight: () => (
          <View style={styles.headerRight}>
            <TouchableOpacity
              hitSlop={styles.hitSlop}
              onPress={handleHelpOnPress}>
              <Image
                source={require('../../shared/images/general/help.png')}
                style={{ tintColor: Colors.linkGrey }}
              />
            </TouchableOpacity>
          </View>
        ),
      });
    }, [alexaCommand])
  );

  useHandleResponse({
    success,
    error,
    clearStatus,
  });

  return (
    <SafeAreaView style={styles.flex}>
      <SectionList
        contentContainerStyle={{ flexGrow: 1 }}
        sections={units}
        keyExtractor={item => item.thingName}
        renderItem={itemRow => {
          const { item } = itemRow;
          return (
            <View style={styles.sectionItem}>
              <TouchableOpacity onPress={() => toggleUnit(item.thingName)}>
                <View style={[styles.flexRow, styles.sectionInnerItem]}>
                  <Text medium numberOfLines={1}>
                    {item.unitName}
                  </Text>
                  <Checkbox round value={item.isAlexaPreferred} />
                </View>
              </TouchableOpacity>
            </View>
          );
        }}
        renderSectionHeader={({ section }) => {
          const { title, data = [] } = section;

          return (
            <View style={[styles.flexRow, styles.sectionHeader]}>
              <Text medium numberOfLines={1} style={styles.headerText}>
                {title}
              </Text>
              <Text small style={styles.lengthText}>
                ({data.length})
              </Text>
            </View>
          );
        }}
      />
      <View style={styles.buttonContainer}>
        <Button
          raised
          isLoading={isLoading}
          disabled={!canSubmit}
          disabledPrimary
          onPress={submitPreferredUnits}>
          Save
        </Button>
      </View>
    </SafeAreaView>
  );
}

const mapStateToProps = state => {
  const {
    preferredUnits: { canSubmit, isLoading, error, success, alexaCommand },
  } = state;
  return {
    units: selector.getAlexaPreferredUnits(state),
    canSubmit,
    isLoading,
    error,
    success,
    alexaCommand,
  };
};

const mapDispatchToProps = dispatch => ({
  toggleUnit: thingName => dispatch(preferredUnits.setToggleUnit(thingName)),
  submitPreferredUnits: () => dispatch(preferredUnits.submitAlexaPreferred()),
  clearStatus: () => dispatch(preferredUnits.clearStatus()),
  resetAlexaRefreshStatus: () =>
    dispatch(preferredUnits.resetAlexaCommandRefresh()),
});

PreferredUnitsScreen.propTypes = {
  units: PropTypes.array,
  toggleUnit: PropTypes.func,
  canSubmit: PropTypes.bool,
  isLoading: PropTypes.bool,
  success: PropTypes.string,
  error: PropTypes.string,
  submitPreferredUnits: PropTypes.func,
  clearStatus: PropTypes.func,
  alexaCommand: PropTypes.object,
  navigation: PropTypes.object,
  resetAlexaRefreshStatus: PropTypes.func,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PreferredUnitsScreen);
