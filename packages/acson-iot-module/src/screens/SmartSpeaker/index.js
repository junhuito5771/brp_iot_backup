import { useEffect } from 'react';
import { connect, preferredUnits } from '@daikin-dama/redux-iot';
import PropTypes from 'prop-types';

import {
  PREFERRED_UNITS_SCREEN,
  ALEXA_LINK_SCREEN,
} from '../../constants/routeNames';

function SmartSpeaker({
  isAlexaLinked,
  fetchAlexaActivateStatus,
  fetchAlexaCommandUrl,
  navigation,
}) {
  useEffect(() => {
    if (!isAlexaLinked) {
      fetchAlexaActivateStatus();
    }

    fetchAlexaCommandUrl();

    if (isAlexaLinked) {
      navigation.replace(PREFERRED_UNITS_SCREEN);
    } else {
      navigation.replace(ALEXA_LINK_SCREEN);
    }
  }, [isAlexaLinked]);

  return null;
}

const mapStateToProps = ({ preferredUnits: { alexaLinkStatus } }) => ({
  isAlexaLinked: alexaLinkStatus === 'activateSuccess',
});

const mapDispatchToProps = dispatch => ({
  fetchAlexaActivateStatus: () =>
    dispatch(preferredUnits.fetchAlexaActivateStatus()),
  fetchAlexaCommandUrl: () =>
    dispatch(preferredUnits.fetchAlexaCommandUrl(false)),
});

SmartSpeaker.propTypes = {
  isAlexaLinked: PropTypes.bool,
  fetchAlexaActivateStatus: PropTypes.func,
  fetchAlexaCommandUrl: PropTypes.func,
};

export default connect(mapStateToProps, mapDispatchToProps)(SmartSpeaker);
