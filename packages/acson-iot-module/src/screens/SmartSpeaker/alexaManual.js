import React from 'react';
import Pdf from 'react-native-pdf';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import Config from 'react-native-config';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  pdf: {
    height: '100%',
    width: '100%',
  },
});

const uri = { uri: Config.ALEXA_MANUAL, cache: true };

const AlexaManualScreen = () => (
  <View style={styles.container}>
    <Pdf
      style={styles.pdf}
      source={uri}
      activityIndicator={<ActivityIndicator size="large" />}
    />
  </View>
);

export default AlexaManualScreen;
