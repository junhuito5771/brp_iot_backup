import React, { useState, useCallback } from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';
import WebView from 'react-native-webview';
import Config from 'react-native-config';
import { connect, preferredUnits } from '@daikin-dama/redux-iot';
import { URLHelper } from '@module/utility';
import PropTypes from 'prop-types';
import { useFocusEffect } from '@react-navigation/native';

import { Colors, Text } from '@module/acson-ui';
import { ALEXA_AUTH_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  loadingCenter: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  actProgressContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actProgressText: {
    marginTop: 5,
  },
});

function getAlexaLWALink() {
  const baseUrl = Config.ALEXA_LWA_LINK;
  const clientId = Config.ALEXA_CLIENT_ID;
  const scope = 'alexa::skills:account_linking';
  const responseType = 'code';
  const redirectUrl = `https://${Config.UNI_LINK}`;
  const state = 'development';

  return (
    `https://${baseUrl}?` +
    `client_id=${clientId}` +
    `&scope=${scope}` +
    `&response_type=${responseType}` +
    `&redirect_uri=${redirectUrl}` +
    `&state=${state}`
  );
}

const AlexaAuthScreen = ({
  fetchAlexaToken,
  setAlexaAuthPending,
  alexaLinkStatus,
  navigation,
}) => {
  const [activeProgress, setActiveProgress] = useState(false);

  useFocusEffect(
    useCallback(() => {
      if (alexaLinkStatus && alexaLinkStatus === 'tokenSuccess') {
        setAlexaAuthPending();
        navigation.replace(ALEXA_AUTH_SCREEN);
      }
    }, [alexaLinkStatus])
  );

  const renderLoading = useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  const onNavigationChanged = state => {
    if (state && state.url && state.url.indexOf('code=') !== -1) {
      const code = URLHelper.getUrlParam(state.url, 'code');
      if (code) {
        setActiveProgress(true);
        fetchAlexaToken({
          code,
          clientId: Config.ALEXA_CLIENT_ID,
          clientSecret: Config.ALEXA_CLIENT_SECRET,
          tokenUrl: Config.ALEXA_TOKEN_URL,
          redirectUrl: Config.UNI_LINK,
          skillStage: Config.ALEXA_SKILL_STAGE,
        });
      }
    }
  };

  return (
    <>
      {activeProgress ? (
        <View style={styles.actProgressContainer}>
          <ActivityIndicator size="large" color={Colors.azureRad} />
          <Text medium style={styles.actProgressText}>
            Processing your request
          </Text>
        </View>
      ) : (
        <WebView
          startInLoadingState
          renderLoading={renderLoading}
          onNavigationStateChange={onNavigationChanged}
          source={{
            uri: getAlexaLWALink(),
          }}
        />
      )}
    </>
  );
};

AlexaAuthScreen.propTypes = {
  fetchAlexaToken: PropTypes.func,
  alexaLinkStatus: PropTypes.string,
  setAlexaAuthPending: PropTypes.func,
  navigation: PropTypes.object,
};

const mapStateToProps = ({ preferredUnits: preferredUnitsState }) => ({
  alexaLinkStatus: preferredUnitsState.alexaLinkStatus,
});

const mapDispatchToProps = dispatch => ({
  fetchAlexaToken: params => dispatch(preferredUnits.fetchAlexaToken(params)),
  setAlexaAuthPending: () =>
    dispatch(
      preferredUnits.setAlexaTokenStatus({
        alexaLinkStatus: 'authPending',
      })
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AlexaAuthScreen);
