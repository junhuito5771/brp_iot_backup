import React, { useCallback } from 'react';
import {
  SafeAreaView,
  View,
  Image,
  Linking,
  StyleSheet,
  NativeModules,
  Platform,
  TouchableOpacity,
} from 'react-native';
import Config from 'react-native-config';
import { ScaleText, URLHelper, NavigationService } from '@module/utility';
import { connect, preferredUnits } from '@daikin-dama/redux-iot';
import { useFocusEffect, useNavigation } from '@react-navigation/native';

import PropTypes from 'prop-types';

import { Text, Button, Colors } from '@module/acson-ui';

import {
  ALEXA_AUTH_SCREEN,
  ALEXA_LWA_SCREEN,
  ALEXA_MANUAL_SCREEN,
} from '../../constants/routeNames';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  top: {
    flex: 0.9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  appNameText: {
    marginTop: 10,
    fontSize: ScaleText(18),
  },
  desc: {
    marginTop: 30,
    marginHorizontal: 20,
  },
  descText: {
    fontSize: ScaleText(14),
    textAlign: 'center',
  },
  bottom: {
    flex: 0.1,
    justifyContent: 'center',
    marginHorizontal: 20,
    marginVertical: 10,
  },
  alexaAppIcon: {
    width: 80,
    height: 80,
  },
  headerRight: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 20,
  },
  hitSlop: {
    top: 10,
    right: 10,
    left: 10,
    bottom: 10,
  },
});

function getAlexaAppLink() {
  const baseUrl = Config.ALEXA_APP_LINK;
  const clientId = Config.ALEXA_CLIENT_ID;
  const scope = 'alexa::skills:account_linking';
  const skillStage = Config.ALEXA_SKILL_STAGE;
  const responseType = 'code';
  const redirectUrl = `https://${Config.UNI_LINK}`;
  const state = 'development';

  return (
    `https://${baseUrl}` +
    `&client_id=${clientId}` +
    `&scope=${scope}` +
    `&skill_stage=${skillStage}` +
    `&response_type=${responseType}` +
    `&redirect_uri=${redirectUrl}` +
    `&state=${state}`
  );
}

function checkIsAlexaInstalledInAndroid() {
  return NativeModules.PkgManagerModule.isAppInstalled(
    'com.amazon.dee.app',
    866607211
  );
}

function checkIsAlexaInstalledInIos() {
  return Linking.canOpenURL('alexa://open');
}

async function getIsAlexaInstalled() {
  let isInstalled;
  try {
    isInstalled = await Platform.select({
      ios: checkIsAlexaInstalledInIos,
      android: checkIsAlexaInstalledInAndroid,
    })();
  } catch (error) {
    isInstalled = false;
  }

  return isInstalled;
}

async function handleOnPressLink() {
  const isAlexaInstalled = await getIsAlexaInstalled();

  if (isAlexaInstalled) {
    Linking.openURL(getAlexaAppLink());
  } else {
    NavigationService.navigate(ALEXA_LWA_SCREEN);
  }
}

function AlexaLinkScreen({
  isLoading,
  fetchAlexaToken,
  alexaLinkStatus,
  setAlexaAuthPending,
}) {
  const navigation = useNavigation();

  useFocusEffect(
    useCallback(() => {
      navigation.setOptions({
        title: <Text>Alexa</Text>,
        headerRight: () => (
          <View style={styles.headerRight}>
            <TouchableOpacity
              hitSlop={styles.hitSlop}
              onPress={() => navigation.navigate(ALEXA_MANUAL_SCREEN)}
              style={styles.marginRight}>
              <Image
                source={require('../../shared/images/general/infoGrey.png')}
                style={{ tintColor: Colors.linkGrey }}
              />
            </TouchableOpacity>
          </View>
        ),
      });
    }, [navigation])
  );
  useFocusEffect(
    useCallback(() => {
      const onReceiveUrl = ({ url }) => {
        const code = URLHelper.getUrlParam(url, 'code');
        if (code) {
          fetchAlexaToken({
            code,
            clientId: Config.ALEXA_CLIENT_ID,
            clientSecret: Config.ALEXA_CLIENT_SECRET,
            tokenUrl: Config.ALEXA_TOKEN_URL,
            redirectUrl: Config.UNI_LINK,
            skillStage: Config.ALEXA_SKILL_STAGE,
          });
        }
      };
      Linking.addEventListener('url', onReceiveUrl);

      return () => {
        Linking.removeEventListener('url', onReceiveUrl);
      };
    }, [])
  );

  useFocusEffect(
    useCallback(() => {
      if (alexaLinkStatus && alexaLinkStatus === 'tokenSuccess') {
        setAlexaAuthPending();
        NavigationService.navigate(ALEXA_AUTH_SCREEN);
      }
    }, [alexaLinkStatus])
  );

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.top}>
        <Image
          style={styles.alexaAppIcon}
          source={require('../../shared/images/alexa/alexaAppIcon.png')}
          resizeMode="contain"
        />
        <View style={styles.desc}>
          <Text style={styles.descText}>
            You can use your voice to control Acson units by linking to Amazon
            smart speakers using Alexa
          </Text>
        </View>
      </View>
      <View style={styles.bottom}>
        <Button raised isLoading={isLoading} onPress={handleOnPressLink}>
          Link with Alexa
        </Button>
      </View>
    </SafeAreaView>
  );
}

AlexaLinkScreen.propTypes = {
  isLoading: PropTypes.bool,
  fetchAlexaToken: PropTypes.func,
  alexaLinkStatus: PropTypes.string,
  setAlexaAuthPending: PropTypes.func,
};

const mapStateToProps = ({ preferredUnits: preferredUnitsState }) => ({
  isLoading: preferredUnitsState.isLoading,
  alexaLinkStatus: preferredUnitsState.alexaLinkStatus,
});

const mapDispatchToProps = dispatch => ({
  fetchAlexaToken: params => dispatch(preferredUnits.fetchAlexaToken(params)),
  setAlexaAuthPending: () =>
    dispatch(
      preferredUnits.setAlexaTokenStatus({
        alexaLinkStatus: 'authPending',
      })
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AlexaLinkScreen);
