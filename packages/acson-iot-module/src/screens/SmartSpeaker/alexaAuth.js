import React, { useState, useEffect, useCallback } from 'react';
import { ActivityIndicator, Alert, StyleSheet, View } from 'react-native';
import WebView from 'react-native-webview';
import Config from 'react-native-config';
import { connect, preferredUnits } from '@daikin-dama/redux-iot';
import PropTypes from 'prop-types';
import { useNavigation } from '@react-navigation/native';

import { Colors, Text } from '@module/acson-ui';
import { PREFERRED_UNITS_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  loadingCenter: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  actProgressContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  actProgressText: {
    marginTop: 5,
  },
});

function getUrlVars(url) {
  const vars = {};
  url.replace(/[?&]+([^=&]+)=([^&]*)/gi, (m, key, value) => {
    vars[key] = value;
  });
  return vars;
}

function getUrlParam(url, paramName, defaultValue = '') {
  const value = getUrlVars(url)[paramName];

  return value || defaultValue;
}

const AlexaAuthScreen = ({ alexaLinkStatus, activateSkill }) => {
  const [activeProgress, setActiveProgress] = useState(false);
  const navigation = useNavigation();

  useEffect(() => {
    if (alexaLinkStatus && alexaLinkStatus === 'activateSuccess') {
      Alert.alert(
        'Alexa link success',
        "You've successfully link Daikin units to Alexa",
        [
          {
            text: 'OK',
            onPress: () => {
              navigation.reset({
                index: 0,
                routes: [{ name: PREFERRED_UNITS_SCREEN }],
              });
            },
          },
        ]
      );
    }
  }, [alexaLinkStatus]);

  const renderLoading = useCallback(
    () => (
      <View style={styles.loadingCenter}>
        <ActivityIndicator size="large" color={Colors.azureRad} />
      </View>
    ),
    []
  );

  const onNavigationChanged = state => {
    if (state && state.url && state.url.indexOf('code=') !== -1) {
      const code = getUrlParam(state.url, 'code');
      if (!activeProgress) {
        setActiveProgress(true);
        activateSkill({
          code,
          skillStage: Config.ALEXA_SKILL_STAGE,
          redirectUrl: Config.UNI_LINK,
          skillEnaUrl: Config.ALEXA_SKILL_ENA_URL,
        });
      }
    }
  };

  return (
    <>
      {activeProgress ? (
        <View style={styles.actProgressContainer}>
          <ActivityIndicator size="large" color={Colors.azureRad} />
          <Text medium style={styles.actProgressText}>
            Activating the skill
          </Text>
        </View>
      ) : (
        <WebView
          startInLoadingState
          renderLoading={renderLoading}
          onNavigationStateChange={onNavigationChanged}
          source={{
            uri: `https://${Config.COGNITO_LOGIN_URL}`,
          }}
        />
      )}
    </>
  );
};

AlexaAuthScreen.propTypes = {
  alexaLinkStatus: PropTypes.string,
  activateSkill: PropTypes.func,
};

const mapStateToProps = ({ preferredUnits: preferredUnitsState }) => ({
  alexaLinkStatus: preferredUnitsState.alexaLinkStatus,
});

const mapDispatchToProps = dispatch => ({
  activateSkill: params => dispatch(preferredUnits.activateAlexaSkill(params)),
  setActivatePending: () =>
    dispatch(
      preferredUnits.setAlexaTokenStatus({
        alexaLinkStatus: 'activatePending',
      })
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(AlexaAuthScreen);
