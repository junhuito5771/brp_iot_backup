import React from 'react';
import { connect, login, userProfile } from '@daikin-dama/redux-iot';
import { Platform, Linking, Alert } from 'react-native';
import PropTypes from 'prop-types';
// eslint-disable-next-line import/no-unresolved
import { content } from '@daikin-dama/redux-iot-marketing';
import {
  StoreHelper,
  LocationHelper,
  NavigationService,
} from '@module/utility';
import Config from 'react-native-config';

import { ModalService } from '@module/acson-ui';
// eslint-disable-next-line import/named
import { HOME_SCREEN, HOME_MARKETING } from '../constants/routeNames';

const promptAlertIfUpdateNeeded = async appID => {
  if (await StoreHelper.needUpdate()) {
    const storeURL = await StoreHelper.getStoreURL(appID);

    const latestVersion = await StoreHelper.getLatestVersion();

    const storeName = Platform.select({
      ios: 'AppStore',
      android: 'PlayStore',
    });
    ModalService.show({
      title: 'Update Available',
      desc: `We've become better! Kindly update to version ${latestVersion} to enjoy a better experience of the app.`,
      buttons: [
        {
          title: `Go to ${storeName}`,
          onPress: () => Linking.openURL(storeURL),
        },
      ],
      cancelable: false,
    });
  }
};

const MainScreen = ({
  initConfigAutoLoginIfCan,
  initContentHelper,
  logOut,
  success,
  error,
  alertTitle,
  clearLoginStatus,
  canAutoLogin,
}) => {
  React.useEffect(() => {
    if (!canAutoLogin) {
      NavigationService.navigate(HOME_MARKETING);
    } else if (error) {
      Alert.alert(alertTitle, error);
      logOut();
      NavigationService.navigate(HOME_MARKETING);
    } else if (success) {
      NavigationService.navigate(HOME_SCREEN);
    }

    clearLoginStatus();
  }, [success, error, canAutoLogin]);

  React.useEffect(() => {
    const appID = Config.APP_STORE_ID;
    if (appID && !__DEV__) {
      promptAlertIfUpdateNeeded(appID);
    }

    initConfigAutoLoginIfCan({
      apiEndpoint: Config.API_URL,
      cognitoEndpoint: Config.COGNITO_URL,
      cognitoClientId: Config.COGNITO_CLIENT_ID,
      cognitoIdentifyPoolId: Config.COGNITO_POOL_ID,
      cognitoUserPoolID: Config.COGNITO_USER_POOL_ID,
      region: Config.REGION,
      iotEndpoint: Config.IOT_ENDPOINT,
      alexaClientId: Config.ALEXA_CLIENT_ID,
      alexaClientSecret: Config.ALEXA_CLIENT_SECRET,
      alexaSkillEnaUrl: Config.ALEXA_SKILL_ENA_URL,
      alexaTokenUrl: Config.ALEXA_TOKEN_URL,
    });

    initContentHelper({
      bucketUrl: Config.BUCKET_URL,
      bucketEndpoint: Config.BUCKET_ENDPOINT,
    });

    LocationHelper.syncGeocode({
      endpoint: Config.GEOCODE_URL,
      key: Config.GEOCODE_KEY,
    });
  }, []);

  return null;
};

const mapStateToProps = ({ login: loginState, user }) => {
  const { success, error, alertTitle } = loginState;
  const { isAutoLogin, hasRefreshToken } = user;

  return {
    success,
    error,
    alertTitle,
    canAutoLogin: isAutoLogin && hasRefreshToken,
  };
};

const mapDispatchToProps = dispatch => ({
  initConfigAutoLoginIfCan: config => {
    dispatch(login.autoLogin(config));
  },
  initContentHelper: config => {
    dispatch(content.init(config));
  },
  clearUserStatus: () => {
    dispatch(userProfile.clearUserStatus());
  },
  logOut: () => {
    dispatch(userProfile.logOut());
  },
  clearLoginStatus: () => {
    dispatch(login.clearLoginStatus());
  },
});

MainScreen.propTypes = {
  initConfigAutoLoginIfCan: PropTypes.func,
  initContentHelper: PropTypes.func,
  clearLoginStatus: PropTypes.func,
  logOut: PropTypes.func,
  success: PropTypes.string,
  error: PropTypes.string,
  alertTitle: PropTypes.string,
  canAutoLogin: PropTypes.bool,
};

export default connect(mapStateToProps, mapDispatchToProps)(MainScreen);
