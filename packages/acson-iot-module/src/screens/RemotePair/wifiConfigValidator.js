import { ValidatorHelper } from '@module/utility';

const {
  hasFieldValue,
  isNotExceedLength,
  isValidLength,
  addError,
} = ValidatorHelper;

const wifiConfigValidator = values => {
  const errors = {};

  const { ssid, password } = values;

  addError(
    errors,
    'ssid',
    [hasFieldValue(ssid), 'SSID is required'],
    [isNotExceedLength(ssid, 31), 'SSID cannot exceed 31 characters']
  );

  addError(
    errors,
    'password',
    [hasFieldValue(password), 'Password is required'],
    [isValidLength(password, 8), 'Password must be at least 8 characters']
  );

  return errors;
};

export default wifiConfigValidator;
