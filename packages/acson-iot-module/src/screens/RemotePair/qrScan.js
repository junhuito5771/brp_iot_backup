import React, { useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Alert,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import SplashScreen from 'react-native-splash-screen';
import Toast from 'react-native-root-toast';
import {
  NetworkHelper,
  viewportWidth,
  viewportHeight,
  NavigationService,
} from '@module/utility';
import { connect, bindGuestDevice, QRCodeHelper } from '@daikin-dama/redux-iot';
import Config from 'react-native-config';

import { Loading, Colors } from '@module/acson-ui';

import {
  WIFI_CONFIG_SCREEN,
  HOME_SCREEN,
  BIND_UNIT_NO_QR_SCREEN,
} from '../../constants/routeNames';

let CameraComponent = null;

// Dynamic import the library based on provider
if (Config.PROVIDER === 'HMS') {
  CameraComponent = require('@module/react-native-hms-camera').default;
} else {
  CameraComponent = require('react-native-camera').RNCamera;
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  frameContainer: {
    flexDirection: 'row',
    width: viewportWidth,
  },
  cameraStyle: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  overlayContainer: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  topOverlay: {
    flex: 1,
    backgroundColor: Colors.transparentBlack,
  },
  bottomOverlay: {
    flex: 1,
    backgroundColor: Colors.transparentBlack,
    alignItems: 'center',
  },
  leftAndRightOverlay: {
    flex: 1,
    backgroundColor: Colors.transparentBlack,
  },
  text: {
    fontSize: 12,
    color: 'white',
    marginTop: 10,
  },
  moreOptText: {
    fontSize: 12,
    color: Colors.white,
    marginTop: 15,
    textDecorationLine: 'underline',
  },
});

const showToast = (message, config) => {
  let toast = Toast.show(message, {
    duration: Toast.durations.LONG,
    position: Toast.positions.BOTTOM,
    shadow: false,
    animation: true,
    hideOnPress: true,
    delay: 0,
    onHidden: () => {
      toast.destroy();
      toast = null;
    },
    ...config,
  });
};

const QrScan = ({
  isLoading,
  error,
  success,
  submitBindGuestDevice,
  clearBindGuestStatus,
  navigation,
}) => {
  useEffect(() => {
    SplashScreen.hide();
    clearBindGuestStatus();

    return clearBindGuestStatus;
  }, []);

  useEffect(() => {
    if (success) {
      NavigationService.navigate(HOME_SCREEN);
    } else if (error) {
      Alert.alert('Error', error, [
        { text: 'OK', onPress: clearBindGuestStatus },
      ]);
    }
  }, [success, error]);

  const checkQrcodeAuth = item => {
    const { factoryKey } = item;
    const itemCode = factoryKey.slice(0, 1);

    return itemCode === 'A';
  };

  const handleQrScan = async data => {
    if (!navigation.isFocused() || isLoading || !!error || !!success) {
      return false;
    }

    const processQRResult = QRCodeHelper.QRHandler(
      data,
      Config.QRCODE_SECRET_KEY,
      Config.QRCODE_IV
    );

    const {
      type,
      error: QRResultError,
      data: qrData,
      version,
    } = processQRResult;

    if (type === 'host' && (version === 3 || version === 2)) {
      const itemCode = checkQrcodeAuth(qrData);
      if (!itemCode) {
        showToast('Invalid QR Code');
        return false;
      }
    }

    if (QRResultError) {
      showToast(QRResultError || 'Invalid QR Code');
      return false;
    }

    if (qrData.unitCode && qrData.unitCode !== 'A') {
      showToast('Invalid QR Code');
      return false;
    }

    if (type === 'guest' && qrData.unitCode === 'A') {
      submitBindGuestDevice(qrData, version);
      return true;
    }

    // Only navigate to wifi config if thingName is not empty
    if (qrData && qrData.thingName && navigation.isFocused()) {
      NavigationService.navigate(WIFI_CONFIG_SCREEN, {
        ...qrData,
        connectedSSID: await NetworkHelper.getSSID(),
      });
    }
    return true;
  };

  const navigateToMoreOpt = async () => {
    NavigationService.navigate(BIND_UNIT_NO_QR_SCREEN, {
      connectedSSID: await NetworkHelper.getSSID(),
    });
  };

  return (
    <View style={styles.mainContainer}>
      <Loading visible={isLoading}>Binding unit to your account...</Loading>
      <CameraComponent
        style={styles.cameraStyle}
        captureAudio={false}
        barCodeTypes={[
          CameraComponent.Constants.BarCodeType.qr,
          CameraComponent.Constants.BarCodeType.datamatrix,
        ]}
        cameraViewDimensions={{ width: viewportWidth, height: viewportHeight }}
        rectOfInterest={{ x: 0.27, y: 0.125, width: 0.45, height: 0.75 }}
        onBarCodeRead={
          navigation.isFocused() ? e => handleQrScan(e.data) : null
        }
        autoFocus
        autoFocusPointOfInterest={{ x: 0.5, y: 0.5 }}>
        <View style={styles.overlayContainer}>
          <View style={styles.topOverlay} />
          <View style={styles.frameContainer}>
            <View style={styles.leftAndRightOverlay} />
            <Image source={require('../../shared/images/general/frame.png')} />
            <View style={styles.leftAndRightOverlay} />
          </View>
          <View style={styles.bottomOverlay}>
            <Text style={styles.text}>
              Position the QR Code within the frame to scan
            </Text>
            <TouchableOpacity onPress={navigateToMoreOpt}>
              <Text style={styles.moreOptText}>More options</Text>
            </TouchableOpacity>
          </View>
        </View>
      </CameraComponent>
    </View>
  );
};

QrScan.propTypes = {
  success: PropTypes.string,
  error: PropTypes.string,
  isLoading: PropTypes.bool,
  submitBindGuestDevice: PropTypes.func,
  clearBindGuestStatus: PropTypes.func,
  navigation: PropTypes.object,
};

const mapStateToProps = ({
  bindGuestDevice: { isLoading, success, error },
}) => ({
  isLoading,
  success,
  error,
});

const mapDispatchToProps = dispatch => ({
  submitBindGuestDevice: (bindGuestDeviceParams, version) => {
    dispatch(
      bindGuestDevice.submitBindGuestDevice(bindGuestDeviceParams, version)
    );
  },
  clearBindGuestStatus: () => {
    dispatch(bindGuestDevice.clearBindGuestStatus());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(QrScan);
