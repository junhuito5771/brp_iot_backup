import React from 'react';
import PropTypes from 'prop-types';
import { connect, deviceConfig } from '@daikin-dama/redux-iot';
import { heightPercentage } from '@module/utility';
import {
  StyleSheet,
  View,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import { Colors, Text } from '@module/acson-ui';

const styles = StyleSheet.create({
  center: {
    flex: 1,
    alignItems: 'center',
  },
  rowContainer: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingHorizontal: 25,
    height: heightPercentage(8),
  },
  touchable: {
    height: '100%',
    width: '100%',
  },
  row: {
    flexDirection: 'row',
  },
  groupLabel: {
    flexGrow: 1,
  },
  icon: {
    height: 17,
    width: 17,
    resizeMode: 'contain',
    justifyContent: 'flex-end',
  },
});

const SelectGroup = ({ navigation, groups, setSelectedGroup, route }) => {
  const { currentValue } = route.params;

  const groupsArray = React.useMemo(
    () =>
      Object.values(groups)
        .filter(group => group.groupIndex >= 0)
        .map(group => ({
          name: group.groupName,
        })),
    [groups]
  );

  const handleSetSelectedGroup = React.useCallback(
    groupName => {
      setSelectedGroup(groupName);
      navigation.goBack();
    },
    [setSelectedGroup]
  );

  return (
    <ScrollView>
      {groupsArray.map(group => (
        <View key={group.name} style={[styles.rowContainer]}>
          <TouchableOpacity
            onPress={() => handleSetSelectedGroup(group.name)}
            style={styles.touchable}
            hitSlop={{
              top: 10,
              bottom: 10,
            }}>
            <View style={[styles.row, styles.center]}>
              <Text style={styles.groupLabel}>{group.name}</Text>
              {currentValue === group.name && (
                <Image
                  source={require('../../shared/images/general/tick.png')}
                  style={styles.icon}
                />
              )}
            </View>
          </TouchableOpacity>
        </View>
      ))}
    </ScrollView>
  );
};

const mapStateToProps = state => ({
  groups: state.units.groupInfo.groups,
});

const mapDispatchToProps = dispatch => ({
  setSelectedGroup: selectedGroup =>
    dispatch(deviceConfig.setSelectedGroup(selectedGroup)),
});

SelectGroup.propTypes = {
  setSelectedGroup: PropTypes.func,
  navigation: PropTypes.object,
  groups: PropTypes.object,
  route: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectGroup);
