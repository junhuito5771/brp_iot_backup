import React, { createRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Keyboard,
  TouchableWithoutFeedback,
  SafeAreaView,
  StyleSheet,
} from 'react-native';
import {
  heightPercentage,
  widthPercentage,
  generalInputPreValidator,
  NavigationService,
} from '@module/utility';
import { withFormik } from 'formik';
import { connect, deviceConfig } from '@daikin-dama/redux-iot';

import {
  FormikTextInput,
  FormikButton,
  Text,
  FormikSelector,
} from '@module/acson-ui';
import deviceConfigValidator from './deviceConfigValidator';
import { HOME_SCREEN, SELECT_GROUP_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    flex: 1,
    paddingTop: heightPercentage(5),
    paddingHorizontal: widthPercentage(5),
  },
  marginTop: {
    marginTop: 50,
  },
  text: {
    fontSize: 20,
    fontWeight: '700',
  },
  topText: {
    marginTop: heightPercentage(7),
  },
  marginBottom: {
    marginBottom: heightPercentage(5),
  },
});

const DeviceConfig = ({
  clearDeviceConfigStatus,
  success,
  error,
  isLoading,
  selectedGroup,
  setSelectedGroup,
}) => {
  const groupInputRef = createRef(null);
  const [groupInput, setGroupInput] = useState(null);

  useEffect(() => {
    if (success) {
      NavigationService.navigate(HOME_SCREEN);
    }

    return clearDeviceConfigStatus;
  }, [success, error]);

  const onChangeText = React.useCallback(
    text => {
      if (selectedGroup) {
        setSelectedGroup(null);
      }
      setGroupInput(text);
    },
    [selectedGroup, groupInput]
  );

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={styles.flex}>
        <View style={styles.container}>
          <Text style={[styles.topText, styles.text]}>Almost there!</Text>
          <Text style={[styles.marginBottom, styles.text]}>
            Set up your device
          </Text>
          <FormikTextInput
            name="name"
            label="Give it a Name"
            validator={generalInputPreValidator}
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            autoCapitalize="none"
            onSubmitEditing={() => groupInputRef.current.focus()}
          />
          <FormikSelector
            name="group"
            label="Where it is"
            validator={generalInputPreValidator}
            textContentType="none"
            keyboardType="default"
            returnKeyType="next"
            hasSpaced
            forwardRef={groupInputRef}
            autoCapitalize="none"
            submitOnEditEnd
            onSelectorPress={() =>
              NavigationService.navigate(SELECT_GROUP_SCREEN, {
                currentValue: selectedGroup || groupInput,
              })
            }
            onChangeText={onChangeText}
            selectedValue={selectedGroup}
          />
          <FormikButton raised isLoading={isLoading} style={styles.marginTop}>
            Save
          </FormikButton>
        </View>
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

const formikDeviceConfig = withFormik({
  validate: deviceConfigValidator,
  mapPropsToValues: ({ name, group }) => ({ name, group }),
  handleSubmit: (values, formikBag) => {
    const deviceConfigParams = {
      deviceName: values.name,
      deviceGroup: values.group,
      thingName: formikBag.props.route.params.thingName,
      thingType: formikBag.props.route.params.thingType,
      randomKey: formikBag.props.route.params.randomKey,
    };
    formikBag.props.submitDeviceConfigParams({
      ...deviceConfigParams,
      logo: '1.png',
    });
  },
})(DeviceConfig);

DeviceConfig.propTypes = {
  error: PropTypes.string,
  success: PropTypes.string,
  clearDeviceConfigStatus: PropTypes.func,
  isLoading: PropTypes.bool,
  selectedGroup: PropTypes.string,
  setSelectedGroup: PropTypes.func,
};

const mapStateToProps = state => ({
  error: state.deviceConfig.error,
  success: state.deviceConfig.success,
  isLoading: state.deviceConfig.isLoading,

  selectedGroup: state.deviceConfig.selectedGroup,
});

const mapDispatchToProps = dispatch => ({
  submitDeviceConfigParams: deviceConfigParams => {
    dispatch(deviceConfig.submitDeviceConfigParams(deviceConfigParams));
  },
  clearDeviceConfigStatus: () => {
    dispatch(deviceConfig.clearDeviceConfigStatus());
  },
  setSelectedGroup: selectedGroup => {
    dispatch(deviceConfig.setSelectedGroup(selectedGroup));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(formikDeviceConfig);
