import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  RefreshControl,
} from 'react-native';
import PropTypes from 'prop-types';
import { connect, bleControl, BLEService } from '@daikin-dama/redux-iot';
import { ConnectionType, NavigationService } from '@module/utility';

import {
  CollapsibleContainer,
  Loading,
  useHandleResponse,
  ModalService,
} from '@module/acson-ui';

import { OTHER_MODE_REMOTE_SCREEN } from '../constants/routeNames';

const { discoverAndProcessUnits } = BLEService;

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  container: {
    paddingHorizontal: 25,
    marginTop: 25,
  },
});

const BluetoothModeScreen = ({
  units,
  connectUnit,
  clearStatus,
  isLoading,
  pairLoading,
  pairSuccess,
  pairError,
  connectedUnitName,
  bleManager,
  dispatch,
}) => {
  const handleNavigateToBleMode = () => {
    NavigationService.navigate(OTHER_MODE_REMOTE_SCREEN, {
      unit: { ACName: connectedUnitName },
      mode: ConnectionType.BLE_MDOE,
    });
  };

  const handleOnPressItem = ({ thingName, disallowed }) => {
    if (disallowed) {
      ModalService.show({
        title: 'Unauthorized unit',
        desc:
          'You do not have permission to access this unit. Please log in to the account paired with this unit first.',
      });
    } else {
      connectUnit(thingName);
    }
  };

  const handleOnRefresh = () => {
    discoverAndProcessUnits(dispatch, bleManager, isLoading);
  };

  useHandleResponse({
    success: pairSuccess,
    error: pairError,
    onSuccess: handleNavigateToBleMode,
    clearStatus,
  });

  return (
    <SafeAreaView style={styles.flex}>
      <Loading visible={!!pairLoading}>{pairLoading}</Loading>
      <ScrollView
        contentContainerStyle={styles.container}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={handleOnRefresh} />
        }>
        <CollapsibleContainer data={units} onPressItem={handleOnPressItem} />
      </ScrollView>
    </SafeAreaView>
  );
};

BluetoothModeScreen.propTypes = {
  units: PropTypes.array,
  connectedUnitName: PropTypes.string,
  clearStatus: PropTypes.func,
  isLoading: PropTypes.bool,
  pairLoading: PropTypes.string,
  pairSuccess: PropTypes.string,
  pairError: PropTypes.string,
  bleManager: PropTypes.object,
  dispatch: PropTypes.func,
  connectUnit: PropTypes.func,
};

const mapStateToProps = state => ({
  units: Object.values(state.bleMode.units),
  isLoading: state.bleMode.isLoading,
  pairLoading: state.bleControl.pairLoading,
  pairSuccess: state.bleControl.success,
  pairError: state.bleControl.error,
  lastUpdated: state.bleMode.lastUpdated,
  connectedUnitName: state.units.allUnits[state.remote.thingName]
    ? state.units.allUnits[state.remote.thingName].ACName
    : '',
});

const mapDispatchToProps = dispatch => ({
  connectUnit: thingName => {
    dispatch(bleControl.connectBLEUnit({ thingName }));
  },
  clearStatus: () => dispatch(bleControl.clearBLEControlStatus()),
  dispatch,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BluetoothModeScreen);
