import React from 'react';
import PropTypes from 'prop-types';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { Colors, ModalService } from '@module/acson-ui';
// eslint-disable-next-line import/no-unresolved
import { damaErrorCodes } from '@module/redux-marketing';
import { scale, verticalScale } from '@module/utility';

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.white,
  },
  searchContainer: {
    backgroundColor: Colors.lightGrey,
    marginHorizontal: scale(25),
    marginTop: verticalScale(20),
    borderRadius: scale(20),
  },
  searchInput: {
    paddingHorizontal: scale(20),
  },
  searchMargin: {
    marginHorizontal: scale(15),
    marginVertical: scale(10),
  },
  flex: {
    flex: 1,
  },
  descriptionFlex: {
    flex: 5,
  },
  row: {
    flexDirection: 'row',
  },
  errorText: {
    color: Colors.darkGrey,
    fontWeight: '500',
    paddingVertical: verticalScale(20),
  },
  textBold: {
    fontWeight: '700',
  },
  descriptionText: {
    paddingHorizontal: scale(25),
  },
  separator: {
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightGrey,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
  headerIcon: {
    marginHorizontal: scale(10),
  },
});

const ErrorCodes = ({ navigation }) => {
  const [isSearchMode, setIsSearchMode] = React.useState(false);
  const [searchInput, setSearchInput] = React.useState('');

  useFocusEffect(
    React.useCallback(() => {
      navigation.setOptions({
        title: <Text>Error Code</Text>,
      });
    }, [navigation])
  );

  React.useEffect(() => {
    navigation.setParams({
      isSearchMode,
      setIsSearchMode,
    });
  }, [isSearchMode, setIsSearchMode]);

  const filteredCodes = damaErrorCodes.filter(({ code }) =>
    code.includes(searchInput)
  );

  const handleSearchInput = React.useCallback(
    input => {
      setSearchInput(input);
    },
    [setSearchInput]
  );

  const showErrorCodeAction = () => {
    ModalService.show({
      title: 'Action Steps',
      desc: 'Please consult Acson Malaysia or your nearest dealer.',
      buttons: [
        {
          title: 'Ok',
          isRaised: true,
        },
      ],
    });
  };

  const renderHeader = React.useMemo(
    () => (
      <View style={[styles.row]}>
        <View style={[styles.center, styles.flex]}>
          <Image
            source={require('../../shared/images/general/errorCode.png')}
          />
        </View>
        <View style={styles.descriptionFlex}>
          <Text
            style={[styles.errorText, styles.descriptionText, styles.textBold]}>
            Description
          </Text>
        </View>
      </View>
    ),
    []
  );

  /* eslint-disable react/prop-types */
  const renderItem = ({ item }) => (
    <TouchableOpacity key={item.code} onPress={showErrorCodeAction}>
      <View style={styles.row}>
        <Text
          style={[
            styles.flex,
            styles.center,
            styles.errorText,
            styles.textBold,
          ]}>
          {item.code}
        </Text>
        <View style={styles.descriptionFlex}>
          <Text style={[styles.errorText, styles.descriptionText]}>
            {item.description}
          </Text>
        </View>
      </View>
    </TouchableOpacity>
  );
  /* eslint-enable react/prop-types */

  const renderEmpty = React.useMemo(
    () => (
      <View style={styles.center}>
        <Text>Error Code not found</Text>
      </View>
    ),
    []
  );

  const renderSeparator = () => <View style={styles.separator} />;

  return (
    <View style={[styles.flex, styles.container]}>
      {isSearchMode && (
        <View style={[styles.row, styles.searchContainer]}>
          <TextInput
            placeholder="Search error code"
            onChangeText={handleSearchInput}
            style={[styles.flex, styles.searchInput]}
          />
          <Image
            style={styles.searchMargin}
            source={require('../../shared/images/general/search.png')}
          />
        </View>
      )}
      <FlatList
        style={styles.errorList}
        // eslint-disable-next-line react/prop-types
        keyExtractor={item => item.code}
        ListHeaderComponent={renderHeader}
        ItemSeparatorComponent={renderSeparator}
        ListEmptyComponent={renderEmpty}
        data={isSearchMode ? filteredCodes : damaErrorCodes}
        renderItem={renderItem}
      />
    </View>
  );
};

ErrorCodes.propTypes = {
  navigation: PropTypes.object,
};

ErrorCodes.navigationOptions = ({ navigation }) => {
  const isSearchMode = navigation.getParam('isSearchMode');
  const setIsSearchMode = navigation.getParam('setIsSearchMode');

  return {
    headerRight: () => (
      <TouchableOpacity
        onPress={() => {
          setIsSearchMode(!isSearchMode);
        }}>
        <Image
          style={styles.headerIcon}
          source={require('../../shared/images/general/search.png')}
        />
      </TouchableOpacity>
    ),
  };
};

export default ErrorCodes;
