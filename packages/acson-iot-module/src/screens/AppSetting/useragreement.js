import React from 'react';
import PropTypes from 'prop-types';
import { Linking, SafeAreaView, StyleSheet, Text, View } from 'react-native';
import WebView from 'react-native-webview';
import { Colors } from '@module/acson-ui';
import { connect } from '@module/redux-auth';
import { privacyPolicy } from '@module/redux-marketing';
import { useFocusEffect } from '@react-navigation/native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  error: {
    paddingTop: 30,
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
});

const ErrorMessage = () => (
  <View style={styles.error}>
    <Text>An error has occurred, please try again later</Text>
  </View>
);

const UserAgreement = ({ init, content, navigation }) => {
  const webviewRef = React.useRef(null);

  useFocusEffect(
    React.useCallback(() => {
      navigation.setOptions({
        title: <Text>Terms Of Use</Text>,
      });
    }, [navigation])
  );

  useFocusEffect(
    React.useCallback(() => {
      init('Acson');
    }, [])
  );

  return (
    <SafeAreaView style={styles.container}>
      {content && (
        <WebView
          ref={webviewRef}
          source={{ uri: content ? content.hyperlink : null }}
          renderError={() => <ErrorMessage />}
          onShouldStartLoadWithRequest={event => {
            if (event.url !== content.hyperlink) {
              webviewRef.current.stopLoading();
              Linking.openURL(event.url);
              return false;
            }
            return true;
          }}
        />
      )}
    </SafeAreaView>
  );
};

UserAgreement.propTypes = {
  init: PropTypes.func,
  content: PropTypes.object,
  navigation: PropTypes.object,
};

const mapStateToProps = ({ privacyPolicy: privacyPolicyState }) => ({
  content: privacyPolicyState.content,
});

const mapDispatchToProps = dispatch => ({
  init: appType => {
    dispatch(privacyPolicy.init(appType));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(UserAgreement);
