import React from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Image } from 'react-native';
import PropTypes from 'prop-types';
import { useFocusEffect } from '@react-navigation/native';
import { connect, settings } from '@daikin-dama/redux-iot';
import moment from 'moment';
import { heightPercentage, NavigationService } from '@module/utility';

import { Colors } from '@module/acson-ui';
import { SETTING_SCREEN } from '../../constants/routeNames';

const styles = StyleSheet.create({
  center: {
    alignItems: 'center',
  },
  row: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.lightCoolGrey,
    paddingHorizontal: 25,
    height: heightPercentage(8),
  },
  icon: {
    height: 17,
    width: 17,
    resizeMode: 'contain',
  },
});

const validityPeriod = [
  { label: '5 minutes', value: moment.duration(5, 'minutes').asMilliseconds() },
  {
    label: '30 minutes',
    value: moment.duration(30, 'minutes').asMilliseconds(),
  },
  { label: '1 Hour', value: moment.duration(1, 'hours').asMilliseconds() },
  { label: '6 Hours', value: moment.duration(6, 'hours').asMilliseconds() },
  { label: '1 Day', value: moment.duration(1, 'days').asMilliseconds() },
  { label: '1 Month', value: moment.duration(1, 'months').asMilliseconds() },
  { label: '1 Year', value: moment.duration(1, 'years').asMilliseconds() },
];

const onSetValidityPeriod = (period, setValidityPeriod) => {
  setValidityPeriod(period);
  NavigationService.navigate(SETTING_SCREEN);
};

const QRCodeValidity = ({ setValidityPeriod, qrCodeValidity, navigation }) => {
  useFocusEffect(
    React.useCallback(() => {
      navigation.setOptions({
        title: <Text>QR Code Validity</Text>,
      });
    }, [navigation])
  );

  return (
    <View style={styles.center}>
      {validityPeriod.map(period => (
        <TouchableOpacity
          onPress={() => onSetValidityPeriod(period, setValidityPeriod)}
          style={styles.row}
          key={period.label}>
          <Text small allowFontScaling={false}>
            {period.label}
          </Text>
          {qrCodeValidity === period.label && (
            <Image
              source={require('../../shared/images/general/tick.png')}
              style={styles.icon}
            />
          )}
        </TouchableOpacity>
      ))}
    </View>
  );
};

const mapStateToProps = state => ({
  qrCodeValidity: state.settings.qrValidityPeriod.label,
});

const mapDispatchToProps = dispatch => ({
  setValidityPeriod: period => dispatch(settings.setValidityPeriod(period)),
});

QRCodeValidity.propTypes = {
  setValidityPeriod: PropTypes.func,
  qrCodeValidity: PropTypes.object,
  navigation: PropTypes.object,
};

export default connect(mapStateToProps, mapDispatchToProps)(QRCodeValidity);
