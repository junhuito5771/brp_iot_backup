import React from 'react';
import { TouchableRow, Text } from '@module/acson-ui';
import { NavigationService } from '@module/utility';

import {
  ALEXA_SCREEN,
  QRCODE_VALIDITY_SCREEN,
  USER_AGREEMENT_SCREEN,
  ERROR_CODES_SCREEN,
} from '../../constants/routeNames';

const handleNavigateToAlexa = () => {
  NavigationService.navigate(ALEXA_SCREEN);
};

const handleNavigateToQRCodeValidity = () => {
  NavigationService.navigate(QRCODE_VALIDITY_SCREEN);
};

const handleNavigateToUserAgreement = () => {
  NavigationService.navigate(USER_AGREEMENT_SCREEN);
};

const handleNavigateToErrorCodes = () => {
  NavigationService.navigate(ERROR_CODES_SCREEN);
};

const settingItems = [
  {
    index: 2,
    key: 'Smart Speaker',
    component: () => (
      <TouchableRow onPress={handleNavigateToAlexa}>
        <Text small>Alexa</Text>
      </TouchableRow>
    ),
  },
  {
    index: 3,
    key: 'QRCode Validity',
    component: () => (
      <TouchableRow onPress={handleNavigateToQRCodeValidity}>
        <Text small>QR Code Validity</Text>
      </TouchableRow>
    ),
  },
  {
    index: 4,
    key: 'Terms',
    component: () => (
      <TouchableRow onPress={handleNavigateToUserAgreement}>
        <Text small>Terms of Use</Text>
      </TouchableRow>
    ),
  },
  {
    index: 5,
    key: 'Error Code',
    component: () => (
      <TouchableRow onPress={handleNavigateToErrorCodes}>
        <Text small>Error Codes</Text>
      </TouchableRow>
    ),
  },
];

export default settingItems;
