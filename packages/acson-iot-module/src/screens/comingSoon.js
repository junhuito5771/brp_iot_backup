import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Text } from '@module/acson-ui';

const styles = StyleSheet.create({
  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const ComingSoon = () => (
  <View style={styles.center}>
    <Text medium>Coming soon.</Text>
    <Text small>Stay tuned for updates on this feature!</Text>
  </View>
);

export default ComingSoon;
