import { Colors } from '@module/acson-ui';

const unitErrors = {
  17: {
    label: 'A1',
    description: 'Malfunction of indoor unit PCB',
    color: Colors.red,
  },
  19: {
    label: 'A3',
    description: 'Drain Level Control System Abnormality',
    color: Colors.red,
  },
  21: {
    label: 'A5',
    description:
      'High pressure control in heating, freeze-up protection control in cooling',
    color: Colors.green,
  },
  22: {
    label: 'A6',
    description: 'Malfunction of fan motor',
    color: Colors.red,
  },
  27: {
    label: 'AH',
    description: 'Malfunction of dust collector of air cleaner',
    color: Colors.red,
  },
  30: {
    label: 'AE',
    description: 'Incorrect water temperature',
    color: Colors.orange,
  },
  36: {
    label: 'C4',
    description: 'Malfunction of liquid pipe thermistor for heat exchanger',
    color: Colors.midBlue,
  },
  37: {
    label: 'C5',
    description: 'Malfunction of gas pipe thermistor for heat exchanger',
    color: Colors.red,
  },
  39: {
    label: 'C7',
    description: 'Front panel driving motor fault',
    color: Colors.red,
  },
  41: {
    label: 'C9',
    description: 'Malfunction of suction air thermistor',
    color: Colors.midBlue,
  },
  49: {
    label: 'E1',
    description: 'Defective outdoor unit PCB',
    color: Colors.red,
  },
  51: {
    label: 'E3',
    description: 'Actuation of high pressure switch (HPS)',
    color: Colors.red,
  },
  52: {
    label: 'E4',
    description: 'Actuation of low pressure switch (LPS)',
    color: Colors.red,
  },
  53: {
    label: 'E5',
    description: 'Inverter compressor motor or overheat',
    color: Colors.red,
  },
  54: {
    label: 'E6',
    description: 'STD compressor motor overcurrent/lock',
    color: Colors.red,
  },
  55: {
    label: 'E7',
    description: 'Malfunction of outdoor unit fan motor system',
    color: Colors.red,
  },
  56: {
    label: 'E8',
    description: 'Overcurrent of inverter compressor',
    color: Colors.orange,
  },
  57: {
    label: 'E9',
    description: 'Malfunction of electronic expansion valve coil',
    color: Colors.red,
  },
  58: {
    label: 'EA',
    description: 'Malfunction of four way valve or cool/ heat switching',
    color: Colors.red,
  },
  64: {
    label: 'H0',
    description: 'Malfunction of sensor system of compressor',
    color: Colors.red,
  },
  67: {
    label: 'H3',
    description: 'Malfunction of high pressure switch (HPS)',
    color: Colors.red,
  },
  70: {
    label: 'H6',
    description: 'Malfunction of position detection sensor',
    color: Colors.red,
  },
  71: {
    label: 'H7',
    description: 'Malfunction of outdoor unit fan motor signal',
    color: Colors.red,
  },
  72: {
    label: 'H8',
    description: 'Malfunction of compressor input (CT) system',
    color: Colors.red,
  },
  73: {
    label: 'H9',
    description: 'Malfunction of outdoor air thermistor',
    color: Colors.orange,
  },
  83: {
    label: 'F3',
    description: 'Malfunction of discharge pipe temperature',
    color: Colors.red,
  },
  86: {
    label: 'F6',
    description: 'Abnormal high pressure or refrigerant overcharged',
    color: Colors.green,
  },
  97: {
    label: 'J1',
    description: 'Malfunction of pressure sensor',
    color: Colors.red,
  },
  99: {
    label: 'J3',
    description: 'Malfunction of discharge pipe thermistor',
    color: Colors.orange,
  },
  101: {
    label: 'J5',
    description: 'Malfunction of suction pipe thermistor',
    color: Colors.red,
  },
  102: {
    label: 'J6',
    description: 'Malfunction of heat exchanger thermistor',
    color: Colors.orange,
  },
  103: {
    label: 'J7',
    description: 'Malfunction of thermistor (Refrigerant circuit)',
    color: Colors.red,
  },
  104: {
    label: 'J8',
    description: 'Malfunction of thermistor (Refrigerant circuit)',
    color: Colors.red,
  },
  105: {
    label: 'J9',
    description: 'Malfunction of thermistor (Refrigerant circuit)',
    color: Colors.red,
  },
  113: {
    label: 'L1',
    description: 'Malfunction of inverter PCB',
    color: Colors.red,
  },
  115: {
    label: 'L3',
    description: 'El.compo. Box temperature rise',
    color: Colors.midBlue,
  },
  116: {
    label: 'L4',
    description: 'Malfunction of inverter radiation fin temperature rise',
    color: Colors.red,
  },
  117: {
    label: 'L5',
    description: 'Inverter instantaneous overcurrent (DC output)',
    color: Colors.red,
  },
  120: {
    label: 'L8',
    description: 'Malfunction of overcurrent inverter compressor',
    color: Colors.red,
  },
  121: {
    label: 'L9',
    description:
      'Malfunction of inverter compressor startup error (Stall prevention)',
    color: Colors.red,
  },
  124: {
    label: 'LC',
    description: 'Malfunction of transmission between control and inverter PCB',
    color: Colors.red,
  },
  129: {
    label: 'P1',
    description: 'Power voltage imbalance or inverter PCB',
    color: Colors.red,
  },
  132: {
    label: 'P4',
    description: 'Malfunction of radiation fin temperature sensor',
    color: Colors.orange,
  },
  141: {
    label: 'PJ',
    description: 'Improper combination between inverter and fan driver',
    color: Colors.red,
  },
  144: {
    label: 'U0',
    description: 'Shortage of refrigerant',
    color: Colors.red,
  },
  146: {
    label: 'U2',
    description: 'Malfunction of power supply or instantaneous power failure',
    color: Colors.red,
  },
  148: {
    label: 'U4',
    description: 'Malfunction of transmission between indoor and outdoor',
    color: Colors.orange,
  },
  151: {
    label: 'U7',
    description:
      'Malfunction of transmission between outdoor units or outdoor storage unit',
    color: Colors.red,
  },
  154: {
    label: 'UA',
    description: 'Improper combination of indoor and outdoor units',
    color: Colors.red,
  },
  155: {
    label: 'UH',
    description: 'Malfunction of system',
    color: Colors.red,
  },
  159: {
    label: 'UF',
    description: 'System is not set yet',
    color: Colors.red,
  },
};

export default unitErrors;

export function getErrorCodeDetail(errorCode) {
  const errorData = unitErrors[errorCode];

  if (errorData) return errorData;

  return undefined;
}
