export const TEMP_COORDS = {
  cool: {
    16: { x: 110, y: 470 },
    17: { x: 84, y: 450 },
    18: { x: 68, y: 430 },
    19: { x: 50, y: 400 },
    20: { x: 33, y: 365 },
    21: { x: 20, y: 330 },
    22: { x: 12, y: 293 },
    23: { x: 10, y: 255 },
    24: { x: 12, y: 215 },
    25: { x: 18, y: 180 },
    26: { x: 25, y: 150 },
    27: { x: 40, y: 120 },
    28: { x: 55, y: 90 },
    29: { x: 75, y: 62 },
    30: { x: 100, y: 35 },
  },
  auto: {
    18: { x: 110, y: 470 },
    19: { x: 80, y: 445 },
    20: { x: 55, y: 410 },
    21: { x: 38, y: 380 },
    22: { x: 25, y: 340 },
    23: { x: 15, y: 300 },
    24: { x: 10, y: 255 },
    25: { x: 12, y: 215 },
    26: { x: 20, y: 170 },
    27: { x: 35, y: 130 },
    28: { x: 55, y: 90 },
    29: { x: 75, y: 65 },
    30: { x: 100, y: 35 },
  },
  heat: {
    10: { x: 110, y: 470 },
    11: { x: 85, y: 450 },
    12: { x: 70, y: 435 },
    13: { x: 60, y: 420 },
    14: { x: 45, y: 395 },
    15: { x: 40, y: 378 },
    16: { x: 28, y: 350 },
    17: { x: 20, y: 325 },
    18: { x: 15, y: 305 },
    19: { x: 13, y: 285 },
    20: { x: 10, y: 255 },
    21: { x: 11, y: 235 },
    22: { x: 15, y: 205 },
    23: { x: 18, y: 185 },
    24: { x: 21, y: 160 },
    25: { x: 31, y: 135 },
    26: { x: 40, y: 112 },
    27: { x: 58, y: 85 },
    28: { x: 68, y: 68 },
    29: { x: 86, y: 48 },
    30: { x: 100, y: 35 },
  },
};

function getMaxMinTempByMode(mode) {
  switch (mode) {
    case 'heat':
      return {
        MIN: 10,
        MAX: 30,
      };
    case 'auto':
      return {
        MIN: 18,
        MAX: 30,
      };
    default:
      return {
        MIN: 16,
        MAX: 30,
      };
  }
}

export const getTempCoord = (modeValue = 'cool', tempValue) => {
  const modeCoord = TEMP_COORDS[modeValue];

  const minTempValue = getMaxMinTempByMode(modeValue).MIN;
  const maxTempValue = getMaxMinTempByMode(modeValue).MAX;

  const curTempValue = Math.max(
    Math.min(maxTempValue, tempValue),
    minTempValue
  );

  return modeCoord[curTempValue];
};

export const getTempValue = (tempValue, modeValue = 'cool') => {
  const minTempValue = getMaxMinTempByMode(modeValue).MIN;
  const maxTempValue = getMaxMinTempByMode(modeValue).MAX;
  return Math.max(Math.min(maxTempValue, tempValue), minTempValue);
};

export const MODE_ENUM = {
  auto: 10,
  cool: 1,
  fan: 2,
  dry: 4,
};

export const MODE_ICONS = {
  auto: {
    on: require('../shared/images/remote/modeAutoOn.png'),
    off: require('../shared/images/remote/modeAutoOff.png'),
    inactive: require('../shared/images/remote/modeAutoInactive.png'),
  },
  cool: {
    on: require('../shared/images/remote/modeCoolOn.png'),
    off: require('../shared/images/remote/modeCoolOff.png'),
    inactive: require('../shared/images/remote/modeCoolInactive.png'),
  },
  fan: {
    on: require('../shared/images/remote/modeFanOn.png'),
    off: require('../shared/images/remote/modeFanOff.png'),
    inactive: require('../shared/images/remote/modeFanInactive.png'),
  },
  dry: {
    on: require('../shared/images/remote/modeDryOn.png'),
    off: require('../shared/images/remote/modeDryOff.png'),
    inactive: require('../shared/images/remote/modeDryInactive.png'),
  },
};
