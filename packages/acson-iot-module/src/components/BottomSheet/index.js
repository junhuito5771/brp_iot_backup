/* eslint-disable no-mixed-operators */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Animated,
  View,
  Text,
  ScrollView,
  PanResponder,
  TouchableWithoutFeedback,
  FlatList,
  Platform,
} from 'react-native';
import {
  remote,
  HandsetRuleHelper,
  RemoteHelper,
  ShadowValueHelper,
} from '@daikin-dama/redux-iot';
import {
  verticalScale,
  viewportWidth,
  viewportHeight,
  isSmallDevice,
  iphoneXS,
} from '@module/utility';

import { IconButton } from '@module/acson-ui';
import styles from './styles';

const options = {
  Quiet: 'silent',
  Swing: 'swing',
  iSenz: 'sense',
  Save: 'ecoplus',
  Sleep: 'sleep',
  Turbo: 'turbo',
  Fan: 'fan',
  iSave: 'smartEcoMax',
  'iTurbo+': 'smartTurbo',
};

const optionValueMap = {
  Quiet: [0, 1],
  Swing: [RemoteHelper.SWING_OFF, RemoteHelper.SWING_ON],
  iSenz: 'sense',
  Save: [RemoteHelper.ECOPLUS_OFF, RemoteHelper.ECOPLUS_ON],
  Sleep: [RemoteHelper.SLEEP_OFF, RemoteHelper.SLEEP_ON],
  Turbo: [RemoteHelper.TURBO_OFF, RemoteHelper.TURBO_ON],
};

const fanSpeedMapDIL = {
  Low: 'level1',
  'Low Medium': 'level2',
  Medium: 'level3',
  'Medium High': 'level4',
  High: 'level5',
  Auto: 'auto',
};

const fanSpeedMapDAMA = {
  Low: 'level1',
  Medium: 'level2',
  High: 'level3',
  Auto: 'auto',
};
const getUpDownSwingOptions = ({ upDownSwing }) => {
  const components = [];

  components.push({
    label: 'Off',
    action: remote.setSwing,
    value: RemoteHelper.SWING_UD_OFF,
    icons:
      upDownSwing === 0
        ? require('../../shared/images/remote/upDownSwingDisabledOn.png')
        : require('../../shared/images/remote/upDownSwingDisabledOff.png'),
  });

  components.push({
    label: 'Full',
    action: remote.setSwing,
    value: RemoteHelper.SWING_UD_FULL,
    icons:
      upDownSwing === 15
        ? require('../../shared/images/remote/upDownSwingFullOn.png')
        : require('../../shared/images/remote/upDownSwingFullOff.png'),
  });

  components.push({
    label: 'Angle 1',
    action: remote.setSwing,
    value: RemoteHelper.SWING_UD_LV1,
    icons:
      upDownSwing === 1
        ? require('../../shared/images/remote/upDownSwingLv1On.png')
        : require('../../shared/images/remote/upDownSwingLv1Off.png'),
  });

  components.push({
    label: 'Angle 2',
    action: remote.setSwing,
    value: RemoteHelper.SWING_UD_LV2,
    icons:
      upDownSwing === 2
        ? require('../../shared/images/remote/upDownSwingLv2On.png')
        : require('../../shared/images/remote/upDownSwingLv2Off.png'),
  });

  components.push({
    label: 'Angle 3',
    action: remote.setSwing,
    value: RemoteHelper.SWING_UD_LV3,
    icons:
      upDownSwing === 3
        ? require('../../shared/images/remote/upDownSwingLv3On.png')
        : require('../../shared/images/remote/upDownSwingLv3Off.png'),
  });

  components.push({
    label: 'Angle 4',
    action: remote.setSwing,
    value: RemoteHelper.SWING_UD_LV4,
    icons:
      upDownSwing === 4
        ? require('../../shared/images/remote/upDownSwingLv4On.png')
        : require('../../shared/images/remote/upDownSwingLv4Off.png'),
  });

  components.push({
    label: 'Angle 5',
    action: remote.setSwing,
    value: RemoteHelper.SWING_UD_LV5,
    icons:
      upDownSwing === 5
        ? require('../../shared/images/remote/upDownSwingLv5On.png')
        : require('../../shared/images/remote/upDownSwingLv5Off.png'),
  });

  return components;
};

const getLeftRightSwingOptions = ({ leftRightSwing }) => {
  const components = [];

  components.push({
    label: 'Off',
    action: remote.setSwingLR,
    value: RemoteHelper.SWING_LR_OFF,
    icons:
      leftRightSwing === 0
        ? require('../../shared/images/remote/leftRightSwingDisabledOn.png')
        : require('../../shared/images/remote/leftRightSwingDisabledOff.png'),
  });

  components.push({
    label: 'Full',
    action: remote.setSwingLR,
    value: RemoteHelper.SWING_LR_FULL,
    icons:
      leftRightSwing === 15
        ? require('../../shared/images/remote/leftRightSwingFullOn.png')
        : require('../../shared/images/remote/leftRightSwingFullOff.png'),
  });

  components.push({
    label: 'Angle 1',
    action: remote.setSwingLR,
    value: RemoteHelper.SWING_LR_LV1,
    icons:
      leftRightSwing === 1
        ? require('../../shared/images/remote/leftRightSwingLv1On.png')
        : require('../../shared/images/remote/leftRightSwingLv1Off.png'),
  });

  components.push({
    label: 'Angle 2',
    action: remote.setSwingLR,
    value: RemoteHelper.SWING_LR_LV2,
    icons:
      leftRightSwing === 2
        ? require('../../shared/images/remote/leftRightSwingLv2On.png')
        : require('../../shared/images/remote/leftRightSwingLv2Off.png'),
  });

  components.push({
    label: 'Angle 3',
    action: remote.setSwingLR,
    value: RemoteHelper.SWING_LR_LV3,
    icons:
      leftRightSwing === 3
        ? require('../../shared/images/remote/leftRightSwingLv3On.png')
        : require('../../shared/images/remote/leftRightSwingLv3Off.png'),
  });

  components.push({
    label: 'Angle 4',
    action: remote.setSwingLR,
    value: RemoteHelper.SWING_LR_LV4,
    icons:
      leftRightSwing === 4
        ? require('../../shared/images/remote/leftRightSwingLv4On.png')
        : require('../../shared/images/remote/leftRightSwingLv4Off.png'),
  });

  components.push({
    label: 'Angle 5',
    action: remote.setSwingLR,
    value: RemoteHelper.SWING_LR_LV5,
    icons:
      leftRightSwing === 5
        ? require('../../shared/images/remote/leftRightSwingLv5On.png')
        : require('../../shared/images/remote/leftRightSwingLv5Off.png'),
  });

  return components;
};

const getCKSwingOptions = ({ ckSwing }) => [
  {
    label: 'Off',
    action: remote.setCKSwing,
    value: RemoteHelper.CKSWING_FULL,
    icons:
      ckSwing === 0
        ? require('../../shared/images/remote/ckFullSwingOn.png')
        : require('../../shared/images/remote/ckFullSwingOff.png'),
  },
  {
    label: 'Draft',
    action: remote.setCKSwing,
    value: RemoteHelper.CKSWING_DRAFT,
    icons:
      ckSwing === 1
        ? require('../../shared/images/remote/ckDraftPreventOn.png')
        : require('../../shared/images/remote/ckDraftPreventOff.png'),
  },
  {
    label: 'Soil',
    action: remote.setCKSwing,
    value: RemoteHelper.CKSWING_SOIL,
    icons:
      ckSwing === 2
        ? require('../../shared/images/remote/ckSoilPreventOn.png')
        : require('../../shared/images/remote/ckSoilPreventOff.png'),
  },
];

const getFanSpeedOptions = ({
  mode,
  fan,
  fanExtend,
  silent,
  enableAutoFan,
  enableLowFan,
  handsetType,
}) => {
  let components = [];
  const curFanSpeedValue =
    silent === 1
      ? 'silent'
      : ShadowValueHelper.getFanLvlByShadowVal(handsetType, fan, fanExtend);

  if (mode === 'dry') {
    components = [
      {
        label: 'Auto',
        icons:
          curFanSpeedValue === 'auto'
            ? require('../../shared/images/remote/fanAutoOn.png')
            : require('../../shared/images/remote/fanAutoOff.png'),
      },
    ];

    return components;
  }

  const itemWidth = viewportWidth / 3;
  const itemWidthMargin = itemWidth - 18 * 2;
  const customWidth = handsetType === 1 ? itemWidthMargin : undefined;

  if (
    enableAutoFan &&
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_AUTO,
      handsetType
    )
  ) {
    components.push({
      label: 'Auto',
      icons:
        curFanSpeedValue === 'auto'
          ? require('../../shared/images/remote/fanAutoOn.png')
          : require('../../shared/images/remote/fanAutoOff.png'),
      ...(customWidth && { containerStyle: { width: customWidth } }),
    });
  }

  if (
    enableLowFan &&
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV1,
      handsetType
    )
  ) {
    components.push({
      label: 'Low',
      icons:
        curFanSpeedValue === 'level1'
          ? require('../../shared/images/remote/lowOn.png')
          : require('../../shared/images/remote/lowOff.png'),
      ...(customWidth && { containerStyle: { width: customWidth } }),
    });
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV2,
      handsetType
    )
  ) {
    components.push({
      label: handsetType === 0 ? 'Medium' : 'Low Medium',
      icons:
        curFanSpeedValue === 'level2'
          ? require('../../shared/images/remote/mediumOn.png')
          : require('../../shared/images/remote/mediumOff.png'),
      ...(customWidth && { containerStyle: { width: customWidth } }),
    });
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV3,
      handsetType
    )
  ) {
    components.push({
      label: handsetType === 0 ? 'High' : 'Medium',
      icons:
        curFanSpeedValue === 'level3'
          ? require('../../shared/images/remote/highOn.png')
          : require('../../shared/images/remote/highOff.png'),
      ...(customWidth && { containerStyle: { width: customWidth } }),
    });
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV4,
      handsetType
    )
  ) {
    components.push({
      label: 'Medium High',
      icons:
        curFanSpeedValue === 'level4'
          ? require('../../shared/images/remote/superHighOn.png')
          : require('../../shared/images/remote/superHighOff.png'),
      ...(customWidth && { containerStyle: { width: customWidth } }),
    });
  }

  if (
    HandsetRuleHelper.isFanSpeedOptAllowed(
      mode,
      RemoteHelper.FANSPEED_LV5,
      handsetType
    )
  ) {
    components.push({
      label: 'High',
      icons:
        curFanSpeedValue === 'level5'
          ? require('../../shared/images/remote/extremeHighOn.png')
          : require('../../shared/images/remote/extremeHighOff.png'),
      ...(customWidth && { containerStyle: { width: customWidth } }),
    });
  }

  return components;
};

const getTabItems = ({
  enableSilent,
  enableSleep,
  enableTurbo,
  enableSmartTurbo,
  enableEcoplus,
  enableAutoFan,
  enableLowFan,
  enableSense,
  enableSmartEcomax,
  enableLRStep,
  enableLRSwing,
  enableUDStep,
  enableCKSwing,
  currentOptions,
  mode,
  fan,
  fanExtend,
  silent,
  handsetType,
}) => {
  const items = [];

  if (
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_FAN_SPEED,
      handsetType
    )
  ) {
    items.push({
      label: 'Fan',
      hasMoreOpt: true,
      action: remote.setFanSpeed,
      icons:
        currentOptions.silent === 0 && currentOptions.turbo === 0
          ? require('../../shared/images/remote/fanOn.png')
          : require('../../shared/images/remote/fanOff.png'),
      moreOptions: getFanSpeedOptions({
        mode,
        fan,
        fanExtend,
        silent,
        enableAutoFan,
        enableLowFan,
        handsetType,
      }),
    });
  }

  if (
    enableSilent &&
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_SILENT,
      handsetType
    )
  ) {
    items.push({
      label: 'Quiet',
      action: remote.setSilent,
      icons: currentOptions.silent
        ? require('../../shared/images/remote/quietOn.png')
        : require('../../shared/images/remote/quietOff.png'),
    });
  }

  if (
    enableSmartTurbo &&
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_TURBO,
      handsetType
    )
  ) {
    items.push({
      label: 'iTurbo+',
      action: remote.setTurbo,
      value: [RemoteHelper.TURBO_OFF, RemoteHelper.TURBO_SMART_ON],
      icons: currentOptions.smartTurbo
        ? require('../../shared/images/remote/iTurboOn.png')
        : require('../../shared/images/remote/iTurboOff.png'),
    });
  } else if (
    enableTurbo &&
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_TURBO,
      handsetType
    )
  ) {
    items.push({
      label: 'Turbo',
      action: remote.setTurbo,
      value: [RemoteHelper.TURBO_OFF, RemoteHelper.TURBO_ON],
      icons: currentOptions.turbo
        ? require('../../shared/images/remote/turboOn.png')
        : require('../../shared/images/remote/turboOff.png'),
    });
  }

  if (enableUDStep) {
    items.push({
      label: 'Up Down Step Swing',
      hasMoreOpt: true,
      action: remote.setFanSpeed,
      icons:
        currentOptions.upDownSwing > 0
          ? require('../../shared/images/remote/upDownSwingOn.png')
          : require('../../shared/images/remote/upDownSwingOff.png'),
      moreOptions: getUpDownSwingOptions(currentOptions),
    });
  }

  if (enableLRStep && enableLRSwing) {
    items.push({
      label: 'Left Right Step Swing',
      hasMoreOpt: true,
      action: remote.setFanSpeed,
      icons:
        currentOptions.leftRightSwing > 0
          ? require('../../shared/images/remote/leftRightSwingOn.png')
          : require('../../shared/images/remote/leftRightSwingOff.png'),
      moreOptions: getLeftRightSwingOptions(currentOptions),
    });
  }

  if (!(enableLRStep && enableLRSwing) && !enableUDStep) {
    items.push({
      label: 'Swing',
      action: remote.setSwing,
      icons: currentOptions.swing
        ? require('../../shared/images/remote/swingOn.png')
        : require('../../shared/images/remote/swingOff.png'),
    });
  }

  if (
    enableSmartEcomax &&
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_ECOPLUS,
      handsetType
    )
  ) {
    items.push({
      label: 'iSave',
      action: remote.setEcoplus,
      value: [RemoteHelper.ECOPLUS_OFF, RemoteHelper.ECO_SMART],
      icons: currentOptions.smartEcoMax
        ? require('../../shared/images/remote/iSaveOn.png')
        : require('../../shared/images/remote/iSaveOff.png'),
    });
  } else if (
    enableEcoplus &&
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_ECOPLUS,
      handsetType
    )
  ) {
    items.push({
      label: 'Save',
      action: remote.setEcoplus,
      value: [RemoteHelper.ECOPLUS_OFF, RemoteHelper.ECOPLUS_ON],
      icons: currentOptions.ecoplus
        ? require('../../shared/images/remote/ecoOn.png')
        : require('../../shared/images/remote/ecoOff.png'),
    });
  }

  if (
    enableSleep &&
    HandsetRuleHelper.isFeatureAllowed(
      mode,
      RemoteHelper.FEAT_SLEEP,
      handsetType
    )
  ) {
    items.push({
      label: 'Sleep',
      action: remote.setSleep,
      icons: currentOptions.sleep
        ? require('../../shared/images/remote/sleepOn.png')
        : require('../../shared/images/remote/sleepOff.png'),
    });
  }

  if (enableCKSwing) {
    items.push({
      label: 'CK Special Swing',
      hasMoreOpt: true,
      action: remote.setFanSpeed,
      icons: currentOptions.ckSwing
        ? require('../../shared/images/remote/ckSwingOn.png')
        : require('../../shared/images/remote/ckSwingOff.png'),
      moreOptions: getCKSwingOptions(currentOptions),
    });
  }

  if (enableLRStep && enableLRSwing && enableUDStep) {
    items.push({
      label: '3D Draft',
      action: remote.setSwing3D,
      icons:
        currentOptions.swing === 1 &&
        currentOptions.leftRightSwing === 15 &&
        currentOptions.upDownSwing === 15
          ? require('../../shared/images/remote/3dDraftOn.png')
          : require('../../shared/images/remote/3dDraftOff.png'),
      moreOptions: getFanSpeedOptions({
        mode,
        fan,
        fanExtend,
        silent,
        enableAutoFan,
        enableLowFan,
        handsetType,
      }),
    });
  }

  if (enableSense) {
    items.push({
      label: 'iSenz',
      action: remote.setSense,
      icons: currentOptions.sense
        ? require('../../shared/images/remote/iSenzOn.png')
        : require('../../shared/images/remote/iSenzOff.png'),
    });
  }

  return items;
};

const snapToPoint = (value, point) => {
  Animated.timing(value, {
    toValue: point,
    duration: 250,
    useNativeDriver: false,
  }).start();
};

const calculateHeight = bottomItems => {
  const baseHeight = bottomItems && bottomItems.length > 8 ? 320 : 220;

  const extraMargin = Platform.select({
    ios: iphoneXS ? 0 : 20,
    default: isSmallDevice ? 50 : 0,
  });

  return verticalScale(baseHeight + extraMargin);
};

const calculateHalfVisible = (
  height,
  extendOptKey,
  bottomItems,
  extendOptions
) => {
  const hasMoreItems = bottomItems && bottomItems.length > 8;
  switch (extendOptKey) {
    case 'Fan': {
      const doubelRow = extendOptions && extendOptions.length > 4;
      const moreOptionHeight = doubelRow ? 0 : height / 3;
      const moreItemsOptionHeight = doubelRow ? height / 3 : height / 2;
      return hasMoreItems ? moreItemsOptionHeight : moreOptionHeight;
    }
    case 'Up Down Step Swing':
      return hasMoreItems ? height / 3 : 0;
    case 'Left Right Step Swing':
      return hasMoreItems ? height / 3 : 0;
    default:
      return hasMoreItems ? height / 2 : height / 3;
  }
};

const BottomSheet = ({ unit, dispatch, disabled }) => {
  const [showMoreOptions, setShowMoreOptions] = React.useState(false);
  const [currentPage, setCurrentPage] = React.useState(0);
  const [extendOptKey, setExtendOptKey] = React.useState('Fan');

  const { modeConfig, handsetType, status } = unit;

  const currentOptions = modeConfig;

  const bottomItems = getTabItems({
    ...unit,
    ...modeConfig,
    currentOptions,
  });

  const containerHeight = calculateHeight(bottomItems);

  const extendOptions =
    (bottomItems.find(({ label }) => label === extendOptKey) || {})
      .moreOptions || [];

  const hiddenMargin = Platform.select({
    ios: iphoneXS ? 70 : 65,
    default: 60,
  });

  const snapPoints = {
    hidden: -containerHeight + hiddenMargin,
    visible: 0,
    halfVisible: -calculateHalfVisible(
      containerHeight,
      extendOptKey,
      bottomItems,
      extendOptions
    ),
  };

  const startingPosition = snapPoints.hidden;

  const animatedPosition = React.useRef(new Animated.Value(startingPosition))
    .current;
  const movementValue = gestureState =>
    viewportHeight - gestureState.moveY + snapPoints.hidden;

  const animateMove = toValue => {
    Animated.spring(animatedPosition, {
      toValue,
      tension: 120,
      useNativeDriver: false,
    }).start();
  };

  React.useEffect(() => {
    if (currentPage >= viewportWidth && showMoreOptions) {
      snapToPoint(animatedPosition, snapPoints.halfVisible);
      return;
    }

    if (showMoreOptions) {
      snapToPoint(animatedPosition, snapPoints.visible);
      return;
    }

    snapToPoint(animatedPosition, snapPoints.hidden);
  }, [
    showMoreOptions,
    currentPage,
    handsetType,
    snapPoints.hidden,
    snapPoints.halfVisible,
  ]);

  const onMoveShouldSetPanResponder = (_, gestureState) =>
    status === 'connected' &&
    !disabled &&
    (gestureState.dy >= 10 || gestureState.dy <= -10);

  const panGesture = PanResponder.create({
    onStartShouldSetPanResponder: onMoveShouldSetPanResponder,
    onMoveShouldSetPanResponder,
    onPanResponderMove: (_, gestureState) => {
      const value =
        currentPage >= viewportWidth
          ? snapPoints.halfVisible
          : snapPoints.visible;
      const toValue = Math.min(value, movementValue(gestureState));
      animateMove(toValue);
    },
    onPanResponderRelease: (_, gestureState) => {
      const IsMoreThanThird =
        movementValue(gestureState) > snapPoints.hidden / 3;
      let toValue;
      if (currentPage < viewportWidth || handsetType === 1) {
        toValue = IsMoreThanThird ? snapPoints.visible : startingPosition;
      } else {
        toValue = IsMoreThanThird ? snapPoints.halfVisible : startingPosition;
      }

      if (IsMoreThanThird) {
        setShowMoreOptions(true);
      } else {
        setShowMoreOptions(false);
      }

      animateMove(toValue);
    },
  });

  const scrollRef = React.useRef(null);

  const handleSelectOption = item => {
    const { label, hasMoreOpt } = item;
    if (hasMoreOpt) {
      setExtendOptKey(label);
      scrollRef.current.scrollToEnd();
      return;
    }
    if (label === '3D Draft') {
      const value = Number(
        !(
          modeConfig.swing === 1 &&
          modeConfig.leftRightSwing === 15 &&
          modeConfig.upDownSwing === 15
        )
      );

      dispatch(item.action(value));

      return;
    }

    const optionCurrentValue = modeConfig[options[label]];
    const optionValues = optionValueMap[label];
    const optionIndex = optionCurrentValue === 1 ? 0 : 1;

    if (item.value) {
      dispatch(item.action(item.value[optionIndex]));
      return;
    }

    dispatch(item.action(optionValues[optionIndex]));
  };

  const handleExtendOptionOnPress = item => {
    if (item.action) {
      dispatch(item.action(item.value));
      return;
    }

    // Handle Fan speed action
    const fanSpeedMap = handsetType === 0 ? fanSpeedMapDAMA : fanSpeedMapDIL;
    const value = fanSpeedMap[item.label];
    dispatch(remote.setFanSpeed(value));
  };

  const handleScroll = event => {
    setCurrentPage(event.nativeEvent.contentOffset.x);
    if (
      event.nativeEvent.contentOffset.x > viewportWidth - 2.5 &&
      Platform.OS === 'android'
    ) {
      scrollRef.current.scrollToEnd();
    }
  };

  return (
    <>
      {showMoreOptions && (
        <TouchableWithoutFeedback onPress={() => setShowMoreOptions(false)}>
          <View style={styles.overlayStyle} />
        </TouchableWithoutFeedback>
      )}
      <Animated.View
        style={[
          styles.bottomViewStyle,
          {
            height: containerHeight,
            bottom: animatedPosition,
          },
        ]}>
        <View style={[styles.touchableStyle]} {...panGesture.panHandlers}>
          <View style={styles.oblongStyle} />
          <Text
            allowFontScaling={false}
            style={[
              styles.textStyle,
              (status === 'disconnected' || disabled) && styles.offTextColor,
            ]}>
            More options
          </Text>
        </View>
        <ScrollView
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          ref={scrollRef}
          onScroll={e => handleScroll(e)}>
          <BottomSheetContent
            data={bottomItems}
            cols={bottomItems && bottomItems.length < 7 ? 3 : 4}
            handlePress={handleSelectOption}
          />
          <BottomSheetContent
            data={extendOptions}
            cols={extendOptions && extendOptions.length <= 3 ? 3 : 4}
            handlePress={handleExtendOptionOnPress}
          />
        </ScrollView>
      </Animated.View>
    </>
  );
};

function formatData({ data, cols }) {
  const numOfFullRows = Math.ceil(data.length / cols);
  const remainingNumOfElements = numOfFullRows * cols - data.length;
  const result = [...data];

  for (let i = 0; i < remainingNumOfElements; i += 1) {
    result.push({
      label: '',
      icons: require('../../shared/images/remote/iSenzOff.png'),
      disabled: true,
    });
  }

  return result;
}

const BottomSheetContent = ({ data, cols, handlePress }) => (
  <FlatList
    keyExtractor={item => item.label}
    key={data.length}
    {...(cols > 1 && {
      numColumns: cols,
      columnWrapperStyle: styles.columnWrapperStyle,
    })}
    style={{ width: viewportWidth }}
    data={formatData({ data, cols })}
    renderItem={({ item }) => (
      <IconButton
        key={item.label}
        src={item.icons}
        label={item.label}
        containerStyle={item.containerStyle}
        iconStyle={item.iconStyle}
        disabled={item.disabled}
        onPress={() => handlePress(item)}
      />
    )}
  />
);

BottomSheetContent.propTypes = {
  data: PropTypes.array,
  cols: PropTypes.number,
  handlePress: PropTypes.func,
};

BottomSheet.propTypes = {
  unit: PropTypes.object,
  dispatch: PropTypes.func,
  disabled: PropTypes.bool,
};

export default BottomSheet;
