import React from 'react';
import { TouchableWithoutFeedback, Image } from 'react-native';
import PropTypes from 'prop-types';
import { MODE_ICONS } from '../../constants/remoteControl';

const modeIcon = (mode, currentMode, status, switchValue) => {
  if (status === 'disconnected' || switchValue === 0) {
    return MODE_ICONS[mode].inactive;
  }
  if (currentMode === mode) {
    return MODE_ICONS[mode].on;
  }
  return MODE_ICONS[mode].off;
};

const ModeButton = ({ mode, currentMode, onPress, status, switchValue }) => (
  <TouchableWithoutFeedback
    onPress={onPress}
    disabled={status === 'disconnected'}>
    <Image source={modeIcon(mode, currentMode, status, switchValue)} />
  </TouchableWithoutFeedback>
);

ModeButton.propTypes = {
  mode: PropTypes.string,
  currentMode: PropTypes.string,
  status: PropTypes.string,
  switchValue: PropTypes.number,

  onPress: PropTypes.func,
};

export default ModeButton;
