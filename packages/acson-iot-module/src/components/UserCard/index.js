import React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  Image,
  StyleSheet,
  Platform,
  TouchableOpacity,
} from 'react-native';
import moment from 'moment';
import Interactable from 'react-native-interactable';
import PropTypes from 'prop-types';
import {
  heightPercentage,
  iphoneXS,
  ScaleText,
  verticalScale,
} from '@module/utility';

import { Text, Colors } from '@module/acson-ui';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  stackedContainer: {
    backgroundColor: Colors.veryDarkRed,
    height: Platform.select({
      ios: iphoneXS ? heightPercentage(10) : heightPercentage(12),
      android: heightPercentage(10),
    }),
    paddingVertical: 15,
    paddingHorizontal: 25,
    position: 'absolute',
    zIndex: -1,
    marginTop: heightPercentage(1),
    width: '98%',
    alignSelf: 'center',
    justifyContent: 'center',
    borderRadius: 7,
  },
  stackedRow: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  blue: {
    color: Colors.midBlue,
  },
  container: {
    width: '100%',
    borderRadius: 7,
    ...Platform.select({
      ios: {
        borderColor: Colors.lightCoolGrey,
        borderWidth: 1,
      },
    }),
    shadowColor: Colors.midLightGrey,
    shadowOffset: {
      width: 0,
      height: 3,
    },
    shadowOpacity: 0.4,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: Colors.white,
    height: Platform.select({
      ios: iphoneXS ? heightPercentage(10) : heightPercentage(12),
      android: heightPercentage(10),
    }),
    paddingHorizontal: 10,
    paddingVertical: 15,
    marginVertical: heightPercentage(1),
    alignSelf: 'center',
    justifyContent: 'center',
  },
  interactableContainer: {
    zIndex: 10,
  },
  userIcon: {
    height: 34,
  },
  guestIcon: {
    height: 34,
    width: 22,
    marginRight: 15,
  },
  name: {
    fontSize: 16,
    fontWeight: '500',
  },
  email: {
    marginTop: 2,
  },
  marginBottom: {
    marginBottom: heightPercentage(1.5),
  },
  paddingHorizontal: {
    paddingHorizontal: 25,
  },
  hitslop: {
    top: 10,
    bottom: 10,
    right: 20,
    left: 20,
  },
  expiryContainer: {
    backgroundColor: Colors.red,
    borderRadius: 15,
    padding: 1,
    paddingHorizontal: 15,
    marginLeft: 5,
  },
  expiryLabel: {
    color: Colors.white,
    fontSize: ScaleText(11),
  },
  textWidth: {
    width: verticalScale(70),
  },
});

const DRAG_CLOSED = 'Closed';
const DRAG_OPEN = 'Open';
const EXPIRY_TIME_FORMAT = 'DD MMMM YYYY, hh:mm A';
const EXPIRY_TIME_MOMENT_FORMAT = 'YYYY-MM-DD HH:mm:s';

const getIcon = (isHost, canDrag) => {
  if (isHost) return require('../../shared/images/unitUser/user.png');
  if (!isHost && !canDrag) return null;
  if (!isHost) return require('../../shared/images/home/editUnit.png');
  return null;
};

const displayExpiryTime = date => {
  const momentDate = moment(date, EXPIRY_TIME_MOMENT_FORMAT);
  return `${moment(momentDate).format(EXPIRY_TIME_FORMAT)}`;
};

const checkExpiryTime = value => {
  if (value) return value !== '-';
  return false;
};

const UserCard = ({
  onPressCard,
  isHost,
  name,
  email,
  expiryTime,
  currentUserIsHost,
  onDragRight,
  canDrag,
  forwardRef,
}) => (
  <View style={styles.paddingHorizontal}>
    <Interactable.View
      snapPoints={[
        { x: 0, id: DRAG_CLOSED },
        { x: -60, id: DRAG_OPEN },
      ]}
      boundaries={{ right: 0, left: -80 }}
      horizontalOnly
      ref={forwardRef}
      dragEnabled={canDrag}
      startOnFront
      style={styles.interactableContainer}>
      <View style={[styles.container, !isHost && styles.marginBottom]}>
        <TouchableWithoutFeedback
          onPress={onPressCard}
          disabled={isHost || !currentUserIsHost}>
          <View style={styles.row}>
            <View>
              <View style={styles.row}>
                <View
                  style={
                    expiryTime && !(!isHost && !canDrag)
                      ? styles.textWidth
                      : null
                  }>
                  <Text
                    numberOfLines={1}
                    ellipsizeMode="middle"
                    style={[styles.name, isHost && styles.blue]}>
                    {name}
                  </Text>
                </View>
                {!isHost && checkExpiryTime(expiryTime) && (
                  <View style={styles.expiryContainer}>
                    <Text style={styles.expiryLabel}>
                      {displayExpiryTime(expiryTime)}
                    </Text>
                  </View>
                )}
              </View>
              <Text style={[styles.email, isHost && styles.blue]}>{email}</Text>
            </View>
            <View>
              <Image
                source={getIcon(isHost, canDrag)}
                resizeMode="contain"
                style={isHost ? styles.userIcon : styles.guestIcon}
              />
            </View>
          </View>
        </TouchableWithoutFeedback>
      </View>
    </Interactable.View>
    <View style={styles.stackedContainer}>
      <View style={styles.stackedRow}>
        <TouchableOpacity hitSlop={styles.hitslop} onPress={onDragRight}>
          <Image
            source={require('../../shared/images/unitUser/delete.png')}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    </View>
  </View>
);

UserCard.propTypes = {
  onPressCard: PropTypes.func,

  isHost: PropTypes.bool,
  currentUserIsHost: PropTypes.bool,
  name: PropTypes.string,
  email: PropTypes.string,
  onDragRight: PropTypes.func,
  forwardRef: PropTypes.object,
  canDrag: PropTypes.bool,
  expiryTime: PropTypes.string,
};

export default React.forwardRef((props, ref) => (
  <UserCard {...props} forwardRef={ref} />
));
