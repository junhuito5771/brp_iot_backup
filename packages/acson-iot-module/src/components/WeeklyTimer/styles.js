import { StyleSheet } from 'react-native';
import { widthPercentage, heightPercentage } from '@module/utility';

import { Colors } from '@module/acson-ui';

const styles = StyleSheet.create({
  flexRow: {
    flexDirection: 'row',
  },
  groupHeader: {
    paddingVertical: widthPercentage(3),
  },
  header: {
    paddingVertical: 15,
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: Colors.borderlightGray,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  row: {
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  item: {
    justifyContent: 'space-around',
  },
  alignCenter: {
    alignItems: 'center',
  },
  justifyCenter: {
    justifyContent: 'center',
  },
  hitslop: {
    top: 20,
    bottom: 20,
    left: 30,
    right: 30,
  },
  modeIcon: {
    marginRight: 5,
  },
  emptyTimer: {
    paddingVertical: 10,
    paddingHorizontal: 25,
  },
  emptyTimerText: {
    color: Colors.borderlightGray,
  },
  timerUnit: {
    borderColor: Colors.borderlightGray,
    borderWidth: 1,
    borderRadius: 10,
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    backgroundColor: Colors.white,
    marginBottom: heightPercentage(3),
  },
  borderBottom: {
    borderBottomColor: Colors.borderlightGray,
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  rowSection: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  userIcon: {
    position: 'absolute',
    left: 15,
  },
  leftIcon: {
    paddingHorizontal: 10,
  },
  textContainer: {
    width: widthPercentage(70),
  },
  spaced: {
    marginBottom: heightPercentage(2),
  },
});

export default styles;
