import React from 'react';
import { View, TouchableOpacity, Image, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { StringHelper } from '@module/utility';

import { Text } from '@module/acson-ui';

import styles from './styles';

const ADD_ICON = require('../../shared/images/timer/add.png');
const USER_TIMER_ICON = require('../../shared/images/timer/user-timer.png');

const getMode = (mode, isPowerOn) => {
  if (isPowerOn) {
    switch (mode) {
      case 'cool':
        return require('../../shared/images/timer/CoolOn.png');
      case 'dry':
        return require('../../shared/images/timer/DryOn.png');
      case 'fan':
        return require('../../shared/images/timer/FanOn.png');
      default:
        return require('../../shared/images/timer/AutoOn.png');
    }
  }

  switch (mode) {
    case 'cool':
      return require('../../shared/images/timer/CoolOff.png');
    case 'dry':
      return require('../../shared/images/timer/DryOff.png');
    case 'fan':
      return require('../../shared/images/timer/FanOff.png');
    default:
      return require('../../shared/images/timer/AutoOff.png');
  }
};

export const WeeklyTimerHeader = ({ title, onPressAdd }) => (
  <View style={[styles.header, styles.row, styles.flexRow]}>
    <View style={styles.textContainer}>
      <Text medium numberOfLines={1}>
        {title}
      </Text>
    </View>
    <View style={styles.leftIcon}>
      <TouchableOpacity hitslop={styles.hitslop} onPress={onPressAdd}>
        <Image source={ADD_ICON} />
      </TouchableOpacity>
    </View>
  </View>
);

WeeklyTimerHeader.propTypes = {
  title: PropTypes.string,
  onPressAdd: PropTypes.func,
};

export const WeeklyTimerSectionHeader = ({ children }) => (
  <View style={styles.groupHeader}>
    <Text medium numberOfLines={1}>
      {children}
    </Text>
  </View>
);

WeeklyTimerSectionHeader.propTypes = {
  children: PropTypes.string,
};

const WeeklyTimerRowItem = ({
  timers,
  dayIndex,
  onPressEdit,
  thingName,
  userProfile,
}) => {
  if (timers.length < 1) {
    return (
      <View style={[styles.row, styles.justifyCenter, styles.emptyTimer]}>
        <Text small style={styles.emptyTimerText}>
          Haven&apos;t set timer yet
        </Text>
      </View>
    );
  }
  return timers.map((timer, index) => {
    const { hour, minute, temp, mode, switchValue, timerIndex, user } = timer;
    return (
      <View
        style={[
          styles.justifyCenter,
          index < timers.length - 1 && styles.borderBottom,
        ]}
        key={`${thingName}_${dayIndex}_${hour}_${minute}_${timerIndex}`}>
        <TouchableOpacity onPress={() => onPressEdit(thingName, timers, index)}>
          <View style={[styles.row, styles.flexRow]}>
            <View style={styles.rowSection}>
              <View style={styles.userIcon}>
                {user === userProfile ? (
                  <Image source={USER_TIMER_ICON} />
                ) : null}
              </View>
              <Text small>{StringHelper.getTimeInString(hour, minute)}</Text>
            </View>
            <View
              style={[styles.flexRow, styles.alignCenter, styles.rowSection]}>
              {switchValue === 1 ? (
                <>
                  <Image
                    source={getMode(mode, Boolean(switchValue))}
                    style={styles.modeIcon}
                  />
                  {!!temp && <Text>{`${temp} \u2103`}</Text>}
                </>
              ) : (
                <Text small>OFF</Text>
              )}
            </View>
          </View>
        </TouchableOpacity>
      </View>
    );
  });
};

WeeklyTimerRowItem.propTypes = {
  timers: PropTypes.array,
  onPress: PropTypes.func,
};

const WeeklyTimerContainer = ({
  groupName,
  units,
  hasSpaced,
  dayIndex,
  onPressAdd,
  onPressEdit,
  userProfile,
}) => (
  <View style={StyleSheet.flatten([hasSpaced && styles.spaced])}>
    <WeeklyTimerSectionHeader>{groupName}</WeeklyTimerSectionHeader>
    {units.map(unit => {
      const { unitName, thingName, timers } = unit;
      return (
        <View style={styles.timerUnit} key={unitName}>
          <WeeklyTimerHeader
            title={unitName}
            onPressAdd={() => onPressAdd(thingName, unit, dayIndex)}
          />
          <WeeklyTimerRowItem
            dayIndex={dayIndex}
            thingName={thingName}
            timers={timers}
            onPressEdit={onPressEdit}
            userProfile={userProfile}
          />
        </View>
      );
    })}
  </View>
);

WeeklyTimerContainer.propTypes = {
  groupName: PropTypes.string,
  units: PropTypes.array,
  hasSpaced: PropTypes.bool,
  dayIndex: PropTypes.number,
  onPressAdd: PropTypes.func,
  onPressEdit: PropTypes.func,
  userProfile: PropTypes.string,
};

export default WeeklyTimerContainer;
