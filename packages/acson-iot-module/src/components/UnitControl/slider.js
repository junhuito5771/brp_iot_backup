import React from 'react';
import { StyleSheet, View, PanResponder } from 'react-native';
import PropTypes from 'prop-types';
import { widthPercentage, heightPercentage } from '@module/utility';
import {
  connect,
  remote,
  RemoteHelper,
  HandsetRuleHelper,
} from '@daikin-dama/redux-iot';
import Svg, {
  LinearGradient,
  Path,
  Defs,
  Stop,
  Circle,
  G,
  Text as SvgText,
} from 'react-native-svg';

import { Colors } from '@module/acson-ui';
import {
  TEMP_COORDS,
  getTempCoord,
  getTempValue,
} from '../../constants/remoteControl';

const styles = StyleSheet.create({
  width: {
    width: widthPercentage(45),
    height: heightPercentage(60),
  },
});

const TempSlider = ({
  unit,
  setTemp,
  temperature,
  setTemperature,
  allUnits,
  setAllUnits,
  desired,
  clearDesireTemp,
}) => {
  // For future check
  // const checkTemp = (temp) => {
  //   if (unit.mode === 'cool' && temp < 18) return 18;
  //   return temp;
  // };
  const [sliderHeight, setSliderHeight] = React.useState(499);
  const acTempInit = getTempValue(unit.modeConfig.acTemp, unit.mode);
  // const acTemp = checkTemp(acTempInit);
  // const temp = checkTemp(temperature);

  const [previousTemp] = React.useState(parseInt(acTempInit, 10));
  const [temps, setTemps] = React.useState({});
  const [coords, setCoords] = React.useState(TEMP_COORDS.cool);

  const heatOrAutoModes = unit.mode === 'heat' || unit.mode === 'auto';

  React.useEffect(() => {
    if (desired) {
      setTemperature(unit.modeConfig.acTemp);
      if (unit.modeConfig.acTemp === desired.temp) {
        setTemperature(unit.modeConfig.acTemp);
        clearDesireTemp();
      } else if (desired.temp === undefined && unit.modeConfig.acTemp !== 1) {
        setTemperature(unit.modeConfig.acTemp);
      }
    }
  }, [unit.modeConfig.acTemp]);

  React.useEffect(() => {
    const newCoords = {};
    if (sliderHeight !== 499) {
      const percentage = 499 / sliderHeight;
      Object.entries(coords).forEach(([key, value]) => {
        newCoords[key] = { x: value.x, y: value.y / percentage };
      });
      setCoords(newCoords);
    }
  }, [sliderHeight]);

  const cx = heatOrAutoModes
    ? getTempCoord(unit.mode, temperature).x
    : getTempCoord('cool', temperature).x;
  const cy = heatOrAutoModes
    ? getTempCoord(unit.mode, temperature).y
    : getTempCoord('cool', temperature).y;

  React.useEffect(() => {
    setTemperature(parseInt(acTempInit, 10));
    if (parseInt(previousTemp, 10) !== parseInt(temperature, 10)) {
      const newUnits = allUnits;
      newUnits[unit.ThingName].acTemp = parseInt(temperature, 10);
      setAllUnits(newUnits);
    }
  }, [acTempInit]);

  React.useEffect(() => {
    if (heatOrAutoModes) {
      setCoords(TEMP_COORDS[unit.mode]);
    } else {
      setCoords(TEMP_COORDS.cool);
    }
    switch (unit.mode) {
      case 'cool':
        setTemps({ min: 16, mid: 23, max: 30 });
        break;
      case 'auto':
        setTemps({ min: 18, mid: 24, max: 30 });
        break;
      case 'heat':
        setTemps({ min: 10, mid: 20, max: 30 });
        break;
      default:
        setTemps({ min: 16, mid: 23, max: 30 });
    }
  }, [unit.mode]);

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => unit.status === 'connected',
    onMoveShouldSetPanResponder: () => unit.status === 'connected',
    onPanResponderRelease: () => {
      setTemp(parseInt(temperature, 10));
    },
    onPanResponderMove: ({ nativeEvent: { locationY } }) => {
      const coordsValue = Object.entries(coords);

      if (locationY > coords[acTempInit].y && temperature > temps.min) {
        coordsValue.forEach(([key, value]) => {
          if (locationY <= value.y) {
            setTemperature(parseInt(key, 10));
          } else if (locationY >= coordsValue[0][1].y) {
            setTemperature(temps.min);
          }
        });
      } else if (locationY < coords[acTempInit].y && temperature < temps.max) {
        coordsValue.reverse().forEach(([key, value]) => {
          if (locationY >= value.y) {
            setTemperature(parseInt(key, 10));
          } else if (locationY <= coordsValue[0][1].y) {
            setTemperature(temps.max);
          }
        });
      }
    },
  });

  const canAdjustTemp = HandsetRuleHelper.isFeatureAllowed(
    unit.mode,
    RemoteHelper.FEAT_TEMP,
    unit.handsetType
  );

  const isGrey =
    unit.status === 'disconnected' || unit.switch === 0 || !canAdjustTemp;

  return (
    <View
      style={styles.width}
      onLayout={event => {
        setSliderHeight(event.nativeEvent.layout.height);
      }}
      {...(canAdjustTemp && panResponder.panHandlers)}>
      <Svg width="100%" height="100%" viewBox="0 0 162 499">
        <Defs>
          <LinearGradient
            x1="48.2901133%"
            y1="3.52062985%"
            x2="48.1220555%"
            y2="94.148369%"
            id="linearGradient-1">
            <Stop
              stopColor={isGrey ? Colors.linkGrey : '#FF002A'}
              offset="0%"
            />
            <Stop
              stopColor={isGrey ? Colors.linkGrey : '#53B2C0'}
              offset="59.9292652%"
            />
            <Stop
              stopColor={isGrey ? Colors.titleGrey : '#49ACAE'}
              offset="100%"
            />
          </LinearGradient>
          <LinearGradient
            x1="48.3020873%"
            y1="3.52062985%"
            x2="48.20013%"
            y2="58.8904278%"
            id="linearGradient-2">
            <Stop
              stopColor={isGrey ? Colors.linkGrey : '#FF002A'}
              offset="0%"
            />
            <Stop
              stopColor={isGrey ? Colors.linkGrey : '#53B2C0'}
              offset="100%"
            />
          </LinearGradient>
          <LinearGradient
            x1="57.1916774%"
            y1="29.0283385%"
            x2="37.6060704%"
            y2="43.5425077%"
            id="linearGradient-3">
            <Stop stopColor="#FFFFFF" offset="0%" />
            <Stop stopColor="#FFFFFF" stopOpacity="0" offset="100%" />
          </LinearGradient>
          <LinearGradient
            x1="39.5578378%"
            y1="53.7421613%"
            x2="28.9731441%"
            y2="50.9975179%"
            id="linearGradient-4">
            <Stop stopColor="#FFFFFF" offset="0%" />
            <Stop stopColor="#FFFFFF" stopOpacity="0" offset="100%" />
          </LinearGradient>
        </Defs>
        <SvgText
          fill={isGrey ? Colors.linkGrey : Colors.darkPink}
          fontSize="16"
          x="140"
          y="90"
          textAnchor="middle">
          {temps.max}
        </SvgText>
        <SvgText
          fill={isGrey ? Colors.linkGrey : Colors.cerulean}
          fontSize="16"
          x="65"
          y="263"
          textAnchor="middle">
          {temps.mid}
        </SvgText>
        <SvgText
          fill={isGrey ? Colors.linkGrey : Colors.blueGreen}
          fontSize="16"
          x="140"
          y="430"
          textAnchor="middle">
          {temps.min}
        </SvgText>
        <G
          id="remote-control-meter-copy-6"
          stroke="none"
          stroke-width="1"
          fill="none"
          fillRule="evenodd">
          <G id="Group-12">
            <Path
              d="M149.998265,1.23677845 L149.997035,5.06165993 C68.4549657,44.420701 11,139.036135 11,249.5 C11,359.963865 68.4549657,454.579299 149.997035,493.93834 L149.998265,497.763222 C66.7600586,458.208888 8,361.958659 8,249.5 C8,137.041341 66.7600586,40.7911117 149.998265,1.23677845 Z"
              id="Combined-Shape"
              fill="url(#linearGradient-1)"
              fillRule="nonzero"
            />
            <Path
              d="M152.479187,456.545505 C153.344929,457.042258 154.213871,457.531328 155.085946,458.012667 L155.085946,458.012667 L148.734761,474.066351 C147.7944,473.547321 146.857428,473.019962 145.923917,472.484323 L145.923917,472.484323 Z M133.99106,444.407207 C134.805693,445.013606 135.624162,445.612781 136.446398,446.204676 L136.446398,446.204676 L128.639,461.335965 C127.752703,460.697955 126.870479,460.052106 125.992399,459.398479 L125.992399,459.398479 Z M116.691118,429.900967 C117.444949,430.609684 118.203242,431.311731 118.965932,432.007043 L118.965932,432.007043 L109.799551,446.034662 C108.977699,445.285414 108.160591,444.528916 107.348298,443.765235 L107.348298,443.765235 Z M100.861314,413.268612 C101.544366,414.069491 102.232461,414.86435 102.925537,415.653117 L102.925537,415.653117 L92.5163078,428.413643 C91.7695742,427.56381 91.028208,426.707414 90.2922757,425.844533 L90.2922757,425.844533 Z M86.65339,394.678814 C87.2576623,395.562241 87.8675113,396.44035 88.4828807,397.313064 L88.4828807,397.313064 L76.9552173,408.653316 C76.292152,407.712959 75.6350308,406.766784 74.983914,405.814873 L74.983914,405.814873 Z M74.2682119,374.391935 C74.7870911,375.348395 75.3120299,376.300263 75.842979,377.247461 L75.842979,377.247461 L63.3340959,387.029932 C62.7618362,386.009038 62.1960478,384.983097 61.6367837,383.952195 L61.6367837,383.952195 Z M63.8562615,352.655873 C64.2832178,353.674206 64.7166402,354.68869 65.1564872,355.699244 L65.1564872,355.699244 L51.8142536,363.80117 C51.3400102,362.711589 50.8726882,361.617759 50.4123323,360.519764 L50.4123323,360.519764 Z M55.5172533,329.645299 C55.8460929,330.712624 56.181703,331.776846 56.5240501,332.837888 L56.5240501,332.837888 L42.5054122,339.148302 C42.1361858,338.003953 41.7742232,336.856167 41.4195607,335.705028 L41.4195607,335.705028 Z M49.3924996,305.714813 C49.6190523,306.8172 49.8525651,307.917206 50.093012,309.014761 L50.093012,309.014761 L35.5692084,313.453824 C35.3098822,312.270092 35.0580365,311.083725 34.8136993,309.894801 L34.8136993,309.894801 Z M45.5434389,281.126002 C45.666057,282.254265 45.7957697,283.380809 45.9325576,284.505569 L45.9325576,284.505569 L31.0828137,287.025128 C30.93536,285.812667 30.7955371,284.598316 30.6633656,283.38215 L30.6633656,283.38215 Z M43.9996156,256.15655 C44.0173552,257.291855 44.0421483,258.426058 44.0739813,259.559103 L44.0739813,259.559103 L29.0800649,260.146812 C29.0457908,258.926881 29.0190979,257.705767 29,256.483536 L29,256.483536 Z M29.8141773,229.541609 L44.755482,231.136184 C44.6689046,232.267974 44.5894162,233.401243 44.5170333,234.53593 L44.5170333,234.53593 L29.5572684,233.204547 C29.6352516,231.982069 29.7208937,230.761066 29.8141773,229.541609 L29.8141773,229.541609 Z M33.1176156,202.844147 L47.8197683,206.370224 C47.6283396,207.483785 47.4439428,208.599478 47.2666006,209.717234 L47.2666006,209.717234 L32.521123,206.453298 C32.7123515,205.248019 32.9111905,204.044943 33.1176156,202.844147 L33.1176156,202.844147 Z M38.8935291,176.750496 L53.1752,182.175994 C52.8807103,183.257496 52.5930825,184.341833 52.3123464,185.42893 L52.3123464,185.42893 L37.9628811,180.259015 C38.2656747,179.086505 38.5759014,177.916971 38.8935291,176.750496 L38.8935291,176.750496 Z M47.0854488,151.554007 L60.7709631,158.813274 C60.3769738,159.849538 59.9895711,160.889373 59.6087927,161.9327 L59.6087927,161.9327 L45.8321818,154.917949 C46.2428118,153.79283 46.660581,152.671488 47.0854488,151.554007 L47.0854488,151.554007 Z M57.5123112,127.725412 L70.4418524,136.712488 C69.9532857,137.69312 69.470958,138.678081 68.9949149,139.667288 L68.9949149,139.667288 L55.9525025,130.910708 C56.4656869,129.844322 56.9856396,128.782527 57.5123112,127.725412 L57.5123112,127.725412 Z M70.1675439,105.251743 L82.1837402,115.860913 C81.6069658,116.772313 81.0359759,117.688783 80.4708238,118.610242 L80.4708238,118.610242 L68.3216372,108.214529 C68.9306735,107.221519 69.5459948,106.233895 70.1675439,105.251743 L70.1675439,105.251743 Z M84.8353436,84.546027 L95.7965014,96.6445992 C95.1398821,97.4745526 94.4885055,98.3102661 93.8424311,99.151665 L93.8424311,99.151665 L82.7299607,87.2472264 C83.4260657,86.3406714 84.1278812,85.4402449 84.8353436,84.546027 L84.8353436,84.546027 Z M101.319516,65.8718434 L111.096003,79.3124622 C110.364731,80.0545588 109.638151,80.8031063 108.916327,81.5580356 L108.916327,81.5580356 L98.9709923,68.2913691 C99.7487278,67.4779633 100.531592,66.6714301 101.319516,65.8718434 L101.319516,65.8718434 Z M119.415978,49.4524745 L127.889382,64.0753412 C127.096254,64.7165303 126.307181,65.3647192 125.522229,66.0198474 L125.522229,66.0198474 L116.864853,51.5481051 C117.710801,50.8420687 118.5612,50.1435035 119.415978,49.4524745 L119.415978,49.4524745 Z M138.906118,35.4869211 L145.970378,51.1194051 C145.120627,51.6551379 144.274317,52.1984049 143.431518,52.7491545 L143.431518,52.7491545 L136.168962,37.2439611 C137.077576,36.6502031 137.989986,36.0645044 138.906118,35.4869211 L138.906118,35.4869211 Z M156.999157,25.3772587 L156.999,26.2114174 L156.748458,25.5003775 C156.832001,25.4592733 156.915567,25.4182337 156.999157,25.3772587 Z"
              id="Combined-Shape"
              fill="url(#linearGradient-2)"
              fillRule="nonzero"
            />
            <Path
              d="M162,0 L162,55.4541154 L111.096003,0.270417378 L162,0 Z"
              id="Combined-Shape"
              fill="url(#linearGradient-3)"
            />
            <Path
              d="M172.34089,478.81199 L164.505454,491.820463 L156.518181,506.217726 L120.249584,482.96864 L172.34089,478.81199 Z"
              id="Combined-Shape"
              fill="url(#linearGradient-4)"
              transform="translate(146.295237, 492.514858) rotate(-30.000000) translate(-146.295237, -492.514858) "
            />
          </G>
        </G>
        <Circle
          cx={cx}
          cy={cy}
          r={10.5}
          fill={isGrey ? Colors.linkGrey : Colors.cerulean}
        />
      </Svg>
    </View>
  );
};

TempSlider.propTypes = {
  setTemperature: PropTypes.func,
  setTemp: PropTypes.func,
  setAllUnits: PropTypes.func,

  temperature: PropTypes.number,
  unit: PropTypes.object,
  allUnits: PropTypes.object,
  desired: PropTypes.object,
  clearDesireTemp: PropTypes.func,
};

const mapDispatchToProps = dispatch => ({
  clearDesireTemp: () => {
    dispatch(remote.clearDesireTemp());
  },
});

export default connect(null, mapDispatchToProps)(TempSlider);
