import { configuration } from '@daikin-dama/redux-iot';
import LoginModeControl from './components/LoginModeControl';
// eslint-disable-next-line import/prefer-default-export
// eslint-disable-next-line import/prefer-default-export

export { default as Tab } from './navigator';
export { default as Stack } from './navigator/loginStack';
export * from '@daikin-dama/redux-iot';

const isIoTModule = true;

export { default as settingStacks } from './navigator/setting';
export { default as settingItems } from './screens/AppSetting/setting';
export * from '@daikin-dama/redux-iot';

function init(config) {
  configuration.init({
    region: config.REGION,
    iotEndpoint: config.IOT_ENDPOINT,
  });
}

export { init };

export { LoginModeControl, isIoTModule };
