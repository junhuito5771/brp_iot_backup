/* eslint-disable react/prop-types */
import React from 'react';
import { Image } from 'react-native';
import {
  HOME_SCREEN,
  WEEKLY_TIMER_SCREEN,
  USAGE_LIST_SCREEN,
} from '../constants/routeNames';

import HomeStack from './home';
import WeeklyTimerStack from './weeklyTimer';
import UsageStack from './usage';

const Tab = {
  [HOME_SCREEN]: {
    name: HOME_SCREEN,
    component: HomeStack,
    options: () => ({
      tabBarLabel: 'Remote',
      tabBarIcon: ({ focused }) => (
        <Image
          source={
            focused
              ? require('../shared/images/tabbar/remote/remoteRed.png')
              : require('../shared/images/tabbar/remote/remoteGrey.png')
          }
        />
      ),
    }),
  },
  [WEEKLY_TIMER_SCREEN]: {
    name: WEEKLY_TIMER_SCREEN,
    component: WeeklyTimerStack,
    options: () => ({
      tabBarLabel: 'Weekly Timer',
      tabBarIcon: ({ focused }) => (
        <Image
          source={
            focused
              ? require('../shared/images/tabbar/timer/timerRed.png')
              : require('../shared/images/tabbar/timer/timerGrey.png')
          }
        />
      ),
    }),
  },
  [USAGE_LIST_SCREEN]: {
    name: USAGE_LIST_SCREEN,
    component: UsageStack,
    options: () => ({
      tabBarLabel: 'Usage',
      tabBarIcon: ({ focused }) => (
        <Image
          source={
            focused
              ? require('../shared/images/tabbar/statistics/chartRed.png')
              : require('../shared/images/tabbar/statistics/chartGrey.png')
          }
        />
      ),
    }),
  },
};

export default Tab;

/* eslint-enable react/prop-types */
