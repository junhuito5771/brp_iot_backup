import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import { ScaleText } from '@module/utility';

import { Colors } from '@module/acson-ui';
import WeeklyTimerScreen from '../screens/WeeklyTimer/weeklyTimer';
import EditWeeklyTimerScreen from '../screens/WeeklyTimer/editWeeklyTimer';

import {
  WEEKLY_TIMER_SCREEN,
  EDIT_WEEKLY_TIMER_SCREEN,
} from '../constants/routeNames';
import DefaultStackOptions from './defaultStackOptions';

const Stack = createStackNavigator();
const TabTopNavigator = createMaterialTopTabNavigator();

const TabList = [
  {
    tabBarLabel: 'S',
    tabBarAccessibilityLabel: 'Sunday',
  },
  {
    tabBarLabel: 'M',
    tabBarAccessibilityLabel: 'Monday',
  },
  {
    tabBarLabel: 'T',
    tabBarAccessibilityLabel: 'Tuesday',
  },
  {
    tabBarLabel: 'W',
    tabBarAccessibilityLabel: 'Wednesday',
  },
  {
    tabBarLabel: 'T',
    tabBarAccessibilityLabel: 'Thursday',
  },
  {
    tabBarLabel: 'F',
    tabBarAccessibilityLabel: 'Friday',
  },
  {
    tabBarLabel: 'S',
    tabBarAccessibilityLabel: 'Satursday',
  },
];

const TimerTabs = () => (
  <TabTopNavigator.Navigator
    sceneContainerStyle={{
      backgroundColor: Colors.white,
    }}
    tabBarOptions={{
      activeTintColor: Colors.black,
      inactiveTintColor: Colors.black,
      allowFontScaling: false,
      style: {
        backgroundColor: Colors.white,
        borderWidth: 0,
        borderColor: Colors.white,
        marginTop: 5,
        elevation: 0,
        shadowColor: Colors.white,
        shadowOpacity: 0.1,
        shadowRadius: 0,
        shadowOffset: {
          height: 0,
        },
      },
      indicatorStyle: {
        backgroundColor: Colors.red,
        height: 3,
        borderRadius: 15,
      },
      labelStyle: {
        fontFamily: 'Avenir-Black',
        fontSize: ScaleText(14),
      },
      tabStyle: {
        padding: 0,
      },
    }}>
    {TabList.map((tabProps, index) => (
      <TabTopNavigator.Screen
        key={tabProps.tabBarAccessibilityLabel}
        name={tabProps.tabBarAccessibilityLabel}
        initialParams={{ dayIndex: index }}
        component={WeeklyTimerScreen}
        options={() => tabProps}
      />
    ))}
  </TabTopNavigator.Navigator>
);

const TimerStack = () => (
  <Stack.Navigator screenOptions={DefaultStackOptions}>
    <Stack.Screen
      name={WEEKLY_TIMER_SCREEN}
      component={TimerTabs}
      options={() => ({
        title: 'Weekly Timer',
        headerLeft: null,
      })}
    />
    <Stack.Screen
      name={EDIT_WEEKLY_TIMER_SCREEN}
      component={EditWeeklyTimerScreen}
      options={() => ({
        title: 'Edit Weekly Timer',
      })}
    />
  </Stack.Navigator>
);

export default TimerStack;
