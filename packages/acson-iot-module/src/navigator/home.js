import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import {
  HOME_SCREEN,
  QR_SCANNER_SCREEN,
  BIND_UNIT_NO_QR_SCREEN,
  WIFI_CONFIG_SCREEN,
  DEVICE_CONFIG_SCREEN,
  SELECT_GROUP_SCREEN,
  SSID_SELECTION_SCREEN,
  REMOTE_SCREEN,
  UNIT_USERS_SCREEN,
  UNIT_USER_SCREEN,
  SHARE_QR_SCREEN,
  ERROR_HISTORY_SCREEN,
  LIMIT_USAGE_SCREEN,
} from '../constants/routeNames';

import DefaultStackOptions from './defaultStackOptions';
import HomeScreen from '../screens/RemoteHome/home';
import QrScanScreen from '../screens/RemotePair/qrScan';
import BindUnitNoQRScreen from '../screens/RemotePair/bindUnitNoQR';
import WifiConfigScreen from '../screens/RemotePair/wifiConfig';
import DeviceConfigScreen from '../screens/RemotePair/deviceConfig';
import SelectGroupScreen from '../screens/RemotePair/selectGroup';
import SSIDSelectionScreen from '../screens/RemotePair/ssidSelection';
import RemoteScreen from '../screens/RemoteControl/remote';
import UnitUsersScreen from '../screens/RemoteSetting/unitUsers';
import UnitUserScreen from '../screens/RemoteSetting/unitUser';
import ShareQRScreen from '../screens/RemoteSetting/shareQR';
import ErrorHistoryScreen from '../screens/RemoteControl/errorHistory';
import LimitUsageScreen from '../screens/RemoteSetting/limitUsage';

// import LimitUsageScreen from '../screens/limitUsage';

const HomeStack = createStackNavigator();

function HomeStackNav() {
  return (
    <HomeStack.Navigator screenOptions={DefaultStackOptions}>
      <HomeStack.Screen
        name={HOME_SCREEN}
        component={HomeScreen}
        options={() => ({
          title: 'Remote Control',
          headerLeft: null,
        })}
      />
      <HomeStack.Screen
        name={QR_SCANNER_SCREEN}
        component={QrScanScreen}
        options={() => ({
          title: 'Scan QR Code',
        })}
      />
      <HomeStack.Screen
        name={BIND_UNIT_NO_QR_SCREEN}
        component={BindUnitNoQRScreen}
        options={() => ({
          title: 'More options',
        })}
      />
      <HomeStack.Screen
        name={WIFI_CONFIG_SCREEN}
        component={WifiConfigScreen}
        options={() => ({
          title: 'Wi-Fi Network Configuration',
        })}
      />
      <HomeStack.Screen
        name={DEVICE_CONFIG_SCREEN}
        component={DeviceConfigScreen}
        options={() => ({
          title: 'Unit Details',
        })}
      />
      <HomeStack.Screen
        name={SELECT_GROUP_SCREEN}
        component={SelectGroupScreen}
        options={() => ({
          title: 'Select Group',
        })}
      />
      <HomeStack.Screen
        name={SSID_SELECTION_SCREEN}
        component={SSIDSelectionScreen}
        options={() => ({
          title: 'Select SSID',
        })}
      />
      <HomeStack.Screen
        name={REMOTE_SCREEN}
        component={RemoteScreen}
        options={() => ({
          headerShown: false,
        })}
      />
      <HomeStack.Screen
        name={UNIT_USERS_SCREEN}
        component={UnitUsersScreen}
        options={() => ({
          title: 'User Management',
        })}
      />
      <HomeStack.Screen
        name={UNIT_USER_SCREEN}
        component={UnitUserScreen}
        options={() => ({
          title: 'Usability Control',
        })}
      />
      <HomeStack.Screen
        name={SHARE_QR_SCREEN}
        component={ShareQRScreen}
        options={() => ({
          title: 'Share QR Code',
        })}
      />
      <HomeStack.Screen
        name={ERROR_HISTORY_SCREEN}
        component={ErrorHistoryScreen}
        options={() => ({
          title: 'Error History',
        })}
      />
      <HomeStack.Screen
        name={LIMIT_USAGE_SCREEN}
        component={LimitUsageScreen}
        options={() => ({
          title: 'Limit Usage',
        })}
      />
    </HomeStack.Navigator>
  );
}

export default HomeStackNav;
