import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import { USAGE_GRAPH_SCREEN, USAGE_LIST_SCREEN } from '../constants/routeNames';
import UsageListScreen from '../screens/Usage/usageList';
import UsageGraphScreen from '../screens/Usage/usageGraph';

import DefaultStackOptions from './defaultStackOptions';

const Stack = createStackNavigator();

const UsageStack = () => (
  <Stack.Navigator screenOptions={DefaultStackOptions}>
    <Stack.Screen
      name={USAGE_LIST_SCREEN}
      component={UsageListScreen}
      options={() => ({
        title: 'Energy Consumption',
        headerLeft: null,
      })}
    />
    <Stack.Screen
      name={USAGE_GRAPH_SCREEN}
      component={UsageGraphScreen}
      options={() => ({
        title: '',
      })}
    />
  </Stack.Navigator>
);

export default UsageStack;
