import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { Webview } from '@module/acson-ui';
import {
  ALEXA_SCREEN,
  PREFERRED_UNITS_SCREEN,
  ALEXA_AUTH_SCREEN,
  ALEXA_COMMAND_SCREEN,
  ALEXA_LINK_SCREEN,
  ALEXA_LWA_SCREEN,
  ALEXA_MANUAL_SCREEN,
} from '../constants/routeNames';

import SmartSpeakerScreen from '../screens/SmartSpeaker';
import AlexaLinkScreen from '../screens/SmartSpeaker/alexaLink';
import PreferredUnitScreen from '../screens/SmartSpeaker/preferredUnits';
import AlexaAuthScreen from '../screens/SmartSpeaker/alexaAuth';
import AlexaLWAScreen from '../screens/SmartSpeaker/alexaLWA';
import AlexaManualScreen from '../screens/SmartSpeaker/alexaManual';

import DefaultStackOptions from './defaultStackOptions';

const SmartSpeakerStack = createStackNavigator();

const SmartSpeakerNav = () => (
  <SmartSpeakerStack.Navigator screenOptions={DefaultStackOptions}>
    <SmartSpeakerStack.Screen
      name={ALEXA_SCREEN}
      component={SmartSpeakerScreen}
    />
    <SmartSpeakerStack.Screen
      name={PREFERRED_UNITS_SCREEN}
      component={PreferredUnitScreen}
      options={() => ({
        title: 'Preferred Units',
      })}
    />
    <SmartSpeakerStack.Screen
      name={ALEXA_AUTH_SCREEN}
      component={AlexaAuthScreen}
      options={() => ({
        title: 'Verify with Acson App Account',
      })}
    />
    <SmartSpeakerStack.Screen
      name={ALEXA_LINK_SCREEN}
      component={AlexaLinkScreen}
      options={() => ({
        title: 'Alexa',
      })}
    />
    <SmartSpeakerStack.Screen
      name={ALEXA_LWA_SCREEN}
      component={AlexaLWAScreen}
      options={() => ({
        title: 'Log in with Amazon',
      })}
    />
    <SmartSpeakerStack.Screen name={ALEXA_COMMAND_SCREEN} component={Webview} />
    <SmartSpeakerStack.Screen
      name={ALEXA_MANUAL_SCREEN}
      component={AlexaManualScreen}
    />
  </SmartSpeakerStack.Navigator>
);

export default SmartSpeakerNav;
