import BluetoothModeScreen from '../screens/bluetoothMode';
import WlanModeScreen from '../screens/wlanMode';
import OtherModeRemoteScreen from '../screens/RemoteControl/remote';

import {
  WLAN_SCREEN,
  BLUETOOTH_SCREEN,
  OTHER_MODE_REMOTE_SCREEN,
} from '../constants/routeNames';

const stack = {
  [BLUETOOTH_SCREEN]: {
    name: BLUETOOTH_SCREEN,
    component: BluetoothModeScreen,
  },
  [WLAN_SCREEN]: {
    name: WLAN_SCREEN,
    component: WlanModeScreen,
  },
  [OTHER_MODE_REMOTE_SCREEN]: {
    name: OTHER_MODE_REMOTE_SCREEN,
    component: OtherModeRemoteScreen,
    options: () => ({
      headerShown: false,
    }),
  },
};

export default stack;
