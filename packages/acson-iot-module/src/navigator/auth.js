// import React from 'react';
// import { View, Platform } from 'react-native';
// import { createStackNavigator, TransitionSpecs } from 'react-navigation-stack';
// import { heightPercentage, ScaleText } from '@module/utility';
// import LoginScreen from '../screens/login';
// import SignUpScreen from '../screens/signUp';
// import ConfirmPasswordScreen from '../screens/enterPassword';
// import SignUpVerificationScreen from '../screens/signUpVerification';
// import ForgotPasswordScreen from '../screens/forgotpassword';
// import ResetPasswordScreen from '../screens/resetPassword';
// import UserAgreementScreen from '../screens/useragreement';
// import BLEModeScreen from '../screens/bluetoothMode';
// import WlanModeScreen from '../screens/wlanMode';
// import RemoteScreen from '../screens/remote';
// import PrivacyPolicyScreen from '../screens/privacyPolicy';

// import Colors from '../../common/shared/Colors';
// import BackButton from '../../common/container/BackButton';

// const AuthStack = createStackNavigator(
//   {
//     Login: {
//       screen: LoginScreen,
//       navigationOptions: ({ navigation }) => ({
//         headerShown: navigation.state.routeName !== 'Login',
//       }),
//     },
//     PrivacyPolicy: {
//       screen: PrivacyPolicyScreen,
//       navigationOptions: () => ({
//         title: 'Confirmation',
//       }),
//     },
//     SignUp: {
//       screen: SignUpScreen,
//       navigationOptions: () => ({
//         title: 'Sign Up',
//       }),
//     },
//     ConfirmPassword: {
//       screen: ConfirmPasswordScreen,
//       navigationOptions: () => ({
//         title: 'Confirm Password',
//       }),
//     },
//     SignUpVerification: {
//       screen: SignUpVerificationScreen,
//       navigationOptions: () => ({
//         title: 'Verification',
//       }),
//     },
//     ForgotPassword: {
//       screen: ForgotPasswordScreen,
//       navigationOptions: () => ({
//         title: 'Forgot Password',
//       }),
//     },
//     ResetPassword: {
//       screen: ResetPasswordScreen,
//       navigationOptions: () => ({
//         title: 'Forgot Password',
//       }),
//     },
//     AuthUserAgreement: {
//       screen: UserAgreementScreen,
//       navigationOptions: ({ navigation }) => ({
//         title: 'Terms of Use',
//         headerLeft: () => <BackButton navigation={navigation} />,
//       }),
//     },
//     BleMode: {
//       screen: BLEModeScreen,
//       navigationOptions: () => ({
//         title: 'Available Units',
//       }),
//     },
//     WlanMode: {
//       screen: WlanModeScreen,
//       navigationOptions: () => ({
//         title: 'Available Units',
//       }),
//     },
//     OtherModeRemote: {
//       screen: RemoteScreen,
//     },
//   },
//   {
//     defaultNavigationOptions: ({ navigation }) => ({
//       headerStyle: {
//         height: heightPercentage(10),
//         elevation: 5,
//         shadowOpacity: 0.2,
//         shadowRadius: 2,
//         shadowOffset: {
//           height: 1,
//         },
//       },
//       headerTitleStyle: {
//         fontFamily: 'Avenir-Black',
//         color: Colors.blueGrey,
//         fontWeight: '700',
//         fontSize: ScaleText(18),
//       },
//       headerTitleAllowFontScaling: false,
//       headerTitleContainerStyle: {
//         justifyContent: 'center',
//         flexDirection: 'row',
//         alignItems: 'center',
//       },
//       headerLeft: () => <BackButton navigation={navigation} />,
//       headerRight: () => <View />,
//       cardStyle: {
//         backgroundColor: 'white',
//       },
//       headerTitleAlign: 'center',
//       transitionSpec: {
//         open: Platform.select({
//           ios: TransitionSpecs.TransitionIOSSpec,
//           android: TransitionSpecs.FadeInFromBottomAndroidSpec,
//         }),
//         close: Platform.select({
//           ios: TransitionSpecs.TransitionIOSSpec,
//           android: TransitionSpecs.FadeOutToBottomAndroidSpec,
//         }),
//       },
//     }),
//     headerMode: 'float',
//   }
// );

// export default AuthStack;
