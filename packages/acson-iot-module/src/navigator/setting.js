import AlexaStack from './alexa';
import QRCodeValidityScreen from '../screens/AppSetting/qrCodeValidty';
import AgreementScreen from '../screens/AppSetting/useragreement';
import ErrorCodes from '../screens/AppSetting/errorCodes';

import {
  ALEXA_SCREEN,
  QRCODE_VALIDITY_SCREEN,
  USER_AGREEMENT_SCREEN,
  ERROR_CODES_SCREEN,
} from '../constants/routeNames';

const settingStacks = {
  [ALEXA_SCREEN]: {
    name: ALEXA_SCREEN,
    component: AlexaStack,
    options: () => ({
      headerShown: false,
    }),
  },
  [QRCODE_VALIDITY_SCREEN]: {
    name: QRCODE_VALIDITY_SCREEN,
    component: QRCodeValidityScreen,
  },
  [USER_AGREEMENT_SCREEN]: {
    name: USER_AGREEMENT_SCREEN,
    component: AgreementScreen,
  },
  [ERROR_CODES_SCREEN]: {
    name: ERROR_CODES_SCREEN,
    component: ErrorCodes,
  },
};

export default settingStacks;
